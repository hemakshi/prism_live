<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('home_model');
		$this->load->model('person_model');
		$this->load->model("order_model");
		require_once(APPPATH . "/config/mail_template.php");
	}
	public function index()
	{
		$data['title'] = "Home | " . WEBSITE_NAME;
		$data['products'] = $this->product_model->getFeaturedProducts();
		$data['banner_images'] = $this->home_model->getBannerImages(GALLERY_SET_BANNER);
		$this->load->view('home', $data);
	}

	public function page_missing()
	{		
		$this->load->view('page_not_found');
	}

	public function about()
	{
		$data['title'] = "About | " . WEBSITE_NAME;
		$this->load->view('about', $data);
	}


	public function uploadFile()
	{
		$data['title'] = " Upload File | " . WEBSITE_NAME;
		$this->load->view('upload_file', $data);
	}

	public function orderHistory()
	{
		$data['title'] = " Order History | " . WEBSITE_NAME;
		$this->load->view('order_history', $data);
	}

	public function ledger()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'receipt_status';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = " Ledger | " . WEBSITE_NAME;
			$data['ledgerData'] = $this->home_model->getPaymentLedger($prs_id);
			$this->load->view('ledger', $data);
		}
	}

	public function pricelist()
	{
		$data['title'] = " Price List | " . WEBSITE_NAME;
		$this->load->view('price_list', $data);
	}

	public function receiptStatus()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'receipt_status';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = " Receipt Status | " . WEBSITE_NAME;
			$data['receiptData'] = $this->home_model->getPaymentReceiptStatus($prs_id);
			$this->load->view('receipt_status', $data);
		}
	}
	public function privacyPolicy()
	{
		$data['title'] = "Privacy Policy |" . WEBSITE_NAME;;
		$this->load->view('privacy_policy', $data);
	}
	public function terms()
	{
		$data['title'] = "Terms & Conditions | " . WEBSITE_NAME;
		$this->load->view('terms', $data);
	}
	public function callUs()
	{
		$data['title'] = "Call us | " . WEBSITE_NAME;
		$this->load->view('call_us', $data);
	}
	public function returnPolicy()
	{
		$data['title'] = "Return Policy | " . WEBSITE_NAME;
		$this->load->view('return_policy', $data);
	}
	public function contact()
	{
		$data['title'] = "Contact | " . WEBSITE_NAME;
		$this->load->view('contact', $data);
	}
	public function dashboard()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		}
		$data['title'] = "Profile | " . WEBSITE_NAME;
		$data['userData'] = $this->person_model->getUserDataById($prs_id);
		$this->load->view('dashboard', $data);
	}
	public function changePassword()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		}
		$data['title'] = "Change Password | " . WEBSITE_NAME;
		$data['userData'] = $this->person_model->getUserDataById($prs_id);
		$this->load->view('change_password', $data);
	}
	public function contact_us_form()
	{
		// $adminsEmailList = $this->home_model->getAdminsEmailList();
		// $adminEmails = "";
		// foreach ($adminsEmailList as $email) {
		// 	$adminEmails .= $email . ",";
		// }

		// Admin Contact Us
		$adminEmaildata['contact_name']    = $this->input->post('contact_name');
		$adminEmaildata['contact_email']   = $this->input->post('contact_email');
		$adminEmaildata['contact_mob']     = $this->input->post('contact_mob');
		$adminEmaildata['contact_sub']     = $this->input->post('contact_sub');
		$adminEmaildata['contact_msg']     = $this->input->post('contact_msg');
		$adminEmaildata['email']           = EMAIL_ID_INFO;
		$adminEmaildata['mail_subject']    = ADMIN_CONTACT_US_SUBJECT;

		$body = ADMIN_CONTACT_US_BODY;
		$body = str_replace('%FULLNAME%', $adminEmaildata['contact_name'], $body);
		$body = str_replace('%EMAIL%', $adminEmaildata['contact_email'], $body);
		$body = str_replace('%MOBILE%', $adminEmaildata['contact_mob'], $body);
		$body = str_replace('%MESSAGE%', $adminEmaildata['contact_msg'], $body);

		$adminEmaildata['mail_body']       = $body;
		$adminEmaildata['template']        = 'email_template/adminContactUs';
		$adminEmaildata['email_cc']        = '';
		// Admin Contact Us
		// User Contact Us
		$userEmaildata['email']            = $adminEmaildata['contact_email'];
		$userEmaildata['subject']          = USER_SUBJECT;
		$userEmaildata['message']          = '';
		$userEmaildata['template']         = 'email_template/userContactUs';
		$userEmaildata['email_cc']         = '';

		// Save Contact Us Data in Database
		$contactUsData['CON_name'] = $this->input->post('contact_name');
		$contactUsData['CON_mail_id'] = $this->input->post('contact_email');
		$contactUsData['CON_mob_no'] = $this->input->post('contact_mob');
		$contactUsData['CON_objective'] = "Inquiry";
		$contactUsData['CON_msg'] = $this->input->post('contact_msg');
		$contactUsData['CON_created_on'] = date('Y-m-d H:i:s');

		// User Contact Us
		$saveConactUs = $this->home_model->insert('contact', $contactUsData);
		$isMailToAdmin = $this->home_model->sendMail($adminEmaildata['email'], $adminEmaildata);
		// $user_email = $this->home_model->sendMail($userEmaildata);

		if ($saveConactUs && $isMailToAdmin) {
			$success = true;
			$message = 'Thank you for contacting us, we will get back to you shortly';
			$linkn    = base_url('home');
		} else {
			$success = false;
			$message = 'Oops !! Some error occured';
			$linkn   = '';
		}
		echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
	}
	public function editProfile()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'profile';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}

			$data['title'] = "Profile Edit | " . WEBSITE_NAME;
			$data['userData'] = $this->person_model->getUserDataById($prs_id);
			$this->load->view('edit_profile', $data);
		}
	}

	/* Complaints Methods- Start */
	public function complaintsList()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'my-complaints';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = " My Compaints | " . WEBSITE_NAME;
			$data['compaintsData'] = $this->home_model->getAllCompaintsByPersonId($prs_id);
			// print_r($data['compaintsData']);
			$this->load->view('complaints_list', $data);
		}
	}

	public function complaintRegisterView()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'register-complaint';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = " Register Compaint | " . WEBSITE_NAME;
			$this->load->view('complaint_register', $data);
		}
	}

	function generateRandomNumber()
	{
		$orderId = mt_rand(0, 10000);
		return $orderId;
	}

	function registerComplaint()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '' || empty($prs_id)) {
			echo json_encode(array("success" => false, 'message' => 'Login must be require !!', 'linkn' => base_url('login')));
		} else {
			$success = true;
			$message = 'Complaint registered successfully.';
			$linkn = base_url() . 'my-complaints';

			$target_dir = COMPLAINT_UPLOAD_DIRECTORY;
			$fileExt = pathinfo($_FILES['printed_file']['name'], PATHINFO_EXTENSION);
			$randomId = $this->generateRandomNumber();
			$newFilename = "complaint" . "_" . $prs_id . $randomId . "." . $fileExt;
			$uploadfile = $target_dir . $newFilename;

			if (move_uploaded_file($_FILES["printed_file"]["tmp_name"], $uploadfile)) {
				$uploadedFilesName = $newFilename;
			} else {
				$success = false;
				$message = "The file " . basename($_FILES["printed_file"]["name"]) . " has not been uploaded.";
			}

			if ($success == true) {
				$complaint_data = array();
				$complaint_data['com_prs_id'] = $prs_id;
				$complaint_data['com_ord_id'] = $this->input->post('order_number');
				$complaint_data['com_desc_id'] = $this->input->post('complaint_description');
				$complaint_data['com_image'] = $uploadedFilesName;
				$complaint_data['com_remarks'] = '';
				$complaint_data['com_status'] = 1;
				$complaint_data['com_updated_by'] = $prs_id;
				$complaint_data['com_created_on'] = date('Y-m-d H:i:s');
				$complaint_data['com_updated_on'] = date('Y-m-d H:i:s');

				$result = $this->home_model->registerComplaint($complaint_data);
				if ($result == 0) {
					$success = false;
					$message = 'Sorry, there was an error registering complaint data.';
				}
			}
			/* Send Mail */
			if ($success == true) {
				$this->home_model->sendMailForRegisterComplaintSuccess($complaint_data);
			}
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
		}
	}
	/* Complaints Methods- End */

	/* Quotation Methods- Start */
	public function quotationsList()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'my-quotations';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = " My Quotations | " . WEBSITE_NAME;
			$data['quotationsDetails'] = $this->home_model->getAllQuotationsByPersonId($prs_id);
			$this->load->view('quotations_list', $data);
		}
	}
	public function quotationDetailView($referenceNo)
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'my-quotations';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = "Quotation Detail# " . $referenceNo . " | " . WEBSITE_NAME;
			$data['quotationData'] = $this->home_model->getQuotationsById($prs_id, $referenceNo);
			// print_r($data['quotationData']);/
			$this->load->view('quotation_detail', $data);
		}
	}
	public function quotationRequestView()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'quotation-request';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = " Quotations Request | " . WEBSITE_NAME;
			$this->load->view('quotation_request', $data);
		}
	}

	function sendQuotationRequest()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '' || empty($prs_id)) {
			echo json_encode(array("success" => false, 'message' => 'Login must be require !!', 'linkn' => base_url('login')));
		} else {
			$success = true;
			$message = 'Quotation request sent successfully.';
			$linkn = base_url() . 'my-quotations';
			$randomReferenceNo = $prs_id . $this->generateRandomNumber();
			$uploadedFilesName = '';


			if (isset($_FILES['file_related_image']) && $_FILES['file_related_image']['name'] != '') {
				$target_dir = QUOTATION_UPLOAD_DIRECTORY;
				$fileExt = pathinfo($_FILES['file_related_image']['name'], PATHINFO_EXTENSION);
				$newFilename = "quotation" . "_" . $randomReferenceNo . "." . $fileExt;
				$uploadfile = $target_dir . $newFilename;

				if (move_uploaded_file($_FILES["file_related_image"]["tmp_name"], $uploadfile)) {
					$uploadedFilesName = $newFilename;
				} else {
					$success = false;
					$message = "The file " . basename($_FILES["file_related_image"]["name"]) . " has not been uploaded.";
				}
			}

			if ($success == true) {
				$quotation_data = array();
				$quotation_data['qr_reference_no'] = $randomReferenceNo;
				$quotation_data['qr_prs_id'] = $prs_id;
				$quotation_data['qr_product_name'] = $this->input->post('txt_product_name');
				$quotation_data['qr_paper_gsm_size'] = $this->input->post('txt_pepar_gsm_size');
				$quotation_data['qr_creasing'] = $this->input->post('sel_creasing');
				$quotation_data['qr_half_cut'] = $this->input->post('sel_halfcut');
				$quotation_data['qr_uv'] = $this->input->post('sel_uv');
				$quotation_data['qr_lamination_type'] = $this->input->post('sel_lamination_type');
				$quotation_data['qr_lamination'] = $this->input->post('sel_lamination');
				$quotation_data['qr_qty'] = $this->input->post('txt_quantity');
				$quotation_data['qr_remarks'] = $this->input->post('txt_remarks');
				$quotation_data['qr_related_file'] = $uploadedFilesName;
				$quotation_data['qr_updated_by'] = $prs_id;
				$quotation_data['qr_created_on'] = date('Y-m-d H:i:s');
				$quotation_data['qr_updated_on'] = date('Y-m-d H:i:s');

				$result = $this->home_model->sendQuotationRequest($quotation_data);
				if ($result == 0) {
					$success = false;
					$message = 'Sorry, there was an error while inserting quotation data.';
				}
			}
			/* Send Mail */
			if ($success == true) {
				$this->home_model->sendMailToAdminForQuotationRequest($quotation_data);
			}
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
		}
	}
	/* Quotation Methods- End */
}
