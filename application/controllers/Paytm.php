<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Paytm extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("order_model");
	}
	function pay($ord_encrypted_id = '')
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		$wallet_bal = 0;
		if (!empty($prs_id) && $prs_id != '') {
			$getWalletDetailByPersonId_result = $this->order_model->getWalletDetailByPersonId($prs_id);
			$wallet_detail = json_decode($getWalletDetailByPersonId_result, true);
			$wallet_bal = $wallet_detail['amount'];
		} else {
			redirect('login', 'refresh');
		}

		$data = array();
		if ($this->session->userdata('payment_order')) {
			$data['order'] = $this->session->userdata('order');
		} else {
			$data['order'] = '';
		}
		$data['title'] = "Add Balance | " . WEBSITE_NAME;
		$data['wallet_balance'] = $wallet_bal;
		$data['customer_mobile'] = $this->session->userdata(PROJECT_SESSION_MOB);
		$data['customer_email'] = $this->session->userdata(PROJECT_SESSION_EMAIL);
		$this->load->view('paytm_form', $data);
		$this->session->set_userdata('payment_flag', '');
	}

	function findActualAmount($txn_amount)
	{
		$txn_amount += ($txn_amount * PAYMENT_TRANSACTION_CHARGE / 100);
		return $txn_amount;
	}

	function paytmpost()
	{
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");

		// following files need to be included
		require_once(APPPATH . "/third_party/paytmlib/config_paytm.php");
		require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");

		$checkSum = "";
		$paramList = array();
		$this->session->set_userdata('transaction_amount', $_POST["TXN_AMOUNT"]);
		$this->session->set_userdata('transaction_remarks', $_POST["REMARKS"]);
		$total_amount = $this->findActualAmount($_POST["TXN_AMOUNT"]);

		$ORDER_ID 		   	= $_POST["ORDER_ID"];
		$CUST_ID 			= $_POST["CUST_ID"];
		$INDUSTRY_TYPE_ID 	= $_POST["INDUSTRY_TYPE_ID"];
		$CHANNEL_ID 		= $_POST["CHANNEL_ID"];
		$TXN_AMOUNT 		= $total_amount;
		$MSISDN 			= $_POST["MSISDN"];
		$EMAIL 			= $_POST["EMAIL"];
		// $CALLBACK_URL 		= $_POST["CALLBACK_URL"];

		// Create an array having all required parameters for creating checksum.
		$paramList["MID"] 				= PAYTM_MERCHANT_MID;
		$paramList["ORDER_ID"] 		= $ORDER_ID;
		$paramList["CUST_ID"] 			= $CUST_ID;
		$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
		$paramList["CHANNEL_ID"] 		= $CHANNEL_ID;
		$paramList["TXN_AMOUNT"] 		= $TXN_AMOUNT;
		$paramList["WEBSITE"] 			= PAYTM_MERCHANT_WEBSITE;
		$paramList["MSISDN"] 			= $MSISDN; //Mobile number of customer
		$paramList["EMAIL"]			= $EMAIL; //Email ID of customer
		$paramList["VERIFIED_BY"] 		= "EMAIL";
		$paramList["IS_USER_VERIFIED"] = "YES";
		// $paramList["CALLBACK_URL"] 	= $CALLBACK_URL;
		$paramList["CALLBACK_URL"] = base_url('paytm/response');

		/*
	 	$paramList["MSISDN"] = $MSISDN; //Mobile number of customer
	 	$paramList["EMAIL"] = $EMAIL; //Email ID of customer
	 	$paramList["VERIFIED_BY"] = "EMAIL"; //
	 	$paramList["IS_USER_VERIFIED"] = "YES"; //
	 	*/

		//Here checksum string will return by getChecksumFromArray() function.		
		$checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
		// log_message('error', 'Paytm ::Request code-' . json_encode($paramList) . 'checksum' . json_encode($checkSum) . 'Print Id' . $ORDER_ID);
		// exit;
		echo "<html>
			<head>
			<title>Merchant Check Out Page</title>
			</head>
			<body>
				<center><h1>Please do not refresh this page...</h1></center>
					<form method='post' action='" . PAYTM_TXN_URL . "' name='f1'>
			<table>
			 <tbody>";

		foreach ($paramList as $name => $value) {
			echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
		}

		echo "<input type='hidden' name='CHECKSUMHASH' value='" . $checkSum . "'>
			 </tbody>
			</table>
			<script type='text/javascript'>
			 document.f1.submit();
			</script>
			</form>
			</body>
			</html>";
	}

	public function response()
	{
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");

		// following files need to be included
		require_once(APPPATH . "/third_party/paytmlib/config_paytm.php");
		require_once(APPPATH . "/third_party/paytmlib/encdec_paytm.php");

		$paytmChecksum = "";
		$paramList = array();
		$order_data = array();
		$transaction_data = array();
		$isValidChecksum = "FALSE";

		$paramList = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

		// log_message('error', 'Paytm ::Response code-' . json_encode($_POST) . 'checksum' . json_encode($paytmChecksum) . 'ORDERID : ' . $_POST['ORDERID']);
		$response_data =  json_encode($_POST);

		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

		$ord_reference_no = $_POST['ORDERID'];
		// $data['order']=$this->order_model->getOrderData($ord_id);		

		if ($isValidChecksum == "TRUE") {
			$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
			$data  = array();
			$pyt_pay_status = PAYMENT_PAY_STATUS_FAILED;
			if ($_POST["STATUS"] == "TXN_SUCCESS") {
				$pyt_pay_status = PAYMENT_PAY_STATUS_SUCCESS;
			} else {
				$pyt_pay_status = PAYMENT_PAY_STATUS_FAILED;
			}

			$data['scontent']['PAYTM_PAY_RESPONSE_CODE'] = $_POST["RESPCODE"];
			$data['scontent']['PAYTM_PAY_STATUS'] = $_POST["STATUS"];
			$data['status']						= $_POST;
			$data['personId'] = $prs_id;
			// log_message('error', 'db_check_txn1 >> ' . json_encode($data));

			// if (isset($_POST) && count($_POST) > 0) {
			// 	foreach ($_POST as $paramName => $paramValue) {
			// 		echo "<br/>" . $paramName . " = " . $paramValue;
			// 	}
			// }

			$paymentRemarks = $this->session->userdata('transaction_remarks') != '' ? $this->session->userdata('transaction_remarks') : '';

			$transaction_data['pyt_pay_response_code'] = $data['scontent']['PAYTM_PAY_RESPONSE_CODE'];
			$transaction_data['pyt_remarks'] = $paymentRemarks;
			$transaction_data['pyt_pay_status'] = $pyt_pay_status;
			$transaction_data['pyt_order_reference_no'] = $data['status']['ORDERID'];
			$transaction_data['pyt_mid_no'] = $data['status']['MID'];
			$transaction_data['pyt_txn_id'] = $data['status']['TXNID'];
			$transaction_data['pyt_txn_amt'] = $data['status']['TXNAMOUNT'];
			$transaction_data['pyt_payment_mode'] = $data['status']['PAYMENTMODE'];
			$transaction_data['pyt_currency'] = $data['status']['CURRENCY'];
			$transaction_data['pyt_txn_date'] = $data['status']['TXNDATE'];
			$transaction_data['pyt_txn_status'] = $data['status']['STATUS'];
			$transaction_data['pyt_resp_code'] = $data['status']['RESPCODE'];
			$transaction_data['pyt_resp_message'] = $data['status']['RESPMSG'];
			$transaction_data['pyt_gateway_name'] = $data['status']['GATEWAYNAME'];
			$transaction_data['pyt_bnk_txn_id'] = $data['status']['BANKTXNID'];
			$transaction_data['pyt_bnk_name'] = $data['status']['BANKNAME'];
			$transaction_data['pyt_crtd_by'] = $prs_id != '' ? $prs_id : 0;
			$transaction_data['pyt_updt_by'] = $prs_id != '' ? $prs_id : 0;

			$this->home_model->insert('paytm_transactions', $transaction_data);
			if ($_POST["STATUS"] == "TXN_SUCCESS") {
				/* Payment Successfull */
				$order_data['ord_payment_status'] = ORDER_PAYMENT_STATUS_PAID;
				$this->home_model->update('ord_reference_no', $ord_reference_no, $order_data, 'person_order');

				/* Update wallet balance- Start */
				if (!empty($prs_id) && $prs_id != '') {
					$wallet_detail = $this->order_model->getWalletDetailByPersonId($prs_id);
					$wallet_detail = json_decode($wallet_detail, true);
					$deductAmount = round(floatval($transaction_data['pyt_txn_amt'] * 3 / 100), 2);
					$amountAfterDeduct = round($transaction_data['pyt_txn_amt'] - $deductAmount, 2);
					$transaction_data['transaction_amount'] = $this->session->userdata('transaction_amount') != '' ? $this->session->userdata('transaction_amount') : $amountAfterDeduct;
					$isBalanceAdded = $this->order_model->addWalletBalance($transaction_data, $wallet_detail);

					if ($isBalanceAdded) {
						$this->session->set_userdata('isUserAvailable', false);
						$data['isBalanceAdded'] = "True";
						// log_message('error', 'Add balance through paytm-success >> ' . json_encode($data));
					} else {
						$this->session->set_userdata('isUserAvailable', false);
						$data['isBalanceAdded'] = "True";
						// log_message('error', 'Add balance through paytm-failure >> ' . json_encode($data));
					}
				} else {
					$this->session->set_userdata('isUserAvailable', false);
					// log_message('error', 'Add balance through paytm-failure >> ' . json_encode($data));
				}
				/* Update wallet balance- End */

				// $this->order_model->sendOrderSuccessMail($ord_reference_no);
				$this->session->session_unset('transaction_amount');
				$this->session->session_unset('transaction_remarks');
				
				$this->session->set_userdata('payment_order', 'success');
				$data['message']					= "Success";
				// log_message('error', 'payment success through paytm >> ' . json_encode($data));
			} else {
				/* Payment Failed */
				$this->order_model->updateOrderStatus($ord_reference_no, ORDER_IS_SUCCESS_FAILED);
				$this->session->set_userdata('payment_order', 'failed');
				// log_message('error', 'payment failure through paytm >> ' . json_encode($data));
				// $order_data['ord_payment_status'] = ;
			}
		} else {
			/* Payment Failed */
			$this->order_model->updateOrderStatus($ord_reference_no, ORDER_IS_SUCCESS_FAILED);
			$this->session->set_userdata('payment_order', 'failed');
			// $order_data['ord_payment_status'] = ;
			// log_message('error', 'payment failure through paytm >> Checksum Mismatched' . json_encode($data));
		}

		$encrypted_id = $this->url_encrypt->encrypt_openssl($_POST['ORDERID']);
		redirect(base_url() . 'add_balance');
		// redirect(base_url().'order-summary-'.$encrypted_id);
	}
}
