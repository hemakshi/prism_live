<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shoping_cart extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
		$this->load->model('person_model');
		// Load cart library
		$this->load->library('cart');

		$this->load->library('form_validation');
	}

	private function _set_headers()
	{
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i(worry)') . ' GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Pragma: no-cache');
	}


	public function add($value = '')
	{
		$this->load->library('cart');
		$name = ucfirst(url_title(convert_accented_characters($_POST['product_name']), ' ', TRUE));
		$data  = array(
			'id' => $_POST['product_id'],
			'name' => $name,
			'qty' => $_POST['product_quantity'],
			'price' => $_POST['product_price'],
			'prd_subtotal' => $_POST['product_subtotal'],
			'category' => $_POST['product_category'],
			'prd_height' => $_POST['product_height'],
			'prd_width' => $_POST['product_width'],
			'filename' => ""
		);
		$this->cart->insert($data);
		$this->findGrandTotal();
		echo $this->view();
	}

	/* Update cart item */
	function updateCart()
	{
		$rowid = $this->input->post('rowid');
		$price = sprintf("%.2f", $this->input->post('price'));
		$qty = $this->input->post('qty');
		$height = sprintf("%.2f", $this->input->post('height'));
		$width = sprintf("%.2f", $this->input->post('width'));
		$category = $this->input->post('category');

		if ($this->input->post('change') == "-") {
			$qty -= 1;
		}
		if ($this->input->post('change') == "+") {
			$qty += 1;
		}
		if ($category == STICKER || $category == BROCHURE_MIX) {
			$price = $price * $qty * $height * $width;
		} else {
			$price *= $qty;
		}

		$data = array(
			'rowid'   => $rowid,
			'qty'     => $qty,
			'prd_height' => $height,
			'prd_width' => $width,
			'prd_subtotal' => $price
		);
		/* Update the cart with the new information */
		$result = $this->cart->update($data);
		$this->findGrandTotal();
		// redirect(base_url() . 'cart');
		$this->load();
		echo $result;
	}

	function generateOrderId()
	{
		$orderId = mt_rand(0, 1000000) . "-" . mt_rand(0, 1000000);
		return $orderId;
	}

	function updateFilename($rowid, $fileName)
	{
		$update = 0;
		if (!empty($rowid) && !empty($fileName)) {
			$data = array(
				'rowid' => $rowid,
				'filename'   => $fileName,
			);
			$update = $this->cart->update($data);
		}
		return $update ? 'ok' : 'err';
	}

	function onCheckout()
	{
		$count = 0;
		$order_id = "";
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);

		if ($prs_id == '') {
			$success = false;
			$message = 'Login must be require for checkout !!';
			$linkn = base_url('login');
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
		} else {
			/* Create Order Reference No. Start */
			// $temp_order_reference_no = $prs_id . "-" . $this->generateOrderId();
			$temp_order_reference_no = $prs_id . "-" . $this->order_model->generateOrderReferenceNumber($prs_id);
			$this->session->unset_userdata('order');

			$ord_id = $this->order_model->create_order($temp_order_reference_no);
			if ($ord_id != '-1') {

				/* GET ORDER REFERENCE NO. FROM DATABASE- START */
				$getLastGeneratedOrdRefNo_result = $this->order_model->getLastGenerated_ord_reference_no($ord_id);
				$orderDetails = json_decode($getLastGeneratedOrdRefNo_result, true);
				$order_id =  $orderDetails['ord_reference_no'];
				/* GET ORDER REFERENCE NO. FROM DATABASE- END */

				$filesArray = array();
				$success = true;
				$message = 'Order created successfully';
				$linkn = '';

				foreach ($this->cart->contents() as $item) {
					$rowid = $item['rowid'];

					if ($_FILES["file_input" . $rowid]["error"] > 0) {
						$success = false;
						$message = 'Not any file selected';
						break;
					} else {
						// $target_dir = "assets/designfiles/";
						$target_dir = DESIGNFILE_UPLOAD_DIRECTORY;
						$fileExt = pathinfo($_FILES['file_input' . $rowid]['name'], PATHINFO_EXTENSION);
						$newFilename = $order_id . "_" . ($count + 1) . "." . $fileExt;
						$uploadfile = $target_dir . $newFilename;

						if (move_uploaded_file($_FILES["file_input" . $rowid]["tmp_name"], $uploadfile)) {
							$filesArray += [$rowid => $newFilename];
						} else {
							$success = false;
							$message = "The file " . basename($_FILES["file_input" . $rowid]["name"]) . " has not been uploaded.";
							break;
						}
					}
					$count++;
				}
				if ($success == true) {
					$insertOrderProducts_Result = $this->order_model->insertOrderProducts($ord_id, $filesArray);
					if ($insertOrderProducts_Result == 0) {
						$success = false;
						$message = 'Sorry, there was an error inserting order products.';
					}
				}

				if ($success == true) {
					$this->session->set_userdata("order_id", $ord_id);
				}
				echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn, 'resposnedata' => $filesArray));
				exit();
			} else {
				$success = false;
				$message = 'Oops !! Some error occured while creating order';
				$linkn   = '';
				echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			}
			/* Create Order Reference No. End */
		}
	}

	// Find Grand Total of All Cart's Item
	function findGrandTotal()
	{
		$grand_total = sprintf("%.2f", 0);
		foreach ($this->cart->contents() as $item) {
			$grand_total += $item['prd_subtotal'];
		}
		$this->load->library('session');
		$this->session->set_userdata("grandtotal", $grand_total);
	}

	function updateItemQty()
	{
		$update = 0;

		// Get cart item info		
		$rowid = $this->input->post('cart_id');
		$qty = $this->input->post('cart_qty');
		$category = $this->input->post('cart_category');

		if (!empty($rowid) && !empty($qty)) {
			$this->load->library('cart');
			$data = array(
				'rowid' => $rowid,
				'qty'   => $qty,
			);
			$update = $this->cart->update($data);
		} else {
			$result['status'] = 'error';
			$result['message'] = validation_errors();
		}

		if ($update == 1) {
			$result['status'] = 'success';
			$result['message'] = 'Item updated...!!!';
			$result['redirect_url'] = site_url('cart');
		} else {
			$result['status'] = 'error';
			$result['message'] = 'Whoops! Something Went Wrong';
		}
		// $this->output->set_content_type('application/json');
		// $this->output->set_output(json_encode($result));
		// $string = $this->output->get_output();
		// echo $string;
		echo $update = $this->cart->update($data);
	}

	public function load($value = '')
	{
		echo $this->view();
	}

	public function remove($value = '')
	{
		$this->load->library('cart');
		$row_id = $_POST['row_id'];

		$data  = array(
			'rowid' => $row_id,
			'qty' => 0
		);
		$this->cart->update($data);
		$this->findGrandTotal();
		echo json_encode(array('message' => 'Product removed Successfully from cart'));
	}

	public function clear($value = '')
	{
		$this->load->library('cart');
		$this->cart->destroy();

		echo json_encode(array('message' => 'Cart cleared Successfully'));
	}

	public function view($value = '')
	{
		$this->_set_headers();
		$this->load->library('cart');
		return count($this->cart->contents());
	}
}
