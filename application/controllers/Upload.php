<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Upload_model');
		$this->load->model("order_model");
	}

	function index()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
			exit();
		} else {
			$data['ref'] = base_url() . 'upload_receipt';
			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['ref'] = $_SERVER['HTTP_REFERER'];
			}
			$data['title'] = "Upload Receipt | " . WEBSITE_NAME;
			$this->load->view('upload_receipt', $data);
		}
	}

	function generateOrderId()
	{
		$orderId = mt_rand(0, 1000000);
		return $orderId;
	}

	function do_upload()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '' || empty($prs_id)) {
			$success = false;
			$message = 'Login must be require !!';
			$linkn = base_url('login');
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
		} else {
			$success = true;
			$message = 'Payment reciept uploaded successfully';
			$linkn = '';

			$target_dir = RECEIPT_UPLOAD_DIRECTORY;
			$fileExt = pathinfo($_FILES['receipt_file']['name'], PATHINFO_EXTENSION);
			$paymentAmouont = $_POST['payment_amount'];
			$randomId = $this->generateOrderId();
			$receiptNumber = $prs_id . "-" . $randomId . "-" . $paymentAmouont;
			$newFilename = "receipt" . "_" . $prs_id . "_" . $randomId . "_" . $paymentAmouont . "." . $fileExt;
			$uploadfile = $target_dir . $newFilename;

			if (move_uploaded_file($_FILES["receipt_file"]["tmp_name"], $uploadfile)) {
				$filesArray = $newFilename;
			} else {
				$success = false;
				$message = "The file " . basename($_FILES["receipt_file"]["name"]) . " has not been uploaded.";
			}

			if ($success == true) {
				$uplod_data = array();
				$uplod_data['rcpt_number'] = $receiptNumber;
				$uplod_data['pay_type'] = $this->input->post('payment_type');
				$uplod_data['pay_mode'] = $this->input->post('payment_mode');
				$uplod_data['bank_name'] = $this->input->post('bank_name');
				$uplod_data['pay_amt'] = $this->input->post('payment_amount');
				$uplod_data['pay_remarks'] = $this->input->post('pay_remarks');
				$uplod_data['rcpt_img'] = $filesArray;
				$uplod_data['upload_by'] = $prs_id;
				$uplod_data['upload_date'] = date('Y-m-d H:i:s');
				$uplod_data['update_on'] = date('Y-m-d H:i:s');

				$result = $this->Upload_model->save_upload($uplod_data);
				if ($result == 0) {
					$success = false;
					$message = 'Sorry, there was an error inserting payment reciept data.';
				}
			}
			/* Send Mail */
			if ($success == true) {
				$this->Upload_model->sendMailForReceiptUploadSuccess($uplod_data);
			}
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn, 'resposnedata' => $filesArray));
		}
	}
}
