<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model("order_model");
	}



	public function index()
	{

		//load the model  
		$this->load->model('Product_model');
		//load the method of model  
		$data['dt'] = $this->select->select();
		//return the data in view  
		$this->load->view('product_detail', $data);
	}



	public function getProducts($cat_slug = false)
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) != '') {
			$data['title'] = "Products | " . WEBSITE_NAME;
			$data['category'] = $cat_slug;
			$data['products'] = $this->product_model->getProductByCategory($cat_slug);

			$this->load->view('product_list', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	public function getPricelist()
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) != '') {
			$data['title'] = "Price List | " . WEBSITE_NAME;

			$data['products'] = $this->product_model->priceList();

			$this->load->view('price_list', $data);
		} else {
			redirect('login', 'refresh');
		}
	}
	public function getProducts1($cat_slug = false)
	{
		$data['title'] = "Products | " . WEBSITE_NAME;
		$data['category'] = $cat_slug;
		$data['products'] = $this->product_model->getProductByCategory($cat_slug);

		$this->load->view('cart', $data);
	}



	// public function getProducts($cat_slug=false)
	// {
	// 	$data['title']="Products | " . WEBSITE_NAME;
	// 	if($cat_slug) {
	// 	$data['cat_data']=$this->product_model->getCatDataBySlug($cat_slug);
	// 	} else {
	// 	$data['cat_data']='';
	// 	}
	// 	$data['categories']=$this->product_model->getProductCategories(SUPPLEMENTS_MENU);
	// 	$data['menu']=SUPPLEMENTS_MENU;
	// 	$this->load->view('product_list',$data);
	// }
	public function cart()
	{
		$data['title'] = "Your Cart | " . WEBSITE_NAME;
		$this->load->view('cart', $data);
	}

	public function getAllProducts($value = '')
	{
		$_POST = json_decode(file_get_contents('php://input'), true);
		echo json_encode($this->product_model->getAllProducts($this->input->post('menu')));
	}
	public function productDetail($prdSlug, $flavour = false)

	{

		$data['product'] = $this->product_model->getProductDataBySlug($prdSlug);


		$data['title'] =  $data['product']->prd_name . " | " . WEBSITE_NAME;

		$data['relatedProducts'] = $this->product_model->getProductsByCat($data['product']->prd_cat_id, $data['product']->prd_id);
		$this->load->view('product_detail', $data);
		//$this->load->view('product_list',$data);
	}

	public function loadZoomImg()
	{
		$res = $this->product_model->getProductFlavourData($this->input->post('product_flavour'));
		print $this->ajaxLoadZoomImg($res);
	}
	function ajaxLoadZoomImg($res)
	{

		print '<img class="zoompro" src="' . base_url() . CHILD_FOLDER . PRODUCT_BIG_IMAGE_PATH . $res->pfi_img . '" data-zoom-image="' . base_url() . CHILD_FOLDER . PRODUCT_LARGE_IMAGE_PATH . $res->pfi_img . '" alt="zoom"/>';
	}
	// public function loadSmallImg()
	// {
	// 	$res= $this->product_model->getProductFlavourData($this->input->post('product_flavour'));
	// 	print $this->ajaxLoadSmallImg($res);
	// }
	// function ajaxLoadSmallImg($res){

	// 	print ' <a data-image="'.base_url().CHILD_FOLDER.PRODUCT_BIG_IMAGE_PATH.$res->pfi_img.'" data-zoom-image="'.base_url().CHILD_FOLDER.PRODUCT_LARGE_IMAGE_PATH.$res->pfi_img.'"><img  style="height: 90px;width: 90px;" src="'.base_url().CHILD_FOLDER.PRODUCT_SMALL_IMAGE_PATH.$res->pfi_img.'" alt=""></a>';
	// }
	public function getProdFlavoursById()
	{
		$prd_id = $this->input->post('prd_id');
		echo json_encode($this->product_model->getProductFlavoursById($prd_id, $this->input->post('flavour')));
	}
	public function getQuantity()
	{
		$qty = $this->input->post('qty');
		echo json_encode($this->product_model->getQuantity($qty));
	}
}
