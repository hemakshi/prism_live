<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    // if ($this->session->userdata(PROJECT_SESSION_ID))
    //   redirect('home', 'refresh');
    $this->load->model('home_model');
    $this->load->model('login_model');
    $this->load->model('person_model');
  }

  public function index()
  {
    if ($this->session->userdata(PROJECT_SESSION_ID))
      redirect('home', 'refresh');
    $this->loginView();
  }

  public function loginView()
  {
    $data['ref'] = base_url() . 'home';

    if (isset($_COOKIE[PROJECT_COOKIE_USERNAME]) and isset($_COOKIE[PROJECT_COOKIE_PASSWORD])) {
      $pos = strrpos($data['ref'], '/');
      $last = $pos === false ? $data['ref'] : substr($data['ref'], $pos + 1);
      if ($last == base_url() || $last == 'logout') {
        $data['ref'] = base_url() . 'home';
      }

      $userParams = array();
      $userParams['usr_username'] = $_COOKIE[PROJECT_COOKIE_USERNAME];
      $userParams['usr_password'] = $_COOKIE[PROJECT_COOKIE_PASSWORD];
      $person_data = $this->login_model->checkLogin($userParams);
      echo var_dump($person_data);
      /* GET WALLET DETAILS- START */
      if ($person_data && $person_data->prs_id) {
        $this->load->model('order_model');
        $getWalletDetailByPersonId_result = $this->order_model->getWalletDetailByPersonId($person_data->prs_id);
        $wallet_detail = json_decode($getWalletDetailByPersonId_result, true);
        $walletBalance = $wallet_detail['amount'] != '' ? $wallet_detail['amount'] : 0;
      }
      /* GET WALLET DETAILS- END */

      if (!empty($person_data)) {
        $newdata = array(
          PROJECT_SESSION_ID      => $person_data->prs_id,
          PROJECT_SESSION_NAME    => $person_data->prs_name,
          PROJECT_SESSION_USERNAME  => $person_data->prs_username,
          PROJECT_SESSION_MOB     => $person_data->prs_mob,
          PROJECT_SESSION_EMAIL     => $person_data->prs_email,
          PROJECT_SESSION_BALANCE  => $walletBalance,
          PROJECT_SESSION_STATUS    => $person_data->prs_status,
        );
        $this->session->set_userdata($newdata);

        redirect($data['ref']);
      } else {
        $this->load->view('login_page', $data);
      }
    } else {


      $data['title']    = 'home';

      if (isset($_SERVER['HTTP_REFERER'])) {
        $data['ref'] = $_SERVER['HTTP_REFERER'];
      }

      $pos = strrpos($data['ref'], '/');
      $last = $pos === false ? $data['ref'] : substr($data['ref'], $pos + 1);
      if ($last == base_url() || $last == 'logout' || $last == 'login' || strpos($data['ref'], 'reset-password') || strpos($data['ref'], 'account-verification') || $last == '') {
        $ref = base_url() . 'home';
      }
      $data['title'] = "Login | " . WEBSITE_NAME;
      $this->load->view('login_page', $data);
    }
  }

  public function checklogin()
  {
    $ref = $this->input->post('ref');
    $rememberme = $this->input->post('rememberme');

    if (isset($_COOKIE[PROJECT_COOKIE_USERNAME]) && isset($_COOKIE[PROJECT_COOKIE_PASSWORD])) {
      $username = get_cookie(PROJECT_COOKIE_USERNAME);
      $pwd   = get_cookie(PROJECT_COOKIE_PASSWORD);

      $_POST['usr_username'] = $username;
      $_POST['usr_password'] = $pwd;

      $value = '1';
    } else {
      $value = '0';
    }

    $arrData = array();
    $arrData['usr_username']  = $this->input->post('usr_username');
    $pwd            = $this->input->post('usr_password');

    if ($value == '0') {
      $encrypt = $this->url_encrypt->encrypt_openssl($pwd);
    } elseif ($value == '1') {
      $encrypt = $pwd;
    }

    $arrData['usr_password'] = $encrypt;
    //$user_data= $this->person_model->getpersonDataByID($arrData);
    // $arrData['usr_password'] =   $this->input->post('usr_password');
    $user_data = $this->login_model->checkLogin($arrData);

    if (isset($user_data) && $user_data != '') {
      //Start of Condition to set the coookie in the client browser
      if ($user_data->prs_mob_verification != OTP_VERIFICATION_DONE) {
        $sncrypted_mobile = $this->url_encrypt->encrypt_openssl($user_data->prs_mob);
        $sncrypted_email = $this->url_encrypt->encrypt_openssl($user_data->prs_email);
        $sncrypted_prs_id = $this->url_encrypt->encrypt_openssl($user_data->prs_id);
        $otp_screen = 'account-verification-' . $sncrypted_prs_id . '-' . $sncrypted_email;
        // redirect($otp_screen,'refresh');
        $pos = strrpos($ref, '/');

        $last = $pos === false ? $ref : substr($ref, $pos + 1);

        log_message('error', ' check login ref = ' . $ref, ' || last = ' . $last);
        if ($last == base_url() || $last == 'logout' || $last == 'login' || strpos($ref, 'reset-password') || strpos($ref, 'account-verification')) {
          $ref = base_url() . 'home';
        }
        $this->session->set_userdata('acv_redirect_url', $ref);
        $this->login_model->generateOtp($user_data->prs_id, $user_data->prs_email);

        echo json_encode(array('success' => true, 'message' => 'Please Verifiy your email Id to login', 'linkn' => $otp_screen));
        exit;
      }
      if (!isset($_COOKIE[PROJECT_COOKIE_USERNAME]) and !isset($_COOKIE[PROJECT_COOKIE_PASSWORD])) {
        if ($rememberme == 1) {
          set_cookie(PROJECT_COOKIE_USERNAME, $user_data->prs_email, 3600 * 24 * 30 * 12 * 10);
          set_cookie(PROJECT_COOKIE_PASSWORD, $user_data->prs_password, 3600 * 24 * 30 * 12 * 10);
        }
      }

      /* GET WALLET DETAILS- START */
      $this->load->model('order_model');
      $getWalletDetailByPersonId_result = $this->order_model->getWalletDetailByPersonId($user_data->prs_id);
      $wallet_detail = json_decode($getWalletDetailByPersonId_result, true);
      $walletBalance = $wallet_detail['amount'] != '' ? $wallet_detail['amount'] : 0;
      /* GET WALLET DETAILS- END */

      $newdata = array(
        PROJECT_SESSION_ID      => $user_data->prs_id,
        PROJECT_SESSION_NAME    => $user_data->prs_name,
        PROJECT_SESSION_USERNAME  => $user_data->prs_username,
        PROJECT_SESSION_MOB     => $user_data->prs_mob,
        PROJECT_SESSION_EMAIL     => $user_data->prs_email,
        PROJECT_SESSION_BALANCE  => $walletBalance,
        PROJECT_SESSION_STATUS    => $user_data->prs_status,
      );

      $this->session->set_userdata($newdata);

      $_SESSION['current_tags'] = '';

      $pos = strrpos($ref, '/');

      $last = $pos === false ? $ref : substr($ref, $pos + 1);

      log_message('error', ' check login ref = ' . $ref, ' || last = ' . $last);
      if ($last == base_url() || $last == 'logout' || $last == 'login' || strpos($ref, 'reset-password') || strpos($ref, 'account-verification')) {
        $ref = base_url() . 'home';
      }

      if (isset($_COOKIE[PROJECT_COOKIE_USERNAME]) && isset($_COOKIE[PROJECT_COOKIE_PASSWORD])) {
        // loginTransaction($this->session->userdataPROJECTSESSION_ID),TRANSACTION_LOGIN);
        echo json_encode(array("success" => true, "message" => 'Login Sucess', 'linkn' => $ref));
        exit;
      } else {
        // loginTransaction($this->session->userdataPROJECTSESSION_ID),TRANSACTION_LOGIN);
        echo json_encode(array("success" => true, "message" => 'Login Sucess', 'linkn' => $ref));
        exit;
      }
    } else {
      if (isset($_COOKIE[PROJECT_COOKIE_USERNAME]) && isset($_COOKIE[PROJECT_COOKIE_PASSWORD])) {
        return false;
      } else {
        echo json_encode(array('success' => false, 'message' => 'Username or Password does not Match.'));
        exit;
      }
    }
  }

  public function logout()
  {
    //loginTransaction($this->session->userdataPROJECTSESSION_ID),TRANSACTION_LOGOUT);
    $newdata = array(
      PROJECT_SESSION_ID      => '',
      PROJECT_SESSION_NAME    => '',
      PROJECT_SESSION_USERNAME => '',
      PROJECT_SESSION_MOB => '',
      PROJECT_SESSION_EMAIL => '',

      PROJECT_SESSION_STATUS    => '',
    );
    $this->session->unset_userdata($newdata);
    $this->session->unset_userdata(PROJECT_SESSION_ID);

    $this->session->sess_destroy();

    //unset cookie on 'logout controller'

    delete_cookie(PROJECT_COOKIE_USERNAME);
    delete_cookie(PROJECT_COOKIE_PASSWORD);

    redirect('', 'refresh');
  }

  public function forgotPasswordReset()
  {
    $data['usr_username'] = $this->input->post('prs_email');
    $prs_data = $this->login_model->checkLogin($data);
    $isSend = false;
    if (isset($prs_data) && !empty($prs_data)) {
      $fpt_data = array();
      $fpt_data['fpt_prs_id']  = $prs_data->prs_id;
      $fpt_data['fpt_code']    = $this->home_model->generateRandomStringNum(4);
      $fpt_data['fpt_status']  = ACTIVE_STATUS;
      $fpt_data['fpt_crtd_dt'] = date('Y-m-d H:i:s');
      $fpt_id = $this->home_model->insert('forgot_password_transaction', $fpt_data);

      if ($fpt_id != '-1') {
        $data = $prs_data->prs_id . '-' . $fpt_data['fpt_code'];
        $prs_data->link = base_url('reset-password-') . $this->url_encrypt->encrypt_openssl($data);
        $isSend = $this->login_model->sendMailForPasswordReset($prs_data);
      }
    }
    if ($isSend) {
      $response = array('success' => true, 'message' => 'Password reset Link has been sent to your email', 'linkn' => base_url());
    } else {
      $response = array('success' => false, 'message' => 'Sorry Email Id does not exists', 'linkn' => base_url());
    }
    echo json_encode($response);
  }
  public function person_reset_password($pswd_parameter = '', $type = '')
  {
    $data['title']    = 'Reset Password';
    if ($pswd_parameter != '') {
      $data['check'] = '0';
      $data['prs_id'] = '0';
      $data['msg'] = 'Oops !! Sorry No Password Reset Request found';
      $pswd_parameter = $this->url_encrypt->decrypt_openssl($pswd_parameter);
      if ($pswd_parameter != '') {
        $pswd_data = explode('-', $pswd_parameter);

        $fpt_prs_id = $pswd_data[0];
        $fpt_code = $pswd_data[1];
        $fpt_data = $this->login_model->getForgotpswdTransaction($fpt_prs_id, $fpt_code);
        if ($fpt_data->fpt_status == 0) {
          $data['check'] = '0';
          $data['msg'] = 'Sorry, Your link has deactivated';
          $data['prs_id'] = '';
        } else if (!empty($fpt_data)) {
          $data['check'] = '1';
          $data['msg'] = 'Link active';
          $data['prs_id'] = $fpt_prs_id;
        } else {
          $data['check'] = '0';
          $data['msg'] = 'Oops !! Sorry your link has expired';
          $data['prs_id'] = '';
        }
        if (empty($fpt_code)) {
          $data['check'] = '1';
          $data['prs_id'] = $fpt_prs_id;
        }
      }
    } else {
      $data['check'] = '0';
      $data['msg'] = 'Oops !! Sorry No Password Reset Request found';
      $data['prs_id'] = '';
    }
    $this->load->view('reset_password', $data);
  }

  public function reset_password()
  {
    $prs_id       = $this->input->post('prs_id');
    $prs_password = $this->url_encrypt->encrypt_openssl($this->input->post('prs_password'));
    $resetData    = array(
      'prs_password' => $prs_password,
    );
    $isReset      = $this->home_model->update('prs_id', $prs_id, $resetData, 'person');
    if ($isReset) {
      $success = true;
      $message = 'Password reset successfully.';
      $linkn    = base_url('login');
    } else {
      $success = false;
      $message = 'Oops !! Some error occured.';
      $linkn   = '';
    }
    echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
  }

  public function personContactValidation($type)
  {
    $pct_value = $this->input->post('value');
    $prs_id = $this->input->post('prs_id');
    switch ($type) {
      case 'mobile':
        $pct_type = 'prs_mob';
        break;
      case 'email':
        $pct_type = 'prs_email';
        break;

      default:
        $pct_type = '';
        break;
    }
    $validate =  $this->login_model->checkPersonContact($pct_type, $pct_value, $prs_id);
    // print_r($validate);
    // echo  ' $validate->count : '.$validate->count;
    if ($validate->count == 0) {
      echo 'true';
    } else {
      echo 'false';
    }
  }
  public function registerCustomer()
  {

    $prs_id      = $this->login_model->registerCustomer();
    if ($prs_id) {
      $success = true;
      $message = 'Registration successfull';
      $register_ref     = $this->input->post('ref');
      // $mob_no  = $this->input->post('prs_mob');
      // $mob_encrypt = $this->url_encrypt->encrypt_openssl($mob_no);
      $emailId  = $this->input->post('prs_email');
      $email_encrypt = $this->url_encrypt->encrypt_openssl($emailId);
      $prs_id_encrypt = $this->url_encrypt->encrypt_openssl($prs_id);
      $linkn    = base_url('account-verification-' . $prs_id_encrypt . '-' . $email_encrypt);
      /**/
      $pos = strrpos($register_ref, '/');

      $last = $pos === false ? $register_ref : substr($register_ref, $pos + 1);

      log_message('error', ' check login register_ref = ' . $register_ref, ' || last = ' . $last);
      if ($last == base_url() || $last == 'logout' || $last == 'login' || strpos($register_ref, 'reset-password') || strpos($register_ref, 'account-verification')) {
        $register_ref = base_url() . 'home';
      }
      $this->login_model->generateOtp($prs_id, $emailId);
      $this->session->set_userdata('acv_redirect_url', $register_ref);
    } else {
      $success = false;
      $message = 'Oops !! Some error occured';
      $linkn   = '';
    }
    echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
  }
  public function acc_verification($prs_id, $emailId = '')
  {
    if ($emailId != '') {
      // Check If user exists
      $prs_id = $this->url_encrypt->decrypt_openssl($prs_id);
      $emailId = $this->url_encrypt->decrypt_openssl($emailId);
      $mobCheck =  $this->login_model->checkPersonContact('prs_id', $prs_id, '');
      if ($mobCheck->count > 0) {
        $data['title'] = 'Account Verification';
        $data['prs_id'] = $mobCheck->prs_id;
        $data['prs_email'] = $emailId;
        $this->load->view('otp_verify', $data);
      }
    } else {
      redirect('404_override', 'refresh');
    }
  }
  public function sendOtp()
  {
    $prs_id = $this->input->post('prs_id');
    // $prs_mob = $this->input->post('prs_mob');
    $prs_email = $this->input->post('prs_email');
    $resend_otp_code = $this->input->post('resend_otp_code');
    if ($resend_otp_code != '') {
      $this->login_model->discard_otp_by_email($prs_email);
    }
    $otp_id = $this->login_model->generateOtp($prs_id, $prs_email);

    echo json_encode(array('success' => true, 'otp_id' => $otp_id));
  }

  public function checkOtp()
  {
    $otp_id = $this->input->post('otp_id');
    $otp_code = $this->input->post('otp_code');
    if ($otp_code != '') {
      //check OTP
      $otp_result = $this->login_model->checkOtp($otp_code);
      if ($otp_result->res_count > 0) {
        //otp verified
        $prs_id = $otp_result->otp_prs_id;
        $verificationData = array();
        $mob_update   = $this->session->userdata('acv_mobile_no_update');
        log_message('error', 'acv_mobile_no_update : ' . $mob_update);
        if ($mob_update && $mob_update != '') {
          $verificationData['prs_mob'] = $otp_result->otp_mob;
        }
        $verificationData['prs_mob_verification'] = OTP_VERIFICATION_DONE;
        $verificationUpdate      = $this->home_model->update('prs_id', $prs_id, $verificationData, 'person');
        $this->session->unset_userdata('acv_mobile_no_update');

        $discard_otp = $this->login_model->discard_otp($otp_result->otp_mob);
        $success = true;
        $message = 'Account Verification successfull';
        $userdata = $this->person_model->getUserDataByMob($otp_result->otp_mob);
        $prsdata = array(
          PROJECT_SESSION_ID        => $userdata->prs_id,
          PROJECT_SESSION_NAME      => $userdata->prs_name,
          PROJECT_SESSION_USERNAME  => $userdata->prs_username,
          PROJECT_SESSION_MOB       => $userdata->prs_mob,
          PROJECT_SESSION_EMAIL     => $userdata->prs_email,

          PROJECT_SESSION_STATUS    => $userdata->prs_status,
        );

        $this->session->set_userdata($prsdata);
        $linkn   = $this->session->userdata('acv_redirect_url');
        $last = strrpos($linkn, '/');
        log_message('error', ' acv_redirect_url ref = ' . $linkn, ' || last = ' . $linkn);
        if ($last == base_url() || $last == 'logout' || $last == 'login' || strpos($linkn, 'reset-password') || strpos($linkn, 'account-verification') || $last == '') {
          $linkn = base_url() . 'home';
        }
        log_message('error', 'acv_redirect_url : ' . $linkn);

        $this->session->unset_userdata('acv_redirect_url');
      } else {
        //otp mismatch
        $success = false;
        $message = 'Oops !! Sorry you entered wrong OTP .';
        $linkn   = '';
      }
    } else {
      $success = false;
      $message = 'Oops !! Sorry some error occured .';
      $linkn   = '';
    }
    echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
  }
}
