<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Person extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('person_model');
    $this->load->model('login_model');
    $this->load->model("order_model");
  }
  public function personAddressform()
  {

    $pad_id      = $this->person_model->updatePersonAddress();
    if ($pad_id) {
      $success = true;
      $message = 'Address Saved successfully';
      $linkn    = '';
    } else {
      $success = false;
      $message = 'Oops !! Some error occured';
      $linkn   = '';
    }
    echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
  }
  public function getUserData()
  {
    $prs_id = $this->session->userdata(PROJECT_SESSION_ID);
    $data = $this->person_model->getUserData($prs_id);
    echo json_encode($data);
  }
  public function updatePerson()
  {
    $prs_id = $this->person_model->updatePerson();
    if ($prs_id) {
      $success = true;
      $linkn   = '';
      $message = 'Profile Updated successfull';
      $mob_no       = $this->input->post('prs_mob');
      $prs_old_mob  = $this->input->post('prs_old_mob');
      $ref          = $this->input->post('ref');
      $person_id    = $this->input->post('prs_id');

      if ($prs_old_mob != $mob_no) {
        $mob_encrypt  = $this->url_encrypt->encrypt_openssl($mob_no);
        $prs_id_encrypt  = $this->url_encrypt->encrypt_openssl($person_id);
        $pos = strrpos($ref, '/');

        $last = $pos === false ? $ref : substr($ref, $pos + 1);

        log_message('error', ' check login ref = ' . $ref, ' || last = ' . $last);
        if ($last == base_url() || $last == 'logout' || $last == 'login' || strpos($ref, 'reset-password') || strpos($ref, 'account-verification')) {
          $ref = base_url() . 'dashboard';
        }
        $this->session->set_userdata('acv_redirect_url', $ref);
        $this->session->set_userdata('acv_mobile_no_update', true);
        $otp_id = $this->login_model->generateOtp($person_id, $mob_no);
        $linkn    = base_url('account-verification-' . $prs_id_encrypt . '-' . $mob_encrypt);
      } else {
        $linkn    = base_url() . 'profile';
      }
      $prsdata  = array(
        PROJECT_SESSION_NAME        => $this->input->post('prs_name'),
        PROJECT_SESSION_MOB         => $this->input->post('prs_mob'),
        // PROJECT_SESSION_COMPNAME      => $this->input->post('comp_name'),
        // PROJECT_SESSION_EMAIL       => $this->input->post('prs_email'),
        PROJECT_SESSION_STATUS      => ACTIVE_STATUS
      );

      $this->session->set_userdata($prsdata);
    } else {
      $success = false;
      $message = 'Oops !! Some error occured';
      $linkn   = '';
    }
    echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
  }

  public function validatePersonPassword()
  {
    $data = array(
      'prs_id' => $this->input->post('prs_id'),
      'old_password' => $this->url_encrypt->encrypt_openssl($this->input->post('old_password'))
    );

    $validate =  $this->person_model->checkPersonPassword($data);
    if ($validate->count == 1) {
      echo 'true';
    } else {
      echo 'false';
    }
  }

  public function change_password()
  {
    $prs_id       = $this->input->post('prs_id');
    $prs_password = $this->url_encrypt->encrypt_openssl($this->input->post('prs_password'));
    $changeData    = array(
      'prs_password' => $prs_password,
    );
    $isReset      = $this->home_model->update('prs_id', $prs_id, $changeData, 'person');
    if ($isReset) {
      set_cookie(PROJECT_COOKIE_PASSWORD, $prs_password, 3600 * 24 * 30 * 12 * 10);
      $success = true;
      $message = 'Password changed successfully';
      $linkn    = '';
    } else {
      $success = false;
      $message = 'Oops !! Some error occured';
      $linkn   = '';
    }
    echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
  }
}
