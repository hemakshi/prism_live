<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('person_model');
		$this->load->model('order_model');
		// Load cart library
		$this->load->library('cart');
	}
	public function address_select()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		if ($prs_id == '') {
			redirect('login', 'refresh');
		}
		$data['title'] = "Prism  | Address Select";
		$data['person_address'] = $this->person_model->getPersonAddress($prs_id);
		$data['default_address'] = $this->person_model->getPersonDefaultAddress($prs_id);
		$this->load->view('address_select', $data);
	}
	public function selectAddress($value = '')
	{
		$ord_adress_id = $this->input->post('ord_adress_id');
		$this->session->set_userdata('selected_address', $ord_adress_id);
		echo json_encode(array("success" => true, 'linkn' => base_url() . 'order-review'));
	}
	public function order_review()
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$this->load->library('cart');
		$cart_count = count($this->cart->contents());
		$pad_id = $this->session->userdata('selected_address');
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		$ord_grand_total = $this->session->userdata('grandtotal');
		$wallet_bal = 0;
		if (!empty($prs_id) && $prs_id != '') {
			$getWalletDetailByPersonId_result = $this->order_model->getWalletDetailByPersonId($prs_id);
			$wallet_detail = json_decode($getWalletDetailByPersonId_result, true);
			$wallet_bal = $wallet_detail['amount'];
		}

		if ($cart_count == '0' || $cart_count == '') {
			redirect('home');
		}
		if ($pad_id == '') {
			redirect('address-select');
		}
		$data['title'] = "Order Review | " . WEBSITE_NAME;
		$data['wallet_balance'] = $wallet_bal;
		$data['ord_grand_total'] = $ord_grand_total;
		$data['shipping_address'] = $this->person_model->getPersonAddressById($pad_id);
		$this->load->view('order_review', $data);
	}

	/* Call this function when click on Pay Now button in Order Review Page */
	public function place_order()
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$this->session->unset_userdata('order');
		$shippingAddressData = $this->person_model->getPersonAddressById($this->input->post('ord_deliver_address'));
		$order_id = $this->session->userdata('order_id');
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);

		if (empty($order_id) || empty($prs_id) || empty($shippingAddressData)) {
			$success = false;
			$message = 'Some order details not found.';
			$linkn   = '';
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		}

		$orderDetails = $this->order_model->getOrderData($order_id);

		if (empty($orderDetails)) {
			$success = false;
			$message = 'Order details not found.';
			$linkn   = '';
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		}
		$walletData = json_decode($this->order_model->getWalletDetailByPersonId($prs_id), true);
		if ($walletData == '' || empty($walletData)) {
			$success = false;
			$message = 'Wallet data not found.';
			$linkn   = '';
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		}
		if ($orderDetails->ord_total_amt > $walletData['amount']) {
			$success = false;
			$message = 'Insufficient wallet balance.';
			$linkn   = '';
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		}


		$payFromBalanceButton_result = $this->order_model->payFromBalanceButton($order_id, $prs_id, $shippingAddressData);

		$success = true;
		$message = 'Payment successful';
		$linkn   = '';
		if ($payFromBalanceButton_result == false) {
			$success = false;
			$message = 'Oops !! Some error occured while payment';
			$linkn   = '';
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		}

		if ($success == true) {
			$this->order_model->sendOrderSuccessMail($orderDetails->ord_reference_no);
			$this->cart->destroy();
			$this->session->unset_userdata('grandtotal');
			$this->session->set_userdata('order', 'success');
		}

		// echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
	}

	public function deleteIncompleteOrderOfCurrentUser()
	{
		$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
		$deleteIncompleteOrderOfCurrentUser_result = $this->order_model->deleteIncompleteOrderOfCurrentUser($prs_id);

		$success = true;
		$message = 'Order Placed Successfully.';
		$linkn   = base_url('order_history');
		if ($deleteIncompleteOrderOfCurrentUser_result == false) {
			$success = false;
			$message = 'Oops !! Some error occured while deleting Incomplete order';
			$linkn   = '';
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
		}
		echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
	}

	/*public function place_order()
	{

		$this->session->unset_userdata('order');
		$ord_reference_no         = $this->home_model->getNewCode(
			array(
				'column'	=> 'ord_reference_no',
				'table'		=> 'person_order',
				'type'		=> 'order_code',
				'where'		=> ''
			)
		);
		$addressData = $this->person_model->getPersonAddressById($this->input->post('ord_deliver_address'));
		$ord_id = $this->order_model->place_order($ord_reference_no, $addressData->pad_mobile);

		if ($ord_id != '-1') {
			$ord_payment_mode = $this->input->post('ord_payment_mode');

			$success = true;
			$message = 'Order Placed successfully';
			$ord_encrypted_id = $this->url_encrypt->encrypt_openssl($ord_id);
			$ord_reference_no_encrypted = $this->url_encrypt->encrypt_openssl($ord_reference_no);
			// Destroy cart
			$this->load->library('cart');
			$this->cart->destroy();
			// Destroy cart

			if ($ord_payment_mode == PAYMENT_TYPE_PAYTM) {
				$linkn    = base_url() . 'paytm/pay/' . $ord_encrypted_id;
			} else {
				$receiver_data = array(
					'email' => $this->session->userdata(PROJECT_SESSION_EMAIL),
					'name' =>   $addressData->pad_name,
					'mobile' =>   $addressData->pad_mobile,
					'alt_mobile' =>    $addressData->pad_alt_phone
				);
				// $this->communication_model->communication('order_placed', $receiver_data,$this->order_model->getOrderDetail($ord_reference_no)); 
				$this->order_model->sendOrderSuccessMail($ord_reference_no);
				$mail = $this->session->set_userdata('order', 'success');
				$linkn = base_url('my-orders');
			}
		} else {
			$success = false;
			$message = 'Oops !! Some error occured while placing order';
			$linkn   = '';
		}
		echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
	}*/

	public function order_summary($ord_reference_encrypted_id)
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$data['title'] = "Order Summary | " . WEBSITE_NAME;
		$ord_reference_no = $this->url_encrypt->decrypt_openssl($ord_reference_encrypted_id);

		$data['order'] = $this->order_model->getOrderData($ord_reference_no, 'ord_reference_no');
		$data['order_address'] = $this->person_model->getPersonAddressById($data['order']->ord_delivery_adddress);
		$data['order_products'] = $this->order_model->getOrderProducts($data['order']->ord_id);
		$this->load->view('order_summary', $data);
	}
	public function getnewCode()
	{
		echo 'ord_reference_no :' . $ord_reference_no         = $this->home_model->getNewCode(
			array(
				'column'	=> 'ord_reference_no',
				'table'		=> 'person_order',
				'type'		=> 'order_code',
				'where'		=> ''
			)
		);
	}
	public function order_list()
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$this->load->model('order_model');
		if ($this->session->userdata('order')) {
			$data['order'] = $this->session->userdata('order');
		} else {
			$data['order'] = '';
		}
		$data['title'] = "Order History | " . WEBSITE_NAME;
		$data['orders'] = $this->order_model->getOrderListData();
		$data['orderProducts'] = $this->order_model->getOrderAllProducts();
		// $data['orderProductStatus']=$this->order_model->getOrderAllProductsStatus();
		$this->load->view('order_list', $data);
	}
	public function orderHistory()
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$this->load->model('order_model');
		if ($this->session->userdata('order')) {
			$data['order'] = $this->session->userdata('order');
		} else {
			$data['order'] = '';
		}
		$data['title'] = " Order History | " . WEBSITE_NAME;
		$data['orders'] = $this->order_model->getOrderListData();
		// $data['orderProducts'] = $this->order_model->getOrderAllProducts();
		// $data['orderProductStatus']=$this->order_model->getOrderAllProductsStatus();
		$this->load->view('order_history', $data);
	}
	public function orderDetail($ord_reference_no)
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$data['title'] = "OrderId#" . $ord_reference_no . "| " . WEBSITE_NAME;
		$data['order'] = $this->order_model->getOrderDetail($ord_reference_no);
		if (!empty($data['order'])) {
			$data['orderProducts'] = $this->order_model->getOrderAllProducts($data['order']->ord_id);
			$data['orderProductStatus'] = $this->order_model->getOrderAllProductsStatus();
		}

		$this->load->view('order_detail', $data);
	}

	public function generateCustomerInvoice($ord_reference_no)
	{
		if ($this->session->userdata(PROJECT_SESSION_ID) == '') {
			redirect('login', 'refresh');
		}

		$data['order'] = $this->order_model->getOrderDetail($ord_reference_no);
		if (!empty($data['order'])) {
			$data['orderProducts'] = $this->order_model->getOrderAllProducts($data['order']->ord_id);
		}
		// print_r($data['order']);
		// print_r($data['orderProducts']);
		$this->load->view('invoice', $data);
	}
}
