<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');	
		$this->load->model("order_model");
	}
	public function getShopProducts($cat_slug=false)
	{
		$data['title']="TSN | Products";
		if($cat_slug) {
		$data['cat_data']=$this->product_model->getCatDataBySlug($cat_slug);
		} else {
		$data['cat_data']='';
		}
		$data['categories']=$this->product_model->getProductCategories(SHOP_MENU);
		$data['menu']=SHOP_MENU;
		$this->load->view('product_list',$data);
	}
public function getAllProducts($value='')
	{
		$_POST = json_decode(file_get_contents('php://input'), true);
		echo json_encode($this->product_model->getAllProducts(SUPPLEMENTS_MENU));
	}
	
}