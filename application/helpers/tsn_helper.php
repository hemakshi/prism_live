<?php

function getDropdownResult($type, $gpm_group, $fieldName, $value = false, $customElements = '')
{
  $CI = &get_instance();
  $str = '';
  switch ($type) {
    case 'dropdown':
      $result = $CI->home_model->getGenPrmResultByGroup($gpm_group);
      foreach ($result as $resultkey) {
        $selected = '';
        if ($value) {
          if ($value == $resultkey->f1) {
            $selected = 'selected="selected"';
          }
        }
        $str .= '<option value="' . $resultkey->f1 . '"' . $selected . '>' . $resultkey->f2 . '</option>';
      }
      break;

    case 'radio':
      $result = $CI->home_model->getGenPrmResultByGroup($gpm_group);
      foreach ($result as $resultkey) {
        $selected = '';
        if ($value) {
          if ($value == $resultkey->f1) {
            $selected = 'checked="checked"';
          }
        }

        $str .= '<input type="radio" data-name="' . $fieldName . '" name="' . $fieldName . '" value="' . $resultkey->f1 . '" ' . $selected . '>&nbsp;&nbsp' . $resultkey->f2 . ' &nbsp;&nbsp';
      }
      break;

    case 'state-dropdown':
      $result = $CI->home_model->getStateDropdown($gpm_group);
      foreach ($result as $resultkey) {
        $selected = '';
        if ($value) {
          if ($value == $resultkey->f1) {
            $selected = 'selected="selected"';
          }
        }

        $str .= '<option value="' . $resultkey->f1 . '"' . $selected . '>' . $resultkey->f2 . '</option>';
      }
      break;

    case 'compaints-desc-dropdown':
      $result = $CI->home_model->getCompaintsDropdown();
      foreach ($result as $resultkey) {
        $selected = '';
        if ($value) {
          if ($value == $resultkey->f1) {
            $selected = 'selected="selected"';
          }
        }
        $str .= '<option value="' . $resultkey->f1 . '"' . $selected . '>' . $resultkey->f2 . '</option>';
      }
      break;

    default:
      $str = '';
      break;
  }

  return $str;
}
