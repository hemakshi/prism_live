<?php
/*

- Use PAYTM_ENVIRONMENT as 'PROD' if you wanted to do transaction in production environment else 'TEST' for doing transaction in testing environment.
- Change the value of PAYTM_MERCHANT_KEY constant with details received from Paytm.
- Change the value of PAYTM_MERCHANT_MID constant with details received from Paytm.
- Change the value of PAYTM_MERCHANT_WEBSITE constant with details received from Paytm.
- Above details will be different for testing and production environment.

*/

define('PAYTM_ENVIRONMENT', 'TEST'); // PROD
define('PAYTM_MERCHANT_KEY', 'aMii4TBZI@i6!Hg5'); //Change this constant's value with Merchant key received from Paytm.
define('PAYTM_MERCHANT_MID', 'MoCVfk40813527929102'); //Change this constant's value with MID (Merchant ID) received from Paytm.
define('PAYTM_MERCHANT_WEBSITE', 'WEBSTAGING'); //Change this constant's value with Website name received from Paytm.

$PAYTM_STATUS_QUERY_NEW_URL='https://securegw-stage.paytm.in/merchant-status/getTxnStatus';
$PAYTM_TXN_URL='https://securegw-stage.paytm.in/theia/processTransaction';
if (PAYTM_ENVIRONMENT == 'PROD') {
	$PAYTM_STATUS_QUERY_NEW_URL='https://securegw.paytm.in/merchant-status/getTxnStatus';
	$PAYTM_TXN_URL='https://securegw.paytm.in/theia/processTransaction';
}

define('PAYTM_REFUND_URL', '');
define('PAYTM_STATUS_QUERY_URL', $PAYTM_STATUS_QUERY_NEW_URL);
define('PAYTM_STATUS_QUERY_NEW_URL', $PAYTM_STATUS_QUERY_NEW_URL);
define('PAYTM_TXN_URL', $PAYTM_TXN_URL);

// define('PAYTM_ENVIRONMENT', 'PROD'); // TEST
// define('PAYTM_MERCHANT_KEY', 'gZ6jmUZ5&uScbAn1'); //Change this constant's value with Merchant key downloaded from portal - sNW&4Opx#Ie5iV2!
// define('PAYTM_MERCHANT_MID', 'DaMusc17225972213472'); //Change this constant's value with MID (Merchant ID) received 
// define('PAYTM_MERCHANT_WEBSITE', 'WEBPROD'); //Change this constant's value with Website name received from Paytm

// $PAYTM_DOMAIN = "securegw-stage.paytm.in";
// if (PAYTM_ENVIRONMENT == 'PROD') {
// 	$PAYTM_DOMAIN = 'securegw.paytm.in';
// }
// define('PAYTM_REFUND_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/REFUND');
// define('PAYTM_STATUS_QUERY_URL', 'https://'.$PAYTM_DOMAIN.'/merchant-status/getTxnStatus');
// define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus');
// define('PAYTM_TXN_URL', 'https://'.$PAYTM_DOMAIN.'/theia/processTransaction');

?>
