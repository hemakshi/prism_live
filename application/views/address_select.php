   <?php $this->load->view('header'); ?>
   <!-- checkout-area start -->
   <div class="checkout-area pb-80 pt-100">
     <div class="container">

       <!-- 		<h6><a data-toggle="modal" href="#person_address_modal" onclick="return clearData();" class="btn btn-add"> <i class="fa fa-plus"> Add</i></a></h6> -->
       <div class="add-address-div">
         <a class="place-order" data-toggle="modal" href="#person_address_modal" onclick="return clearData();" class="btn btn-add">Add new address</a>
         <a class="place-order" href="#" onclick="return selectAddress();" class="btn btn-add">Continue</a>

       </div>

       <div class="row">

         <?php foreach ($person_address as $person_address_key) { ?>
           <div class="col-md-4 col-sm-12 col-lg-4">
             <?php
              if ($person_address_key->pad_id == $default_address) {
                echo ' <div class="address-row selected" onclick="return updateSelectedAddress(this,' . $person_address_key->pad_id . ')">';
              } else {
                echo ' <div class="address-row" onclick="return updateSelectedAddress(this,' . $person_address_key->pad_id . ')">';
              }
              ?>
             <button class="select"></button>
             <div class="name">

               <span class="cust-name"> <?php echo $person_address_key->pad_name; ?></span>
               <!-- <span class="lbl-default-address"> (Default)</span> -->
               <span class="address-type"><?php echo $person_address_key->address_type; ?></span></div>
             <div class="address">
               <div class="address-content"><span class="address-field lbl"><?php echo $person_address_key->pad_address; ?>
               </div>
               <div><?php echo $person_address_key->pad_locality; ?></div>
               <div><?php echo $person_address_key->pad_city; ?>- <?php echo $person_address_key->pad_pincode; ?></div>
               <div><?php echo $person_address_key->state_name; ?></div>
               <div>Mobile: <?php echo $person_address_key->pad_mobile; ?></div>
               <?php
                if ($person_address_key->pad_landmark != '') {
                  echo '<div>Landmark: ' . $person_address_key->pad_landmark . '</div>';
                }
                if ($person_address_key->pad_alt_phone != '') {
                  echo '<div>Alternate Mobile: ' . $person_address_key->pad_alt_phone . '</div>';
                }
                ?>
             </div>
             <div class="side-menu"><button class="tappable delete">Remove<span class="icon"></span></button><button class="tappable edit" data-pad_id="<?php echo $person_address_key->pad_id; ?> " data-address_type="<?php echo $person_address_key->pad_address_type; ?> " data-address_type_name="<?php echo $person_address_key->address_type; ?> " data-pad_name="<?php echo $person_address_key->pad_name; ?> " data-pad_mobile="<?php echo $person_address_key->pad_mobile; ?> " data-pad_pincode="<?php echo $person_address_key->pad_pincode; ?> " data-pad_locality="<?php echo $person_address_key->pad_locality; ?> " data-pad_address="<?php echo $person_address_key->pad_address; ?> " data-pad_city="<?php echo $person_address_key->pad_city; ?> " data-state="<?php echo $person_address_key->pad_state; ?> " data-state_name="<?php echo $person_address_key->state_name; ?> " data-pad_city="<?php echo $person_address_key->pad_city; ?> " data-pad_landmark="<?php echo $person_address_key->pad_landmark; ?>" data-pad_alt_phone="<?php echo $person_address_key->pad_alt_phone; ?>" data-toggle="modal" href="#person_address_modal" onclick="return updateFormData(this)">Edit</button></div>
             <!-- <button class="select"></button> -->
           </div>
       </div>
     <?php } ?>

     </div>
   </div>
   </div>
   </div>
   <div class="modal fade" id="person_address_modal" tabindex="-1" role="basic" aria-hidden="true">
     <div class="modal-dialog modal-full">
       <div class="modal-content">
         <form id="person_address_form">
           <input type="hidden" name="selected_address" id="selected_address" value="<?php echo $default_address ?>">
           <div class="modal-header">
             <h4 class="modal-title">Add new address</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

           </div>
           <div class="modal-body">
             <div class="row">
               <input type="hidden" name="pad_prs_id" id="pad_prs_id" value="<?php echo $this->session->userdata(PROJECT_SESSION_ID); ?>">
               <input type="hidden" name="pad_id" id="pad_id" value="">
               <!-- 	<div class="col-md-6 col-lg-6">
	                				<div class="contact-form-style mb-20">
		                				<label class="control-label">Type</label>
		                				<select class="form-control" id="pad_address_type" name="pad_address_type" data-msg="Please Select Address Type">
		                					<?php echo getDropdownResult('dropdown', 'address_type', 'pad_address_type'); ?>
		                				</select>
	                				</div>
	                			</div> -->
               <div class="col-md-6">
                 <div class="contact-form-style mb-10">
                   <label>Name. *</label>
                   <input id="pad_name" name="pad_name" type="text" data-msg="Please enter name" required="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="contact-form-style mb-10">
                   <label>Mobile Number *</label>
                   <input id="pad_mobile" name="pad_mobile" type="text" data-msg="Please enter mobile number" required="">
                 </div>
               </div>

               <div class="col-md-6">
                 <div class="contact-form-style mb-10">
                   <label>Pincode *</label>
                   <input id="pad_pincode" name="pad_pincode" type="text" data-msg="Please enter pincode" required="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="contact-form-style mb-10">
                   <label>Locality *</label>
                   <input type="text" id="pad_locality" name="pad_locality" data-msg="Please enter locality" required="">
                 </div>
               </div>

               <div class="col-md-12">
                 <div class="contact-form-style mb-10">
                   <label>Address (Area and street) <span class="asterix">*</span></label>
                   <textarea required name="pad_address" id="pad_address" placeholder="" data-msg="Please enter address" required=""></textarea>
                 </div>
               </div>
               <!-- <div class="col-md-6">
	                				<div class="form-group">
		                				<label class="control-label">Location</label>
		                				<input id="pad_location_id" name="pad_location_id" type="text" data-msg="Please Enter location" required="">
		                			</div>
	                			</div> -->
               <div class="col-md-12">
                 <div class="contact-form-style mb-10">
                   <label>City, District, Town <span class="asterix">*</span></label>
                   <input type="text" id="pad_city" name="pad_city" data-msg="Please enter city" required="">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="contact-form-style mb-10">
                   <label>State <span class="asterix">*</span></label>
                   <select id="pad_state" name="pad_state" data-msg="Please select state" required="">
                     <?php echo getDropdownResult('state-dropdown', 'pad_state', 'pad_state'); ?>
                   </select>

                 </div>
               </div>

               <div class="col-md-6">
                 <div class="contact-form-style mb-10">
                   <label>Landmark</label>
                   <input id="pad_landmark" name="pad_landmark" type="text">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="contact-form-style mb-10">
                   <label>Alternate Phone</label>
                   <input id="pad_alt_phone" name="pad_alt_phone" type="text">
                 </div>
               </div>
               <!-- <div class="col-md-12">
                 <div class="contact-form-style mb-10">
                   <label>Address type</label>
                   <select id="pad_address_type" name="pad_address_type" data-msg="Please Select Address Type">
                     <?php echo getDropdownResult('dropdown', 'address_type', 'pad_address_type'); ?>
                   </select>
                 </div>

               </div> -->
             </div>
           </div>
           <div class="modal-footer">
             <div class="button-box">
               <button type="button" data-dismiss="modal" class="btn-cancel">Cancel</button>
               <button type="submit" id="btn-save">Save</button>
             </div>
           </div>
         </form>
       </div>
       <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
   </div>
   <!-- Footer style Start -->
   <?php $this->load->view('footer'); ?>
   <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/form_validation/person_address.js"></script>
   </body>

   </html>