<?php $this->load->view('header'); ?>

<div class="container pb-100">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('ledger') ?>">Ledger</a></li>
    </ul>
  </div>
  <div class="text-center">
    <h3 style="padding-bottom: 10px;
    padding-top: 10px; color: white; background-color: #000">Ledger</h3>
  </div>
  <table class="table table-hover table-responsive-sm table-responsive-md" id="dTableWithBtn" style="width: 100%; background-color: #efefef !important;">
    <thead>
      <tr>
        <th>Sr.No</th>
        <th>Date</th>
        <th>Trans. Type </th>
        <th>Trans. Mode</th>
        <th>Journal Name</th>
        <th>Credit</th>
        <th>Debit</th>
        <th>Balance</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (!empty($ledgerData) && $ledgerData != '') {
        $srNo = 1;
        foreach ($ledgerData as $ledger) {
      ?>
          <tr>
            <td><?php echo $srNo; ?></td>
            <td><?php echo date("d-m-Y H:i:s", strtotime($ledger->created_on)); ?></td>
            <td> <?php
                  switch ($ledger->type) {
                    case CREDIT:
                      echo "Credit";
                      break;
                    case DEBIT:
                      echo "Debit";
                      break;
                    default:
                      echo "-";
                      break;
                  }
                  ?>
            </td>
            <td> <?php
                  switch ($ledger->transaction_mode) {
                    case BANK:
                      echo "Bank";
                      break;
                    case CASH:
                      echo "Cash";
                      break;
                    case PAY_FROM_BALANCE:
                      echo "Pay From Balance";
                      break;
                    case PAYMENT_GATEWAY:
                      echo "Payment Gateway";
                      break;
                    case TRANSACTION_MODE_WALLET:
                      echo "Wallet";
                      break;
                    default:
                      echo "-";
                      break;
                  }
                  ?>
            </td>
            <td><?php echo $ledger->description; ?></td>
            <td><?php echo $ledger->type == CREDIT ? '<i class="fa fa-rupee"></i> ' . $ledger->amount : ''; ?></td>
            <td><?php echo $ledger->type == DEBIT ? '<i class="fa fa-rupee"></i> ' . $ledger->amount : ''; ?></td>
            <td><?php echo '<i class="fa fa-rupee"></i> ' . $ledger->closing_balance; ?></td>
          </tr>
        <?php
          $srNo++;
        }
      } else {
        ?>
        <tr>
          <td colspan="8" class="text-center">No data available in table</td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<script>
  $(document).ready(function() {
    $('#dTableWithBtn').DataTable({
      dom: 'Blfrtip',
      buttons: {
        buttons: ['print','csv'],
        className: "btn btn-danger"
      }      
    });
  });
</script>

</body>

</html>