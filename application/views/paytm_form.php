<?php $this->load->view('header'); ?>
<?php
$prs_id = $this->session->userdata(PROJECT_SESSION_ID);
if ($prs_id == '') {
	redirect('login', 'refresh');
}
$paymentOrderResponse = $this->session->userdata('payment_order');
$this->session->unset_userdata('payment_order');
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
	<meta name="GENERATOR" content="Evrsoft First Page">
</head>

<!-- Content Start -->
<div class="about-us-area  pb-100">
	<div class="container">
		<div class="breadcrumb-content">
			<ul>
				<li><a href="<?php echo site_url('home') ?>">Home</a></li>
				<li class="active">Add Balance</li>
			</ul>
		</div>
		<?php		
		if ($paymentOrderResponse && $paymentOrderResponse == 'success') {
			echo '<div class="alert alert-success">
  						Your transaction has been completed successfully.
					</div>';
		}
		if ($paymentOrderResponse && $paymentOrderResponse == 'failed') {
			echo '<div class="alert alert-danger">
						Transaction failed.Please try again
					</div>';
		}
		?>
		<div class="row">
			<!-- Add Balance Form Area Start -->
			<div class="col-lg-6">
				<div class="small-title mb-30">
					<h2>Add Balance Form</h2>
					<p></p>
					<div>
						<label class="fz16">Current Balance:</label>
						<label> Rs. <?php echo $wallet_balance; ?> </label>
					</div>
				</div>
				<!-- action="<?php echo site_url('paytm/paytmpost') ?>" -->
				<form id="add_balance_form" method="post" action="<?php echo site_url('paytm/paytmpost') ?>">
					<div class="row">
						<input type="hidden" id="ORDER_ID" maxlength="20" size="20" name="ORDER_ID" value="<?php echo  "ORDS" . $prs_id . rand(10000, 99999999) ?>">
						<input type="hidden" id="CUST_ID" maxlength="12" size="12" name="CUST_ID" value="<?php echo "CUST00" . $prs_id ?>">
						<input type="hidden" id="INDUSTRY_TYPE_ID" maxlength="12" size="12" name="INDUSTRY_TYPE_ID" value="Retail">
						<input type="hidden" id="CHANNEL_ID" maxlength="12" size="12" name="CHANNEL_ID" value="WEB">
						<input type="hidden" id="MSISDN" name="MSISDN" value="<?php echo  $customer_mobile; ?>">
						<input type="hidden" id="EMAIL" name="EMAIL" value="<?php echo  $customer_email; ?>">

						<div class="col-lg-12">
							<div class="contact-form-style mb-20">
								<input type="text" name="TXN_AMOUNT" id="TXN_AMOUNT" required placeholder="Amount *" title="Amount" tabindex="1" data-msg="Please Enter Amount">
							</div>
						</div>
						<div class="col-lg-12">
							<div class="contact-form-style">
								<textarea name="REMARKS" required placeholder="Remarks *" title="Remarks" tabindex="2" data-msg="Please Enter Remarks"></textarea>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="contact-form-style">
								<button value="CheckOut" type="submit" id="SUBMIT" name="SUBMIT" tabindex="3">ADD BALANCE</button>
							</div>
						</div>
					</div>
				</form>
				<p class="form-messege"></p>
			</div>

			<div class="col-lg-6">
				<img class="img-responsive" src="assets/img/add-money1.png">
	</div>
			<!-- Add Balance Form Area End -->
		</div>
	</div>
</div>
<!-- Content End -->
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/add_balance.js"></script>

<script type="text/javascript">
	// Add the following code if you want the name of the file appear on select
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
</script>
</body>

</html>