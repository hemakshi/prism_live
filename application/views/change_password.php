   <?php $this->load->view('header'); ?>
   <style type="text/css">
   	.login-form-container label,
   	#not-match-error,
   	.contact-form-style {
   		font-size: 16px;
   	}

   	.errormesssage {
   		font-size: 12px !important;
   		margin-bottom: 0px;
   	}

   	.login-form-container input {
   		height: 36px !important;
   		font-size: 18px !important;
   		margin-bottom: 0px !important;
   	}

   	.contact-form-style>button {
   		margin: 5px 0 0 !important;
   	}

   	h3.page-title {
   		font-size: 25px;
   	}

   	/* .login-form-container {
   		padding: 30px 50px 30px 50px;
   	} */

   	.contact-form-style>button {
   		padding: 14px 27px 25px;
   	}

   	.btn:not(:disabled):not(.disabled) {
   		color: white;
   	}

   	.color-red {
   		color: #d00;
   	}
   </style>
   <div class="portlet">
   	<div class=" row mr-0 ml-0"  >


   		<div class="container-fluid login-form-container">
   			<div class="text-center title_wrap ">
   				<h3 class="page-title text-center">Change Password</h3>
   				<span class="sp_line color-primary">
   				</span>
   			</div>
   			<div class="login-form-container forget_pass_form_div">
   				<div class="col-lg-7 col-md-12 ml-auto mr-auto">
   					<form class="" id="change_password_form" method="post" autocomplete="off">
   						<input type="hidden" name="prs_id" id="prs_id" value="<?php echo $userData->prs_id; ?>">
   						<div class="row">

   							<div class="col-md-12">
   								<div class="form-group">
   									<label class="text-dark">Old Password
   										<span class="asterix-error"><em class="color-red">* </em> </span>
   									</label>
   									<input type="password" name="old_password" id="old_password" class="form-control" value="" data-msg="Please Enter Old Password">
   								</div>
   							</div>

   							<div class="col-md-12">
   								<div class="form-group">
   									<label class="text-dark">New Password
   										<span class="asterix-error"><em class="color-red">* </em> </span>
   									</label>
   									<input type="password" name="new_password" id="new_password" class="form-control" value="" data-msg="Please Enter New Password">
   								</div>
   							</div>

   							<div class="col-md-12">
   								<div class="form-group">
   									<label class="text-dark">Confirm Password
   										<span class="asterix-error"><em class="color-red">* </em> </span>
   									</label>
   									<input type="password" name="prs_confirm_pass" id="prs_confirm_pass" class="form-control" value="" data-msg="Please Enter Confirm Password">
   									<div class="help-block">
   									</div>
   								</div>
   							</div>

   						</div>
   						<div class="form-actions contact-form-style">
   							<button type="submit" class="btn btn_save">Submit&nbsp;<i class="fa fa-check"></i></button>
   							<button type="button" class="btn btn_processing" style="display: none;"><i class='fa fa-spinner'></i> Submitting in progress...</i></button>
   						</div>
   					</form>


   				</div>

   			</div>
   		</div>
   	  </div>
  </div>
</div>
   <?php $this->load->view('footer'); ?>
   <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/form_validation/change_password.js"></script>
   </body>

   </html>