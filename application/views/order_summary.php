<?php $this->load->view('header');?>
		<div class=" pb-80 pt-100">
            <div class="container">
               <h4>Order Summary</h4>
                <div class="row order-summary-block">
                	<div class="col-lg-12 col-md-12">
                		<div class="row">
              				<div class="col-md-1">
              					<label class="control-label"><b>Order No :</b> </label>
              				</div>
              				<div class="col-md-2">
              					<p>
              						<?php echo $order->ord_reference_no; ?>
              					</p>
              				</div>
              				<div class="col-md-1">
              					<label class="control-label"><b>Status</b> </label>
              				</div>
              				<div class="col-md-2">
              					<p>
              						<?php echo $order->order_status_name; ?>
              					</p>
              				</div>
              				<div class="col-md-2">
              					<label class="control-label"><b>Payment Mode</b> </label>
              				</div>
              				<div class="col-md-2">
              					<p>
              						<?php echo $order->ord_payment_mode_name; ?>
              					</p>
              				</div>
              			</div>
              			<div class="">
              				<h4>Address</h5>
              					<div class="address">
			                        <div class="address-content"><span class="address-field lbl"><?php echo $order_address->pad_address; ?>
			                           </div>
			                           <div><?php echo $order_address->pad_locality;?></div>
			                            <div><?php echo $order_address->pad_city;?>- <?php echo $order_address->pad_pincode;?></div>
			                            <div><?php echo $order_address->state_name;?></div>
			                            <div>Mobile: <?php echo $order_address->pad_mobile;?></div>
			                            <?php 
			                            if($order_address->pad_landmark != '') {
			                              echo '<div>Landmark: '.$order_address->pad_landmark.'</div>';
			                            }
			                            if($order_address->pad_alt_phone != '') {
			                              echo '<div>Alternate Mobile: '.$order_address->pad_alt_phone.'</div>';
			                            }
			                            ?>
			                      </div>
              			</div>
                	</div>
                	
                </div>
                <br>
                
                  <div class="cart-main-area ptb-100">
		            <div class="container">
		                <h3 class="page-title">Products</h3>
		                <div class="row">
		                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
		                            <div class="table-content table-responsive">
		                                <table>
		                                    <thead>
		                                        <tr>
		                                             <th>Image</th>
					                                <th>Product Name</th>
					                                <th> Price</th>
					                                <!-- <th>Flavour</th> -->
					                                <th>Qty</th>
					                                <th>Subtotal</th>
		                                        </tr>
		                                    </thead>
		                                  
		                                    <tbody>
				                        	<?php foreach ($order_products as $order_products_key)
		             						{ ?>
		         							<tr>
		                                        <td class="product-thumbnail">
		                                            <a href="#"><img src="<?php echo base_url().CHILD_FOLDER.PRODUCT_BIG_IMAGE_PATH.$order_products_key->img_path;?>" class="prd-img-order-summary img-responsive	" alt=""></a>
		                                        </td>
		                                        <td class="product-name"><a href="#"><?php echo $order_products_key->prd_name ;?></a></td>
		                                        <td class="product-price-cart"><span class="amount"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $order_products_key->odp_amt ;?></span></td>
		                                        <!-- <td><?php 	echo $order_products_key->flavour_name; ?></td> -->
		                                        <td class="product-quantity">
		                                            <div class="pro-dec-cart">
		                                                <?php echo $order_products_key->odp_quantity ;?>
		                                            </div>
		                                        </td>
		                                        <td class="product-subtotal"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $order_products_key->odp_total_amt ;?></td>
		                                    </tr>
		             						<?php } ?>
				                        </tbody>
				                        <tfoot>
							            <tr>
							            	<td colspan="3"></td>
							            	<td>Sub Total</td>
							               <td ><?php echo $order->ord_sub_total; ?></td>
							            </tr>
							            <tr>
							            	<td colspan="3"></td>
							            	<td>Total</td>
							               <td ><?php echo $order->ord_total_amt; ?></td>
							            </tr>
							         </tfoot>
		                                </table>
		                            </div>
		                          
		                    </div>
		                </div>
		            </div>
		        </div>
            </div>
        </div>
<?php $this->load->view('footer');?>
    </body>
</html>