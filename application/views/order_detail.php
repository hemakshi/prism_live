<?php $this->load->view('header'); ?>

<!-- About Us Area Start -->
<div class="about-us-area pb-100">
  <div class="container">
    <div class="breadcrumb-content">
      <ul>
        <li><a href="<?php echo site_url('home') ?>">Home</a></li>
        <li><a href="<?php echo site_url('order_history') ?>">Orders</a></li>
      </ul>
    </div>
    <?php
    if (!empty($order)) {
    ?>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="orderInfo-block">
            <div>
              <span class="orderInfo-label"> Placed On: </span>
              <span class="orderInfo-value"><?php echo date("d-M-Y", strtotime($order->ord_crtd_dt)) ?></span>
            </div>
            <div>
              <span class="orderInfo-label"> Order No.: </span>
              <span class="orderInfo-value"><?php echo $order->ord_reference_no ?></span>
            </div>
            <div>
              <span class="orderInfo-label"> Order Status: </span>
              <span class="orderInfo-value"><?php echo $order->ord_status_name ?></span>
            </div>
            <div>
              <span class="orderInfo-label"> Total amount: </span>
              <span class="orderInfo-value"><?php echo $order->ord_total_amt ?></span>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="orderInfo-block">
            <div class="orderInfo-heading">Updates sent to:</div>
            <div>
              <span class="orderInfo-label"><i class="fa fa-phone" aria-hidden="true"></i> </span>
              <span class="orderInfo-value">+91 <?php echo $order->pad_mobile ?></span>
            </div>
            <div>
              <span class="orderInfo-label"><i class="fa fa-envelope" aria-hidden="true"></i> </span>
              <span class="orderInfo-value"><?php echo $order->ord_update_sent_email ?></span>
            </div>

          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="orderInfo-block">
            <div class="orderInfo-heading">Shipping Address:</div>
            <div class="bold"><?php echo $order->pad_name ?></div>
            <div><?php echo $order->pad_address ?></div>
            <div><?php echo $order->pad_locality ?> <?php echo $order->pad_city; ?>- <?php echo $order->pad_pincode; ?></div>

          </div>
        </div>
          <!-- <div class="orderInfo-block last-child">
            <div class="orderInfo-heading"> Payment Mode: </div>
            <div> <?php echo $order->ord_payment_mode_name ?></div>
          </div> -->
          </div>
          <div class="section-title-wrap text-center hd mb-50 mt-50">
                        <h3 class="section-title" style="color:white; font-size:30px;" >Items In This Order</h3>
                    </div>
          <div class="row  table-responsive">
         
            <!-- <div class="order-items-title"></div> -->
            <table class="table table-hover" class="col-12">
              <thead>
                <tr>
                  <th onclick="sortTable(0)" scope="col">Name</th>
                  <th onclick="sortTable(1)" scope="col">Price</th>
                  <th onclick="sortTable(2)" scope="col">Qty</th>
                  <th onclick="sortTable(3)" scope="col">Height x Width</th>
                  <th onclick="sortTable(4)" scope="col">Subtotal</th>
                  <th onclick="sortTable(5)" scope="col">File</th>
                  <th onclick="sortTable(6)" scope="col">Status</th>
                  <th onclick="sortTable(7)" scope="col">Remarks</th>
                  <th onclick="sortTable(8)" scope="col">Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($orderProducts as $ordersProductKey) {
                ?>
                  <tr>
                    <td><?php echo $ordersProductKey->prd_name; ?></td>
                    <td><?php echo $ordersProductKey->odp_amt; ?></td>
                    <td><?php echo $ordersProductKey->odp_quantity; ?></td>
                    <td>
                      <?php
                      if (!empty($ordersProductKey->odp_height) && $ordersProductKey->odp_height != 0 && !empty($ordersProductKey->odp_width) && $ordersProductKey->odp_width != 0)
                        echo $ordersProductKey->odp_height . " x " . $ordersProductKey->odp_width;
                      else
                        echo "-";
                      ?>
                    </td>
                    <td><?php echo $ordersProductKey->odp_subtotal; ?></td>
                    <td><a class="text-info" rel="noopener noreferrer" download="<?php echo $ordersProductKey->odp_file ?>" href="<?php echo base_url() . DESIGNFILE_UPLOAD_DIRECTORY . $ordersProductKey->odp_file ?>"> <?php echo $ordersProductKey->odp_file; ?> </a></td>
                    <td><?php echo $ordersProductKey->ops_status_name; ?></td>
                    <td><?php echo $ordersProductKey->ops_remark ? $ordersProductKey->ops_remark : '-'; ?></td>
                    <td><?php echo date("M d, Y", strtotime($ordersProductKey->odp_date)); ?></td>
                  </tr>
                <?php
                }
                ?>
              </tbody>
            </table>

          </div>
        </div>
     
    <?php
    } else {
      echo '<h5 class="text-center text-danger">Something went wrong</h5>';
    }
    ?>
  </div>
</div>

<!-- End Brand Area -->
<?php $this->load->view('footer'); ?>
</body>

</html>