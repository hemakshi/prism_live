<?php $this->load->view('header'); ?>

<div class="container pb-100">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('price_list') ?>">Price List</a></li>
    </ul>
  </div>
  <h3 class="text-center" style="padding-bottom: 10px;
    padding-top: 10px; color: white; background-color: #000">Price List</h3>

  <table class="table table-hover table-responsive-sm table-responsive-md" id="dTable" style="width: 100%; background-color: #efefef !important;">
    <thead>
      <tr>
        <th>Sr.No</th>
        <th>Category</th>
        <th>Product Name</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i = 1;
      foreach ($products as $key) {
      ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $key->category_name; ?></td>
          <td><?php echo $key->prd_name; ?></td>
          <td>₹. <?php echo $key->prd_price; ?></td>
        </tr>
      <?php
        $i++;
      }
      ?>
    </tbody>
  </table>

</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>

<script>
  $(document).ready(function() {
    $('#dTable').DataTable({

    });
  });
</script>

</body>

</html>