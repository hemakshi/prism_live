<?php $this->load->view('header'); ?>

<div class="container pb-100">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('my-complaints') ?>">My Complaints</a></li>
    </ul>
  </div>
  <div class="text-right">
    <a class="btn btn-sm btn-danger mb-2 border-radius-3" href="<?php echo site_url('register-complaint') ?>">Register Complaint</a>
  </div>
  <div class="text-center">
    <h3 style="padding-bottom: 10px; padding-top: 10px; color: white; background-color: #000">My Complaints</h3>
  </div>
  <table class="table table-hover table-responsive-sm table-responsive-md" id="dTable" style="width: 100%; background-color: #efefef  !important;">
    <thead>
      <tr>
        <th scope="col">Sr.No</th>
        <th>Order Id</th>
        <th>Description</th>
        <th>Attachment</th>
        <th>Remarks</th>
        <th>Status</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (!empty($compaintsData) && $compaintsData != '') {
        $srNo = 1;
        foreach ($compaintsData as $row) {
      ?>
          <tr>
            <td scope="row"> <?php echo $srNo; ?> </td>
            <td> <?php echo $row->com_ord_id; ?> </td>
            <td title="<?php echo $row->com_desc_name; ?>"> <?php echo $row->com_desc_name; ?> </td>
            <td> <a class="text-info" target="_blank" rel="noopener noreferrer" href="<?php echo base_url() . COMPLAINT_UPLOAD_DIRECTORY . $row->com_image ?>"> <?php echo $row->com_image; ?> </a></td>
            <td> <?php echo $row->com_remarks ? $row->com_remarks : '-' ?> </td>
            <td> <?php echo $row->com_status_name; ?> </td>
            <td> <?php echo date("d-m-Y", strtotime($row->com_updated_on)); ?> </td>
          </tr>
        <?php
          $srNo++;
        }
      } else {
        ?>
        <tr>
          <td colspan="7" class="text-center">No data available in table</td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>

<script>
  $(document).ready(function() {
    $('#dTable').DataTable({

    });
  });
</script>

</body>

</html>