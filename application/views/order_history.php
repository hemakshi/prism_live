<?php $this->load->view('header'); ?>

<div class="container pb-100">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('order_history') ?>">Order History</a></li>
    </ul>
  </div>
  <div class="text-center">
    <h3 style="padding-bottom: 10px; padding-top: 10px; color: white; background-color: #000">
      Order History
    </h3>
  </div>

  <table class="table table-hover table-responsive-sm table-responsive-md" id="dTable" style="width: 100%; background-color: #efefef  !important;">
    <thead>
      <tr>
        <th onclick="sortTable(0)" scope="col">Sr.No</th>
        <th onclick="sortTable(1)" scope="col">Order ID</th>
        <th onclick="sortTable(2)" scope="col">Sub Total</th>
        <th onclick="sortTable(3)" scope="col">Charges</th>
        <th onclick="sortTable(4)" scope="col">Order Total</th>
        <th onclick="sortTable(6)" scope="col">Order Status</th>
        <th onclick="sortTable(5)" scope="col">Order Date</th>
        <th onclick="sortTable(7)" scope="col">Action</th>
        <th onclick="sortTable(8)" scope="col">Print</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (!empty($orders) && $orders != '') {
        $srNo = 1;
        foreach ($orders as $order) {
      ?>
          <tr>
            <td><?php echo $srNo; ?></td>
            <th scope="row"><?php echo $order->ord_reference_no; ?></th>
            <td> <?php echo $order->ord_sub_total; ?> </td>
            <td><?php echo $order->ord_shipping_charges; ?></td>
            <td><?php echo $order->ord_total_amt; ?></td>
            <td><?php echo $order->ord_status_name; ?></td>
            <td><?php echo date("d-M-Y", strtotime($order->ord_date)); ?></td>
            <td><a class="order-detail" href="<?php echo site_url('order-detail/' . $order->ord_reference_no) ?>">Order Details</a></td>
            <td><a class="btn btn-sm btn-danger" style="border-radius: 3px;" href="<?php echo site_url('invoice/' . $order->ord_reference_no) ?>"><i class="fa fa-print"></i></a></td>
          </tr>
        <?php
          $srNo++;
        }
      } else {
        ?>
        <tr>
          <td colspan="8" class="text-center">No data available in table</td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>

<script>
  $(document).ready(function() {
    $('#dTable').DataTable({
      
    });
  });
</script>

</body>

</html>