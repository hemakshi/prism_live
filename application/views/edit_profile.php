<?php $this->load->view('header'); ?>
<style type="text/css">
	label {
		color: #7d7a7a;
	}
</style>
<!-- my account start -->
<div class="checkout-area pb-80 pt-100">
	<div class="container">
		<div class="row">
			<div class="ml-auto mr-auto col-lg-9">

				<div class="profile card">
					<div class="profile-data">
						<div class="tab-content edit-profile">
							<div class="page-content">
								<div class="row pb-25">
									<h3 class="info">Account Details</h3>
									<div class="col-lg-6 col-md-6">
										<div class="contact-form-style">
											<label>Email-ID *</label>
											<input name="prs_email" id="prs_email" placeholder="Email" type="email" value="<?php echo $userData->prs_email ?>" disabled>
										</div>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="contact-form-style">
											<label>Password *</label>
											<input name="prs_password" id="prs_password" placeholder="Password" type="Password" disabled value="********">
										</div>
									</div>
								</div>
								<form id="person_update_form" method="POST">
									<input type="hidden" name="prs_id" id="prs_id" value="<?php echo $this->session->userdata(PROJECT_SESSION_ID) ?>">
									<input type="hidden" name="prs_old_mob" id="prs_old_mob" value="<?php echo $userData->prs_mob ?>">
									<input type="hidden" name="ref" id="ref" value="<?php echo $ref ?>">
									<div class="row">
										<h3 class="info">General Information</h3>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Name *</label>
												<input type="text" name="prs_name" id="prs_name" placeholder="Name" value="<?php echo $userData->prs_name ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Mobile Number *</label>
												<input type="text" name="prs_mob" id="prs_mob" placeholder="Mobile Number" value="<?php echo $userData->prs_mob ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>GST Number</label>
												<input type="text" name="prs_gst" id="prs_gst" placeholder="GST Number" value="<?php echo $userData->prs_gst ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Whatsapp Number *</label>
												<input type="text" name="prs_whatsapp" id="prs_whatsapp" placeholder="Whatsapp Number" value="<?php echo $userData->prs_whatsapp ?>">
											</div>
										</div>
										<!-- <?php
												$date = '';
												if ($userData->prs_dob != '0000-00-00') {
													$date = date("d-m-Y", strtotime($userData->prs_dob));
												}
												?>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Date of birt</label>
												<input type="text" name="prs_dob" id="prs_dob" placeholder="DD/MM/YYYY" value="<?php echo $date ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Gender</label>
												<select id="prs_gender" name="prs_gender">
													<?php echo getDropdownResult('dropdown', 'gender', 'prs_gender'); ?>
												</select>
											</div>
										</div> -->
										<!-- 	<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Your bio</label>
												<textarea name="prs_bio" id="prs_bio" placeholder="Your bio"><?php echo $userData->prs_bio ?></textarea>
											</div>
										</div> -->

										<h3 class="info">Location Information</h3>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Address (Area and street) *</label>
												<textarea type="text" name="prs_address" id="prs_address" placeholder="Address (Area and street)"> <?php echo $userData->prs_address ?> </textarea>
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>City *</label>
												<input type="text" name="prs_city" id="prs_city" placeholder="City" value="<?php echo $userData->prs_city ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Pin Code *</label>
												<input type="text" name="prs_pincode" id="prs_pincode" placeholder="Pin Code" value="<?php echo $userData->prs_pincode ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>District *</label>
												<input type="text" name="prs_dist" id="prs_dist" placeholder="District" value="<?php echo $userData->prs_dist ?>">
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>State *</label>
												<select class="form-control" id="prs_state" name="prs_state" data-msg="Please select state" required>
													<option value="" selected disabled>Select State *</option>
													<?php echo getDropdownResult('state-dropdown', 'pad_state', 'pad_state', $userData->prs_state); ?>
												</select>
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="contact-form-style mb-20">
												<label>Country *</label>
												<input type="text" name="prs_country" id="prs_country" placeholder="Country" value="<?php echo $userData->prs_country ?>">
											</div>
										</div>

										<div>

										</div>
										<div class="col-lg-12 col-md-12">
											<div class="button-box">
												<button type="button" class="btn-cancel" id="btn-cancel">Cancel</button>
												<button type="submit" id="btn-dave">Save</button>
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


</div>
<!-- my account end -->
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/user_update.js"></script>
</body>

</html>