	<!-- Footer style Start -->
	<footer class="footer-area pt-75 gray-bg-3">
	    <div class="footer-top gray-bg-3 pb-35">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-3 col-md-6 col-sm-6">
	                    <div class="footer-widget mb-40">
	                        <div class="footer-title mb-25">
	                            <!-- <h4>My Account</h4> -->
	                        </div>
	                        <div class="footer-content">

	                            <div class="logo">
	                                <a href="<?php echo site_url('home'); ?>">
	                                    <img alt="" src="<?php echo base_url(); ?>assets/images/logo-ft.png">
	                               
                                    </a>
	                            </div>

	                            <!-- <ul>
	                                <li><a href="<?php echo base_url('profile'); ?>">My Account</a></li>
	                                <li><a href="<?php echo base_url('my-orders'); ?>">Orders</a></li>
	                                <li><a href="<?php echo base_url('profile'); ?>">WishList</a></li>
	                            </ul> -->
	                        </div>
	                    </div>
	                </div>
	                <div class="col-lg-3 col-md-6 col-sm-6">
	                     <div class="footer-widget mb-40">
                                <div class="footer-title mb-25">
                                    <h3>Bank Details</h3>
                                </div>
                                <div class="footer-content">
                                    <ul>

                                    <p><b style="font-size: 18px; font-weight: 550;">Bank of Baroda</b><br>
                                          University Road Rajkot<br>
                                         <b>IFSC:</b>  BARB0UNIRAJ<br>
                                          <b>A/C:</b> 36720200000422</p>
                                 <!-- <li><a href="<?php echo site_url('about');?>">About Us</a></li> --> 
                                       
                                      <!--  <li><a href="<?php echo base_url('privacy-policy');?>">Privacy Policy</a></li>
                                        <li><a href="<?php echo base_url('terms-and-conditions');?>">Terms & Conditions</a></li> -->
                                    <!--  <li><a href="<?php echo base_url('return-policy');?>">Shipping & Return Policy</a></li> -->
                                     </ul>
                                </div>
                            </div> 
                        </div>
						 <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer-widget mb-40">
                                <div class="footer-title mb-25">
                                    <h3>Reach Us</h3>
                                </div>
                                <div class="footer-content">
                                    <!-- <ul>
                                        <li><a target="_blank" href="https://www.facebook.com/Transform-Sports-Nutrition-336009693631208/">Facebook</a></li>
                                         <li><a   target="_blank" href="https://www.instagram.com/transformsportsnutrition/">Instagram</a></li>
                                         <li><a   target="_blank" href="https://twitter.com/TransformSport1">Twitter</a></li>
                                       </ul> -->
                                     

                                          <p><i class="fa fa-map-marker mr-2" aria-hidden="true"></i>16, Vijay Plot Main Road,<br> <span class="pl-3"> Gondal Road, <br></span> <span class="pl-3"> Rajkot.- 360002</span></p>
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer-widget footer-widget-red footer-black-color mb-40">
                                <div class="footer-title mb-25">
                                    <h3>Contact Us</h3>
                                </div>
                                <div class="footer-about">
                                   
                                    <div class="footer-contact mt-20">
                                        <ul>
                                            <li><a href="tel:+91 73592 47000"><i class="fa fa-phone mr-2" aria-hidden="true"></i> +91 73592 47000  </a><li>
                                               <li><a href="tel:+91 98796 98793"><i class="fa fa-phone mr-2" aria-hidden="true"></i>  +91 98796 98793 </a>  <li>
                                             <li><a href="mailto:info@prismprints.in"><i class="fa fa-envelope mr-2" aria-hidden="true"></i> info@prismprints.in</a></li>
                                        </ul>
                                    </div>
									
                                </div>
                                <div class="wp">
                                     <a href="https://api.WhatsApp.com/send?phone=917359247000" target="blank"><img src="assets/img/Whatsapp1.png"></a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom  gray-bg-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <div class="text-center"><p>Copyright © Prism Prints. All Right Reserved.</p></div>
                                <!-- <div class="text-center"><p>Copyright © <a href="<?php echo base_url();?>#">Prism Prints</a>.  All Right Reserved.</p></div> -->
                            </div>
                        </div>
                         </div>
                </div>
            </div>
        </footer>
		<!-- Footer style End -->
      	<!-- all js here -->
        <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.0.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/popper.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/ajax-mail.js"></script>
        <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url();?>assets/js/main.js"></script>
        <script src="<?php echo base_url();?>assets/js/angular.min.js"></script>
           <script src="<?php echo base_url();?>assets/js/product_list.js"></script>
           <script type="text/javascript">
       var base_url='<?php echo base_url()?>';
        $('#btn-cancel').click(function() {
            history.go(-1);
        });
     </script>
