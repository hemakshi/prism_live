   <?php $this->load->view('header');?>

 
 <div class="about-us-area pb-100">
            <div class="container">
            <div class="breadcrumb-content">
                    <ul>
                        <li><a href="<?php echo site_url('home')?>">Home</a></li>
                            <li><a href="<?php echo site_url('upload_file')?>">Upload File</a></li>
                       </ul>
                </div>
              <h3>Upload File</h3>
                <hr style="margin: 32px 0px;">
       <div class="row">

  <div class="container mt-3">
  
  <br>
 
  <form action="/action_page.php">
    
    <div class=" mb-3 col-md-6">
      <input type="file" class="custom-file-input" id="customFile" name="filename">
      <label class="custom-file-label" for="customFile">Choose file</label>
    </div>
    
    <div class="mt-3" >
      <button type="submit" class="btn btn-primary" style="line-height: 0.2; border-radius: 17px;
    padding: 17px 31px;">Upload</button>
      
    </div>

  
  </form>


</div>
</div>

<div class="row pt-50">

	<h4>Note:</h4>
<p>
Please Check the following before giving the job.
File Format : Coral Draw file (Any Version) are most preferable.
Fonts : All Fonts must be converted to Curve, for maximum quality give BITMAP file (i.e. JPEG & TIFF)
Colours : Colours Format Artwork sholuld be in CMYK mode, Avoid using 4-colour shade (e.g. C6,M70,Y35,K305) nas this may result in patchyness color variation.
Others : Do not use spot Colours (i.e. Pantone) & Registration (C100,M100,Y100,K100) Do not use Scale with image in Outline & Always use behind fill in outline
Our Samples : Please use our sample Diagrams
</p>

</div>
</div>
</div>




   	<?php $this->load->view('footer');?>
    <script src="<?php echo base_url()?>assets/js/cart.js"></script>
    <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
    </body>
</html>