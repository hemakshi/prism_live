<?php $this->load->view('header');?>
<?php
 $this->session->unset_userdata('order');
?>
<style type="text/css">
    .prod-set {
        height: unset;
    }
    .prod-set .prod-item {
        margin-bottom: 10px;
    }
    .row {
        margin-bottom: 30px;
    }
</style>
    
        <!-- About Us Area Start -->
        <div class="about-us-area  pb-100">
            <div class="container">
                 <div class="breadcrumb-content">
                    <ul>
                        <li><a href="<?php echo site_url('home')?>">Home</a></li>
                            <li class="active">Orders</li>
                       </ul>
                </div>
     <?php
     if($order == 'success' && $order) {
        echo '<div class="alert alert-success">
  Thank you for shopping with TSN.Your order has been placed successfully.
</div>';
     }
      if($order == 'failed' && $order) {
        echo '<div class="alert alert-danger">
  Order failed.Please try again
</div>';
     }
     ?>

                  <?php 
                    if(!empty($orders)) {
     ?>
              
                     <?php 
            foreach ($orders as $ordersKey)
             {
              ?> 
               <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                   <div class="cart-header">
                    <div class="my-cart-text">
                      <h4>ORDER NO: <?php echo $ordersKey->ord_reference_no;?></h4>  
                    </div>
                     <div class="my-cart-price">
                         <a class="order-detail" href="<?php echo site_url('order-detail/'.$ordersKey->ord_reference_no)?>">Order Details</a>
                    </div>
                    </div>
                     <?php 
                     

            foreach ($orderProducts as $ordersProductKey)
             {
                 if($ordersProductKey->odp_ord_id == $ordersKey->ord_id) 
                {

              ?> 
                      <div class="prod-set">
                       <div class="prod-item">
                          <div class="col1">
                            <img src="<?php echo base_url().CHILD_FOLDER.PRODUCT_SMALL_IMAGE_PATH.$ordersProductKey->img_path?>" alt="<?php echo $ordersProductKey->prd_name ;?>" width="86" height="118">
                          </div> 
                          <div class="col2">
                            <div class="row m0">
                                <div class="col-md-7 col-lg-8 col-sm-12">
                                    <div class="prod-name"><?php echo $ordersProductKey->prd_name ;?></div>  
                                    <div class="flavour-qty-wrap ptb5">
                                      <div class="flavour-qty">
                                          <span class="flv-drop" >
                                             <span>Flavour: </span> 
                                              <span><?php echo $ordersProductKey->prd_flavour ;?> </span> 
                                             
                                          </span>
                                          <span class="qty-drop" >
                                             <span>Qty: </span> 
                                              <span><?php echo $ordersProductKey->odp_quantity ;?> </span> 
                                             </span>
                                      </div>

                                          <div class="prod-name"><?php echo $ordersProductKey->prd_status ;?> (<?php echo date("M d, Y", strtotime( $ordersProductKey->odp_date))?>)</div>    
                                    </div>
                                    <div class="edit-move-delete">
                                         <ul class="progress-indicator">
                                             <?php 
                          $productStatus = $this->order_model->getOrderProductStatus($ordersProductKey->odp_prd_id, $ordersKey->ord_id);
                         
                          foreach ($productStatus as $status) {
                 if($status->class == "completed") {
                            echo '<li class="completed">
                <span class="bubble"></span>
                '.$status->status.'<br><small>'.date("M d, Y", strtotime( $status->ops_crtd_dt)).'</small>
            </li>';
                           } else {
                           echo ' <li >
                <span class="bubble"></span>
               '.$status->status.'
            </li>' ;
                           }
                          }
                          ?> 

           
        <!--     <li class="completed">
                <span class="bubble"></span>
                Shipped <br><small>14 Nov</small>
            </li>
            
            <li >
                <span class="bubble"></span>
               Delivery <br><small>25 Nov</small>
            </li> -->
           
        </ul>
                                    </div>
                                </div>
                                 <div class="col-md-4 col-lg-4 col-sm-12">
                                    <div class="prod-price">Rs. <?php echo $ordersProductKey->odp_total_amt ;?> </div> 
                                </div>
                                
                            </div>
                         </div>
                       </div> 
                        </div>
                    <?php }
                }
                    ?>
                </div>
              
                   
               </div>

                        
                    <?php
          } 
          
          ?>
          
                 <?php } else {
                    echo "<h3 class='page-title empty-cart' >Your Cart is empty</h3>";
                }
                ?>
               </div>
        </div>
         
        </div>
    <?php $this->load->view('footer');?>
       <script src="<?php echo base_url()?>assets/js/cart.js"></script>
    </body>
</html>
