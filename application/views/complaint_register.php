<?php $this->load->view('header'); ?>
<style>
  .errormesssage {
    caret-color: black;
    color: #ff6161;
    font-size: 14px;
    font-weight: 500;
  }
</style>
<div class="container">
  <div class="breadcrumb-content">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('my-complaints') ?>">My Complaints</a></li>
      <li><a href="">Register Complaint</a></li>
    </ul>
  </div>

  <div class="text-center">
    <h3 style="padding-bottom: 10px; padding-top: 10px; margin-bottom:20px; color: white; background-color: #000">Register Complaint</h3>
  </div>

  <div class="container">

    <div class="pb-100">
      <form id="complaint_register_form" name="complaint_register_form" enctype="multipart/form-data" method="POST">
        <div class="form-row mb-1">
          <div class="col-md-3"></div>

          <div class="col-md-3 mb-2">
            <label for="validationDefault03">Order Number <span class="emsg">*</span></label>
          </div>
          <div class="col-md-4 mb-2">
            <input type="text" name="order_number" class="form-control" id="validationDefault03" required placeholder="Order Number" required>
          </div>

          <div class="col-md-3"></div>
        </div>

        <div class="form-row mb-1">
          <div class="col-md-3"></div>

          <div class="col-md-3 mb-2">
            <label for="validationDefault03">Complaint Description <span class="emsg">*</span></label>
          </div>
          <div class="col-md-4 mb-2">
            <select class="form-control" id="complaint_description" name="complaint_description" required>
              <option value="" selected disabled>Select Your Complaint</option>
              <?php echo getDropdownResult('compaints-desc-dropdown', '', ''); ?>
            </select>
          </div>

          <div class="col-md-3"></div>
        </div>

        <div class="form-row mb-1">
          <div class="col-md-3"></div>

          <div class="col-md-3 mb-2">
            <label for="validationDefault03">Please upload Photo/Video of Printed Card <span class="emsg">*</span></label>
          </div>
          <div class="col-md-4 mb-2">
            <input type="file" name="printed_file" class="custom-file-input" id="printed_file" required onchange="return fileValidation()">
            <label class="custom-file-label" for="printed_file">Choose file</label>
          </div>

          <div class="col-md-3"></div>
        </div>

        <div class="text-center">
          <button type="submit" class="btn btn-danger mt-20" id="btn_register" style="line-height: 0.2; border-radius: 17px; padding: 17px;">Register</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/complaint_register.js"></script>

<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>

</body>

</html>