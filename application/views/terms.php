<?php $this->load->view('header');?>

		<!-- About Us Area Start -->
        <div class="about-us-area pb-100">
            <div class="container">
            <div class="breadcrumb-content">
                    <ul>
                        <li><a href="<?php echo site_url('home')?>">Home</a></li>
                            <li><a href="<?php echo site_url('about')?>">Terms & Conditions</a></li>
                       </ul>
                </div>
                <div class="row">
                   <div class="overview-content-2">
					<p class="MsoNormalCxSpFirst" style="line-height:normal"><span lang="EN-US" style="font-size:20.0pt">Terms and Conditions<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">INTRODUCTION:<o:p></o:p></span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">transformsportsnutrition.com
(‘Website’) is an online service owned, operated and managed by Transform
Sports Nutrition (TSN) (‘TSN’ or ‘we’ or ‘us’). In using/accessing the Transform
Sports Nutrition (TSN) website, you are deemed to have accepted the terms and
conditions of the agreement listed below or as may be revised from time to time
(‘User Agreement’), which is, for an indefinite period and you understand and
agree that you are bound by such terms till the time you access this Website.
If you have any queries about the terms and conditions of this User Agreement
or have any comments or complaints on or about the Website, please email us at info.transformsportsnutrion.com.
We reserve the right to change the terms and conditions of this User Agreement
from time to time without any obligation to inform you and it is your
responsibility to look through them as often as possible.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">OWNERSHIP OF RIGHTS:</span></b><br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Any
use of this Website or its contents, including copying or storing it or them in
whole or part, other than for your own personal, non-commercial use is
prohibited without the explicit permission of Transform Sports Nutrition (TSN).
All information displayed, transmitted or carried on the Website is protected
by copyright and other intellectual property laws. Copyrights and other
intellectual property in respect of the some of the content on the Website may
be owned by the third parties. This site is designed, updated and maintained by
Transform Sports Nutrition (TSN) or its licensors. You shall not modify, publish,
transmit, transfer, sell, reproduce, create derivative work from, distribute,
repost, perform, display or in any way commercially exploit any of the content
available on the Website.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">ACCURACY OF CONTENT AND INVITATION TO OFFER:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
have taken all care and precaution to try and provide accurate data and
information. In the preparation of the content of this Website, in particular
to ensure that prices quoted are correct at time of publishing and all products
have been fairly described. All prices are displayed inclusive of GST. Services
however are listed exclusive of service tax as rules for service tax vary with
different services. Packaging may vary from that shown. The weights, dimensions
and capacities given are approximate only. We have made every effort to display
as accurately as possible the colours of our products that appear on the
Website. However, as the actual colours you see will depend on your monitor, we
cannot guarantee that your monitor’s display of any colour will accurately reflect
the colour of the product on delivery. All products/services and information
displayed on the Website constitute an invitation to offer. Your order for
purchase constitutes your offer which shall be subject to the terms and
conditions of this User Agreement. We reserve the right to accept or reject
your offer in part or in full. Our acceptance of your order will take place
upon dispatch of the product(s) ordered. Dispatch of all the product(s)
ordered, may or may not happen at the same time, in such a scenario that
portion of the order which has been dispatched will be deemed to have been
accepted by us and the balance would continue to be on offer to us and we
reserve the right to accept or reject such balance order. No act or omission of
Transform Sports Nutrition (TSN) &nbsp;prior
to the actual dispatch of the product (s) ordered will constitute acceptance of
your offer. If you have supplied us with your email address or phone number, we
will notify you by email and/or phone number, as the case may be, as soon as
possible to confirm receipt of your order and email/contact you again to
confirm dispatch and therefore acceptance of the order.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">GENERAL TERMS OF USE:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Being
eligible to use and by using the website/ transacting on the Mobile App or
through any other mode of communication/ by making any purchase during the
aforesaid tenure, User hereby agree to be bound by this Terms of Use;<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">User
is hereby authorised to use the website for lawful purpose and for the purposes
as mentioned under the policies of the Transform Sports Nutrition (TSN) and
Google only, any violation to the policy will lead to strict legal action
against the User in terms of the policies and applicable laws;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN)&nbsp; strives to
provide best prices possible on products and services to users buying or
availing from the Website. However, Transform Sports Nutrition (TSN)&nbsp; does not guarantee that the price will be the
lowest in the city, region or geography. Prices and availability are subject to
change without notice or any consequential liability on the Transform Sports
Nutrition (TSN);<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">The
prices displayed for each item under the promotion will be for the respective
days of promotion only and will not be applicable on any date prior to or after
the date of promotion;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">All offers
on Products shall be for limited stocks and are for limited period(s).
Transform Sports Nutrition (TSN)&nbsp; may at
its sole discretion modify, extend, update or withdraw the offers on products
without prior notice to the Users. In such events, the revisions, as the case
may be, will be updated on the website accordingly;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">A
promo code, once used shall not be refunded in case of cancellation of order
either by Customer or Transform Sports Nutrition (TSN);<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">We
don’t run the promotions that prompt users to initiate a purchase, download, or
other commitment without first providing all relevant information and without
obtaining the user’s explicit consent promotions that represent our services in
a way that is not accurate, realistic, and truthful;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">All
products sold on Transform Sports Nutrition (TSN).<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">In
case of Cash back offers extended by banks during the aforesaid period, user
will, in addition to this Terms of Use Policy, be governed by the Terms and
Conditions of the said bank/card issuer;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN)&nbsp; may, at any time
due to various reasons, including but not limited to technical errors,
unavailability of stocks or for any other reasons whatsoever, cancel the orders
placed by Users. Transform Sports Nutrition's (TSN) &nbsp;decision of cancellation of order shall be
final and Transform Sports Nutrition (TSN) shall not be liable for such
cancellation(s) whatsoever;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Expected
delivery time as mentioned on website may vary than the usual time of delivery;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN) &nbsp;strives to
provide accurate products, services and pricing information, typographical and
other errors may occur. In the event that a product or service is listed at an
incorrect price or with incorrect information due to an error in pricing or
product or service information, Transform Sports Nutrition (TSN) &nbsp;may, at its discretion, either contact User
for instructions or cancel the User’s order and will notify the User about such
cancellation;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN) shall have right to modify the price of a product or
service any time, without any prior information;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Cancellation
by Transform Sports Nutrition (TSN): There may be certain orders that Transform
Sports Nutrition (TSN) &nbsp;is unable to
accept and must cancel. Transform Sports Nutrition (TSN) &nbsp;reserves the right, at its sole discretion, to
refuse or cancel any order for any reason whatsoever, without assigning any
reason to the User, User accept and undertake that the same is acceptable and
he/ she/ it will not contest/ raise any dispute on the same. The situations
that may result in cancellation of User’s order includes, without limitation,
non availability of the product or quantities ordered by User, non-
availability of the service, inaccuracies or errors in pricing information, or
problems identified by Transform Sports Nutrition's (TSN) &nbsp;credit and fraud avoidance department team;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN) may also require additional verifications or information
before accepting any order. Transform Sports Nutrition (TSN) will contact the
User if all or any portion of User’s order is cancelled or if additional
information is required to accept the User’s order. If the order is cancelled
after credit card has been charged, the said amount will be reversed back in
User’s credit card account;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Cancellation
by the User: In case of requests for order cancellations, Transform Sports
Nutrition (TSN), at its sole discretion, reserves the right to accept or reject
requests for order cancellations for any reason whatsoever, without assigning
any reason to the User. As part of usual business practice, if Transform Sports
Nutrition (TSN) &nbsp;receives a cancellation
notice and the order has not been processed/ approved by Transform Sports
Nutrition (TSN), Transform Sports Nutrition (TSN) shall cancel the order and
refund the entire amount to User within a reasonable period of time;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN) &nbsp;will not be able
to cancel orders that have already been processed, it is pertinent to note that
the Transform Sports Nutrition (TSN) &nbsp;has
the full right to decide whether an order has been processed or not. User
hereby agree and undertakes that the decision taken by the Transform Sports
Nutrition (TSN) &nbsp;is acceptable to User
and User shall not create any dispute on the decision taken by Transform Sports
Nutrition (TSN) &nbsp;on cancellation;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN) &nbsp;reserves the
right to cancel any orders that classify as ‘Bulk Order’ as determined by Transform
Sports Nutrition (TSN) &nbsp;as per certain
criteria. Transform Sports Nutrition (TSN) &nbsp;Reward Point or Promo Code used for placing
the ‘Bulk Order’ will not be refunded as per this cancellation policy. An order
can be classified as ‘Bulk Order’ if it meets with the below mentioned
criteria, which may not be exhaustive, viz:<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Products
ordered are not for self-consumption but for commercial resale;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Multiple
orders placed for same product at the same address;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Bulk
quantity of the same product ordered;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Invalid
address given in order details;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Any
malpractice used to place the order.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transform
Sports Nutrition (TSN) &nbsp;Reserve the
right, at its sole discretion, to change, modify, add or remove portions of
this document, at any time without any prior written notice to user;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Limitation
of Liability: Under no circumstances Transform Sports Nutrition's (TSN) &nbsp;liability shall exceed giving the User a
replacement of the same product/ alternate product of the same value;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">These
terms and conditions are subject to Indian laws and any dispute shall be
subject to jurisdiction of the courts in Mumbai (India) only.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">USAGE RESTRICTIONS:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
shall not use the Website for any of the following purposes:<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Disseminating
any unlawful, harassing, libelous, abusive, threatening, harmful, vulgar,
obscene, or otherwise objectionable material.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Transmitting
material that encourages conduct that constitutes a criminal offence, results
in civil liability or otherwise breaches any relevant laws, regulations or code
of practice.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Gaining
unauthorised access to other computer / network systems.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Interfering
with any other person’s use or enjoyment of the Website.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Breaching
any applicable laws.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Interfering
or disrupting networks or websites connected to the Website.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Making,
transmitting or storing electronic copies of materials protected by copyright
without the permission of the owner.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">You
are not permitted to host, display, upload, modify, publish, transmit, update
or share any information on the Website that<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">belongs
to another person and to which you do not have any right to;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">is
grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic,
paedophilic, libellous, invasive of another’s privacy, hateful, or racially,
ethnically objectionable, disparaging, relating or encouraging money laundering
or gambling, or otherwise unlawful in any manner whatever;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">harm
minors in any way;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">infringes
any patent, trademark, copyright or other proprietary rights;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">violates
any law for the time being in force;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">deceives
or misleads the addressee about the origin of such messages or communicates any
information which is grossly offensive or menacing in nature;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">impersonate
another person;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">contains
software viruses or any other computer code, files or programs designed to
interrupt, destroy or limit the functionality of any computer resource
including the Website;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">threatens
the unity, integrity, defense, security or sovereignty of India, friendly
relations with foreign states, or public order or causes incitement to the
commission of any cognisable offence or prevents investigation of any offence
or is insulting any other nation.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">You
are also prohibited from:<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">violating
or attempting to violate the integrity or security of the Website or its
content;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">transmitting
any information (including job posts, messages and hyperlinks) on or through
the Website that is disruptive or competitive to the provision of services by
us;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">intentionally
submitting on the Website any incomplete, false or inaccurate information;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">making
any unsolicited communications to other users of the Website;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">using
any engine, software, tool, agent or other device or mechanism (such as
spiders, robots, avatars or intelligent agents) to navigate or search the
Website;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">attempting
to decipher, decompile, disassemble or reverse engineer any part of the
Website;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">copying
or duplicating in any manner any of the content on the Website or other
information available from the Website;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">framing
or hotlinking or deeplinking any content on the Website.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">QUANTITY RESTRICTIONS:</span></b><br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
reserve the right, at our sole discretion, to limit the quantity of items
purchased per person, per household or per order. These restrictions may be
applicable to orders placed by the same account, the same credit / debit card,
and also to orders that use the same billing and/or shipping address. We will
provide notification to the customer should such limits be applied. We also
reserve the right, at our sole discretion, to prohibit sales to any one as we
may deem fit.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">PRICING INFORMATION:</span></b><br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">While
we strive to provide accurate product and pricing information, pricing or
typographical errors may occur. We cannot confirm the price of a product until
after you order. In the event that a product is listed at an incorrect price or
with incorrect information due to an error in pricing or product information,
we shall have the right, at our sole discretion, to refuse or cancel any orders
placed for that product, unless the product has already been dispatched. In the
event that an item is mis-priced, we may, at our sole discretion, either
contact you for instructions or cancel your order and notify you of such
cancellation. Unless the product ordered by you has been dispatched, your offer
will not be deemed accepted and we will have the right to modify the price of
the product and contact you for further instructions using the e-mail address
or the contact number provided by you during the time of registration, or
cancel the order and notify you of such cancellation. In the event we accept
your order the same shall be debited to your credit / debit card account and
duly notified to you by email or the contact number, as the case may be, that
the payment has been processed. The payment may be processed prior to dispatch
of the product that you have ordered. If we have to cancel the order after we
have processed the payment, the said amount will be reversed back to your
credit / debit card account. We strive to provide you with best value, however
prices and availability are subject to change without notice.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Our
promotional offers/discounts are not sitewide and are limited to selected
categories. Coupon codes may not be applicable on categories like diapers, baby
food etc. or such other product or service as may be determined by us in our
sole discretion.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">INDEMNITY:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
disclaim all warranties or conditions, whether expressed or implied, (including
without limitation implied, warranties or conditions of information and
context). We shall not be liable to any person for any loss or damage which may
arise from the use of any of the information contained in any of the materials
on this Website. This User Agreement and any contractual obligation between us
and you will be governed by the laws of India, subject to the exclusive
jurisdiction of Courts in Mumbai. All disputes will be subject to arbitration
in Mumbai in English by a single arbitrator appointed by us under the Arbitration
and Conciliation Act, 1996. Each party to arbitration shall bear its own cost.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
agree to defend, indemnify and hold harmless Transform Sports Nutrition (TSN),
its employees, directors, officers, agents and their successors and assigns
from and against any and all claims, liabilities, damages, losses, costs and
expenses, including attorney’s fees, caused by or arising out of claims based
upon your actions or inactions, which may result in any loss or liability to Transform
Sports Nutrition (TSN) &nbsp;or any third
party including but not limited to breach of any warranties, representations or
undertakings or in relation to the non-fulfillment of any of your obligations
under this User Agreement or arising out of your violation of any applicable
laws, regulations including but not limited to intellectual property rights,
payment of statutory dues and taxes, claim of libel, defamation, violation of rights
of privacy or publicity, loss of service by other subscribers and infringement
of intellectual property or other rights. This clause shall survive the expiry
or termination of this User Agreement.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">ELIGIBILITY TO USE:</span></b><br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Use of
the Website is available only to persons who can form legally binding contracts
under applicable law. Persons who are “incompetent to contract” within the
meaning of the Indian Contract Act, 1872 including un-discharged insolvents
etc. are not eligible to use the Website. We reserve the right to terminate
your membership and refuse to provide you with access to the Website at our
sole discretion. The Website is not available to persons whose membership has
been suspended or terminated by us for any reason whatsoever. If you are registering
as a business entity, you represent that you have the authority to bind the
entity to this User Agreement. We make no representation that any products or
services referred to in the materials on this Website are appropriate for use,
or available outside India. Those who choose to access this Website from
outside India are responsible for compliance with local laws if and to the
extent local laws are applicable. We will deliver the products only within
India and will not be liable for any claims relating to any products ordered
from outside India. Some Indian states prohibit direct sale of merchandise from
other states and require special documentation to effect such a sale without
dual taxation, if we receives an order from such states or to be delivered to
such states under such circumstances we retain the right to accept or reject
the order at our sole discretion. Those who choose to access this Website from
Indian states which restrict such use are responsible for compliance with local
laws if and to the extent local state laws are applicable. We will deliver the
products only within states having open import policy and will not be liable
for any claims relating to any products ordered from restricted states. Except
where additional terms and conditions are provided which are product specific,
these terms and conditions supersede all previous representations,
understandings, or agreements and shall prevail notwithstanding any variance
with any other terms of any order submitted.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">PRIVACY:</span></b><br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">IF YOU
DO NOT AGREE, PLEASE DO NOT USE OR ACCESS THE WEBSITE.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">All
the information provided to us by you, including sensitive personal
information, is voluntary. You have the right to withdraw your consent at any
time, in accordance with the terms of this User Agreement, but please note that
withdrawal of consent will not be retroactive.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
can access, modify, correct and eliminate the data about you which has been
collected pursuant to your decision to become a user of the Website. If you
update any information relating to you, we may keep a copy of the information
which you originally provided to us in its archives.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Due to
the communications standards on the Internet, when you visit the Website, we
automatically receive the URL of the site from which you came and the site to
which you are going when you leave. We also receive the Internet Protocol (IP)
address of your computer (or the proxy server you used to access the World Wide
Web), your computer operating system and type of web browser you are using,
email patterns, as well as the name of your internet service provider (ISP).
This information is used to analyze overall trends to help us improve our
service.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">The
Website uses temporary cookies to store certain data (that is not sensitive
personal data or information) that is used by us and our service providers for
the technical administration of the Website, research and development, and for
administration. In the course of serving advertisements or optimizing services
to you, we may allow authorized third parties to place or recognize a unique
cookie on your browser. We do not store personally identifiable information in
the cookies.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We may
keep records of telephone calls received and made for making inquiries, orders
or other purposes for the purpose of administration of services, research and
development, quality management services and for proper administration.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
allow other companies, to serve advertisements to you. These companies include
third party ad servers, ad agencies, ad technology vendors and research firms.
We may “target” some ads to you that fit a certain general profile. We do NOT
use personally identifiable information to target ads.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
assume no responsibility, and shall not be liable for, any damages to, or
viruses that may infect your equipment on account of you access to, use of, or
browsing the Website or the downloading of any material, data, text, images,
video content, or audio content from the Website. If you are dissatisfied with
the Website, your sole remedy is to discontinue using the Website.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">This
privacy policy applies to websites and services that are operated and managed
by us. We do not exercise control over the sites displayed as search results or
links from within its services. These other sites may place their own cookies
or other files on your computer, collect data or solicit personal information
from you, for which we are not responsible or liable. Accordingly, we do not
make any representations concerning the privacy practices or policies of such
third parties or terms of use of such websites, nor do we guarantee the
accuracy, integrity, or quality of the information, data, text, software,
sound, photographs, graphics, videos, messages or other materials available on
such websites. The inclusion or exclusion does not imply any endorsement by us
of the website, the website’s provider, or the information on the website. We
encourage you to read the privacy policies of that website.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">The
Website may enable you to communicate with other users or to post information
to be accessed by others, whereupon other users may collect such data. We
hereby expressly disclaim any liability for any misuse of such information that
is made available by visitors in such a manner.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
value the privacy of information pertaining to our associates. The linkage between
your IP address and your personally identifiable information is not shared with
third-parties without your permission or except when required by law.
Notwithstanding the above, we may share some of the aggregate findings and
details with advertisers, sponsors, investors, strategic partners, and others
in order to help grow our business without obtaining any approval from you. We
will enable you to communicate your privacy concerns to us and that we will
respond to them appropriately. We do not disclose any personal information to
advertisers and for other marketing and promotional purposes that could be used
to personally identify you, such as your password, credit card number and bank
account number.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">REFUSAL OF SERVICE:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
reserve the right to refuse service to anyone at any time. We reserve the
right, in our sole discretion, to suspend or cancel the service at any time if
a computer virus, bug, or other technical problem corrupts the security, or
proper administration of the service.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">PROMOTIONAL COMMUNICATIONS:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">“We
may also send you other information about us, the Site, our other websites, our
products, sales promotions, our newsletters, SMS updates, anything relating to
other companies in our group or our business partners. If you would prefer not
to receive any of this additional information as detailed in this paragraph (or
any part of it) please click the “unsubscribe” link in any email that we send
to you or register as a Do Not Disturb user. Within 7 working days (days which
are neither (i) a Saturday or Sunday, nor (ii) a public holiday anywhere in
India) of receipt of your instruction we will cease to send you information as
requested. In case of any clarity you can contact our Customer support
service”.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">FINANCIAL DETAILS:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
agree, understand and confirm that the credit / debit card details or other
financial details provided by you for availing of services on the Website will
be correct and accurate and you shall not use the credit /debit card or
financial facility which is not lawfully owned / obtained by you. You also
understand that any financial information submitted by you is directly received
by our acquiring bank and not taken by us. We will not be liable for any credit
/ debit card fraud. The liability for use of a card fraudulently will be on you
and the onus to ‘prove otherwise’ shall be exclusively on you. We and our
associated acquiring bank or financial institutions reserve the right to
recover the cost of goods, collection charges and lawyers fees from persons
using the Website fraudulently. We and our associated acquiring banks or
financial institutions reserve the right to initiate legal proceedings against
such persons for fraudulent use of the Website and any other unlawful acts or
acts or omissions in breach of these terms and conditions in accordance with
applicable laws.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">COMMUNICATION:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">When
you visit the Website or send emails to us, you are communicating with us
electronically. You consent to receive communications from us electronically.
We will communicate with you by email or by posting notices on the Website. You
agree that all agreements, notices, disclosures and other communications that
we provide to you electronically satisfy any legal requirement that such
communications be in writing. When you submit your phone number along with your
shipping address or to request our call back, you consent to receive calls on
that number for communication related to your order/request and other site
related communication.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">WEBSITE FEEDBACK, USER COMMENTS AND USER GENERATED CONTENT:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">All
reviews, comments, feedback, postcards, suggestions, ideas, and other
submissions disclosed, submitted to us on or by this Website or otherwise
disclosed, submitted or offered in connection with your use of this Website
(collectively, the “Comments”) shall be and remain our property. Such
disclosure, submission or offer of any Comments shall constitute an assignment
to us of all worldwide rights, titles and interests in all copyrights and other
intellectual properties in the Comments. Thus, we own exclusively all such
rights, titles and interests and shall not be limited in any way in its use,
commercial or otherwise, of any Comments. We will be entitled to use,
reproduce, disclose, modify, adapt, create derivative works from, publish,
display and distribute any Comments you submit for any purpose whatsoever,
without restriction and without compensating you in any way. We are and shall
be under no obligation (1) to maintain any Comments in confidence; (2) to pay
you any compensation for any Comments; or (3) to respond to any Comments. You
agree that any Comments submitted by you to the Website will not violate this
policy or any right of any third party, including copyright, trademark, privacy
or other personal or proprietary right(s), and will not cause injury to any
person or entity. You further agree that no Comments submitted by you to the
Website will be or contain libelous or otherwise unlawful, threatening, abusive
or obscene material, or contain software viruses, political campaigning, commercial
solicitation, chain letters, mass mailings or any form of ‘spam’. We do not
regularly review posted Comments, but do reserve the right (but not the
obligation) to monitor and edit or remove any Comments submitted on the
Website. You grant us the right to use the name that you submit in connection
with any Comments. You agree not to use a false email address, impersonate any
person or entity, or otherwise mislead as to the origin of any Comments you
submit. You are and shall remain solely responsible for the content of any
Comments you make and you agree to indemnify us and our affiliates for all
claims resulting from any Comments you submit. We and our affiliates take no
responsibility and assume no liability for any Comments submitted by you or any
third party.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">COPYRIGHT &amp; TRADEMARK:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We and
our suppliers and licensors expressly reserve all intellectual property rights
in all text, programs, products, processes, technology, content and other
materials, which appear on this Website. Access to this Website does not confer
and shall not be considered as conferring upon anyone any license under any of
our or any third party’s intellectual property rights. All rights, including
copyright, in this Website are owned by or licensed to us . Any use of this
Website or its contents, including copying or storing it or them in whole or
part, other than for your own personal, non-commercial use is prohibited
without our prior permission. You may not modify, distribute or re-post
anything on this Website for any purpose. The Transform Sports Nutrition (TSN)
names and logos and all related product and service names, design marks and
slogans are the trademarks or service marks of Transform Sports Nutrition (TSN)
&nbsp;or licensed to Transform Sports
Nutrition (TSN). All other marks are the property of their respective companies
and you shall not use or exploit the same in any manner whatsoever. No
trademark or service mark license is granted to you in connection with the
materials contained on this Website. Access to this Website does not authorize
anyone to use any name, logo or mark which appear on the Website in any manner.
References on this Website to any names, marks, products or services of third
parties or hypertext links to third party websites or information are provided
solely as a convenience to you and do not in any way constitute or imply our
endorsement, sponsorship or recommendation of the third party, information,
product or service. We are not responsible for the content of any third party
websites and does not make any representations regarding the content or
accuracy of material on such websites. If you decide to link to any such third
party websites, you do so entirely at your own risk. All materials, including
images, text, illustrations, designs, icons, photographs, programs, music clips
or downloads, video clips and written and other materials that are part of this
Website (collectively, the “Contents”) are intended solely for personal,
non-commercial use. You may download or copy the Contents and other
downloadable materials displayed on the Website for your personal use only. No
right, title or interest in any downloaded materials or software is transferred
to you as a result of any such downloading or copying. You may not reproduce
(except as noted above), publish, transmit, distribute, display, modify, create
derivative works from, sell or participate in any sale of or exploit in any
way, in whole or in part, any of the Content, the Website or any related
software. All software used on this Website is the property of Transform Sports
Nutrition (TSN) &nbsp;and licensors and
protected by Indian and international copyright laws. The Content and software
on this Website may be used only as a shopping resource. Any other use,
including the reproduction, modification, distribution, transmission, republication,
display, or performance, of the Content on this Website is strictly prohibited.
Unless otherwise noted, all Content are copyrights, trademarks, trade dress
and/or other intellectual property owned, controlled or licensed by us, our
affiliates or by third parties who have licensed their materials to us and are
protected by Indian and international copyright laws. The compilation (meaning
the collection, arrangement, and assembly) of all Content on this Website is
the exclusive property of Transform Sports Nutrition (TSN) and is also
protected by Indian and international copyright laws. We don’t offer any
Counterfeit goods contain a trademark or logo that is identical to or
substantially indistinguishable from the trademark of another.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">OBJECTIONABLE MATERIAL:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
understand that by using this Website or any services provided on the Website,
you may encounter Content that may be deemed by some to be offensive, indecent,
or objectionable, which Content may or may not be identified as such. You agree
to use the Website and any service at your sole risk and that to the fullest
extent permitted under applicable law, we and our affiliates shall have no
liability to you for Content that may be deemed offensive, indecent, or
objectionable to you.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">TERMINATION:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">This
User Agreement is effective unless and until terminated by either you or us.
You may terminate this User Agreement at any time by informing us in writing
through Indian postal system by registered post, that you no longer wish to be
associated with this Website, provided that you discontinue any further use of
this Website. We may terminate this User Agreement at any time and may do so
immediately without notice, and accordingly deny you access to the Website.
Such termination will be without any liability to Transform Sports Nutrition
(TSN). Upon any termination of the User Agreement by either you or us, you must
promptly destroy all materials downloaded or otherwise obtained from this
Website, as well as all copies of such materials, whether made under the User
Agreement or otherwise. Our right to any Comments shall survive any termination
of this User Agreement. Any such termination of the User Agreement shall not
cancel your obligation to pay for the product already ordered from the Website
or affect any liability that may have arisen under the User Agreement.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">LIMITATION OF LIABILITY AND DISCLAIMERS:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">The
Website is provided without any warranties or guarantees and in an “As Is”
condition. You must bear the risks associated with the use of the Website. The
Website provides content from other Internet websites or resources and while we
try to ensure that material included on the Website is correct, reputable and
of high quality, we shall not be held liable or responsible if this is not the
case. We will not be responsible for any errors or omissions or for the results
obtained from the use of such information or for any technical problems you may
experience with the Website. This disclaimer does not apply to any product
warranty offered by the manufacturer of the product as specified in the product
specifications. This disclaimer constitutes an essential part of this User
Agreement. To the fullest extent permitted under applicable law, we shall not
be liable for any indirect, incidental, special, consequential or exemplary
damages, including but not limited to, damages for loss of profits, goodwill,
use, data or other intangible losses arising out of or in connection with the
Website, its services or this User Agreement. Without prejudice to the
generality of the section above, our total liability to you for all liabilities
arising out of this User Agreement be it in tort or contract is limited to the
amount charged to you, against the value of the products ordered by you. Transform
Sports Nutrition (TSN), its associates and technology partners make no
representations or warranties about the accuracy, reliability, completeness
and/or timeliness of any content, information, software, text, graphics, links
or communications provided on or through the use of the Website or that the
operation of the Website will be error free and/or uninterrupted. We assumes no
liability whatsoever for any monetary or other damage suffered by you on
account of the delay, failure, interruption, or corruption of any data or other
information transmitted in connection with use of the Website; and/or any
interruption or errors in the operation of the Website.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">WEBSITE SECURITY:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
are prohibited from violating or attempting to violate the security of the
Website, including, without limitation:</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Accessing
data not intended for you or logging onto a server or an account which you are
not authorized to access;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Attempting
to probe, scan or test the vulnerability of a system or network or to breach
security or authentication measures without proper authorization;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Attempting
to interfere with service to any other user, host or network, including,
without limitation, via means of submitting a virus to the Website,
overloading, ‘flooding,’ ‘spamming’, ‘mail bombing’ or ‘crashing;<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Sending
unsolicited email, including promotions and/or advertising of products or
services; or<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Forging
any TCP/IP packet header or any part of the header information in any email or
newsgroup posting. Violations of system or network security may result in civil
or criminal liability. We will investigate occurrences that may involve such
violations and may involve, and cooperate with, law enforcement authorities in
prosecuting users who are involved in such violations. You agree not to use any
device, software or routine to interfere or attempt to interfere with the
proper working of this Website or any activity being conducted on this Website.
You agree, further, not to use or attempt to use any engine, software, tool,
agent or other device or mechanism (including without limitation browsers,
spiders, robots, avatars or intelligent agents) to navigate or search this
Website other than the search engine and search agents available from Transform
Sports Nutrition (TSN) &nbsp;on this Website
and other than generally available third party web browsers (e.g., Google
Chrome, Firefox, Microsoft Internet Explorer).<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">“We
have in place appropriate technical and security measures to prevent
unauthorized or unlawful access to or accidental loss of or destruction or
damage to your information. When we collect data through the Site, we collect
your personal details on a secured server through firewalls. The Company does
not access, store or keep debit card data or credit card data and or any
financial information. All transactions done using Secure Server Software (SSL)
for 128 bit encryption through third party gateways and Transform Sports
Nutrition (TSN) &nbsp;plays no role in the
transaction, except for directing the customers to gateways or relevant
webpage(s). Accordingly, Transform Sports Nutrition (TSN) &nbsp;shall not be responsible or liable for any
loss or damage due to any disclosure whatsoever of Personal Information while
using the third party gateways and or website. Transform Sports Nutrition (TSN)
&nbsp;shall not be liable for any loss or
damage sustained by reason of any disclosure (inadvertent or otherwise) of any
Personal Information concerning the User’s account and / or information
relating to or regarding online transactions using credit cards / debit cards
/cash cards/net banking and / or their verification process and particulars nor
for any error, omission or inaccuracy with respect to any information so
disclosed and used. We maintain physical, electronic and procedural safeguards
in connection with the collection, storage and disclosure of your information.
To protect your privacy and security, we will verify your identity before
granting access or making changes to your Personal Information. If you have
registered your profile on the Website, your ID and Password are required in
order to access your Account. You are responsible for protecting against
unauthorized access to your password and to your computer.”<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">ENTIRE AGREEMENT:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If any
part of this User Agreement is determined to be invalid or unenforceable
pursuant to applicable law including, but not limited to, the warranty
disclaimers and liability limitations set forth above, then the invalid or
unenforceable provision will be deemed to be superseded by a valid, enforceable
provision that most closely matches the intent of the original provision and
the remainder of the User Agreement shall continue in effect. Unless otherwise
specified herein, this User Agreement constitutes the entire agreement between
you and us with respect to the Websites/services and it supersedes all prior or
contemporaneous communications and proposals, whether electronic, oral or
written, between you and us with respect to the Websites/services. Our failure
to act with respect to a breach by you or others does not waive its right to
act with respect to subsequent or similar breaches.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">CONTACT INFORMATION:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If any
User has any grievance, comment, question or suggestion regarding any of our
Product/Services, please contact our Customer Support Executive , who will
redress the grievances of the Users expeditiously but within one month from the
date of receipt of grievance, and who can be reached by:</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">Sending
a letter marked to the attention of Customer Support Executive, Transform
sports nutrition, The Chamber, Unit no.8, Plot no. 89 - A/B, Kandivali Inds.
Estate, Charkop, Kandivali (West, Mumbai - 400067 (MH); or Sending an email to info.transformsportsnutrition.com.<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">HEALTH RELATED INFORMATION:</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Health
Related Information. The information contained in the Web Site is provided for
informational purposes only and is not meant to substitute for the advice
provided by your doctor or other health care professional. You should not use
the information available on or through the Web Site (including, but not
limited to, information that may be provided on the Web Site by healthcare or
nutrition professionals employed by or contracting with Transform Sports
Nutrition (TSN)) for diagnosing or treating a health problem or disease, or
prescribing any medication. Information and statements regarding dietary
supplements have not been evaluated by the Food and Drug Administration and are
not intended to diagnose, treat, cure, or prevent any disease. You should read
carefully all product packaging prior to use. The results from the products
will vary from person to person. No individual result should be seen as
typical.</p>	  
                        </div>
                     </div>
            </div>
        </div>
	
		<!-- End Brand Area -->
	<?php $this->load->view('footer');?>
    </body>
</html>
