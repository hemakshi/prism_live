<?php $this->load->view('header');?>

		<!-- About Us Area Start -->
        <div class="about-us-area pb-100">
            <div class="container">
            <div class="breadcrumb-content">
                    <ul>
                        <li><a href="<?php echo site_url('home')?>">Home</a></li>
                            <li><a href="<?php echo site_url('about')?>">Shipping & Return Policy</a></li>
                       </ul>
                </div>
                <div class="row">
                   <div class="overview-content-2">
					<p class="MsoNormalCxSpFirst" style="line-height:normal"><span lang="EN-US" style="font-size:18.0pt">Shipping &amp; Returns<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Here
at Transform Sports Nutrition (TSN) we take pride in making sure that each and
every customer has a positive experience. Customers can order with confidence
and peace-of-mind knowing that your money will never be wasted on a product
that is sub-par. If you feel that the results of your supplements are
insufficient, please contact us and we will make sure that the situation is
remedied quickly and fairly.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b>SHIPPING
POLICY</b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
ship Monday through Saturday. All orders are shipped by ARAMAX.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Once
you have the tracking number please allow time for it to enter the carrier’s
system and update.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
strive to uphold the highest levels of quality customer service. It is our
ultimate goal at Transform Sports Nutrition (TSN) to create life-long customers
and we will do everything in our power to ensure that your experience with us
is one that makes you want to shop with us again and again.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">ORDER PROCESSING</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Our
standard order processing time is 1 to 3 business days for all items that are
in stock. Most orders that clear payment authorization and are placed before
1:00pm and will be processed the same day. Processing times are not calculated
into the estimated delivery times.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Orders
are not processed on weekends or Public and National holidays. Extra time may
be required for larger orders, international orders, orders requiring
additional customer information.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">INTERNATIONAL CUSTOMS</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
are responsible to know the import regulations for your country. Please
research all supplements to ensure that they are legal for importation PRIOR to
ordering. Once the order has left our premises we cannot be held responsible
for customs delays. Most countries will have no trouble but we post this
disclaimer in the off chance that there is an issue.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If, by
chance, the order is returned to us and you wish to have a refund then we will
do so only after we have received the goods back. The refund will be calculated
as Total cost less shipping and a 15% restocking fee for a total of an 85%
refund not including shipping.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
provide tracking information for all orders so please be diligent and keep
track of your orders' progress through the mail system.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We do
not have a reship policy. We DO assume that all clients are aware of their own countries'
laws and therefore you are responsible for dealing with your countries'
customs.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Please
know your own import regulations before ordering. It is your responsibility. If
customs does not return the products to us we cannot issue the 85% refund to
you.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If
your order is denied entry into your country, we strongly advise attempting to
have the items sent back to us and we will refund your order less shipping and
a 15% restocking fee.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><b><span lang="EN-US">RETURN/REFUND POLICY</span></b></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We
will accept returns if it is due to damage that is our fault or manufacturer
defects within 10 days of original purchase.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If you
are missing an item or the item that you receive is damaged, then please
contact us and we will send out a replacement free of any shipping costs to
you. You must notify us within 3 days of receipt of shipment.&nbsp;</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If a
refund is deemed necessary, then we will manually refund your transaction for
the appropriate amount for the returned items. Chargebacks will not be taken
lightly and will subject the client to being put on the no-sell list. Refunds
will only be issued after the items have been returned to us. Please always
contact us first because most issues can be resolved very easily.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">All
Items are shipped safely and in a manner that ensures safe arrival. If for any
reason this is not the case, then please email us and we will work with you.
Each case is handled individually.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If a
product is missing from your order, then please contact us and once we verify
the legitimacy of the claim then we will send the missing item to you. All
orders are documented and are photographed prior to shipping to avoid any
fraudulent claims.</p>	   
                        </div>
                     </div>
            </div>
        </div>
	
		<!-- End Brand Area -->
	<?php $this->load->view('footer');?>
    </body>
</html>
