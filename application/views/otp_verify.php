   <?php $this->load->view('header'); ?>
   <div class="login-register-area ptb-100">
       <div class="container">
           <div class="row">
               <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                   <div class="login-register-wrapper">
                       <div class="login-form-container">
                           <h5 class="otp_message_html center">OTP has been sent to <b><?php echo $prs_email; ?></b></h5>
                           <br>
                           <div class="login-register-form otp-verification-form">
                               <form id="otp_verification" class="center">
                                   <input type="hidden" name="prs_id" id="prs_id" value="<?php echo $prs_id; ?>">
                                   <!-- <input type="hidden" name="prs_mob" id="prs_mob" value="<?php echo $prs_mob; ?>"> -->
                                   <input type="hidden" name="prs_email" id="prs_email" value="<?php echo $prs_email; ?>">
                                   <input type="hidden" name="otp_id" id="otp_id" value="">
                                   <input type="text" id="otp_code" name="otp_code" required="" data-msg="Please Enter OTP" placeholder="Enter OTP" class="otp_message_code">
                                   <div class="col-md-12 pt-20 button-box">
                                       <button type="submit" class="otp_btn_verify">Verify</button>
                                       <button type="button" class="otp_resend back-btn" style="display: none;">Resend OTP</i></button>
                                       <button type="button" class="btn btn_processing" style="display: none;"><i class='fa fa-spinner'></i> Sending OTP...</i></button>
                                       <button type="button" class="back-btn" onclick="goBack(1);">Cancel</i></button>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   <?php $this->load->view('footer'); ?>
   <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/form_validation/otp_transaction.js"></script>
   </body>

   </html>