<?php $this->load->view('header'); ?>

<div class="login-register-area pb-100 pt-20">
    <div class="container">
        <div class="row" style="box-shadow: 5px 10px 18px #888888;">
            <div class="col-lg-8 col-md-12 ml-auto mr-auto">
                <div class="login-register-wrapper">
                    <div class="login-register-tab-list nav mt-45 mb-0">
                        <a class="active" data-toggle="tab" href="#lg1" id="login">
                            <h4> login </h4>
                        </a>
                        <a data-toggle="tab" href="#lg2" id="register">
                            <h4> register </h4>
                        </a>
                    </div>
                    <div class="tab-content">
                        <div id="lg1" class="tab-pane active">
                            <div class="login-form-container login-form">
                                <div class="login-register-form">
                                    <form id="login_form" method="post">
                                        <input type="hidden" id="ref" value="<?php echo $ref; ?>">
                                        <input type="text" name="usr_username" id="usr_username" placeholder="Email/Mobile number">
                                        <p id="not-match-error" class="errormesssage" style="display:none">Your username or password is incorrect</p>
                                        <input type="password" name="usr_password" id="usr_password" placeholder="Password">
                                        <div class="button-box">
                                            <label class="rememberme mt-checkbox mt-checkbox-outline" style="color:#D13132">
                                                <input type="checkbox" name="rememberme" id="rememberme" value="1" /> Remember me
                                            </label>
                                            <div class="login-toggle-btn">
                                                <a href="javascript:;" class="forget_pass_button">Forgot Password?</a>
                                            </div>
                                            <button type="submit" id="login_btn_save">Login</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="lg2" class="tab-pane">
                            <div class="login-form-container">
                                <div class="login-register-form">
                                    <form id="register_form" method="post">
                                        <input type="hidden" id="reg_ref" value="<?php echo $ref; ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="register_register_prs_name" id="register_prs_name" placeholder="Full Name *" required="" data-msg="Please Enter Name">
                                            </div>
                                            <div class="col-md-6">
                                                <input name="register_prs_email" id="register_prs_email" placeholder="Email *" type="email" data-msg="Please Enter Email">
                                            </div>

                                            <div class="col-md-6">
                                                <input type="password" name="register_prs_password" id="register_prs_password" placeholder="Password *" data-msg="Please enter password">
                                            </div>

                                            <!-- <div class="col-md-6">
                                          <input type="password" name="register_prs_cnfrm_password" id="register_prs_cnfrm_password" placeholder="Re-Enter Password *" data-msg="Please re-enter password">
                                           </div>
 -->
                                            <div class="col-md-6">
                                                <input name="register_prs_mob" id="register_prs_mob" placeholder="Mobile *" type="text" data-msg="Please Enter Mobile No.">
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_comp_name" id="register_comp_name" placeholder="Company Name*" required="" data-msg="Please Enter Company Name*">
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_prs_contact_name" id="register_prs_cont_name" placeholder="Contact person*" required="" data-msg="Please Enter Contact person Name*">
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_prs_country" id="register_prs_country" placeholder="Country*" required="" data-msg="Please Enter Country*">
                                            </div>

                                            <div class="col-md-6">
                                                <select class="form-control" id="register_prs_state" name="register_prs_state" data-msg="Please select state" required>
                                                    <option value="" selected disabled>Select State *</option>
                                                    <?php echo getDropdownResult('state-dropdown', 'pad_state', 'pad_state'); ?>
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_prs_dist" id="register_prs_dist" placeholder="District*" required="" data-msg="Please Enter District*">
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_prs_city" id="register_prs_city" placeholder="City / Town*" required="" data-msg="Please Enter City / Town*">
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_prs_pincode" id="register_prs_pincode" placeholder="Pincode*" required="" data-msg="Please Enter Pincode*">
                                            </div>

                                            <div class="col-md-6">
                                                <textarea name="register_prs_address" id="register_prs_address" placeholder="Full Address*" required="" data-msg="Please Enter Full Address*"></textarea>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" name="register_prs_landmark" id="register_prs_landmark" placeholder="Landmark" data-msg="Please Enter Landmark">
                                            </div>



                                            <div class="col-md-6">
                                                <input name="register_prs_whatsapp" id="register_prs_whatsapp" placeholder="Whatsapp No *" required="" type="text" data-msg="Please Enter Whatsapp No.*">
                                            </div>

                                            <div class="col-md-6">
                                                <input name="register_prs_gst" id="register_prs_gst" placeholder="GST No" type="text">
                                            </div>




                                        </div>
                                        <div class="button-box pt-20">
                                            <button type="submit" id="register_btn_save"><span>Register</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-form-container" id="forget_pass_form_div" style="display: none;">
                        <!-- BEGIN FORGOT PASSWORD FORM -->
                        <div class="login-register-form">
                            <form class="forget-form" id="forget_pass_form" method="post">
                                <h4 class="center">Forgot Password ?</h4>
                                <p class="center">We will send you a link to reset your password.</p>
                                <p class="light-blue-color block" id="error"></p>
                                <input type="email" autocomplete="off" placeholder="Enter email address" name="fpwd_email" id="fpwd_email" required="" data-msg="Please Enter Email Id">

                                <div class="button-box pt-20">
                                    <button type="button" class="back-btn" id="back-btn">Back </button>
                                    <button type="submit" id="fpwd_btn_save">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- END FORGOT PASSWORD FORM -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/login.js"></script>
</body>

</html>