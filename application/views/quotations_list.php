<?php $this->load->view('header'); ?>

<div class="container pb-100">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('my-quotations') ?>">My Quotations</a></li>
    </ul>
  </div>
  <div class="text-right">
    <a class="btn btn-sm btn-danger mb-2 border-radius-3" href="<?php echo site_url('quotation-request') ?>"> Get Quotation</a>
  </div>
  <div class="text-center">
    <h3 style="padding-bottom: 10px; padding-top: 10px; color: white; background-color: #000">My Quotations</h3>
  </div>
  <table class="table table-hover table-responsive-sm table-responsive-md" id="dTable" style="width: 100%; background-color: #efefef  !important;">
    <thead>
      <tr>
        <th scope="col">Sr.No</th>
        <th>Quotation Id</th>
        <th>Requested On</th>
        <th>Product Name</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (!empty($quotationsDetails) && $quotationsDetails != '') {
        $srNo = 1;
        foreach ($quotationsDetails as $row) {
          $fullDescription = $row->qr_paper_gsm_size . ', Lamination Type:' . $row->qr_lamination_type . ', Lamination Side:' . $row->qr_lamination . ', Creasing:' . $row->qr_creasing . ', Half Cut:' . $row->qr_half_cut . ', UV:' . $row->qr_uv;
      ?>
          <tr>
            <td scope="row"> <?php echo $srNo; ?> </td>
            <td> <?php echo $row->qr_reference_no; ?> </td>
            <td> <?php echo date("d-m-Y", strtotime($row->qr_created_on)); ?> </td>
            <td> <?php echo $row->qr_product_name; ?> </td>
            <td title="<?php echo $fullDescription; ?>">
              <?php
              echo strlen($fullDescription) > 30 ? substr($fullDescription, 0, 30) . "..." : $fullDescription;
              ?>
            </td>
            <td> <?php echo $row->qr_price ? '<i class="fa fa-rupee"></i>' . $row->qr_price : '-'; ?> </td>
            <td>
              <a class="order-detail" href="<?php echo site_url('quotation-detail/' . $row->qr_reference_no) ?>">Show Detail</a>
            </td>

            <!-- <td> <a class="text-info" target="_blank" rel="noopener noreferrer" href="<?php echo base_url() . COMPLAINT_UPLOAD_DIRECTORY . $row->com_image ?>"> <?php echo $row->com_image; ?> </a></td> -->
          </tr>
        <?php
          $srNo++;
        }
      } else {
        ?>
        <tr>
          <td colspan="7" class="text-center">No data available in table</td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>

<script>
  $(document).ready(function() {
    $('#dTable').DataTable({

    });
  });
</script>

</body>

</html>