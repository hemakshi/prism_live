<?php $this->load->view('header'); ?>

<!-- Contact Area Start -->
<div class="contact-us pb-20">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="<?php echo site_url('home') ?>">Home</a></li>
                <li>Contact</li>
            </ul>
        </div>
        <div class="row">
            <!-- Contact Form Area Start -->
            <div class="col-lg-6 pb-50">
                <div class="bold small-title mb-30 ">
                    <h2>Contact Form</h2>
                    <!-- <p></p> -->
                </div>
                <form id="contact_form" method="POST">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="contact-form-style mb-20">
                                <input name="contact_name" id="contact_name" placeholder="Full Name" type="text" required data-msg="Please Enter Full Name">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact-form-style mb-20">
                                <input name="contact_email" id="contact_email" placeholder="Email Address" type="email" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact-form-style mb-20">
                                <input name="contact_mob" id="contact_mob" placeholder="Mobile Number" type="text" required data-msg="Please Enter Mobile Number">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-form-style">
                                <textarea name="contact_msg" id="contact_msg" placeholder="Message" data-msg="Please Enter Message"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="contact-form-style">
                                <button type="submit" id="btn_save">SEND MESSAGE</button>
                            </div>
                        </div>
                    </div>
                </form>
                <p class="form-messege"></p>
            </div>
            <!-- Contact Form Area End -->
            <!-- Contact Address Strat -->
             <div class="col-lg-4">           
            <!-- <div class="small-title mb-30 mt-30"> -->
                <!-- <h3 class="bold">Prism Prints </h3> -->
                <!-- <div class="col-lg-12"> -->
                   <!-- <h2>testing</h2> -->
                    <div class="bold small-title mb-30 ">
                    <h2>Prism Prints</h2>
                    <!-- <p></p> -->
                </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact-information mb-30">
                                <h4>Our Address</h4>
                                <p>16, Vijay Plot Main Road, Gondal Road, <br>Rajkot.- 360002</p>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="contact-information contact-mrg mb-30">
                                <h4>Contact Us</h4>
                                <p>
                                    <a href="tel:7359247000"><i class="fa fa-phone mr-2" aria-hidden="true"></i> +91 73592 47000 </a>
                                </p>
                                <p>
                                    <a href="tel:9879698793"><i class="fa fa-phone mr-2" aria-hidden="true"></i> +91 98796 98793 </a>
                                </p>
                                <p>
                                    <a href="mailto:info@prismprints.in"><i class="fa fa-envelope mr-2" aria-hidden="true"></i> info@prismprints.in</a>
                                </p>
                                <p>
                                    <a href="http://prismprints.in/" target="_blank"><i class="fa fa-globe mr-2" aria-hidden="true"></i> www.prismprints.in</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Contact Address Strat -->
            <!-- </div> -->
        </div>
    </div>
</div>

<!-- Contact Area Start -->
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/contact_us.js"></script>
</body>

</html>