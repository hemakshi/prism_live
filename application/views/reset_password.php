   <?php $this->load->view('header'); ?>
   <div class="portlet">
   	<div class="row">
   		<div class="container-fluid login-form-container">
   			<div class="text-center title_wrap ">
   				<h3 class="page-title text-center"> Reset Password </h3>
   				<span class="sp_line color-primary"> </span>
   			</div>
   			<div class="login-form-container forget_pass_form_div">
   				<div class="col-lg-7 col-md-12 ml-auto mr-auto">
   					<form class="" id="reset_password_form" method="post">
   						<input type="hidden" name="prs_id" id="prs_id" value="<?php echo $prs_id ?>">
   						<?php if ($check == '1') {  ?>
   							<div class="row">
   								<div class="col-md-6">
   									<div class="form-group">
   										<label>Password
   											<span class="asterix-error"><em>* </em> </span>
   										</label>
   										<input type="password" name="prs_password" id="prs_password" class="form-control" value="">
   									</div>
   								</div>
   								<div class="col-md-6">
   									<div class="form-group">
   										<label>Confirm Password
   											<span class="asterix-error"><em>* </em> </span>
   										</label>
   										<input type="password" name="prs_confirm_pass" id="prs_confirm_pass" class="form-control" value="">
   										<div class="help-block">
   										</div>
   									</div>
   								</div>
   							</div>
   							<div class="form-actions">
   								<button type="button" id="back-btn" class="btn red btn-outline forget_pass_button">Back </button>
   								<button type="submit" id="reset_pwd_btn_submit" class="btn btn_save">Submit&nbsp;<i class="fa fa-check"></i></button>
   								<button type="button" class="btn btn_processing" style="display: none;"><i class='fa fa-spinner'></i> Submitting in progress...</i></button>
   							</div>
   						<?php } else { ?>
   							<h3 class="text-center text-danger"><?php echo $msg ?></h3>
   						<?php } ?>
   					</form>
   				</div>

   			</div>
   		</div>
   	</div>
   </div>
   <?php $this->load->view('footer'); ?>
   <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/form_validation/reset_password.js"></script>
   </body>

   </html>