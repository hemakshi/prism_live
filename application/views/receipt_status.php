<?php $this->load->view('header'); ?>

<div class="container pb-100">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('receipt_status') ?>">Receipt Status</a></li>
    </ul>
  </div>
  <div class="text-center ">
    <h3 style="padding-bottom: 10px;
    padding-top: 10px; color: white; background-color: #000">Receipt Status</h3>
  </div>
  <table class="table table-hover table-responsive-sm table-responsive-md" id="dTable" style="width: 100%; background-color: #efefef  !important;">
    <thead>
      <tr>
        <th scope="col">Sr.No</th>
        <th>Receipt No.</th>
        <th>Payment Type</th>
        <th>Payment Mode</th>
        <th>Receipt Amount</th>
        <th>Receipt Image</th>
        <th>Status</th>
        <th>Remarks</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (!empty($receiptData) && $receiptData != '') {
        $srNo = 1;
        foreach ($receiptData as $row) {
      ?>
          <tr>
            <td scope="row"> <?php echo $srNo; ?> </td>
            <td> <?php echo $row->rcpt_number ?> </td>
            <td> <?php echo $row->pay_type ?> </td>
            <td> <?php echo $row->pay_mode ?> </td>
            <td> <?php echo $row->pay_amt ?> </td>
            <td> <a class="text-info" target="_blank" rel="noopener noreferrer" href="<?php echo base_url() . RECEIPT_UPLOAD_DIRECTORY . $row->rcpt_img ?>"> <?php echo $row->rcpt_img; ?> </a></td>
            <td>
              <?php
              switch ($row->status) {
                case RECEIPT_STATUS_PENDING:
                  echo "Pending";
                  break;
                case RECEIPT_STATUS_APPROVE:
                  echo "Approve";
                  break;
                case RECEIPT_STATUS_REJECT:
                  echo "Reject";
                  break;
                default:
                  echo "Pending";
                  break;
              }
              ?>
            </td>
            <td> <?php echo $row->pay_remarks ? $row->pay_remarks : '-' ?> </td>
            <td> <?php echo date("d-m-Y", strtotime($row->update_on)); ?> </td>
          </tr>
        <?php
          $srNo++;
        }
      } else {
        ?>
        <tr>
          <td colspan="9" class="text-center">No data available in table</td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>

<script>
  $(document).ready(function() {
    $('#dTable').DataTable({

    });
  });
</script>


</body>

</html>