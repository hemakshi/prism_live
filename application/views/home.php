<?php $this->load->view('header');?>
        <!-- Slider Start -->
        <div class="slider-area">
            <div class="slider-active owl-dot-style owl-carousel">
                <?php foreach ($banner_images as $banner_images_key) { 
                $link = '';
                if($banner_images_key->gls_link) {
                  $link=  $banner_images_key->gls_link;  
                }
                ?>
             <a href="<?php echo $link?>">
               <div class=" bg-img" >
                    <img src="<?php echo base_url(GALLERY_SET_IMAGE).$banner_images_key->gls_image;?>">
                   <!--  <div class="container">
                        <div class="slider-content slider-animated-1">
                          <p class="animated theme"><?php echo $banner_images_key->gls_name; ?></p>
                        </div>
                    </div> -->
                </div>
                <?php } ?>
                </a>
                
            </div>
        </div>
        <!-- Slider End -->
    
        <!-- New Products Start -->
        <div class="product-area  pt-90 pb-65"  >
            <div class="container">
                <div class="product-top-bar section-border mb-55">
                    <div class="section-title-wrap text-center hd" >
                        <h3 class="section-title" style="color:white" >Our Products</h3>
                    </div>
                </div>
                <div class="tab-content jump">
                        <!-- Shop Page Area Start -->
        <div class="shop-page-area">
            <div class="container">
                <div class="row flex-row-reverse">
                    <div class="col-lg-12">
                    <div class="grid-list-product-wrapper ">
                            <div class="product-grid product-view pb-20">

                                

                                <div class="row ">
                                    
                                     <div class="col-md-4 pb-40 d-flex justify-content-center" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">
                                       <a href="<?php echo site_url('products/visiting-card')?>">
                                        <img class="card-img-top" src="assets/img/product/Business Card.png" alt="Card image">
                                       <!--  <div class="card-body">
                                        <center><h4 class="card-title">VISITING CARD  </h4></center> 
                                    </div> -->
                                    </a>
                                    </div>
                                    </div> 

                                <div class="col-md-4 pb-40 d-flex justify-content-around" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">
                                       <a href="<?php echo site_url('products/letterhead')?>">
                                        <img class="card-img-top" src="assets/img/product/Letterhead.png" alt="Card image">
                                        <!-- <div class="card-body">
                                        <center><h4 class="card-title">LETTERHEAD</h4></center>
   
                                    </div> -->
                                </a>
                                    </div>
                                    </div> 
                                   
 
                                <div class="col-md-4 pb-40 d-flex justify-content-around" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">
                                       <a href="<?php echo site_url('products/envelope')?>">
                                        <img class="card-img-top" src="assets/img/product/Envelope.png" alt="Card image">
                                      <!--   <div class="card-body">
                                        <center><h4 class="card-title">ENVELOPE</h4></center>
   
                                    </div> -->
                                </a>
                                    </div>
                                    </div> 
                                    

                                   
                                    <!--  -->
                                 <div class="col-md-4 pb-40 d-flex justify-content-around" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">
                                       <a href="<?php echo site_url('products/sticker')?>">
                                        <img class="card-img-top" src="assets/img/product/Sticker.png" alt="Card image">
                                      <!--   <div class="card-body">
                                        <center><h4 class="card-title">STICKER</h4></center>
   
                                    </div> -->
                                </a>
                                    </div>
                                    </div> 

                                    <div class="col-md-4 pb-40 d-flex justify-content-around" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">

                                        <a href="<?php echo site_url('products/pamphlet-flyers')?>">
                                        <img class="card-img-top" src="assets/img/product/Flyer.png" alt="Card image">
                                       <!--  <div class="card-body">
                                        <center><h4 class="card-title">PAMPHLET / FLYERS</h4></center>
                                    </div> -->
                                </a>
                                    </div>
                                    </div> 

                                    <div class="col-md-4 pb-40 d-flex justify-content-around" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">
                                        <a href="<?php echo site_url('products/brochure')?>">
                                        <img class="card-img-top" src="assets/img/product/Brochure.png" alt="Card image">
                                       <!--  <div class="card-body">
                                        <center><h4 class="card-title">BROCHURE</h4></center>
                                    </div> -->
                                </a>
                                    </div>
                                    </div> 

                                <div class="col-md-4 pb-40 d-flex justify-content-around" >
                                       
                                       <div class="card" style="width:300px; box-shadow: 5px 10px 18px #888888;">
                                       <a href="<?php echo site_url('products/brochure-mix')?>">
                                        <img class="card-img-top" src="assets/img/product/Brochure.png" alt="Card image">
                                       <!-- <div class="card-body"> -->
                                        <!-- <center><h4 class="card-title">BROCHURE (Mix) </h4></center> -->
   
                                    <!-- </div>  -->
                                </a>
                                    </div>
                                    </div> 

                                    
                                    </div>
                            </div>
                           
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
        <!-- Shop Page Area Start -->
            </div>
        </div>
    </div>
        <!-- New Products End -->
        <!-- Testimonial Area Start -->
        <!-- <div class="testimonials-area bg-img pt-100 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="testimonial-active owl-carousel">
                            <div class="single-testimonial text-center">
                                <div class="testimonial-img">
                                    <img alt="" src="<?php echo base_url();?>assets/img/icon-img/testi.png">
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed do eiusmod tempor incididunt ut labore</p>
                                <h4>Gregory Perkins</h4>
                                <h5>Customer</h5>
                            </div>
                            <div class="single-testimonial text-center">
                                <div class="testimonial-img">
                                    <img alt="" src="<?php echo base_url();?>assets/img/icon-img/testi.png">
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed do eiusmod tempor incididunt ut labore</p>
                                <h4>Khabuli Teop</h4>
                                <h5>Marketing</h5>
                            </div>
                            <div class="single-testimonial text-center">
                                <div class="testimonial-img">
                                    <img alt="" src="<?php echo base_url();?>assets/img/icon-img/testi.png">
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed do eiusmod tempor incididunt ut labore </p>
                                <h4>Lotan Jopon</h4>
                                <h5>Admin</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Testimonial Area End -->
    <?php $this->load->view('footer');?>
    </body>
</html>
