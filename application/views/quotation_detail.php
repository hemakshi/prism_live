<?php $this->load->view('header'); ?>
<style>
  .table td {
    padding: .20rem;
  }

  label {
    margin: 5px;
  }
</style>
<div class="container">
  <div class="breadcrumb-content">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('my-quotations') ?>">My Quotations</a></li>
      <li><a href="">Quotation Details- <?php echo $quotationData->qr_reference_no; ?></a></li>
    </ul>
  </div>

  <div class="text-center">
    <h4 style="padding-bottom: 10px; padding-top: 10px;">Quotation Details - <?php echo $quotationData->qr_reference_no; ?></h4>
  </div>

  <div class="container">

    <div class="pb-50">
      <div class="form-row">
        <div class="col-lg-1"></div>

        <div class="col-lg-10">
          <table class="table table-bordered table-responsive-sm">
            <tr>
              <td><label>Quotation Id</label></td>
              <td><?php echo $quotationData->qr_reference_no; ?></td>
            </tr>
            <tr>
              <td><label>Request Date</label></td>
              <td><?php echo date("d-M-Y h:i:s A", strtotime($quotationData->qr_created_on)); ?></td>
            </tr>
            <tr>
              <td><label>Updated On</label></td>
              <td><?php echo date("d-M-Y h:i:s A", strtotime($quotationData->qr_updated_on)); ?></td>
            </tr>
            <tr>
              <td><label>Product Name</label></td>
              <td><?php echo $quotationData->qr_product_name; ?></td>
            </tr>
            <tr>
              <td><label>Description</label></td>
              <td>
                <?php
                $fullDescription = $quotationData->qr_paper_gsm_size . ', Lamination Type:' . $quotationData->qr_lamination_type . ', Lamination Side:' . $quotationData->qr_lamination . ', Creasing:' . $quotationData->qr_creasing . ', Half Cut:' . $quotationData->qr_half_cut . ', UV:' . $quotationData->qr_uv;
                echo $fullDescription;
                ?>
              </td>
            </tr>
            <tr>
              <td><label>Staff Description</label></td>
              <td> <?php echo $quotationData->qr_remarks; ?> </td>
            </tr>
            <tr>
              <td><label>HSN</label></td>
              <td> <?php echo $quotationData->qr_hsn ? $quotationData->qr_hsn : '-'; ?> </td>
            </tr>
            <tr>
              <td><label>Tax(%)</label></td>
              <td> <?php echo $quotationData->qr_tax ? $quotationData->qr_tax : '-'; ?> </td>
            </tr>
            <tr>
              <td><label>Quantity</label></td>
              <td><?php echo $quotationData->qr_qty; ?></td>
            </tr>
            <tr>
              <td><label>Price</label></td>
              <td> <?php echo $quotationData->qr_price ? $quotationData->qr_price : '-'; ?> </td>
            </tr>
            <tr>
              <td><label>Total Price(with tax)</label></td>
              <td> <?php echo $quotationData->qr_total_price ? $quotationData->qr_total_price : '-'; ?> </td>
            </tr>

          </table>
        </div>

        <div class="col-lg-1"></div>
      </div>
    </div>

  </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

</body>

</html>