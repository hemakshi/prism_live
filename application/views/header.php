<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $title ?></title>
    <meta name="description" content="">
    <meta name="robots" content="noindex, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.png">

    <!-- all css here -->
    <!-- <link href="https://fonts.googleapis.com/css2?family=Zilla+Slab:wght@500&display=swap" rel="stylesheet"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/meanmenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <link href="<?php echo base_url(); ?>assets/css/progress-wizard.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Datatable-Start -->    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/data_tables/datatables.min.css" />    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/css/buttons.dataTables.min.css" />        
    <!-- Datatable-Start -->

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: "Montserrat", sans-serif;
        }


        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            /* Safari */
            animation: spin 2s linear infinite;
        }

        .gray-bg {
            background: #c7c7c7;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .balance-title {
            color: #1b1b1b;
            display: inline-block;
            font-family: "Montserrat", sans-serif;
            font-size: 14px;
            font-weight: 600;
            line-height: 100px;
            text-transform: uppercase;
        }

        .balance-text {
            font-weight: bold;
            color: #d43230;
        }
    </style>
</head>

<body>
    <!-- header start -->
    <header class="header-area gray-bg clearfix">
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-6">
                        <div class="logo">
                            <a href="<?php echo site_url('home'); ?>">
                                <img alt="" src="<?php echo base_url(); ?>assets/images/logo.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-6">
                        <div class="header-bottom-right">
                            <div class="main-menu">
                                <nav>
                                    <ul>
                                        <li class="top-hover"><a href="<?php echo site_url('home'); ?>">Home</a>
                                        </li>
                                        <!-- <li><a href="<?php echo site_url('about'); ?>">About</a></li>                                         -->

                                        <?php if ($this->session->userdata(PROJECT_SESSION_ID)) {
                                            $product_category_active = $this->input->get('product_category');
                                            $menuActiveClass = '';
                                            if ($product_category_active != '') {
                                                $menuActiveClass = 'menu-active';
                                            }
                                        ?>

                                            <li class="top-hover <?php echo $menuActiveClass; ?>"><a href="">Product</a>
                                                <?php
                                                $product_category = $this->product_model->getProductCategories(SUPPLEMENTS_MENU);
                                                ?>
                                                <ul class="submenu">
                                                    <?php
                                                    $catActiveClass = '';
                                                    foreach ($product_category as $productCategorykey) {
                                                        $catActiveClass = '';
                                                        if ($productCategorykey->cat_slug == $product_category_active) {
                                                            $catActiveClass = 'menu-active';
                                                        }
                                                    ?>
                                                        <li class="<?php echo $catActiveClass; ?>"> <a href="<?php echo site_url('products/') . $productCategorykey->cat_slug; ?>"><?php echo $productCategorykey->cat_name; ?></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php
                                            $product_category = $this->product_model->getProductCategories(SHOP_MENU);
                                            if (!empty($product_category)) {

                                            ?>
                                                <li class="top-hover <?php echo $menuActiveClass; ?>"><a href="">Shop</a>
                                                    <ul class="submenu">
                                                        <?php
                                                        $catActiveClass = '';
                                                        foreach ($product_category as $productCategorykey) {
                                                            $catActiveClass = '';
                                                            if ($productCategorykey->cat_slug == $product_category_active) {
                                                                $catActiveClass = 'menu-active';
                                                            }
                                                        ?>
                                                            <li class="<?php echo $catActiveClass; ?>"> <a href="<?php echo site_url('shop/') . $productCategorykey->cat_slug; ?>"><?php echo $productCategorykey->cat_name; ?></a> </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php }
                                            ?>
                                        <?php } ?>

                                        <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?>
                                            <li><a href="<?php echo site_url('price_list'); ?>">Price List</a></li>
                                        <?php } ?>

                                        <li><a href="<?php echo site_url('contact'); ?>">contact</a></li>

                                        <?php if ($this->session->userdata(PROJECT_SESSION_ID)) {
                                            /* GET WALLET DETAILS- START */
                                            $getWalletDetailByPersonId_result = $this->order_model->getWalletDetailByPersonId($this->session->userdata(PROJECT_SESSION_ID));
                                            $wallet_detail = json_decode($getWalletDetailByPersonId_result, true);
                                            $walletBalance = $wallet_detail['amount'] != '' ? $wallet_detail['amount'] : 0;
                                            $this->session->set_userdata(PROJECT_SESSION_BALANCE, $walletBalance);
                                            /* GET WALLET DETAILS- END */
                                        ?>

                                            <li>
                                                <a href="<?php echo site_url('add_balance') ?>">BALANCE</a>
                                                <i class="fa fa-rupee"></i>
                                                <span class="balance-text">
                                                    <?php if ($this->session->userdata(PROJECT_SESSION_BALANCE) != '') {
                                                        echo $this->session->userdata(PROJECT_SESSION_BALANCE);
                                                    } else {
                                                        echo "0";
                                                    } ?>
                                                </span>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </nav>
                            </div>
                            <div class="header-currency dn">
                                <span class="digit">
                                    <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?>
                                        <a><?php if ($this->session->userdata(PROJECT_SESSION_NAME) != '') {
                                                echo $this->session->userdata(PROJECT_SESSION_NAME);
                                            } else {
                                                echo 'User';
                                            } ?>
                                        </a>
                                        <i class="ti-angle-down"></i>
                                </span>
                                <div class="dollar-submenu">
                                    <ul>
                                        <li><a href="<?php echo base_url('order_history') ?>"> MY ORDERS </a></li>
                                        <li><a href="<?php echo site_url('upload_receipt'); ?>">UPLOAD RECEIPT </a></li>
                                        <li><a href="<?php echo base_url('receipt_status') ?>"> RECEIPT STATUS</a></li>
                                        <li><a href="<?php echo base_url('my-quotations') ?>">GET QUOTATION</a></li>
                                        <li><a href="<?php echo base_url('my-complaints') ?>">MY COMPLAINTS</a></li>
                                        <li><a href="<?php echo base_url('ledger') ?>">LEDGER</a></li>
                                        <li><a href="<?php echo base_url('profile') ?>"> PROFILE</a></li>
                                        <li><a href="<?php echo base_url('change_password') ?>">CHANGE PASSWORD</a></li>
                                        <li><a href="<?php echo base_url('logout') ?>">LOGOUT</a></li>
                                    </ul>
                                </div>
                            <?php } else { ?>
                                <a href="<?php echo base_url('login') ?>">Login</a>
                            <?php } ?>
                            </div>
                            <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?>
                                <div class="header-cart ml-cart">
                                    <a href="<?php echo site_url('cart'); ?>">
                                        <div class="cart-icon">
                                            <i class="ti-shopping-cart"></i>
                                            <?php
                                            if (count($this->cart->contents()) > 0) {
                                                echo ' <span  id="cart-count" class="badge">' . count($this->cart->contents()) . '</span>';
                                            } else {
                                                echo ' <span  id="cart-count" class="badge"></span>';
                                            }
                                            ?>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="mobile-menu-area">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul>
                                <li class="top-hover"><a href="<?php echo site_url('home'); ?>">home</a>
                                </li>

                                <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?>
                                    <?php
                                    $product_category_active = $this->input->get('product_category');
                                    $menuActiveClass = '';
                                    if ($product_category_active != '') {
                                        $menuActiveClass = 'menu-active';
                                    } ?>
                                    <li class="top-hover <?php echo $menuActiveClass; ?>"><a href="">Products</a>
                                        <?php
                                        $product_category = $this->product_model->getProductCategories();
                                        ?>
                                        <ul class="submenu">
                                            <?php
                                            $catActiveClass = '';
                                            foreach ($product_category as $productCategorykey) {
                                                $catActiveClass = '';
                                                if ($productCategorykey->cat_slug == $product_category_active) {
                                                    $catActiveClass = 'menu-active';
                                                }
                                            ?>
                                                <li class="<?php echo $catActiveClass; ?>"> <a href="<?php echo site_url('products/') . $productCategorykey->cat_slug; ?>"><?php echo $productCategorykey->cat_name; ?></a> </li>
                                            <?php } ?>
                                        </ul>
                                    </li>


                                    <li><a href="<?php echo site_url('price_list'); ?>">Price List</a></li>
                                <?php } ?>

                                <li><a href="<?php echo site_url('contact'); ?>">contact</a></li>
                                <!-- <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?> 
                                    <li>
                                        <a>Balance <i class="fa fa-rupee"></i>
                                            <span>
                                                <?php if ($this->session->userdata(PROJECT_SESSION_BALANCE) != '') {
                                                    echo $this->session->userdata(PROJECT_SESSION_BALANCE);
                                                } else {
                                                    echo "0";
                                                } ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php } ?> -->
                            </ul>
                        </nav>
                    </div>
                    <div class="dn1">
                        <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?>
                            <a>Balance <i class="fa fa-rupee"></i>
                                <span>
                                    <?php if ($this->session->userdata(PROJECT_SESSION_BALANCE) != '') {
                                        echo $this->session->userdata(PROJECT_SESSION_BALANCE);
                                    } else {
                                        echo "0";
                                    } ?>
                                </span>
                            </a>
                        <?php } ?>

                        <div class="header-currency1 ">
                            <span class="digit">
                                <?php if ($this->session->userdata(PROJECT_SESSION_ID)) { ?>
                                    <a><?php if ($this->session->userdata(PROJECT_SESSION_NAME) != '') {
                                            echo $this->session->userdata(PROJECT_SESSION_NAME);
                                        } else {
                                            echo 'User';
                                        } ?>
                                    </a>
                                    <i class="ti-angle-down"></i>
                            </span>
                            <div class="dollar-submenu">
                                <ul>
                                    <!-- <li><a href="<?php echo base_url('my-orders') ?>"> Orders</a></li> -->
                                    <!-- <li><a href="<?php echo base_url('profile/edit') ?>"> Edit profile</a></li> -->
                                    <li><a href="<?php echo base_url('order_history') ?>"> ORDER HISTORY</a></li>
                                    <li><a href="<?php echo base_url('add_balance') ?>"> ADD BALANCE</a></li>
                                    <li><a href="<?php echo site_url('upload_receipt'); ?>">UPLOAD RECEIPT </a></li>
                                    <li><a href="<?php echo base_url('receipt_status') ?>"> RECEIPT STATUS</a></li>
                                    <li><a href="<?php echo base_url('ledger') ?>">LEDGER</a></li>
                                    <li><a href="<?php echo base_url('profile') ?>"> PROFILE</a></li>
                                    <li><a href="<?php echo base_url('change_password') ?>"> CHANGE PASSWORD</a></li>
                                    <li><a href="<?php echo base_url('logout') ?>">LOGOUT</a></li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <a href="<?php echo base_url('login') ?>">Login</a>
                        <?php } ?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header end -->