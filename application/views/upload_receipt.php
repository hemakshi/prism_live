<?php $this->load->view('header'); ?>

<div class="container ">
  <div class="breadcrumb-content ">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('upload_receipt') ?>">Upload Receipt</a></li>
    </ul>
  </div>
  <div class="text-center">
    <h3 style="padding-bottom: 10px; padding-top: 10px; color: white; background-color: #000">Upload Receipt</h3>
  </div>
  <!-- <hr>/ -->

  <div class="container">
    <p class="text-danger mt-4">
      <u>Terms & Conditions :</u><br>
      Please Check Carefully Amount & Receipt Amount Should Be Match Otherwise Your Account Will Be Block
    </p>
    <div class="pb-100 ">
      <form id="receipt_upload_form" name="receipt_upload_form" enctype="multipart/form-data" method="POST">
        <div class="form-row">
          <div class="col-md-4 mb-3 contact-form-style">
            <label for="validationDefault04">Payment Type</label>
            <select name="payment_type" class="custom-select" id="drop1">
              <option selected disabled value="">Select Paytment Type</option>
              <option value="Bank">Bank</option>
              <option value="Cash">Cash</option>
              <option value="Wallet">Wallet</option>
            </select>
            <div class="pt-20">

              <label for="validationDefault04">Payment Mode</label>
              <select name="payment_mode" class="custom-select" id="drop2" required>
                <option selected disabled value="">Select Payment Mode</option>
                <option data-parent_id="Bank" value="Bank of Baroda">Bank of Baroda</option>

                <option data-parent_id="Wallet" value="Paytm">Paytm</option>
                <option data-parent_id="Wallet" value="Google Pay">Google Pay</option>
                <option data-parent_id="Cash" value="Cash Angadiyu/Office Credit">Cash Angadiyu/Office Credit</option>
              </select>
            </div>


          </div>

          <div class="col-md-4 mb-3 contact-form-style">
            <label for="validationDefault03">Paid Amount</label>
            <input type="text" name="payment_amount" class="form-control" id="validationDefault03" required placeholder="Amount *">
            <div class="pt-20">
              <label for="validationDefault03">Receipt Image</label>
              <div class="custom-file mb-3">
                <input type="file" name="receipt_file" required class="custom-file-input" id="receipt_file" onchange="return fileValidation()">
                <label class="custom-file-label" for="receipt_file">Choose file</label>
              </div>
            </div>


          </div>


         
        </div>


        <div class="form-row ">
          <div class="col-md-4 mb-3 contact-form-style">
            <label for="pay_remarks">Remarks</label>
            <textarea name="pay_remarks" id="pay_remarks" required placeholder="Remarks *" title="Remarks" tabindex="2" data-msg="Please Enter Remarks"></textarea>
          </div>

          <div class="col-md-4 mb-3 contact-form-style">
            <div class="text-center pt-20">
              <button class="btn btn-danger mt-20" id="btn-upload" type="submit" style="line-height: 0.2; border-radius: 17px; padding: 17px 31px;">Upload</button>
            </div>
          </div>
           <div class="col-md-4 mb-3 ">
            <img class="mx-auto d-block" src="assets/img/product/Qrcode.png">
          </div>

        </div>



      </form>

    </div>
  </div>
</div>
<?php $this->load->view('footer'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>

<script>
  var isReceiptUploadFormValid = false;
  $(function() {
    var validator = $("#receipt_upload_form").validate({
      rules: {
        payment_type: {
          required: true
        },
        payment_mode: {
          required: true
        },
        bank_name: {
          required: true
        },
        payment_amount: {
          required: true,
          number: true,
          min: 1
        },
        receipt_file: {
          required: true
        }
      },
      messages: {
        payment_type: {
          required: "Please Select Payment Type"
        },
        payment_mode: {
          required: "Please Select Payment Mode"
        },
        bank_name: {
          required: "Please Select Bank Name"
        },
        payment_amount: {
          required: "Please Enter Payment Amount"
        },
        receipt_file: {
          required: "Please Select Payment Receipt"
        }
      },
      errorClass: "errormesssage",
      submitHandler: function(form) {
        isReceiptUploadFormValid = true;
      }
    });
  });

  $(document).ready(function() {
    $("#receipt_upload_form").submit(function(e) {
      if (isReceiptUploadFormValid == true) {
        try {
          e.preventDefault();
          var checkoutForm = new FormData(this);
          $.ajax({
            url: base_url + "Upload/do_upload",
            data: checkoutForm,
            type: 'POST',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            async: false,
            beforeSend: function() {
              $('#result').html('Processing...');
            },
            success: function(data) {
              if (data.success == true) {
                alert(data.message);
                location.reload();
              } else {
                alert(data.message);
                window.location.href = data.linkn;
              }
            }
          });
          e.preventDefault();
        } catch (e) {
          console.log(e);
        }
      } else {
        e.preventDefault();
      }
    });

    $(document).on("change", "#drop1", function() {
      var val = $(this).val();
      $("#drop2 option").hide();
      $.each($("#drop2 option"), function(key, value) {
        var data = $(this).data("parent_id");
        if (data) {
          if ($.trim(val) == $.trim(data)) {
            $(this).show();
          }
        }
        if (val == "Bank") {
          $("#dd_bank_name").show();
        } else {
          $("#dd_bank_name").hide();
        }
      });
      if (val == "") {
        $("#drop2 option").show();
      }
    });
  })

  function fileValidation() {
    var fileInput = document.getElementById('receipt_file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf|\.xlss)$/i;
    if (!allowedExtensions.exec(filePath)) {
      alert('Please upload file having extensions .jpeg/.jpg/.png/.pdf/.xlss only.');
      fileInput.value = '';
      return false;
    }
  }
</script>

<!-- <script type="text/javascript">
  function populateParentlist(choice) {
    $('#choices').find('tr').each(function() {
      trowid = $(this).attr('id');
      $("#Parentlist").append('<option>' + trowid + '</option>');
    });
  }

  function populateSublist(choice) {
    $('#Sublist').find('option').remove();
    $('#choices').find('tr').each(function() {
      trowid = $(this).attr('id');
      $(this).find('td').each(function() {
        tdid = $(this).attr('id');
        if (trowid != choice)
          return;
        $('#Sublist').append('<option>' + tdid + '</option>');
      });
    });
  }

  populateParentlist('paymode')
  populateSublist('paymode')

  $(function() {
    $("#Parentlist").change(function() {
      var parsel = $(this).val();
      populateSublist(parsel);
    });
  });  
</script> -->
</body>

</html>