<!DOCTYPE html>
<html>

<head>
    <title>404-Page Not Found</title>
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/page_not_found.scss"> -->
</head>

<body>
    <div class="face">
        <div class="band">
            <div class="red"></div>
            <div class="white"></div>
            <div class="blue"></div>
        </div>
        <div class="eyes"></div>
        <div class="dimples"></div>
        <div class="mouth"></div>
    </div>

    <h1>Oops! Something went wrong!</h1>
    <div class="btn">Return to Home</div>
</body>

</html>