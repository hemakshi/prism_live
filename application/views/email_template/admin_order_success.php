<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 12 (filtered medium)">
<style>

 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"Lucida Sans";
	panose-1:2 11 6 2 3 5 4 2 2 4;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:#fff;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-reply;
	font-family:"Calibri","sans-serif";
	color:#1F497D;}
.MsoChpDefault
	{mso-style-type:export-only;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.Section1
	{page:Section1;}
.track_button
{
      background-color: rgb(209, 50, 49);
    border: medium none;
    border-radius: 0;
    color: #fff;
    cursor: pointer;
    font-size: 13px;
    font-weight: 500;
    padding: 13px 42px 14px;
    text-transform: uppercase;
    /*margin: 3px 339px;*/
}

</style>
<!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1" />
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US link=blue vlink=purple>

<div class=Section1>



<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<div>

<div>

<p class=MsoNormal><span style='font-family:"Verdana","sans-serif";color:#0B5394'><o:p>&nbsp;</o:p></span></p>

</div>

<div>

<div>

<div>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;background:#F2F2F2'>
 <tr>
  <td valign=top style='padding:22.5pt 15.0pt 22.5pt 15.0pt'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="90%"
   style='width:90.0%;'>
   <tr>
    <td valign=top style='border:solid #DADADA 1.0pt;background:white;
    padding:22.5pt 22.5pt 22.5pt 22.5pt'>
    <div>
    <p class=MsoNormal style='margin-bottom:12.0pt;line-height:18.0pt'><span
    style='font-size:10.5pt;font-family:"Lucida Sans","sans-serif";color:#222222'>Dear Admin,
     <br>
     An Order has been placed through your website for <?php echo $data['total_products']; ?> item (Order No.:  <?php echo $data['ord_data']->ord_reference_no; ?>)
     <br>
    <br>
    <br>
    <a class="track_button" href="<?php echo $data['order_link']; ?>"> View Order Details here</a>
    <br>
    <div style='font-size:10.5pt;font-family:"Lucida Sans","sans-serif"'>
     <!--  Track your ticket here - -->
   <!--  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
     style='border-collapse:collapse;display: inline-block;
    vertical-align: middle;'>
     <tr>
      <td style='border:solid #3B6E22 1.0pt;border-bottom:solid #2C5115 1.0pt;
      background:#69A74E;padding:0in 0in 0in 0in'>
      <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
       style='border-collapse:collapse'>
       <tr>
        <td style='padding:0in 0in 0in 0in'>
        <div style='border:none;border-top:solid #95BF82 1.0pt;padding:2.0pt 0in 0in 0in'>
        <p class=MsoNormal><span style='font-size:10.5pt;font-family:"Lucida Sans","sans-serif"'><a
        href="https://support.lancer.co.in/portal/ticket/1969" target="_blank"><span
        style='color:white'>View ticket</span></a><o:p></o:p></span></p>
        </div>
        </td>
       </tr>
      </table>
      </td>
     </tr>
    </table> -->



    <!--  <img src="http://localhost/stanley/care/v1/public/logo_image/nextasy_1.png"> -->
     <span style='font-size:10.5pt;font-family:"Lucida Sans","sans-serif"'>Thanks and regards, <br>
    <?php echo PROJECT_NAME; ?>
  </span></p>
    </div>
    </div>
    </td>
   </tr>
   <tr>
    <td valign=top style='padding:3.75pt 0in 0in 0in'>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

</div>

</div>

</div>

</div>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>

</div>

</body>

</html>
