<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style type="text/css">
  body {
    color: #4d4d4d;
    font-family: Montserrat,sans-serif,Helvetica,Arial;
  }

    .content {
      border: 1px solid #eaeaec;
    box-shadow: 0 0 4px rgba(40,44,63,.08);
    max-width: 640px;
        margin: 0 auto;
    }
    .header,.info {
      padding: 15px;
      border-bottom: 2px solid #eaeaec;
    }
    h3 {
      font-size: 20px;
      font-weight: bold;
      margin: 5px 0 2px 0;
    }
    .line {
          border-bottom-width: 4px;
    border-bottom-color: rgb(209, 50, 49);
    border-bottom-style: solid;
    line-height: 1.5;
    display: inline-block;
    }
    p{
      margin: 10px 0;
      }
  </style>
</head>
<body >
<div class="content">
  <div class="header">
    <img src="https://transformsportsnutrition.com/assets/images/logo.png">
  </div>  
  <div class="info">
  <h3>Order</h3>
  <h3 class="line"><?php echo  $data['ord_data']->ord_status==1 ? 'Confirmed' : ''?></h3> 
  <div class="temp">
    <p>Hey <?php echo $this->session->userdata(PROJECT_SESSION_NAME)?>,</p>
    <p>Thank you for shopping at Transform Sports Nutrition. Your order #<?php echo $data['ord_data']->ord_reference_no;?> has been confirmed.</p>
    <p>Amount: <strong>RS. <?php echo  $data['ord_data']->ord_total_amt?></strong></p>
    <!-- <p>Estimated Delivery: <strong>23 Dec,2018</strong></p> -->
    <p><strong>Shipping/Billing Address:</strong></p>
    <p><?php echo   $data['customer_name']?></p>
<p><?php echo  $data['ord_data']->pad_address?> </p>
<p><?php echo  $data['ord_data']->pad_locality?> </p>
<p><?php echo $data['ord_data']->pad_city;?>- <?php echo $data['ord_data']->pad_pincode;?> </p></p>
  <?php 
      if($data['ord_data']->pad_landmark != '') {
          echo '<p>Landmark: '.$data['ord_data']->pad_landmark.'</p>';
           }
                           
       ?>

  </div>
  </div>
</div>
</body>
</html>