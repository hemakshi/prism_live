<?php $this->load->view('header');

$isProductWithHeightAndWidth = 0;
if ($category == STICKER || $category == BROCHURE_MIX) {
    $isProductWithHeightAndWidth = 1;
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<style type="text/css">
    .scrollable-menu {
        height: auto;
        max-height: 200px;
        overflow-x: hidden;
    }
</style>
<!-- Shop Page Area Start -->
<div class="shop-page-area pb-50">
    <div class="container" ng-controller="productsCtrl">
        <!-- Breadcrumb Area Start -->
        <div class="breadcrumb-content">
            <ul>
                <li><a href="<?php echo site_url('home') ?>">Home</a></li>
                <li class="active">Products</li>
            </ul>
        </div>
        <div id="notify"></div>
        <!-- Breadcrumb Area End -->
        <div class="row flex-row-reverse">
            <!-- <div class="col-lg-12"> -->
            <div class="container pb-50 table-responsive">
                <!--  -->
                <table class="table table-hover table-responsive-sm table-responsive-md" id="dTable" style="width: 100%; background-color: #efefef;">
                    <thead>
                        <tr>
                            <th scope="col">Sr.No</th>
                            <th scope="col" style="min-width: 220px;">Product Name</th>
                            <th scope="col">Size</th>
                            <th scope="col">Price</th>
                            <?php
                            if ($isProductWithHeightAndWidth) {
                            ?>
                                <th scope="col">Height</th>
                                <th scope="col">Width</th>
                            <?php
                            }
                            ?>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                            <!--  <th scope="col">Upload File</th> -->
                            <th scope="col">Order</th>

                        </tr>
                    </thead>
                    <tbody class="body">

                        <?php
                        $i = 1;
                        foreach ($products as $product) {
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $product->prd_name; ?></td>
                                <td><?php echo $product->prd_size; ?></td>
                                <td>
                                    <input id="price<?php echo $product->prd_id ?>" name="price" value="<?php echo $product->prd_price; ?>" class="form-control" disabled="disabled" />
                                </td>
                                <?php
                                if ($isProductWithHeightAndWidth) { ?>
                                    <td>
                                        <input id="height<?php echo $product->prd_id ?>" type="text" min="1" value="1" name="height" class="form-control allownumericwithdecimal" style="width: 59%;" onchange="findTotalAmount(<?php echo $product->prd_id ?>, <?php echo $isProductWithHeightAndWidth ?>)">
                                        <span></span>
                                    </td>
                                    <td>
                                        <input id="width<?php echo $product->prd_id ?>" type="text" min="1" value="1" name="width" class="form-control allownumericwithdecimal" style="width: 59%;" onchange="findTotalAmount(<?php echo $product->prd_id ?>, <?php echo $isProductWithHeightAndWidth ?>)">
                                    </td>
                                <?php } ?>
                                <td>
                                    <select class="form-control " id="qty<?php echo $product->prd_id ?>" name="qty" onchange="findTotalAmount(<?php echo $product->prd_id; ?>, <?php echo $isProductWithHeightAndWidth; ?>)">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                    </select>
                                </td>

                                <td><input id="total<?php echo $product->prd_id ?>" value="<?php echo $product->prd_price; ?>" name="total" class="form-control" type="number" disabled="disabled"></td>
                                <!--<td> <a class="btn btn-outline-secondary" href="<?php echo site_url('upload_file'); ?>">Upload File</a> </td>-->
                                <td>

                                    <!--  <button type="button" class="btn btn-outline-secondary">Order</button> -->

                                    <div class="quality-add-to-cart">
                                        <div class="contact-form-style">
                                            <?php
                                            $flag = false;
                                            if (count($this->cart->contents()) > 0) {
                                                foreach ($this->cart->contents() as $items) {
                                                    if ($items["id"] == $product->prd_id) {
                                                        $flag = true;
                                                        break;
                                                    } else {
                                                        $flag = false;
                                                    }
                                                }
                                            }
                                            if ($flag) {
                                                echo '<a href="' . site_url('cart') . '" id="added" class="go-to-cart' . $product->prd_id . '" style="padding:10px 15px 10px;"> 
                                                    GO TO CART
                                                </a>';
                                            } else {
                                            ?>
                                                <button id="add-to-cart<?php echo $product->prd_id ?>" class="submit add_cart" type="submit" data-productname="<?php echo $product->prd_name ?>" data-price="<?php echo  $product->prd_price ?>" data-productid="<?php echo $product->prd_id ?>" data-category="<?php echo $category ?>" data-product_width="1" data-product_height="1" data-product_qty="1" style="padding: 10px 13px 10px;">
                                                    <i class="fa fa-cart-plus" style="font-size:14px" aria-hidden="true"></i>
                                                </button>
                                            <?php
                                            }

                                            ?>
                                            <!-- <a href="<?php echo site_url('cart') ?>" id="added-cart" style="display: none"> GO TO CART </a> -->
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Shop Page Area Start -->
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url() ?>assets/js/cart.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data_tables/Buttons-1.6.5/js/buttons.html5.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dTable').DataTable({

        });
    });

    $(".allownumericwithdecimal").on("keypress keyup blur", function(event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    function findTotalAmount(id, isProductWithHeightAndWidth) {
        let price = $('#price' + id).val();
        let qty = $('#qty' + id).val();
        let height = parseFloat($('#height' + id).val()).toFixed(2);
        let width = parseFloat($('#width' + id).val()).toFixed(2);
        $('#add-to-cart' + id).data("product_qty", parseInt(qty));

        if (isProductWithHeightAndWidth) {
            $('#add-to-cart' + id).data("product_width", width);
            $('#add-to-cart' + id).data("product_height", height);
            let total = parseFloat(parseFloat(price) * parseInt(qty) * parseFloat(height) * parseFloat(width)).toFixed(2);
            $('#total' + id).val(total);
        } else {
            let total = parseFloat(parseFloat(price) * parseInt(qty)).toFixed(2);
            $('#total' + id).val(total);
        }
    }
</script>

</body>

</html>