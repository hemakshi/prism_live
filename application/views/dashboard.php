<?php $this->load->view('header'); ?>

<!-- my account start -->
<div class="checkout-area pb-80 pt-100">
    <div class="container">
        <div class="row">
            <div class="ml-auto mr-auto col-lg-9">

                <div class="profile card">
                    <div class="profile-data">
                        <div class="tab-content">
                            <h4>Profile Details</h4>
                            <div class="page-content">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="title">Name</td>
                                            <td><?php echo $userData->prs_name ?></td>
                                        </tr>
                                  <!--       <tr>
                                            <td class="title">Gender</td>
                                            <td><?php echo $userData->gender ?></td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $date = '- - -';
                                            if ($userData->prs_dob != '0000-00-00') {
                                                $date = date("d-m-Y", strtotime($userData->prs_dob));
                                            }
                                            ?>
                                            <td class="title">Date of birth</td>
                                            <td> <?php echo  $date ?></td>
                                        </tr>
                                        <tr>
                                            <td class="title">Bio</td>
                                            <td class="bio"><?php echo $userData->prs_bio ? $userData->prs_bio : '- - -' ?></td>
                                        </tr> -->
                                        <tr>
                                            <td class="title">Mobile Number</td>
                                            <td><?php echo $userData->prs_mob ?></td>
                                        </tr>
                                        <tr>
                                            <td class="title">Whatsapp Number</td>
                                            <td><?php echo $userData->prs_whatsapp ?></td>
                                        </tr>
                                        <tr>
                                            <td class="title">E-mail id</td>
                                            <td><?php echo $userData->prs_email ?></td>
                                        </tr>
                                        <tr>
                                            <td class="title">GST Number</td>
                                            <td><?php echo $userData->prs_gst ? $userData->prs_gst : '- - -' ?></td>
                                        </tr>
                                        <tr>
                                            <td class="title">Address</td>
                                            <td>
                                                <?php echo $userData->prs_address . ", " . $userData->prs_city . ", " . $userData->prs_dist . ", " . $userData->state_name . ", " . $userData->prs_country . "- " . $userData->prs_pincode ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="button-box">
                                    <a href="<?php echo site_url('profile/edit') ?>"> <button type="submit">Edit</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- my account end -->
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/user_update.js"></script>
</body>

</html>