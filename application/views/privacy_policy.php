<?php $this->load->view('header');?>

		<!-- About Us Area Start -->
        <div class="about-us-area pb-100">
            <div class="container">
            <div class="breadcrumb-content">
                    <ul>
                        <li><a href="<?php echo site_url('home')?>">Home</a></li>
                            <li><a href="<?php echo site_url('about')?>">Privacy policy</a></li>
                       </ul>
                </div>
                <div class="row">
                   <div class="overview-content-2">
<p class="MsoNormalCxSpFirst" style="line-height:normal"><span lang="EN-US" style="font-size:14.0pt">This privacy policy sets out how Transform Sports
Nutrition (TSN) uses and protects any information that you give Transform
Sports Nutrition (TSN) when you use this Website.</span></p><p class="MsoNormalCxSpFirst" style="line-height:normal">Transform
Sports Nutrition (TSN)&nbsp; is committed to
ensuring that your privacy is protected.&nbsp;
Should we ask you to provide certain information by which you can be
identified when using this Website, then you can be assured that it will only
be used in accordance with this privacy statement.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">&nbsp;</span>Transform
Sports Nutrition (TSN) may change this policy from time to time by updating
this page.&nbsp; You should check this page
from time to time to ensure that you are secure/protected with any
changes.&nbsp; This policy is effective from
10 November 2018.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">&nbsp;</span><span style="font-size: 14pt;">What we collect.</span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">We may
collect the following information:</p><p class="MsoListParagraphCxSpFirst" style="text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">Name<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;line-height:
normal;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">Contact information including
email address<o:p></o:p></span></p><p class="MsoListParagraphCxSpLast" style="text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">Other information relevant to
customer surveys and/or offers<o:p></o:p></span></p><p class="MsoNormalCxSpFirst" style="line-height:normal"><span style="font-size: 16pt;">What we do with the information we gather.</span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">&nbsp;</span>We
require this information to understand your needs and provide you with a better
service, and in particular for the following reasons:</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US" style="text-indent: -18pt; font-family: Symbol;"><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;</span></span><span lang="EN-US" style="text-indent: -18pt;">Internal record keeping</span></p><p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;line-height:
normal;mso-list:l1 level1 lfo2"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">We may use the information to
improve our products and services<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;line-height:
normal;mso-list:l1 level1 lfo2"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">We may periodically send
promotional emails about new products, special offers or other information
which we think you may find interesting using the email address which you have
provided<o:p></o:p></span></p><p class="MsoListParagraphCxSpLast" style="text-indent:-18.0pt;line-height:normal;
mso-list:l1 level1 lfo2"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">From time to time, we may also
use your information to contact you for market research purposes.</span></p><p class="MsoListParagraphCxSpLast" style="text-indent:-18.0pt;line-height:normal;
mso-list:l1 level1 lfo2"><span lang="EN-US"><br></span><span style="font-size: 16pt; text-indent: -18pt;">Security</span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span lang="EN-US">&nbsp;</span>We are
committed to ensuring that your information is secure. In order to prevent unauthorized
access or disclosure we have put in place suitable physical, electronic and
managerial procedures to safeguard and secure the information we collect
online.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span style="font-size: 16pt;">How we use cookies</span><br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span style="font-family: Calibri, sans-serif; font-size: 11pt;">A cookie is a small file which asks permission
to be placed on your computer's hard drive. Once you agree, the file is added
and the cookie helps analyze Web traffic or lets you know when you visit a&nbsp;</span>particular
site. Cookies allow Web applications to respond to you as an individual. The
Web application can tailor its operations to your needs, likes and dislikes by
gathering and remembering information about your preferences.We use
traffic log cookies to identify which pages are being used.&nbsp; This helps us analyze data about Webpage
traffic and improve our Website in order to tailor it to customer needs. We
only use this information for statistical analysis purposes and then the data
is removed from the system.Overall,
cookies help us provide you with a better Website, by enabling us to monitor
which pages you find useful and which you do not. A cookie in no way gives us
access to your computer or any information about you, other than the data you
choose to share with us.You
can choose to accept or decline cookies. Most Web browsers automatically accept
cookies, but you can usually modify your browser setting to decline cookies if
you prefer. This may prevent you from taking full advantage of the Website.</p><p class="MsoNormalCxSpFirst" style="line-height:normal"><span lang="EN-US" style="font-size:16.0pt">Links to other Websites<o:p></o:p></span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">Our
Website may contain links to other Websites of interest.&nbsp; However, once you have used these links to
leave our site, you should note that we do not have any control over that other
Website.&nbsp; Therefore, we cannot be
responsible for the protection and privacy of any information which you provide
whilst visiting such sites and such sites are not governed by this privacy
statement. You should exercise caution and look at the privacy statement
applicable to the Website in question.<br></p><p class="MsoNormalCxSpMiddle" style="line-height:normal"><span style="font-size: 16pt;">Controlling your personal information</span></p><p class="MsoNormalCxSpMiddle" style="line-height:normal">You
may choose to restrict the collection or use of your personal information in
the following ways:</p><p class="MsoNormalCxSpLast" style="line-height:normal"><span style="text-indent: -18pt;">Some forms we ask you to fill
out may include a check box to confirm that you would like to receive updates
and news from us.&nbsp; You can choose not to
check these boxes, when present</span></p><p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;line-height:
normal;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">If you do receive any
communications from us that you would rather not receive, you can request to be
removed from our mailing list via the Contact Us form on this Website.<o:p></o:p></span></p><p class="MsoListParagraphCxSpLast" style="text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span lang="EN-US" style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">¾<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="EN-US">You can use our
"unsubscribe" facility that will be included in any general email
newsletter. <o:p></o:p></span></p><p class="MsoNormalCxSpFirst" style="line-height:normal">We
will not sell, distribute or lease your personal information to third parties
unless we have your permission or are required by law to do so.&nbsp; We may use your personal information to send
you promotional information about third parties which we think you may find
interesting if you tell us that you wish this to happen.</p><p class="MsoNormalCxSpMiddle" style="line-height:normal">If you
believe that any information we are holding on you is incorrect or incomplete,
please write to us at the address on our Contact page or email us at the above
address as soon as possible.&nbsp; We will
promptly correct any information found to be incorrect.</p>

<p class="MsoNormalCxSpMiddle" style="line-height:normal">&nbsp;<span style="font-family: Calibri, sans-serif; font-size: 11pt;">&nbsp;</span></p> 
							
                        </div>
                     </div>
            </div>
        </div>
	
		<!-- End Brand Area -->
	<?php $this->load->view('footer');?>
    </body>
</html>
