<?php $this->load->view('header'); ?>
<style>
  .errormesssage {
    caret-color: black;
    color: #ff6161;
    font-size: 14px;
    font-weight: 500;
  }

  .note {
    color: #ff6161;
    font-size: 12px;
  }
</style>
<div class="container">
  <div class="breadcrumb-content">
    <ul>
      <li><a href="<?php echo site_url('home') ?>">Home</a></li>
      <li><a href="<?php echo site_url('my-quotations') ?>">My Quotations</a></li>
      <li><a href="">Quotation Request</a></li>
    </ul>
  </div>

  <div class="text-center">
    <h3 style="padding-bottom: 10px; padding-top: 10px; margin-bottom:20px; color: white; background-color: #000">Quotation Request - Customised Order</h3>
  </div>

  <div class="container">

    <form id="quotation_request_form" name="quotation_request_form" enctype="multipart/form-data" method="POST">
      <div class="pb-10">

        <table class="table table-bordered table-responsive-sm table-responsive-md">
          <tr>
            <th colspan="4">
              <h4>Order Details</h4>
            </th>
          </tr>

          <tr>
            <td><label>Product Name <span class="emsg">*</span></label></td>
            <td colspan="3">
              <input type="text" name="txt_product_name" class="form-control" id="validationDefault03" required placeholder="Enter Product Name">
            </td>
          </tr>

          <tr>
            <td><label>Paper GSM & Size <span class="emsg">*</span></label></td>
            <td>
              <input type="text" name="txt_pepar_gsm_size" class="form-control" required placeholder="eg- 130GSM paper 13x19 inch">
            </td>
            <td><label>Creasing</label></td>
            <td>
              <select name="sel_creasing" class="custom-select" id="drop1">
                <option value="N/A">N/A</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </td>
          </tr>

          <tr>
            <td><label>Half Cut</label></td>
            <td>
              <select name="sel_halfcut" class="custom-select" id="drop1">
                <option value="N/A">N/A</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </td>
            <td><label>UV</label></td>
            <td>
              <select name="sel_uv" class="custom-select" id="drop1">
                <option value="N/A">N/A</option>
                <option value="1 Side">1 Side</option>
                <option value="2 Side">2 Side</option>
                <option value="Only Outside">Only Outside</option>
                <option value="All Pages">All Pages</option>
              </select>
            </td>
          </tr>

          <tr>
            <td><label>Lamination Type</label></td>
            <td>
              <select name="sel_lamination_type" class="custom-select" id="drop1">
                <option value="N/A">N/A</option>
                <option value="Matt">Matt</option>
                <option value="Gloss">Gloss</option>
                <option value="Velvet">Velvet</option>
              </select>
            </td>
            <td><label>Lamination</label></td>
            <td>
              <select name="sel_lamination" class="custom-select" id="drop1">
                <option value="N/A">N/A</option>
                <option value="1 Side">1 Side</option>
                <option value="2 Side">2 Side</option>
                <option value="Only Outside">Only Outside</option>
                <option value="All Pages">All Pages</option>
              </select>
            </td>
          </tr>

          <tr>
            <td><label>Quantity <span class="emsg">*</span></label></td>
            <td colspan="3">
              <input type="text" name="txt_quantity" class="form-control" required placeholder="Enter Quantity">
              <span for="txt_quantity" class="note">Separate with comma(,) for requesting quotation for multiple quantity.</span>
            </td>
          </tr>

          <tr>
            <td><label>Remarks(Job Details) <span class="emsg">*</span></label></td>
            <td colspan="3">
              <textarea name="txt_remarks" id="txt_remarks" required placeholder="Enter Remarks"></textarea>
            </td>
          </tr>

          <tr>
            <td><label>Select Related file<span class="emsg"></span></label></td>
            <td colspan="3">
              <div class="custom-file mb-3">
                <input type="file" name="file_related_image" class="custom-file-input" id="file_related_image" onchange="return fileValidation()">
                <label class="custom-file-label" for="file_related_image">Choose file</label>
                <span class="note">An Image describing product can be submitted here.</span>
              </div>
            </td>
          </tr>
        </table>

      </div>

      <div class="text-center mb-50">
        <button type="submit" class="btn btn-danger mt-10" id="btn_send_quotation_request" style="line-height: 0.2; border-radius: 17px; padding: 17px;">Send Quotation Request</button>
      </div>
    </form>
  </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/quotation_request.js"></script>

<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>

</body>

</html>