   <?php $this->load->view('header');?>
	
        <!-- Product Deatils Area Start -->
        <div class="product-details pb-100 pb-95">
            <div class="container">
                 <div class="breadcrumb-content">
                    <ul>
                        <li><a href="<?php echo site_url('home')?>">Home</a></li>
                            <!-- <li><a href="<?php echo site_url('supplements')?>">Supplements</a></li> -->
                              <li class="active"><?php echo $product->prd_name?></li>
                       </ul>
                </div>
                
                <div class="row">
                   <!--  <div class="col-lg-6 col-md-12">
                        <div class="product-details-img"> -->
                          <!--   <div id="zoom-img">
                                <img class="zoompro" src="<?php echo base_url().CHILD_FOLDER.PRODUCT_BIG_IMAGE_PATH.$productDefaultFlavour->pfi_img; ?>" data-zoom-image="<?php echo base_url().CHILD_FOLDER.PRODUCT_LARGE_IMAGE_PATH.$productDefaultFlavour->pfi_img; ?>" alt="zoom"/>
                            </div> -->

                           <!--  <div id="gallery" class="mt-20 product-dec-slider owl-carousel">
                                   <div id="small-img"> 
                                 <a data-image="<?php echo base_url().CHILD_FOLDER.PRODUCT_BIG_IMAGE_PATH.$productDefaultFlavour->pfi_img; ?>" data-zoom-image="<?php echo base_url().CHILD_FOLDER.PRODUCT_LARGE_IMAGE_PATH.$productDefaultFlavour->pfi_img; ?>">
                                    <img  style="height: 90px;width: 90px;" src="<?php echo base_url().CHILD_FOLDER.PRODUCT_SMALL_IMAGE_PATH.$productDefaultFlavour->pfi_img; ?>" alt="">
                                </a>
                                </div> -->
                               <!--  <?php 
                                foreach ($product_images as $key) {
                                    ?>
                                     <a data-image="<?php echo base_url().CHILD_FOLDER.PRODUCT_BIG_IMAGE_PATH.$key->img_path; ?>" data-zoom-image="<?php echo base_url().CHILD_FOLDER.PRODUCT_LARGE_IMAGE_PATH.$key->img_path; ?>">
                                    <img  style="height: 90px;width: 90px;" src="<?php echo base_url().CHILD_FOLDER.PRODUCT_SMALL_IMAGE_PATH.$key->img_path; ?>" alt="">
                                </a>
                                   <?php
                                }
                                ?> 
                            </div>-->
                         <!--  </div>
                    </div> -->
                    <div class="col-lg-6 col-md-12">
                        <div class="product-details-content">
                            <!-- <h4><?php echo $product->prd_name?></h4>
                            <div class="rating-review" style="display: none">
                                <div class="pro-dec-rating">
                                    <i class="ion-android-star-outline theme-star"></i>
                                    <i class="ion-android-star-outline theme-star"></i>
                                    <i class="ion-android-star-outline theme-star"></i>
                                    <i class="ion-android-star-outline theme-star"></i>
                                    <i class="ion-android-star-outline"></i>
                                </div>
                                <div class="pro-dec-review">
                                    <ul>
                                        <li>32 Reviews </li>
                                        <li> Add Your Reviews</li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- <?php
                            $price = '';
                             if($product->prd_discount > 0) {
                                $price = round($product->prd_price * (100-$product->prd_discount)/100);
                                ?>
                                <div class="price-wrapper-detail">
                                <span class="discounted-price"><i class="fa fa-inr" aria-hidden="true"></i><span >
                                    <?php echo round($product->prd_price * (100-$product->prd_discount)/100)?>
                                   </span></span>
                                 <span class="original-price">₹<?php echo $product->prd_price?></span>
                                  <span  class="discount"><?php echo $product->prd_discount?>% Off</span>
                         </div>-->
                    <!--  <?php }
                     else {
                         $price = $product->prd_price;
                       echo ' <div class="price-wrapper-detail">
                                <span class="discounted-price"><i class="fa fa-inr" aria-hidden="true"></i><span class="price">'.$product->prd_price.'</span></span>
                                 
                         </div> ';
                     }
                     ?>  -->
                           <p><?php echo $product->prd_short_desc?> </p>
                           <div class="switch-field">
                           <div class="switch-title">
                            <p class="validation">Please select a flavour</p>
                          <div id="notify"></div>
                            <input type="hidden" name="product_slug" id="product_slug" value="<?php echo $product->prd_slug?>">
                           </div>
                            <div class="select-flavours">
                                          <?php foreach ($productFlavours as $key) {

                                            $checked="";
                                            if($productDefaultFlavour->pfi_flv_id == $key->flv_id)
                                                {
                                                    $checked="checked";
                                                }
                                           ?>
                                 <input type="radio" <?php echo  $checked?>  onclick="flavourSelected(this)"  data-slug="<?php echo $key->flv_slug?>" data-name="<?php echo $key->flv_name?>" name="flavours" value="<?php echo $key->flv_id?>" id="<?php echo $key->flv_id?>"/>
                                  <label for="<?php echo $key->flv_id?>"><?php echo $key->flv_name?></label>
                                    <?php
                                      
                                   }
                                      
                                      ?>   </div>

                           </div>
                        
                            <div class="quality-add-to-cart">
                                    <div class="contact-form-style">
                                      <!--   -->

                                    <?php
                            $flag=false;
                            if(count($this->cart->contents())>0)
                            {
                                foreach ($this->cart->contents() as $items)
                                {
                                    // if ($items["id"] == $product->prd_id && $items["flavour"] == $productDefaultFlavour->pfi_flv_id)
                                    // {
                                    //     $flag=true;
                                    //     break;
                                    // }
                                    // else
                                    // {
                                    //     $flag=false;
                                    // }

                                }
                            }
                            if($flag)
                            {
                                echo '<a href="'.site_url('cart').'" id="added">  GO TO CART </a>';
                            }
                            else
                            {
                                ?>
                              <button id="add-to-cart" class="submit add_cart" type="submit" data-productname="<?php echo $product->prd_name ?>" data-price="<?php echo  $price ?>" data-productid="<?php echo $product->prd_id ?>" data-image="<?php echo base_url().CHILD_FOLDER.PRODUCT_SMALL_IMAGE_PATH.$productDefaultFlavour->pfi_img; ?>" >ADD TO CART</button>
                                <?php
                            }

                            ?>
                            <a href="<?php echo site_url('cart')?>" id="added-cart" style="display: none">  GO TO CART </a>
                                    </div>
                            </div>
                            <div class="pro-dec-categories">
                                <ul>
                                    <li class="categories-title">Category:</li>
                                    <li><?php echo $product->prd_cat_id_name?></li>
                                   </ul>
                            </div>
                          <div class="pro-dec-social" style="display: none">
                                <ul>
                                    <li><a class="tweet" href="#"><i class="ion-social-twitter"></i> Tweet</a></li>
                                    <li><a class="share" href="#"><i class="ion-social-facebook"></i> Share</a></li>
                                    <li><a class="google" href="#"><i class="ion-social-googleplus-outline"></i> Google+</a></li>
                                    <li><a class="pinterest" href="#"><i class="ion-social-pinterest"></i> Pinterest</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Deatils Area End -->
<!--  -->

<div class="container pb-100 table-responsive">

  <!--  -->
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Sr.No</th>
      <th scope="col">Product Name</th>
      <th scope="col">Price</th>
      <th scope="col">Quantity</th>
      <th scope="col">Total</th>
      <th scope="col">Upload File</th>
      <th scope="col">Order</th>
     
    </tr>
  </thead>
  <tbody>
    <?php
 foreach($dt as $row)
  {

    echo "<tr>";
  echo "<td>".$row->pid."</td>";
  echo "<td>".$row->name."</td>";
  echo "<td>".$row->price."</td>";
 
  

    }?>
      <td>  
          <select name="Qty" id="Qty" style="width: 60%; border: 2px solid #e0b105;height: 30px;">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
</select>
 
  
</div></td>
<td></td>
<td>
  
   <a class="btn btn-outline-secondary" href="<?php echo site_url('upload_file');?>">Upload File</a>
  

</td>
<td>  
    <button type="button" class="btn btn-outline-secondary">Order</button>
</td>
    </tr>
 
    <tr>
      <th scope="row">2</th>
            <td>  DRIPOFF_NT_SINGLE</td>
      <td>Rs. 350</td>
      <td>   
          <select name="Qty" id="Qty" style="width: 60%; border: 2px solid #e0b105;height: 30px;">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
</select>
  
</div>
</td>
<td></td>
<td>
 <a href="<?php echo site_url('upload_file');?>" class="btn btn-outline-secondary">Upload File</a>
   
</td>
<td>
    <button type="button" class="btn btn-outline-secondary">Order</button>
</td>
    </tr>

    <tr>
      <th scope="row">3</th>
      <td>  DRIPOFF_NT_SINGLE</td>
      <td>Rs. 350</td>
      <td>    
          <select name="Qty" id="Qty" style="width: 60%; border: 2px solid #e0b105;height: 30px;">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
</select>
 
  
</div></td>
<td></td>
<td>
  <a href="<?php echo site_url('upload_file');?>" class="btn btn-outline-secondary">Upload File</a>
     
</td>
<td>
    <button type="button" class="btn btn-outline-secondary">Order</button>
</td>
    </tr>
  </tbody>
</table>
</div>

        <!-- <div class="description-review-area pb-70">
            <div class="container">
                <div class="description-review-wrapper">
                    <div class="description-review-topbar nav text-center">
                        <a class="active" data-toggle="tab" href="#des-details1">Description</a>
                       <a data-toggle="tab" href="#des-details3" style="display: none">Review</a>
                    </div>
                    <div class="tab-content description-review-bottom">
                        <div id="des-details1" class="tab-pane active">
                            <div class="product-description-wrapper">
                               <?php echo $product->prd_descripion?>
                            </div>
                        </div>
                      <div id="des-details3" class="tab-pane">
                            <div class="rattings-wrapper">
                                <div class="sin-rattings">
                                    <div class="star-author-all">
                                        <div class="ratting-star f-left">
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <span>(5)</span>
                                        </div>
                                        <div class="ratting-author f-right">
                                            <h3>Potanu Leos</h3>
                                            <span>12:24</span>
                                            <span>9 March 2018</span>
                                        </div>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost rud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost.</p>
                                </div>
                                <div class="sin-rattings">
                                    <div class="star-author-all">
                                        <div class="ratting-star f-left">
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <i class="ion-star theme-color"></i>
                                            <span>(5)</span>
                                        </div>
                                        <div class="ratting-author f-right">
                                            <h3>Kahipo Khila</h3>
                                            <span>12:24</span>
                                            <span>9 March 2018</span>
                                        </div>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost rud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost.</p>
                                </div>
                            </div>
                            <div class="ratting-form-wrapper">
                                <h3>Add your Comments :</h3>
                                <div class="ratting-form">
                                    <form action="#">
                                        <div class="star-box">
                                            <h2>Rating:</h2>
                                            <div class="ratting-star">
                                                <i class="ion-star theme-color"></i>
                                                <i class="ion-star theme-color"></i>
                                                <i class="ion-star theme-color"></i>
                                                <i class="ion-star theme-color"></i>
                                                <i class="ion-star"></i>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="rating-form-style mb-20">
                                                    <input placeholder="Name" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="rating-form-style mb-20">
                                                    <input placeholder="Email" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="rating-form-style form-submit">
                                                    <textarea name="message" placeholder="Message"></textarea>
                                                    <input type="submit" value="add review">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-area pb-100" style="display: none">
            <div class="container">
                <div class="product-top-bar section-border mb-35">
                    <div class="section-title-wrap">
                        <h3 class="section-title section-bg-white">Related Products</h3>
                    </div>
                </div>
                <div class="featured-product-active hot-flower owl-carousel product-nav">
                    <div class="product-wrapper">
                        <div class="product-img">
                            <a href="product-details.html">
                                <img alt="" src="<?php echo base_url();?>assets/img/product/product-1.jpg">
                            </a>
                            <span>-30%</span>
                            <div class="product-action">
                                <a class="action-wishlist" href="#" title="Wishlist">
                                    <i class="ion-android-favorite-outline"></i>
                                </a>
                                <a class="action-cart" href="#" title="Add To Cart">
                                    <i class="ion-ios-shuffle-strong"></i>
                                </a>
                                <a class="action-compare" href="#" data-target="#exampleModal" data-toggle="modal" title="Quick View">
                                    <i class="ion-ios-search-strong"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-content text-left">
                            <div class="product-hover-style">
                                <div class="product-title">
                                    <h4>
                                        <a href="product-details.html">Le Bongai Tea</a>
                                    </h4>
                                </div>
                                <div class="cart-hover">
                                    <h4><a href="product-details.html">+ Add to cart</a></h4>
                                </div>
                            </div>
                            <div class="product-price-wrapper">
                                <span>$100.00 -</span>
                                <span class="product-price-old">$120.00 </span>
                            </div>
                        </div>
                    </div>
                    
                  
                </div>
            </div>
        </div> -->
	<?php $this->load->view('footer');?>
    <script src="<?php echo base_url()?>assets/js/cart.js"></script>
    </body>
</html>