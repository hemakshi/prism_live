<?php $this->load->view('header'); ?>
<div class="checkout-area pb-80 pt-100">
  <div class="container">
    <div class="row">
      <!-- <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="grand-totall  order-review">
          <h4 class="cart-bottom-title section-bg-gary-cart text-center">Choose Payment Mode</h4>
          <div class="title-wrap">
            <div class="ship-wrapper order-review-payment-mode">
              <div class="single-ship ">
                <input type="radio" checked="" value="<?php echo PAYMENT_TYPE_COD; ?>" name="ord_payment_mode">
                <label class="fz16">Cash on delivery</label>
                <label>(Pay with Cash when your order is delivered)</label>

              </div>
              <div class="single-ship ">
                <input type="radio" value="<?php echo PAYMENT_TYPE_PAYTM ?>" name="ord_payment_mode">
                <label class="fz16">Paytm </label>
              </div>
            </div>
            <button class="place-order pay btn_save">Pay Now</button>

          </div><br>
          <?php echo $this->session->userdata('order'); ?>
        </div>
      </div> -->
      <!-- PAY FROM BALANCE SECTION START -->
      <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="grand-totall  order-review">
          <h4 class="cart-bottom-title section-bg-gary-cart text-center">Pay from Balance</h4>
          <div class="title-wrap">
            <div class="ship-wrapper order-review-payment-mode">
              <div class="single-ship ">
                <!-- <input type="radio" checked="" value="<?php echo PAYMENT_TYPE_COD; ?>" name="ord_payment_mode"> -->
                <label class="fz16">Current Balance</label><br>
                <label> Rs. <?php echo $wallet_balance; ?> </label>
              </div>
              <div class="single-ship">
                <!-- <input type="radio" value="<?php echo PAYMENT_TYPE_PAYTM ?>" name="ord_payment_mode">
                <label class="fz16">Paytm </label> -->
              </div>
            </div>
            <button class="place-order pay btn_save">Pay Now</button>

          </div><br>
          <?php echo $this->session->userdata('order'); ?>
        </div>
      </div>
      <!-- PAY FROM BALANCE SECTION END -->

      <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="grand-totall order-review">
          <h4 class="cart-bottom-title section-bg-gary-cart text-center">Order Summary</h4>


          <h5><?php echo count($this->cart->contents()); ?> Item</h5>
          <h5>Total<span> <?php echo $this->session->userdata('grandtotal'); ?></span></h5>
          <h5 class="border-bottom">Delivery<span> -</span></h5>
          <div class="cart-footer">
            <h5>Total Payable<span>Rs. <?php echo $this->session->userdata('grandtotal') + SHIPPING_CHARGES; ?></span></h5>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="grand-totall order-review scroll-y">
          <h4 class="cart-bottom-title section-bg-gary-cart text-center">Deliver to</h4>

         <!--  <div class="address-content"><span class="address-field lbl"><?php echo $shipping_address->pad_address; ?>
          </div> -->
          <div><?php echo $shipping_address->pad_locality; ?></div>
          <div><?php echo $shipping_address->pad_city; ?>- <?php echo $shipping_address->pad_pincode; ?></div>
          <div><?php echo $shipping_address->state_name; ?></div>
          <div>Mobile: <?php echo $shipping_address->pad_mobile; ?></div>
          <?php
          if ($shipping_address->pad_landmark != '') {
            echo '<div>Landmark: ' . $shipping_address->pad_landmark . '</div>';
          }
          if ($shipping_address->pad_alt_phone != '') {
            echo '<div>Alternate Mobile: ' . $shipping_address->pad_alt_phone . '</div>';
          }
          ?>
          <a class="change-address" href="<?php echo base_url('address-select'); ?>"> Want to change your address? Click here</a>          
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Order Values -->
<input type="hidden" name="ord_address" id="ord_address" value="<?php echo $shipping_address->pad_id; ?>">
<input type="hidden" name="ord_grand_total" id="ord_grand_total" value="<?php echo $ord_grand_total; ?>">
<input type="hidden" name="wallet_balance" id="wallet_balance" value="<?php echo $wallet_balance; ?>">

<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form_validation/place_order.js"></script>
<!-- <script type="text/javascript">
  swal({
    title: "Good job!",
    text: "You clicked the button!",
    icon: "success",
    button: "Aww yiss!",
  });
</script> -->
</body>

</html>