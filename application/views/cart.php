<?php $this->load->view('header'); ?>


<style type="text/css">
  
</style>

<!-- About Us Area Start -->
<div class="about-us-area  pb-100">
  <div class="container">
    <div class="breadcrumb-content">
      <ul>
        <li><a href="<?php echo site_url('home') ?>">Home</a></li>
        <li class="active">Cart</li>
      </ul>
    </div>
    <?php
    if (count($this->cart->contents()) > 0) {

    ?>
      <form id="checkoutForm" name="checkoutForm" enctype="multipart/form-data" method="post">
        <div class="row">
          <div class="col-md-8 col-lg-8 col-sm-12">
            <div class="cart-header">
              <div id="result" class="text-center bold"></div>
              <div class="my-cart-text">
                <h3>My cart (<?php echo count($this->cart->contents()); ?> Items)</h3>
              </div>
              <div class="my-cart-price">
                <h3>Total: Rs. <?php echo sprintf("%.2f", $this->session->userdata('grandtotal')); ?> </h3>
              </div>
            </div>
            <div class="prod-set">
              <?php
              $count = 0;
              $i = 1;
              foreach ($this->cart->contents() as $item) {
                $count++;
                // echo json_encode($item);
              ?>
                <input type="hidden" name="rowid" value="<?php echo $item['rowid'] ?>" />
                <input type="hidden" name="rowid<?php echo $item['rowid'] ?>" value="<?php echo $item['rowid'] ?>" />
                <input type="hidden" name="price<?php echo $item['rowid'] ?>" value="<?php echo $item['price'] ?>" />
                <input type="hidden" name="category<?php echo $item['rowid'] ?>" value="<?php echo $item['category'] ?>" />
                <div class="prod-item">
                  <div class="col1">
                    <!-- <img src="<?php echo $item["image"] ?>" alt="<?php echo $item["name"]; ?>" width="86" height="118"> -->
                  </div>
                  <div class="col2">
                    <div class="row m0">
                      <div class="col-md-9 col-lg-9 col-sm-12">
                        <div class="prod-name pb-20">
                          <span>Product Name: <?php echo $item["name"]; ?> </span>
                        </div>
                        <div class="row pb-20">

                          <?php if ($item["category"] == STICKER || $item["category"] == BROCHURE_MIX) { ?>
                            <div class="col-md-3 pb-10 text-center">
                              <span>Height</span>
                              <input type="text" class="updatecart allownumericwithdecimal" name="height<?php echo $item['rowid'] ?>" id="height<?php echo $item['rowid'] ?>" value="<?php echo $item['prd_height']; ?>" style="width: 70%;" data-rowid="<?php echo $item['rowid'] ?>">
                            </div>

                            <div class="col-md-3 pb-10 text-center">
                              <span>Width</span>
                              <input type="text" class="updatecart allownumericwithdecimal" name="width<?php echo $item['rowid'] ?>" id="width<?php echo $item['rowid'] ?>" value="<?php echo $item['prd_width']; ?>" style="width: 70%;" data-rowid="<?php echo $item['rowid'] ?>">
                            </div>
                          <?php } ?>

                          <div class="col-md-3 pb-10 text-center ">
                            <span>Qty</span>
                            <input type="button" name="minus" class="btn btn-primary btn-xs changeqty" value="-" data-rowid="<?php echo $item['rowid'] ?>" />
                            <input type="text" class="updatecart text-center" name="qty<?php echo $item['rowid'] ?>" value="<?php echo $item['qty'] ?>" id="qty<?php echo $item['rowid'] ?>" data-rowid="<?php echo $item['rowid'] ?>" />
                            <input type="button" name="plus" class="btn btn-primary btn-xs changeqty" value="+" data-rowid="<?php echo $item['rowid'] ?>" />

                          </div>

                          <div class="col-md-3 pb-10 text-center">
                            <span>Price</span><br>
                            <span>Rs. <?php echo $this->cart->format_number($item['price']); ?> </span>
                          </div>
                        </div>

                        <div class="custom-file mb-3" style="width:85%;">
                          <input type="file" required class="custom-file-input form-control file_input" name="file_input<?php echo $item['rowid'] ?>" id="file_input<?php echo $item['rowid'] ?>" class="custom-file-input saveFilePath" data-rowid="<?php echo $item['rowid'] ?>" onchange="return designFileValidation(this)">
                          <!-- <input type="file" required class="custom-file-input form-control file_input" name="file_input<?php echo $item['rowid'] ?>" id="file_input<?php echo $item['rowid'] ?>" class="custom-file-input saveFilePath" data-rowid="<?php echo $item['rowid'] ?>"> -->
                          <label id="lbl_file_input" style="font-size: 14px;" class="custom-file-label" for="file_input">Choose file</label>
                          <!-- <br>
                          <img src="<?php echo $item['filepath'] ?>" width="200" id="customFileImg<?php echo $item['rowid'] ?>"  />
                          <br>
                          <div id="disp_tmp_path<?php echo $item['rowid'] ?>"><?php echo $item['filepath'] ?></div> -->
                        </div>

                        <div class="edit-move-delete pb-10" id="remove_cart_product" data-cart_row='<?php echo $item["rowid"]; ?>'>
                          <div class="actions"><span class="confirm-delete-item"><span class="text m-gray confirm-delete-item tappable">REMOVE</span></span></div>
                        </div>
                      </div>
                      <div class="col-md-3 col-lg-3 col-sm-12">
                        <div class="prod-price">
                          <!-- Rs. <?php echo $this->cart->format_number($item['price']); ?> <br> -->
                          Subtotal Rs. <?php echo $this->cart->format_number($item['prd_subtotal']); ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              <?php
                $i++;
              }

              ?>
            </div>

          </div>
          <div class="col-md-4 col-lg-4 col-sm-12">
            <div class="grand-totall">
              <div class="title-wrap">
                <h4 class="cart-bottom-title section-bg-gary-cart">Cart Details</h4>
              </div>
              <h5>Cart total <span>Rs. <?php echo sprintf("%.2f", $this->session->userdata('grandtotal')); ?> </span></h5>
              <!-- 
                                       -->
              <div class="cart-footer">
                <h5>Order Total<span>Rs. <?php echo sprintf("%.2f", $this->session->userdata('grandtotal')); ?> </span></h5>
                <!-- <a class="place-order" href="<?php echo base_url('address-select'); ?>">Checkout</a> -->
                <button type="submit" class="place-order" name="checkout" value="checkout">Checkout</button>
              </div>

            </div>
          </div>
        </div>
      </form>
    <?php } else {
      echo "<h3 class='page-title empty-cart' >Your Cart is empty</h3>";
    }
    ?>
  </div>

  <!--   <div class="modal fade" id="update_cart_modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <form id="person_address_form">

          <div class="modal-body">
            <input type="hidden" name="cart_type" id="cart_type" value="">
            <input type="hidden" name="cart_id" id="cart_id" value="">
            <div class="bd">
              <div class="sizes-cont">
                <div class="title" id="modal-title">Change Flavour</div>
                <div class="sizes" id="change-flavour">
                </div>
                <div class="sizes" id="change-qty"></div>

              </div>
            </div>

          </div>
      </div>

    </div>
    </div> -->
  <!-- /.modal-content -->

  <!-- /.modal-dialog -->
</div>
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url() ?>assets/js/cart.js"></script>

<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

<script type="text/javascript">
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });

  $(".allownumericwithdecimal").on("keypress keyup blur", function(event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });
</script>
</body>

</html>