<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('content_model');
  }

  public function upload_logo($data)
  {
    $_tablename = 'profiles';

    $caption = $data['caption'];
    $details = $data['details'];

    if (isset($_FILES["file"]["type"])) {
      $validextensions = array("jpeg", "jpg", "png");
      $temporary = explode(".", $_FILES["file"]["name"]);
      $file_extension = end($temporary);

      if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") ||
          ($_FILES["file"]["type"] == "image/jpeg")) &&
        ($_FILES["file"]["size"] < 100000) &&
        in_array($file_extension, $validextensions)
      ) {
        if ($_FILES["file"]["error"] > 0) {
          echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
          $sourcePath = $_FILES['file']['tmp_name']; // Store source path in a variable
          $targetPath = "uploads/profiles/" . $_FILES['file']['name']; // The Target path where file is to be stored
          move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
          // The Image Data
          $imageData = [
            'caption'   => $caption,
            'description'   => $details,
          ];

          // Insert the data
          $this->db->insert($this->_tablename, $imageData);
        }
      }
    }
  }

  /* CODE BY-KISHAN START*/
  function generateOrderReferenceNumber($orderPersonId)
  {
    $isUnique = false;
    $order_reference_no = '';

    while ($isUnique != true) {
      $orderId = mt_rand(0, 1000000) . "-" . mt_rand(0, 1000000);
      $OrdReferenceId = $orderPersonId . "-" . $orderId;
      $this->db->where('ord_reference_no', $OrdReferenceId);
      $q = $this->db->get('person_order');

      if ($q->num_rows() > 0) {
        continue;
      } else {
        $order_reference_no = $orderId;
        break;
      }
    }
    return $order_reference_no;
  }

  public function create_order($ord_reference_no)
  {
    // $this->db->trans_start(TRUE);
    //Person Order
    $cart_total = sprintf("%.2f", $this->session->userdata('grandtotal'));
    $prs_id = $this->session->userdata(PROJECT_SESSION_ID);
    $order_sub_total = $cart_total;
    $order_grand_total = $cart_total + ORDER_DELIVER_CHARGE;
    $order_data = array();
    $order_data['ord_reference_no'] = $ord_reference_no;
    $order_data['ord_prs_id'] = $prs_id;
    $order_data['ord_delivery_adddress'] = 0;
    $order_data['ord_payment_mode'] = ORDER_PAYMENT_MODE_PAY_FROM_BALANCE;
    $order_data['ord_payment_status'] = ORDER_PAYMENT_STATUS_PENDING;
    $order_data['ord_sub_total'] = $order_sub_total;
    $order_data['ord_shipping_charges'] = ORDER_DELIVER_CHARGE;
    $order_data['ord_update_sent_mobile'] = $this->session->userdata(PROJECT_SESSION_MOB);
    $order_data['ord_update_sent_email'] = $this->session->userdata(PROJECT_SESSION_EMAIL);
    $order_data['ord_total_amt'] = $order_grand_total;
    $order_data['ord_date'] = date('Y-m-d');
    $order_data['ord_is_success'] = ORDER_IS_SUCCESS_PROCESSING;
    $order_data['ord_crtd_dt'] = date('Y-m-d H:i:s');
    $order_data['ord_crtd_by'] = $prs_id;
    $order_data['ord_updt_by'] = $prs_id;
    // print_r($order_data);
    $ord_id = $this->home_model->insert('person_order', $order_data);
    return $ord_id;
  }

  public function insertOrderProducts($ord_id, $filesArray)
  {
    $ord_sts_id = 0;
    if (!empty($ord_id)) {
      // Person Order Products
      $prs_id = $this->session->userdata(PROJECT_SESSION_ID);
      foreach ($this->cart->contents() as $items) {
        $last_number = $this->home_model->getLastInvoiceNumber();
        $rowid = $items['rowid'];

        if (empty($last_number)) {
          $odp_invoice_number = INVOICE_NUMBER_STARTS;
        } else {
          $odp_invoice_number = $last_number->odp_invoice_number + 1;
        }

        $ord_product_data = array(
          'odp_ord_id' => $ord_id,
          'odp_invoice_number' => $odp_invoice_number,
          'odp_prd_id' => $items['id'],
          'odp_quantity' => $items['qty'],
          'odp_height' => $items['prd_height'] == '' ? 0 : $items['prd_height'],
          'odp_width' => $items['prd_width']  == '' ? 0 : $items['prd_width'],
          'odp_quantity' => $items['qty'],
          'odp_file' => $filesArray[$rowid],
          'odp_amt' => $items['price'],
          'odp_subtotal' => $items['prd_subtotal'],
          'odp_total_amt' => $items['prd_subtotal'],
          'ord_shipping_charges' => 0,
          'odp_status' => PLACED,
          'odp_date' => date('Y-m-d'),
          'odp_crtd_dt' => date('Y-m-d H:i:s'),
          'odp_crtd_by' => $prs_id,
          'odp_updt_by' => $prs_id,
        );
        $this->db->insert('person_order_products', $ord_product_data);

        $order_product_status_data = array();
        $order_product_status_data['ops_status'] = PLACED;
        $order_product_status_data['ops_remark'] = '';
        $order_product_status_data['ops_ord_id'] = $ord_id;
        $order_product_status_data['ops_prd_id'] = $items['id'];
        $order_product_status_data['ops_crtd_dt']   = date('Y-m-d H:i:s');
        $order_product_status_data['ops_crtd_by']   = $prs_id;
        $order_product_status_data['ops_updt_by']   = $prs_id;
        $ord_sts_id = $this->home_model->insert('person_order_product_status', $order_product_status_data);
      }
    }
    return $ord_sts_id;
  }

  public function payFromBalanceButton($order_id, $prs_id, $shippingAddressData)
  {
    if (!empty($order_id) && !empty($prs_id) && !empty($shippingAddressData)) {
      /* Get wallet detail */
      $wallet_sql = "SELECT * FROM `wallet` WHERE `prs_id`=" . $prs_id;
      $wallet_query = $this->db->query($wallet_sql);
      $wallet_result = $wallet_query->row();
      if (empty($wallet_result) || $wallet_result == '') {
        return false;
      }

      /* Get person_order detail */
      $prs_order_sql = "SELECT * FROM `person_order` WHERE `ord_id`= '" . $order_id . "' AND `ord_prs_id` = " . $prs_id;
      $prs_order_query = $this->db->query($prs_order_sql);
      $prs_order_result = $prs_order_query->row();
      if (empty($prs_order_result) || $prs_order_result == '') {
        return false;
      }

      if ($wallet_result->amount >= $prs_order_result->ord_total_amt) {
        /* Deduct amount from wallet balance */
        $wallet_bal = number_format((float)$wallet_result->amount, 2, '.', '');
        $order_amount = number_format((float)$prs_order_result->ord_total_amt, 2, '.', '');
        $closing_balance = $wallet_bal - $order_amount;
        $deduct_wallet_amount_sql = "UPDATE `wallet` SET `amount` = '" . $closing_balance . "', `updated_on`= '" . date('Y-m-d H:i:s') . "' WHERE `prs_id` =" . $prs_id;
        $deduct_wallet_amount_query = $this->db->query($deduct_wallet_amount_sql);

        if ($deduct_wallet_amount_query == 1) {
          /* Add entry in wallet history */
          $walletHistoryData = array();
          $walletHistoryData['prs_id']          = $prs_id;
          $walletHistoryData['wallet_id']       = $wallet_result->id;
          $walletHistoryData['odp_ord_id']      = $order_id;
          $walletHistoryData['description']     = "Pay for order Id# " . $prs_order_result->ord_reference_no;
          $walletHistoryData['type']            = DEBIT;
          $walletHistoryData['transaction_mode'] = PAY_FROM_BALANCE;
          $walletHistoryData['amount']          = $prs_order_result->ord_total_amt;
          $walletHistoryData['closing_balance'] = $closing_balance;
          $walletHistoryData['created_on']      = date('Y-m-d H:i:s');
          $walletHistory_id = $this->home_model->insert('wallet_history', $walletHistoryData);
          if ($walletHistory_id == '-1') {
            return false;
          }
          /* Update person_order status */
          $update_order_status_sql = "UPDATE `person_order` SET `ord_is_success` = " . ORDER_IS_SUCCESS_SUCCESS . " , `ord_status`= " . ORDER_STATUS_PLACED . ", `ord_payment_status` = " . ORDER_PAYMENT_STATUS_PAID . ", `ord_delivery_adddress` = '" . $shippingAddressData->pad_id . "', `ord_update_sent_mobile`= '" . $shippingAddressData->pad_mobile . "' WHERE `ord_id` =" . $order_id;
          $update_order_status_query = $this->db->query($update_order_status_sql);

          /* Insert Order Status */
          // $order_status_data = array();
          // $order_status_data['ods_ord_id']  = $order_id;
          // $order_status_data['ods_status']  = ORDER_STATUS_SUCCESS;
          // $order_status_data['ods_date']    = date('Y-m-d');
          // $order_status_data['ods_crtd_by'] = $prs_id;
          // $order_status_data['ods_updt_by'] = $prs_id;
          // $ord_sts_id = $this->home_model->insert('order_status', $order_status_data);
          // if ($update_order_status_query != 1) {
          //   return false;
          // }
          return $update_order_status_query == 1 ? true : false;
        } else {
          return false;
        }
      } else {
        return false;
      }

      // $order_status_data = array();

      // $order_status_data['ods_date']   = date('Y-m-d');
      // $ord_sts_id = $this->home_model->insert('order_status', $order_status_data);

      // $this->load->library('cart');
      // $this->cart->destroy();
      return true;
    } else {
      return false;
    }
  }

  /* Delete Incomplete Order & Order Items of Current User */
  public function deleteIncompleteOrderOfCurrentUser($prs_id)
  {
    $returnValue = true;
    $Incomplete_order_sql = "SELECT `ord_id` FROM `person_order` WHERE `ord_is_success`= " . ORDER_IS_SUCCESS_PROCESSING . " AND `ord_prs_id` = " . $prs_id;
    $Incomplete_order_query = $this->db->query($Incomplete_order_sql);
    $Incomplete_orders = $Incomplete_order_query->result();

    $count = 0;
    foreach ($Incomplete_orders as $order) {
      $a[$count] = $order->ord_id;

      $delete_incomplete_order_sql = "DELETE FROM `person_order` WHERE `ord_id` = '" . $order->ord_id . "' AND `ord_prs_id` = " . $prs_id;
      $delete_incomplete_order_query = $this->db->query($delete_incomplete_order_sql);

      if ($delete_incomplete_order_query) {
        $order_item_sql = "SELECT * FROM `person_order_products` WHERE `odp_ord_id` = " . $order->ord_id;
        $order_item_query = $this->db->query($order_item_sql);
        $order_items = $order_item_query->result();

        $target_dir = DESIGNFILE_UPLOAD_DIRECTORY;
        foreach ($order_items as $item) {
          $delete_order_item_sql = "DELETE FROM `person_order_products` WHERE `odp_id` = " . $item->odp_id;
          $delete_order_item_result = $this->db->query($delete_order_item_sql);

          if ($delete_order_item_result) {
            $delete_order_product_status_sql = "DELETE FROM `person_order_product_status` WHERE `ops_ord_id` = " . $order->ord_id;
            $delete_order_product_status_result = $this->db->query($delete_order_product_status_sql);

            $path_to_file = $target_dir . $item->odp_file;
            if (unlink($path_to_file)) {
              // echo "File" . $item->odp_file . " deleted successfully";
            } else {
              // echo "Errors occured while deleting file" . $item->odp_file;
              $returnValue = false;
              break;
            }
          }
        }
      }
      $count++;
    }
    return $returnValue;
  }
  /* CODE BY-KISHAN END*/

  public function place_order($ord_reference_no, $ord_update_sent_mobile)
  {
    // $this->db->trans_start(TRUE);
    //Person Order
    $prs_id = $this->session->userdata(PROJECT_SESSION_ID);
    $order_sub_total = $this->cart->total();
    $order_grand_total = $this->cart->total() + ORDER_DELIVER_CHARGE;
    $order_data = array();
    $order_data['ord_reference_no'] = $ord_reference_no;
    $order_data['ord_prs_id'] = $prs_id;
    $order_data['ord_delivery_adddress'] = $this->input->post('ord_deliver_address');
    $order_data['ord_payment_mode'] = $this->input->post('ord_payment_mode');
    $order_data['ord_sub_total'] = $order_sub_total;
    $order_data['ord_shipping_charges'] = $this->input->post('ord_shipping_charges');
    $order_data['ord_update_sent_mobile'] = $ord_update_sent_mobile;
    $order_data['ord_update_sent_email'] = $this->session->userdata(PROJECT_SESSION_EMAIL);
    $order_data['ord_total_amt'] = $order_grand_total;
    $order_data['ord_date'] = date('Y-m-d');
    $order_data['ord_status'] = ORDER_STATUS_PLACED;
    $order_data['ord_is_success'] = ORDER_IS_SUCCESS_SUCCESS;
    $order_data['ord_crtd_dt'] = date('Y-m-d H:i:s');
    $order_data['ord_crtd_by'] = $prs_id;
    // print_r($order_data);
    $ord_id = $this->home_model->insert('person_order', $order_data);

    if ($ord_id != '-1') {
      // Person Order Products
      foreach ($this->cart->contents() as $items) {
        $last_number = $this->home_model->getLastInvoiceNumber();
        if (empty($last_number)) {
          $odp_invoice_number = INVOICE_NUMBER_STARTS;
        } else {
          $odp_invoice_number =   $last_number->odp_invoice_number + 1;
        }
        $ord_product_data = array(
          'odp_ord_id' => $ord_id,
          'odp_invoice_number' => $odp_invoice_number,
          'odp_prd_id' => $items['id'],
          'odp_flavour' => $items['flavour'],
          'odp_quantity' => $items['qty'],
          'odp_amt' => $items['price'],
          'odp_subtotal' => $items['subtotal'],
          'odp_total_amt' => $items['subtotal'],
          'odp_status' => PLACED,
          'odp_date' => date('Y-m-d'),
          'odp_crtd_dt' => date('Y-m-d H:i:s'),
          'odp_crtd_by' => $prs_id
        );
        $this->db->insert('person_order_products', $ord_product_data);
        $order_product_status_data = array();
        $order_product_status_data['ops_status'] = PLACED;
        $order_product_status_data['ops_ord_id'] = $ord_id;
        $order_product_status_data['ops_prd_id'] = $items['id'];
        $order_product_status_data['ops_crtd_dt']   = date('Y-m-d H:i:s');
        $order_product_status_data['ops_crtd_by']   = $prs_id;
        $ord_sts_id = $this->home_model->insert('person_order_product_status', $order_product_status_data);
      }

      // $order_status_data = array();
      // $order_status_data['ods_status'] = PLACED;
      // $order_status_data['ods_ord_id'] = $ord_id;
      // $order_status_data['ods_date']   = date('Y-m-d');
      // $ord_sts_id = $this->home_model->insert('order_status', $order_status_data);

      $this->load->library('cart');
      $this->cart->destroy();
    }

    return $ord_id;
  }
  public function getOrderData($ord_id, $field_name = '')
  {
    $sql = "SELECT *,
    (SELECT prs_mob from person where prs_id=ord_prs_id LIMIT 1) ord_prs_mob,
    (SELECT prs_email from person where prs_id=ord_prs_id LIMIT 1) ord_prs_email,
    (select gen_prm.gnp_name from gen_prm where gen_prm. gnp_value=ord_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
    (select gen_prm.gnp_name from gen_prm where gen_prm. gnp_value=ord_status and gen_prm.gnp_group='order_status') ord_status_name
     FROM `person_order` where ";
    if ($field_name != '') {
      $sql .= " ord_reference_no ='" . $ord_id . "'";
    } else {
      $sql .= "ord_id='" . $ord_id . "'";
    }
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function getOrderProducts($ord_id)
  {
    $sql = "SELECT *,
    (SELECT prd_name from products where prd_id=odp_prd_id) prd_name,
     (select  images.img_path from images where images. img_type_id=odp_prd_id and  img_type=" . PRODUCT_IMG_TYPE . "  limit 1) as img_path
     FROM `person_order_products` where odp_ord_id='" . $ord_id . "'";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getOrderListData()
  {
    $prs_id = $this->session->userdata(PROJECT_SESSION_ID);
    if ($prs_id != '') {
      $sql = "SELECT *,
        (SELECT prs_mob FROM person WHERE prs_id=ord_prs_id LIMIT 1) ord_prs_mob,
        (SELECT prs_email FROM person WHERE prs_id=ord_prs_id LIMIT 1) ord_prs_email,
        (SELECT gen_prm.gnp_name FROM  gen_prm WHERE gen_prm.gnp_value=ord_payment_mode AND gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
        (select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=ord_status AND gen_prm.gnp_group='order_status') ord_status_name        
         FROM `person_order` WHERE ord_prs_id='" . $prs_id . "'  AND ord_is_success != " . ORDER_IS_SUCCESS_PROCESSING . " ORDER BY  ord_crtd_dt DESC";
      $query = $this->db->query($sql);
      $result = $query->result();
      return $result;
    }
  }
  public function getOrderDetail($ord_reference_no)
  {
    $sql = "SELECT *,
        (SELECT prs_name from person where prs_id=ord_prs_id LIMIT 1) ord_prs_name,
        (SELECT prs_mob from person where prs_id=ord_prs_id LIMIT 1) ord_prs_mob,
        (SELECT prs_email from person where prs_id=ord_prs_id LIMIT 1) ord_prs_email,
        (SELECT COUNT(*) from person_order_products where odp_ord_id=ord_id ) ord_prd_count,
        (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=ord_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
        (select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=ord_status AND gen_prm.gnp_group='order_status') ord_status_name
         FROM `person_order` left join person_addresses on person_addresses.pad_id = person_order.ord_delivery_adddress   where ord_prs_id='" . $this->session->userdata(PROJECT_SESSION_ID) . "' and ord_reference_no='" . $ord_reference_no . "'  order by  ord_crtd_dt desc";
    $query = $this->db->query($sql);
    return $query->row();
  }

  public function getOrderAllProducts($ord_id = false)
  {
    $sql = "SELECT *,
    (select flv_name from product_flavours where flv_id = odp_flavour) prd_flavour,
    (SELECT prd_name from products where prd_id=odp_prd_id) prd_name,
    (select gen_prm.gnp_name from  gen_prm where gen_prm.gnp_value=odp_status and gen_prm.gnp_group='order_status') prd_status,
    (select  product_flavor_images.pfi_img from product_flavor_images where product_flavor_images.pfi_prd_id=person_order_products.odp_prd_id  limit 1) as img_path,
    (SELECT ops_status from person_order_product_status WHERE ops_ord_id = odp_ord_id AND  ops_prd_id = odp_prd_id ORDER BY person_order_product_status.ops_id DESC LIMIT 1) ops_status,
    (SELECT ops_remark from person_order_product_status WHERE ops_ord_id = odp_ord_id AND  ops_prd_id = odp_prd_id ORDER BY person_order_product_status.ops_id DESC LIMIT 1) ops_remark,
    (select gen_prm.gnp_name from  gen_prm where gen_prm.gnp_value=ops_status and gen_prm.gnp_group='order_prd_status') ops_status_name
     FROM `person_order_products`";
    if ($ord_id) {
      $sql .= " where odp_ord_id = " . $ord_id . "";
    }
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getOrderAllProductsStatus($ord_id = false)
  {
    $sql = "SELECT *,
    (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=ops_status and gen_prm.gnp_group='order_prd_status') prd_status FROM `person_order_product_status`";
    if ($ord_id) {
      $sql .= " where ops_ord_id = " . $ord_id . "";
    }
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getOrderProductStatus($ops_prd_id, $ord_id)
  {
    $sql = "SELECT ops_status, ops_crtd_dt, (SELECT gen_prm.gnp_name FROM gen_prm WHERE gen_prm.gnp_value = person_order_product_status.ops_status and gen_prm.gnp_group = 'order_prd_status') as status, 'completed' as class FROM `person_order_product_status` WHERE `ops_prd_id` = " . $ops_prd_id . " and ops_ord_id=" . $ord_id . "
UNION 
SELECT gnp_value, '' as ops_crtd_dt, gnp_name as status,'' as class from gen_prm WHERE gnp_group = 'order_prd_status' and gnp_status = 1 and gnp_value NOT in (SELECT ops_status FROM `person_order_product_status` WHERE `ops_prd_id` = " . $ops_prd_id . " and ops_ord_id=" . $ord_id . " )";

    $query = $this->db->query($sql);
    return $query->result();
  }
  public function updateOrderStatus($ord_reference_no, $status)
  {
    $orderData = array();
    $orderData['ord_is_success'] = $status;
    return $this->home_model->update('ord_reference_no', $ord_reference_no, $orderData, 'person_order');
  }
  public function sendOrderSuccessMail($ord_reference_no)
  {
    if ($ord_reference_no != '') {
      $ord_data = $this->getOrderDetail($ord_reference_no);
      if ($ord_data != '') {
        $customerOrderSuccess = array();
        // Customer Email
        $receiver_data = array(
          'email' =>  $this->session->userdata(PROJECT_SESSION_EMAIL),
          'name' =>  $ord_data->pad_name,
          'mobile' =>  $ord_data->pad_mobile,
          'alt_mobile' => $ord_data->pad_alt_phone ? $ord_data->pad_alt_phone : '',
        );
        $mailData =  $this->content_model->getContent('user_order_placed', $receiver_data, $ord_data);
        $this->home_model->sendMail($ord_data->ord_prs_email, $mailData);
        $this->home_model->SendMessage($mailData['msg_body'], $ord_data->ord_prs_mob);

        $this->load->model('communication_model'); // load model
        $adminDetails = $this->communication_model->getAdminDetails();
        $adminsEmailList = $this->communication_model->getAdminsEmailList();
        $emailTo = "";
        foreach ($adminsEmailList as $email) {
          $emailTo .= $email . ",";
        }

        $mailData =  $this->content_model->getContent('admin_order_placed', '', $ord_data);
        $this->home_model->sendMail($emailTo, $mailData);
        // $this->home_model->SendMessage($mailData['msg_body'], $adminDetails->usr_mobile);
        // log_message('NEXLOG', 'Mail sent to all admins of order place successfully >> order_model/sendOrderSuccessMail || SentTo:' . json_encode($adminsEmailList));
      }
    }
  }
  /* Code By Kishan- Start */
  public function getLastGenerated_ord_reference_no($ord_id)
  {
    $sql = "SELECT * FROM `person_order` WHERE `ord_id`=" . $ord_id;
    $query = $this->db->query($sql);
    $result = $query->row();
    return json_encode($result);
  }
  public function getWalletDetailByPersonId($person_id)
  {
    $sql = "SELECT * FROM `wallet` WHERE `prs_id`=" . $person_id;
    $query = $this->db->query($sql);
    $result = $query->row();
    return json_encode($result);
  }

  public function addWalletBalance($transactionData, $walletData)
  {
    try {
      // $deductAmount = round(floatval($transactionData['pyt_txn_amt'] * 3 / 100), 2);
      // $amountAfterDeduct = round($transactionData['pyt_txn_amt'] - $deductAmount, 2);
      $transactionAmt = $transactionData['transaction_amount'];
      $totalAmount = round(floatval($walletData['amount'] + $transactionAmt), 2);
      $data = array(
        'amount'  => $totalAmount,
        'updated_on' => date('Y-m-d H:i:s')
      );
      $this->db->where('prs_id', $transactionData['pyt_crtd_by']);
      $isBalanceAdded = $this->db->update('wallet', $data);

      if ($isBalanceAdded) {
        $walletHistoryData = array(
          'prs_id' => $transactionData['pyt_crtd_by'],
          'wallet_id' => $walletData['id'],
          'type' => TRANSACTION_TYPE_CREDIT,
          'transaction_mode' => PAYMENT_GATEWAY,
          'amount' => $transactionAmt,
          'closing_balance' => $totalAmount,
          'description' => "Balance Added of OrderId#" . $transactionData['pyt_order_reference_no'],
          'created_on' => date('Y-m-d H:i:s')
        );

        $this->db->insert('wallet_history', $walletHistoryData);
      }
      return true;
    } catch (Exception $e) {
      log_message('NEXLOG', 'error occured while adding wallet balance >> order_model/addWalletBalance ::ERROR-' . $e);
      return false;
    }
  }
  /* Code By Kishan- End */
}
