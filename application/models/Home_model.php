<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('email');
    require_once(APPPATH . "/config/mail_template.php");
  }
  public function getBannerImages($type)
  {
    $sqlQuery = "Select * from `gallery_set` where `gls_status` = '" . ACTIVE_STATUS . "' and gls_type = '" . $type . "' ";
    $query = $this->db->query($sqlQuery);
    $result = $query->result();
    return $result;
  }
  function insert($table, $array)
  {
    $this->db->insert($table, $array);
    $id = $this->db->insert_id();
    return $id;
  }
  public function getGenPrmResultByGroup($gpm_group)
  {
    $sqlQuery   = "SELECT  gnp_value as f1,gnp_name as f2  FROM gen_prm where gnp_group ='" . $gpm_group . "' and gnp_status='" . ACTIVE_STATUS . "' order by gnp_order";
    $query = $this->db->query($sqlQuery);
    $result = $query->result();
    return $result;
  }
  public function getStateDropdown()
  {
    $sqlQuery = "SELECT stm_id as f1,stm_name as f2 FROM state_master where stm_status='" . ACTIVE_STATUS . "' ORDER by f2 ASC";
    $query = $this->db->query($sqlQuery);
    $result = $query->result();
    return $result;
  }
  function update($field, $id, $array, $table)
  {
    $this->db->where($field, $id);
    $this->db->update($table, $array);
    return $id;
  }
  function getAdminsEmailList()
  {
    $sql = "SELECT `usr_email` FROM `user` WHERE `usr_dpt_id`=" . DEPARTMENT_ADMIN . " AND `user_isMailSend`=".ACTIVE_STATUS."";
    $query = $this->db->query($sql);
    $arrayResult = $query->result_array();
    $result = array_map(function ($value) {
      return $value['usr_email'];
    }, $arrayResult);
    return $result;
  }
  public function sendMail($emailTo, $email_data)
  {
    $email_notifiy = $this->getBusinessParamData('email');
    if (!empty($email_notifiy) && $email_notifiy->bpm_status) {
      log_message('error', 'Email configurations -- ' . EMAIL_PROTOCOL . ' -- ' . EMAIL_HOST . ' -- ' . EMAIL_USERNAME . ' -- ' . EMAIL_PASSWORD . ' -- ' . EMAIL_TYPE . ' -- ' . EMAIL_CHARSET . ' -- ' . EMAIL_WORDWRAP);

      if (SEND_MAIL_USING_SMTP) {
        if ($emailTo != '') {
          $email_config = array(
            'protocol' => EMAIL_PROTOCOL,
            'smtp_host' => EMAIL_HOST,
            'smtp_port' => EMAIL_PORT,
            'smtp_user' => EMAIL_USERNAME,
            'smtp_pass' => EMAIL_PASSWORD,
            'mailtype'  => EMAIL_TYPE,
            'charset'   => EMAIL_CHARSET,
            'wordwrap' => EMAIL_WORDWRAP
          );

          $this->email->initialize($email_config);
          $this->email->clear(TRUE);
          $this->email->set_newline("\r\n");
          $this->email->from(EMAIL_USERNAME);
          $this->email->subject($email_data['mail_subject']);
          $this->email->message($email_data['mail_body']);
          $this->email->to($emailTo);
          log_message('error', 'CRIMINAL SUBJECT' . $email_data['mail_subject'] . ' || MESSAGE : ' . $email_data['mail_body']);
          log_message('error', 'CRIMINAL EMAIL' . json_encode($emailTo));

          if ($this->email->send()) {
            return true;
          } else {
            log_message('error', 'error occured while sending mail in home_model/sendMail' . $this->email->print_debugger());
            echo $this->email->print_debugger();
            return false;
          }
        } else {
          return false;
        }
      } else {
        if ($emailTo != '') {
          $to = $emailTo;
          $subject = $email_data['mail_subject'];
          $message = $email_data['mail_body'];

          // Always set content-type when sending HTML email
          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          // More headers
          $headers .= "From: Prism Prints <" . EMAIL_USERNAME . ">" . "\r\n";

          if (mail($to, $subject, $message, $headers)) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    }
  }
  public function getBusinessParamData($busParamname)
  {
    $businessParamSql = "SELECT * FROM bsn_prm where bpm_status = '" . ACTIVE_STATUS . "' and bpm_name='" . $busParamname . "' ";
    $query = $this->db->query($businessParamSql);
    $row = $query->row();
    return $row;
  }
  public function getLastInvoiceNumber()
  {
    $businessParamSql = "SELECT odp_invoice_number FROM person_order_products order by odp_id desc limit 1";
    $query = $this->db->query($businessParamSql);
    return $query->row();
  }
  function generateRandomStringNum($length)
  {
    $randomNumber = '';
    for ($i = 0; $i < $length; $i++) {
      $randomNumber .= mt_rand(0, 9);
    }
    return $randomNumber;
  }
  public function SendMessage($message, $mobile_no)
  {
    $sms_notifiy = $this->getBusinessParamData('sms');
    if (!empty($sms_notifiy) && $sms_notifiy->bpm_status) {
      log_message('error', '>>mobile no = ' . $mobile_no);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,  "http://api.msg91.com/api/sendhttp.php?");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "?country=91&sender=MSGIND&route=4&mobiles=$mobile_no&authkey=243235A5ebDlzp5bc72911&message=$message");
      $buffer = curl_exec($ch);
      log_message('error', '>>mobile no = ' . $mobile_no);
      return true;
    }
  }
  public function dateinmysql($datefromuser = '')
  {
    if (empty($datefromuser)) {
      return '0000-00-00';
    } else {
      $date = str_replace('/', '-', $datefromuser);
      return date('Y-m-d', strtotime($date));
    }
  }
  public  function getNewCode($data)
  {
    $type = $this->getBusinessParamData($data['type']);
    if (!isset($type->bpm_name)) return '';
    $column_prefix = explode('_', $data['column'])[0];
    $code = 0;
    if (isset($type->bpm_default_value) && $type->bpm_default_value != '') {
      $code = $type->bpm_default_value;
    }
    $sqlQuery = "select IFNULL(" . $data['column'] . ",0) newcode from " . $data['table'] . " where " . $column_prefix . "_status = '" . ACTIVE_STATUS . "'";
    if (isset($data['where']) && $data['where'] != '') $sqlQuery .= " and " . $data['where'];
    $sqlQuery .= " order by " . $column_prefix . "_id desc limit 1";
    $query = $this->db->query($sqlQuery);
    $result = $query->row();
    $code = $type->bpm_value . $code;
    // echo 'new code : '.$result->newcode.' || code : '.$code;
    if (!empty($result) && $result->newcode >= $code) {
      $code = $result->newcode;
      $generated_code =  $type->bpm_value . str_pad((str_replace($type->bpm_value, '', $code) + 1), 4, '0', 0);
    } else {
      $generated_code = $code;
    }
    return $generated_code;
  }

  public function check_ord_txn_bsnprm($data = array())
  {

    $this->db->from('bsn_prm');

    if (isset($data['PAYTM_PAY_STATUS']) && $data['PAYTM_PAY_STATUS'] != '') {
      $this->db->where('bpm_name', 'PAYTM_PAY_STATUS');
      $this->db->where('bpm_value', $data['PAYTM_PAY_STATUS']);
    }

    if (isset($data['PAYTM_PAY_RESPONSE_CODE']) && $data['PAYTM_PAY_RESPONSE_CODE'] != '') {
      $this->db->where('bpm_name', 'PAYTM_PAY_RESPONSE_CODE');
      $this->db->where('bpm_value', $data['PAYTM_PAY_RESPONSE_CODE']);
    }

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
      $result = $query->result();
      return $result[0]->bpm_id;
    }
    return false;
  }

  public function getPaymentReceiptStatus($prs_id)
  {
    $sql = "SELECT * FROM `payment_rcpt` WHERE `upload_by`= '" . $prs_id . "' ORDER BY `update_on` DESC";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getPaymentLedger($prs_id)
  {
    $sql = "SELECT * FROM `wallet_history` WHERE `prs_id`= '" . $prs_id . "' ORDER BY `created_on` DESC";
    // $sql = "SELECT * FROM `wallet_history` 
    //   LEFT JOIN `wallet` USING (id, prs_id) 
    //   WHERE wallet_history.prs_id= wallet.prs_id AND wallet_history.wallet_id= wallet.id AND
    //   wallet_history.prs_id= '" . $prs_id . "' ORDER BY wallet_history.created_on DESC";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getAllPaymentTypes()
  {
    $sql = "SELECT * FROM `gen_prm` WHERE `gnp_group`= 'payment_type' AND `gnp_status`= '" . ACTIVE_STATUS . "' ORDER BY `gnp_order`";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  /* Complaints Methods- Start */
  public function getAllCompaintsByPersonId($prs_id)
  {
    $sql = "SELECT *,
    (SELECT `cm_desc` FROM `complains_master` WHERE `cm_id`= complains.com_desc_id) com_desc_name,
    (SELECT `gnp_name` FROM `gen_prm` WHERE `gnp_value`= complains.com_status AND `gnp_status`=" . ACTIVE_STATUS . " AND `gnp_group`='complains_status') com_status_name
    FROM `complains` WHERE com_prs_id='" . $prs_id . "' ORDER BY `com_updated_on` DESC";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getCompaintsDropdown()
  {
    $sqlQuery = "SELECT `cm_id` AS f1, `cm_desc` AS f2 FROM `complains_master` WHERE `cm_isactive`='" . ACTIVE_STATUS . "' ORDER BY f2 ASC";
    $query = $this->db->query($sqlQuery);
    $result = $query->result();
    return $result;
  }
  public function getCompaintDescById($com_desc_id)
  {
    $sql = "SELECT * FROM `complains_master` WHERE cm_id='" . $com_desc_id . "'";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  function registerComplaint($data)
  {
    $result = $this->db->insert('complains', $data);
    return $result;
  }
  function sendMailForRegisterComplaintSuccess($complaintData)
  {
    if ($complaintData != '') {
      $complaintData = json_decode(json_encode($complaintData), FALSE);

      $this->load->model('person_model'); // load model      
      $personData = $this->person_model->getUserData($complaintData->com_prs_id);
      if ($personData != '') {
        $complaintDescData = $this->getCompaintDescById($complaintData->com_desc_id);

        $this->load->model('content_model'); // load model
        $this->load->model('communication_model'); // load model       

        $adminsEmailList = $this->communication_model->getAdminsEmailList();
        $emailTo = "";
        foreach ($adminsEmailList as $email) {
          $emailTo .= $email . ",";
        }
        $contentDataForAdminMail = array(
          'customer_name' => $personData->prs_name,
          'order_number' => $complaintData->com_ord_id,
          'complaint_desc' => $complaintDescData->cm_desc,
          'complaint_date' => $complaintData->com_created_on
        );
        $contentDataForAdminMail = json_decode(json_encode($contentDataForAdminMail), FALSE);
        $mailData = $this->content_model->getContent('admin_complaint_received', '', $contentDataForAdminMail);
        return $this->home_model->sendMail($emailTo, $mailData);
      }
    }
  }
  /* Complaints Methods- End */

  /* Quotation Methods- Start */
  public function getAllQuotationsByPersonId($personId)
  {
    $sql = "SELECT * FROM `quotation_request` WHERE `qr_prs_id`='" . $personId . "' ORDER BY `qr_updated_on` DESC";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getQuotationsById($personId, $referenceNo)
  {
    $sql = "SELECT * FROM `quotation_request` WHERE `qr_prs_id`='" . $personId . "' AND `qr_reference_no` = '" . $referenceNo . "'";
    $query = $this->db->query($sql);
    $result = $query->row();
    return $result;
  }

  function sendQuotationRequest($data)
  {
    $result = $this->db->insert('quotation_request', $data);
    return $result;
  }
  function sendMailToAdminForQuotationRequest($quotationData)
  {
    if ($quotationData != '') {
      $quotationData = json_decode(json_encode($quotationData), FALSE);

      $this->load->model('person_model'); // load model      
      $personData = $this->person_model->getUserData($quotationData->qr_prs_id);
      if ($personData != '') {

        $this->load->model('content_model'); // load model
        $this->load->model('communication_model'); // load model       

        // $adminsEmailList = $this->communication_model->getAdminsEmailList();
        // $emailTo = "";
        // foreach ($adminsEmailList as $email) {
        //   $emailTo .= $email . ",";
        // }
        $contentDataForAdminMail = array(
          'customer_name' => $personData->prs_name,
          'qr_reference_no' => $quotationData->qr_reference_no,
          'qr_product_name' => $quotationData->qr_product_name,
          'qr_paper_gsm_size' => $quotationData->qr_paper_gsm_size,
          'qr_creasing' => $quotationData->qr_creasing,
          'qr_half_cut' => $quotationData->qr_half_cut,
          'qr_uv' => $quotationData->qr_uv,
          'qr_lamination_type' => $quotationData->qr_lamination_type,
          'qr_lamination' => $quotationData->qr_lamination,
          'qr_qty' => $quotationData->qr_qty,
          'qr_remarks' => $quotationData->qr_remarks,
          'qr_created_on' => $quotationData->qr_created_on,
        );
        $contentDataForAdminMail = json_decode(json_encode($contentDataForAdminMail), FALSE);
        $mailData = $this->content_model->getContent('admin_quotation_request', '', $contentDataForAdminMail);
        return $mailData;
        return $this->home_model->sendMail(EMAIL_ID_QUOTATION, $mailData);
      }
    }
  }
  /* Quotation Methods- ENd */
}
