<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Person_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }


  public function getPersonAddress($pad_prs_id)
  {
    $sql = "SELECT *,(SELECT gnp_name from gen_prm where gnp_status=" . ACTIVE_STATUS . " and gnp_group='address_type' and gnp_value=pad_address_type) address_type,
     (SELECT stm_name from state_master where stm_id=pad_state) state_name FROM `person_addresses` where pad_status=" . ACTIVE_STATUS . " and pad_prs_id='" . $pad_prs_id . "' order by pad_crtd_dt";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getPersonDefaultAddress($prs_id)
  {
    $sql = "SELECT prs_default_address  FROM `person` where prs_id=" . $prs_id . "";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row->prs_default_address;
  }
  public function updatePersonAddress()
  {
    $pad_id = $this->input->post('pad_id');
    $padData = array();
    $padData['pad_prs_id']        = $this->input->post('pad_prs_id');
    $padData['pad_address_type']  = $this->input->post('pad_address_type');
    $padData['pad_name']          = $this->input->post('pad_name');
    $padData['pad_mobile']        = $this->input->post('pad_mobile');
    $padData['pad_pincode']       = $this->input->post('pad_pincode');
    $padData['pad_city']          = $this->input->post('pad_city');
    $padData['pad_state']         = $this->input->post('pad_state');
    $padData['pad_locality']      = $this->input->post('pad_locality');
    $padData['pad_address']       = $this->input->post('pad_address');
    $padData['pad_landmark']      = $this->input->post('pad_landmark');
    $padData['pad_alt_phone']     = $this->input->post('pad_alt_phone');
    $padData['pad_status']        = ACTIVE_STATUS;

    if ($pad_id != '') {
      $padData['pad_updt_by']       = $this->input->post('pad_prs_id');
      $padData['pad_updt_dt']       = date('Y-m-d H:i:s');
      $pad = $this->home_model->update('pad_id', $pad_id, $padData, 'person_addresses');
    } else {
      $padData['pad_updt_by']       = $this->input->post('pad_prs_id');
      $padData['pad_crtd_by']       = $this->input->post('pad_prs_id');
      $padData['pad_updt_dt']       = date('Y-m-d H:i:s');
      $padData['pad_crtd_dt']       = date('Y-m-d H:i:s');
      $pad = $this->home_model->insert('person_addresses', $padData);
      if ($this->input->post('selected_address') == 0) {
        $defaultAddress = array();
        $defaultAddress['prs_default_address'] = $pad;
        $this->home_model->update('prs_id', $this->input->post('pad_prs_id'), $defaultAddress, 'person');
      }
    }
    return $pad;
  }
  public function getPersonAddressById($pad_id)
  {
    $sql = "SELECT *,(SELECT gnp_name from gen_prm where gnp_status=" . ACTIVE_STATUS . " and gnp_group='address_type' and gnp_value=pad_address_type) address_type,
     (SELECT stm_name from state_master where stm_id=pad_state) state_name FROM `person_addresses` where pad_id='" . $pad_id . "' ";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function getUserData($prs_id)
  {
    $sql = "SELECT * FROM `person` where prs_id='" . $prs_id . "'";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function updatePerson()
  {
    $prs_id                     = $this->input->post('prs_id');
    if (!empty($prs_id)) {
      $prsData = array();
      $prs_old_mob              = $this->input->post('prs_old_mob');
      $prs_mob                  = $this->input->post('prs_mob');
      $prsData['prs_name']      = $this->input->post('prs_name');
    //   $prsData['prs_bio']       = $this->input->post('prs_bio');
    //   $prsData['prs_gender']    = $this->input->post('prs_gender');
    //   $prsData['prs_dob']       = $this->home_model->dateinmysql($this->input->post('prs_dob'));
      $prsData['prs_whatsapp']  = $this->input->post('prs_whatsapp');
      $prsData['prs_gst']       = $this->input->post('prs_gst');
      $prsData['prs_address']   = $this->input->post('prs_address');
      $prsData['prs_city']      = $this->input->post('prs_city');
      $prsData['prs_pincode']   = $this->input->post('prs_pincode');
      $prsData['prs_dist']      = $this->input->post('prs_dist');
      $prsData['prs_state']     = $this->input->post('prs_state');
      $prsData['prs_country']   = $this->input->post('prs_country');
      $prsData['prs_updt_by']   = $prs_id;
      $prsData['prs_updt_dt']   = date('Y-m-d H:i:s');

      if ($prs_old_mob != $prs_mob) {
        $prsData['prs_mob_verification'] = OTP_VERIFICATION_PENDING;
      }
      $update_result = $this->home_model->update('prs_id', $prs_id, $prsData, 'person');
      return true;
    } else {
      return false;
    }
  }
  public function getUserDataByMob($prs_mob)
  {
    $sql = "SELECT * FROM `person` where prs_mob='" . $prs_mob . "'";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function getUserDataById($prs_id)
  {
    $sql = "SELECT *,
    (SELECT stm_name from state_master where stm_id=prs_state) state_name,
    (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person.prs_gender and gen_prm.gnp_group='gender') gender
    FROM `person` WHERE `prs_id`= " . $prs_id . "";
    $query = $this->db->query($sql);
    return $query->row();
  }
  public function updateMobNoOfUser($prs_id, $prs_mob = '')
  {
    $prsData = array();
    $prsData['prs_mob']  = $this->input->post('prs_mob');
    if ($prs_mob != '') {
      $prs_mob  = $this->input->post('prs_mob');
    }
    $prsData['prs_mob_verification']   = OTP_VERIFICATION_PENDING;
    $update_result = $this->home_model->update('prs_id', $prs_id, $prsData, 'person');
    return true;
  }
  public function checkPersonPassword($data)
  {
    $sqlQuery = "SELECT COUNT(*) as count,prs_id FROM `person` where prs_status='" . ACTIVE_STATUS . "' and `prs_password`='" . $data['old_password'] . "' ";
    if ($data['prs_id'] != '') {
      $sqlQuery .= " AND prs_id ='" . $data['prs_id'] . "'";
    }
    log_message('error', ' sqlQuery : ' . $sqlQuery);
    $query = $this->db->query($sqlQuery);
    $user_data = $query->row();
    log_message('error', ' row : ' . json_encode($user_data));
    log_message('error', 'person_model::checkPersonContact <<');
    return $user_data;
  }
}
