<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }


  function save_upload($data)
  {
    $data = array(
      'rcpt_number' => $data['rcpt_number'],
      'pay_type' => $data['pay_type'],
      'pay_mode' => $data['pay_mode'],
      'pay_amt' => $data['pay_amt'],
      'pay_remarks' => $data['pay_remarks'],
      'bank_name' => $data['bank_name'],
      'rcpt_img' => $data['rcpt_img'],
      'status' => RECEIPT_STATUS_PENDING,
      'upload_by' => $data['upload_by'],
      'upload_date' => $data['upload_date'],
      'update_on' => $data['update_on'],
      'updated_by' => $data['upload_by']
    );
    $result = $this->db->insert('payment_rcpt', $data);
    return $result;
  }

  function sendMailForReceiptUploadSuccess($receiptData)
  {
    if ($receiptData != '') {
      $receiptData = json_decode(json_encode($receiptData), FALSE);

      $this->load->model('person_model'); // load model      
      $personData = $this->person_model->getUserData($receiptData->upload_by);
      if ($personData != '') {
        $this->load->model('content_model'); // load model
        $this->load->model('communication_model'); // load model
       
        $mailData =  $this->content_model->getContent('user_upload_receipt', $personData, $receiptData);
        $this->home_model->sendMail($personData->prs_email, $mailData);
        
        $adminsEmailList = $this->communication_model->getAdminsEmailList();
        $emailTo = "";
        foreach ($adminsEmailList as $email) {
          $emailTo .= $email . ",";
        }
        $contentDataForAdminMail = array(
          'receipt_number' => $receiptData->rcpt_number,
          'customer_name' => $personData->prs_name
        );
        $contentDataForAdminMail = json_decode(json_encode($contentDataForAdminMail), FALSE);
        $mailData = $this->content_model->getContent('admin_upload_receipt', '', $contentDataForAdminMail);
        $this->home_model->sendMail($emailTo, $mailData);
      }
    }
  }
}
