<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class communication_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function communication($event_name, $receiver_data, $mail_data)
	{
		require_once(APPPATH . "/config/mail_template.php");
		log_message('error', '>> communication');
		$event_id =  $this->getEventID($event_name);
		if (!empty($event_id)) {
			$event_usr =  $this->getEventUsr($event_id->ent_id);
			foreach ($event_usr as $key) {


				if ($key->eus_type == EUS_TYPE_EMAIL) {

					$email_notifiy = $this->CheckSendNotify('email');
					if (!empty($email_notifiy)) {
						if ($key->eus_usr_type == EUS_USR_TYPE_ADMIN) {
							$email_id = ADMIN_EMAIL_VALUE;

							log_message('error', '>> communication admin email id = ' . $email_id);
						} else {
							$email_id = $receiver_data['email'];

							log_message('error', '>> communication user email id = ' . $email_id);
						}
						$sql = "SELECT tmp_name,tmp_sub,tmp_msg
					FROM `com_template` WHERE `tmp_id`=" . $key->eus_tmp_id . "";
						$query = $this->db->query($sql);
						$data =  $query->result();
						$rowtemp = $query->row();
						$msg = $this->getMailMsg($rowtemp->tmp_name, $receiver_data, $mail_data);

						$this->sendMail($receiver_data['email'], $msg['subject'], $msg['body']);
					}
				} else if ($key->eus_type == EUS_TYPE_SMS) {
					$sms_notifiy = $this->CheckSendNotify('sms');
					if (!empty($sms_notifiy)) {
						log_message('error', 'inside sms');
						if ($key->eus_usr_type == EUS_USR_TYPE_ADMIN) {
							$mobile_no = ADMIN_SMS_VALUE;
							log_message('error', '  admin mobile_no = ' . $mobile_no);
						} else {
							$mobile_no = $receiver_data['mobile'];
							log_message('error', ' user mobile_no = ' . $mobile_no);
						}
						$sql = "SELECT  tmp_name,tmp_sub,tmp_msg
			        FROM `com_template` WHERE `tmp_id`=" . $key->eus_tmp_id . "";
						$query = $this->db->query($sql);
						$data =  $query->result();
						$rowtemp = $query->row();
						if ($mobile_no != '') {
							$message = $this->getMsgData($rowtemp->tmp_name, $receiver_data, $mail_data);
							log_message('error', ' mobile_no :' . $mobile_no . 'message = ' . $message['message']);
							$message['message'] = str_replace(' ', '%20', $message['message']);
							$this->SendMessage($message['message'], $mobile_no);
						}
					}
				}
			}
		}
	}

	function getEventID($event_name)
	{
		$sql = "SELECT ent_id FROM `com_events` WHERE `ent_name`='" . $event_name . "' and `ent_status`=" . ACTIVE_STATUS . "";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row;
	}

	function getEventUsr($ent_id)
	{
		$sql = "SELECT eus_id,eus_ent_id,eus_usr_type,eus_tmp_id,
		(Select com_template.tmp_name from com_template where com_template.tmp_id =com_events_usr.eus_tmp_id
	) template_name, eus_type FROM `com_events_usr` WHERE `eus_ent_id`=" . $ent_id . " and eus_status=" . ACTIVE_STATUS . " order by eus_order_by";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getAdminDetails()
	{
		$sql = "SELECT * from user where usr_dpt_id=" . DEPARTMENT_ADMIN . "";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row;
	}
	function getAdminsEmailList()
	{
		$sql = "SELECT `usr_email` FROM `user` WHERE `usr_dpt_id`=" . DEPARTMENT_ADMIN . " AND `user_isMailSend`=".ACTIVE_STATUS."";
		$query = $this->db->query($sql);
		$arrayResult = $query->result_array();
		$result = array_map(function ($value) {
			return $value['usr_email'];
		}, $arrayResult);
		return $result;
	}
	function getAdminMobile()
	{
		$sql = "SELECT usr_mobile from user where usr_dpt_id=" . DEPARTMENT_ADMIN . "";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row->usr_mobile;
	}

	function getMailMsg($name, $receiver_data, $mail_data)
	{
		$order_placed = '';
		$subject = '';
		$body = '';

		// if ($name == 'order_placed_user_mail') {
		// 	$subject = CLIENT_ORDER_PLACED_SUBJECT;
		// 	$subject = str_replace('%ORDER_NO%', $mail_data->ord_reference_no, $subject);
		// 	$body = CLIENT_ORDER_PLACED_BODY;
		// 	$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
		// 	$body = str_replace('%ORDER_NUMBER%', $mail_data->ord_reference_no, $body);
		// 	$body = str_replace('%AMOUNT%', $mail_data->ord_total_amt, $body);
		// 	$body = str_replace('%CUSTOMER_NAME%', $mail_data->pad_name, $body);
		// 	$body = str_replace('%LOCALITY%', $mail_data->pad_locality, $body);
		// 	$body = str_replace('%ADDRESS%', $mail_data->pad_address, $body);
		// 	$body = str_replace('%CITY%', $mail_data->pad_city, $body);
		// 	$body = str_replace('%PINCODE%', $mail_data->pad_pincode, $body);
		// }

		// if ($name == 'product_status_changed_admin_mail') {
		// 	$status_changed_subject = ADMIN_PRODUCT_STATUS_CHANGED_SUBJECT;
		// 	$status_changed_subject = str_replace('%STATUS_NAME%', $mail_data['order_product_status'], $status_changed_subject);
		// 	$status_changed_subject = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $status_changed_subject);
		// 	$status_changed_subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $status_changed_subject);
		// 	$status_changed_body = ADMIN_PRODUCT_STATUS_CHANGED_BODY;
		// 	$status_changed_body = str_replace('%STATUS_NAME%', $mail_data['order_product_status'], $status_changed_body);
		// 	$status_changed_body = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $status_changed_body);
		// 	$status_changed_body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $status_changed_body);
		// 	$status_changed_body = str_replace('%ORDER_LINK%', base_url() . 'orders/' . $mail_data['ord_reference_no'], $status_changed_body);
		// }
		$msg = array("subject" => $subject, "body" => $body);
		return $msg;
	}
	public function sendMail($email_id, $email_sub, $email_msg)
	{

		$email_notifiy = $this->CheckSendNotify('email');
		if (!empty($email_notifiy)) {
			log_message('error', '>> sendMail email id = ' . $email_id);
			log_message('error', '>> sendMail sub = ' . $email_sub);
			log_message('error', '>> sendMail message = ' . $email_msg);
			$list = '' . $email_id . '';
			$config = array(
				'protocol' => EMAIL_PROTOCOL,
				'smtp_host' => EMAIL_HOST,
				'smtp_port' => 587,
				'smtp_user' => EMAIL_USERNAME,
				'smtp_pass' => EMAIL_PASSWORD,
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1',
				'wordwrap' => TRUE,
				'_smtp_auth' => true
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL_USERNAME, PROJECT_NAME);
			$this->email->subject($email_sub);
			$this->email->to($list);
			$this->email->message($email_msg);

			// TEMP::
			if ($this->email->send()) {
				return true;
			} else {
				log_message('error', 'error occured in people insert communication_model/send_mail' . $this->email->print_debugger());
				echo $this->email->print_debugger();
				return false;
			}
		} else {
			return true;
		}
	}
	function getMsgData($name, $receiver_data, $mail_data)
	{
		$status_changed_msg = '';
		// if ($name == 'order_placed_user_mail') {
		// 	$status_changed_msg = CLIENT_ORDER_PLACED_SUBJECT;
		// 	$status_changed_msg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $status_changed_msg);
		// 	$status_changed_msg = str_replace('%DELIVERY_DATE%', '30 Dec', $status_changed_msg);
		// 	$status_changed_msg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $status_changed_msg);
		// 	$status_changed_msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $status_changed_msg);
		// }
		if ($name == 'product_status_changed_admin_msg' && $mail_data['ord_id'] == PRODUCT_OUT_FOR_DELIVERY) {
			$status_changed_msg = ADMIN_PRODUCT_OUT_FOR_DELIVERY_MSG;
			$status_changed_msg = str_replace('%DELIVERY_DATE%', '30 Dec', $status_changed_msg);
			$status_changed_msg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $status_changed_msg);
			$status_changed_msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $status_changed_msg);
		}
		switch ($name) {

			case 'product_status_changed_user_msg':
				$msg = array("message" => $status_changed_msg);
				break;
			case 'product_status_changed_admin_msg':
				$msg = array("message" => $status_changed_msg);
				break;
			default:
				$msg = array("message" => 'Something went wrong');
		}
		return $msg;
	}
	public function SendMessage($message, $mobile_no)
	{  //echo $mobile_no;
		$sms_notifiy = $this->CheckSendNotify('sms');
		if (!empty($sms_notifiy)) {
			log_message('error', '>>mobile no = ' . $mobile_no);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,  "http://api.msg91.com/api/sendhttp.php?");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "?country=91&sender=MSGIND&route=4&mobiles=$mobile_no&authkey=243235A5ebDlzp5bc72911&message=$message");
			$buffer = curl_exec($ch);
			// print_r( $buffer);
		}
		return true;
	}
	function CheckSendNotify($bpm_name)
	{
		$sql = "SELECT * FROM `bsn_prm` WHERE `bpm_name`='" . $bpm_name . "' and `bpm_status`=" . ACTIVE_STATUS . "";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row;
	}
}
