<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model
{
	function __construct()
	{
		require_once(APPPATH . "/config/mail_template.php");
		parent::__construct();
		$this->load->model('Content_model');
	}

	public function getContent($event, $receiver_data, $content_data)
	{
		$mail_subject = '';
		$mail_body = '';
		$msg_body = '';

		if ($event == 'user_order_placed') {
			$mail_subject = USER_ORDER_PLACED_SUBJECT;
			$mail_subject = str_replace('%ORDER_NO%', $content_data->ord_reference_no, $mail_subject);
			$mail_body = USER_ORDER_PLACED_BODY;
			$mail_body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $mail_body);
			$mail_body = str_replace('%ORDER_NUMBER%', $content_data->ord_reference_no, $mail_body);
			$mail_body = str_replace('%AMOUNT%', $content_data->ord_total_amt, $mail_body);
			$mail_body = str_replace('%CUSTOMER_NAME%', $content_data->pad_name, $mail_body);
			$mail_body = str_replace('%LOCALITY%', $content_data->pad_locality, $mail_body);
			$mail_body = str_replace('%ADDRESS%', $content_data->pad_address, $mail_body);
			$mail_body = str_replace('%CITY%', $content_data->pad_city, $mail_body);
			$mail_body = str_replace('%PINCODE%', $content_data->pad_pincode, $mail_body);
			$mail_body = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $mail_body);
			$mail_body = str_replace('%WEBSITE_LOGO_PATH%', WEBSITE_ROOT_PATH . WEBSITE_LOGO_PATH, $mail_body);
			$mail_body = str_replace('%WEBSITE_LINK%', WEBSITE_ROOT_PATH, $mail_body);

			$msg_body = ORDER_PLACED_USER_MSG;
			$msg_body = str_replace('%ORDER_NO%', $content_data->ord_reference_no, $msg_body);
			$msg_body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $msg_body);
		}
		if ($event == 'admin_order_placed') {
			$mail_subject = ADMIN_ORDER_PLACED_SUBJECT;
			$mail_subject = str_replace('%ORDER_NUMBER%', $content_data->ord_reference_no, $mail_subject);
			$mail_body = ADMIN_ORDER_PLACED_BODY;
			$mail_body = str_replace('%ORDER_NUMBER%', $content_data->ord_reference_no, $mail_body);
			$mail_body = str_replace('%ORDER_LINK%', ADMIN_WEBSITE_ROOT_PATH . 'orders/' . $content_data->ord_reference_no, $mail_body);
		}
		if ($event == 'user_upload_receipt') {
			$mail_subject = USER_RECEIPT_UPLOADED_SUBJECT;
			$mail_subject = str_replace('%RECEIPT_NUMBER%', $content_data->rcpt_number, $mail_subject);

			$mail_body = USER_RECEIPT_UPLOADED_BODY;
			$mail_body = str_replace('%WEBSITE_LOGO_PATH%', WEBSITE_ROOT_PATH . WEBSITE_LOGO_PATH, $mail_body);
			$mail_body = str_replace('%RECEIVER_NAME%', $receiver_data->prs_name, $mail_body);
			$mail_body = str_replace('%RECEIPT_NUMBER%', $content_data->rcpt_number, $mail_body);
			$mail_body = str_replace('%AMOUNT%', $content_data->pay_amt, $mail_body);
			$mail_body = str_replace('%PAYMENT_TYPE%', $content_data->pay_type, $mail_body);
			$mail_body = str_replace('%PAYMENT_MODE%', $content_data->pay_mode, $mail_body);
			$mail_body = str_replace('%RECEIPT_STATUS_LINK%', WEBSITE_ROOT_PATH . 'receipt_status', $mail_body);
			$mail_body = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $mail_body);
		}
		if ($event == 'admin_upload_receipt') {
			$mail_subject = ADMIN_RECEIPT_UPLOADED_SUBJECT;
			$mail_subject = str_replace('%RECEIPT_NUMBER%', $content_data->receipt_number, $mail_subject);

			$mail_body = ADMIN_RECEIPT_UPLOADED_BODY;
			$mail_body = str_replace('%RECEIPT_NUMBER%', $content_data->receipt_number, $mail_body);
			$mail_body = str_replace('%CUSTOMER_NAME%', $content_data->customer_name, $mail_body);
			$mail_body = str_replace('%PAYMENT_RECEIPT_LINK%', ADMIN_WEBSITE_ROOT_PATH . 'payment', $mail_body);
		}
		if ($event == 'user_reset_password') {
			$mail_subject = USER_PASSWORD_RESET_SUBJECT;

			$mail_body = USER_PASSWORD_RESET_BODY;
			$mail_body = str_replace('%WEBSITE_LOGO_PATH%', WEBSITE_ROOT_PATH . WEBSITE_LOGO_PATH, $mail_body);
			$mail_body = str_replace('%PASSWORD_RESET_LINK%', $content_data->link, $mail_body);
			$mail_body = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $mail_body);
		}
		if ($event == 'admin_complaint_received') {
			$mail_subject = ADMIN_COMPLAINT_RECEIVED_SUBJECT;

			$mail_body = ADMIN_COMPLAINT_RECEIVED_BODY;
			$mail_body = str_replace('%CUSTOMER_NAME%', $content_data->customer_name, $mail_body);
			$mail_body = str_replace('%CUSTOMER_FULLNAME%', $content_data->customer_name, $mail_body);
			$mail_body = str_replace('%ORDER_ID%', $content_data->order_number, $mail_body);
			$mail_body = str_replace('%COMPLAINT_DESC%', $content_data->complaint_desc, $mail_body);
			$mail_body = str_replace('%COMPLAINT_DATE%', date("d-M-Y", strtotime($content_data->complaint_date)), $mail_body);
		}
		if ($event == 'admin_quotation_request') {
			$mail_subject = ADMIN_QUOTATION_REQUEST_SUBJECT;
			$mail_subject = str_replace('%QUOTATION_ID%', $content_data->qr_reference_no, $mail_subject);

			$mail_body = ADMIN_QUOTATION_REQUEST_BODY;
			$mail_body = str_replace('%CUSTOMER_NAME%', $content_data->customer_name, $mail_body);
			$mail_body = str_replace('%QUOTATION_ID%', $content_data->qr_reference_no, $mail_body);
			$mail_body = str_replace('%PRODUCT_NAME%', $content_data->qr_product_name, $mail_body);
			$mail_body = str_replace('%PAPER_GSM_SIZE%', $content_data->qr_paper_gsm_size, $mail_body);
			$mail_body = str_replace('%CREASING%', $content_data->qr_creasing, $mail_body);
			$mail_body = str_replace('%HALF_CUT%', $content_data->qr_half_cut, $mail_body);
			$mail_body = str_replace('%UV%', $content_data->qr_uv, $mail_body);
			$mail_body = str_replace('%LAMINATION_TYPE%', $content_data->qr_lamination_type, $mail_body);
			$mail_body = str_replace('%LAMINATION%', $content_data->qr_lamination, $mail_body);
			$mail_body = str_replace('%STAFF_DESCRIPTION%', $content_data->qr_remarks, $mail_body);
			$mail_body = str_replace('%QUANTITY%', $content_data->qr_qty, $mail_body);
			$mail_body = str_replace('%RECEIVE_DATE%', date("d-M-Y", strtotime($content_data->qr_created_on)), $mail_body);
		}
		if ($event == 'user_login_otp') {
			$mail_subject = USER_LOGIN_OTP_SUBJECT;
			$mail_subject = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $mail_subject);

			$mail_body = USER_LOGIN_OTP_BODY;
			$mail_body = str_replace('%WEBSITE_LOGO_PATH%', WEBSITE_ROOT_PATH . WEBSITE_LOGO_PATH, $mail_body);
			$mail_body = str_replace('%LOGIN_OTP%', $content_data['otp_code'], $mail_body);
			$mail_body = str_replace('%LOGIN_OTP%', $content_data['otp_code'], $mail_body);
			$mail_body = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $mail_body);
		}
		$msg = array("mail_subject" => $mail_subject, "mail_body" => $mail_body, "msg_body" => $msg_body);
		return $msg;
	}
}
