<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function checkLogin($arrData = array())
  {
    $user_data = '';
    $pswd = '';
    if (isset($arrData['usr_username']) and !empty($arrData['usr_username'])) {
      switch ($arrData['usr_username']) {
        case is_numeric($arrData['usr_username']):
          $condition = ' prs_mob = "' . $arrData['usr_username'] . '" ';
          break;
        case filter_var($arrData['usr_username'], FILTER_VALIDATE_EMAIL):
          $condition = ' prs_email = "' . $arrData['usr_username'] . '" ';
          break;
        default:
          $condition = ' prs_username = "' . $arrData['usr_username'] . '" ';
          break;
      }
      if (isset($arrData['usr_password']) and !empty($arrData['usr_password'])) {
        $pswd = ' AND prs_password ="' . $arrData['usr_password'] . '" ';
      }
      $sqlQuery = 'SELECT * FROM person WHERE ' . $condition . '  ' . $pswd . ' AND prs_status = "' . ACTIVE_STATUS . '"';
      $query = $this->db->query($sqlQuery);
      $user_data = $query->row();
    }
    return $user_data;
  }
  public function checkPersonContact($prs_type, $prs_value, $prs_id = '')
  {
    log_message('error', 'person_model::checkPersonContact >>');

    $sqlQuery = "SELECT COUNT(*) as count,prs_id,prs_mob FROM person where prs_status='" . ACTIVE_STATUS . "' and " . $prs_type . "='" . $prs_value . "' ";
    if ($prs_id != '') {
      $sqlQuery .= " and prs_id !='" . $prs_id . "'";
    }
    log_message('error', ' sqlQuery : ' . $sqlQuery);
    $query = $this->db->query($sqlQuery);
    $user_data = $query->row();
    log_message('error', ' row : ' . json_encode($user_data));
    log_message('error', 'person_model::checkPersonContact <<');
    return $user_data;
  }
  public function registerCustomer()
  {
    $prsData = array();
    $prsData['prs_name']             = $this->input->post('prs_name');
    $prsData['prs_email']            = $this->input->post('prs_email');
    $prsData['prs_username']         = $this->input->post('prs_email');
    $prsData['prs_mob']              = $this->input->post('prs_mob');
    $prsData['comp_name']            = $this->input->post('comp_name');
    $prsData['cont_name']            = $this->input->post('cont_name');
    $prsData['prs_country']          = $this->input->post('prs_country');
    $prsData['prs_state']            = $this->input->post('prs_state');
    $prsData['prs_dist']             = $this->input->post('prs_dist');
    $prsData['prs_city']             = $this->input->post('prs_city');
    $prsData['prs_pincode']          = $this->input->post('prs_pincode');
    $prsData['prs_address']          = $this->input->post('prs_address');
    $prsData['prs_landmark']         = $this->input->post('prs_landmark');
    $prsData['prs_whatsapp']         = $this->input->post('prs_whatsapp');
    $prsData['prs_gst']              = $this->input->post('prs_gst');
    $prsData['prs_password']         = $this->url_encrypt->encrypt_openssl($this->input->post('prs_password'));
    $prsData['prs_status']           = ACTIVE_STATUS;
    $prsData['prs_mob_verification'] = OTP_VERIFICATION_PENDING;
    $prsData['prs_crtd_dt']          = date('Y-m-d H:i:s');
    $prsData['prs_updt_dt']          = date('Y-m-d H:i:s');
    $prs_id = $this->home_model->insert('person', $prsData);
    if (!empty($prs_id) && $prs_id != 0) {
      /* Insert Initial wallet amount */
      $walletData = array();
      $walletData['prs_id']     = $prs_id;
      $walletData['amount']     = WALLET_INIT_AMOUNT;
      $walletData['status']     = WALLET_STATUS_TRUE;
      $walletData['active']     = WALLET_ACTIVE_TRUE;
      $walletData['created_on'] = date('Y-m-d H:i:s');
      $walletData['updated_on'] = date('Y-m-d H:i:s');
      $wallet_id = $this->home_model->insert('wallet', $walletData);
      if (!empty($wallet_id) && $wallet_id != 0) {
        /* Insert Initial wallet history amount */
        $walletHistoryData = array();
        $walletHistoryData['prs_id']           = $prs_id;
        $walletHistoryData['wallet_id']        = $wallet_id;
        $walletHistoryData['type']             = CREDIT;
        $walletHistoryData['transaction_mode'] = PAY_FROM_BALANCE;
        $walletHistoryData['amount']           = WALLET_INIT_AMOUNT;
        $walletHistoryData['closing_balance']  = WALLET_INIT_AMOUNT;
        $walletHistoryData['description']      = "Initial Balance";
        $walletHistoryData['created_on']       = date('Y-m-d H:i:s');
        $wallet_id = $this->home_model->insert('wallet_history', $walletHistoryData);
      }
      return $prs_id;
    } else {
      return false;
    }
  }
  public function sendMailForPasswordReset($person_data)
  {
    $resetPwdData = array();
    $resetPwdData['people'] = $person_data;
    $resetPwdData['email']  = $person_data->prs_email;
    $resetPwdData['link']   = $person_data->link;

    $this->load->model('content_model'); // load model
    $mailData = $this->content_model->getContent('user_reset_password', '', json_decode(json_encode($resetPwdData), FALSE));
    $isMailSend = $this->home_model->sendMail($person_data->prs_email, $mailData);
    return $isMailSend == true ? $isMailSend : false;
  }
  public function getForgotpswdTransaction($fpt_prs_id, $fpt_code)
  {
    $sql = "SELECT * FROM `forgot_password_transaction` WHERE  fpt_prs_id= '" . $fpt_prs_id . "' and fpt_code = '" . $fpt_code . "' and fpt_status='" . ACTIVE_STATUS . "' and  NOW() <= DATE_ADD(fpt_crtd_dt, INTERVAL " . FPT_LINK_VALIDITY_TIME . ") ORDER BY fpt_id DESC LIMIT 1";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function checkOtp($otp_code)
  {
    $checkSql = "SELECT otp_prs_id,count(*) res_count,otp_mob from otp_transaction where otp_code='" . $otp_code . "' and otp_status='" . ACTIVE_STATUS . "' ORDER BY otp_crtd_dt DESC LIMIT 1";
    $query = $this->db->query($checkSql);
    $row = $query->row();
    return $row;
  }
  public function discard_otp($otp_prs_mob)
  {
    $checkSql = "UPDATE otp_transaction set otp_status='" . OTP_VERIFICATION_DISCARD . "' where otp_mob='" . $otp_prs_mob . "' ";
    $query = $this->db->query($checkSql);
    return true;
  }

  public function discard_otp_by_email($otp_prs_email)
  {
    $checkSql = "UPDATE `otp_transaction` SET `otp_status`='" . OTP_VERIFICATION_DISCARD . "' WHERE `otp_email`='" . $otp_prs_email . "' ";
    $this->db->query($checkSql);
    return true;
  }
  /* Old Code */
  // public function generateOtp($prs_id, $prs_mob)
  // {
  //   $discard_otp = $this->login_model->discard_otp($prs_mob);
  //   $otp_code = $this->home_model->generateRandomStringNum(4);
  //   $otp_data = array();
  //   $otp_data['otp_prs_id']  = $prs_id;
  //   $otp_data['otp_mob']     = $prs_mob;
  //   $otp_data['otp_code']    = $otp_code;
  //   $otp_data['otp_status']  = ACTIVE_STATUS;
  //   $otp_data['otp_crtd_dt'] = date('Y-m-d H:i:s');
  //   $otp_id = $this->home_model->insert('otp_transaction', $otp_data);
  //   //send OTP on SMS
  //   $otp_msg = "Hello from Prism Prints ! Your One Time Password for account verification is " . $otp_code . ". Please keep it confidential and do not share it with anyone.";
  //   $otp_send = $this->home_model->SendMessage($otp_msg, $otp_data['otp_mob']);
  //   return $otp_id;
  // }
  public function generateOtp($prs_id, $prs_email)
  {
    $this->login_model->discard_otp_by_email($prs_email);
    $otp_code = $this->home_model->generateRandomStringNum(4);
    $otp_data = array();
    $otp_data['otp_prs_id']  = $prs_id;
    $otp_data['otp_mob']     = 0;
    $otp_data['otp_email']   = $prs_email;
    $otp_data['otp_code']    = $otp_code;
    $otp_data['otp_status']  = ACTIVE_STATUS;
    $otp_data['otp_crtd_dt'] = date('Y-m-d H:i:s');
    $otp_id = $this->home_model->insert('otp_transaction', $otp_data);

    // Send OTP on EmailId
    $this->load->model('content_model');
    $mailData = $this->content_model->getContent('user_login_otp', '', $otp_data);
    // $this->home_model->sendMail($otp_data['otp_email'], $mailData);
    return $otp_id;
  }
}
