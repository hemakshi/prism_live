<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function setconsole($label = 'var', $x)
  {
?>
    <script type='text/javascript'>
      console.log('<?php echo ($label) ?>');
      console.log('<?php echo json_encode($x) ?>');
    </script>
<?php
  }

  public function priceList()
  {
    $sql = "SELECT prd_name, prd_price,
    (SELECT `cat_name` FROM `product_category` WHERE  cat_id = products.prd_cat_id) category_name
    FROM `products` ORDER BY category_name";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getProductCategories($menu = false)
  {
    $sql = "SELECT * FROM `product_category` where `cat_status`=" . ACTIVE_STATUS . "";
    if ($menu) {
      $sql .= " and  cat_menu=" . $menu . " ";
    }
    $sql .= " order by `cat_order`";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getProductByCategory($cat_slug)
  {
    $sql = "SELECT * FROM `products` left join product_category on  products.prd_cat_id = product_category.cat_id  where cat_slug = '" . $cat_slug . "'";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getCatDataBySlug($cat_slug)
  {
    $sql = "SELECT * FROM `product_category` where cat_slug='" . $cat_slug . "'";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function getFeaturedProducts()
  {
    $sql = "SELECT *,
 (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name
 -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_quantity_unit and gen_prm.gnp_group='prd_quantity_unit') prd_quantity_name,
-- (select  product_flavor_images.pfi_img from product_flavor_images where product_flavor_images.pfi_prd_id=products.prd_id  limit 1) as img_path 
FROM `products` where prd_status=" . ACTIVE_STATUS . " and prd_id in (select fp_prd_id from featured_products ) order by prd_order";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getAllProducts($menu = false)
  {
    //     if ($menu == SUPPLEMENTS_MENU) {
    //       $sql = "SELECT *,
    //    (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name,
    //   -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_quantity_unit and gen_prm.gnp_group='prd_quantity_unit') prd_quantity_name,
    //   -- (SELECT GROUP_CONCAT(flv_name) FROM product_flavours where flv_id in (products.prd_flv_id)) prd_flavours,
    //   -- (select  product_flavor_images.pfi_img from product_flavor_images where product_flavor_images.pfi_prd_id=products.prd_id  limit 1) as img_path,
    //  (select fp_status from  featured_products where featured_products.fp_prd_id=products.prd_id) featured_product FROM `products` where prd_status=" . ACTIVE_STATUS . " and prd_menu=" . $menu . " order by prd_order ";
    //     }
    //     if ($menu == SHOP_MENU) {
    //       $sql = "SELECT *,
    //    (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name,
    //   -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_quantity_unit and gen_prm.gnp_group='prd_quantity_unit') prd_quantity_name,
    //   -- (SELECT GROUP_CONCAT(flv_name) FROM product_flavours where flv_id in (products.prd_flv_id)) prd_flavours,
    //   -- (select  product_flavor_images.pfi_img from product_flavor_images where product_flavor_images.pfi_prd_id=products.prd_id  limit 1) as img_path,
    //  (select fp_status from  featured_products where featured_products.fp_prd_id=products.prd_id) featured_product FROM `products` where prd_status=" . ACTIVE_STATUS . " and prd_menu=" . $menu . " order by prd_order ";
    //     }
    //     $query = $this->db->query($sql);
    //     $result = $query->result();
    //     return $result;
  }
  public function getProductsByCat($cat, $id)
  {
    $sql = "SELECT * from products where prd_cat_id = " . $cat . " and prd_id != " . $id . "  and prd_status=" . ACTIVE_STATUS . " order by prd_order";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function getProductList($prdCategory = '')
  {
    $prdCatCondition = '';
    if ($prdCategory != '') {
      $prdCatCondition = ' and prd_cat_id IN (SELECT cat_id from product_category where cat_status="' . ACTIVE_STATUS . '" and cat_slug="' . $prdCategory . '")';
    }
    $sql = "SELECT *,
   (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name,
   -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_quantity_unit and gen_prm.gnp_group='prd_quantity_unit') prd_quantity_name,
   -- (SELECT GROUP_CONCAT(flv_name) FROM product_flavours where flv_id in (products.prd_flv_id)) prd_flavours,
(select  images.img_path from images where images. img_type_id=products.prd_id and  img_type=" . PRODUCT_IMG_TYPE . "  limit 1) as img_path,
   
   (select fp_status from  featured_products where featured_products.fp_prd_id=products.prd_id) featured_product FROM `products` where prd_status=" . ACTIVE_STATUS . " " . $prdCatCondition . " order by prd_order";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getProductDataBySlug($prd_slug)
  {
    $sql = "SELECT *, (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name,
   (select product_category.cat_slug from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_slug,
(select  images.img_path from images where images. img_type_id=products.prd_id and  img_type=" .
      PRODUCT_IMG_TYPE . "  limit 1) as img_path,
  FROM `products` where prd_slug='" . $prd_slug . "'";
    $query = $this->db->query($sql);
    return $query->row();
  }

  public function getProductImagesBySlug($prd_slug)
  {
    $sql = "SELECT`img_id`, `img_type`, `img_type_id`, `img_title`, `img_path`, `img_name`, `img_alternate_text` from  images where img_type=" . PRODUCT_IMG_TYPE . " and img_type_id=(select prd_id from products where prd_slug='" . $prd_slug . "')";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public function getProductFlavours($flavours)
  {
    $sql = "SELECT   `flv_id`, `flv_name`, flv_slug,`flv_order` FROM `product_flavours` where flv_id in (" . $flavours . ")";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getCategoryDetailBySLug($catSlug)
  {
    $sql = "SELECT * FROM `product_category` where cat_status=" . ACTIVE_STATUS . " and cat_slug='" . $catSlug . "' order by cat_order";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function getDefaultFlavourData($pfi_prd_id, $flavour = false)
  {
    if ($flavour) {
      $sql = "SELECT * FROM `product_flavor_images` where pfi_prd_id=" . $pfi_prd_id . " and  pfi_flv_id = (select flv_id from product_flavours where flv_slug = '" . $flavour . "')";
    } else {
      $sql = "SELECT * FROM `product_flavor_images` where pfi_prd_id=" . $pfi_prd_id . " limit 1";
    }
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
  public function getProductFlavourData($pfi_flv_id)
  {
    $sql = "SELECT * FROM `product_flavor_images` where pfi_flv_id=" . $pfi_flv_id . "";
    $query = $this->db->query($sql);
    return $query->row();
  }
  public function getProductFlavoursById($prd_id, $flavour)
  {

    $sql = "SELECT   `flv_id`, `flv_name`, `flv_order` FROM `product_flavours` where FIND_IN_SET(flv_id,(select products.prd_flv_id from products where prd_id='" . $prd_id . "')) >0  order by flv_order";
    $query = $this->db->query($sql);
    $str = '';
    foreach ($query->result() as $row) {
      $selected = '';
      if ($row->flv_id != $flavour) {

        $str .= '<button  onclick="valueChanged(this)" data-id="' . $row->flv_id . '" data-name="' . $row->flv_name . '" type="button" class="size-btn-group size-btn selected"><span class="size-btn-group-inner size-btn-group size-text " >' . $row->flv_name . '</span</button>';
      } else {
        $str .= '<button type="button" class="size-btn-group size-btn selected"><span class="size-btn-group-inner size-btn-group size-text ">' . $row->flv_name . '</span><span class="tick"> <i class="fa fa-check" aria-hidden="true"></i></span></button>';
      }
    }
    return $str;
  }
  public function getQuantity($qty)
  {
    $this->setconsole("getQuantity", $qty);
    $sql = "SELECT * FROM `gen_prm` WHERE `gnp_group` = 'cart_qty' and `gnp_status`=1 ORDER by `gnp_order`";
    $query = $this->db->query($sql);
    $str = '';
    foreach ($query->result() as $row) {
      $selected = '';
      if ($row->gnp_value != $qty) {

        $str .= '<button onclick="valueChanged(this)"  data-qty="' . $row->gnp_value . '"  type="button" class="size-btn-group size-btn selected"><span class="size-btn-group-inner size-btn-group size-text ">' . $row->gnp_name . '</span</button>';
      } else {
        $str .= '<button type="button" class="size-btn-group size-btn selected"><span class="size-btn-group-inner size-btn-group size-text ">' . $row->gnp_name . '</span><span class="tick"> <i class="fa fa-check" aria-hidden="true"></i></span></button>';
      }
    }
    return $str;
  }
}
