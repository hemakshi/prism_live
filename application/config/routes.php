<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = 'My404';
$route['about'] = 'Home/about';
$route['select_view'] = 'pro/index';
$route['contact'] = 'Home/contact';
$route['product-detail/(:any)'] = 'product/productDetail/$1';
$route['product-detail/(:any)/(:any)'] = 'product/productDetail/$1/$2';
$route['products/(:any)'] = 'product/getProducts/$1';
$route['shop/(:any)'] = 'shop/getShopProducts/$1';
$route['login'] = 'login';
$route['logout'] = 'login/logout';
$route['profile'] = 'Home/dashboard';
$route['profile/edit'] = 'Home/editProfile';
$route['call_us'] = 'Home/callUs';
$route['cart'] = 'product/cart';
$route['privacy-policy'] = 'Home/privacyPolicy';
$route['terms-and-conditions'] = 'Home/terms';
$route['return-policy'] = 'Home/returnPolicy';
$route['upload_file'] = 'Home/uploadFile';
$route['upload_receipt'] = 'Upload';
$route['receipt_status'] = 'Home/receiptStatus';
$route['ledger'] = 'Home/ledger';
$route['price_list'] = 'product/getPricelist';
$route['add_balance'] = 'paytm/pay';
$route['redirect_paytm'] = 'paytm/redirect';
$route['order_history'] = 'order/orderHistory';
$route['change_password'] = 'Home/changePassword';
$route['my-complaints'] = 'Home/complaintsList';
$route['register-complaint'] = 'Home/complaintRegisterView';
$route['my-quotations'] = 'Home/quotationsList';
$route['quotation-request'] = 'Home/quotationRequestView';
$route['quotation-detail/(:any)'] = 'Home/quotationDetailView/$1';

//Orders
$route['address-select'] = 'order/address_select';
$route['order-review'] = 'order/order_review';
$route['order-detail/(:any)'] = 'order/orderDetail/$1';
$route['order-summary-(:any)'] = 'order/order_summary/$1';
$route['invoice/(:any)'] = 'order/generateCustomerInvoice/$1';

$route['reset-password-(:any)']          = 'login/person_reset_password/$1';
$route['account-verification-(:any)-(:any)']     = 'login/acc_verification/$1/$2';
$route['my-orders']     = 'order/order_list';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
