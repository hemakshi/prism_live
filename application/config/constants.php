<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('REMOTE_ADDR', 'remote_addr');
define('PROJECT_COOKIE_NAME',     'prism');
define('PROJECT_COOKIE_VERSION', 'v9');
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('INACTIVE_STATUS', 2);
define('ACTIVE_STATUS', 1);
define('PRODUCT_IMG_TYPE', '1');
define('FEATURED_PRODUCT_LIMIT', '5');
define('CHILD_FOLDER', 'app/');
define('PRODUCT_BIG_IMAGE_PATH', 'public/images/products/big/');
define('PRODUCT_SMALL_IMAGE_PATH', 'public/images/products/small/');
define('PRODUCT_LARGE_IMAGE_PATH', 'public/images/products/large/');
define('FLAVOUR_PATH', 'public/images/products/flavours/');
define('GALLERY_SET_BANNER', 1);

define('GALLERY_SET_IMAGE', 'app/public/images/gallery_set/big/');
define('GALLERY_SET_IMAGE_RESIZE', 'app/public/images/gallery_set/small/');

/****************** Project Login ********************/
define('PROJECT_SESSION_ID', 'prism_web_ppl_id');
define('PROJECT_SESSION_NAME', 'prism_web_ppl_name');
define('PROJECT_SESSION_USERNAME', 'prism_web_ppl_username');
define('PROJECT_SESSION_BALANCE', 'prism_web_ppl_balance');
define('PROJECT_SESSION_MOB', 'prism_web_ppl_mob');
define('PROJECT_SESSION_EMAIL', 'prism_web_ppl_email');
define('PROJECT_SESSION_STATUS', 'prism_web_ppl_status');
define('PROJECT_COOKIE_USERNAME', 'prism_web_cookie_ppl_username');
define('PROJECT_COOKIE_PASSWORD', 'prism_web_cookie_ppl_password');
define('TRANSACTION_LOGIN', 1);
define('TRANSACTION_LOGOUT', 2);
/****************** Project Login ********************/
/*******************ENCRYPTION KEY*********************/
define('KEY', 'TSN');
define('CIPHER', 'AES-128-ECB');
/*******************ENCRYPTION KEY*********************/

/*********** Bank Details ***********/
define('BANK_NAME', 'Bank of Baroda');
define('BANK_BRANCH', 'University Road Rajkot');
define('BANK_IFSC_CODE', 'BARB0UNIRAJ');
define('BANK_ACCOUNT_NUMBER', '36720200000422');
/*********** Bank Details ***********/

/* Project Details Start */
define('PROJECT_NAME', 'Prism Prints');
define('WEBSITE_ROOT_PATH', 'https://localhost/prism_live/');
define('ADMIN_WEBSITE_ROOT_PATH', 'https://localhost/prism_live/app/');
// define('WEBSITE_ROOT_PATH', 'https://prismprints.in/');
// define('ADMIN_WEBSITE_ROOT_PATH', 'https://prismprints.in/app/');
define('WEBSITE_LOGO_PATH', 'assets/img/logo/logo.png');
define('INSTAGRAM_ID_LINK', 'https://www.instagram.com/prismprints/');

define('COMPANY_ADDRESS_CUSTOMER', '13, Vijay Plot Main Road,<br> Rajkot.- 360002.');
define('EMAIL_ID_INFO','info@prismprints.in');
define('EMAIL_ID_ADMIN','prismprintsonline@gmail.com');
define('EMAIL_ID_QUOTATION','quotation.prismprints@gmail.com');
/* Project Details End */

define('DEPARTMENT_ADMIN', 1);
define('DEPARTMENT_CUSTOMER', 2);

define('SEND_MAIL_USING_SMTP',0);
define('PEOPLE_MAIL_HTML_PATH', 'email_template/');


define('FPT_LINK_VALIDITY_TIME', '24 HOUR');

define('OTP_VERIFICATION_PENDING', '0');
define('OTP_VERIFICATION_DONE', '1');
define('OTP_VERIFICATION_DISCARD', '2');

// Order Constants Start
define('ORDER_SOURCE_WEBSITE', 1);
define('ORDER_PAYMENT_MODE_PAYTM', 2);
define('ORDER_PAYMENT_MODE_PAY_FROM_BALANCE', 3);
define('ORDER_DELIVER_CHARGE', '0');

define('ORDER_IS_SUCCESS_PROCESSING', 0);
define('ORDER_IS_SUCCESS_SUCCESS', 1);
define('ORDER_IS_SUCCESS_FAILED', 2);

define('ORDER_STATUS_PLACED', 1);
define('ORDER_STATUS_CANCELLED', 2);
define('ORDER_STATUS_DISPATCHED', 3);
define('ORDER_STATUS_DELIVERED', 4);

// Order Constants End

define('PAYMENT_TYPE_COD', '1');
define('PAYMENT_TYPE_PAYTM', '2');
define('PAYMENT_TRANSACTION_CHARGE', '3'); // In Percentage
define('PAYMENT_PAY_STATUS_FAILED', '0');
define('PAYMENT_PAY_STATUS_SUCCESS', '1');
define('SHIPPING_CHARGES', '0');
define('ORDER_PAYMENT_STATUS_PENDING', '1');
define('ORDER_PAYMENT_STATUS_PAID', '2');

define('SUPPLEMENTS_MENU', '1');
define('SHOP_MENU', '2');
define('INVOICE_NUMBER_STARTS', '10000001');


// Start Products with Height & Width
define('STICKER', 'sticker');
define('BROCHURE_MIX', 'brochure-mix');
// End Products with Height & Width

// Start Order Status
define('INCOMPLETE', 0);
define('COMPLETE', 1);
// End Order Status

// Start Transaction Type
define('CREDIT', 1);
define('DEBIT', 2);
// End Transaction Type

// Start Upload File Path
define('DESIGNFILE_UPLOAD_DIRECTORY', 'assets/designfiles/');
define('RECEIPT_UPLOAD_DIRECTORY', 'assets/payment_receipts/');
define('COMPLAINT_UPLOAD_DIRECTORY', 'assets/complaint_files/');
define('QUOTATION_UPLOAD_DIRECTORY', 'assets/quotation_files/');
// End Upload File Path

define('PLACED', 1);
define('OUT_FOR_DELIVERY', 2);
define('DELIVERED', 3);

/* Product Status Start */
define('ORD_PRODUCT_STATUS_PLACED', 1);
define('ORD_PRODUCT_STATUS_PROCESSING', 2);
define('ORD_PRODUCT_STATUS_FILE_CORRUPTED', 3);
define('ORD_PRODUCT_STATUS_MISMATCH_QUANTITY', 4);
define('ORD_PRODUCT_STATUS_MISMATCH_QUALITY', 5);
define('ORD_PRODUCT_STATUS_IS_IN_PRINTING', 6);
define('ORD_PRODUCT_STATUS_ORDER_IS_READY', 7);
define('ORD_PRODUCT_STATUS_DISPATCH', 8);
define('ORD_PRODUCT_STATUS_DELIVERED', 9);
/* Product Status End */

/* Payment Receipt Status- Start */
define('RECEIPT_STATUS_FAIL', 0);
define('RECEIPT_STATUS_PENDING', 1);
define('RECEIPT_STATUS_APPROVE', 2);
define('RECEIPT_STATUS_REJECT', 3);
/* Payment Receipt Status- End */

/* Wallet History Transaction Mode-Start */
define('WALLET_INIT_AMOUNT', 0.0);
define('WALLET_STATUS_TRUE', 1);
define('WALLET_ACTIVE_TRUE', 1);
define('WALLET_ACTIVE_FALSE', 0);
/* Wallet History Transaction Mode-End */

/* Wallet History Transaction Mode-Start */
define('BANK', 1);
define('CASH', 2);
define('PAY_FROM_BALANCE', 3);
define('PAYMENT_GATEWAY', 4);
define('TRANSACTION_MODE_WALLET', 5);
/* Wallet History Transaction Mode-End */

define('TRANSACTION_TYPE_CREDIT', 1);
define('TRANSACTION_TYPE_DEBIT', 2);