<?php

/**************START COMMUNICATION VALUE **************/
define('ADMIN_EMAIL_VALUE', 'prismprintsonline@gmail.com');
define('ADMIN_SMS_VALUE', '9136126136');
define('EUS_TYPE_EMAIL', 'email');
define('EUS_TYPE_SMS', 'sms');
define('EUS_USR_TYPE_USER', 'user');
define('EUS_USR_TYPE_ADMIN', 'admin');
define('ADMIN_EMAIL', 'admin_email');
define('ADMIN_CONTACT', 'admin_contact');
define('ADMIN_SOURCE_EMAIL', 'admin_source_email');
define('ADMIN_SOURCE_PASSWORD', 'admin_source_pw');
define('EUS_STATUS_ACTIVE', '1');
define('WEBSITE_NAME', 'Prism Prints');

/***************END COMMUNICATION VALUE************************/
define('EMAIL_PROTOCOL', 'smtp');
define('EMAIL_HOST', 'ssl://mail.gmail.com');
define('EMAIL_PORT', 465);
define('EMAIL_USERNAME', 'noreply@prismprints.in');
define('EMAIL_PASSWORD', 'kishan.1111');
define('EMAIL_TYPE', 'html');
define('EMAIL_CHARSET', 'iso-8859-1');
define('EMAIL_WORDWRAP', TRUE);

/***********************************************************
				START EMAIL TEMPLATES
 ************************************************************/

/*************************START USER ORDER PLACED**********/
define('USER_ORDER_PLACED_SUBJECT', 'Your order #%ORDER_NO% has been placed successfully');

define('USER_ORDER_PLACED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order</h3>
            <h3 class='line'>Confirmed</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Thank you for shopping at %WEBSITE_NAME%.Your order <strong>#%ORDER_NUMBER%</strong> has been confirmed.</p>

                <p>Amount: <strong>RS. %AMOUNT%</strong><br> %CUSTOMER_NAME%
                    <br> %ADDRESS%
                    <br> %LOCALITY%
                    <br> %CITY%- %PINCODE%
                </p>
                <p>Please login to our website to check your Order Status and Ledger Details.
                    <br>
                    <a href='%WEBSITE_LINK%' target='_blank'>www.prismprints.in</a>
                </p>
                <br>Thank you, <br> Team %WEBSITE_NAME% <br></p>
            </div>
        </div>
    </div>
</body>
</html>");

/*************************END USER ORDER PLACED**********/

/*************************START ADMIN ORDER PLACED**********/

define('ADMIN_ORDER_PLACED_SUBJECT', 'New order %ORDER_NUMBER% has placed.');
define('ADMIN_ORDER_PLACED_BODY', 'Hi Admin,<br><br>
	New order <strong>#%ORDER_NUMBER%</strong> has been placed.<br>
	Link: <strong><a href="%ORDER_LINK%" target="_blank">View Order</a></strong>');

/*************************END ADMIN ORDER PLACED**********/

/*************************START ADD WALLET BALANCE **********/
define('ADMIN_ADD_BALANCE_SUBJECT', 'Balance Added, Payment Order ID: %PAYMENT_ORDER_ID%- %WEBSITE_NAME%');
define('ADMIN_ADD_BALANCE_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header, .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='info'>
            <h3>Payment</h3>
            <h3 class='line'>Received Rs. %PAYMENT_AMOUNT%</h3>
            <div class='temp'>
                <p>Hey Admin,</p>
                <p>Payment received from <strong>%SENDER_EMAIL%</strong>.</p>

                <h4>Payment Details:</h4>
                <p>Order ID: <strong>%PAYMENT_ORDER_ID%</strong> </p>
                <p>Amount: <strong>RS. %PAYMENT_AMOUNT%</strong> </p>

                <p>
                    <strong><a href='%PAYMENT_DETAIL_LINK%' target='_blank'>Payment Details</a></strong>
                </p>
            </div>
        </div>
    </div>
</body>
</html>");

define('USER_ADD_BALANCE_SUBJECT', 'Payment Receipt# %RECEIPT_NUMBER% has been uploaded successfully');
define('USER_ADD_BALANCE_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }        
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }        
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }        
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }        
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }    
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
          <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Payment Receipt</h3>
            <h3 class='line'>Uploaded</h3>
            <div class='temp'>
              <p>Hey %RECEIVER_NAME%,</p>
              <p>Your payment receipt has been uploaded.</p>

              <h4>Receipt Details:</h4>
              <p>Receipt Number: <strong>%RECEIPT_NUMBER%</strong> </p>
              <p>Paid Amount: <strong>RS. %AMOUNT%</strong> </p>
              <p>Payment Type: <strong>%PAYMENT_TYPE%</strong> </p>
              <p>Payment Mode: <strong>%PAYMENT_MODE%</strong> </p>

              <p>You can view your receipt updates by clicking on below link: <br>
                  <a href='%RECEIPT_STATUS_LINK%' target='_blank'>Receipt Status</a>
              </p>
              <br>Thank you, <br> Team %WEBSITE_NAME% <br>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END ADD WALLET BALANCE **********/

/*************************START PAYMENT RECEIPT UPLOAD **********/

define('ADMIN_RECEIPT_UPLOADED_SUBJECT', 'Requested You for Add Balance- Payment Receipt# %RECEIPT_NUMBER%');
define('ADMIN_RECEIPT_UPLOADED_BODY', 'Hi Admin,<br><br>
	New payment receipt# <strong>%RECEIPT_NUMBER%</strong> has been uploaded by <strong>%CUSTOMER_NAME%</strong> for add wallet balance.<br>
  Link: <strong><a href="%PAYMENT_RECEIPT_LINK%" target="_blank">Payment Receipts List</a></strong>');

define('USER_RECEIPT_UPLOADED_SUBJECT', 'Payment Receipt# %RECEIPT_NUMBER% has been uploaded successfully');
define('USER_RECEIPT_UPLOADED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }        
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }        
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }        
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }        
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }    
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
          <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Payment Receipt</h3>
            <h3 class='line'>Uploaded</h3>
            <div class='temp'>
              <p>Hey %RECEIVER_NAME%,</p>
              <p>Your payment receipt has been uploaded.</p>

              <h4>Receipt Details:</h4>
              <p>Receipt Number: <strong>%RECEIPT_NUMBER%</strong> </p>
              <p>Paid Amount: <strong>RS. %AMOUNT%</strong> </p>
              <p>Payment Type: <strong>%PAYMENT_TYPE%</strong> </p>
              <p>Payment Mode: <strong>%PAYMENT_MODE%</strong> </p>

              <p>You can view your receipt updates by clicking on below link: <br>
                  <a href='%RECEIPT_STATUS_LINK%' target='_blank'>Receipt Status</a>
              </p>
              <br>Thank you, <br> Team %WEBSITE_NAME% <br>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END PAYMENT RECEIPT UPLOAD **********/

/*************************START LOGIN OTP **********/
define('USER_LOGIN_OTP_SUBJECT', '%WEBSITE_NAME% - Login using OTP');
define('USER_LOGIN_OTP_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        p {
            margin: 10px 0;
        }
        .pb-10 {
            padding-bottom: 10px;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <div class='pb-10'>
                <p>Login using OTP: <strong>%LOGIN_OTP%</strong></p>
            </div>
            <div class='temp'>
                <p>Hello,</p>
                <p>
                    Use the OTP <strong>%LOGIN_OTP%</strong> to login.<br> The code is valid for 15 minutes and can be used only once.
                </p>
                <p>
                    <br>See you soon, <br> Team %WEBSITE_NAME% <br>
                </p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END LOGIN OTP **********/

/*************************START RESET PASSWORD**********/
define('USER_PASSWORD_RESET_SUBJECT', 'Reset Your Password');
define('USER_PASSWORD_RESET_BODY', " <!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        p {
            margin: 10px 0;
        }
        .text-center {
            text-align: center;
        }
        .reset-btn {
            background: #28a745;
            text-decoration: none !important;
            font-weight: 500;
            margin: 15px;
            color: #fff !important;
            text-transform: uppercase;
            font-size: 14px;
            padding: 10px 24px;
            display: inline-block;
            border-radius: 50px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <div class='temp'>
              <p>Hello,</p>
              <p>You recently asked to reset your account password.</p>
              <p>Please click the button below to change your password.</p>
              <p class='text-center'>
                  <a href='%PASSWORD_RESET_LINK%' target='_blank' class='reset-btn'>Reset Password</a>
              </p>
              <p>Note that this link is valid for 24 hours. After the time limit has expired, you will have to resubmit the request for a password reset.</p>
              <br>Thank you, <br> Team %WEBSITE_NAME% <br>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END RESET PASSWORD**********/

/*************************START CONTACT US MAIL TO ADMIN**********/

define('ADMIN_CONTACT_US_SUBJECT', 'Inquiry Received- Prism Prints');
define('USER_SUBJECT', 'Thank you for contacting us- Prism Prints');
define('ADMIN_CONTACT_US_BODY', '<!DOCTYPE html>
<html>
<head>
 <style type="text/css">
  body {
    color: #4d4d4d;
    font-family: Montserrat,sans-serif,Helvetica,Arial;
  }

    .content {
      border: 1px solid #eaeaec;
    box-shadow: 0 0 4px rgba(40,44,63,.08);
    max-width: 640px;
        margin: 0 auto;
    }
    .header,.info {
      padding: 15px;
      border-bottom: 2px solid #eaeaec;
    }
    h3 {
      font-size: 20px;
      font-weight: bold;
      margin: 5px 0 2px 0;
    }
    p{
      margin: 10px 0;
      }
  </style>
</head>
<body >
<div class="content">
  <div class="info">
  <h3>New Inquiry Received</h3>
  <div class="temp">
    <p>Full Name: <strong>%FULLNAME%</strong></p>
    <p>Email Id: <strong>%EMAIL%</strong></p>
    <p>Mobile No.: <strong>%MOBILE%</strong></p>
    <p>Message: <strong>%MESSAGE%</strong></p>
  </div>
  </div>
</div>
</body>
</html>
');
/*************************END CONTACT US MAIL TO ADMIN**********/

/*************************START ADMIN COMPLAINT RECEIVED **********/
define('ADMIN_COMPLAINT_RECEIVED_SUBJECT', 'New Complaint Received- ' . WEBSITE_NAME);
define('ADMIN_COMPLAINT_RECEIVED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: Montserrat, sans-serif, Helvetica, Arial;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header, .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='info'>
            <h3>Complaint Received</h3>
            <p>
                Hey Admin, <br> New Complaint registered by <strong>'%CUSTOMER_NAME%'</strong>.
            </p>
            <div class='temp'>
                <h4>Complaint Details:</h4>
                <p>
                    Customer Name: <strong>%CUSTOMER_FULLNAME%</strong><br>
                    Order Id: <strong>%ORDER_ID%</strong><br>
                    Description: <strong>%COMPLAINT_DESC%</strong><br>
                    Date: <strong>%COMPLAINT_DATE%</strong>
                </p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END ADMIN COMPLAINT RECEIVED **********/

/*************************START ADMIN QUOTATION REQUEST **********/
define('ADMIN_QUOTATION_REQUEST_SUBJECT', 'Quotation- %QUOTATION_ID% Request Received');
define('ADMIN_QUOTATION_REQUEST_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: Montserrat, sans-serif, Helvetica, Arial;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
        table, td, th {
            border: 1px solid black;
            padding-left: 5px;
        }
        table {
            width: 80%;
            border-collapse: collapse;
            text-align: left;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='info'>
            <h3>Quotation</h3>
            <h3 class='line'>Request</h3>
            <p>
                Hey Admin, <br> New Quotation request received from <strong>%CUSTOMER_NAME%</strong>.
            </p>
            <div class='temp'>
                <h4>Quotation Details:</h4>
                <table>
                    <tr>
                        <th>Quotation Id</th>
                        <td>%QUOTATION_ID%</td>
                    </tr>
                    <tr>
                        <th>Product Name</th>
                        <td>%PRODUCT_NAME%</td>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td>%QUANTITY%</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>                            
                            Paper GSM & Size: %PAPER_GSM_SIZE% <br>
                            Creasing: %CREASING% <br>
                            Half Cut: %HALF_CUT% <br>
                            UV: %UV% <br>
                            Lamination Type: %LAMINATION_TYPE% <br>
                            Lamination: %LAMINATION%<br>                            
                        </td>
                    </tr>
                    <tr>
                        <th>Staff Description</th>
                        <td>%STAFF_DESCRIPTION%</td>
                    </tr>                   
                    <tr>
                        <th>Received On</th>
                        <td>%RECEIVE_DATE%</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END ADMIN QUOTATION REQUEST **********/

/***********************************************************
				END EMAIL TEMPLATES
 ************************************************************/

/***********************************************************
				START MSG TEMPLATES
 ************************************************************/

define('ORDER_PLACED_USER_MSG', 'Hi %RECEIVER_NAME%, Order Confirmed: ID %ORDER_NO%. Please check your email for more details. Thank you for shopping with Prism Prints');

define('ORDER_PLACED_ADMIN_MSG', 'Hi Admin, New order has placed %ORDER_NO%. Please check your email for more details.');

/***********************************************************
				END MSG TEMPLATES
 ************************************************************/
