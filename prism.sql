-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2021 at 07:28 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prism`
--

-- --------------------------------------------------------

--
-- Table structure for table `bsn_prm`
--

CREATE TABLE `bsn_prm` (
  `bpm_id` int(11) NOT NULL,
  `bpm_name` varchar(400) NOT NULL,
  `bpm_value` varchar(100) NOT NULL COMMENT 'send email/sms; true=1, false=0',
  `bpm_default_value` varchar(100) NOT NULL,
  `bpm_status` int(11) NOT NULL,
  `bpm_crtd_dt` datetime NOT NULL,
  `bpm_crtd_by` int(11) NOT NULL,
  `bpm_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `bpm_updt_by` varchar(50) NOT NULL,
  `bpm_last_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='business parameters';

--
-- Dumping data for table `bsn_prm`
--

INSERT INTO `bsn_prm` (`bpm_id`, `bpm_name`, `bpm_value`, `bpm_default_value`, `bpm_status`, `bpm_crtd_dt`, `bpm_crtd_by`, `bpm_updt_dt`, `bpm_updt_by`, `bpm_last_ip`) VALUES
(1, 'logo_ico', 'public/logo_image/logo.ico', '', 0, '0000-00-00 00:00:00', 0, '2017-10-13 08:49:46', '', ''),
(2, 'logo', 'public/logo_image/logo.png', '', 0, '0000-00-00 00:00:00', 0, '2017-10-13 08:52:55', '', ''),
(3, 'email', '1', '', 1, '0000-00-00 00:00:00', 0, '2021-01-07 06:31:43', '', ''),
(4, 'sms', '1', '', 0, '0000-00-00 00:00:00', 0, '2021-01-09 08:51:26', '', ''),
(5, 'PAYTM_PAY_RESPONSE_CODE', '1', '', 0, '2018-12-02 18:20:00', 0, '2020-11-24 06:22:44', '0', ''),
(6, 'PAYTM_PAY_STATUS', 'TXN_SUCCESS', '', 0, '2018-12-02 18:20:00', 0, '2018-12-02 12:50:00', '0', ''),
(7, 'order_code', 'ORD', '0001', 1, '0000-00-00 00:00:00', 0, '2018-12-02 17:06:45', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('4b2ff2243d8c5914643b6b3b2e543697', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611252969, 'a:7:{s:9:\"user_data\";s:0:\"\";s:10:\"tsn_usr_id\";s:1:\"1\";s:16:\"tsn_usr_username\";s:5:\"admin\";s:13:\"tsn_usr_email\";s:28:\"kishanzalariya7600@gmail.com\";s:14:\"tsn_usr_mobile\";s:10:\"7600631363\";s:14:\"tsn_usr_dpt_id\";s:1:\"1\";s:12:\"is_logged_in\";b:1;}'),
('c737d33482ac8028ed94f9fb2bda8a96', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611250557, ''),
('468402907bb8caa0b3a299e9aea74e54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611251092, 'a:7:{s:9:\"user_data\";s:0:\"\";s:10:\"tsn_usr_id\";s:1:\"1\";s:16:\"tsn_usr_username\";s:5:\"admin\";s:13:\"tsn_usr_email\";s:28:\"kishanzalariya7600@gmail.com\";s:14:\"tsn_usr_mobile\";s:10:\"7600631363\";s:14:\"tsn_usr_dpt_id\";s:1:\"1\";s:12:\"is_logged_in\";b:1;}'),
('071f7838dd6091fd7ff6ea5e8520a983', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611252969, 'a:7:{s:9:\"user_data\";s:0:\"\";s:10:\"tsn_usr_id\";s:1:\"1\";s:16:\"tsn_usr_username\";s:5:\"admin\";s:13:\"tsn_usr_email\";s:28:\"kishanzalariya7600@gmail.com\";s:14:\"tsn_usr_mobile\";s:10:\"7600631363\";s:14:\"tsn_usr_dpt_id\";s:1:\"1\";s:12:\"is_logged_in\";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions_web`
--

CREATE TABLE `ci_sessions_web` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions_web`
--

INSERT INTO `ci_sessions_web` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('7e39b591a6a585679fc93f134f58100a', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', 1511530813, ''),
('24a753ec35090bdb4ece4507cbcb1c40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1513078026, ''),
('ee992e2b9f06a6c48fae0af863407a68', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1511327890, ''),
('e571bb99a52ebf7f052d9ceb19833b75', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1511323962, ''),
('bd2516a8c1013e338ec754929b4087d4', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', 1511269241, ''),
('4becdbea4747cb9ecde3c84b9cc78e2c', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1511262070, ''),
('6804e2b6ffa91bf6ddede649997970d3', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', 1511268810, ''),
('639d42de08841088f7f1e97adb6bc2f6', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1537621723, 'a:2:{s:9:\"user_data\";s:0:\"\";s:13:\"cart_contents\";a:4:{s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:7:{s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:2:\"id\";s:2:\"15\";s:4:\"name\";s:21:\"Pomegranate air spray\";s:3:\"qty\";s:1:\"2\";s:5:\"price\";s:3:\"373\";s:5:\"image\";s:50:\"http://localhost/v30/assets/img/product_no_img.png\";s:8:\"subtotal\";i:746;}s:32:\"f457c545a9ded88f18ecee47145a72c0\";a:7:{s:5:\"rowid\";s:32:\"f457c545a9ded88f18ecee47145a72c0\";s:2:\"id\";s:2:\"49\";s:4:\"name\";s:7:\"Almonds\";s:3:\"qty\";s:1:\"1\";s:5:\"price\";s:3:\"796\";s:5:\"image\";s:50:\"http://localhost/v30/assets/img/product_no_img.png\";s:8:\"subtotal\";i:796;}s:11:\"total_items\";i:3;s:10:\"cart_total\";i:1542;}}'),
('7b7b4a194dc39deb048ee96e34f7b8e6', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1537622049, 'a:2:{s:9:\"user_data\";s:0:\"\";s:13:\"cart_contents\";a:3:{s:32:\"0e65972dce68dad4d52d063967f0a705\";a:7:{s:5:\"rowid\";s:32:\"0e65972dce68dad4d52d063967f0a705\";s:2:\"id\";s:3:\"198\";s:4:\"name\";s:6:\"Amchur\";s:3:\"qty\";s:1:\"2\";s:5:\"price\";s:3:\"141\";s:5:\"image\";s:50:\"http://localhost/v30/assets/img/product_no_img.png\";s:8:\"subtotal\";i:282;}s:11:\"total_items\";i:2;s:10:\"cart_total\";i:282;}}');

-- --------------------------------------------------------

--
-- Table structure for table `com_events`
--

CREATE TABLE `com_events` (
  `ent_id` int(11) NOT NULL,
  `ent_name` varchar(400) NOT NULL,
  `ent_when` varchar(400) NOT NULL,
  `ent_status` int(11) NOT NULL,
  `ent_crtd_by` varchar(50) NOT NULL,
  `ent_crtd_dt` datetime NOT NULL,
  `ent_updt_by` varchar(50) NOT NULL,
  `ent_updt_dt` datetime NOT NULL,
  `ent_last_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='communication events';

--
-- Dumping data for table `com_events`
--

INSERT INTO `com_events` (`ent_id`, `ent_name`, `ent_when`, `ent_status`, `ent_crtd_by`, `ent_crtd_dt`, `ent_updt_by`, `ent_updt_dt`, `ent_last_ip`) VALUES
(1, 'product_status_changed', 'when product status changed', 1, '1', '2019-01-13 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `com_events_usr`
--

CREATE TABLE `com_events_usr` (
  `eus_id` int(11) NOT NULL,
  `eus_ent_id` int(11) NOT NULL,
  `eus_usr_type` varchar(50) NOT NULL,
  `eus_tmp_id` int(11) NOT NULL,
  `eus_type` varchar(50) NOT NULL,
  `eus_order_by` int(11) NOT NULL,
  `eus_status` int(11) NOT NULL,
  `eus_crtd_by` varchar(50) NOT NULL,
  `eus_crtd_dt` datetime NOT NULL,
  `eus_updt_by` varchar(50) NOT NULL,
  `eus_updt_dt` datetime NOT NULL,
  `eus_last_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `com_events_usr`
--

INSERT INTO `com_events_usr` (`eus_id`, `eus_ent_id`, `eus_usr_type`, `eus_tmp_id`, `eus_type`, `eus_order_by`, `eus_status`, `eus_crtd_by`, `eus_crtd_dt`, `eus_updt_by`, `eus_updt_dt`, `eus_last_ip`) VALUES
(1, 1, 'user', 1, 'email', 1, 1, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 1, 'user', 2, 'sms', 2, 1, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 1, 'admin', 3, 'email', 3, 1, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(4, 1, 'admin', 4, 'sms', 4, 1, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `com_template`
--

CREATE TABLE `com_template` (
  `tmp_id` int(11) NOT NULL,
  `tmp_name` varchar(100) NOT NULL,
  `tmp_sub` varchar(200) NOT NULL,
  `tmp_msg` text NOT NULL,
  `tmp_crtd_by` varchar(50) NOT NULL,
  `tmp_crtd_dt` datetime NOT NULL,
  `tmp_updt_by` varchar(50) NOT NULL,
  `tmp_updt_dt` datetime NOT NULL,
  `tmp_last_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `com_template`
--

INSERT INTO `com_template` (`tmp_id`, `tmp_name`, `tmp_sub`, `tmp_msg`, `tmp_crtd_by`, `tmp_crtd_dt`, `tmp_updt_by`, `tmp_updt_dt`, `tmp_last_ip`) VALUES
(1, 'product_status_changed_user_mail', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 'product_status_changed_user_msg', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 'product_status_changed_admin_mail', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(4, 'product_status_changed_admin_msg', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `CON_id` int(11) NOT NULL,
  `CON_name` varchar(50) NOT NULL,
  `CON_mail_id` varchar(50) NOT NULL,
  `CON_mob_no` varchar(20) NOT NULL,
  `CON_objective` varchar(200) NOT NULL,
  `CON_msg` varchar(200) NOT NULL,
  `CON_created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`CON_id`, `CON_name`, `CON_mail_id`, `CON_mob_no`, `CON_objective`, `CON_msg`, `CON_created_on`) VALUES
(1, 'Kishan Zalariya', 'kishanzalariya7600@gmail.com', '+917600631363', 'Inquiry', 'Testing...', '2020-12-16 05:06:28'),
(2, 'kishan zalariya', 'user@gmail.com', '987643211', 'Inquiry', 'klkll', '2020-12-16 05:06:28');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dpt_id` int(11) NOT NULL,
  `dpt_name` varchar(200) NOT NULL,
  `dpt_head_id` int(11) NOT NULL,
  `dpt_parent_id` int(11) NOT NULL,
  `dpt_Status` int(11) NOT NULL DEFAULT 1,
  `dpt_crtd_by` int(11) NOT NULL,
  `dpt_crtd_dt` datetime NOT NULL,
  `dpt_updt_by` int(11) NOT NULL,
  `dpt_updt_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dpt_id`, `dpt_name`, `dpt_head_id`, `dpt_parent_id`, `dpt_Status`, `dpt_crtd_by`, `dpt_crtd_dt`, `dpt_updt_by`, `dpt_updt_dt`) VALUES
(1, 'Admin', 0, 0, 1, 1, '2020-12-22 16:24:45', 0, '2020-12-22 10:54:45'),
(2, 'Customer ', 0, 0, 0, 1, '2020-12-22 16:24:45', 0, '2020-12-22 10:54:45'),
(3, 'Account', 0, 0, 1, 1, '2020-12-22 16:24:45', 0, '2020-12-22 10:54:45'),
(4, 'Offline Customer', 0, 0, 0, 1, '2021-01-02 11:24:52', 0, '2021-01-02 05:54:52');

-- --------------------------------------------------------

--
-- Table structure for table `department_menu_access`
--

CREATE TABLE `department_menu_access` (
  `dma_id` int(11) NOT NULL,
  `dma_dpt_id` int(11) NOT NULL COMMENT '1=Admin, 2=Customer, 3=Account',
  `dma_mnu_id` int(11) NOT NULL COMMENT '1-Dashboard, 2-Settings, 3-products, 4-Banner Images, 5-Orders, 6-Categories, 7-Customer details, 8-Payment Receipt, 9-Ledger, 10-Offline, 11-Paytm Transaction, 12-Contact Us, 13-Manage Users',
  `dma_created_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department_menu_access`
--

INSERT INTO `department_menu_access` (`dma_id`, `dma_dpt_id`, `dma_mnu_id`, `dma_created_on`) VALUES
(1, 1, 1, '2020-12-25 13:34:48'),
(2, 1, 2, '2020-12-25 13:34:48'),
(3, 1, 3, '2020-12-25 13:34:48'),
(4, 1, 4, '2020-12-25 13:34:48'),
(5, 1, 5, '2020-12-25 13:34:48'),
(6, 1, 6, '2020-12-25 13:34:48'),
(7, 1, 7, '2020-12-25 13:34:48'),
(8, 1, 8, '2020-12-25 13:34:48'),
(9, 1, 9, '2020-12-25 13:34:48'),
(10, 1, 10, '2020-12-25 13:34:48'),
(11, 1, 11, '2020-12-25 13:35:45'),
(12, 1, 12, '2020-12-25 13:35:45'),
(13, 1, 13, '2020-12-25 13:35:45'),
(14, 3, 7, '2020-12-25 13:37:32'),
(15, 3, 9, '2020-12-25 13:37:32');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `evt_id` int(11) NOT NULL,
  `evt_slug` varchar(500) NOT NULL,
  `evt_title` varchar(500) NOT NULL,
  `evt_description` blob NOT NULL,
  `evt_img` varchar(100) NOT NULL,
  `evt_strt_dt` date NOT NULL,
  `evt_end_dt` date NOT NULL,
  `evt_dt_type` varchar(255) NOT NULL,
  `evt_order` varchar(50) NOT NULL,
  `evt_crtd_by` varchar(50) NOT NULL,
  `evt_updt_by` varchar(50) NOT NULL,
  `evt_crtd_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`evt_id`, `evt_slug`, `evt_title`, `evt_description`, `evt_img`, `evt_strt_dt`, `evt_end_dt`, `evt_dt_type`, `evt_order`, `evt_crtd_by`, `evt_updt_by`, `evt_crtd_dt`) VALUES
(2, '9th-agrovision-workshops-national-expo-conference-0260', '9th Agrovision, Workshops, National Expo & Conference', 0x3c703e56656e7565203a2052657368696d626167682047726f756e642c204e61677075722c204d616861726173687472612c20496e6469613c62723e3c2f703e, 'ppE96Fa1aR.jpg', '2017-11-10', '2017-11-13', 'multiple', '', '1', '1', '2017-11-18 13:43:47'),
(3, '5th-world-tea-coffee-expo-5545', '5th World Tea & Coffee Expo', 0x3c703e56656e7565203a20426f6d62617920436f6e76656e74696f6e616c2026616d703b2045786869626974696f6e2043656e7472652c20476f726567616f6e202845292c204d756d6261692c20496e646961266e6273703b3c2f703e3c703e61736461736473612057414441573c2f703e3c703e3c62723e3c2f703e3c703e3c2f703e, ' ', '2017-11-11', '2017-11-11', 'multiple', '', '1', '1', '2017-11-18 13:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `featured_products`
--

CREATE TABLE `featured_products` (
  `fp_id` int(11) NOT NULL,
  `fp_prd_id` int(11) NOT NULL,
  `fp_status` int(11) NOT NULL,
  `fp_crtd_dt` date NOT NULL,
  `fp_crtd_by` int(11) NOT NULL,
  `fp_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fp_updt_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `fdk_id` int(11) NOT NULL,
  `fdk_slug` varchar(200) NOT NULL,
  `fdk_subject` varchar(250) NOT NULL,
  `fdk_type` int(11) NOT NULL,
  `fdk_description` blob NOT NULL,
  `fdk_user` int(11) NOT NULL,
  `fdk_status` int(11) NOT NULL,
  `fdk_date` date NOT NULL,
  `fdk_crtd_dt` datetime NOT NULL,
  `fdk_crtd_by` int(11) NOT NULL,
  `fdk_updt_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forgot_password_transaction`
--

CREATE TABLE `forgot_password_transaction` (
  `fpt_id` int(11) NOT NULL,
  `fpt_prs_id` int(11) NOT NULL,
  `fpt_is_admin_user` int(11) NOT NULL DEFAULT 0,
  `fpt_code` int(11) NOT NULL,
  `fpt_status` int(11) NOT NULL,
  `fpt_crtd_dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forgot_password_transaction`
--

INSERT INTO `forgot_password_transaction` (`fpt_id`, `fpt_prs_id`, `fpt_is_admin_user`, `fpt_code`, `fpt_status`, `fpt_crtd_dt`) VALUES
(23, 1, 1, 2323, 1, '2021-01-10 21:17:07'),
(24, 1, 1, 9922, 1, '2021-01-10 21:19:20'),
(25, 1, 1, 3412, 1, '2021-01-10 21:21:06'),
(26, 1, 1, 3796, 1, '2021-01-10 21:22:28'),
(27, 1, 1, 7905, 0, '2021-01-12 09:31:06');

-- --------------------------------------------------------

--
-- Table structure for table `form_access`
--

CREATE TABLE `form_access` (
  `fma_id` int(255) NOT NULL,
  `fma_usr_id` int(255) NOT NULL,
  `fma_mnu_id` int(255) NOT NULL,
  `fma_sbm_id` int(255) NOT NULL,
  `fma_access` varchar(11) NOT NULL,
  `fma_read` varchar(11) NOT NULL,
  `fma_write` varchar(11) NOT NULL,
  `fma_update` varchar(11) NOT NULL,
  `fma_delete` varchar(11) NOT NULL,
  `fma_status` varchar(5) NOT NULL DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_access`
--

INSERT INTO `form_access` (`fma_id`, `fma_usr_id`, `fma_mnu_id`, `fma_sbm_id`, `fma_access`, `fma_read`, `fma_write`, `fma_update`, `fma_delete`, `fma_status`) VALUES
(1, 1, 2, 1, '1', '1', '1', '1', '1', 'y'),
(3, 1, 3, 3, '1', '1', '1', '1', '1', 'y'),
(60, 1, 7, 0, '1', '1', '1', '1', '1', 'y'),
(68, 1, 1, 0, '1', '1', '1', '1', '1', 'y'),
(69, 1, 3, 11, '1', '1', '1', '1', '1', 'y'),
(70, 1, 3, 12, '1', '1', '1', '1', '1', 'y'),
(73, 1, 3, 15, '1', '1', '1', '1', '1', 'y'),
(74, 1, 10, 0, '1', '1', '1', '1', '1', 'y'),
(75, 1, 4, 0, '1', '1', '1', '1', '1', 'y'),
(76, 1, 5, 0, '1', '1', '1', '1', '1', 'y'),
(77, 1, 6, 0, '1', '1', '1', '1', '1', 'y'),
(78, 1, 8, 0, '1', '1', '1', '1', '1', 'y'),
(79, 1, 9, 0, '1', '1', '1', '1', '1', 'y'),
(80, 1, 11, 0, '1', '1', '1', '1', '1', 'y'),
(81, 1, 12, 0, '1', '1', '1', '1', '1', 'y'),
(82, 1, 13, 0, '1', '1', '1', '1', '1', 'y'),
(84, 3, 7, 0, '1', '1', '1', '1', '1', 'y'),
(85, 3, 9, 0, '1', '1', '1', '1', '1', 'y'),
(86, 1, 10, 18, '1', '1', '1', '1', '1', 'y'),
(87, 1, 10, 19, '1', '1', '1', '1', '1', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_set`
--

CREATE TABLE `gallery_set` (
  `gls_id` int(11) NOT NULL,
  `gls_type` int(11) NOT NULL COMMENT 'gen_prm group same as col name',
  `gls_name` varchar(200) NOT NULL,
  `gls_image` varchar(200) NOT NULL,
  `gls_link` text NOT NULL,
  `gls_order_by` int(11) NOT NULL,
  `gls_status` int(11) NOT NULL DEFAULT 1 COMMENT ' gen_prm group = \r\n\r\nactive_status',
  `gls_crdt_by` int(11) NOT NULL COMMENT 'people id',
  `gls_crdt_dt` datetime NOT NULL,
  `gls_updt_by` int(11) NOT NULL COMMENT 'people id',
  `gls_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_set`
--

INSERT INTO `gallery_set` (`gls_id`, `gls_type`, `gls_name`, `gls_image`, `gls_link`, `gls_order_by`, `gls_status`, `gls_crdt_by`, `gls_crdt_dt`, `gls_updt_by`, `gls_updt_dt`) VALUES
(73, 1, 'banner1', 'AMINO ROAR WEB.jpg', '', 1, 1, 0, '0000-00-00 00:00:00', 0, '2021-01-05 16:58:02'),
(74, 1, 'banner2', 'nwe website banner.jpg', '', 2, 1, 0, '0000-00-00 00:00:00', 0, '2021-01-05 16:57:24'),
(75, 1, 'banner3', 'Website Banner-3301.jpg', '', 3, 1, 0, '0000-00-00 00:00:00', 0, '2021-01-05 16:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `gen_prm`
--

CREATE TABLE `gen_prm` (
  `gnp_id` int(11) NOT NULL,
  `gnp_name` varchar(200) NOT NULL,
  `gnp_value` int(11) NOT NULL,
  `gnp_group` varchar(100) NOT NULL,
  `gnp_order` int(11) NOT NULL,
  `gnp_status` int(11) NOT NULL,
  `gnp_crtd_by` int(11) NOT NULL COMMENT 'person_id',
  `gnp_crtd_dt` datetime NOT NULL,
  `gnp_updt_by` int(11) NOT NULL COMMENT 'person_id',
  `gnp_updt_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gen_prm`
--

INSERT INTO `gen_prm` (`gnp_id`, `gnp_name`, `gnp_value`, `gnp_group`, `gnp_order`, `gnp_status`, `gnp_crtd_by`, `gnp_crtd_dt`, `gnp_updt_by`, `gnp_updt_dt`) VALUES
(1, 'ml', 1, 'prd_quantity_unit', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:45:53'),
(2, 'gms', 2, 'prd_quantity_unit', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:47:02'),
(3, 'Kg', 3, 'prd_quantity_unit', 3, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:47:02'),
(51, 'Cash', 1, 'payment_mode', 1, 1, 1, '2017-06-06 00:00:00', 0, '2017-10-25 17:42:05'),
(52, 'Paytm', 2, 'payment_mode', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-26 13:11:58'),
(54, 'Home (All day delivery)', 1, 'address_type', 1, 1, 1, '2017-06-06 00:00:00', 0, '2017-10-25 17:42:05'),
(55, 'Work (Delivery between 10AM-6PM)', 2, 'address_type', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-26 13:13:56'),
(90, 'Litres', 4, 'prd_quantity_unit', 4, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:47:02'),
(91, 'lbs', 5, 'prd_quantity_unit', 5, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:47:02'),
(99, 'Placed', 1, 'order_status', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-25 14:12:02'),
(100, 'Cancelled', 2, 'order_status', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-25 14:12:02'),
(101, 'Grievance', 1, 'feedback_type', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-25 14:12:02'),
(102, 'Suggetion', 2, 'feedback_type', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-25 14:12:02'),
(111, 'Male', 1, 'gender', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-11-03 09:01:42'),
(112, 'Female', 2, 'gender', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-11-03 09:01:42'),
(124, 'Agent', 1, 'career_status', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-11-16 13:04:50'),
(129, 'Delivery', 1, 'pcd_delivery_sts', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-11-21 09:35:40'),
(130, 'Non-Delivery ', 2, 'pcd_delivery_sts', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-11-21 09:35:40'),
(132, 'In Stock', 1, 'availability', 1, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:45:53'),
(133, 'Out Of Stock', 2, 'availability', 2, 1, 0, '0000-00-00 00:00:00', 0, '2017-10-14 11:45:53'),
(134, '3', 3, 'cart_qty', 3, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:09'),
(135, '4', 4, 'cart_qty', 4, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:09'),
(136, '5', 5, 'cart_qty', 5, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(137, '6', 6, 'cart_qty', 6, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(138, '7', 7, 'cart_qty', 7, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(139, '8', 8, 'cart_qty', 8, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(140, '9', 9, 'cart_qty', 9, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(141, '10', 10, 'cart_qty', 10, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(142, '1', 1, 'cart_qty', 1, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(143, '2', 2, 'cart_qty', 2, 1, 0, '0000-00-00 00:00:00', 0, '2018-10-23 18:13:10'),
(144, 'Placed', 1, 'order_prd_status', 1, 1, 1, '2020-12-13 05:54:09', 0, '2020-12-13 00:24:09'),
(145, 'Processing', 2, 'order_prd_status', 2, 1, 1, '2020-12-13 05:54:09', 0, '2020-12-13 00:24:09'),
(146, 'File corrupted', 3, 'order_prd_status', 3, 1, 1, '2020-12-13 05:54:09', 0, '2020-12-13 00:24:09'),
(147, 'Supplements', 1, 'menu', 1, 1, 0, '0000-00-00 00:00:00', 0, '2018-12-25 21:27:30'),
(148, 'Shop', 2, 'menu', 1, 1, 0, '0000-00-00 00:00:00', 0, '2018-12-25 21:27:30'),
(149, 'Active', 1, 'status', 1, 1, 0, '0000-00-00 00:00:00', 0, '2018-12-25 21:27:30'),
(150, 'Inctive', 2, 'status', 1, 1, 0, '0000-00-00 00:00:00', 0, '2018-12-25 21:27:30'),
(151, 'Pay from Balance', 3, 'payment_mode', 3, 1, 0, '0000-00-00 00:00:00', 0, '2020-11-27 13:11:58'),
(152, 'Dispatched', 3, 'order_status', 3, 1, 0, '2020-12-09 05:15:48', 0, '2020-12-08 23:45:48'),
(153, 'Delivered', 4, 'order_status', 4, 1, 0, '2020-12-09 05:15:48', 0, '2020-12-08 23:45:48'),
(154, 'Pending', 1, 'payment_receipt_status', 1, 1, 1, '2020-12-10 05:09:58', 0, '2020-12-09 23:39:58'),
(155, 'Approve', 2, 'payment_receipt_status', 2, 1, 1, '2020-12-10 05:11:08', 0, '2020-12-09 23:41:08'),
(156, 'Reject', 3, 'payment_receipt_status', 3, 1, 1, '2020-12-10 05:11:37', 0, '2020-12-09 23:41:37'),
(157, 'Bank', 1, 'payment_type', 1, 1, 1, '2020-12-11 16:47:58', 0, '2020-12-11 11:17:58'),
(158, 'Cash', 2, 'payment_type', 2, 1, 1, '2020-12-11 16:47:58', 0, '2020-12-11 11:17:58'),
(159, 'Pay From Balance', 3, 'payment_type', 3, 2, 1, '2020-12-11 16:47:58', 0, '2020-12-11 11:17:58'),
(160, 'Mismatch quantity', 4, 'order_prd_status', 4, 1, 1, '2020-12-13 05:55:26', 0, '2020-12-13 00:25:26'),
(161, 'Mismatch quality', 5, 'order_prd_status', 5, 1, 1, '2020-12-13 05:57:35', 0, '2020-12-13 00:27:35'),
(162, 'Is in printing', 6, 'order_prd_status', 6, 1, 1, '2020-12-13 05:58:34', 0, '2020-12-13 00:28:34'),
(163, 'Order is ready', 7, 'order_prd_status', 7, 1, 1, '2020-12-13 05:59:20', 0, '2020-12-13 00:29:20'),
(164, 'Dispatched', 8, 'order_prd_status', 8, 1, 1, '2020-12-13 05:59:58', 0, '2020-12-13 00:29:58'),
(165, 'Delivered', 9, 'order_prd_status', 9, 1, 1, '2020-12-13 06:00:22', 0, '2020-12-13 00:30:22'),
(169, 'Payment gateway', 4, 'payment_mode', 4, 1, 0, '2021-01-20 22:52:55', 0, '2021-01-20 11:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `img_id` int(11) NOT NULL,
  `img_type` varchar(50) NOT NULL,
  `img_type_id` int(11) NOT NULL,
  `img_title` varchar(100) NOT NULL,
  `img_path` varchar(5000) NOT NULL,
  `img_name` longtext NOT NULL,
  `img_alternate_text` varchar(100) NOT NULL,
  `img_crtd_date` datetime NOT NULL,
  `img_crtd_by` int(11) NOT NULL,
  `img_updt_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `img_updt_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`img_id`, `img_type`, `img_type_id`, `img_title`, `img_path`, `img_name`, `img_alternate_text`, `img_crtd_date`, `img_crtd_by`, `img_updt_date`, `img_updt_by`) VALUES
(8, '1', 5, 'Gain Matrix', 'GIANT PUMP.jpg', 'GIANT PUMP.jpg', 'Gain Matrix', '2018-11-13 15:01:50', 0, '2018-11-13 09:31:50', 0),
(9, '1', 6, 'Product1 Muscle Gainerfdg', '02 (Vikrant Kasar).jpg', '02 (Vikrant Kasar).jpg', 'Product1 Muscle Gainerfdg', '2018-11-17 19:19:14', 0, '2018-11-17 13:49:14', 0),
(14, '1', 3, 'Meal Replacement', '1-4561.jpg', '1-4561.jpg', 'Meal Replacement', '2018-12-01 14:32:10', 0, '2018-12-01 09:02:10', 0),
(19, '1', 3, 'Meal Replacement', '2-8310.jpg', '2-8310.jpg', 'Meal Replacement', '2018-12-01 16:37:22', 0, '2018-12-01 11:07:22', 0),
(20, '1', 3, 'Meal Replacement', '3.jpg', '3.jpg', 'Meal Replacement', '2018-12-01 16:37:22', 0, '2018-12-01 11:07:22', 0),
(21, '1', 1, 'TOTAL WAR - PRE WORKOUT', 'Total_War_supplement_facts_corporate_720x.png', 'Total_War_supplement_facts_corporate_720x.png', 'TOTAL WAR - PRE WORKOUT', '2018-12-10 18:43:41', 0, '2018-12-10 13:13:41', 0),
(36, '1', 5, 'IMPACT MASS', 'Impact Mass   (1.5kg) Dutch Chocolate.png', 'Impact Mass   (1.5kg) Dutch Chocolate.png', 'IMPACT MASS', '2018-12-18 11:34:43', 0, '2018-12-18 06:04:43', 0),
(37, '1', 6, 'PRO 4 SR', 'PRO SR4     (1.5kg) Dutch Chocolate.png', 'PRO SR4     (1.5kg) Dutch Chocolate.png', 'PRO 4 SR', '2018-12-18 11:56:37', 0, '2018-12-18 06:26:37', 0),
(38, '1', 7, 'EGG WHEY', 'EGG WHEY (1kg) CAFE MOCHA.png', 'EGG WHEY (1kg) CAFE MOCHA.png', 'EGG WHEY', '2018-12-18 12:22:30', 0, '2018-12-18 06:52:30', 0),
(39, '1', 8, 'RAW WHEY', 'RAW WHEY.png', 'RAW WHEY.png', 'RAW WHEY', '2018-12-18 13:11:54', 0, '2018-12-18 07:41:54', 0),
(40, '1', 9, 'AMINO ROAR', 'AMINO ROAR Orange.png', 'AMINO ROAR Orange.png', 'AMINO ROAR', '2018-12-18 13:49:55', 0, '2018-12-18 08:19:55', 0),
(41, '1', 10, 'BCAA 3.2.1', 'BCAA Orange.png', 'BCAA Orange.png', 'BCAA 3.2.1', '2018-12-18 14:44:53', 0, '2018-12-18 09:14:53', 0),
(42, '1', 11, 'XPLODE FX', 'Xplode Orange.png', 'Xplode Orange.png', 'XPLODE FX', '2018-12-18 16:27:08', 0, '2018-12-18 10:57:08', 0),
(89, '1', 22, 'On Whey', 'VideshKasar.jpg', 'VideshKasar.jpg', 'On Whey', '2019-01-29 20:54:06', 0, '2019-01-29 15:24:06', 0),
(91, '1', 13, 'HYPER WHEY', '71X0VCaq1IL._SL1000_.jpg', '71X0VCaq1IL._SL1000_.jpg', 'HYPER WHEY', '2019-02-02 14:20:11', 0, '2019-02-02 08:50:11', 0),
(93, '1', 12, '100% ISOLATE WHEY', 'isolate.jpg', 'isolate.jpg', '100% ISOLATE WHEY', '2019-02-02 16:34:51', 0, '2019-02-02 11:04:51', 0),
(94, '1', 12, '100% ISOLATE WHEY', 'isolate1.jpg', 'isolate1.jpg', '100% ISOLATE WHEY', '2019-02-02 16:35:57', 0, '2019-02-02 11:05:57', 0),
(95, '1', 19, 'AMINO ROAR', 'amino1.jpg', 'amino1.jpg', 'AMINO ROAR', '2019-02-02 16:37:52', 0, '2019-02-02 11:07:52', 0),
(96, '1', 19, 'AMINO ROAR', 'amino.jpg', 'amino.jpg', 'AMINO ROAR', '2019-02-02 16:38:18', 0, '2019-02-02 11:08:18', 0),
(97, '1', 20, 'BCAA 3.2.1', 'bcaa.jpg', 'bcaa.jpg', 'BCAA 3.2.1', '2019-02-02 16:39:48', 0, '2019-02-02 11:09:48', 0),
(98, '1', 20, 'BCAA 3.2.1', 'amino2-9690.jpg', 'amino2-9690.jpg', 'BCAA 3.2.1', '2019-02-02 16:42:16', 0, '2019-02-02 11:12:16', 0),
(99, '1', 17, 'EGG WHEY', 'egg whey.jpg', 'egg whey.jpg', 'EGG WHEY', '2019-02-02 16:43:54', 0, '2019-02-02 11:13:54', 0),
(100, '1', 17, 'EGG WHEY', 'egg whey 2.jpg', 'egg whey 2.jpg', 'EGG WHEY', '2019-02-02 16:44:25', 0, '2019-02-02 11:14:25', 0),
(101, '1', 13, 'HYPER WHEY', 'hyper1.jpg', 'hyper1.jpg', 'HYPER WHEY', '2019-02-02 16:45:51', 0, '2019-02-02 11:15:51', 0),
(102, '1', 15, 'IMPACT MASS', 'impact mass.jpg', 'impact mass.jpg', 'IMPACT MASS', '2019-02-02 16:47:27', 0, '2019-02-02 11:17:27', 0),
(103, '1', 15, 'IMPACT MASS', 'impact mass1.jpg', 'impact mass1.jpg', 'IMPACT MASS', '2019-02-02 16:47:53', 0, '2019-02-02 11:17:53', 0),
(104, '1', 14, 'MUSCLE IMPACT', 'muscleimpact.jpg', 'muscleimpact.jpg', 'MUSCLE IMPACT', '2019-02-02 16:49:24', 0, '2019-02-02 11:19:24', 0),
(105, '1', 14, 'MUSCLE IMPACT', 'muscleimpact1.jpg', 'muscleimpact1.jpg', 'MUSCLE IMPACT', '2019-02-02 16:50:46', 0, '2019-02-02 11:20:46', 0),
(106, '1', 16, 'PRO 4 SR', 'pro4sr.jpg', 'pro4sr.jpg', 'PRO 4 SR', '2019-02-02 16:53:53', 0, '2019-02-02 11:23:53', 0),
(107, '1', 16, 'PRO 4 SR', 'pro4sr 1.jpg', 'pro4sr 1.jpg', 'PRO 4 SR', '2019-02-02 16:55:11', 0, '2019-02-02 11:25:11', 0),
(108, '1', 18, 'RAW WHEY', 'rawwhey.jpg', 'rawwhey.jpg', 'RAW WHEY', '2019-02-02 16:56:04', 0, '2019-02-02 11:26:04', 0),
(109, '1', 18, 'RAW WHEY', 'rawwhey1.jpg', 'rawwhey1.jpg', 'RAW WHEY', '2019-02-02 16:57:10', 0, '2019-02-02 11:27:10', 0),
(110, '1', 21, 'XPLODE FX', 'xplode.jpg', 'xplode.jpg', 'XPLODE FX', '2019-02-02 16:59:46', 0, '2019-02-02 11:29:46', 0),
(111, '1', 21, 'XPLODE FX', 'xplode1.jpg', 'xplode1.jpg', 'XPLODE FX', '2019-02-02 17:00:07', 0, '2019-02-02 11:30:07', 0),
(112, '1', 22, 'NT 1 SIDE', 'envelope.jpg', 'envelope.jpg', 'NT 1 SIDE', '2020-10-02 19:02:34', 0, '2020-10-02 13:32:34', 0),
(113, '1', 23, 'My vitamin', 'letter.jpg', 'letter.jpg', 'My vitamin', '2020-10-03 16:10:30', 0, '2020-10-03 10:40:30', 0),
(114, '1', 24, 'My vitamin', 'letter-7409.jpg', 'letter-7409.jpg', 'My vitamin', '2020-10-03 16:26:14', 0, '2020-10-03 10:56:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `loc_id` int(11) NOT NULL,
  `loc_name` varchar(400) NOT NULL,
  `loc_lat` double NOT NULL,
  `loc_long` double NOT NULL,
  `loc_crtd_by` varchar(50) NOT NULL,
  `loc_crtd_dt` datetime NOT NULL,
  `loc_updt_by` varchar(50) NOT NULL,
  `loc_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `loc_last_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_master`
--

CREATE TABLE `menu_master` (
  `mnu_id` int(11) NOT NULL,
  `mnu_name` varchar(100) NOT NULL,
  `mnu_order` int(11) NOT NULL,
  `mnu_status` varchar(5) NOT NULL DEFAULT 'y',
  `mnu_link` varchar(100) NOT NULL,
  `mnu_icon` varchar(50) NOT NULL,
  `mnu_crtd_by` int(11) NOT NULL,
  `mnu_crtd_date` date NOT NULL,
  `mnu_updt_by` int(11) NOT NULL,
  `mnu_updt_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_master`
--

INSERT INTO `menu_master` (`mnu_id`, `mnu_name`, `mnu_order`, `mnu_status`, `mnu_link`, `mnu_icon`, `mnu_crtd_by`, `mnu_crtd_date`, `mnu_updt_by`, `mnu_updt_date`) VALUES
(1, 'Dashboard', 1, 'y', 'dashboard', 'fa fa-tachometer ', 0, '0000-00-00', 0, '2017-10-13 13:21:18'),
(2, 'Settings', 8, 'n', '', 'fa fa-cog', 0, '0000-00-00', 0, '2017-10-13 13:25:41'),
(3, 'Products', 4, 'y', 'products', 'fa fa-tags', 0, '0000-00-00', 0, '2017-10-13 13:21:18'),
(4, 'Banner Images', 2, 'y', 'gallery-set-list', 'fa fa-picture-o', 0, '0000-00-00', 0, '2017-10-13 18:51:18'),
(5, 'Orders', 5, 'y', 'orders', 'fa fa-file-text-o', 0, '0000-00-00', 0, '2017-10-13 18:51:18'),
(6, 'Categories', 3, 'y', 'categories', 'fa fa-tachometer ', 0, '0000-00-00', 0, '2017-10-13 13:21:18'),
(7, 'Customer details', 7, 'y', 'user_details', 'fa fa-users', 0, '0000-00-00', 0, '2020-11-30 10:53:52'),
(8, 'Payment Receipt', 6, 'y', 'payment', 'fa fa-rupee', 0, '2020-11-30', 0, '2020-11-30 11:42:44'),
(9, 'Ledger', 9, 'y', 'ledger', 'fa fa-list-ul', 0, '2020-12-04', 0, '2020-12-04 12:13:07'),
(10, 'Offline', 11, 'y', '', 'fa fa-power-off', 0, '2020-12-09', 0, '2020-12-08 11:14:15'),
(11, 'Paytm Transaction', 10, 'y', 'paytm_transaction', 'fa fa-money', 1, '2020-12-09', 0, '2020-12-14 11:41:48'),
(12, 'Contact Us', 13, 'y', 'contact_us_list', 'fa fa-envelope', 0, '2020-12-09', 0, '2020-12-15 23:32:40'),
(13, 'Manage Users', 12, 'y', 'user_list', 'fa fa-user', 1, '2020-12-22', 0, '2020-12-22 12:05:35');

-- --------------------------------------------------------

--
-- Table structure for table `menu_transaction`
--

CREATE TABLE `menu_transaction` (
  `mtr_id` int(11) NOT NULL,
  `mtr_mnu_id` int(11) NOT NULL,
  `mtr_dpt_id` int(11) NOT NULL,
  `mtr_status` varchar(5) NOT NULL DEFAULT 'y',
  `mnu_logo` varchar(100) NOT NULL,
  `mtr_crtd_by` int(11) NOT NULL,
  `mtr_crtd_date` datetime NOT NULL DEFAULT current_timestamp(),
  `mtr_updt_by` int(11) NOT NULL,
  `mtr_updt_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_transaction`
--

INSERT INTO `menu_transaction` (`mtr_id`, `mtr_mnu_id`, `mtr_dpt_id`, `mtr_status`, `mnu_logo`, `mtr_crtd_by`, `mtr_crtd_date`, `mtr_updt_by`, `mtr_updt_date`) VALUES
(1, 1, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(2, 2, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(3, 3, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(4, 4, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(5, 5, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(6, 6, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(7, 7, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(8, 8, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(9, 9, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(10, 10, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(11, 11, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(12, 12, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(13, 13, 1, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(14, 7, 3, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(15, 9, 3, 'y', '', 0, '2020-12-25 09:51:56', 0, '2020-12-25 09:51:56'),
(16, 1, 3, 'y', '', 0, '2020-12-25 09:58:50', 0, '2020-12-25 09:58:50');

-- --------------------------------------------------------

--
-- Table structure for table `offline_order`
--

CREATE TABLE `offline_order` (
  `offline_order_id` int(11) NOT NULL,
  `offline_order_reference_no` text NOT NULL,
  `offline_order_user_id` int(11) NOT NULL,
  `offline_order_subtotal` double NOT NULL,
  `offline_order_shipping_charges` double NOT NULL,
  `offline_order_discount` double NOT NULL DEFAULT 0,
  `offline_order_total` double NOT NULL,
  `offline_order_paid_amt` double NOT NULL,
  `offline_order_pending_amt` double NOT NULL,
  `offline_order_payment_status` tinyint(4) NOT NULL,
  `offline_order_payment_mode` int(11) NOT NULL,
  `offline_order_remarks` text DEFAULT NULL,
  `offline_order_status` tinyint(4) NOT NULL,
  `offline_order_is_success` tinyint(4) NOT NULL,
  `offline_order_created_by` int(11) NOT NULL,
  `offline_order_updated_by` int(11) NOT NULL,
  `offline_order_created_on` datetime NOT NULL,
  `offline_order_updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `offline_order_product`
--

CREATE TABLE `offline_order_product` (
  `oop_id` int(11) NOT NULL,
  `oop_order_id` int(11) NOT NULL,
  `oop_product_id` int(11) NOT NULL,
  `oop_designfile` text NOT NULL,
  `oop_quantity` int(11) NOT NULL,
  `oop_height` double NOT NULL,
  `oop_width` int(11) NOT NULL,
  `oop_price` double NOT NULL,
  `oop_discount` double NOT NULL DEFAULT 0,
  `oop_total` double NOT NULL,
  `oop_status` int(11) NOT NULL,
  `oop_remarks` text NOT NULL,
  `oop_created_by` int(11) NOT NULL,
  `oop_updated_by` int(11) NOT NULL,
  `oop_created_on` datetime NOT NULL,
  `oop_updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `offline_users`
--

CREATE TABLE `offline_users` (
  `offline_user_id` int(11) NOT NULL,
  `offline_user_fullname` varchar(50) NOT NULL,
  `offline_user_username` varchar(50) NOT NULL,
  `offline_user_email` varchar(100) NOT NULL,
  `offline_user_mobile` varchar(50) NOT NULL,
  `offline_user_gender` tinyint(4) DEFAULT NULL,
  `offline_user_dob` date DEFAULT NULL,
  `offline_user_gst` varchar(30) DEFAULT NULL,
  `offline_user_address_line` varchar(200) NOT NULL,
  `offline_user_city` varchar(50) NOT NULL,
  `offline_user_pincode` int(10) NOT NULL,
  `offline_user_district` varchar(50) NOT NULL,
  `offline_user_state` varchar(50) NOT NULL,
  `offline_user_status` int(1) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `offline_user_created_by` int(11) NOT NULL,
  `offline_user_updated_by` int(11) NOT NULL,
  `offline_user_created_on` datetime NOT NULL,
  `offline_user_updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `offline_users`
--

INSERT INTO `offline_users` (`offline_user_id`, `offline_user_fullname`, `offline_user_username`, `offline_user_email`, `offline_user_mobile`, `offline_user_gender`, `offline_user_dob`, `offline_user_gst`, `offline_user_address_line`, `offline_user_city`, `offline_user_pincode`, `offline_user_district`, `offline_user_state`, `offline_user_status`, `offline_user_created_by`, `offline_user_updated_by`, `offline_user_created_on`, `offline_user_updated_on`) VALUES
(1, 'harshad Tretiya', 'harshad90676', 'harshad.tretiya@gmail.com', '987543210', 1, '1998-06-14', 'gj2020', '         Vavdi road         ', 'Morbi', 363641, 'Morbi', 'Gujarat', 1, 1, 1, '2020-12-27 11:07:27', '2021-01-02 16:22:58'),
(3, 'Kaushal Tretiya', 'kaushal.tretiya', 'kaushal.tretiya@gmail.com', '9874131232', 1, '1998-09-12', '', 'Nirav Park, PL-21, B/H Syam-2 Society\nPanchasar road', 'Morbi', 363641, 'Morbi', 'Gujarat', 1, 1, 1, '2021-01-03 09:51:22', '2021-01-03 09:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `otp_transaction`
--

CREATE TABLE `otp_transaction` (
  `otp_id` int(11) NOT NULL,
  `otp_prs_id` int(11) NOT NULL DEFAULT 0,
  `otp_mob` varchar(100) NOT NULL,
  `otp_code` varchar(150) NOT NULL,
  `otp_status` int(11) NOT NULL COMMENT '1= active, 2=inactive',
  `otp_crtd_dt` timestamp NULL DEFAULT current_timestamp(),
  `otp_updt_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_rcpt`
--

CREATE TABLE `payment_rcpt` (
  `ID` int(200) NOT NULL,
  `rcpt_number` varchar(30) NOT NULL,
  `pay_type` varchar(200) NOT NULL,
  `pay_mode` varchar(200) NOT NULL,
  `pay_amt` double NOT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `rcpt_img` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `pay_remarks` text NOT NULL,
  `upload_by` int(11) NOT NULL,
  `upload_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `update_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paytm_transactions`
--

CREATE TABLE `paytm_transactions` (
  `pyt_id` int(11) NOT NULL,
  `pyt_pay_response_code` int(11) NOT NULL,
  `pyt_pay_status` int(11) DEFAULT NULL,
  `pyt_order_reference_no` varchar(200) DEFAULT NULL COMMENT 'foreign key order reference no',
  `pyt_mid_no` varchar(200) DEFAULT NULL,
  `pyt_txn_id` varchar(200) DEFAULT NULL,
  `pyt_txn_amt` double DEFAULT NULL,
  `pyt_payment_mode` varchar(100) DEFAULT NULL,
  `pyt_currency` varchar(100) DEFAULT NULL,
  `pyt_txn_date` datetime DEFAULT NULL,
  `pyt_txn_status` varchar(200) DEFAULT NULL,
  `pyt_resp_code` varchar(200) DEFAULT NULL COMMENT 'response code',
  `pyt_resp_message` varchar(200) DEFAULT NULL COMMENT 'response message',
  `pyt_gateway_name` varchar(200) DEFAULT NULL,
  `pyt_bnk_txn_id` varchar(200) DEFAULT NULL,
  `pyt_bnk_name` varchar(200) DEFAULT NULL,
  `pyt_status` int(11) DEFAULT NULL,
  `pyt_crtd_by` int(11) NOT NULL,
  `pyt_updt_by` int(11) NOT NULL,
  `pyt_crtd_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `pyt_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paytm_transactions`
--

INSERT INTO `paytm_transactions` (`pyt_id`, `pyt_pay_response_code`, `pyt_pay_status`, `pyt_order_reference_no`, `pyt_mid_no`, `pyt_txn_id`, `pyt_txn_amt`, `pyt_payment_mode`, `pyt_currency`, `pyt_txn_date`, `pyt_txn_status`, `pyt_resp_code`, `pyt_resp_message`, `pyt_gateway_name`, `pyt_bnk_txn_id`, `pyt_bnk_name`, `pyt_status`, `pyt_crtd_by`, `pyt_updt_by`, `pyt_crtd_dt`, `pyt_updt_dt`) VALUES
(35, 1, 1, 'ORDS1525639839', 'MoCVfk40813527929102', '20210121111212800110168051502300098', 1030, 'CC', 'INR', '2021-01-21 00:39:26', 'TXN_SUCCESS', '01', 'Txn Success', 'HDFC', '777001452707159', 'JPMorgan Chase Bank', NULL, 15, 15, '2021-01-21 00:36:08', '2021-01-20 19:06:08'),
(36, 1, 1, 'ORDS1584171405', 'MoCVfk40813527929102', '20210121111212800110168028403704931', 515, 'CC', 'INR', '2021-01-21 00:40:21', 'TXN_SUCCESS', '01', 'Txn Success', 'HDFC', '777001246185385', 'JPMorgan Chase Bank', NULL, 15, 15, '2021-01-21 00:36:46', '2021-01-20 19:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `prs_id` int(11) NOT NULL,
  `prs_name` varchar(200) NOT NULL,
  `prs_username` varchar(200) NOT NULL COMMENT 'unique(use for login)',
  `prs_email` varchar(200) NOT NULL COMMENT 'unique',
  `prs_whatsapp` varchar(20) NOT NULL,
  `prs_mob` varchar(200) NOT NULL COMMENT 'unique(use for login)',
  `prs_gender` int(11) NOT NULL,
  `prs_dob` date NOT NULL,
  `prs_password` text NOT NULL,
  `prs_gst` varchar(200) NOT NULL,
  `prs_bio` text NOT NULL,
  `cont_name` varchar(100) NOT NULL,
  `comp_name` varchar(100) NOT NULL,
  `prs_address` varchar(200) NOT NULL,
  `prs_city` varchar(200) NOT NULL,
  `prs_pincode` int(10) NOT NULL,
  `prs_dist` varchar(200) NOT NULL,
  `prs_state` varchar(200) NOT NULL,
  `prs_country` varchar(200) NOT NULL,
  `prs_landmark` varchar(100) NOT NULL,
  `prs_location` text NOT NULL,
  `prs_default_address` int(11) NOT NULL COMMENT 'person_addresses.pad_id',
  `prs_mob_verification` int(11) DEFAULT NULL,
  `prs_status` int(11) NOT NULL COMMENT '0-pending,1-active,2-inactive',
  `prs_crtd_by` int(11) NOT NULL,
  `prs_updt_by` int(11) NOT NULL,
  `prs_crtd_dt` datetime NOT NULL,
  `prs_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`prs_id`, `prs_name`, `prs_username`, `prs_email`, `prs_whatsapp`, `prs_mob`, `prs_gender`, `prs_dob`, `prs_password`, `prs_gst`, `prs_bio`, `cont_name`, `comp_name`, `prs_address`, `prs_city`, `prs_pincode`, `prs_dist`, `prs_state`, `prs_country`, `prs_landmark`, `prs_location`, `prs_default_address`, `prs_mob_verification`, `prs_status`, `prs_crtd_by`, `prs_updt_by`, `prs_crtd_dt`, `prs_updt_dt`) VALUES
(7, 'Vaishali Kasar', '', 'vaishalikasar13@gmail.com', '0', '8655385802', 0, '0000-00-00', '123456', '', '', '', '', '', '', 0, '', '', '', '', '', 4, 1, 1, 0, 0, '0000-00-00 00:00:00', '2020-09-24 09:33:28'),
(13, 'hema', '', 'hema@gmail.com', '0', '08080862532', 0, '0000-00-00', 'dXd3MC9jMWZIMEZaaFFCb2JWa2M3QT09', '', '', 'Hemakshi', 'Hetvi Interior', '', '', 0, '', '', '', '', '', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', '2020-10-19 13:22:52'),
(14, 'user1', '', 'user1@gmail.com', '2147483647', '08080862533', 0, '0000-00-00', 'dXd3MC9jMWZIMEZaaFFCb2JWa2M3QT09', '123456', '', 'Hemakshi', 'Hetvi Interior', 'dahisar', 'Mumbai', 400068, 'Mumbai', 'MAHARASHTRA', 'India', 'Gokul aanand', '', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', '2020-10-19 13:26:44'),
(15, 'Kishan', 'kishanzalariya', 'kishanzalariya7600@gmail.com', '63636363', '7600631363', 1, '1998-05-01', 'dHFiTndITnJOYmhxTTVvV09sOWt5UT09', '', 'I am kishan.', 'HT', 'MavenScript', 'Nirav Park', 'MR', 363641, 'Morbi', 'GJ', 'INDIA', '', '', 7, 1, 1, 15, 15, '0000-00-00 00:00:00', '2021-01-10 07:49:48'),
(19, 'Kishan', '', 'kisha@gmail.com', '9876543210', '09876543210', 0, '0000-00-00', 'dHFiTndITnJOYmhxTTVvV09sOWt5UT09', '', '', 'HT', 'MavenScript', 'Nirav Park', 'Morbi', 363641, 'Morbi', 'Gujarat', 'India', '', '', 8, 1, 1, 0, 0, '2020-12-17 17:46:09', '2020-12-17 17:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `person_addresses`
--

CREATE TABLE `person_addresses` (
  `pad_id` int(11) NOT NULL,
  `pad_prs_id` int(11) NOT NULL,
  `pad_address_type` int(11) NOT NULL,
  `pad_state` int(11) NOT NULL,
  `pad_name` varchar(200) NOT NULL,
  `pad_mobile` varchar(200) NOT NULL,
  `pad_alt_phone` varchar(200) NOT NULL,
  `pad_locality` varchar(200) NOT NULL,
  `pad_address` text NOT NULL,
  `pad_landmark` varchar(200) NOT NULL,
  `pad_pincode` varchar(50) NOT NULL,
  `pad_city` varchar(200) NOT NULL,
  `pad_status` int(11) NOT NULL,
  `pad_crtd_by` int(11) NOT NULL,
  `pad_updt_by` int(11) NOT NULL,
  `pad_crtd_dt` datetime NOT NULL,
  `pad_updt_dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_addresses`
--

INSERT INTO `person_addresses` (`pad_id`, `pad_prs_id`, `pad_address_type`, `pad_state`, `pad_name`, `pad_mobile`, `pad_alt_phone`, `pad_locality`, `pad_address`, `pad_landmark`, `pad_pincode`, `pad_city`, `pad_status`, `pad_crtd_by`, `pad_updt_by`, `pad_crtd_dt`, `pad_updt_dt`) VALUES
(1, 1, 1, 27, 'videsh kasar', '9967215433', '9967215433', '298 last stop', 'E wing, 702 Mandakini  building rawalpada dahisar(East)', 'MAHARASHTRA', '400068', 'mumbai', 1, 0, 0, '0000-00-00 00:00:00', '2018-12-10 18:45:22'),
(2, 3, 1, 27, 'stanley', '9686434732', '', 'kandiwali', 'kandiwali', '', '400067', 'mumbao', 1, 0, 0, '0000-00-00 00:00:00', '2018-12-13 18:18:43'),
(3, 5, 1, 27, 'Asmita', '9769729926', '', '298 last stop', 'E wing, 702 Mandakini  building rawalpada dahisar(East)', 'MAHARASHTRA', '400068', 'mumbai', 1, 0, 0, '0000-00-00 00:00:00', '2018-12-20 11:46:32'),
(4, 7, 1, 27, 'videsh kasar', '8655385802', '', '298 last stop', 'E wing, 702 Mandakini  building rawalpada dahisar(East)', 'MAHARASHTRA', '400068', 'mumbai', 1, 0, 0, '0000-00-00 00:00:00', '2019-01-13 01:09:19'),
(7, 15, 1, 24, 'kishan zalariya    ', '9876543210  ', '1111122222', 'Morbi    ', 'Nirav Park, Panchasar Road   ', '', '363641    ', 'MORBI', 1, 15, 15, '2020-12-01 19:31:03', '2020-12-01 19:32:53'),
(8, 19, 1, 24, 'Kishan', '09876543210', '', 'Nirav Park', 'Nirav Park', '', '363641', 'Morbi', 1, 19, 0, '2020-12-17 18:02:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `person_order`
--

CREATE TABLE `person_order` (
  `ord_id` int(11) NOT NULL,
  `ord_reference_no` text NOT NULL,
  `ord_source` int(11) NOT NULL COMMENT '1 = Web, 2 = Adhoc ',
  `ord_prs_id` int(11) NOT NULL,
  `ord_delivery_adddress` int(11) DEFAULT 0,
  `ord_payment_mode` int(11) NOT NULL,
  `ord_sub_total` double NOT NULL,
  `ord_discount` double NOT NULL,
  `ord_shipping_charges` double DEFAULT NULL,
  `ord_total_amt` float NOT NULL,
  `ord_payment_status` int(11) NOT NULL COMMENT '1=pending,2=paid',
  `ord_status` int(11) NOT NULL,
  `ord_remark` text DEFAULT NULL,
  `ord_is_success` int(11) NOT NULL COMMENT '0=Pending, 1=Success, 2=Failed',
  `ord_date` date NOT NULL,
  `ord_update_sent_mobile` varchar(100) NOT NULL,
  `ord_update_sent_email` varchar(100) NOT NULL,
  `ord_crtd_by` int(11) NOT NULL,
  `ord_crtd_dt` datetime NOT NULL,
  `ord_updt_by` int(11) NOT NULL,
  `ord_updt_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person_order_products`
--

CREATE TABLE `person_order_products` (
  `odp_id` int(11) NOT NULL,
  `odp_ord_id` int(11) NOT NULL,
  `odp_prd_id` int(11) NOT NULL,
  `odp_file` text NOT NULL,
  `odp_quantity` int(11) NOT NULL,
  `odp_height` double DEFAULT 0,
  `odp_width` double DEFAULT 0,
  `odp_amt` float NOT NULL,
  `odp_subtotal` double NOT NULL,
  `odp_disc_type` int(11) NOT NULL,
  `odp_disc_amt` double NOT NULL,
  `ord_shipping_charges` double NOT NULL,
  `odp_total_amt` double NOT NULL,
  `odp_flavour` int(11) NOT NULL,
  `odp_status` int(11) NOT NULL,
  `odp_date` date NOT NULL,
  `odp_invoice_number` bigint(20) NOT NULL,
  `odp_crtd_by` int(11) NOT NULL,
  `odp_updt_by` int(11) NOT NULL,
  `odp_crtd_dt` datetime NOT NULL,
  `ord_updt_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person_order_product_status`
--

CREATE TABLE `person_order_product_status` (
  `ops_id` int(11) NOT NULL,
  `ops_ord_id` int(11) NOT NULL COMMENT 'person_order.ord_id',
  `ops_prd_id` int(11) NOT NULL,
  `ops_status` int(11) NOT NULL COMMENT 'Placed=1, Processing=2, File_corrupted=3, Mismatch_quantity=4, Mismatch_quality=5, Is_in_printing=6, Order_is_ready=7, Dispatched=8, Delivered=9',
  `ops_remark` text NOT NULL,
  `ops_crtd_dt` datetime NOT NULL,
  `ops_crtd_by` int(11) NOT NULL,
  `ops_updt_by` int(11) NOT NULL,
  `ops_updt_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `pic_id` int(11) NOT NULL,
  `pic_title` varchar(100) NOT NULL,
  `pic_desc` varchar(100) NOT NULL,
  `pic_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prod`
--

CREATE TABLE `prod` (
  `pid` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `price` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prod`
--

INSERT INTO `prod` (`pid`, `name`, `price`) VALUES
(1, 'card', 120),
(2, 'envolope', 150);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prd_id` int(11) NOT NULL,
  `prd_menu` int(11) NOT NULL,
  `prd_slug` varchar(200) NOT NULL,
  `prd_name` text NOT NULL,
  `prd_cat_id` int(11) NOT NULL COMMENT 'category. cat_id',
  `prd_price` float NOT NULL,
  `prd_order` int(11) NOT NULL,
  `prd_status` int(11) NOT NULL COMMENT '0-inactive,1-active',
  `prd_datetime` datetime NOT NULL,
  `prd_crtd_by` int(11) NOT NULL,
  `prd_crtd_date` datetime NOT NULL,
  `prd_updt_by` int(11) NOT NULL,
  `prd_updt_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prd_id`, `prd_menu`, `prd_slug`, `prd_name`, `prd_cat_id`, `prd_price`, `prd_order`, `prd_status`, `prd_datetime`, `prd_crtd_by`, `prd_crtd_date`, `prd_updt_by`, `prd_updt_date`) VALUES
(27, 0, 'nt-1-side', 'NT 1 SIDE', 1, 261.99, 0, 1, '2020-10-03 17:09:38', 1, '2020-10-03 17:09:38', 1, '2020-12-01 17:50:42'),
(28, 1, 'nt-2-side', 'NT 2 SIDE', 1, 300.99, 2, 1, '2020-10-03 17:11:13', 1, '2020-10-03 17:11:13', 0, '2020-11-29 10:05:57'),
(29, 1, 'art-card-250-gsm-1-side', 'ART CARD (250 GSM) 1 SIDE ', 1, 160, 3, 1, '2020-10-03 17:12:37', 1, '2020-10-03 17:12:37', 0, '2020-10-03 11:42:37'),
(30, 1, 'art-card-250-gsm-2-side', 'ART CARD (250 GSM) 2 SIDE ', 1, 250, 4, 1, '2020-10-03 17:13:01', 1, '2020-10-03 17:13:01', 0, '2020-10-03 11:43:01'),
(31, 1, 'art-card-250-gsm-gloss-lami-2-side', 'ART CARD (250 GSM) + GLOSS LAMI. 2 SIDE ', 1, 260, 5, 1, '2020-10-03 17:13:19', 1, '2020-10-03 17:13:19', 0, '2020-10-03 11:43:19'),
(32, 1, 'matt-card-350-gsm-2-side', 'MATT CARD (350 GSM) 2 SIDE ', 1, 380, 6, 1, '2020-10-03 17:13:35', 1, '2020-10-03 17:13:35', 0, '2020-10-03 11:43:35'),
(33, 1, 'matt-2-side-crystal-uv-1-side-370-gsm', 'MATT 2 SIDE + CRYSTAL UV 1 SIDE (370 GSM) ', 1, 550, 7, 1, '2020-10-03 17:13:55', 1, '2020-10-03 17:13:55', 0, '2020-10-03 11:43:55'),
(34, 1, 'matt-2-side-crystal-uv-2-side-370-gsm', 'MATT 2 SIDE + CRYSTAL UV 2 SIDE (370 GSM) ', 1, 650, 8, 1, '2020-10-03 17:14:23', 1, '2020-10-03 17:14:23', 0, '2020-10-03 11:44:23'),
(35, 1, '80-gsm-sunshine-210-x-297-mm', '80 GSM SUNSHINE 210 x 297 mm', 2, 950, 1, 1, '2020-10-03 17:15:08', 1, '2020-10-03 17:15:08', 0, '2020-10-03 11:45:08'),
(36, 1, '100-gsm-sunshine-210-x-297-mm', '100 GSM SUNSHINE 210 x 297 mm', 2, 1050, 2, 1, '2020-10-03 17:15:38', 1, '2020-10-03 17:15:38', 0, '2020-10-03 11:45:38'),
(37, 0, '100-gsm-alabaster-210-x-297-mm', '100 GSM ALABASTER 210 x 297 mm', 2, 1100, 3, 1, '2020-10-03 17:15:55', 1, '2020-10-03 17:15:55', 1, '2020-10-15 12:45:43'),
(38, 1, '80-gsm-bond-paper-210-x-297-mm', '80 GSM BOND PAPER 210 x 297 mm', 2, 1350, 4, 1, '2020-10-03 17:16:18', 1, '2020-10-03 17:16:18', 0, '2020-10-03 11:46:18'),
(39, 1, '100-gsm-bond-paper-210-x-297-mm', '100 GSM BOND PAPER 210 x 297 mm', 2, 1550, 5, 1, '2020-10-03 17:16:36', 1, '2020-10-03 17:16:36', 0, '2020-10-03 11:46:36'),
(40, 1, '95-x-425-80-gsm-sunshine', '9.5 X 4.25 - 80 GSM SUNSHINE ', 3, 1150, 1, 1, '2020-10-03 17:17:09', 1, '2020-10-03 17:17:09', 0, '2020-10-03 11:47:09'),
(41, 1, '95-x-425-100-gsm-sunshine', '9.5 X 4.25 - 100 GSM SUNSHINE ', 3, 1250, 2, 1, '2020-10-03 17:17:36', 1, '2020-10-03 17:17:36', 0, '2020-10-03 11:47:36'),
(42, 1, '95-x-425-100-gsm-alabaster', '9.5 X 4.25 - 100 GSM ALABASTER', 3, 1300, 3, 1, '2020-10-03 17:17:55', 1, '2020-10-03 17:17:55', 0, '2020-10-03 11:47:55'),
(43, 1, '95-x-425-80-gsm-bond-paper', '9.5 X 4.25 - 80 GSM BOND PAPER', 3, 1550, 4, 1, '2020-10-03 17:18:11', 1, '2020-10-03 17:18:11', 0, '2020-10-03 11:48:11'),
(44, 1, '95-x-425-100-gsm-bond-paper', '9.5 X 4.25 - 100 GSM BOND PAPER', 3, 1750, 5, 1, '2020-10-03 17:18:27', 1, '2020-10-03 17:18:27', 0, '2020-10-03 11:48:27'),
(45, 1, '95-x-425-130-gsm-art-paper', '9.5 X 4.25 - 130 GSM ART PAPER', 3, 2000, 6, 1, '2020-10-03 17:18:44', 1, '2020-10-03 17:18:44', 1, '2020-10-03 12:07:25'),
(46, 1, 'sticker-without-lami-sq', 'STICKER WITHOUT LAMI. (SQ)', 4, 27, 1, 1, '2020-10-03 17:19:11', 1, '2020-10-03 17:19:11', 0, '2020-10-03 11:49:11'),
(47, 1, 'sticker-with-lami-sq', 'STICKER WITH LAMI. (SQ)', 4, 31, 2, 1, '2020-10-03 17:19:27', 1, '2020-10-03 17:19:27', 0, '2020-10-03 11:49:27'),
(48, 1, 'pvc-sticker-sq', 'PVC STICKER (SQ)', 4, 70, 3, 1, '2020-10-03 17:19:41', 1, '2020-10-03 17:19:41', 0, '2020-10-03 11:49:41'),
(49, 1, 'sticker-a4', 'STICKER - A4 ', 4, 2475, 4, 1, '2020-10-03 17:20:02', 1, '2020-10-03 17:20:02', 0, '2020-10-03 11:50:02'),
(50, 1, 'sticker-lami-a4', 'STICKER + LAMI. - A4 ', 4, 2925, 5, 1, '2020-10-03 17:20:17', 1, '2020-10-03 17:20:17', 0, '2020-10-03 11:50:17'),
(51, 1, 'sticker-11-x-17-inch-a3', 'STICKER - 11 x 17 inch (A3)', 4, 4500, 6, 1, '2020-10-03 17:20:42', 1, '2020-10-03 17:20:42', 0, '2020-10-03 11:50:42'),
(52, 1, 'sticker-lami-11-x-17-inch-a3', 'STICKER + Lami. - 11 x 17 inch (A3)', 4, 5000, 6, 1, '2020-10-03 17:20:56', 1, '2020-10-03 17:20:56', 0, '2020-10-03 11:50:56'),
(53, 1, '130-gsm-art-paper-single-side-a4', '130 GSM ART PAPER Single side (A4)', 5, 1250, 1, 1, '2020-10-03 17:21:20', 1, '2020-10-03 17:21:20', 0, '2020-10-03 11:51:20'),
(54, 1, '130-gsm-art-paper-front-back-a4', '130 GSM ART PAPER Front-back (A4)', 5, 1700, 2, 1, '2020-10-03 17:21:41', 1, '2020-10-03 17:21:41', 0, '2020-10-03 11:51:41'),
(55, 1, '170-gsm-art-paper-front-back-a4', '170 GSM ART PAPER Front-back (A4)', 5, 1950, 3, 1, '2020-10-03 17:21:55', 1, '2020-10-03 17:21:55', 0, '2020-10-03 11:51:55'),
(56, 1, '90-gsm-art-paper-front-back-2000-a4', '90 GSM ART PAPER Front-back 2000 (A4)', 5, 2300, 4, 1, '2020-10-03 17:22:11', 1, '2020-10-03 17:22:11', 0, '2020-10-03 11:52:11'),
(57, 1, '90-gsm-art-paper-front-back-5000-a4', '90 GSM ART PAPER Front-back 5000 (A4)', 5, 4400, 5, 1, '2020-10-03 17:22:34', 1, '2020-10-03 17:22:34', 0, '2020-10-03 11:52:34'),
(58, 1, '130-gsm-art-paper-front-back-2000-a4', '130 GSM ART PAPER Front-back 2000 (A4)', 5, 2850, 6, 1, '2020-10-03 17:22:51', 1, '2020-10-03 17:22:51', 0, '2020-10-03 11:52:51'),
(59, 1, '130-gsm-art-paper-front-back-5000-a4', '130 GSM ART PAPER Front-back 5000 (A4)', 5, 5450, 7, 1, '2020-10-03 17:23:08', 1, '2020-10-03 17:23:08', 0, '2020-10-03 11:53:08'),
(60, 1, '250-gsm-art-card-single-side-a4', '250 GSM ART CARD Single side (A4)', 6, 1850, 1, 1, '2020-10-03 17:23:26', 1, '2020-10-03 17:23:26', 0, '2020-10-03 11:53:26'),
(61, 1, '250-gsm-art-card-front-back-a4', '250 GSM ART CARD FRONT-BACK (A4)', 6, 2400, 2, 1, '2020-10-03 17:23:44', 1, '2020-10-03 17:23:44', 0, '2020-10-03 11:53:44'),
(62, 1, '250-gsm-art-card-front-back-gloss-lami-a4', '250 GSM ART CARD FRONT-BACK + Gloss LAMI. (A4)', 6, 2825, 3, 1, '2020-10-03 17:24:08', 1, '2020-10-03 17:24:08', 0, '2020-10-03 11:54:08'),
(63, 1, '350-gsm-art-card-front-back-matt-lami-a4', '350 GSM ART CARD FRONT-BACK + Matt LAMI. (A4)', 6, 4700, 4, 1, '2020-10-03 17:24:27', 1, '2020-10-03 17:24:27', 0, '2020-10-03 11:54:27'),
(64, 1, '250-gsm-art-card-single-side-a4', '250 GSM ART CARD Single side (A4)', 7, 21, 1, 1, '2020-10-03 17:24:44', 1, '2020-10-03 17:24:44', 0, '2020-10-03 11:54:44'),
(65, 1, '250-gsm-art-card-front-back-a4', '250 GSM ART CARD FRONT-BACK (A4)', 7, 28, 2, 1, '2020-10-03 17:25:16', 1, '2020-10-03 17:25:16', 0, '2020-10-03 11:55:16'),
(66, 1, '250-gsm-art-card-front-back-gloss-lami-a4', '250 GSM ART CARD FRONT-BACK + Gloss LAMI. (A4)', 7, 33, 3, 1, '2020-10-03 17:25:37', 1, '2020-10-03 17:25:37', 0, '2020-10-03 11:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL,
  `cat_menu` int(11) NOT NULL,
  `cat_slug` varchar(150) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_order` int(11) NOT NULL,
  `cat_status` int(11) NOT NULL COMMENT '0-inactive,1-active',
  `cat_crtd_by` int(11) NOT NULL,
  `cat_crtd_date` datetime NOT NULL,
  `cat_updt_by` int(11) NOT NULL,
  `cat_updt_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`cat_id`, `cat_menu`, `cat_slug`, `cat_name`, `cat_order`, `cat_status`, `cat_crtd_by`, `cat_crtd_date`, `cat_updt_by`, `cat_updt_date`) VALUES
(1, 1, 'visitng-card', 'VISITING CARD', 1, 1, 1, '2018-09-23 08:35:45', 0, '2018-09-23 03:05:45'),
(2, 1, 'letterhead', 'LETTERHEAD', 2, 1, 1, '2018-09-23 08:35:52', 1, '2018-09-23 03:05:52'),
(3, 1, 'envelope', 'ENVELOPE', 3, 1, 1, '2018-11-10 14:33:43', 0, '2018-11-10 09:03:43'),
(4, 1, 'sticker', 'STICKER', 4, 1, 1, '2018-12-17 16:19:32', 1, '2018-12-17 10:49:32'),
(5, 1, 'pamphlet-flyers', 'PAMPHLET / FLYERS', 5, 1, 1, '2018-12-17 16:31:45', 1, '2018-12-17 11:01:45'),
(6, 1, 'brochure', 'BROCHURE', 6, 1, 1, '2018-12-17 16:32:45', 1, '2018-12-17 11:02:45'),
(7, 1, 'brochure-mix', 'BROCHURE (Mix)', 7, 1, 1, '2020-10-02 15:01:39', 0, '2020-10-02 09:31:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `pid` int(200) NOT NULL,
  `pd_name` varchar(200) NOT NULL,
  `pd_price` varchar(200) NOT NULL,
  `pd_category` varchar(200) NOT NULL,
  `sub_category` varchar(200) DEFAULT NULL,
  `prd_status` int(11) NOT NULL COMMENT '0-inactive,1-active',
  `prd_order` int(11) NOT NULL,
  `pd_crt_date` datetime NOT NULL,
  `pd_up_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`pid`, `pd_name`, `pd_price`, `pd_category`, `sub_category`, `prd_status`, `prd_order`, `pd_crt_date`, `pd_up_date`) VALUES
(1, 'NT 1 SIDE', 'RS.260', 'Visiting Card', '', 1, 0, '0000-00-00 00:00:00', '2020-09-30 09:43:51'),
(2, 'NT 2 SIDE', 'RS.300', 'Visiting Card', NULL, 1, 0, '2020-09-22 14:43:00', '2020-09-30 09:43:55'),
(3, 'ART CARD (250 GSM) 1 SIDE ', 'RS.160', 'Visiting Card', NULL, 0, 0, '2020-09-30 14:11:02', '2020-09-30 08:41:18'),
(4, '80 GSM SUNSHINE 210 x 297 mm', 'RS.950', 'Letterhead', NULL, 0, 0, '0000-00-00 00:00:00', '2020-10-01 13:17:53'),
(5, '80 GSM SUNSHINE 210 x 297 mm', 'RS.1050', 'Letterhead', NULL, 0, 0, '0000-00-00 00:00:00', '2020-10-01 13:17:59');

-- --------------------------------------------------------

--
-- Table structure for table `product_flavor_images`
--

CREATE TABLE `product_flavor_images` (
  `pfi_id` int(11) NOT NULL,
  `pfi_prd_id` int(11) NOT NULL,
  `pfi_flv_id` int(11) NOT NULL,
  `pfi_img` text NOT NULL,
  `pfi_status` int(11) NOT NULL,
  `pfi_crtd_dt` int(11) NOT NULL,
  `pfi_crtd_by` int(11) NOT NULL,
  `pfi_updt_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `pfi_updt_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_flavours`
--

CREATE TABLE `product_flavours` (
  `flv_id` int(11) NOT NULL,
  `flv_slug` varchar(200) NOT NULL,
  `flv_name` varchar(100) NOT NULL,
  `flv_order` int(11) NOT NULL,
  `flv_crtd_by` int(11) NOT NULL,
  `flv_crtd_dt` datetime NOT NULL,
  `flv_updt_by` int(11) NOT NULL,
  `flv_updt_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_flavours`
--

INSERT INTO `product_flavours` (`flv_id`, `flv_slug`, `flv_name`, `flv_order`, `flv_crtd_by`, `flv_crtd_dt`, `flv_updt_by`, `flv_updt_date`) VALUES
(2, 'rich-chocolate', 'Dutch Chocolate', 1, 1, '2018-11-29 17:27:54', 1, '2018-11-29 11:57:54'),
(3, 'choco-cookie', 'Choco Cookie', 2, 1, '2018-11-29 17:33:19', 0, '2018-11-29 12:03:19'),
(4, 'caffe-mocha', 'Caffe Mocha', 3, 1, '2018-11-29 17:34:04', 0, '2018-11-29 12:04:04'),
(5, 'green-apple', 'Green Apple', 4, 1, '2018-11-29 17:36:50', 0, '2018-11-29 12:06:50'),
(6, 'lemon', 'Lemon', 5, 1, '2018-11-29 17:40:36', 0, '2018-11-29 12:10:36'),
(7, 'orange', 'Orange', 6, 1, '2018-11-29 17:41:12', 0, '2018-11-29 12:11:12'),
(8, 'blueberry', 'Blueberry ', 7, 1, '2018-11-29 17:45:05', 0, '2018-11-29 12:15:05'),
(9, 'unflavored', 'Unflavored', 8, 1, '2018-12-18 12:43:27', 0, '2018-12-18 07:13:27');

-- --------------------------------------------------------

--
-- Table structure for table `shop_brands`
--

CREATE TABLE `shop_brands` (
  `brd_id` int(11) NOT NULL,
  `brd_slug` varchar(200) NOT NULL,
  `brd_name` varchar(100) NOT NULL,
  `brd_order` int(11) NOT NULL,
  `brd_status` int(11) NOT NULL,
  `brd_crtd_by` int(11) NOT NULL,
  `brd_crtd_dt` datetime NOT NULL,
  `brd_updt_by` int(11) NOT NULL,
  `brd_updt_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state_master`
--

CREATE TABLE `state_master` (
  `stm_id` int(11) NOT NULL,
  `stm_name` varchar(255) NOT NULL,
  `stm_code` int(11) NOT NULL,
  `stm_crdt_by` int(11) NOT NULL,
  `stm_crdt_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `stm_updt_by` int(11) NOT NULL,
  `stm_updt_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `stm_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'gen_prm group = active_status '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state_master`
--

INSERT INTO `state_master` (`stm_id`, `stm_name`, `stm_code`, `stm_crdt_by`, `stm_crdt_dt`, `stm_updt_by`, `stm_updt_dt`, `stm_status`) VALUES
(1, 'JAMMU AND KASHMIR', 1, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:35', 1),
(2, 'HIMACHAL PRADESH', 2, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(3, 'PUNJAB', 3, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(4, 'CHANDIGARH', 4, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(5, 'UTTARAKHAND', 5, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(6, 'HARYANA', 6, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(7, 'DELHI', 7, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(8, 'RAJASTHAN', 8, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(9, 'UTTAR  PRADESH', 9, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(10, 'BIHAR', 10, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(11, 'SIKKIM', 11, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(12, 'ARUNACHAL PRADESH', 12, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(13, 'NAGALAND', 13, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(14, 'MANIPUR', 14, 1, '2017-11-20 00:00:00', 0, '2017-11-20 20:42:32', 1),
(15, 'MIZORAM', 15, 1, '2017-11-20 00:00:00', 0, '2017-11-20 20:42:22', 1),
(16, 'TRIPURA', 16, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(17, 'MEGHLAYA', 17, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(18, 'ASSAM', 18, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(19, 'WEST BENGAL', 19, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(20, 'JHARKHAND', 20, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(21, 'ODISHA', 21, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(22, 'CHATTISGARH', 22, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(23, 'MADHYA PRADESH', 23, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(24, 'GUJARAT', 24, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(25, 'DAMAN AND DIU', 25, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(26, 'DADRA AND NAGAR HAVELI', 26, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(27, 'MAHARASHTRA', 27, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(28, 'ANDHRA PRADESH(BEFORE DIVISION)', 28, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(29, 'KARNATAKA', 29, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(30, 'GOA', 30, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(31, 'LAKSHWADEEP', 31, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(32, 'KERALA', 32, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(33, 'TAMIL NADU', 33, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(34, 'PUDUCHERRY', 34, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(35, 'ANDAMAN AND NICOBAR ISLANDS', 35, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(36, 'TELANGANA', 36, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1),
(37, 'ANDHRA PRADESH (NEW)', 37, 1, '2017-11-20 00:00:00', 0, '2017-11-19 17:06:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu_master`
--

CREATE TABLE `sub_menu_master` (
  `sbm_id` int(11) NOT NULL,
  `sbm_mnu_id` int(11) NOT NULL,
  `sbm_name` varchar(100) NOT NULL,
  `sbm_pagelink` varchar(100) NOT NULL,
  `sbm_parent_id` int(11) NOT NULL,
  `sbm_order` int(11) NOT NULL,
  `sbm_group` varchar(50) NOT NULL,
  `sbm_status` varchar(5) NOT NULL DEFAULT 'y',
  `sbm_crtd_by` int(11) NOT NULL,
  `sbm_crtd_date` date NOT NULL,
  `sbm_updt_by` int(11) NOT NULL,
  `sbm_updt_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_menu_master`
--

INSERT INTO `sub_menu_master` (`sbm_id`, `sbm_mnu_id`, `sbm_name`, `sbm_pagelink`, `sbm_parent_id`, `sbm_order`, `sbm_group`, `sbm_status`, `sbm_crtd_by`, `sbm_crtd_date`, `sbm_updt_by`, `sbm_updt_date`) VALUES
(1, 2, 'Form Access', 'form_access', 2, 3, 'submenu', 'n', 1, '2020-12-22', 0, '2020-12-22 16:20:48'),
(11, 3, 'products', 'products', 3, 3, 'submenu', 'y', 1, '2020-12-22', 0, '2021-01-02 06:02:34'),
(12, 3, 'Flavours', 'product-flavours', 3, 2, 'submenu', 'n', 1, '2020-12-22', 0, '2020-12-22 16:20:51'),
(15, 3, 'Product Flavours Images', 'product/flavours', 3, 4, 'submenu', 'n', 1, '2020-12-22', 0, '2020-12-22 16:20:54'),
(18, 10, 'Orders', 'offline_order_list', 10, 1, 'submenu', 'y', 1, '2020-12-22', 0, '2021-01-02 06:09:34'),
(19, 10, 'Customers', 'offline_customers', 10, 2, 'submenu', 'y', 1, '2021-01-02', 0, '2021-01-02 06:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `temp_images`
--

CREATE TABLE `temp_images` (
  `timg_id` int(11) NOT NULL,
  `timg_reference_no` varchar(100) NOT NULL,
  `timg_type` varchar(50) NOT NULL COMMENT '1-product',
  `timg_path` varchar(100) NOT NULL,
  `timg_name` varchar(100) NOT NULL,
  `timg_crtd_date` datetime NOT NULL,
  `timg_crtd_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_images`
--

INSERT INTO `temp_images` (`timg_id`, `timg_reference_no`, `timg_type`, `timg_path`, `timg_name`, `timg_crtd_date`, `timg_crtd_by`) VALUES
(98, 'image-9044', '1', 'amino2.jpg', 'amino2.jpg', '2019-02-02 04:41:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `usr_id` int(11) NOT NULL,
  `usr_dpt_id` int(11) NOT NULL COMMENT '1= admin, 3= accountant',
  `usr_fullname` varchar(50) NOT NULL,
  `usr_username` varchar(100) NOT NULL,
  `usr_mobile` varchar(20) NOT NULL,
  `usr_email` varchar(200) NOT NULL,
  `usr_password` varchar(100) NOT NULL,
  `usr_status` int(11) NOT NULL,
  `usr_created_by` int(11) NOT NULL,
  `usr_update_by` int(11) NOT NULL,
  `usr_created_on` datetime NOT NULL,
  `usr_update_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`usr_id`, `usr_dpt_id`, `usr_fullname`, `usr_username`, `usr_mobile`, `usr_email`, `usr_password`, `usr_status`, `usr_created_by`, `usr_update_by`, `usr_created_on`, `usr_update_on`) VALUES
(1, 1, 'Prism Print', 'admin', '7600631363', 'kishanzalariya7600@gmail.com', 'Kww3B3O2/oFYMoD/1MrX7Q==', 1, 0, 0, '2020-10-17 15:37:31', '2020-10-17 15:37:31'),
(3, 3, 'Kishan Zalariya', 'accounter', '9876598756', 'accounter@gmail.com', 'Kww3B3O2/oFYMoD/1MrX7Q==', 1, 1, 1, '2020-12-25 14:57:47', '2020-12-25 14:57:47');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `prs_id` int(11) NOT NULL,
  `amount` double NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL,
  `active` int(11) DEFAULT 1,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `prs_id`, `amount`, `status`, `active`, `created_on`, `updated_on`) VALUES
(1, 15, 5000, 1, 1, '2020-11-02 10:33:16', '2021-01-21 23:46:07'),
(2, 13, 5000, 1, 1, '2020-11-21 14:09:51', '2020-11-21 14:09:51'),
(3, 19, 5000, 1, 1, '2020-12-17 17:46:10', '2020-12-19 06:11:12');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_history`
--

CREATE TABLE `wallet_history` (
  `id` int(11) NOT NULL,
  `prs_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `odp_ord_id` varchar(30) DEFAULT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=Credit, 2=Debit',
  `transaction_mode` int(11) NOT NULL COMMENT '1=Bank, 2=Cash, 3=Pay From Balance, 4= Payment gateway',
  `amount` double NOT NULL,
  `closing_balance` double NOT NULL,
  `description` text NOT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bsn_prm`
--
ALTER TABLE `bsn_prm`
  ADD PRIMARY KEY (`bpm_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `ci_sessions_web`
--
ALTER TABLE `ci_sessions_web`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `com_events`
--
ALTER TABLE `com_events`
  ADD PRIMARY KEY (`ent_id`);

--
-- Indexes for table `com_events_usr`
--
ALTER TABLE `com_events_usr`
  ADD PRIMARY KEY (`eus_id`),
  ADD KEY `eus_ent_id` (`eus_ent_id`),
  ADD KEY `eus_tmp_id` (`eus_tmp_id`),
  ADD KEY `eus_ent_id_2` (`eus_ent_id`),
  ADD KEY `eus_tmp_id_2` (`eus_tmp_id`);

--
-- Indexes for table `com_template`
--
ALTER TABLE `com_template`
  ADD PRIMARY KEY (`tmp_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`CON_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dpt_id`);

--
-- Indexes for table `department_menu_access`
--
ALTER TABLE `department_menu_access`
  ADD PRIMARY KEY (`dma_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`evt_id`);

--
-- Indexes for table `featured_products`
--
ALTER TABLE `featured_products`
  ADD PRIMARY KEY (`fp_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`fdk_id`);

--
-- Indexes for table `forgot_password_transaction`
--
ALTER TABLE `forgot_password_transaction`
  ADD PRIMARY KEY (`fpt_id`);

--
-- Indexes for table `form_access`
--
ALTER TABLE `form_access`
  ADD PRIMARY KEY (`fma_id`);

--
-- Indexes for table `gallery_set`
--
ALTER TABLE `gallery_set`
  ADD PRIMARY KEY (`gls_id`);

--
-- Indexes for table `gen_prm`
--
ALTER TABLE `gen_prm`
  ADD PRIMARY KEY (`gnp_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `menu_master`
--
ALTER TABLE `menu_master`
  ADD PRIMARY KEY (`mnu_id`);

--
-- Indexes for table `menu_transaction`
--
ALTER TABLE `menu_transaction`
  ADD PRIMARY KEY (`mtr_id`);

--
-- Indexes for table `offline_order`
--
ALTER TABLE `offline_order`
  ADD PRIMARY KEY (`offline_order_id`);

--
-- Indexes for table `offline_order_product`
--
ALTER TABLE `offline_order_product`
  ADD PRIMARY KEY (`oop_id`);

--
-- Indexes for table `offline_users`
--
ALTER TABLE `offline_users`
  ADD PRIMARY KEY (`offline_user_id`);

--
-- Indexes for table `otp_transaction`
--
ALTER TABLE `otp_transaction`
  ADD PRIMARY KEY (`otp_id`);

--
-- Indexes for table `payment_rcpt`
--
ALTER TABLE `payment_rcpt`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `paytm_transactions`
--
ALTER TABLE `paytm_transactions`
  ADD PRIMARY KEY (`pyt_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`prs_id`);

--
-- Indexes for table `person_addresses`
--
ALTER TABLE `person_addresses`
  ADD PRIMARY KEY (`pad_id`);

--
-- Indexes for table `person_order`
--
ALTER TABLE `person_order`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `person_order_products`
--
ALTER TABLE `person_order_products`
  ADD PRIMARY KEY (`odp_id`);

--
-- Indexes for table `person_order_product_status`
--
ALTER TABLE `person_order_product_status`
  ADD PRIMARY KEY (`ops_id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `prod`
--
ALTER TABLE `prod`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prd_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `product_flavor_images`
--
ALTER TABLE `product_flavor_images`
  ADD PRIMARY KEY (`pfi_id`);

--
-- Indexes for table `product_flavours`
--
ALTER TABLE `product_flavours`
  ADD PRIMARY KEY (`flv_id`);

--
-- Indexes for table `shop_brands`
--
ALTER TABLE `shop_brands`
  ADD PRIMARY KEY (`brd_id`);

--
-- Indexes for table `state_master`
--
ALTER TABLE `state_master`
  ADD PRIMARY KEY (`stm_id`);

--
-- Indexes for table `sub_menu_master`
--
ALTER TABLE `sub_menu_master`
  ADD PRIMARY KEY (`sbm_id`);

--
-- Indexes for table `temp_images`
--
ALTER TABLE `temp_images`
  ADD PRIMARY KEY (`timg_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`usr_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_history`
--
ALTER TABLE `wallet_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `CON_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dpt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `department_menu_access`
--
ALTER TABLE `department_menu_access`
  MODIFY `dma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `featured_products`
--
ALTER TABLE `featured_products`
  MODIFY `fp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `fdk_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forgot_password_transaction`
--
ALTER TABLE `forgot_password_transaction`
  MODIFY `fpt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `form_access`
--
ALTER TABLE `form_access`
  MODIFY `fma_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `gallery_set`
--
ALTER TABLE `gallery_set`
  MODIFY `gls_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `gen_prm`
--
ALTER TABLE `gen_prm`
  MODIFY `gnp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `loc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_master`
--
ALTER TABLE `menu_master`
  MODIFY `mnu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `menu_transaction`
--
ALTER TABLE `menu_transaction`
  MODIFY `mtr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `offline_order`
--
ALTER TABLE `offline_order`
  MODIFY `offline_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `offline_order_product`
--
ALTER TABLE `offline_order_product`
  MODIFY `oop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `offline_users`
--
ALTER TABLE `offline_users`
  MODIFY `offline_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `otp_transaction`
--
ALTER TABLE `otp_transaction`
  MODIFY `otp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_rcpt`
--
ALTER TABLE `payment_rcpt`
  MODIFY `ID` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `paytm_transactions`
--
ALTER TABLE `paytm_transactions`
  MODIFY `pyt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `prs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `person_addresses`
--
ALTER TABLE `person_addresses`
  MODIFY `pad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `person_order`
--
ALTER TABLE `person_order`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `person_order_products`
--
ALTER TABLE `person_order_products`
  MODIFY `odp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `person_order_product_status`
--
ALTER TABLE `person_order_product_status`
  MODIFY `ops_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prod`
--
ALTER TABLE `prod`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `pid` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_flavor_images`
--
ALTER TABLE `product_flavor_images`
  MODIFY `pfi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_flavours`
--
ALTER TABLE `product_flavours`
  MODIFY `flv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shop_brands`
--
ALTER TABLE `shop_brands`
  MODIFY `brd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state_master`
--
ALTER TABLE `state_master`
  MODIFY `stm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `sub_menu_master`
--
ALTER TABLE `sub_menu_master`
  MODIFY `sbm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `temp_images`
--
ALTER TABLE `temp_images`
  MODIFY `timg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wallet_history`
--
ALTER TABLE `wallet_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
