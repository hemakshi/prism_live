<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	controller name always in small latter,

*/

class gallery extends CI_Controller {
	/*
	for load any model we use __construct function

	*/

		public function __construct(){
         parent::__construct();
         $this->load->model('gallery_model');
    }
        public function galleryList()
        {
        	$data['gallery_list']=$this->gallery_model->getGalleryList();
        	$this->load->view('gallery_list',$data);
        }

        public function galleryAdd()
        {
            $this->load->view('add_gallery_images');

        }

        


      public function uploadImages()
  {

 

  if (isset($_FILES['files']) && !empty($_FILES['files'])) 
  { 
    //global $project_documents_path;
    $no_files = count($_FILES["files"]['name']);
    $i=0;
    $p=1;
    $count=1;
    $uploaded_documents='';
    $failed_documents='';
    $gal_title = explode(',',$this->input->post("gal_title"));
    for ($i = 0; $i < $no_files; $i++) 
    {
        $uploaded_documents.= $_FILES['files']['name'][$i].'</br>';
         if (file_exists(GALLERY_IMAGES_PATH.$_FILES['files']['name'][$i])) 
         {
            $_FILES["files"]["name"][$i]=$this->home_model->getFileName($_FILES['files']['name'][$i]);
         }
         else
         {
            $_FILES["files"]["name"][$i]=$_FILES["files"]["name"][$i];
         }
        move_uploaded_file($_FILES["files"]["tmp_name"][$i],GALLERY_IMAGES_PATH .$_FILES["files"]["name"][$i]);
                           // echo 'File successfully uploaded : uploads/' . $_FILES['files']['name'][$i] . ' ';
      //   if ($_FILES['files']['size'][$i] > 15728640)
      // { 
         copy(GALLERY_IMAGES_PATH.$_FILES["files"]["name"][$i], PATH_TO_STORE_FILE_RESIZE.$_FILES["files"]["name"][$i]);
         $data = getimagesize(GALLERY_IMAGES_PATH.$_FILES['files']['name'][$i]);
                    $width = $data[0];
                    $height = $data[1];
                    if ($width > WIDTH_SPECIFIED_BY_USER)
                    {
                      $image_height =(WIDTH_SPECIFIED_BY_USER * $height) / $width;
                      $config['image_library'] = 'gd2';
                      $config['source_image'] =GALLERY_IMAGES_PATH.$_FILES['files']['name'][$i]; //get original image
                      //$config['create_thumb'] = TRUE; 
                      $config['maintain_ratio'] = TRUE;
                      $config['width'] = WIDTH_SPECIFIED_BY_USER;
                      $config['height'] = $image_height;
                      $this->image_lib->initialize($config);
                      if (!$this->image_lib->resize()) {
                          $this->handle_error($this->image_lib->display_errors());
                          return false;
                      }
                      $this->image_lib->clear();
                    }
      // }
         $small_img=$_FILES['files']['name'][$i];
         $gal_small_img_name=$this->home_model->convertSmallFileName($small_img);
         copy(GALLERY_IMAGES_PATH.$_FILES["files"]["name"][$i],GALLERY_SMALL_IMAGES_PATH.$gal_small_img_name);

         $data1 = getimagesize(GALLERY_SMALL_IMAGES_PATH.$gal_small_img_name);
                    $width_sml = $data1[0];
                    $height_sml = $data1[1];
                    if ($width_sml > WIDTH_SPECIFIED_BY_USER_FOR_SMALL_IMG)
                    {
                      $image_height_sml =(WIDTH_SPECIFIED_BY_USER_FOR_SMALL_IMG * $height_sml) / $width_sml;
                      $config['image_library'] = 'gd2';
                      $config['source_image'] =GALLERY_SMALL_IMAGES_PATH.$gal_small_img_name; //get original image
                      //$config['create_thumb'] = TRUE; 
                      $config['maintain_ratio'] = TRUE;
                      $config['width'] = WIDTH_SPECIFIED_BY_USER_FOR_SMALL_IMG;
                      $config['height'] = $image_height_sml;
                      $this->image_lib->initialize($config);
                      if (!$this->image_lib->resize()) {
                          $this->handle_error($this->image_lib->display_errors());
                          return false;
                      }
                      $this->image_lib->clear();
                    }

         $data = array(
                    'gal_name' =>  $_FILES['files']['name'][$i],
                    'gal_tittle' =>  $gal_title[$i],
                    'gal_small_img_name'=>$gal_small_img_name,
                    'gal_date_time' =>date('Y-m-d h:i:s'),
                    'gal_crtd_dt' =>date('Y-m-d h:i:s'),
                    'gal_crtd_by'=>$this->session->userdata('hnv_prs_id')
                     );
              $this->db->insert('gallery_images', $data);
          $count++;
    }
        if($p==1)
        {
          echo json_encode(array("success"=>true,"message"=>"Images uploaded successfully","linkn"=>base_url().'gallery-images-list'));
        }
        // elseif($p==2)
        // {
        //   echo json_encode(array("success"=>false,"message"=>"some error occurs..."));
        // }
      }
      else {
        echo 'Please choose at least one file';
      }
  }
//end
public function deleteImages()
  {
    $gal_id = $this->input->post('gal_id');
      if ($gal_id != '') {
       $id = $this->gallery_model->delete_Image($gal_id);     

       if ($id==1) {         
          echo json_encode(array('success'=>true,'message'=>'Image deleted succesfully'));
        } else {
          echo json_encode(array('success'=>false,'message'=>'Some Error Occured'));
        }
      }else {
        echo json_encode(array('success'=>false,'message'=>'Image is empty'));
      }
  }
         
 }