<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('shop_model');
		$this->load->model('product_model');
		$this->load->model('order_model');
		$this->load->model('wallet_model');
		$this->load->model('offline_model');
		// Load cart library
		$this->load->library('cart');
		$this->load->helper(array('cookie', 'url'));
	}
	public function productCatView($value = '')
	{
		$data['category'] = $this->product_model->getProductCategories();
		$data['menu'] = $this->home_model->getDataGpmData('menu');
		$this->load->view('product_cat_list', $data);
	}
	public function getProdStatus()
	{
		echo json_encode($this->product_model->getProdStatus());
	}

	public function insertProductCategory($value = '')
	{
		if ($this->input->post('cat_id') == "") {
			$string = $this->product_model->insertProductCategory();
			if ($string != -1) {
				echo json_encode(array("success" => true, "message" => 'Category has been added successfully.'));
			} else {
				echo json_encode(array("success" => false, "message" => "some error has occured..."));
			}
		} else {

			$string = $this->product_model->updateProductCategory();
			if ($string) {
				echo json_encode(array("success" => true, "message" => "Category has been updated successfully."));
			} else {
				echo json_encode(array("success" => false, "message" => "some error has occured..."));
			}
		}
	}
	public function deleteProductCategory()
	{
		if ($this->input->post('cat_id') != "") {
			$result = $this->product_model->deleteProductCategory();
			if ($result) {
				echo json_encode(array("success" => true, "message" => 'Category has been deleted successfully.'));
			} else {
				echo json_encode(array("success" => false, "message" => "some error has occured..."));
			}
		}
	}
	public function editProductCat($value = '')
	{

		$sql = "SELECT cat_id,cat_name, cat_order,cat_menu FROM `product_category` where cat_id=" . $this->input->post('cat_id') . "";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			foreach ($row as $kk => $vv) {
				echo $vv . "##";
			}
		}
	}

	public function receiptView()
	{
		$data['paymentReceiptData'] = $this->home_model->getReceipt();
		$this->load->view('payment_receipt', $data);
	}

	public function ledgerView()
	{
		$data['ledgerData'] = $this->home_model->getAllWalletHistory();
		$this->load->view('ledger', $data);
	}

	public function offlineView()
	{
		$this->load->view('offline_order');
	}
	public function paytmTransaction()
	{
		$data['paytmTransactions'] = $this->home_model->getAllPaytmTransactions();
		$this->load->view('paytm_transactions', $data);
	}

	public function userDetails()
	{
		$data['user'] = $this->home_model->getUserDetails();
		$this->load->view('cust-details', $data);
	}
	public function productsView($value = '')
	{
		$data['products'] = $this->product_model->getProducts();
		$this->load->view('product_list', $data);
	}
	public function productAddView($value = '')
	{
		$data['flavours'] = $this->product_model->getAllFlavour();
		$data['menu'] = $this->home_model->getDataGpmData('menu');
		$this->load->view('product_add', $data);
	}
	public function uploadProductImages($value = '')
	{
		if (isset($_FILES['files']) && !empty($_FILES['files'])) {
			$timg_reference_no = $this->input->post('timg_reference_no');
			if ($timg_reference_no == '') {
				$timg_reference_no = 'image-' . $this->home_model->generateRandomStringNum(4);
			} else {
				$timg_reference_no = $timg_reference_no;
			}

			$no_files = count($_FILES["files"]['name']);
			$i = 0;
			$p = 1;
			$count = 1;
			$uploaded_documents = '';
			$failed_documents = '';
			$validextensions = array("jpeg", "jpg", "png", "JPG", "PNG");
			$document_input = explode(',', $this->input->post("document_input"));
			for ($i = 0; $i < $no_files; $i++) {
				if ($document_input[$i] == 1) {
					$temporary = explode(".", $_FILES['files']['name'][$i]);
					$file_extension = end($temporary);
					if ($_FILES['files']['size'][$i] > 5120000 || !in_array($file_extension, $validextensions)) {


						$p = 2;
						$failed_documents .= $_FILES['files']['name'][$i] . '</br>';
					} else {

						$uploaded_documents .= $_FILES['files']['name'][$i] . '</br>';
						if (file_exists(PRODUCT_BIG_IMAGE_PATH . $_FILES['files']['name'][$i])) {
							$_FILES["files"]["name"][$i] = $this->home_model->getFileName($_FILES['files']['name'][$i]);
							if (file_exists(PRODUCT_BIG_IMAGE_PATH . $_FILES["files"]["name"][$i])) {
								$_FILES["files"]["name"][$i] = $this->home_model->getFileName($_FILES['files']['name'][$i]);
							} else {
								$_FILES["files"]["name"][$i] = $_FILES["files"]["name"][$i];
							}
						} else {
							$_FILES["files"]["name"][$i] = $_FILES["files"]["name"][$i];
						}

						move_uploaded_file($_FILES["files"]["tmp_name"][$i], PRODUCT_BIG_IMAGE_PATH . $_FILES["files"]["name"][$i]);

						copy(PRODUCT_BIG_IMAGE_PATH . $_FILES["files"]["name"][$i], PRODUCT_SMALL_IMAGE_PATH . $_FILES["files"]["name"][$i]);
						copy(PRODUCT_BIG_IMAGE_PATH . $_FILES["files"]["name"][$i], PRODUCT_LARGE_IMAGE_PATH . $_FILES["files"]["name"][$i]);
						$data = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES['files']['name'][$i]);
						$width = $data[0];
						$height = $data[1];
						$image_height = (PRODUCT_WIDTH_BIG * $height) / $width;
						//CONTROLLING IMAGE WIDTH Large

						$config['image_library'] = 'gd2';
						$config['source_image'] = PRODUCT_BIG_IMAGE_PATH . $_FILES['files']['name'][$i]; //get original image
						//$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;

						$config['width'] = PRODUCT_WIDTH_BIG;
						$config['height'] = $image_height;
						$this->image_lib->initialize($config);
						if (!$this->image_lib->resize()) {
							$this->handle_error($this->image_lib->display_errors());
						}
						//END OF CONTROLLING IMAGE WIDTH CODE

						//CONTROLLING IMAGE WIDTH SMall
						$data_small_img = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES['files']['name'][$i]);
						$width_small = $data_small_img[0];
						$height_small = $data_small_img[1];

						$image_height_small = (PRODUCT_WIDTH_SMALL * $height) / $width;
						$config['image_library'] = 'gd2';
						$config['source_image'] = PRODUCT_SMALL_IMAGE_PATH . $_FILES['files']['name'][$i]; //get original image
						//$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;

						$config['width'] = PRODUCT_WIDTH_SMALL;
						$config['height'] = $image_height_small;
						$this->image_lib->initialize($config);
						if (!$this->image_lib->resize()) {
							$this->handle_error($this->image_lib->display_errors());
						}

						//CONTROLLING IMAGE WIDTH LARGE
						$data_large_img = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES['files']['name'][$i]);
						$width_large = $data_large_img[0];
						$height_large = $data_large_img[1];

						$image_height_large = (PRODUCT_WIDTH_LARGE * $height) / $width;
						$config['image_library'] = 'gd2';
						$config['source_image'] = PRODUCT_LARGE_IMAGE_PATH . $_FILES['files']['name'][$i]; //get original image
						//$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;

						$config['width'] = PRODUCT_WIDTH_LARGE;
						$config['height'] = $image_height_large;
						$this->image_lib->initialize($config);
						if (!$this->image_lib->resize()) {
							$this->handle_error($this->image_lib->display_errors());
						}



						$data = array(
							'timg_reference_no' => $timg_reference_no,
							'timg_name' =>  $_FILES["files"]["name"][$i],
							'timg_type' => PRODUCT_IMG_TYPE,
							'timg_path' => $_FILES["files"]["name"][$i],
							'timg_crtd_date' => date('Y-m-d h:i:s'),
							'timg_crtd_by' => $this->session->userdata('hnv_prs_id')
						);

						$this->db->insert('temp_images', $data);
					}
					$count++;
				}
			}

			if ($p == 1) {

				echo json_encode(array("success" => true, "message" => $uploaded_documents, 'timg_reference_no' => $timg_reference_no));
			} elseif ($p == 2) {
				echo json_encode(array("success" => false, "message" => $failed_documents, "uploaded_documents" => $uploaded_documents, 'timg_reference_no' => $timg_reference_no));
			}
		} else {
			echo 'Please choose at least one file';
		}
	}
	public function insertProductData($value = '')
	{

		$this->db->trans_start();
		// $images=$this->home_model->getTempImg(PRODUCT_IMG_TYPE,$this->input->post('timg_reference_no'));
		// if(!empty( $images))
		// {
		$prd_id = $this->product_model->insertProduct();
		if ($prd_id != -1) {
			// $string=$this->home_model->insertImages($images,$prd_id,$this->input->post('timg_reference_no'),$this->input->post('prd_name'));

			echo json_encode(array("success" => true, "message" => 'Your product has been added successfully.', "linkn" => base_url() . 'products'));
		} else {
			log_message('error', 'error occured while inserting product >> product/insertProduct');

			echo json_encode(array("success" => false, "message" => ERROR_MSG));
		}
		// }

		// else
		// {

		// 	echo json_encode(array("success"=>false,"message"=>"Please select product images."));

		// }
		$this->db->trans_complete();
	}
	public function updateProductData($value = '')
	{
		$this->db->trans_start();
		// $images=$this->home_model->getTempImg(PRODUCT_IMG_TYPE,$this->input->post('timg_reference_no'));

		$string = $this->product_model->updateProduct();
		if ($string) {
			// if(!empty( $images))
			// {
			// 	$string=$this->home_model->insertImages($images, $this->input->post('prd_id'),$this->input->post('timg_reference_no'),$this->input->post('prd_name'));
			// }

			echo json_encode(array("success" => true, "message" => 'Your product has been updated successfully.', "linkn" => base_url() . 'products'));
		} else {
			log_message('error', 'error occured while inserting product >> product/updateProduct');

			echo json_encode(array("success" => false, "message" => ERROR_MSG));
		}
		$this->db->trans_complete();
	}

	public function delete()
	{
		$this->load->model('product_model');

		$id = $this->input->get('prd_id');

		if ($this->product_model->deleteuser($id)) {
			$data['data'] = $this->product_model->getuser();
			$this->load->view('product_list' . $data);
		}
	}

	public function deleteProductById()
	{
		$id = $this->input->post('prd_id');
		$dataDelete = $this->product_model->deleteData('products', array('prd_id' => $id));
		if ($dataDelete == true) {
			echo json_encode(array("success" => true, "message" => 'Product has been deleted successfully.'));
		} else {
			echo json_encode(array("success" => false, "message" => "some error has occured..."));
		}
	}



	public function productEditView($prd_slug)
	{
		$data['product'] = $this->product_model->getProductDataBySlug($prd_slug);
		$data['menu'] = $this->home_model->getDataGpmData('menu');
		// $data['brands']=$this->shop_model->getAllBrands();
		$data['category'] = $this->product_model->getProductCategories($data['product']->prd_menu);
		$this->load->view('product_edit', $data);
	}
	public function productImageEditView($prd_slug)
	{
		$data['product'] = $this->product_model->getProductDataBySlug($prd_slug);
		$data['product_img'] = $this->product_model->getProductImagesBySlug($prd_slug);
		$this->load->view('product_images_edit', $data);
	}
	public function deleteProductImage($value = '')
	{

		$string = $this->product_model->deleteProductImage();
		if ($string != '') {

			unlink(PRODUCT_BIG_IMAGE_PATH . $this->input->post('img_name'));
			unlink(PRODUCT_SMALL_IMAGE_PATH . $this->input->post('img_name'));
			echo json_encode(array("success" => true, "message" => "Image has been removed from product successfully."));
		} else {
			log_message('error', 'error occured while deleting product image >> product/deleteProductImage');
			global $error_message;
			echo json_encode(array("success" => false, "message" => $error_message));
		}
	}
	public function makeFeatured()
	{
		$pra_value = $this->product_model->insertMakeFeatured();

		if ($pra_value == 1) {

			echo json_encode(array("success" => true, "message" => "Product has been made as featured successfully."));
		} else {
			echo json_encode(array("success" => false, "message" => "Some error Occurs."));
		}
	}
	public function removeMakeFeatured()
	{

		$pra_value = $this->product_model->removeMakeFeatured();

		if ($pra_value == 1) {

			echo json_encode(array("success" => true, "message" => "Selected product has been removed from featured category successfully."));
		} else {
			echo json_encode(array("success" => false, "message" => "Some error Occurs."));
		}
	}
	public function getPrdName($prd_name)
	{
		log_message('error', '>> getPrdName controller prd_name =' . $prd_name);
		$product = $this->product_model->getProductName($prd_name);
		$prd_name = array();
		foreach ($product as $key) {
			$prd_name[] = $key->prd_name;
		}

		echo json_encode($prd_name);
	}

	public function flavourListView($value = '')
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			$data['flavours'] = $this->product_model->getAllFlavour();
			$this->load->view('flavour_list', $data);
		} else {
			redirect('', 'refresh');
		}
	}
	public function insertFlavour($value = '')
	{
		if ($this->input->post('flv_id') == "") {
			$string = $this->product_model->insertFlavour();
			if ($string != -1) {
				echo json_encode(array("success" => true, "message" => 'Flavour has been added successfully.'));
			} else {
				echo json_encode(array("success" => false, "message" => "some error has occured..."));
			}
		} else {

			$string = $this->product_model->updateFlavour();
			if ($string) {
				echo json_encode(array("success" => true, "message" => "Flavour has been updated successfully."));
			} else {
				echo json_encode(array("success" => false, "message" => "some error has occured..."));
			}
		}
	}
	public function editFlavour($value = '')
	{
		$sql = "SELECT   `flv_id`, `flv_name`, `flv_order` FROM `product_flavours` where flv_id=" . $this->input->post('flv_id') . "";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			foreach ($row as $kk => $vv) {
				echo $vv . "##";
			}
		}
	}
	public function deleteFlavour($value = '')
	{

		$string = $this->product_model->deleteFlavour();
		if ($string) {

			unlink(Flavour_PATH . $this->input->post('flv_old_img'));
			echo json_encode(array("success" => true, "message" => 'Image has been deleted successfully.'));
		} else {
			echo json_encode(array("success" => false, "message" => "some error has occured..."));
		}
	}
	public function productFlavourListView($value = '')
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			$data['product_flavours'] = $this->product_model->getProductFlavours();
			$data['products'] = $this->product_model->getProducts();
			$this->load->view('product_flavour_list', $data);
		} else {
			redirect('', 'refresh');
		}
	}
	public function ajaxCallFlavours()
	{
		$flavours = $this->product_model->getAssignedPrdFlavours($this->input->post('pfi_prd_id'));
		$res = $this->product_model->getPrdFlavours($flavours->prd_flv_id, $this->input->post('pfi_prd_id'));
		print $this->ajaxCombo1($res);
	}
	function ajaxCombo1($res)
	{
		$str = "<option value=''>-- Please Select --</option>";
		foreach ($res as $row) {
			$str .= "<option value='" . $row->flv_id . "'>" . $row->flv_name . "</option>";
		}
		print $str;
	}
	public function insertPrdFlavourImg($value = '')
	{
		if ($this->input->post('pfi_id') == "") {

			$string = $this->product_model->insertPrdFlavourImg();
			if ($string == 'error') {
				echo json_encode(array("success" => false, "message" => 'some error occured.Try again'));
			} elseif ($string == 'size') {
				echo json_encode(array("success" => false, "message" => 'Invallid Size'));
			} elseif ($string == 'select_img') {
				echo json_encode(array("success" => false, "message" => 'Select  Image.'));
			} elseif ($string == 'true') {
				echo json_encode(array("success" => true, "message" => 'Product flavour has been added successfully'));
			} else {
				echo json_encode(array("success" => false, "message" => 'some error occured'));
			}
		} else {

			$string = $this->product_model->updatePrdFlavourImg();
			if ($string == 'error') {
				echo json_encode(array("success" => false, "message" => 'some error occured.Try again'));
			} elseif ($string == 'size') {
				echo json_encode(array("success" => false, "message" => 'Invallid Size'));
			} elseif ($string == 'select_img') {
				echo json_encode(array("success" => false, "message" => 'Select Flavour Image.'));
			} elseif ($string == 'true') {
				echo json_encode(array("success" => true, "message" => 'Product flavour  has been updated successfully'));
			} else {
				echo json_encode(array("success" => false, "message" => 'some error occured'));
			}
		}
	}
	public function editPrdFlv($value = '')
	{

		$sql = "SELECT pfi_id,(select products.prd_name from products where products.prd_id= product_flavor_images.pfi_prd_id) prd_name,(select product_flavours.flv_name from product_flavours where product_flavours.flv_id= product_flavor_images.pfi_flv_id) flavour, pfi_img FROM `product_flavor_images` where pfi_id=" . $this->input->post('pfi_id') . "";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			foreach ($row as $kk => $vv) {
				echo $vv . "##";
			}
		}
	}
	public function ajaxCallCategories()
	{
		$res = $this->product_model->getProductCategories($this->input->post('menu'));
		print $this->ajaxCombo2($res);
	}
	function ajaxCombo2($res)
	{
		$str = "<option value=''>-- Please Select --</option>";
		foreach ($res as $row) {
			$str .= "<option value='" . $row->cat_id . "'>" . $row->cat_name . "</option>";
		}
		print $str;
	}

	public function updateSingleProductShippingCharges()
	{
		$orderId = $this->input->post('order_id');
		$orderProductId = $this->input->post('odp_id');
		$shippingCharges = $this->input->post('product_shipping_charges');
		$orderData = json_decode(json_encode($this->order_model->getOrderDataById($orderId)), true);
		if ($orderData != false) {
			$orderProductData = json_decode(json_encode($this->product_model->getSingleProductById($orderProductId)), true);
			if ($orderProductData != false) {
				// echo var_dump($orderProductData);
				$this->load->model('communication_model');
				$updateSingleProductShippingChargesResult = $this->product_model->updateSingleProductShippingCharges($orderData, $orderProductData);
				if ($updateSingleProductShippingChargesResult) {
					$walletData = json_decode(json_encode($this->home_model->getWalletDataByPersonId($orderData['ord_prs_id'])), true);
					$deducChargesResult = $this->wallet_model->deductChargesFromWalletBalance($shippingCharges, $walletData[0], $orderData);
					if ($deducChargesResult) {
						// $data['order'] = json_decode(json_encode($this->order_model->getOrderDataById($orderId)), true);					
						// $res = array_merge($data['order'], $data['product']);
						// $receiver_data = array(
						// 	'email' => $res['prs_email'],
						// 	'name' =>   $res['pad_name'],
						// 	'mobile' =>  $res['pad_mobile'],
						// 	'alt_mobile' =>  $res['pad_alt_phone']
						// );
						// $this->communication_model->communication('product_status_changed', $receiver_data, $res);
						echo json_encode(array("success" => true, "message" => 'Shipping Charges has been updated successfully.'));
					}
				}
			} else {
				echo json_encode(array("success" => true, "message" => 'Order product data not found.'));
			}
		} else {
			echo json_encode(array("success" => true, "message" => 'Order data not found.'));
		}
	}
}
