<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class offline_shoping_cart extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart'); // Load cart library
		$this->load->library('form_validation');
		$this->load->model('offline_model');
		$this->load->model('product_model');
	}

	private function _set_headers()
	{
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i(worry)') . ' GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Pragma: no-cache');
	}


	public function addToCart()
	{
		$this->load->library('cart');
		$name = ucfirst(url_title(convert_accented_characters($_POST['product_name']), ' ', TRUE));
		$productDetail = $this->product_model->getProductById($_POST['product_id']);
		$data  = array(
			'id' => $_POST['product_id'],
			'name' => $productDetail->prd_name ? $productDetail->prd_name : "-",
			'qty' => $_POST['product_quantity'],
			'price' => $_POST['product_price'],
			'prd_subtotal' => $_POST['product_subtotal'],
			'category' => $_POST['product_category'],
			'prd_height' => $_POST['product_height'],
			'prd_width' => $_POST['product_width'],
			'filename' => "",
			'offline_user_id' => $_POST['offline_user_id'],
			'remarks' => $_POST['item_remark']
		);
		$this->cart->insert($data);
		$this->findGrandTotal();
		echo $this->view();
	}

	/* Update cart item */
	function updateCart()
	{
		$rowid = $this->input->post('rowid');
		$price = sprintf("%.2f", $this->input->post('price'));
		$qty = $this->input->post('qty');
		$height = sprintf("%.2f", $this->input->post('height'));
		$width = sprintf("%.2f", $this->input->post('width'));
		$category = $this->input->post('category');

		if ($this->input->post('change') == "-") {
			$qty -= 1;
		}
		if ($this->input->post('change') == "+") {
			$qty += 1;
		}
		if ($category == CATEGORY_STICKER || $category == CATEGORY_BROCHURE_MIX) {
			$price = $price * $qty * $height * $width;
		} else {
			$price *= $qty;
		}

		$data = array(
			'rowid'   => $rowid,
			'qty'     => $qty,
			'prd_height' => $height,
			'prd_width' => $width,
			'prd_subtotal' => $price
		);
		/* Update the cart with the new information */
		$result = $this->cart->update($data);
		$this->findGrandTotal();
		// redirect(base_url() . 'cart');
		$this->load();
		echo $result;
	}

	function updateFilename($rowid, $fileName)
	{
		$update = 0;
		if (!empty($rowid) && !empty($fileName)) {
			$data = array(
				'rowid' => $rowid,
				'filename'   => $fileName,
			);
			$update = $this->cart->update($data);
		}
		return $update ? 'ok' : 'err';
	}

	function onOfflineCheckout()
	{
		$count = 0;
		$order_id = "";
		$admin_id = $this->session->userdata('tsn_usr_id');
		$offlineUserId = $this->session->userdata('session_offline_userid');

		if ($admin_id == '' || $admin_id == null) {
			$success = false;
			$message = 'Login must be require for checkout !!';
			$linkn = base_url();
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		} else if ($offlineUserId == '' || $offlineUserId == null) {
			$success = false;
			$message = 'Please select username for checkout !!';
			$linkn = base_url('offline');
			echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			exit();
		} else {
			$postData = $this->input->post();

			/* Create Order Reference No. Start */
			$offlineOrderId = $this->offline_model->generateOfflineOrderId($offlineUserId);
			$temp_order_reference_no = $offlineUserId . "-" . $offlineOrderId;
			$this->session->unset_userdata('order_is_success');

			$ord_id = $this->offline_model->createOfflineOrder($temp_order_reference_no, $postData);
			if ($ord_id != '-1') {

				/* GET ORDER REFERENCE NO. FROM DATABASE- START */
				$getLastGeneratedOrdRefNo_result = $this->offline_model->getOfflineOrderDetailsById($ord_id);
				$orderDetails = json_decode($getLastGeneratedOrdRefNo_result, true);
				$order_id =  $orderDetails['offline_order_reference_no'];
				/* GET ORDER REFERENCE NO. FROM DATABASE- END */

				$filesArray = array();
				$success = true;
				$message = 'Order placed successfully';
				$linkn = '';

				foreach ($this->cart->contents() as $item) {
					$rowid = $item['rowid'];

					if ($_FILES["file_input" . $rowid]["error"] > 0) {
						// $success = false;
						// $message = 'Not any file selected';
						// break;
					} else {
						if ($_FILES["file_input" . $rowid] != '') {
							$target_dir = DESIGNFILE_UPLOAD_PATH;
							$fileExt = pathinfo($_FILES['file_input' . $rowid]['name'], PATHINFO_EXTENSION);
							$newFilename = $order_id . "_" . ($count + 1) . "." . $fileExt;
							$uploadfile = $target_dir . $newFilename;

							if (move_uploaded_file($_FILES["file_input" . $rowid]["tmp_name"], $uploadfile)) {
								$filesArray += [$rowid => $newFilename];
							} else {
								$success = false;
								$message = "The file " . basename($_FILES["file_input" . $rowid]["name"]) . " has not been uploaded.";
								break;
							}
						}
					}
					$count++;
				}
				if ($success == true) {
					$insertOrderProducts_Result = $this->offline_model->insertOfflineOrderProducts($ord_id, $filesArray);
					if ($insertOrderProducts_Result == 0) {
						$success = false;
						$message = 'Sorry, there was an error inserting order products.';
					}
				}

				if ($success == true) {
					$this->session->set_userdata("order_id", $ord_id);
				}
				$this->session->set_userdata("order_is_success", $success);
				echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn, 'resposnedata' => $filesArray));
				exit();
			} else {
				$success = false;
				$message = 'Oops !! Some error occured while creating order';
				$linkn   = '';
				echo json_encode(array("success" => $success, 'message' => $message, 'linkn' => $linkn));
			}
			/* Create Order Reference No. End */
		}
	}

	// Find Grand Total of All Cart's Item
	function findGrandTotal()
	{
		$this->load->library('cart');
		$this->load->library('session');
		$shippingCharges = isset($_POST['shipping_charge']) ? $_POST['shipping_charge'] : 0;		
		$cart_total = sprintf("%.2f", 0);
		$grand_total = sprintf("%.2f", 0);
		foreach ($this->cart->contents() as $item) {
			$cart_total += $item['prd_subtotal'];
		}
		$grand_total = $cart_total + $shippingCharges; // Add shipping charge		
		$this->session->set_userdata("carttotal", $cart_total);
		$this->session->set_userdata("shipping_charge", $shippingCharges);
		$this->session->set_userdata("grandtotal", $grand_total);
		echo $grand_total;
	}

	function updateItemQty()
	{
		$update = 0;

		// Get cart item info		
		$rowid = $this->input->post('cart_id');
		$qty = $this->input->post('cart_qty');
		$category = $this->input->post('cart_category');

		if (!empty($rowid) && !empty($qty)) {
			$this->load->library('cart');
			$data = array(
				'rowid' => $rowid,
				'qty'   => $qty,
			);
			$update = $this->cart->update($data);
		} else {
			$result['status'] = 'error';
			$result['message'] = validation_errors();
		}

		if ($update == 1) {
			$result['status'] = 'success';
			$result['message'] = 'Item updated...!!!';
			$result['redirect_url'] = site_url('cart');
		} else {
			$result['status'] = 'error';
			$result['message'] = 'Whoops! Something Went Wrong';
		}
		// $this->output->set_content_type('application/json');
		// $this->output->set_output(json_encode($result));
		// $string = $this->output->get_output();
		// echo $string;
		echo $update = $this->cart->update($data);
	}

	public function load($value = '')
	{
		echo $this->view();
	}

	public function removeItem($value = '')
	{
		$this->load->library('cart');
		$row_id = $_POST['row_id'];

		$data = array(
			'rowid' => $row_id,
			'qty'   => 0
		);
		$this->cart->update($data);
		$this->findGrandTotal();
		echo "true";
	}

	public function clear($value = '')
	{
		$this->load->library('cart');
		$this->cart->destroy();

		echo json_encode(array('message' => 'Cart cleared Successfully'));
	}

	public function view($value = '')
	{
		// $this->_set_headers();
		$this->load->library('cart');
		return count($this->cart->contents());
	}
}
