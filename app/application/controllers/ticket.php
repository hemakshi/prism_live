<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	controller name always in small latter,

*/

class ticket extends CI_Controller {
	/*
	for load any model we use __construct function

	*/

		public function __construct()
    {
         parent::__construct();
         $this->load->model('ticket_model');
         $this->load->model('person_model');
         $this->load->model('communication_model'); 
    }

    public function ticketList($value='')
    {


 if($this->session->userdata('hnv_prs_id')!='')
     {

      
           $data['ticket'] = $this->ticket_model->getTickets();
           $data['my_ticket'] = $this->ticket_model->getTicketsByUser();
           $data['assign_ticket'] = $this->ticket_model->getTicketsByAssign();
           $data['cancel_ticket'] = $this->ticket_model->getTicketsCancel();
           $data['onhold_ticket'] = $this->ticket_model->getTicketsonhold();
           $data['closed_ticket'] = $this->ticket_model->getTicketsclosed();
           $data['tagged_tickets']= $this->ticket_model->getUserTaggedTickets($this->session->userdata('hnv_prs_id'));

          $this->load->view('ticket_list',$data);
      }
      else
      {
         $this->load->view('ticket_list');
      }
    }
public function addNewTicket($value='')
	{
        $this->session->unset_userdata('tck_id');
         $this->session->unset_userdata('tck_id1');
		$this->load->view('add_ticket');
	}
	// public function addTicket($value='')
	// {

	// 	 $this->load->view('add_ticket');
	// }


	// public function editTicket($value='')
	// {
	// 	$this->load->view('ticket_edit_view');
	// }
	// public function detailTicket($value='')
	// {
	// 	$this->load->view('ticket_detail_view');
	// }

	public function uploadDocuments()
{
	
        if (isset($_FILES['files']) && !empty($_FILES['files'])) {
          global $ticket_documents_path;
            $no_files = count($_FILES["files"]['name']);
            $i=0;
            $p=1;
            $count=1;
            $uploaded_documents='';
              $failed_documents='';
              
               $tta_tck_ref_no=$this->input->post('tta_tck_ref_no');
              
              
            for ($i = 0; $i < $no_files; $i++) {
            
            	 $temporary = explode(".", $_FILES['files']['name'][$i]);
                $file_extension = end($temporary);
            	
                //   echo  $_FILES['files']['name'][$i].'</br>';  
                // echo  $_FILES['files']['size'][$i].'</br>';  


                // if ($_FILES["files"]["error"][$i] > 0) {


                //     echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                // } 
                // else {
                    if ($_FILES['files']['size'][$i] > 5120000)
                     {
                    		
                    		
                    		 $p=2;
                    		 $failed_documents.=$_FILES['files']['name'][$i].'</br>';

                        // echo 'File already exists : uploads/' . $_FILES["files"]["name"][$i];
                    }
                     else {
                     	 $uploaded_documents.= $_FILES['files']['name'][$i].'</br>';
                     	 $tta_original_name=$_FILES['files']['name'][$i];
                        $_FILES['files']['name'][$i]= $this->home_model->generateRandomString(10).'.'.$file_extension;
                     		
                        move_uploaded_file($_FILES["files"]["tmp_name"][$i],$ticket_documents_path.$_FILES["files"]["name"][$i]);
                        // echo 'File successfully uploaded : uploads/' . $_FILES['files']['name'][$i] . ' ';
                      
                         
                          if( $tta_tck_ref_no=='')
                        {
                           $tta_tck_ref_no='TKT'.$this->home_model->generateRandomStringNum(4);
                       
                       }
                        else
                    {   $tta_tck_ref_no= $tta_tck_ref_no;
                    
                    }

                       $data = array(
                    'tta_tck_ref_no' =>  $tta_tck_ref_no,
                    'tta_temp_name' =>  base_url().$ticket_documents_path.$_FILES['files']['name'][$i],
                    'tta_original_name' => $tta_original_name,
                    'tta_crtd_dt' =>date('Y-m-d h:i:s'),
                    'tta_crdt_by'=>$this->session->userdata('prs_id')
                     );
                $this->db->insert('temp_ticket_att', $data);

						
                }
                $count++;
               
            }

           
            if($p==1)
            {
            	 
            	 echo json_encode(array("success"=>true,"message"=>$uploaded_documents,'tta_tck_ref_no'=> $tta_tck_ref_no));
            }
            elseif($p==2)
            {


            	echo json_encode(array("success"=>false,"message"=>$failed_documents,'tta_tck_ref_no'=> $tta_tck_ref_no,"uploaded_documents"=>$uploaded_documents));
            }
            
       
    }
         else {
            echo 'Please choose at least one file';
        	}
    
}
   function moreUploadDocuments() 
    {
        if (isset($_FILES['files']) && !empty($_FILES['files'])) {
          global $ticket_documents_path;
            $no_files = count($_FILES["files"]['name']);
            $i=0;
            $p=1;
            $count=1;
           $uploaded_documents='';
              $failed_documents='';
              
               $tta_tck_ref_no=$this->input->post('tck_id');
            for ($i = 0; $i < $no_files; $i++) {
            
               $temporary = explode(".", $_FILES['files']['name'][$i]);
                $file_extension = end($temporary);
              
                //   echo  $_FILES['files']['name'][$i].'</br>';  
                // echo  $_FILES['files']['size'][$i].'</br>';  


                // if ($_FILES["files"]["error"][$i] > 0) {


                //     echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                // } 
                // else {
                    if ($_FILES['files']['size'][$i] > 5120000)
                     {
                        
                        
                             $p=2;
                             $failed_documents.=$_FILES['files']['name'][$i].'</br>';

                        // echo 'File already exists : uploads/' . $_FILES["files"]["name"][$i];
                    }
                     else {
                         $uploaded_documents.= $_FILES['files']['name'][$i].'</br>';
                         $tta_original_name=$_FILES['files']['name'][$i];
                        $_FILES['files']['name'][$i]= $this->home_model->generateRandomString(10).'.'.$file_extension;
                        
                        move_uploaded_file($_FILES["files"]["tmp_name"][$i],$ticket_documents_path.$_FILES["files"]["name"][$i]);
                        // echo 'File successfully uploaded : uploads/' . $_FILES['files']['name'][$i] . ' ';
                      
                        
                      
                       $data = array(
                    'tta_tck_ref_no' =>  $tta_tck_ref_no,
                    'tta_temp_name' =>  base_url().$ticket_documents_path.$_FILES['files']['name'][$i],
                    'tta_original_name' => $tta_original_name,
                    'tta_crtd_dt' =>date('Y-m-d h:i:s'),
                    'tta_crdt_by'=>$this->session->userdata('prs_id')
                     );
                $this->db->insert('temp_ticket_att', $data);

                         

                       
                    }
                // }
                $count++;
               
            }

           
            if($p==1)
            {
               // echo json_encode(array("success"=>true,"message"=>'File successfully uploaded :'. $uploaded_images));
               echo json_encode(array("success"=>true,"message"=>$uploaded_documents,'tta_tck_ref_no'=> $tta_tck_ref_no));
            }
            elseif($p==2)
            {


           echo json_encode(array("success"=>false,"message"=>$failed_documents,'tta_tck_ref_no'=> $tta_tck_ref_no,"uploaded_documents"=>$uploaded_documents));
            }
            
        }
         else {
            echo 'Please choose at least one file';
          }
    }
public function insertTicket($value='')
{
	if($this->input->post('tck_ref_no')!='')
	{
		$documents=$this->ticket_model->getTempDocuments($this->input->post('tck_ref_no'));
	}
	else
	{
		$documents='';
	}
	$tck_no='T'.$this->ticket_model->csn();


    $createslug =$tck_no;
       $tck_slug = url_title($createslug, 'dash', TRUE);

      
       $tck_id=$this->ticket_model->insertTicket($tck_no,$tck_slug);
         if($tck_id!=-1)
         {
         	if( !empty($documents))
         	{
               $string=$this->ticket_model->insertTicketDocuments($documents,$tck_id,$this->input->post('tck_ref_no'));
         	}
            if($this->input->post('tck_user')!='')
            {
             
             

           $this->session->set_userdata('tck_id',$tck_id);
                global $ticket_event;
               $person_data=$this->person_model->getUserData($this->input->post('tck_user'));
               $this->communication_model->communication($ticket_event,$person_data->prs_email,$person_data->prs_name,'',$person_data->prs_mob); 
            }

            
           echo json_encode(array("success"=>true,"message"=>'Your ticket has been raised successfully.',"linkn"=>base_url().'ticket-'.$tck_slug)); 
      }
          else
          {
         log_message('error','error occured while moving temporary attachment  to ticket attachment table   >> ticket/insertTicket');
        global $error_message;
        echo json_encode(array("success"=>false,"message"=>$error_message));

          }
         

         }
         public function insertMoreDocuments($value='')
         {
             $tck_id=$this->input->post('tck_id');
            
    if($this->input->post('tck_id')!='')
    {
        $documents=$this->ticket_model->getTempDocuments($this->input->post('tck_id'));

    }
    else
    {
        $documents='';
    }
    
            if( !empty($documents))
            {
               $string=$this->ticket_model->insertTicketDocuments($documents,$tck_id,$tck_id);
                echo json_encode(array("success"=>true,"message"=>'Your documents has been attached to ticket successfully.',"linkn"=>base_url().'ticket')); 
            }
            else
            {
 echo json_encode(array("success"=>false,"message"=>'Select atleast one file.')); 
            }
           

         

         }
         
         public function ticketDetail($tck_slug)
         {

           $this->session->unset_userdata('tkc_id_owner');
    $this->session->unset_userdata('tkc_id');
    $this->session->unset_userdata('tck_old_user_name');
    $this->session->unset_userdata('tck_old_status_name');
        $tck_id=$this->ticket_model->getIdBySlug($tck_slug);
        $data['ticket'] = $this->ticket_model->getTicketById($tck_id);
        // print_r($data['ticket']);
        $data['attachment'] = $this->ticket_model->getTicketAttachments($tck_id);
        $data['comment'] = $this->ticket_model->getTicketComments($tck_id);
       $data['history'] = $this->ticket_model->getTicketHistory($tck_id);
       
      $this->load->view('ticket_detail',$data);
         }

public function insertTicketComment()
{
    global $ticket_comment_event,$ticket_owner_comment_event;
   $tkc_id=$this->ticket_model->insertTicketComment();
         if($tkc_id!=-1)
         {
           
   $this->session->set_userdata('tkc_id_owner',$tkc_id);
              if( $this->input->post('tagged_person')!='')
        {

         
             $this->session->set_userdata('tkc_id',$tkc_id);
             $tagged_email=$this->user_model->getUserEmail( @join(',', $this->input->post('tagged_person')));
             $this->communication_model->communication($ticket_comment_event,$tagged_email,'','',''); 
         }
             $person_data=$this->person_model->getUserData($this->input->post('tck_user'));




           
             $this->communication_model->communication($ticket_owner_comment_event,$person_data->prs_email,$person_data->prs_name,'',$person_data->prs_mob); 

              echo json_encode(array("success"=>true)); 
         }
         else{
             log_message('error','error occured while inserting comment   >> ticket/insertTicketComment');
        global $error_message;
        echo json_encode(array("success"=>false,"message"=>$error_message));

         }
}
public function ticketEditView($tck_slug)
{
  $this->session->unset_userdata('tck_old_user_name');
    $this->session->unset_userdata('tck_old_status_name');

     if($this->session->userdata('hnv_prs_id')!='')
     {
   $tck_id=$this->ticket_model->getIdBySlug($tck_slug);
   $data['ticket'] = $this->ticket_model->getTicketForEdit($tck_id);
    $this->load->view('ticket_edit_view',$data);
}
else
    {
        $this->load->view('ticket_edit_view');

    }

}
public function updateTicket($value='')
{
       $string=$this->ticket_model->updateTicket();

       if($string!='')
 {
     if($this->input->post('tck_user')!='')
            {
               log_message('error',' inside if condition update ticket');
                $this->session->set_userdata('tck_id1',$this->input->post('tck_id'));
                if($this->input->post('tck_status_old')!=$this->input->post('tck_status') || $this->input->post('tck_user_old')!=$this->input->post('tck_user'))
                {
                 $this->session->set_userdata('tck_old_user_name',$this->input->post('tck_old_user_name'));
                  $this->session->set_userdata('tck_old_status_name',$this->input->post('tck_old_status_name'));
                }

              
          

                global $ticket_changed_event;
                $person_data=$this->person_model->getUserData($this->input->post('tck_user'));
                $this->communication_model->communication($ticket_changed_event,$person_data->prs_email,$person_data->prs_name,'',$person_data->prs_mob);  
                
            }
             echo json_encode(array("success"=>true,"message"=>"Ticket has been updated successfully.","linkn"=>base_url().'ticket-'.$this->input->post('tck_slug')));
            log_message('error',' inside string not empty condition update ticket');
            }

 else
 {
    log_message('error','error occured while updating ticket >> ticket/updateTicket');
        global $error_message;
        echo json_encode(array("success"=>false,"message"=>$error_message));
 }
    
}
public function updateTicketstatus($value='')
{
     $string=$this->ticket_model->updateTicketstatus();
   $tck_id=  $this->input->post('tck_id');
      $ticket=$this->ticket_model->getTicketById( $tck_id);
      $this->session->set_userdata('tck_id1',$tck_id);
      $this->session->set_userdata('tck_old_user_name',$this->input->post('tck_old_user_name'));
        $this->session->set_userdata('tck_old_status_name',$this->input->post('tck_old_status_name'));

             
          global $ticket_changed_event;
          $person_data=$this->person_model->getUserData($this->input->post('tck_user'));
          $this->communication_model->communication($ticket_changed_event,$person_data->prs_email,$person_data->prs_name,'',$person_data->prs_mob); 
       
        
            
     echo json_encode(array("success"=>true,"message"=>"Ticket has been updated successfully.","linkn"=>base_url().'ticket'));
}

public function updateTicketassign($value='')
{
	 $string=$this->ticket_model->updateTicketassign();
   $tck_id=  $this->input->post('tck_id');
      $this->session->set_userdata('tck_id1',$tck_id);
      $this->session->set_userdata('tck_old_user_name',$this->input->post('tck_old_user_name'));
       $this->session->set_userdata('tck_old_status_name',$this->input->post('tck_old_status_name'));

          
               
        global $ticket_changed_event;
        $person_data=$this->person_model->getUserData($this->input->post('tck_assign'));
        $this->communication_model->communication($ticket_changed_event,$person_data->prs_email,$person_data->prs_name,'',$person_data->prs_mob); 
     echo json_encode(array("success"=>true,"message"=>"Ticket has been updated successfully.","linkn"=>base_url().'ticket'));
}

public function removeattachment($value='')
{
		$tka_id = $this->input->post('tka_id');
		 $string=$this->ticket_model->removeattachment();

		
		  echo json_encode(array("success"=>true,"message"=>"Ticket Attahment has been deleted.","linkn"=>base_url().'ticket'));
}

public function removeticket($value='')
{
	   $string=$this->ticket_model->removeticket();
	  echo json_encode(array("success"=>true,"message"=>"Ticket Attahment has been deleted.","linkn"=>base_url().'ticket'));
}

   public function getTicketsByStatus($status_name)
    {
      global $open,$Development,$review,$rework,$closed;

      switch ($status_name)
       {
        case 'open':
          $status=$open;
          break;
           case 'development':
          $status=$Development;
          break;
           case 'review':
          $status=$review;
          break;
           case 're-work':
          $status=$rework;
          break;
           case 'closed':
          $status=$closed;
          break;
        
       
      }

      $data['tickets'] = $this->ticket_model->getUserTicketByStatus($this->session->userdata('prs_id'),$status);
     $data['title']=$this->session->userdata('prs_name').'\'s '.$status_name.' Tickets';
     $this->load->view('master_ticket',$data);
    } 

   public function getTicketsByUserStatus($prs_slug,$status_name)
    {
      $user = $this->user_model->getUserDataBySlug($prs_slug);
      global $open,$Development,$review,$rework,$closed;

      switch ($status_name)
       {
        case 'open':
          $status=$open;
          break;
           case 'ongoing':
          $status=$Development.','.$review.','.$rework;
          break;
         
           case 'closed':
          $status=$closed;
          break;
         
       
      }

      $data['tickets'] = $this->ticket_model->getTicketsByUsernStatus($user->prs_id,$status);
     $data['title']=$user->prs_name.'\'s '.$status_name.' Tickets';
     $this->load->view('master_ticket',$data);
    } 
    public function getAllTicketsByUser($prs_slug,$prj_slug)
    {
      $data['user'] = $this->user_model->getUserDataBySlug($prs_slug);
     $data['project'] = $this->project_model->getProjectDataBySlug($prj_slug); 

     $data['tickets'] = $this->ticket_model->getTicketsByUserProjectID($data['user']->prs_id,$data['project']->prj_id);

      $data['title']=$data['user']->prs_name.'\'s '. $data['project']->prj_name.' Tickets';

      $this->load->view('master_ticket',$data);
    }

    public function getDescriptionFromTicketID()
    {
        $description_data = $this->ticket_model->getDescriptionOfEachTicket($this->input->post('tck_id'));
        echo json_encode(array("success"=>true,"message"=>strip_tags($description_data)));  
    }
    public function getDescriptionForCancelTicket()
    {
        $description_data = $this->ticket_model->getDescriptionOfCancelTicket($this->input->post('tck_id'));
        echo json_encode(array("success"=>true,"message"=>strip_tags($description_data)));  
    }
    public function getCommentFromTicketID()
    {
        $comment_data = $this->ticket_model->getTicketComments($this->input->post('tck_id'));
         $str="";
        
        foreach ($comment_data as $key) {
          $str .='
       <b>'.$key->tck_user_name.'</b>: ' .$key->tkc_comment.'</br>';

          
        }
                echo json_encode(array("success"=>true,"message"=>$str));  
    }
    public function getAttachmentFromTicketID()
    {
        $attach_data = $this->ticket_model->getAttachmentOfEachTicket($this->input->post('tck_id'));
        echo json_encode(array("success"=>true,"message"=>strip_tags($attach_data)));  
    }
}
?>