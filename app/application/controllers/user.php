<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('home_model');
    $this->load->model('user_model');
    $this->load->model('form_access_model');
  }

  public function user_list()
  {
    $data['user_list'] = $this->user_model->getUserList();
    $this->load->view('user_list', $data);
  }
  public function user_add()
  {
    $this->load->view('user_add');
  }

  public function user_edit($prs_slug)
  {
    $data['user_details'] = $this->user_model->getUserDataBySlug($prs_slug);
    $this->load->view('user_edit', $data);
  }
  public function user_detail($prs_slug)
  {
    $data['user'] = $this->user_model->getUserDataBySlug($prs_slug);
    $this->load->view('user_detail', $data);
  }

  public function insertUser()
  {
    $loggedInUser = $this->session->userdata('tsn_usr_id');
    if (empty($loggedInUser) || $loggedInUser == '') {
      echo json_encode(array("success" => false, "message" => 'Some error occured', "linkn" => base_url()));
      exit();
    }

    $prs_id = $this->user_model->insert_user();
    if ($prs_id != '') {
      $access = $this->form_access_model->gerantAccess($this->input->post('prs_department'), $prs_id);
      if ($access) {
        echo json_encode(array("success" => true, "message" => 'User Registered Successfully', "linkn" => base_url('user_list')));
        exit();
      } else {
        echo json_encode(array("success" => false, "message" => 'Some error occured', "linkn" => base_url('user_list')));
        exit();
      }
    } else {
      echo json_encode(array("success" => false, "message" => 'Some error occured'));
      exit();
    }


    /* OLD CODE-START */
    // $loc_id = $this->home_model->validateLocation($this->input->post('prs_location'));
    // if ($loc_id == 0) {
    //   echo json_encode(array("success" => false, "message" => INVALID_LOCATION));
    // } else {
    //   $check_validation = $this->checkServerSideValidation();
    //   if ($check_validation != '') {
    //     echo json_encode(array("success" => false, "message" => $check_validation));
    //   } else {
    //     $image = '';
    //     if (isset($_FILES["file"]["type"])) {
    //       $image_path = PERSON_IMAGE_PATH;
    //       $image = $this->ValidateImage($image_path, $_FILES["file"]["name"], 'file');
    //     }
    //     $slug = $this->input->post('prs_name') . '-' . $this->home_model->generateRandomStringNum(4);
    //     $prs_slug = url_title($slug, 'dash', TRUE);
    //     $prs_id = $this->user_model->insert_user($prs_slug, $loc_id, $image);
    //     if ($prs_id != '') {
    //       $user_id = $this->user_model->insertUserData($prs_id);
    //       $access = $this->form_access_model->gerantAccess($this->input->post('prs_department'), $prs_id);
    //       if ($access) {
    //         echo json_encode(array("success" => true, "message" => 'User Registered Successfully', "linkn" => base_url('user')));
    //       } else {
    //         echo json_encode(array("success" => false, "message" => 'Some error occured', "linkn" => base_url('user')));
    //       }
    //     } else {
    //       echo json_encode(array("success" => false, "message" => 'Some error occured'));
    //     }
    //   }
    // }
    /* OLD CODE-END */
  }

  public function updateUserDetail()
  {
    log_message('error', '>>In controller updateUserDetail');
    $string = $this->user_model->updateUserDetails();
    if ($string) {
      echo json_encode(array("success" => true, "message" => 'Your data has been updated successfully.', "linkn" => base_url() . 'user_detail/' . $this->input->post('prs_slug')));
    } else {
      log_message('error', 'error occured while updating user');
      echo json_encode(array("success" => false, "message" => "error occured while updating user"));
    }
  }

  public function updateUser()
  {
    log_message('error', '>>In controller updateUser');
    // $check_validation = $this->checkValidation();
    // log_message('error','Cheack Validation'.$check_validation);
    // if($check_validation != '')
    // {
    // echo json_encode(array("success"=>false,"message"=>$check_validation));
    // }
    // else
    // {

    $loc_id = $this->home_model->validateLocation($this->input->post('prs_location'));
    if ($loc_id == 0) {
      echo json_encode(array("success" => false, "message" => INVALID_LOCATION));
    } else {
      $image = ' ';
      if (isset($_FILES["file"]["type"])) {
        $image_path = PERSON_IMAGE_PATH;
        $image = $this->ValidateImage($image_path, $_FILES["file"]["name"], 'file');
      }
      $string = $this->user_model->updateUserData($image, $loc_id);
      if ($string != ' ') {
        echo json_encode(array("success" => true, "message" => 'Your data has been updated successfully.', "linkn" => base_url() . 'user-detail-' . $this->input->post('prs_slug')));
      } else {
        log_message('error', 'error occured while updating user');
        echo json_encode(array("success" => false, "message" => ERROR_MSG));
      }
    }
  }

  public function ValidateImage($image_path, $image_name)
  {

    log_message('error', 'in Validate image controller');

    $string = $this->user_model->ImageValidation($image_path, $image_name, 'file');
    if ($string == 'error') {
      echo json_encode(array("success" => false, "message" => 'some error occured.Try again'));
    } elseif ($string == 'size') {
      echo json_encode(array("success" => false, "message" => 'Invalid size'));
    } elseif ($string != '') {
      log_message('error', 'image valid');
      return $string;
    } else {
      echo json_encode(array("success" => false, "message" => 'some error occured'));
    }
  }
  public function checkValidation()
  {

    log_message('error', '>>User Controller: checkValidation');
    if ($this->input->post('prs_slug') != ' ') {
      $prs_slug = $this->input->post('prs_slug');
    } else {
      $prs_slug = ' ';
    }


    $check = $this->user_model->check_validation($this->input->post('type'), $this->input->post('value'), $prs_slug);


    if ($check != '0') {
      echo json_encode(array("success" => true, "message" => $this->input->post('type') . 'already exists', "linkn" => base_url()));
    } else {
      echo json_encode(array("success" => false, "message" => '', "linkn" => base_url()));
    }
  }
  public function checkServerSideValidation()
  {
    $message = '';
    if ($this->input->post('prs_slug') != ' ') {
      $prs_slug = $this->input->post('prs_slug');
    } else {
      $prs_slug = ' ';
    }

    if ($this->input->post('prs_email') != '') {
      log_message('error', '>>email not empty');
      $email_check = $this->user_model->check_validation('prs_email', $this->input->post('prs_email'), $prs_slug);
      log_message('error', '>> email_check validation count = ' . $email_check);
      if ($email_check != '0') {
        $message = 'Email already exists ';
      }
    }
    if ($this->input->post('prs_mob') != '') {
      log_message('error', '>>mobile no not empty');
      $mob_check = $this->user_model->check_validation('prs_mob', $this->input->post('prs_mob'), $prs_slug);
      log_message('error', '>> mob_check validation count = ' . $mob_check);
      if ($mob_check != '0') {
        $message = 'Mobile No. already exists ';
      }
    }
    if ($this->input->post('prs_user_name') != '') {
      log_message('error', '>>username not empty');
      $username_check = $this->user_model->check_validation('prs_username', $this->input->post('prs_user_name'), $prs_slug);
      log_message('error', '>> username_check validation count = ' . $username_check);
      if ($username_check != '0') {
        $message = 'Username already exists ';
      }
    }
    return $message;
  }
  public function user_change_pass()
  {
    //$id=6;
    log_message('nexlog', '>> user_change_pass');
    $user = $this->user_model->check_password($this->input->post('old_password'), $this->input->post('prs_slug'));
    if ($user == '0') {
      echo json_encode(array("success" => false, "message" => 'Please enter valid Old password'));
      exit();
    } else {
      $user = $this->user_model->changePasssword();
      if ($user) {
        echo json_encode(array("success" => true, "message" => 'Password changed successfully.', "linkn" => base_url('user-detail-') . $this->input->post('prs_slug')));
        exit();
      } else {
        echo json_encode(array("success" => false, "message" => '.Some error occured'));
        exit();
      }
    }
  }

  public function validatePersonPassword()
  {
    $data = array(
      'usr_id' => $this->input->post('user_id'),
      'old_password' => $this->input->post('inputPasswordOld')
    );

    $validate =  $this->user_model->checkPersonPassword($data);
    if ($validate->count == 1) {
      echo 'true';
    } else {
      echo 'false';
    }
  }

  public function changeUserPassword()
  {
    log_message('nexlog', '>> changeUserPassword');
    $data = array(
      'usr_id' => $this->input->post('user_id'),
      'old_password' => $this->input->post('old_password')
    );
    $validate = $this->user_model->checkPersonPassword($data);
    if ($validate->count == 1) {
      $user = $this->user_model->changeUserPassword();
      if ($user) {
        echo json_encode(array("success" => true, "message" => 'Password changed successfully.'));
        exit();
      } else {
        echo json_encode(array("success" => false, "message" => '.Some error occured'));
        exit();
      }
    } else {
      echo json_encode(array("success" => false, "message" => 'Something went wrong.'));
      exit();
    }
  }
}
