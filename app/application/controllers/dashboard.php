<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('person_model');
		$this->load->model('home_model');
	}
	public function dashboardView($value = '')
	{		
		$data['numberOfOrders'] = $this->home_model->totalNumberOfOrders();
		$data['numberOfPaymentReceipt'] = $this->home_model->totalNumberOfPaymentReceipt();
		$data['numberOfQuotations'] = $this->home_model->totalNumberOfQuotations();
		$data['numberOfInquiry'] = $this->home_model->totalNumberOfInquiry();
		$this->load->view('dashboard', $data);
	}
	public function contactUsList()
	{
		$data['contactUsDetails'] = $this->home_model->getContactUsDetails();
		$this->load->view('contact_us_list', $data);
	}
	public function quotationListView()
	{
		$data['quotationsDetails'] = $this->home_model->getAllQuotations();
		$this->load->view('quotations_list', $data);
	}
	public function quotationDetail($quotationId)
	{
		$data['quotationData'] = $this->home_model->getQuotationDetailById($quotationId);
		$this->load->view('quotation_detail', $data);
	}
	public function complaintListView()
	{
		$data['complaintsDetails'] = $this->home_model->getAllCompaints();
		$this->load->view('complaints_list', $data);
	}
}
