<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class shop extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('shop_model');		
	}
	

public function brandsList($value='')
	{
		if($this->session->userdata('tsn_usr_id')!='')
		{
			$data['brands']=$this->shop_model->getAllBrands();
			$this->load->view('brands_list',$data);
		}
		else
		{
			redirect('','refresh');
		}
	}
	public function insertBrand($value='')
	{	
		if($this->input->post('brd_id') =="")
		{
			$string=$this->shop_model->insertBrand();
			if($string!=-1)
			{
				echo json_encode(array("success"=>true,"message"=>'Brand has been added successfully.'));	
			}
			else
			{
				echo json_encode(array("success"=>false,"message"=>"some error has occured..."));
			}
		}
		else{

			$string=$this->shop_model->updateBrand();
			if($string)
			{
				echo json_encode(array("success"=>true,"message"=>"Brand has been updated successfully."));
			}
			else
			{
				echo json_encode(array("success"=>false,"message"=>"some error has occured..."));
			}
		}
	}
	public function editBrand($value='')
	{
		$sql="SELECT   `brd_id`,`brd_name`, `brd_order`,brd_status FROM `shop_brands` where brd_id=".$this->input->post('brd_id')."";
		$query=$this->db->query($sql);
		foreach ($query->result() as $row)
		{
			foreach($row as $kk => $vv)
			{
				echo $vv."##";
			}
		}
	}
	public function deleteFlavour($value='')
	{
	
		$string=$this->product_model->deleteFlavour();
		if($string)
		{

			unlink(Flavour_PATH.$this->input->post('brd_old_img'));
			echo json_encode(array("success"=>true,"message"=>'Image has been deleted successfully.'));  
		}
		else
		{
			echo json_encode(array("success"=>false,"message"=>"some error has occured...")); 
		}
	}
	public function productFlavourListView($value='')
	{
		if($this->session->userdata('tsn_usr_id')!='')
		{
			$data['product_flavours']=$this->product_model->getProductFlavours();
			$data['products']=$this->product_model->getProducts();
			$this->load->view('product_flavour_list',$data);
		}
		else
		{
			redirect('','refresh');
		}
	}
	public function ajaxCallBrands()
	{
		 $res = $this->shop_model->getAllBrands(ACTIVE_STATUS);
		 print $this->ajaxCombo1($res);
	}
		function ajaxCombo1($res){
		$str ="<option value=''>-- Please Select --</option>";
		foreach($res as $row){
			$str.="<option value='".$row->brd_id."'>".$row->brd_name."</option>";
		}
		print $str;
	}
}