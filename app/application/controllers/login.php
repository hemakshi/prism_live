<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('person_model');
	}


	public function loginView()
	{
		if (isset($_SERVER['HTTP_REFERER'])) {
			$data['ref'] = $_SERVER['HTTP_REFERER'];
		} else {
			$data['ref'] = base_url() . 'dashboard';
		}
		if (isset($_COOKIE['tsn_usr_username']) && isset($_COOKIE['tsn_usr_password'])) {
			$pos = strrpos($data['ref'], '/');
			$last = $pos === false ? $data['ref'] : substr($data['ref'], $pos + 1);
			if ($last == base_url() || $last == 'logout') {
				$data['ref'] = base_url() . 'dashboard';
			}

			$person_data = $this->person_model->getUserdataForLogin($_COOKIE['tsn_usr_username'], $_COOKIE['tsn_usr_password']);
			if (!empty($person_data)) {
				$newdata = array(
					'tsn_usr_id' => $person_data->usr_id,
					'tsn_usr_username'  => $person_data->usr_username,
					'tsn_usr_email'  => $person_data->usr_email,
					'tsn_usr_mobile'  => $person_data->usr_mobile,
					'tsn_usr_dpt_id' => $person_data->usr_dpt_id,
					'is_logged_in' => TRUE
				);
				$this->session->set_userdata($newdata);

				redirect($data['ref']);
			} else {
				$this->load->view('login', $data);
			}
		} else {
			$this->load->view('login', $data);
		}
	}
	public function loginUser()
	{

		$ref = $this->input->post('ref');
		$pos = strrpos($ref, '/');
		$last = $pos === false ? $ref : substr($ref, $pos + 1);
		if ($last == base_url() || $last == 'logout') {
			$ref = base_url() . 'dashboard';
		}

		$person_data = $this->person_model->getUserdataForLogin($this->input->post('usr_username'), openssl_encrypt(trim($this->input->post('usr_password')), CIPHER, KEY));
		if (!empty($person_data)) {
			$newdata = array(
				'tsn_usr_id' => $person_data->usr_id,
				'tsn_usr_username'  => $person_data->usr_username,
				'tsn_usr_email'  => $person_data->usr_email,
				'tsn_usr_mobile'  => $person_data->usr_mobile,
				'tsn_usr_dpt_id' => $person_data->usr_dpt_id,
				'is_logged_in' => TRUE
			);
			$this->session->set_userdata($newdata);

			if ($this->input->post('rememberme') == 1) {
				setcookie('tsn_usr_username', $this->input->post('usr_username'), time() + (3600 * 24 * 30 * 12 * 10), '/'); //  3600 days
				setcookie('tsn_usr_password', openssl_encrypt(trim($this->input->post('usr_password')), CIPHER, KEY), time() + (3600 * 24 * 30 * 12 * 10), '/'); //  3600 days
			}

			echo json_encode(array("success" => true, 'linkn' => $ref));
		} else {
			echo json_encode(array("success" => false));
		}
	}

	public function logout()
	{

		$newdata = array(
			'tsn_usr_id' => '',
			'tsn_usr_username' => '',
			'tsn_usr_name' => '',
			'tsn_usr_email' => '',
			'tsn_usr_mob' => '',
			'tsn_usr_dpt_id' => '',
			'is_logged_in' => False
		);

		$this->session->unset_userdata($newdata);
		$this->session->sess_destroy();
		if (isset($_COOKIE['tsn_usr_username']) && isset($_COOKIE['tsn_usr_password'])) {
			delete_cookie('tsn_usr_username');
			delete_cookie('tsn_usr_password');
		}


		redirect('', 'refresh');
	}

	public function forgotPasswordResetForUser()
	{
		$userEmailId = $this->input->post('user_email');
		$userDetails = $this->person_model->getUserdataByEmail($userEmailId);
		if (isset($userDetails) && !empty($userDetails)) {
			$isSend = false;

			$fpt_data = array();
			$fpt_data['fpt_prs_id']  = $userDetails->usr_id;
			$fpt_data['fpt_is_admin_user'] = 1;
			$fpt_data['fpt_code']    = $this->home_model->generateRandomStringNum(4);
			$fpt_data['fpt_status']  = ACTIVE_STATUS;
			$fpt_data['fpt_crtd_dt'] = date('Y-m-d H:i:s');
			$fpt_id = $this->home_model->insert('forgot_password_transaction', $fpt_data);

			if ($fpt_id != '-1') {
				$data = $userDetails->usr_id . '-' . $fpt_data['fpt_code'];
				$userDetails->link = base_url('reset-password-') . $this->url_encrypt->encrypt_openssl($data);
				$isSend = $this->person_model->sendMailToUserForPasswordReset($userDetails);
			}
			$str = '';
			if ($isSend) {
				$str .= '<h style="color:#98ce44;margin-top: 3px;width: 170px;">
				<span class="glyphicon glyphicon-ok "></span> Please check your email. We sent you your password.</h>';
				$response = array('success' => true, 'message' => $str);
			} else {
				$str .= '<h style="color:#ff0000;margin-top: 3px;width: 170px;">
                <span class="glyphicon glyphicon-ok "></span> Something went wrong.</h>';
				$response = array('success' => false, 'message' => $str);
			}
			echo json_encode($response);
		} else {
			// $str = '';
			// $str .= '<form  method="POST">';
			$str = '<h3 style="color:red; margin-top: 3px; width: 170px;"><span class="glyphicon glyphiconn-remove-circle"></span>Please enter valid email Id.</h3>';
			echo json_encode(array("success" => false, "message" => $str));
		}
	}

	public function user_reset_password($pswd_parameter = '', $type = '')
	{
		$data['title']    = 'Reset Password';
		if ($pswd_parameter != '') {
			$data['check'] = '0';
			$data['user_id'] = '0';
			$data['msg'] = 'Oops !! Sorry No Password Reset Request found';
			$pswd_parameter = $this->url_encrypt->decrypt_openssl($pswd_parameter);
			if ($pswd_parameter != '') {
				$pswd_data = explode('-', $pswd_parameter);

				$fpt_user_id = $pswd_data[0];
				$fpt_code = $pswd_data[1];
				$fpt_data = $this->person_model->getForgotpswdTransaction($fpt_user_id, $fpt_code);
				if (!empty($fpt_data)) {
					if ($fpt_data->fpt_status == 0) {
						$data['check'] = '0';
						$data['msg'] = 'Sorry, Your link has deactivated';
						$data['user_id'] = '';
						$data['fpt_id'] = '';
					} else if (!empty($fpt_data)) {
						$data['check'] = '1';
						$data['msg'] = 'Link active';
						$data['user_id'] = $fpt_user_id;
						$data['fpt_id'] = $fpt_data->fpt_id;
					} else {
						$data['check'] = '0';
						$data['msg'] = 'Oops !! Sorry your link has expired';
						$data['user_id'] = '';
						$data['fpt_id'] = '';
					}
					if (empty($fpt_code)) {
						$data['check'] = '1';
						$data['user_id'] = $fpt_user_id;
						$data['fpt_id'] = '';
					}
				} else {
					$data['check'] = '0';
					$data['msg'] = 'Oops !! Sorry your link has expired';
					$data['user_id'] = '';
				}
			}
		} else {
			$data['check'] = '0';
			$data['msg'] = 'Oops !! Sorry No Password Reset Request found';
			$data['user_id'] = '';
		}
		$this->load->view('user_reset_password', $data);
	}

	public function user_reset_password_submit()
	{
		/* Update password */
		$user_id = $this->input->post('user_id');
		$resetData = array(
			'usr_password' => openssl_encrypt(trim($this->input->post('user_password')), CIPHER, KEY),
		);
		$isReset = $this->home_model->update('usr_id', $user_id, $resetData, 'user');
		/* Update password */

		/* Deactivate Forgot password status */
		$fpt_id = $this->input->post('fpt_id');
		$deactiveStatus = array(
			'fpt_status' => 0,
		);
		$this->home_model->update('fpt_id', $fpt_id, $deactiveStatus, 'forgot_password_transaction');
		/* Deactivate Forgot password status */

		if ($isReset) {
			$success = true;
			$message = 'Password reset successfully.';
			$linkn    = base_url();
		} else {
			$success = false;
			$message = 'Oops !! Some error occured.';
			$linkn   = '';
		}
		echo json_encode(array('success' => $success, 'message' => $message, 'linkn' => $linkn));
	}

	public function mail_pw()
	{
		$emailId = $this->input->post('CLI_email');
		$sql = "SELECT usr_email, usr_password from user where usr_email='" . $emailId . "'";
		$query = $this->db->query($sql);
		$row = $query->row();

		if (!empty($row)) {

			$email = $this->input->post('CLI_email');
			$emailsend = $email;

			$subject = "Forgot Password - TSN";

			$style = 'color:black';
			$style1 = 'font-weight:bold;color:black;size:18px';
			$style2 = 'font-weight:bold;color:black;size:13px';

			$msg1 = "<TABLE border='1' cellpadding='2' cellspacing='1' valign='top' style='font-family: Arial, Helvetica, sans-serif;font-size: 13px;padding-top: 5px;padding-left: 3px;padding-bottom: 5px;border-bottom: 1px solid #000000;border-left: 1px solid #000000;border-right: 1px solid #000000;border-top: 1px solid #000000; border-collapse:collapse' width='400'>";
			$msg1 .= "<TR><TD style='padding: 8px'>Email ID :</TD><TD>" . $email . "</TD></TR>";
			$msg1 .= "<TR><TD style='padding: 8px'>Password :</TD><TD>" . openssl_decrypt($row->usr_password, CIPHER, KEY)	. "</TD></TR>";
			$msg1 .= "</TABLE>";

			$body = "
  				<table border='0' width='100%' align='center' cellpadding='2' cellspacing='1' style='font-family: Arial, Helvetica, sans-serif;font-size: 13px;padding-top: 5px;padding-left: 3px;'><tr><td><p style=" . $style2 . ">To, </p><p style=" . $style2 . ">Respected Sir/ Mam,</p><p>Kindly find your login credentials below :</p></td></tr>
  				<tr><td align='left'>" . $msg1 . "</td></tr>
  				<tr><td style='font-family:times new roman, serif;'><BR><BR>Thanks & Regards,<BR><u style=" . $style2 . ">TSN</u><BR></td></tr></table>";

			$message = $body;

			$config = array(
				'protocol' =>  EMAIL_PROTOCOL,
				'smtp_host' => EMAIL_HOST,
				'smtp_port' => 587,
				'smtp_user' => EMAIL_USERNAME_ADMIN,
				'smtp_pass' => EMAIL_PASSWORD,
				'mailtype'  => EMAIL_TYPE,
				'charset'   => EMAIL_CHARSET,
				'wordwrap' =>  EMAIL_WORDWRAP,
				'_smtp_auth' => true
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL_USERNAME_ADMIN, PROJECT_NAME);
			$this->email->subject($subject);
			$this->email->to($emailsend);
			$this->email->message($message);

			$str = '';
			if ($this->email->send()) {
				// print_r($this->email->print_debugger());
				$str .= '<h style="color:#98ce44;margin-top: 3px;width: 170px;">
                <span class="glyphicon glyphicon-ok "></span> Please check your email. We sent you your password.</h>';
			} else {
				log_message('debug', 'error occured in people insert communication_model/send_mail' . $this->email->print_debugger());
				show_error($this->email->print_debugger());
				$str .= '<h style="color:#ff0000;margin-top: 3px;width: 170px;">
                <span class="glyphicon glyphicon-ok "></span> Something went wrong.</h>';
			}
			echo json_encode(array("success" => true, 'message' => $str));
		} else {
			$str = '';
			$str .= '<form  method="POST">';
			$str .= '<h style="color:red; margin-top: 3px; width: 170px;"><span class="glyphicon glyphiconn-remove-circle"></span>Please enter valid email-ID.</h>';
			echo json_encode(array("success" => false, "message" => $str));
		}
	}
}
