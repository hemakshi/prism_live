<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class offline_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
	}
	public function offlineOrderListView()
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			// load model 
			$this->load->model('offline_model');

			$data['tab'] = 'all';
			$data['orders'] = $this->offline_model->getAllOrders();
			$data['order_placed'] = $this->offline_model->getAllOrders(OFFLINE_ORD_STATUS_PLACED);
			$data['order_dispached'] = $this->offline_model->getAllOrders(OFFLINE_ORD_STATUS_DISPATCHED);
			$data['order_delivered'] = $this->offline_model->getAllOrders(OFFLINE_ORD_STATUS_DELIVERED);
			$data['order_cancelled'] = $this->offline_model->getAllOrders(OFFLINE_ORD_STATUS_CANCELLED);
			$this->load->view('offline_order_list', $data);
		} else {
			redirect('', 'refresh');
		}
	}

	public function offlineOrderCheckView()
	{
		$this->load->view('offline_checkout');
	}

	public function getProductsByCategoryId()
	{
		// POST data 
		$postData = $this->input->post();

		// load model 
		$this->load->model('offline_model');

		// get data 
		$data = $this->offline_model->getProductsByCategoryId($postData);
		echo json_encode($data);
	}

	public function getProductById()
	{
		// POST data 
		$postData = $this->input->post();

		// load model 
		$this->load->model('offline_model');

		// get data 
		$data = $this->offline_model->getProductById($postData);
		echo json_encode($data);
	}

	public function getOfflineUserById()
	{
		// POST data 
		$postData = $this->input->post();

		// load model 
		$this->load->model('offline_model');

		// get data 
		$data = $this->offline_model->getOfflineUserById($postData);
		if ($data) {
			$this->session->set_userdata("session_offline_userid", $data[0]->offline_user_id);
			$this->session->set_userdata("session_offline_username", $data[0]->offline_user_username);
			echo json_encode(array("success" => true, 'message' => '', 'data' => $data));
		} else {
			$this->session->set_userdata("session_offline_userid", '');
			$this->session->set_userdata("session_offline_username", '');
			echo json_encode(array("success" => false, 'message' => 'User not found.'));
		}
	}

	public function generateOfflineOrderId()
	{
		// load model 
		$this->load->model('offline_model');

		// get data 
		$data = $this->offline_model->generateOfflineOrderId();
		if ($data) {
			echo json_encode(array("success" => true, 'message' => '', 'data' => $data));
		} else {
			echo json_encode(array("success" => false, 'message' => "Something went wrong"));
		}
	}

	public function insertOrderItem()
	{
		// POST data
		$postData = $this->input->post();

		// load model
		$this->load->model('offline_model');

		/* Generate Offline Order */
		$genrateOfflineorderResult = $this->offline_model->genrateOfflineorder();

		// post order Item data
		$data = $this->offline_model->insertOrderItem($postData);
		if ($data) {
			echo json_encode(array("success" => true, 'message' => '', 'data' => $data));
		} else {
			echo json_encode(array("success" => false, 'message' => "Something went wrong"));
		}
	}

	public function getOfflineOrderDetailsById()
	{
		// POST data
		$postData = $this->input->post();

		// load model 
		$this->load->model('offline_model');

		// get data 
		$orderData = $this->offline_model->getOfflineOrderDetailsById($postData['order_id']);
		if ($orderData) {
			$orderDetails = json_decode($orderData, true);
			echo json_encode(array("success" => true, 'message' => '', 'data' => $orderDetails));
		} else {
			echo json_encode(array("success" => false, 'message' => "Something went wrong"));
		}
	}

	public function updateOrderDetail()
	{
		$postData = $this->input->post();

		$this->load->model('offline_model');

		$isUpdated = $this->offline_model->updateOrderDetail($postData);
		if ($isUpdated && $postData['ord_status'] == OFFLINE_ORD_STATUS_DISPATCHED) {
			$this->load->model('communication_model');
			$orderFullData = $this->offline_model->getOfflineOrderFullDetailsById($postData['offline_order_id']);
			$receiver_data = array(
				'name' => $orderFullData->offline_user_fullname,
			);
			$orderData = array(
				'ord_reference_no' => $orderFullData->offline_order_reference_no
			);
			$smsData = $this->communication_model->getMsgData('offline_order_out_for_delivery_user_msg', $receiver_data, $orderData);
			if ($smsData != '' && $orderFullData->offline_user_mobile != '') {
				$this->communication_model->SendMessage($smsData, $orderFullData->offline_user_mobile);
			}
		}
		echo json_encode(array("success" => true, "message" => 'Order Detail updated successfully.'));
	}

	public function getOfflineOrderData($ord_reference_no)
	{
		$this->load->model('offline_model');

		if ($this->session->userdata('tsn_usr_id') != '') {
			$data['order'] = $this->offline_model->getOfflineOrderByRefrence($ord_reference_no);
			if ($data['order'] != false) {
				$data['product'] = $this->offline_model->getOfflineOrderProductsById($data['order']->offline_order_id);
				$this->load->view('offline_order_detail', $data);
			} else {
				$this->offlineOrderListView();
			}
		} else {
			redirect('', 'refresh');
		}
	}

	/* Mannage Offline Customer- Start */
	public function offlineCustomerListView()
	{
		// load model 
		$this->load->model('offline_model');

		$data['user_list'] = $this->offline_model->getOfflineCustomersList();
		$this->load->view('offline_customer_list', $data);
	}
	public function customer_detail($user_id)
	{
		// load model 
		$this->load->model('offline_model');

		$data['user'] = $this->offline_model->getOfflineUserAllDetailsById($user_id);
		if ($data['user']) {
			$this->load->view('offline_customer_detail', $data);
		} else {
			$this->offlineCustomerListView();
		}
	}
	public function customer_edit($user_id)
	{
		// load model 
		$this->load->model('offline_model');

		$data['user_details'] = $this->offline_model->getOfflineUserAllDetailsById($user_id);
		if ($data['user_details']) {
			$this->load->view('offline_customer_edit', $data);
		} else {
			$this->offlineCustomerListView();
		}
	}

	public function customer_add()
	{
		$this->load->view('offline_customer_add');
	}
	public function insertOfflineCustomer()
	{
		$loggedInUser = $this->session->userdata('tsn_usr_id');
		if (empty($loggedInUser) || $loggedInUser == '') {
			echo json_encode(array("success" => false, "message" => 'Some error occured', "linkn" => base_url()));
			exit();
		}

		// POST data
		$postData = $this->input->post();

		// load model 
		$this->load->model('offline_model');

		// Insert data
		$prs_id = $this->offline_model->insertOfflineCustomer($postData);

		if ($prs_id != 0 && $prs_id != '') {
			echo json_encode(array("success" => true, "message" => 'Customer Registered Successfully', "linkn" => base_url('offline_customers')));
			exit();
		} else {
			echo json_encode(array("success" => false, "message" => 'Some error occured', "linkn" => base_url('offline_customers')));
			exit();
		}
	}

	public function updateOfflineCustomerDetail()
	{
		$loggedInUser = $this->session->userdata('tsn_usr_id');
		if (empty($loggedInUser) || $loggedInUser == '') {
			echo json_encode(array("success" => false, "message" => 'Some error occured', "linkn" => base_url()));
			exit();
		}

		// load model
		$this->load->model('offline_model');

		$result = $this->offline_model->updateOfflineCustomerDetail();
		// echo json_encode($result); exit();
		if ($result) {
			echo json_encode(array("success" => true, "message" => 'Your data has been updated successfully.', "linkn" => base_url() . 'offline_customers'));
		} else {
			log_message('error', 'error occured while updating user');
			echo json_encode(array("success" => false, "message" => "error occured while updating user"));
		}
	}

	public function generateInvoiceForOfflineOrder($order_id)
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			// load model
			$this->load->model('offline_model');

			$data['order'] = $this->offline_model->getOfflineOrderFullDetailsById($order_id);
			$data['products'] = $this->offline_model->getOfflineOrderProductsById($order_id);
			if ($data['order'] != '' && $data['products'] != '') {
				$this->load->view('offline_order_invoice', $data);
			} else {
				$this->offlineOrderListView();
			}
		} else {
			redirect('', 'refresh');
		}
	}
	/* Mannage Offline Customer- End */

	public function checkValidationForOfflineUser()
	{
		// load model 
		$this->load->model('offline_model');

		if ($this->input->post('prs_slug') != ' ') {
			$prs_slug = $this->input->post('prs_slug');
		} else {
			$prs_slug = ' ';
		}
		$check = $this->offline_model->checkValidationForOfflineUser($this->input->post('type'), $this->input->post('value'), $prs_slug);
		if ($check != '0') {
			echo json_encode(array("success" => true, "message" => $this->input->post('value') . ' already exists', "linkn" => base_url()));
		} else {
			echo json_encode(array("success" => false, "message" => '', "linkn" => base_url()));
		}
	}

	/* Manage Offline Payment History- Start */
	public function offlineOrderAndPaymentHistoryView()
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			$this->load->model('offline_model');

			$data['orderPaymentHistoryData'] = $this->offline_model->getAllOrders();
			$this->load->view('offline_payment_history', $data);
		} else {
			redirect('', 'refresh');
		}
	}
	/* Manage Offline Payment History- End */
}
