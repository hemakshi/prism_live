<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class order extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('product_model');
		$this->load->model('order_model');
		$this->load->model('wallet_model');
	}
	public function getAllOrders()
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			$data['tab'] = 'all';
			$data['orders'] = $this->order_model->getAllOrders();
			$data['order_placed'] = $this->order_model->getAllOrders(ORDER_PLACED);
			$data['order_dispached'] = $this->order_model->getAllOrders(ORDER_DISPATCHED);
			$data['order_delivered'] = $this->order_model->getAllOrders(ORDER_DELIVERED);
			$data['order_cancelled'] = $this->order_model->getAllOrders(ORDER_CANCELLED);
			$this->load->view('order_list', $data);
		} else {
			redirect('', 'refresh');
		}
	}
	public function getOrderData($ord_reference_no)
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			$data['order'] = $this->order_model->getUserOrderByRefrence($ord_reference_no);
			if ($data['order'] != false) {
				$data['product'] = $this->product_model->getOrderProductsById($data['order']->ord_id);
				$this->load->view('order_detail', $data);
			} else {
				$this->getAllOrders();
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function updateOrderProductStatus($value = '')
	{
		$this->load->model('communication_model');
		$string = $this->order_model->updateProductOrderStatus();
		if ($string) {
			$string = $this->order_model->insertProductOrderStatus();
			if ($string != -1) {
				$data['order'] = json_decode(json_encode($this->order_model->getOrderDataById($this->input->post('order_id'))), true);
				$data['product'] = json_decode(json_encode($this->product_model->getProductDataById($this->input->post('order_id'), $this->input->post('product_id'))), true);
				$orderAndProductsData = array_merge($data['order'], $data['product']);

				/* Send Mail To Customer- Start */
				$receiver_data = array(
					'email' => $orderAndProductsData['prs_email'],
					'name' =>   $orderAndProductsData['prs_name'],
					'mobile' =>  $orderAndProductsData['prs_mob'],
					'alt_mobile' =>  $orderAndProductsData['pad_mobile']
				);
				$mailData = $this->communication_model->getMailMsg('product_status_changed_user_mail', $receiver_data, $orderAndProductsData);
				$this->communication_model->sendMail($orderAndProductsData['prs_email'], $mailData['subject'], $mailData['body']);
				/* Send Mail To Customer- End */

				/* Send SMS to customer-Start */
				if ($mailData['smsMsg'] != '' && $orderAndProductsData['prs_mob'] != '') {
					$this->communication_model->SendMessage($mailData['smsMsg'], $orderAndProductsData['prs_mob']);
				}
				/* Send SMS to customer-End */

				echo json_encode(array("success" => true, "message" => 'Status has been updated successfully.'));
			}
		}
	}

	public function updateOrderShippingCharges()
	{
		$orderId = $this->input->post('order_id');
		$shippingCharges = $this->input->post('ord_shipping_charges');
		$orderData = json_decode(json_encode($this->order_model->getOrderDataById($orderId)), true);
		if ($orderData != false) {
			$this->load->model('communication_model');
			$updateOrderShippingChargesResult = $this->order_model->updateOrderShippingCharges($orderData);
			if ($updateOrderShippingChargesResult) {
				$walletData = json_decode(json_encode($this->home_model->getWalletDataByPersonId($orderData['ord_prs_id'])), true);
				$deducChargesResult = $this->wallet_model->deductChargesFromWalletBalance($shippingCharges, $walletData[0], $orderData);
				if ($deducChargesResult) {
					/* Send SMS to customer-Start */
					$receiver_data = array(
						'name' =>   $orderData['prs_name'],
						'mobile' =>  $orderData['prs_mob']
					);
					$deductAmount = round(floatval($shippingCharges), 2);
					$totalAmount = round(floatval($walletData['amount'] - $deductAmount), 2);
					$orderData['charges_amount'] = $deductAmount;
					$orderData['avialable_amount'] = $totalAmount;

					$smsData = $this->communication_model->getMsgData('shipping_charges_deducted_user_msg', $receiver_data, $orderData);
					if ($smsData != '' && $orderData['prs_mob'] != '') {
						$this->communication_model->SendMessage($smsData, $orderData['prs_mob']);
					}
					/* Send SMS to customer-End */
					echo json_encode(array("success" => true, "message" => 'Shipping Charges has been updated successfully.'));
				}
			}
		} else {
			echo json_encode(array("success" => true, "message" => 'Order data not found.'));
		}
	}

	public function generateInvoice($order_id, $product_id)
	{
		if ($this->session->userdata('tsn_usr_id') != '') {
			$data['order'] = $this->order_model->getOrderDataById($order_id);
			$data['product'] = $this->product_model->getProductDataById($order_id, $product_id);
			// print_r($data['order']);
			// print_r($data['product']);
			$this->load->view('invoice', $data);
		} else {
			redirect('', 'refresh');
		}
	}

	public function getOrderStatus()
	{
		echo json_encode($this->order_model->getOrderStatus());
	}

	public function updateOrderStatus()
	{
		$this->load->model('communication_model');
		$orderDetailBeforeUpdate = json_decode(json_encode($this->order_model->getOrderDataById($this->input->post('order_id'))), true);
		$string = $this->order_model->updateOrderStatus();
		if ($string) {
			$data['order'] = json_decode(json_encode($this->order_model->getOrderDataById($this->input->post('order_id'))), true);
			$orderDetails = array_merge($data['order']);

			/* Refund balance in wallet when order status is cancelled- Start */
			if ($orderDetails && $orderDetailBeforeUpdate['ord_status'] != $orderDetails['ord_status']) {
				if ($orderDetails['ord_status'] == OFFLINE_ORD_STATUS_CANCELLED) {
					$walletData = json_decode(json_encode($this->home_model->getWalletDataByPersonId($orderDetails['ord_prs_id'])), true);
					$AddBalanceResult = $this->wallet_model->refundWalletBalanceOfCancelOrder($orderDetails, $walletData[0]);
					if ($AddBalanceResult == false) {
						echo json_encode(array("success" => false, "message" => 'Error occured while adding wallet balance.'));
						exit();
					}
				}
			}
			/* Refund balance in wallet when order status is cancelled- End */

			/* Send Mail To Customer- Start */
			$receiver_data = array(
				'email' => $orderDetails['prs_email'],
				'name' =>   $orderDetails['prs_name'],
				'mobile' =>  $orderDetails['prs_mob'],
				'alt_mobile' =>  $orderDetails['pad_mobile']
			);
			$mailData = $this->communication_model->getMailMsg('order_status_changed_user_mail', $receiver_data, $orderDetails);
			$this->communication_model->sendMail($orderDetails['prs_email'], $mailData['subject'], $mailData['body']);
			/* Send Mail To Customer- End */

			/* Send SMS to customer-Start */
			if ($mailData['smsMsg'] != '' && $orderDetails['prs_mob'] != '') {
				$this->communication_model->SendMessage($mailData['smsMsg'], $orderDetails['prs_mob']);
			}
			/* Send SMS to customer-End */

			if ($orderDetails['ord_status'] == OFFLINE_ORD_STATUS_CANCELLED) {
				echo json_encode(array("success" => true, "message" => 'Status has been updated and order amount refunded successfully.', 'data' => $orderDetails));
			} else {
				echo json_encode(array("success" => true, "message" => 'Status has been updated successfully.', 'data' => $orderDetails));
			}
		}
	}

	/* START- PAYMENT RECEIPT */
	public function getPaymentReceiptStatus()
	{
		echo json_encode($this->wallet_model->getPaymentReceiptStatus());
	}

	public function updatePaymentReceiptStatus($value = '')
	{
		$this->load->model('communication_model');
		$updateStatusResult = $this->wallet_model->updatePaymentReceiptStatus();
		if ($updateStatusResult) {
			/* Add balance in wallet when receipt status is approve- Start */
			$receiptId = $this->input->post('payment_receipt_id');
			$IsApproveReceiptStatus = $this->input->post('receipt_status');
			$singleReceiptData = json_decode(json_encode($this->home_model->getReceiptById($receiptId)), true);
			$singleReceiptData = $singleReceiptData[0];

			if ($singleReceiptData) {
				if (!empty($IsApproveReceiptStatus) && $IsApproveReceiptStatus == RECEIPT_STATUS_APPROVE) {
					$walletData = json_decode(json_encode($this->home_model->getWalletDataByPersonId($singleReceiptData['upload_by'])), true);
					$AddBalanceResult = $this->wallet_model->addWalletBalance($singleReceiptData, $walletData[0]);
					if ($AddBalanceResult == false) {
						echo json_encode(array("success" => false, "message" => 'Error occured while adding wallet balance.'));
						exit();
					}
				}
			}
			/* Add balance in wallet when receipt status is approve- End */

			/* Send Mail To Customer- Start */
			$receiver_data = array(
				'email' => $singleReceiptData['prs_email'],
				'name' =>   $singleReceiptData['prs_name'],
				'mobile' =>  $singleReceiptData['prs_mob'],
				'alt_mobile' =>  $singleReceiptData['prs_whatsapp']
			);
			$deductAmount = round(floatval($singleReceiptData['pay_amt'] * 3 / 100), 2);
			$amountAfterDeduct = round($singleReceiptData['pay_amt'] - $deductAmount, 2);

			$walletDataAfterAddBalance = json_decode(json_encode($this->home_model->getWalletDataByPersonId($singleReceiptData['upload_by'])), true);
			$personAndReceiptData = array_merge($singleReceiptData, $walletDataAfterAddBalance[0], array("added_balance" => $amountAfterDeduct));

			$mailData = $this->communication_model->getMailMsg('payment_receipt_status_changed_user_mail', $receiver_data, $personAndReceiptData);
			$this->communication_model->sendMail($singleReceiptData['prs_email'], $mailData['subject'], $mailData['body']);
			/* Send Mail To Customer- End */

			/* Send SMS to customer-Start */
			if ($mailData['smsMsg'] != '' && $singleReceiptData['prs_mob'] != '') {
				$this->communication_model->SendMessage($mailData['smsMsg'], $singleReceiptData['prs_mob']);
			}
			/* Send SMS to customer-End */

			echo json_encode(array("success" => true, "message" => 'Payment receipt has been updated successfully.'));
		} else {
			echo json_encode(array("success" => false, "message" => 'Something went wrong'));
		}
	}
	/* END- PAYMENT RECEIPT */
}
