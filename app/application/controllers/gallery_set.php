<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gallery_set extends CI_Controller
{
  function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('gallery_set_model');
        // check_logged();
    }

    public function gallery_set_list()
	{
    // $data['dataTableData']       = $this->gallery_set_model->getGalleryForList(TABLE_COUNT);
    
		$this->load->view('gallery-set-list');
	}
  public function gls_form()
  {
      $glsId = $this->gallery_set_model->gls_form_update();
    if($glsId)
    {
      $success = true;
      $message = 'Details Saved successfully';
    }
    else
    {
      $success = false;
      $message = 'Oops !! Some error occured';
    }
    echo json_encode(array('success'=>$success,'message'=>$message));
    
  }


    public function getGalleryDataTableList()
    {
      $dataOptn = $this->input->get();
      $dataTableData = $this->gallery_set_model->getGalleryForList($dataOptn);
      $dataTableArray['data'] = $dataTableData;
      
      echo json_encode($dataTableArray);
    }
    public function deleteGlsImage()
    {
        $glsId = $this->gallery_set_model->deleteGlsImage();
      $gls_old_img = $this->input->post('gls_old_img');
      if($glsId)
      {
        if($gls_old_img != '')
        {
          UnlinkFile(GALLERY_SET_IMAGE.$gls_old_img);
          UnlinkFile(GALLERY_SET_IMAGE_RESIZE.$gls_old_img);
        }
        $success = true;
        $message = 'Image Set Deleted successfully';
      }
      else
      {
        $success = false;
        $message = 'Oops !! Some error occured';
      }
      echo json_encode(array('success'=>$success,'message'=>$message));
      
    }
}

?>
