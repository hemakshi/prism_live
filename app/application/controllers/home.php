<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}
	public function uploadDocuments($value = '')
	{


		if (isset($_FILES['files']) && !empty($_FILES['files'])) {
			$tatc_reference_no = $this->input->post('tatc_reference_no');
			if ($tatc_reference_no == '') {
				$tatc_reference_no = 'doc' . $this->home_model->generateRandomStringNum(4);
			} else {
				$tatc_reference_no = $tatc_reference_no;
			}

			$no_files = count($_FILES["files"]['name']);
			$i = 0;
			$p = 1;
			$count = 1;
			$uploaded_documents = '';
			$failed_documents = '';

			$document_input = explode(',', $this->input->post("document_input"));
			for ($i = 0; $i < $no_files; $i++) {
				if ($document_input[$i] == 1) {
					$temporary = explode(".", $_FILES['files']['name'][$i]);
					$file_extension = end($temporary);
					if ($_FILES['files']['size'][$i] > 5120000) {


						$p = 2;
						$failed_documents .= $_FILES['files']['name'][$i] . '</br>';
					} else {

						$uploaded_documents .= $_FILES['files']['name'][$i] . '</br>';
						if (file_exists(FEEDBACK_DOCUMENT_PATH . $_FILES['files']['name'][$i])) {
							$_FILES["files"]["name"][$i] = $this->home_model->getFileName($_FILES['files']['name'][$i]);
							if (file_exists(FEEDBACK_DOCUMENT_PATH . $_FILES["files"]["name"][$i])) {
								$_FILES["files"]["name"][$i] = $this->home_model->getFileName($_FILES['files']['name'][$i]);
							} else {
								$_FILES["files"]["name"][$i] = $_FILES["files"]["name"][$i];
							}
						} else {
							$_FILES["files"]["name"][$i] = $_FILES["files"]["name"][$i];
						}

						move_uploaded_file($_FILES["files"]["tmp_name"][$i], FEEDBACK_DOCUMENT_PATH . $_FILES["files"]["name"][$i]);



						$data = array(
							'tatc_reference_no' => $tatc_reference_no,
							'tatc_name' =>  $_FILES["files"]["name"][$i],
							'tatc_type' => FEEDBACK_DOCUMENT_TYPE,
							'tatc_path' => base_url() . FEEDBACK_DOCUMENT_PATH . $_FILES["files"]["name"][$i]

						);

						$this->db->insert('temp_attachments', $data);
					}
					$count++;
				}
			}

			if ($p == 1) {

				echo json_encode(array("success" => true, "message" => $uploaded_documents, 'tatc_reference_no' => $tatc_reference_no));
			} elseif ($p == 2) {
				echo json_encode(array("success" => false, "message" => $failed_documents, "uploaded_documents" => $uploaded_documents, 'tatc_reference_no' => $tatc_reference_no));
			}
		} else {
			echo 'Please choose at least one file';
		}
	}

	public function insertAttachments($value = '')
	{
		$documents = $this->home_model->getTempDocuments($this->input->post('doc_type'), $this->input->post('tatc_reference_no'));

		if (!empty($documents)) {
			$string = $this->home_model->insertDocument($documents, $this->input->post('type_id'), $this->input->post('tatc_reference_no'));
			if ($string) {
				echo json_encode(array("success" => true, "message" => 'Your attachments for feedback has been added successfully.'));
			} else {
				log_message('nexlog', 'error occured while inserting documents >> home/insertAttachments');

				echo json_encode(array("success" => false, "message" => ERROR_MSG));
			}
		} else {
			echo json_encode(array("success" => false, "message" => 'Please choose at least one file'));
		}
	}
	public function getPinCodeCombo()
	{
		$pincode = $this->home_model->getcombo('select pcd_id as f1,pcd_no as f2 from pincode_details where pcd_state_id="' . $this->input->post('state_id') . '"');
		echo json_encode($pincode);
	}

	public function updateQuotationDetails()
	{
		$postData = $this->input->post();
		$postData['qr_updated_on'] = date('Y-m-d H:i:s');

		$this->home_model->update('qr_id', $postData['qr_id'], $postData, 'quotation_request');
		echo json_encode(array("success" => true, "message" => 'Quotation details saved successfully.', "linkn" => base_url('quotations')));
	}

	public function updateCompliantDetails()
	{
		$postData = $this->input->post();
		$postData['com_updated_on'] = date('Y-m-d H:i:s');

		$this->home_model->update('com_id', $postData['com_id'], $postData, 'complains');
		echo json_encode(array("success" => true, "message" => 'Complaint details saved successfully.', "linkn" => base_url('quotations')));
	}
}
