<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('HIGH_PTIORITY_SELECTED', 1);
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*******************START STATUS*********************/

define('INACTIVE_STATUS', 2);
define('ACTIVE_STATUS', 1);
define('ADMIN_APPROVAL', 0);
define('ORDER_PLACED', 1);
define('ORDER_CANCELLED', 2);
define('ORDER_DISPATCHED', 3);
define('ORDER_DELIVERED', 4);
define('ORDER_FAIL', 0);

define('RECEIPT_STATUS_FAIL', 0);
define('RECEIPT_STATUS_PENDING', 1);
define('RECEIPT_STATUS_APPROVE', 2);
define('RECEIPT_STATUS_REJECT', 3);

define('TRANSACTION_TYPE_CREDIT', 1);
define('TRANSACTION_TYPE_DEBIT', 2);

define('TRANSACTION_MODE_BANK', 1);
define('TRANSACTION_MODE_CASH', 2);
define('TRANSACTION_MODE_PAY_FROM_BALANCE', 3);
define('TRANSACTION_MODE_PAYMENT_GATEWAY', 4);
define('TRANSACTION_MODE_WALLET', 5);

define('PAYMENT_PAY_STATUS_FAILED', '0');
define('PAYMENT_PAY_STATUS_SUCCESS', '1');
/*******************START ERROR MSG*********************/
define('INVALID_USER', 'Username and Password did not match');


//Ticket Severity
define('severity_high', 1);
/* Encryption and Decryption*/
define('KEY', 'NextasyMumbaiToDubai2017');
define('CIPHER', 'AES-128-ECB');

/*******************IMAGES PATH*********************/

define('PROJECT_NAME', 'Prism Prints');
define('WEBSITE_ROOT_PATH', 'https://localhost/prism_live/');
define('ADMIN_WEBSITE_ROOT_PATH', 'https://localhost/prism_live/app/');
// define('WEBSITE_ROOT_PATH', 'https://prismprints.in/');
// define('ADMIN_WEBSITE_ROOT_PATH', 'https://prismprints.in/app/');
define('ADMIN_WEBSITE_LOGO_PATH', 'public/images/logo_image/logo.png');
define('PAYMENT_RECEIPT_PATH', 'assets/payment_receipts/');
define('ORDER_DESIGN_FILE_PATH', 'assets/designfiles/');
define('COMPLAINT_ATTACHMENT_PATH', 'assets/complaint_files/');
define('QUOTATION_FILE_PATH', 'assets/quotation_files/');
define('INSTAGRAM_ID_LINK', 'https://www.instagram.com/prismprints/');

// define('EMAIL_PROTOCOL','smtp');
// define('EMAIL_HOST','mail.nexdemo.in');
// define('EMAIL_PORT',587);
// define('EMAIL_USERNAME','noreply@nexdemo.in');
// define('EMAIL_PASSWORD','Nexta@12#$');
// define('EMAIL_TYPE','html');
// define('EMAIL_CHARSET','iso-8859-1');
// define('EMAIL_WORDWRAP',TRUE);

define('SEND_MAIL_USING_SMTP', 0);
define('FPT_LINK_VALIDITY_TIME', '24 HOUR');

define('EMAIL_PROTOCOL', 'smtp');
define('EMAIL_HOST', 'ssl://mail.gmail.com');
define('EMAIL_PORT', 465);
define('EMAIL_USERNAME_ADMIN', 'noreply@prismprints.in');
define('EMAIL_PASSWORD', 'gmailpass@1111');
define('EMAIL_TYPE', 'html');
define('EMAIL_CHARSET', 'iso-8859-1');
define('EMAIL_WORDWRAP', TRUE);

/*********** Bank Details ***********/
define('BANK_NAME', 'Bank of Baroda');
define('BANK_BRANCH', 'University Road Rajkot');
define('BANK_IFSC_CODE', 'BARB0UNIRAJ');
define('BANK_ACCOUNT_NUMBER', '36720200000422');
/*********** Bank Details ***********/

/*******************OTHER*********************/

define('SLUG_WORD_LIMIT', 20);
define('PRODUCT_WIDTH_LARGE', 1200);
define('PRODUCT_WIDTH_BIG', 570);
define('PRODUCT_WIDTH_SMALL', 90);

define('INVALID_LOCATION', 'Please enter valid location');
define('USER_ACTIVE_STATUS', '1');

/***********************************************************
				START URL ENCRYPTION VALUE
 ************************************************************/
define('URL_ENCRYPT_KEY', 'NextasyMumbaiToDubai2017');
define('URL_ENCRYPT_IV', 'NextasyMumbaiToDubai2017');


define('WIDTH_SPECIFIED_BY_USER', 1500);
define('RECIPE_SRC_IMG', 'gd2');
define('RECIPE_MAINTAIN_RATIO', TRUE);
define('PATH_TO_STORE_FILE_RESIZE', 'public/gallery/original_image/');
define('WIDTH_SPECIFIED_BY_USER_FOR_SMALL_IMG', 500);
define('PRODUCT_IMG_TYPE', '1');

define('IMAGE_SIZE', 5240000); //5MB

define('PRODUCT_BIG_IMAGE_PATH', 'public/images/products/big/');
define('PRODUCT_SMALL_IMAGE_PATH', 'public/images/products/small/');
define('DEFAULT_IMG_PATH', 'public/images/products/default_img.png');
define('PRODUCT_LARGE_IMAGE_PATH', 'public/images/products/large/');
define('GALLERY_SET_IMAGE', 'public/images/gallery_set/big/');
define('GALLERY_SET_IMAGE_RESIZE', 'public/images/gallery_set/small/');
define('DOC_SIZE', 800);
define('BRAND_IMAGE_SIZE', 5240000); //5MB

define('BRAND_PATH', 'public/images/brands/');

define('GALLERY_SET_BANNER', 1);
define('ADMIN_DEPARTMENT', 1);

/* PAYMENT RECEIPT STATUS- START */
define('PAYMENT_RECEIPT_STATUS_PENDING', 1);
define('PAYMENT_RECEIPT_STATUS_APPROVE', 2);
define('PAYMENT_RECEIPT_STATUS_REJECT', 3);
/* PAYMENT RECEIPT STATUS- END */

/* ORDER STATUS- START */
define('ORDER_STATUS_PLACED', 1);
define('ORDER_STATUS_CANCELLED', 2);
define('ORDER_STATUS_DISPATCHED', 3);
define('ORDER_STATUS_DELIVERED', 4);
/* ORDER STATUS- END */

/* PRODUCT STATUS- START */
define('PRODUCT_OUT_FOR_DELIVERY', 2);
define('PRODUCT_DELIVERED', 3);

define('PRODUCT_STATUS_PLACED', 1);
define('PRODUCT_STATUS_PROCESSING', 2);
define('PRODUCT_STATUS_FILE_CORRUPTED', 3);
define('PRODUCT_STATUS_MISMATCH_QAUNTITY', 4);
define('PRODUCT_STATUS_MISMATCH_QAULITY', 5);
define('PRODUCT_STATUS_IS_IN_PRINTING', 6);
define('PRODUCT_STATUS_ORDER_IS_READY', 7);
define('PRODUCT_STATUS_DISPATCHED', 8);
define('PRODUCT_STATUS_DELIVERED', 9);
/* PRODUCT STATUS- END */

define('COMPANY_ADDRESS', '13, Vijay Plot Main Road,<br> Rajkot.- 360002.');
define('SGST', 9);
define('CGST', 9);
define('SUPPLEMENTS_MENU', '1');
define('SHOP_MENU', '2');

/****************** Project Login ********************/
define('PROJECT_ADMIN_SESSION_ID', '');
define('PROJECT_ADMIN_SESSION_NAME', '');
define('PROJECT_ADMIN_SESSION_USERNAME', '');
define('PROJECT_ADMIN_SESSION_MOB', '');
define('PROJECT_ADMIN_SESSION_EMAIL', '');
define('PROJECT_ADMIN_SESSION_STATUS', '');
define('PROJECT_ADMIN_COOKIE_USERNAME', '');
define('PROJECT_ADMIN_COOKIE_PASSWORD', '');
define('TRANSACTION_LOGIN', 1);
define('TRANSACTION_LOGOUT', 2);
/****************** Project Login ********************/

/****************** Offline Order ********************/
define('DESIGNFILE_UPLOAD_PATH', 'public/images/designfile/');

define('CATEGORY_STICKER', 4);
define('CATEGORY_BROCHURE_MIX', 7);

define('OFFLINE_ORD_IS_SUCCESS_PROCESSING', 0);
define('OFFLINE_ORD_IS_SUCCESS_SUCCESS', 1);
define('OFFLINE_ORD_IS_SUCCESS_FAILED', 2);

define('OFFLINE_ORD_STATUS_PENDING', 0);
define('OFFLINE_ORD_STATUS_PLACED', 1);
define('OFFLINE_ORD_STATUS_CANCELLED', 2);
define('OFFLINE_ORD_STATUS_DISPATCHED', 3);
define('OFFLINE_ORD_STATUS_DELIVERED', 4);

define('ORDER_PAYMENT_STATUS_PENDING', '1');
define('ORDER_PAYMENT_STATUS_PAID', '2');

define('OFFLINE_ORD_PAYMENT_MODE_CASH', 1);
define('OFFLINE_ORD_PAYMENT_MODE_PAYTM', 2);
define('OFFLINE_ORD_PAYMENT_MODE_PAY_FROM_BAL', 3);

/* Product Status Start */
define('OFFLINE_ORD_PRD_STATUS_PLACED', 1);
define('OFFLINE_ORD_PRD_STATUS_PROCESSING', 2);
define('OFFLINE_ORD_PRD_STATUS_FILE_CORRUPTED', 3);
define('OFFLINE_ORD_PRD_STATUS_MISMATCH_QUANTITY', 4);
define('OFFLINE_ORD_PRD_STATUS_MISMATCH_QUALITY', 5);
define('OFFLINE_ORD_PRD_STATUS_IS_IN_PRINTING', 6);
define('OFFLINE_ORD_PRD_STATUS_ORDER_IS_READY', 7);
define('OFFLINE_ORD_PRD_STATUS_DISPATCH', 8);
define('OFFLINE_ORD_PRD_STATUS_DELIVERED', 9);
/* Product Status End */
/****************** Offline Order ********************/