<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login/loginView";
$route['dashboard'] = "dashboard/dashboardView";
$route['categories'] = "product/productCatView";
$route['product-flavours'] = "product/flavourListView";
$route['products'] = "product/productsView";
$route['products/add'] = "product/productaddView";
$route['product-image/edit/(:any)'] = "product/productImageEditView/$1";
$route['products/edit/(:any)'] = "product/productEditView/$1";
$route['brands'] = "shop/brandsList";
$route['product/flavours'] = "product/productFlavourListView";
$route['orders'] = "order/getAllOrders";
$route['payment'] = "product/receiptView";
$route['user_details'] = "product/userDetails";
$route['ledger'] = "product/ledgerView";
$route['paytm_transaction'] = "product/paytmTransaction";
$route['orders/(:any)'] = "order/getOrderData/$1";
$route['invoice/(:num)/(:num)'] = "order/generateInvoice/$1/$2";
$route['contact_us_list'] = "dashboard/contactUsList";
$route['quotations'] = "dashboard/quotationListView";
$route['quotations/(:any)'] = "dashboard/quotationDetail/$1";
$route['complaints'] = "dashboard/complaintListView";
$route['logout'] = "login/logout";

// Offline Section- Start
$route['offline'] = "product/offlineView";
$route['offline_checkout'] = "offline_controller/offlineOrderCheckView";
$route['offline_order_list'] = "offline_controller/offlineOrderListView";
$route['offline_order/(:any)'] = "offline_controller/getOfflineOrderData/$1";
$route['offline_payment_history'] = "offline_controller/offlineOrderAndPaymentHistoryView";
$route['offline_customers'] = "offline_controller/offlineCustomerListView";
$route['customer_add'] = "offline_controller/customer_add";
$route['customer_detail/(:num)'] = "offline_controller/customer_detail/$1";
$route['customer_edit/(:num)'] = "offline_controller/customer_edit/$1";
$route['offline_invoice/(:num)'] = "offline_controller/generateInvoiceForOfflineOrder/$1";
// Offline Section- End

//gallery set
$route['gallery-set-list']  = 'gallery_set/gallery_set_list';
$route['gallery-getlist']  = 'gallery_set/getGallerytDataTableList';

$route['user_list'] = "user/user_list";
$route['user_add'] = "user/user_add";
$route['user_detail/(:num)'] = "user/user_detail/$1";
$route['user_edit/(:num)'] = "user/user_edit/$1";
$route['reset-password-(:any)'] = 'login/user_reset_password/$1';