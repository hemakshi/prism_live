<?php

/**************START COMMUNICATION VALUE **************/
define('ADMIN_EMAIL_VALUE', 'prismprintsonline@gmail.com');
define('ADMIN_SMS_VALUE', '9136126136');
define('EUS_TYPE_EMAIL', 'email');
define('EUS_TYPE_SMS', 'sms');
define('EUS_USR_TYPE_USER', 'user');
define('EUS_USR_TYPE_ADMIN', 'admin');
define('ADMIN_EMAIL', 'admin_email');
define('ADMIN_CONTACT', 'admin_contact');
define('ADMIN_SOURCE_EMAIL', 'admin_source_email');
define('ADMIN_SOURCE_PASSWORD', 'admin_source_pw');
define('EUS_STATUS_ACTIVE', '1');
define('WEBSITE_NAME', 'Prism Prints');
/***************END COMMUNICATION VALUE************************/

/***********************************************************
				START EMAIL TEMPLATES
 ************************************************************/

/*************************START PAYMENT RECEIPT APPROVE**********/
define('CUSTOMER_RECEIPT_APPROVE_SUBJECT', 'Your receipt# %RECEIPT_NUMBER% is approved- %WEBSITE_NAME%');
define('CUSTOMER_RECEIPT_APPROVE_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Payment Receipt</h3>
            <h3 class='line'>Approved</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your payment receipt# <strong>%RECEIPT_NUMBER%</strong> is approved.</p>
                <p>Remarks: <strong>%REMARKS% </strong></p>
                <p>Rs.<strong>%ADDED_BALANCE% </strong> added in %WEBSITE_NAME% wallet balance.</p>
                <p>Total balance: Rs.<strong>%TOTAL_BALANCE%</strong></p>
                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END PAYMENT RECEIPT APPROVE**********/

/*************************END PAYMENT RECEIPT REJECTED **********/
define('CUSTOMER_RECEIPT_REJECT_SUBJECT', 'Your receipt# %RECEIPT_NUMBER% is rejected- %WEBSITE_NAME%');
define('CUSTOMER_RECEIPT_REJECT_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Payment Receipt</h3>
            <h3 class='line'>Rejected</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your payment receipt# <strong>%RECEIPT_NUMBER%</strong> rejected.</p>
                <p>Remarks: <strong>%REMARKS% </strong></p>
                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END PAYMENT RECEIPT REJECTED **********/

/*************************START CUSTOMER ORDER CANCELLED**********/
define('CUSTOMER_ORDER_CANCELLED_SUBJECT', 'Your order# %ORDER_NUMBER% has been cancelled- %WEBSITE_NAME%');
define('CUSTOMER_ORDER_CANCELLED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order</h3>
            <h3 class='line'>Cancelled</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your Order <strong>#%ORDER_NUMBER%</strong> has been cancelled.</p>
                <p>Remarks: %REMARKS%</p>

                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END CUSTOMER ORDER CANCELLED**********/

/*************************START CUSTOMER ORDER DISPATCHED**********/
define('CUSTOMER_ORDER_DISPATCHED_SUBJECT', 'Your order# %ORDER_NUMBER% is out for delivery- %WEBSITE_NAME%');
define('CUSTOMER_ORDER_DISPATCHED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order</h3>
            <h3 class='line'>Out for delivery</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your Order <strong>#%ORDER_NUMBER%</strong> is out for delivery.</p>
                <p>Remarks: %REMARKS%</p>
                <p>Order Amount: <strong>RS. %AMOUNT%</strong></p>

                <h4>Shipping Address:</h4>
                <p>%CUSTOMER_NAME%</p>
                <p>%ADDRESS%</p>
                <p>%LOCALITY% </p>
                <p>%CITY%- %PINCODE% </p>
                <p>+91 %MOBILE_NUMBER% </p>
                
                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END CUSTOMER ORDER DISPATCHED**********/

/*************************START CUSTOMER ORDER DELIVERED**********/
define('CUSTOMER_ORDER_DELIVERED_SUBJECT', 'Your order# %ORDER_NUMBER% delivered- %WEBSITE_NAME%');
define('CUSTOMER_ORDER_DELIVERED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header, .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order</h3>
            <h3 class='line'>Delivered</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your Order <strong>#%ORDER_NUMBER%</strong> was delivered on %DELIVERY_DATE%. Thank you for choosing us.</p>                
                
                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END CUSTOMER ORDER DELIVERED**********/

/*************************START USER PRODUCT OUT FOR DELIVERY**********/
define('CUSTOMER_PRODUCT_OUT_FOR_DELIVERY_SUBJECT', 'Your order# %ORDER_NUMBER% of item %PRODUCT_NAME% is out for delivery- %WEBSITE_NAME%');
define('CUSTOMER_PRODUCT_OUT_FOR_DELIVERY_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header, .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order Item</h3>
            <h3 class='line'>out for delivery</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your item <strong>%PRODUCT_NAME%</strong> of Order# <strong>%ORDER_NUMBER%</strong> is out for delivery.</p>
                <p>Remarks: <strong>%REMARKS%</strong></p>
                <p>Amount: <strong>RS. %AMOUNT%</strong></p>

                <h4>Shipping Address:</h4>
                <p>%CUSTOMER_NAME%</p>
                <p>%ADDRESS%</p>
                <p>%LOCALITY% </p>
                <p>%CITY%- %PINCODE% </p>
                <p>+91 %MOBILE_NUMBER% </p>
                
                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END USER PRODUCT OUT FOR DELIVERY**********/

/*************************START USER PRODUCT DELIVERED**********/
define('CUSTOMER_PRODUCT_DELIVERED_SUBJECT', 'Your order# %ORDER_NUMBER% of item %PRODUCT_NAME% is delivered- %WEBSITE_NAME%');
define('CUSTOMER_PRODUCT_DELIVERED_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header, .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order Item</h3>
            <h3 class='line'>Delivered</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your item <strong>%PRODUCT_NAME%</strong> of Order#<strong>%ORDER_NUMBER%</strong> was delivered on <strong>%DELIVERY_DATE%</strong>. Thank you for choosing us.
                <br>
                <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END USER PRODUCT OUT FOR DELIVERED**********/

/*************************START USER PRODUCT STATUS CHANGED**********/
define('CUSTOMER_PRODUCT_STATUS_CHANGE_SUBJECT', 'Your order# %ORDER_NUMBER% of item %PRODUCT_NAME% status changed- %WEBSITE_NAME%');
define('CUSTOMER_PRODUCT_STATUS_CHANGE_BODY', "<!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,.info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        h3 {
            font-size: 20px;
            font-weight: bold;
            margin: 5px 0 2px 0;
        }
        .line {
            border-bottom-width: 4px;
            border-bottom-color: rgb(209, 50, 49);
            border-bottom-style: solid;
            line-height: 1.5;
            display: inline-block;
        }
        p {
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <h3>Order Item</h3>
            <h3 class='line'>Status Change</h3>
            <div class='temp'>
                <p>Hey %RECEIVER_NAME%,</p>
                <p>Your Order#<strong>%ORDER_NUMBER%</strong>, <strong>%PRODUCT_NAME%</strong> item's status changed to <strong>%PRODUCT_STATUS%</strong>.
                    <p>Remarks: <strong>%REMARKS%</strong></p>
                    <br>
                    <p>Thank you,<br> Team %WEBSITE_NAME%</p>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END USER PRODUCT STATUS CHANGED**********/

/*************************START ADMIN PRODUCT STATUS CHANGED**********/

define('ADMIN_PRODUCT_STATUS_CHANGED_SUBJECT', 'Product %PRODUCT_NAME% from order ID %ORDER_NUMBER% is %STATUS_NAME%');
define('ADMIN_PRODUCT_STATUS_CHANGED_BODY', 'Hi Admin,<br><br>
	Product %PRODUCT_NAME% from order ID %ORDER_NUMBER% is %STATUS_NAME%<br>
	Link: <strong><a href="%ORDER_LINK%">View Order</></strong>');

/*************************END ADMIN PRODUCT STATUS CHANGED**********/

/*************************START RESET PASSWORD**********/
define('ADMIN_PASSWORD_RESET_SUBJECT', 'Reset Your Password');
define('ADMIN_PASSWORD_RESET_BODY', " <!DOCTYPE html>
<html>
<head>
    <style type='text/css'>
        body {
            color: #4d4d4d;
            font-family: 'Open Sans', sans-serif;
        }
        .content {
            border: 1px solid #eaeaec;
            box-shadow: 0 0 4px rgba(40, 44, 63, .08);
            max-width: 640px;
            margin: 0 auto;
        }
        .header,
        .info {
            padding: 15px;
            border-bottom: 2px solid #eaeaec;
        }
        p {
            margin: 10px 0;
        }
        .text-center {
            text-align: center;
        }
        .reset-btn {
            background: #28a745;
            text-decoration: none !important;
            font-weight: 500;
            margin: 15px;
            color: #fff;
            text-transform: uppercase;
            font-size: 14px;
            padding: 10px 24px;
            display: inline-block;
            border-radius: 50px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class='content'>
        <div class='header'>
            <img src='%WEBSITE_LOGO_PATH%'>
        </div>
        <div class='info'>
            <div class='temp'>
              <p>Hello,</p>
              <p>You recently asked to reset your account password.</p>
              <p>Please click the button below to change your password.</p>
              <p class='text-center'>
                  <a href='%PASSWORD_RESET_LINK%' target='_blank' class='reset-btn'>Reset Password</a>
              </p>
              <p>Note that this link is valid for 24 hours. After the time limit has expired, you will have to resubmit the request for a password reset.</p>
              <br>Thank you, <br> Team %WEBSITE_NAME% <br>
            </div>
        </div>
    </div>
</body>
</html>");
/*************************END RESET PASSWORD**********/

/***********************************************************
				END EMAIL TEMPLATES
 ************************************************************/

/***********************************************************
				START MSG TEMPLATES
 ************************************************************/
define('CLIENT_SHIPPING_CHARGES_DEDUCTED_MSG', 'Hi %RECEIVER_NAME%, Shipping charges Rs.%CHARGES_AMOUNT% deducted for order Id# %ORDER_NUMBER%.AvblBal: Rs.%AVIALABLE_BALANCE%');
define('CLIENT_OFFLINE_ORDER_OUT_FOR_DELIVERY_MSG', 'Hi %RECEIVER_NAME%, your order Id# %ORDER_NUMBER% is out for delivery.It will reach you by %DELIVERY_DATE%');

define('CLIENT_RECEIPT_APPROVED_MSG', 'Hi %RECEIVER_NAME%, Your payment receipt# %RECEIPT_NUMBER% approved And Rs.%ADDED_BALANCE% credited. AvblBal: Rs.%TOTAL_BALANCE%');
define('CLIENT_ORDER_OUT_FOR_DELIVERY_MSG', 'Hi %RECEIVER_NAME%, your order Id# %ORDER_NUMBER% is out for delivery.Remark: %ADMIN_REMARKS%. It will reach you by %DELIVERY_DATE%');
define('CLIENT_PRODUCT_OUT_FOR_DELIVERY_MSG', 'Hi %RECEIVER_NAME%, your Product %PRODUCT_NAME% from order Id# %ORDER_NUMBER% is out for delivery.Remark: %ADMIN_REMARKS%. It will reach you by %DELIVERY_DATE%');

define('ADMIN_PRODUCT_OUT_FOR_DELIVERY_MSG', 'Hi Admin, Product %PRODUCT_NAME% from order Id# %ORDER_NUMBER% is out for delivery.Estimated delivery date is %DELIVERY_DATE%.Check email for more details');
define('CLIENT_PRODUCT_DELIVERED_MSG', 'Hi %RECEIVER_NAME%, Order Delivered: your Product %PRODUCT_NAME% from order Id# %ORDER_NUMBER% was delivered on %DELIVERY_DATE%.Thank you for shopping with %WEBSITE_NAME%');
define('ADMIN_PRODUCT_DELIVERED_MSG', 'Hi Admin, Product %PRODUCT_NAME% from order Id# %ORDER_NUMBER% is delivered.Check email for more details');

/***********************************************************
				END MSG TEMPLATES
 ************************************************************/
