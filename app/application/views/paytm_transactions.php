<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Paytm Transaction</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- <link rel="shortcut icon"  href="<?php echo base_url(); ?>public/assets/global/img/favicon.png"> -->

  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }
  </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>

            <li>
              <span>Paytm Transaction</span>
            </li>
          </ul>

        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <!-- <i class="icon-user font-orange"></i> -->
                  <span class="caption-subject bold uppercase">Paytm Transaction</span>
                </div>
                <div class="tools"> </div>
              </div>

              <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                  <thead>
                    <tr>
                      <th> Sr.No </th>
                      <th> Order Id </th>
                      <th> Email </th>
                      <th> Mobile No </th>
                      <th> Amount </th>
                      <th> Status </th>
                      <th> Pay Status </th>
                      <th> Merchant Id </th>
                      <th> Transaction Id </th>
                      <th> Payment Mode </th>
                      <th> Bank Name </th>
                      <th> Bank Id </th>
                      <th> Gateway Name </th>
                      <th> Currency </th>
                      <th> Response Code </th>
                      <th> Response Message </th>
                      <th> Pay Response Code</th>
                      <th> Transaction Date </th>
                      <th> Created On </th>
                      <th> Update On </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $text = base_url();
                    $i = 1;
                    foreach ($paytmTransactions as $key) {
                      //$str= preg_replace('/\W\w+\s*(\W*)$/', '$1', $text).'product/'.$key->prd_slug;
                      // $str= 'http://handvorganic.com/'.'product/'.$key->prd_slug;
                    ?>
                      <tr class="odd gradeX">
                        <td style="text-align: center;">
                          <?php echo $i; ?>
                        </td>
                        <td><?php echo $key->pyt_order_reference_no; ?></td>
                        <td><?php echo $key->prs_email; ?></td>
                        <td><?php echo $key->prs_mob; ?></td>
                        <td><i class="fa fa-rupee"></i> <?php echo $key->pyt_txn_amt; ?></td>
                        <td>
                          <?php
                          switch ($key->pyt_txn_status) {
                            case 'TXN_SUCCESS':
                              echo "Success";
                              break;
                            case 'PENDING':
                              echo "Pending";
                              break;
                            case 'TXN_FAILURE':
                              echo "Failed";
                              break;
                            default:
                              echo "Failed";
                              break;
                          }
                          ?>
                        </td>
                        <td>
                          <?php
                          switch ($key->pyt_pay_status) {
                            case PAYMENT_PAY_STATUS_SUCCESS:
                              echo "Success";
                              break;
                            case PAYMENT_PAY_STATUS_FAILED:
                              echo "Failed";
                              break;
                            default:
                              echo "Pending";
                              break;
                          }
                          ?>
                        </td>

                        <td><?php echo $key->pyt_mid_no; ?></td>
                        <td><?php echo $key->pyt_txn_id; ?></td>
                        <td>
                          <?php
                          switch ($key->pyt_payment_mode) {
                            case 'CC':
                              echo "Credit Card";
                              break;
                            case 'DC':
                              echo "Debit Card";
                              break;
                            case 'NB':
                              echo "Net Banking";
                              break;
                            case 'PPI':
                              echo "Paytm Wallet";
                              break;
                            case 'UPI':
                              echo "Bhim Upi";
                              break;
                            default:
                              echo "-";
                              break;
                          }

                          ?>
                        </td>
                        <td><?php echo $key->pyt_bnk_name; ?></td>
                        <td><?php echo $key->pyt_bnk_txn_id; ?></td>
                        <td><?php echo $key->pyt_gateway_name; ?></td>
                        <td><?php echo $key->pyt_currency; ?></td>
                        <td><?php echo $key->pyt_resp_code; ?></td>
                        <td><?php echo $key->pyt_resp_message; ?></td>
                        <td><?php echo $key->pyt_pay_response_code; ?></td>
                        <td><?php echo date('d-M-Y', strtotime($key->pyt_txn_date)); ?></td>
                        <td><?php echo date('d-M-Y h:m:i A', strtotime($key->pyt_crtd_dt)); ?></td>
                        <td><?php echo date('d-M-Y h:m:i A', strtotime($key->pyt_updt_dt)); ?></td>
                      </tr>
                    <?php
                      $i++;
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>

      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('common/footer') ?>
  <!-- END FOOTER -->
  <!--[if lt IE 9]>
<script src="<?php echo base_url() ?>public/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
  <!-- BEGIN CORE PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>



  <!-- END THEME LAYOUT SCRIPTS -->
  <script type="text/javascript">


  </script>
</body>

</html>