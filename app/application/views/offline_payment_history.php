<?php
if ($this->session->userdata('tsn_usr_id') == '') {
   $abc = base_url();
   echo '<script> ';
   echo 'window.location="' . $abc . '"';
   echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8" />
   <title>Prism Prints | Offline Payment History</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="Preview page of Metronic Admin Theme #1 for rowreorder extension demos" name="description" />
   <meta content="" name="author" />
   <!-- BEGIN GLOBAL MANDATORY STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN THEME GLOBAL STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME GLOBAL STYLES -->
   <!-- BEGIN THEME LAYOUT STYLES -->
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
   <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>public/assets/pages/img/favicon.png" />
   <!-- END THEME LAYOUT STYLES -->
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   <div class="page-wrapper">
      <!-- BEGIN HEADER -->
      <?php echo $this->load->view('common/header'); ?>
      <!-- END HEADER -->
      <!-- BEGIN HEADER & CONTENT DIVIDER -->
      <div class="clearfix"> </div>
      <!-- END HEADER & CONTENT DIVIDER -->
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
         <!-- BEGIN SIDEBAR -->
         <?php echo $this->load->view('common/sidebar'); ?>
         <!-- END SIDEBAR -->
         <!-- BEGIN CONTENT -->
         <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
               <div class="page-bar">
                  <ul class="page-breadcrumb">
                     <li>
                        <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                        <i class="fa fa-circle"></i>
                     </li>
                     <li>
                        <span>Offline Payment History</span>
                     </li>
                  </ul>
               </div>
               <!-- END PAGE BAR -->

               <div class="row">
                  <div class="col-md-12">
                     <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     <div class="portlet light bordered">
                        <div class="portlet-title">
                           <div class="caption font-orange">
                              <span class="caption-subject bold uppercase">Offline Payment History</span>
                           </div>
                           <div class="tools"> </div>
                        </div>

                        <div class="portlet-body">
                           <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
                              <thead>
                                 <tr>
                                    <th>Sr.No</th>
                                    <th>Order No.</th>
                                    <th>Username</th>
                                    <th class="text-center">Paid Amt</th>
                                    <th class="text-center">Pending Amt</th>
                                    <th class="text-right">Total</th>
                                    <th>Status</th>
                                    <th>Order Date</th>
                                    <th>Updated On</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                 $i = 1;
                                 foreach ($orderPaymentHistoryData as $key) { ?>
                                    <tr>
                                       <td><?php echo $i; ?></td>
                                       <td style="min-width: 150px"><?php echo $key->offline_order_reference_no; ?></td>
                                       <td><?php echo $key->offline_user_username; ?></td>
                                       <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_paid_amt); ?></td>
                                       <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_pending_amt); ?></td>
                                       <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_total); ?></td>
                                       <td>
                                          <?php
                                          switch ($key->offline_order_payment_status) {
                                             case 1:
                                                echo "Pending";
                                                break;
                                             case 2:
                                                echo "Paid";
                                                break;
                                             default:
                                                echo "-";
                                                break;
                                          }
                                          ?>
                                       </td>                                       
                                       <td><?php echo date('d-M-Y', strtotime($key->offline_order_created_on)); ?></td>
                                       <td><?php echo date('d-M-Y', strtotime($key->offline_order_updated_on)); ?></td>
                                    </tr>
                                 <?php
                                    $i++;
                                 }
                                 ?>

                              </tbody>
                           </table>
                        </div>
                     </div>
                     <!-- END EXAMPLE TABLE PORTLET-->
                  </div>
               </div>

            </div>
            <!-- END CONTENT BODY -->
         </div>
         <!-- END CONTENT -->
         <!-- BEGIN QUICK SIDEBAR -->
         <!-- END QUICK SIDEBAR -->
      </div>
      <!-- END CONTAINER -->

      <!-- BEGIN FOOTER -->
      <?php echo $this->load->view('common/footer'); ?>
      <!-- END FOOTER -->
   </div>

   <!-- BEGIN QUICK NAV -->
   <div class="quick-nav-overlay"></div>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN THEME GLOBAL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
   <!-- END THEME GLOBAL SCRIPTS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME LAYOUT SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>public/js/form_validation/update-offline-order.js"></script>

</body>

</html>