<?php
   if($this->session->userdata('tsn_usr_id')==''){
      $abc = base_url();
        echo '<script> ';
          echo 'window.location="'.$abc.'"';
        echo '</script>';
    }   
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Prism Prints |  Gallery Set</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url()?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
<style type="text/css">
  .portlet.light .dataTables_wrapper .dt-buttons {
    margin-top: -64px;
    display: none;
}

/* .dataTables_wrapper .dataTables_length{
  display: none;
}

div.dataTables_wrapper 
div.dataTables_filter {
  display: none;
} */
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header')?>  
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar')?>  
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard')?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <span>Gallery Set</span>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12 ">
              <div class="portlet light bordered">
              
        

                            <div class="portlet-title">
          <span class="caption-subject bold uppercase font-orange">Gallery Set &nbsp; <a id="addnewclick"  class="btn btn-danger filter-cancel">Add Gallery Set</a></span>
          <div class="tools">
          </div>
        </div>

               <div class="portlet-body form" >
                 <!-- BEGIN FORM-->
                 <input type="hidden" name="gls_img_small" id="gls_img_small" value="<?php echo GALLERY_SET_IMAGE_RESIZE; ?>">
                 <input type="hidden" name="gls_img_big" id="gls_img_big" value="<?php echo GALLERY_SET_IMAGE; ?>">
                 <div class="form" id="addnewform" style="display:none">
                   <form  id="gls_form" method="post" action="" class="horizontal-form">
                     <!-- HIDDEN FIELDS -->
                     <input type="hidden" id="gls_type" name="gls_type" value="<?php echo GALLERY_SET_BANNER; ?>">
                     <input type="hidden" id="gls_id" name="gls_id" value="">
                     <input type="hidden" id="gls_old_img" name="gls_old_img" value="">
                     <input type="hidden" id="gls_default_img" name="gls_default_img" value="<?php echo base_url().DEFAULT_IMG_PATH?>">
                     <div class="form-body" style="padding: 0px;">
                       <div class="row">
                        <div class="col-md-3">
                         <div class="fileinput fileinput-new" data-provides="fileinput">

                           <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img id="gls_img_pic" src="<?php echo base_url().DEFAULT_IMG_PATH?>" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" > </div>
                            <div>
                             <span class="btn default btn-file">
                               <span class="fileinput-new"> Select Img </span>
                               <span class="fileinput-exists"> Change </span>
                               <input  type="file" id="gls_image" name="gls_image"  required></span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                               <h6 id="message"></h6>
                             </div>
                             <h6 id="message"></h6>
                           </div>

                         </div>

                         <div class="col-md-3">
                           <div class="form-group">

                            <input class="form-control" placeholder="Name" id="gls_name" name="gls_name"  type="text" required >
                            <span class="help-block"></span> 
                          </div>
                        </div>
                     <div class="col-md-3">
                         <div class="form-group">

                          <input class="form-control" placeholder="Order" id="gls_order_by" name="gls_order_by"  type="text"  >
                          <span class="help-block"></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                         <div class="form-group">

                          <input class="form-control" placeholder="Link" id="gls_link" name="gls_link"  type="text"  >
                          <span class="help-block"></span>
                        </div>
                      </div>
                     <div class="col-md-3">
                      <div class="form-group">

                        <button type="submit"  class="btn blue" name="form_submit" id="form_submit" >
                         Save</button>
                         <button type="button" class="btn" name="form_submit_cancel" id="form_submit_cancel" onclick="addnewclickCancel();">
                           Cancel</button>

                         </div>
                       </div>
                     </div>

                   </div>
                 </form>
                 <!-- END FORM-->
               </div>
             </div>

             <div class="portlet-body table-responsive">
              <table class="table  table-bordered table-hover pagination_table dataTable no-footer" id="gallery_set_list">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Order</th>
                   <th>Img</th>
                    <!-- <th>Link</th> -->
                    <th>Action</th> 
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          <!--   <div class="modal fade" id="imageModal" tabindex="-1" role="Add" aria-hidden="true">
             <div class="modal-dialog">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <strong> <h4 align="center" class="modal-title" id="modal-title"></h4></strong> 
                 </div>
                 <form role="form" id="add_bank_details" style="text-align: center" >
                   <img  height="400px" width="400px" src="" id="image_src_tg">

                   <div class="modal-footer">
                     <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                   </div>
                 </form>
               </div>

             </div>

           </div> -->


           
         </div>
       </div>
     </div>
   </div>
   <!-- END CONTENT BODY -->
 </div>
 <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php $this->load->view('common/footer')?> 
<!-- END FOOTER -->
</div>


<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script src="<?php echo base_url()?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>      
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/js/form_validation/gallery_set.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>
