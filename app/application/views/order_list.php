<?php
if ($this->session->userdata('tsn_usr_id') == '') {
   $abc = base_url();
   echo '<script> ';
   echo 'window.location="' . $abc . '"';
   echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8" />
   <title>Prism Prints | Orders</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="Preview page of Metronic Admin Theme #1 for rowreorder extension demos" name="description" />
   <meta content="" name="author" />
   <!-- BEGIN GLOBAL MANDATORY STYLES -->
   <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" typ="text/css" />-->
   <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN THEME GLOBAL STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME GLOBAL STYLES -->
   <!-- BEGIN THEME LAYOUT STYLES -->
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
   <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>public/assets/pages/img/favicon.png" />
   <!-- END THEME LAYOUT STYLES -->
</head>
<!-- END HEAD -->
<!-- START SWEET ALERT -->
<style type="text/css">
   /* tab active colour*/
   .tabbable-custom>.nav-tabs>li.active {
      border-top: 3px solid #005dc1;
   }

   .portlet.light .dataTables_wrapper .dt-buttons {
      margin-top: -51px;
   }

   #count {
      color: red;
   }
</style>
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>public/assets/alert/sweetalert.css">-->
<!-- END SWEET ALERT -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   <div class="page-wrapper">
      <!-- BEGIN HEADER -->
      <?php echo $this->load->view('common/header'); ?>
      <!-- END HEADER -->
      <!-- BEGIN HEADER & CONTENT DIVIDER -->
      <div class="clearfix"> </div>
      <!-- END HEADER & CONTENT DIVIDER -->
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
         <!-- BEGIN SIDEBAR -->
         <?php echo $this->load->view('common/sidebar'); ?>
         <!-- END SIDEBAR -->
         <!-- BEGIN CONTENT -->
         <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
               <div class="page-bar">
                  <ul class="page-breadcrumb">
                     <li>
                        <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                        <i class="fa fa-circle"></i>
                     </li>
                     <li>
                        <span>Orders</span>
                     </li>
                  </ul>
               </div>
               <!-- END PAGE BAR -->
               <div class="row">
                  <div class="col-md-12">
                     <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     <div class="portlet light bordered">
                        <div class="portlet-title">
                           <div class="caption font-orange">
                              <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                              <span class="caption-subject bold uppercase">Orders</span><br>
                              <p class="text-danger small">*Note: If the status of the order set as cancelled, then the amount will be refunded to the wallet balance.</p>
                           </div>
                           <div class="tools"> </div>
                        </div>
                        <div class="portlet-body ">
                           <div class="tabbable-custom table-responsive">
                              <ul class="nav nav-tabs ">
                                 <li class="<?php echo $active = $tab  == 'all' ? 'active' : ''; ?>">
                                    <a href="#tab_5_1" data-toggle="tab"> All <span id="count"> (<?php echo count($orders) ?>) </span></a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'placed' ? 'active' : ''; ?>">
                                    <a href="#tab_5_2" data-toggle="tab">Placed <span id="count"> (<?php echo count($order_placed) ?>)</span></a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'dispatched' ? 'active' : ''; ?>">
                                    <a href="#tab_5_3" data-toggle="tab"> Dispatched <span id="count"> (<?php echo count($order_dispached) ?>)</span></a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'delivered' ? 'active' : ''; ?>">
                                    <a href="#tab_5_4" data-toggle="tab">Delivered<span id="count"> (<?php echo count($order_delivered) ?>)</span> </a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'cancelled' ? 'active' : ''; ?>">
                                    <a href="#tab_5_5" data-toggle="tab"> Cancelled <span id="count"> (<?php echo count($order_cancelled) ?>)</span></a>
                                 </li>
                              </ul>
                              <div class="tab-content">
                                 <!-- All Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'all' ? 'active' : ''; ?>" id="tab_5_1">
                                    <div class="portlet-title">
                                       <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1"  id="sample_1">
                                          <thead>
                                             <tr>
                                                <th style="display: none;"> No. </th>
                                                <th>Order No.</th>
                                                <th>Order Source</th>
                                                <th>Placed On</th>
                                                <th>Payment Mode</th>
                                                <th class="text-center">Charges</th>
                                                <th class="text-right">Total</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($orders as $key) {
                                             ?>
                                                <tr>
                                                   <td style="display: none;"></td>
                                                   <td style="min-width: 140px;"><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->ord_reference_no) ?>"><?php echo $key->ord_reference_no ?></a></td>
                                                   <td><?php echo $key->pad_city . "-" . $key->pad_pincode ?></td>
                                                   <td><?php echo date('d-M-Y h:m:i A', strtotime($key->ord_crtd_dt)); ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><?php echo floatval($key->ord_shipping_charges); ?></td>
                                                   <td class="text-right"><?php echo floatval($key->ord_total_amt); ?></td>
                                                   <td><?php echo $key->ord_status_name ?></td>
                                                   <td class="text-center" style="min-width: 100px;">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderStatusModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-order_status="<?php echo $key->ord_status ?>" data-ord_remark="<?php echo $key->ord_remark; ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger  filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                      <?php if ($key->ord_shipping_charges == 0) { ?>
                                                         <a data-toggle="modal" title="Add Charges" onclick="return showAddOrdrShippingChargeModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-shipping_charge="<?php echo $key->ord_shipping_charges; ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger filter-cancel">
                                                            <i class="fa fa-money" aria-hidden="true"></i>
                                                         </a>
                                                      <?php } ?>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- All Order Section- End -->

                                 <!-- Placed Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'placed' ? 'active' : ''; ?>" id="tab_5_2">
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th style="display: none;"> No. </th>
                                                <th>Order No.</th>
                                                <th>Order Source</th>
                                                <th>Placed On</th>
                                                <th>Payment mode</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_placed as $key) {
                                             ?>
                                                <tr>
                                                   <td style="display: none;"></td>
                                                   <td><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->ord_reference_no) ?>"><?php echo $key->ord_reference_no ?></a></td>
                                                   <td><?php echo $key->pad_address ?></td>
                                                   <td><?php echo date('d-M-Y h:m:i A', strtotime($key->ord_crtd_dt)); ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><?php echo floatval($key->ord_total_amt) ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderStatusModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-order_status="<?php echo $key->ord_status ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger btn-outline filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                      <?php if ($key->ord_shipping_charges == 0) { ?>
                                                         <a data-toggle="modal" title="Add Charges" onclick="return showAddOrdrShippingChargeModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-shipping_charge="<?php echo $key->ord_shipping_charges; ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger red btn-outline filter-cancel">
                                                            <i class="fa fa-money" aria-hidden="true"></i>
                                                         </a>
                                                      <?php } ?>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Placed Order Section- End -->

                                 <!-- Dispatched Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'dispatched' ? 'active' : ''; ?>" id="tab_5_3">
                                    <div class="portlet-title">
                                       <!-- <div class="caption font-dark">
                                             <i class="icon-settings font-dark"></i>
                                             <span class="caption-subject bold uppercase">Product has been move to dispatched but not delivered</span>
                                          </div> -->
                                       <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th style="display: none;"> No. </th>
                                                <th>Order No.</th>
                                                <th>Order Source</th>
                                                <th>Placed On</th>
                                                <th>Payment mode</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_dispached as $key) {
                                             ?>
                                                <tr>
                                                   <td style="display: none;"></td>
                                                   <td><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->ord_reference_no) ?>"><?php echo $key->ord_reference_no ?></a></td>
                                                   <td><?php echo $key->pad_address ?></td>
                                                   <td><?php echo date('d-M-Y h:m:i A', strtotime($key->ord_crtd_dt)); ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><?php echo floatval($key->ord_total_amt) ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderStatusModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-order_status="<?php echo $key->ord_status ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                      <?php if ($key->ord_shipping_charges == 0) { ?>
                                                         <a data-toggle="modal" title="Add Charges" onclick="return showAddOrdrShippingChargeModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-shipping_charge="<?php echo $key->ord_shipping_charges; ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger filter-cancel">
                                                            <i class="fa fa-money" aria-hidden="true"></i>
                                                         </a>
                                                      <?php } ?>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Dispatched Order Section- End -->

                                 <!-- Delivered Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'delivered' ? 'active' : ''; ?>" id="tab_5_4">
                                    <div class="portlet-title">
                                       <!-- <div class="caption font-dark">
                                             <i class="icon-settings font-dark"></i>
                                             <span class="caption-subject bold uppercase">Product Order has been placed but dispatch is pending </span>
                                          </div> -->
                                       <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th style="display: none;"> No. </th>
                                                <th>Order No.</th>
                                                <th>Shipped to</th>
                                                <th>Placed On</th>
                                                <th>Payment mode</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_delivered as $key) {
                                             ?>
                                                <tr>
                                                   <td style="display: none;"></td>
                                                   <td><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->ord_reference_no) ?>"><?php echo $key->ord_reference_no ?></a></td>
                                                   <td><?php echo $key->pad_address ?></td>
                                                   <td><?php echo date('d-M-Y h:m:i A', strtotime($key->ord_crtd_dt)); ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><?php echo floatval($key->ord_total_amt) ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderStatusModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-order_status="<?php echo $key->ord_status ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Delivered Order Section- End -->

                                 <!-- Cancelled Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'cancelled' ? 'active' : ''; ?>" id="tab_5_5">
                                    <div class="portlet-title">
                                       <!-- <div class="caption font-dark">
                                             <i class="icon-settings font-dark"></i>
                                             <span class="caption-subject bold uppercase">Product has been move to dispatched but not delivered</span>
                                          </div> -->
                                       <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th style="display: none;"> No. </th>
                                                <th>Order No.</th>
                                                <th>Order Source</th>
                                                <th>Placed On</th>
                                                <th>Payment mode</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_cancelled as $key) {
                                             ?>
                                                <tr>
                                                   <td style="display: none;"></td>
                                                   <td><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->ord_reference_no) ?>"><?php echo $key->ord_reference_no ?></a></td>
                                                   <td><?php echo $key->pad_address ?></td>
                                                   <td><?php echo date('d-M-Y h:m:i A', strtotime($key->ord_crtd_dt)); ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><?php echo floatval($key->ord_total_amt) ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderStatusModal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-order_status="<?php echo $key->ord_status ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Cancelled Order Section- End -->

                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- END EXAMPLE TABLE PORTLET-->
                     <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     <!-- END EXAMPLE TABLE PORTLET-->
                  </div>
               </div>
            </div>
            <!-- END CONTENT BODY -->
         </div>
         <!-- END CONTENT -->
         <!-- BEGIN QUICK SIDEBAR -->
         <!-- END QUICK SIDEBAR -->
      </div>
      <!-- END CONTAINER -->
      <!-- START MODAL FOR CHANGE ORDER STATUS -->
      <div class="modal fade" id="changeOrderStatusModal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title" id="order-status-modal-title"></h4>
               </div>
               <form role="form" id="form_change_order_status">
                  <!-- HIDDEN FIELDS -->
                  <input type="hidden" name="order_id" id="order_id">
                  <input type="hidden" name="ord_prs_id" id="ord_prs_id">
                  <div class="modal-body">
                     <div class="row">
                        <div class="form-group col-md-6">
                           <label for="form_control_1">Select Status<span style="color:red">*</span></label>
                           <select class="form-control" name="ord_status" id="ord_status">
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="form_control_1">Remark</label>
                           <textarea id="ops_remark" name="ops_remark" class="form-control"></textarea>
                        </div>
                     </div>
                  </div>

                  <div class="modal-footer">
                     <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                     <button type="submit" id="form_submit" class="btn blue">Save changes</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!-- END MODAL FOR CHANGE ORDER STATUS -->

      <!-- START MODAL FOR ADD ORDER SHIPPING CHARGES -->
      <div class="modal" id="AddShippingCharge" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title" id="app-shipping-charge-modal-title"></h4>
               </div>
               <form role="form" id="formAddShippingCharge" method="POST">
                  <input type="hidden" name="order_id" id="order_id">
                  <input type="hidden" name="ord_prs_id" id="ord_prs_id">
                  <div class="modal-body">
                     <div class="form-group">
                        <label for="form_control_1">Enter Shipping Charges<span style="color:red">*</span></label>
                        <input class="form-control" name="ord_shipping_charges" id="ord_shipping_charges" placeholder="Shipping Charges" required />
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                     <button type="submit" id="form_submit" class="btn blue">Save changes</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!-- START MODAL FOR ADD ORDER SHIPPING CHARGES -->

      <!-- BEGIN FOOTER -->
      <?php echo $this->load->view('common/footer'); ?>
      <!-- END FOOTER -->
   </div>

   <!-- BEGIN QUICK NAV -->
   <div class="quick-nav-overlay"></div>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
   <!--<script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>-->
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN THEME GLOBAL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
   <!-- END THEME GLOBAL SCRIPTS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME LAYOUT SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>public/js/form_validation/update-order-status.js"></script>
   <script type="text/javascript">
      function deleteOrder(ord_id, ord_order_no) {
         var d = confirm("Are you sure you want to delete this order.");
         if (d == 1) {
            data = {
               ord_id: ord_id,
               ord_order_no: ord_order_no
            }

            $.ajax({
               type: "POST",
               // url: base_url + "MFS/savefree_trial", 
               url: base_url + "order/deleteOrder",
               data: data,
               dataType: "json",
               success: function(response) {
                  if (response.success == true) {
                     alert(response.message);
                     location.reload();
                     // $('#remove_shortlisted'+i).css('background-color', 'white');
                     //    $('#remove_shortlisted'+i).css('color', '#ea3d36');
                  } else {
                     alert(response.message);
                     location.reload();
                  }
               }
            });
            return false;
         }
      }
   </script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- <script type="text/javascript">
         var e=$(".pagination_table1");
         e.dataTable( {
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending", sortDescending: ": activate to sort column descending"
                }
                , emptyTable:"No data available in table", info:"Showing _START_ to _END_ of _TOTAL_ entries", infoEmpty:"No entries found", infoFiltered:"(filtered1 from _MAX_ total entries)", lengthMenu:"_MENU_ entries", search:"Search:", zeroRecords:"No matching records found"
            }
            , buttons:[ {
                extend: "print", className: "btn dark btn-outline"
            }
            , {
                extend: "pdf", className: "btn green btn-outline"
            }
            , {
                extend: "csv", className: "btn purple btn-outline "
            }
            ], rowReorder: {}
            , order:[[0, "asc"]], lengthMenu:[[25, 50, 100, 200, -1], [25, 50, 100, 200, "All"]], pageLength:50, dom:"<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
         }
         )
      </script> -->
</body>

</html>