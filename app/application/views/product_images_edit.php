<?php
if($this->session->userdata('tsn_usr_id')==''){
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="'.$abc.'"';
  echo '</script>';
}   
?>

<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
	<meta charset="utf-8" />
	<title>TSN | Edit <?php echo $product->prd_name?>'s Images</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="<?php echo base_url()?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="<?php echo base_url()?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN THEME LAYOUT STYLES -->
	<link href="<?php echo base_url()?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
	<link href="<?php echo base_url()?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME LAYOUT STYLES -->
	<style type="text/css">
	.mt-element-ribbon {
		padding: 0px; 

	}
</style>
<link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
	<!-- BEGIN HEADER -->
	<?php $this->load->view('common/header')?>  
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<?php $this->load->view('common/sidebar')?> 
		<!-- END SIDEBAR -->
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->

			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						  <a href="<?php echo site_url('dashboard')?>">Dashboard</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						  <a href="<?php echo site_url('products')?>">Products</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Images</span>
					</li>
				</ul>

			</div>
			<!-- END PAGE BAR -->

			<!-- BEGIN : RIBBONS -->

			<div class="row">

				<div class="col-lg-12">
					<div class="portlet light portlet-fit bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class=" icon-layers font-green"></i>
								<span class="caption-subject font-green bold uppercase"><?php echo $product->prd_name?> Images</span>

							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<?php
								foreach ($product_img as $key)
								{
									?>
									<div class="col-xs-3">
										<div class="product-wrapper">
											<span class="prd-overlay">
												<div class="overlay-content">
													<a onclick="deleteImg(<?php echo $key->img_id?>,'<?php echo $key->img_name?>')" title="Delete Image from <?php echo $product->prd_name?>" class="remove-product"> <i class="fa fa-times fa-lg" aria-hidden="true" ></i></a>
												</div>
											</span>

											<img  class="img-responsive product-img" src="<?php echo base_url().PRODUCT_BIG_IMAGE_PATH.$key->img_path?>" title="<?php echo $key->img_title?>" alt="<?php echo $key->img_alternate_text?>">
										</div>
									</div>

									<?php
								}
								?>

							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- END : RIBBONS -->
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->

	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php $this->load->view('common/footer')?> 
<!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="<?php echo base_url()?>public/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url()?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function deleteImg(img_id,img_name) 
	{ 
		var d=confirm("Are you sure you want to remove this image.");
	if(d==1)
	{
		
		data = {
			img_id:img_id,
			img_name:img_name
			}
		$.ajax({
			type: "POST",
			url:base_url+'product/deleteProductImage', 
			data : data,
			dataType:"json",
			success: function(response){

				if(response.success==true){
					alert(response.message);
					location.reload();
				}
				else{
					alert(response.messages);
					location.reload();

				}
			}

		});

		return false;


}
}
</script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>