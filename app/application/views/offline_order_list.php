<?php
if ($this->session->userdata('tsn_usr_id') == '') {
   $abc = base_url();
   echo '<script> ';
   echo 'window.location="' . $abc . '"';
   echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8" />
   <title>Prism Prints | Offline Orders</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="Preview page of Metronic Admin Theme #1 for rowreorder extension demos" name="description" />
   <meta content="" name="author" />
   <!-- BEGIN GLOBAL MANDATORY STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN THEME GLOBAL STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME GLOBAL STYLES -->
   <!-- BEGIN THEME LAYOUT STYLES -->
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
   <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>public/assets/pages/img/favicon.png" />
   <!-- END THEME LAYOUT STYLES -->
</head>
<!-- END HEAD -->
<!-- START SWEET ALERT -->
<style type="text/css">
   /* tab active colour*/
   .tabbable-custom>.nav-tabs>li.active {
      border-top: 3px solid #005dc1;
   }

   .portlet.light .dataTables_wrapper .dt-buttons {
      margin-top: -51px;
   }

   #count {
      color: red;
   }
</style>
<!-- END SWEET ALERT -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   <div class="page-wrapper">
      <!-- BEGIN HEADER -->
      <?php echo $this->load->view('common/header'); ?>
      <!-- END HEADER -->
      <!-- BEGIN HEADER & CONTENT DIVIDER -->
      <div class="clearfix"> </div>
      <!-- END HEADER & CONTENT DIVIDER -->
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
         <!-- BEGIN SIDEBAR -->
         <?php echo $this->load->view('common/sidebar'); ?>
         <!-- END SIDEBAR -->
         <!-- BEGIN CONTENT -->
         <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
               <div class="page-bar">
                  <ul class="page-breadcrumb">
                     <li>
                        <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                        <i class="fa fa-circle"></i>
                     </li>
                     <li>
                        <span>Offline Orders</span>
                     </li>
                  </ul>
               </div>
               <!-- END PAGE BAR -->
               <div class="row">
                  <div class="col-md-12">
                     <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     <div class="portlet light bordered">
                        <div class="portlet-title">
                           <div class="caption font-orange">
                              <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                              <span class="caption-subject bold uppercase">Offline Orders</span><br>
                           </div>
                           <div class="btn-group" style="padding-left: 10px">
                              <a href="<?php echo base_url('offline') ?>" class="btn green">
                                 Create Order <i class="fa fa-plus"></i>
                              </a>
                           </div>
                           <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                           <div class="tabbable-custom ">
                              <ul class="nav nav-tabs ">
                                 <li class="<?php echo $active = $tab  == 'all' ? 'active' : ''; ?>">
                                    <a href="#tab_5_1" data-toggle="tab"> All <span id="count"> (<?php echo count($orders) ?>) </span></a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'placed' ? 'active' : ''; ?>">
                                    <a href="#tab_5_2" data-toggle="tab">Placed <span id="count"> (<?php echo count($order_placed) ?>)</span></a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'dispatched' ? 'active' : ''; ?>">
                                    <a href="#tab_5_3" data-toggle="tab"> Dispatched <span id="count"> (<?php echo count($order_dispached) ?>)</span></a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'delivered' ? 'active' : ''; ?>">
                                    <a href="#tab_5_4" data-toggle="tab">Delivered<span id="count"> (<?php echo count($order_delivered) ?>)</span> </a>
                                 </li>
                                 <li class="<?php echo $active = $tab  == 'cancelled' ? 'active' : ''; ?>">
                                    <a href="#tab_5_5" data-toggle="tab"> Cancelled <span id="count"> (<?php echo count($order_cancelled) ?>)</span></a>
                                 </li>
                              </ul>
                              <div class="tab-content">
                                 <!-- All Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'all' ? 'active' : ''; ?>" id="tab_5_1">
                                    <div class="portlet-title">
                                       <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                       <!-- <table class="table table-striped table-bordered table-hover pagination_table1"> -->
                                       <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                          <thead>
                                             <tr>
                                                <th style="display: none;"> No. </th>
                                                <th>Order No.</th>
                                                <th>Username</th>
                                                <th class="text-center">Paid Amt</th>
                                                <th class="text-center">Pending Amt</th>
                                                <th class="text-right">Total</th>
                                                <th>Payment Status</th>
                                                <th class="text-center">Status</th>
                                                <th>Updated On</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($orders as $key) {
                                             ?>
                                                <tr>
                                                   <td style="display: none;"></td>
                                                   <td style="min-width: 150px"><a style="text-decoration: underline;" href="<?php echo site_url('offline_order/' . $key->offline_order_reference_no) ?>"><?php echo $key->offline_order_reference_no ?></a></td>
                                                   <td><?php echo $key->offline_user_username; ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_paid_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_pending_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_total); ?></td>
                                                   <td>
                                                      <?php
                                                      switch ($key->offline_order_payment_status) {
                                                         case 1:
                                                            echo "Pending";
                                                            break;
                                                         case 2:
                                                            echo "Paid";
                                                            break;
                                                         default:
                                                            echo "-";
                                                            break;
                                                      }
                                                      ?>
                                                   </td>
                                                   <td><?php echo $key->ord_status_name; ?></td>
                                                   <td><?php echo date('d-M-Y', strtotime($key->offline_order_updated_on)); ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Edit Order Detail" onclick="return showChangeOrderDetailModal(this)" data-order_id="<?php echo $key->offline_order_id ?>" data-ord_reference_no="<?php echo $key->offline_order_reference_no ?>" data-order_status="<?php echo $key->offline_order_status ?>" data-ord_prs_id="<?php echo $key->offline_order_user_id ?>" class="btn btn-danger filter-cancel order-list-action">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                      <a title="Print invoice" target="_blank" href="<?php echo site_url('offline_invoice/' . $key->offline_order_id); ?>" class="btn btn-danger filter-cancel" id="action-btn">
                                                         <i class="fa fa-print" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- All Order Section- End -->

                                 <!-- Placed Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'placed' ? 'active' : ''; ?>" id="tab_5_2">
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th>Order No.</th>
                                                <th>Username</th>
                                                <th>Payment Mode</th>
                                                <th class="text-center">Paid Amt</th>
                                                <th class="text-center">Pending Amt</th>
                                                <th class="text-right">Total</th>
                                                <th class="text-center">Status</th>
                                                <th>Updated On</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_placed as $key) {
                                             ?>
                                                <tr>
                                                   <td style="min-width: 150px"><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->offline_order_reference_no) ?>"><?php echo $key->offline_order_reference_no ?></a></td>
                                                   <td><?php echo $key->offline_user_username; ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_paid_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_pending_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_total); ?></td>
                                                   <td><?php echo $key->ord_status_name ?></td>
                                                   <td><?php echo date('d-M-Y', strtotime($key->offline_order_updated_on)); ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderDetailModal(this)" data-order_id="<?php echo $key->offline_order_id ?>" data-ord_reference_no="<?php echo $key->offline_order_reference_no ?>" data-order_status="<?php echo $key->offline_order_status ?>" data-ord_prs_id="<?php echo $key->offline_order_user_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Placed Order Section- End -->

                                 <!-- Dispatched Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'dispatched' ? 'active' : ''; ?>" id="tab_5_3">

                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th>Order No.</th>
                                                <th>Username</th>
                                                <th>Payment Mode</th>
                                                <th class="text-center">Paid Amt</th>
                                                <th class="text-center">Pending Amt</th>
                                                <th class="text-right">Total</th>
                                                <th class="text-center">Status</th>
                                                <th>Updated On</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_dispached as $key) {
                                             ?>
                                                <tr>
                                                   <td style="min-width: 150px"><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->offline_order_reference_no) ?>"><?php echo $key->offline_order_reference_no ?></a></td>
                                                   <td><?php echo $key->offline_user_username; ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_paid_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_pending_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_total); ?></td>
                                                   <td><?php echo $key->ord_status_name ?></td>
                                                   <td><?php echo date('d-M-Y', strtotime($key->offline_order_updated_on)); ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderDetailModal(this)" data-order_id="<?php echo $key->offline_order_id ?>" data-ord_reference_no="<?php echo $key->offline_order_reference_no ?>" data-order_status="<?php echo $key->offline_order_status ?>" data-ord_prs_id="<?php echo $key->offline_order_user_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Dispatched Order Section- End -->

                                 <!-- Delivered Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'delivered' ? 'active' : ''; ?>" id="tab_5_4">
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th>Order No.</th>
                                                <th>Username</th>
                                                <th>Payment Mode</th>
                                                <th class="text-center">Paid Amt</th>
                                                <th class="text-center">Pending Amt</th>
                                                <th class="text-right">Total</th>
                                                <th class="text-center">Status</th>
                                                <th>Updated On</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_delivered as $key) {
                                             ?>
                                                <tr>
                                                   <td style="min-width: 150px"><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->offline_order_reference_no) ?>"><?php echo $key->offline_order_reference_no ?></a></td>
                                                   <td><?php echo $key->offline_user_username; ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_paid_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_pending_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_total); ?></td>
                                                   <td><?php echo $key->ord_status_name ?></td>
                                                   <td><?php echo date('d-M-Y', strtotime($key->offline_order_updated_on)); ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderDetailModal(this)" data-order_id="<?php echo $key->offline_order_id ?>" data-ord_reference_no="<?php echo $key->offline_order_reference_no ?>" data-order_status="<?php echo $key->offline_order_status ?>" data-ord_prs_id="<?php echo $key->offline_order_user_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Delivered Order Section- End -->

                                 <!-- Cancelled Order Section- Start -->
                                 <div class="tab-pane <?php echo $active = $tab  == 'cancelled' ? 'active' : ''; ?>" id="tab_5_5">
                                    <div class="portlet-body table-responsive">
                                       <table class="table table-striped table-bordered table-hover pagination_table1">
                                          <thead>
                                             <tr>
                                                <th>Order No.</th>
                                                <th>Username</th>
                                                <th>Payment Mode</th>
                                                <th class="text-center">Paid Amt</th>
                                                <th class="text-center">Pending Amt</th>
                                                <th class="text-right">Total</th>
                                                <th class="text-center">Status</th>
                                                <th>Updated On</th>
                                                <th class="text-center">Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                             foreach ($order_cancelled as $key) {
                                             ?>
                                                <tr>
                                                   <td style="min-width: 150px"><a style="text-decoration: underline;" href="<?php echo site_url('orders/' . $key->offline_order_reference_no) ?>"><?php echo $key->offline_order_reference_no ?></a></td>
                                                   <td><?php echo $key->offline_user_username; ?></td>
                                                   <td><?php echo $key->ord_payment_mode_name ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_paid_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_pending_amt); ?></td>
                                                   <td class="text-right"><i class="fa fa-rupee"></i><?php echo floatval($key->offline_order_total); ?></td>
                                                   <td><?php echo $key->ord_status_name ?></td>
                                                   <td><?php echo date('d-M-Y', strtotime($key->offline_order_updated_on)); ?></td>
                                                   <td class="text-center">
                                                      <a data-toggle="modal" title="Change Status" onclick="return showChangeOrderDetailModal(this)" data-order_id="<?php echo $key->offline_order_id ?>" data-ord_reference_no="<?php echo $key->offline_order_reference_no ?>" data-order_status="<?php echo $key->offline_order_status ?>" data-ord_prs_id="<?php echo $key->offline_order_user_id ?>" class="btn btn-danger filter-cancel">
                                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                                      </a>
                                                   </td>
                                                </tr>
                                             <?php
                                             }
                                             ?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- Cancelled Order Section- End -->

                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- END EXAMPLE TABLE PORTLET-->
                     <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     <!-- END EXAMPLE TABLE PORTLET-->
                  </div>
               </div>
            </div>
            <!-- END CONTENT BODY -->
         </div>
         <!-- END CONTENT -->
         <!-- BEGIN QUICK SIDEBAR -->
         <!-- END QUICK SIDEBAR -->
      </div>
      <!-- END CONTAINER -->
      <!-- START MODAL FOR CHANGE ORDER STATUS -->
      <div class="modal fade" id="showChangeOrderDetailModal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title" id="order-status-modal-title"></h4>
               </div>
               <form role="form" id="form_update_order_detail">
                  <!-- HIDDEN FIELDS -->
                  <input type="hidden" name="order_id" id="order_id" />
                  <input type="hidden" name="ord_prs_id" id="ord_prs_id" />
                  <input type="hidden" name="order_total" id="order_total" />
                  <input type="hidden" name="paid_amount" id="paid_amount" />
                  <input type="hidden" name="pending_amount" id="pending_amount" />
                  <div class="modal-body">
                     <div class="row">
                        <div class="form-group col-md-6">
                           <span>Order Total: </span> <i class="fa fa-rupee"></i><span id="label_order_total"></span> <br>
                           <span>Paid Amount: </span> <i class="fa fa-rupee"></i><span id="label_paid_amount"></span> <br>
                           <span>Pending Amount: </span> <i class="fa fa-rupee"></i><span id="label_pending_amount"></span>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="form_control_1">Add Amount</label>
                           <input id="add_amount" name="add_amount" class="form-control" />
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group col-md-6">
                           <label for="form_control_1">Select Status<span style="color:red">*</span></label>
                           <select class="form-control" name="ord_status" id="ord_status">
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="form_control_1">Remark</label>
                           <textarea id="ops_remark" name="ops_remark" class="form-control"></textarea>
                        </div>
                     </div>

                  </div>

                  <div class="modal-footer">
                     <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                     <button type="submit" id="form_submit" class="btn blue">Save changes</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!-- END MODAL FOR CHANGE ORDER STATUS -->

      <!-- BEGIN FOOTER -->
      <?php echo $this->load->view('common/footer'); ?>
      <!-- END FOOTER -->
   </div>

   <!-- BEGIN QUICK NAV -->
   <div class="quick-nav-overlay"></div>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>   
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN THEME GLOBAL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
   <!-- END THEME GLOBAL SCRIPTS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME LAYOUT SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>public/js/form_validation/update-offline-order.js"></script>
   <script type="text/javascript">
      function deleteOrder(ord_id, ord_order_no) {
         var d = confirm("Are you sure you want to delete this order.");
         if (d == 1) {
            data = {
               ord_id: ord_id,
               ord_order_no: ord_order_no
            }

            $.ajax({
               type: "POST",
               // url: base_url + "MFS/savefree_trial", 
               url: base_url + "order/deleteOrder",
               data: data,
               dataType: "json",
               success: function(response) {
                  if (response.success == true) {
                     alert(response.message);
                     location.reload();
                     // $('#remove_shortlisted'+i).css('background-color', 'white');
                     //    $('#remove_shortlisted'+i).css('color', '#ea3d36');
                  } else {
                     alert(response.message);
                     location.reload();
                  }
               }
            });
            return false;
         }
      }
   </script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- <script type="text/javascript">
         var e=$(".pagination_table1");
         e.dataTable( {
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending", sortDescending: ": activate to sort column descending"
                }
                , emptyTable:"No data available in table", info:"Showing _START_ to _END_ of _TOTAL_ entries", infoEmpty:"No entries found", infoFiltered:"(filtered1 from _MAX_ total entries)", lengthMenu:"_MENU_ entries", search:"Search:", zeroRecords:"No matching records found"
            }
            , buttons:[ {
                extend: "print", className: "btn dark btn-outline"
            }
            , {
                extend: "pdf", className: "btn green btn-outline"
            }
            , {
                extend: "csv", className: "btn purple btn-outline "
            }
            ], rowReorder: {}
            , order:[[0, "asc"]], lengthMenu:[[25, 50, 100, 200, -1], [25, 50, 100, 200, "All"]], pageLength:50, dom:"<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
         }
         )
      </script> -->
</body>

</html>