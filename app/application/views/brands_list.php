<?php
   if($this->session->userdata('tsn_usr_id')==''){
      $abc = base_url();
        echo '<script> ';
          echo 'window.location="'.$abc.'"';
        echo '</script>';
    }   
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>TSN |  Flavours</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url()?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  
  <style type="text/css">
  #message

  {
    color:red;
  }
 
  .help-block
  {
   margin-bottom: 0;
   text-align: left;
   font-size: 12px;
   line-height: 15px;
   display: block;
   color: #c40000;
 }
 </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header')?>  
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar')?>  
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard')?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <span>Brands</span>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12 ">
              <div class="portlet light bordered">
              
        

                            <div class="portlet-title">
          <span class="caption-subject bold uppercase font-orange">Brands &nbsp; <a    id="addnewclick"  class="btn btn-sm red btn-outline filter-cancel">Add Brand</a></span>
          <div class="tools">


          </div>
        </div>

               <div class="portlet-body form" >
                 <!-- BEGIN FORM-->
                 <div class="form" id="addnewform" style="display:none">
                   <form  id="add_param" method="post" action="" class="horizontal-form">
                     <!-- HIDDEN FIELDS -->
                     <input type="hidden" id="brd_id" name="brd_id" value="">
                   <div class="form-body" style="padding: 0px;">
                       <div class="row">
                         <div class="col-md-3">
                           <div class="form-group">

                            <input class="form-control" placeholder="Name" id="brd_name" name="brd_name"  type="text" required >
                            <span class="help-block"></span> 
                          </div>
                        </div>
                     <div class="col-md-3">
                         <div class="form-group">

                          <input class="form-control" placeholder="Order" id="brd_order" name="brd_order"  type="text"  >
                          <span class="help-block"></span>
                        </div>
                      </div>
                       <div class="col-md-3">
                        <div class="form-group">
                         <select class="form-control"   name="brd_status" id="brd_status">
                           <option value="" disabled=""> Select Status</option>
                           <?php echo getDropdown('status');?>
                         </select>
                       </div>
                     </div>
                     <div class="col-md-3">
                      <div class="form-group">

                        <button type="submit"  class="btn blue" name="form_submit" id="form_submit" >
                         Save</button>
                         <button type="button" onclick="addnewclickCancel()" class="btn" name="form_submit_cancel" id="form_submit_cancel" >
                           Cancel</button>

                         </div>
                       </div>
                     </div>

                   </div>
                 </form>
                 <!-- END FORM-->
               </div>
             </div>

             <div class="portlet-body">
              <table class="table  table-bordered table-hover pagination_table dataTable no-footer" id="sample_1">
                <thead>
                  <tr>
                    <th style="display: none;">  </th>

                    <th>Name</th>
                    <th>Status</th>
                    <th>Order</th>
                   <th>Action</th> 


                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  foreach ($brands as $key ) 
                  {
                    ?>
                    <tr >
                     <td style="display: none"></td>

                     <td><?php echo $key->brd_name;?></td>
                       <td><?php echo $key->brd_status_name;?></td>
                     <td><?php echo $key->brd_order;?></td>
                    <td><a  class="edit" title="Edit <?php echo $key->brd_name;?>" id="updatenewclick" onclick="edit(<?php echo $key->brd_id;?>)"><i  class="fa fa-pencil" aria-hidden="true" ></i></a> </tr>
                    <?php 
                    $i++;  
                  }
                  ?>


                </tbody>
              </table>
            </div>
            </div>
       </div>
     </div>
   </div>
   <!-- END CONTENT BODY -->
 </div>
 <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php $this->load->view('common/footer')?> 
<!-- END FOOTER -->
</div>


<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script src="<?php echo base_url()?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>      
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url()?>public/assets/pages/scripts/table-datatables-rowreorder.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/form_validation/editable-brand.js"></script>  
<script src="<?php echo base_url();?>public/js/form_validation/add-brand.js"></script> 


</body>

</html>
