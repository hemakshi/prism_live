<?php
if ($this->session->userdata('tsn_usr_id') == '') {
   $abc = base_url();
   echo '<script> ';
   echo 'window.location="' . $abc . '"';
   echo '</script>';
}
?>

<!DOCTYPE html>

<html lang="en">

<head>
   <meta charset="utf-8" />
   <title>PRISM PRINTS | Quotation Details</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <!-- BEGIN GLOBAL MANDATORY STYLES -->

   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
   <!-- END GLOBAL MANDATORY STYLES -->

   <!-- BEGIN THEME GLOBAL STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME GLOBAL STYLES -->
   <!-- BEGIN THEME LAYOUT STYLES -->
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME LAYOUT STYLES -->
   <link rel="shortcut icon" href="favicon.ico" />
   <style type="text/css">
      .td-style {
         width: 10%;
         text-align: center;
      }

      .errormesssage {
         caret-color: black !important;
         color: #ff6161 !important;
         font-size: 12px;
         font-weight: 500;
      }
   </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('common/header') ?>
   <!-- END HEADER -->
   <!-- BEGIN HEADER & CONTENT DIVIDER -->
   <div class="clearfix"> </div>
   <!-- END HEADER & CONTENT DIVIDER -->
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
         <!-- BEGIN CONTENT BODY -->
         <div class="page-content">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
               <ul class="page-breadcrumb">
                  <li>
                     <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                     <i class="fa fa-circle"></i>
                  </li>
                  <li>
                     <a style="text-decoration: none" href="<?php echo site_url('quotations') ?>"> <span>Quotations</span></a>
                     <i class="fa fa-circle"></i>
                  </li>
                  <li>
                     <span><?php echo $quotationData->qr_reference_no; ?></span>
                  </li>
               </ul>
            </div>
            <div class=""></div>

            <div class="row">
               <div class="col-md-12 ">
                  <div class="portlet light bordered">
                     <div>
                        <div class="portlet-body">

                           <h5 class="form-section  no-margin-bottom bold">Quotation Details</h5>
                           <form role="form" id="quotation_detail_form" method="POST">
                              <input type="hidden" name="quotation_id" id="quotation_id" value="<?php echo $quotationData->qr_id; ?>" required />
                              <table class="table table table-bordered ">
                                 <tr>
                                    <th>Quotation Id</th>
                                    <td><?php echo $quotationData->qr_reference_no; ?></td>
                                    <th>Received Date</th>
                                    <td><?php echo date("d-M-Y h:i:s A", strtotime($quotationData->qr_created_on)); ?></td>
                                 </tr>

                                 <tr>
                                    <th>Product Name</th>
                                    <td><?php echo $quotationData->qr_product_name; ?></td>
                                    <th>Updated On</th>
                                    <td><?php echo date("d-M-Y h:i:s A", strtotime($quotationData->qr_updated_on)); ?></td>
                                 </tr>

                                 <tr>
                                    <th>Paper GSM & Size</th>
                                    <td><?php echo $quotationData->qr_paper_gsm_size; ?></td>
                                    <th>Creasing</th>
                                    <td><?php echo $quotationData->qr_creasing; ?></td>
                                 </tr>
                                 <tr>
                                    <th>Half Cut</th>
                                    <td><?php echo $quotationData->qr_half_cut; ?></td>
                                    <th>UV</th>
                                    <td><?php echo $quotationData->qr_uv; ?></td>
                                 </tr>
                                 <tr>
                                    <th>Lamination Type</th>
                                    <td><?php echo $quotationData->qr_lamination_type; ?></td>
                                    <th>Lamination</th>
                                    <td><?php echo $quotationData->qr_lamination; ?></td>
                                 </tr>


                                 <tr>
                                    <th>Staff Description</th>
                                    <td colspan="3">
                                       <textarea class="form-control" name="quotation_remarks" id="quotation_remarks" placeholder="Staff Description"><?php echo $quotationData->qr_remarks; ?></textarea>
                                    </td>
                                 </tr>

                                 <tr>
                                    <th>HSN</th>
                                    <td colspan="3">
                                       <input type="text" class="form-control" name="quotation_hsn" id="quotation_hsn" placeholder="HSN" value="<?php echo $quotationData->qr_hsn; ?>" required />
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Tax(%)</th>
                                    <td colspan="3">
                                       <input type="text" class="form-control" name="quotation_tax" id="quotation_tax" placeholder="Tax(%)" value="<?php echo $quotationData->qr_tax; ?>" required />
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Quantity</th>
                                    <td colspan="3"><?php echo $quotationData->qr_qty; ?></td>
                                 </tr>
                                 <tr>
                                    <th>Price (<i class="fa fa-rupee"></i>)</th>
                                    <td colspan="3">
                                       <input type="text" class="form-control" name="quotation_price" id="quotation_price" placeholder="Price" value="<?php echo $quotationData->qr_price; ?>" required />
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Total Price(with tax)</th>
                                    <td colspan="3">
                                       <input type="text" class="form-control" name="quotation_total_price" id="quotation_total_price" placeholder="Total Price" value="<?php echo $quotationData->qr_total_price; ?>" required />
                                    </td>
                                 </tr>
                              </table>
                              <a class="btn red" href="<?php echo site_url('quotations') ?>">Cancel</a>
                              <button type="submit" id="form_submit" class="btn blue">Save changes</button>
                           </form>
                           <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                     </div>
                     <!-- END CONTAINER -->

                  </div>
               </div>
            </div>

         </div>

         <!-- BEGIN FOOTER -->
         <?php $this->load->view('common/footer') ?>
         <!-- END FOOTER -->
      </div>
   </div>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
   <!-- END CORE PLUGINS -->

   <!-- BEGIN THEME GLOBAL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
   <!-- END THEME GLOBAL SCRIPTS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME LAYOUT SCRIPTS -->
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>public/js/form_validation/quotation-detail-update.js"></script>
</body>

</html>