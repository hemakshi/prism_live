<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
if ($this->session->userdata('session_offline_userid') === null || $this->session->userdata('session_offline_userid') == '') {
  $back = base_url() . "offline";
  echo '<script> ';
  echo 'window.location="' . $back . '"';
  echo '</script>';
}
$selectedUsername = $this->session->userdata('session_offline_username');
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Offline Cart</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- <link rel="shortcut icon"  href="<?php echo base_url(); ?>public/assets/global/img/favicon.png"> -->

  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }

    table {
      width: 100%;
      margin: 20px 0;
      border-collapse: collapse;
    }

    table,
    th,
    td {
      border: 1px solid #cdcdcd;
    }

    table th,
    table td {
      padding: 5px;
      text-align: left;
    }
  </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <a href="<?php echo site_url('offline') ?>">Create Order</a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <span>Cart- <?php echo $selectedUsername; ?></span>
            </li>
          </ul>
        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <span class="caption-subject bold uppercase">Cart - <?php echo $selectedUsername; ?></span>
                </div>
                <div class="tools"> </div>
              </div>
              <div class="portlet-body">
              </div>


              <?php
              if (count($this->cart->contents()) > 0) {

              ?>
                <form id="offlineCheckoutForm" name="offlineCheckoutForm" enctype="multipart/form-data" method="post">
                  <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-12">
                      <div class="cart-header">

                        <div id="result" class="text-center bold"></div>

                        <div class="my-cart-text">
                          <h3>Cart (<?php echo count($this->cart->contents()); ?> Items) <button class="btn btn-danger" id="clear_cart">Clear Cart</button></h3>
                        </div>
                        <div class="my-cart-price">
                          <h3>Total: Rs. <?php echo sprintf("%.2f", $this->session->userdata('carttotal')); ?> </h3>
                        </div>
                      </div>

                      <div class="prod-set">
                        <?php
                        $count = 0;
                        $i = 1;

                        foreach ($this->cart->contents() as $item) {
                          $count++;
                          // echo json_encode($item);
                        ?>
                          <input type="hidden" name="rowid" value="<?php echo $item['rowid'] ?>" />
                          <input type="hidden" name="rowid<?php echo $item['rowid'] ?>" value="<?php echo $item['rowid'] ?>" />
                          <input type="hidden" name="price<?php echo $item['rowid'] ?>" value="<?php echo $item['price'] ?>" />
                          <input type="hidden" name="category<?php echo $item['rowid'] ?>" value="<?php echo $item['category'] ?>" />
                          <div class="prod-item">
                            <div class="col2">
                              <div class="row m0">
                                <div class="col-md-7 col-lg-8 col-sm-8">
                                  <div class="prod-name pb-20">
                                    <span>Product Name: <?php echo $item["name"]; ?> </span>
                                  </div>
                                  <div class="row pb-20">

                                    <?php if ($item["category"] == CATEGORY_STICKER || $item["category"] == CATEGORY_BROCHURE_MIX) { ?>
                                      <div class="col-md-3 pb-10 text-center">
                                        <span>Height</span>
                                        <input type="text" class="updatecart allownumericwithdecimal" name="height<?php echo $item['rowid'] ?>" id="height<?php echo $item['rowid'] ?>" value="<?php echo $item['prd_height']; ?>" style="width: 70%;" data-rowid="<?php echo $item['rowid'] ?>">
                                      </div>

                                      <div class="col-md-3 pb-10 text-center">
                                        <span>Width</span>
                                        <input type="text" class="updatecart allownumericwithdecimal" name="width<?php echo $item['rowid'] ?>" id="width<?php echo $item['rowid'] ?>" value="<?php echo $item['prd_width']; ?>" style="width: 70%;" data-rowid="<?php echo $item['rowid'] ?>">
                                      </div>
                                    <?php } ?>

                                    <div class="col-md-3 pb-10 text-center qty-section">
                                      <span>Qty</span>
                                      <input type="button" name="minus" class="btn btn-xs btn-primary changeqty" value="-" data-rowid="<?php echo $item['rowid'] ?>" />
                                      <input type="text" class="updatecart" name="qty<?php echo $item['rowid'] ?>" value="<?php echo $item['qty'] ?>" id="qty<?php echo $item['rowid'] ?>" data-rowid="<?php echo $item['rowid'] ?>" />
                                      <input type="button" name="plus" class="btn btn-xs btn-primary changeqty" value="+" data-rowid="<?php echo $item['rowid'] ?>" />
                                    </div>

                                    <div class="col-md-3 pb-10 text-center">
                                      <span>Price</span><br>
                                      <span>Rs. <?php echo $this->cart->format_number($item['price']); ?> </span>
                                    </div>
                                  </div>

                                  <div class="custom-file mb-3" style="width:85%;">
                                    <input type="file" class="custom-file-input form-control file_input" name="file_input<?php echo $item['rowid'] ?>" id="file_input<?php echo $item['rowid'] ?>" class="custom-file-input saveFilePath" data-rowid="<?php echo $item['rowid'] ?>">
                                    <label style="font-size: 14px;" class="custom-file-label" for="file_input">Choose file</label>
                                  </div>

                                  <div class="edit-move-delete pb-10" id="remove_cart_product" data-cart_row='<?php echo $item["rowid"]; ?>'>
                                    <div class="actions"><span class="confirm-delete-item"><span class="text m-gray confirm-delete-item tappable">REMOVE</span></span></div>
                                  </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12">
                                  <div class="prod-price">
                                    Subtotal Rs. <?php echo $this->cart->format_number($item['prd_subtotal']); ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        <?php
                          $i++;
                        }

                        ?>
                      </div>

                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12" style="margin-top: 60px;">
                      <div class="grand-totall">
                        <div class="title-wrap">
                          <h4 class="cart-bottom-title section-bg-gary-cart">Cart Details</h4>
                        </div>
                        <h5>Cart total <span>Rs. <?php echo sprintf("%.2f", $this->session->userdata('carttotal')); ?> </span></h5>
                        <h5 class="shipping-charge-section">
                          <span class="label-shipping_charge"> Shipping Charges </span>
                          <span>
                            <input type="text" id="shipping_charge" name="shipping_charge" class="input-shipping-charge form-control" value="<?php echo sprintf("%.2f", $this->session->userdata('shipping_charge')); ?>" />
                          </span>
                        </h5>

                        <div class="cart-footer">
                          <h5 class="shipping-charge-section">Order Total<span>Rs. <?php echo sprintf("%.2f", $this->session->userdata('grandtotal')); ?> </span></h5>
                          <input type="hidden" id="grand_total" name="grand_total" value="<?php echo sprintf("%.2f", $this->session->userdata('grandtotal')); ?>" />
                        </div>

                      </div>
                    </div>

                    <div class="col-md-4 col-lg-4 col-sm-12 pt-10">
                      <div class="grand-totall">
                        <div class="title-wrap">
                          <h4 class="cart-bottom-title section-bg-gary-cart">Payment Details</h4>
                        </div>

                        <h5 class="shipping-charge-section">
                          <span class="label-shipping_charge"> Pay Amount </span>
                          <span>
                            <input type="text" id="pay_amount" name="pay_amount" required class="input-shipping-charge form-control" />
                          </span>
                        </h5>
                        <h5 class="shipping-charge-section">
                          <span class="label-shipping_charge"> Pending Amount </span>
                          <span>
                            <input type="text" id="pending_amount" name="pending_amount" readonly class="input-shipping-charge form-control" />
                          </span>
                        </h5>

                        <div class="cart-footer pt-10">
                          <!-- <button type="submit" class="place-order" name="checkout" value="checkout">Checkout</button> -->
                          <button type="submit" class="btn btn-lg blue" id="place_order" name="place_order">Place Order <i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                        </div>

                      </div>
                    </div>
                  </div>
                </form>
              <?php } else {
                echo "<h3 class='page-title empty-cart' >Your Cart is empty</h3>";
              }
              ?>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('common/footer') ?>
  <!-- END FOOTER -->
  <!-- BEGIN QUICK NAV -->
  <div class="quick-nav-overlay"></div>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <!-- <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
  <!-- <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script> -->
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <!-- END THEME LAYOUT SCRIPTS -->

  <!-- BEGIN CUSTOM SCRIPTS -->
  <script src="<?php echo base_url() ?>public/js/offline_cart.js"></script>
  <!-- END CUSTOM SCRIPTS -->

</body>

</html>