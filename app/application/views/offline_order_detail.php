<?php
if ($this->session->userdata('tsn_usr_id') == '') {
   $abc = base_url();
   echo '<script> ';
   echo 'window.location="' . $abc . '"';
   echo '</script>';
}
?>

<!DOCTYPE html>

<html lang="en">

<head>
   <meta charset="utf-8" />
   <title>Prism Prints | Order Details</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <!-- BEGIN GLOBAL MANDATORY STYLES -->

   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
   <!-- END GLOBAL MANDATORY STYLES -->

   <!-- BEGIN THEME GLOBAL STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME GLOBAL STYLES -->
   <!-- BEGIN THEME LAYOUT STYLES -->
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME LAYOUT STYLES -->
   <link rel="shortcut icon" href="favicon.ico" />
   <style type="text/css">
      .td-style {
         width: 10%;
         text-align: center;
      }

      .errormesssage {
         caret-color: black !important;
         color: #ff6161 !important;
         font-size: 12px;
         font-weight: 500;
      }
   </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('common/header') ?>
   <!-- END HEADER -->
   <!-- BEGIN HEADER & CONTENT DIVIDER -->
   <div class="clearfix"> </div>
   <!-- END HEADER & CONTENT DIVIDER -->
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
         <!-- BEGIN CONTENT BODY -->
         <div class="page-content">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
               <ul class="page-breadcrumb">
                  <li>
                     <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                     <i class="fa fa-circle"></i>
                  </li>
                  <li>
                     <a href="<?php echo site_url('offline_order_list') ?>">Offline Orders</a>
                     <i class="fa fa-circle"></i>
                  </li>
                  <li>
                     <span><?php echo $order->offline_order_reference_no ?></span>
                  </li>
               </ul>
            </div>
            <div class=""></div>
            <div class="row">
               <div class="col-md-12 ">
                  <div class="portlet light bordered">

                     <div class="portlet-body no-padding-top">
                        <form class="form-horizontal form" role="form">
                           <div class="form-body no-padding-top">
                              <h5 class="form-section  no-margin-bottom bold">
                                 Shipping Details:
                              </h5>
                              <div class="row">
                                 <div class="col-md-12 detail table-responsive">
                                    <table class="table table table-bordered">
                                       <tbody>
                                          <tr>
                                             <td>Order No</td>
                                             <td><?php echo $order->offline_order_reference_no ?></td>
                                             <td>Order Date</td>
                                             <td><?php echo date('d-M-Y', strtotime($order->offline_order_created_on)) ?></td>
                                          </tr>
                                          <tr>
                                             <td>Order Total</td>
                                             <td> <i class="fa fa-inr"></i> <?php echo floatval($order->offline_order_total) ?></td>
                                             <td>Customer Name</td>
                                             <td><?php echo $order->offline_user_username ?></td>

                                          </tr>
                                          <tr>
                                             <td>Payment Status</td>
                                             <td>
                                                <?php
                                                switch ($order->offline_order_payment_status) {
                                                   case 1:
                                                      echo "Pending";
                                                      break;
                                                   case 2:
                                                      echo "Paid";
                                                      break;
                                                   default:
                                                      echo "-";
                                                      break;
                                                }
                                                ?>
                                             </td>
                                             <td>Mobile No. </td>
                                             <td><?php echo $order->offline_user_mobile ?></td>
                                          </tr>

                                          <tr>
                                             <td>Email Id: </td>
                                             <td><?php echo $order->offline_user_email ?></td>
                                             <td>Shipping Address</td>
                                             <td>
                                                <div class="bold"><?php echo $order->offline_user_fullname . ","; ?></div>
                                                <div><?php echo $order->offline_user_address_line . ","; ?></div>
                                                <div><?php echo $order->offline_user_city; ?>- <?php echo $order->offline_user_pincode . ","; ?></div>
                                                <div><?php echo $order->offline_user_district . ","; ?></div>
                                                <div><?php echo $order->state_name; ?></div>
                                             </td>
                                          </tr>

                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>

                     <h5 class="form-section  no-margin-bottom bold">Products</h5>
                     <div>
                        <div class="portlet-body table-responsive">
                           <table class="table table table-bordered product ">
                              <thead>
                                 <th> Product </th>
                                 <th> File </th>
                                 <th> Price </th>
                                 <th> Quantity </th>
                                 <th> Height </th>
                                 <th> Width </th>
                                 <th> Total </th>
                              </thead>
                              <tbody>
                                 <?php
                                 foreach ($product as $key) {
                                 ?>
                                    <tr>
                                       <td id="product">
                                          <?php echo $key->prduct_name ?>
                                       </td>
                                       <td><a class="text-info" rel="noopener noreferrer"  download="<?php echo $key->oop_designfile; ?>"  href="<?php echo  base_url() . DESIGNFILE_UPLOAD_PATH . $key->oop_designfile; ?>"> <?php echo $key->oop_designfile; ?> </a></td>
                                       <td>
                                          <i class="fa fa-inr"></i>
                                          <?php echo floatval($key->oop_price); ?>
                                       </td>
                                       <td id="product">
                                          <?php echo $key->oop_quantity ?>
                                       </td>

                                       <td>
                                          <?php
                                          if (!empty($key->oop_height) && $key->oop_height != 0)
                                             echo $key->oop_height;
                                          else
                                             echo "-";
                                          ?>
                                       </td>
                                       <td>
                                          <?php
                                          if (!empty($key->oop_width) && $key->oop_width != 0)
                                             echo $key->oop_width;
                                          else
                                             echo "-";
                                          ?>
                                       </td>
                                       <td>
                                          <i class="fa fa-inr"></i>
                                          <?php echo floatval($key->oop_total) ?>
                                       </td>
                                    </tr>
                                 <?php
                                 }
                                 ?>

                              </tbody>
                           </table>

                        </div>
                     </div>
                     <h5 class="form-section  no-margin-bottom bold">Payment Detail</h5>

                     <table class="table table table-bordered product ">
                        <thead>
                           <tr>
                              <th> Payment Amount </th>
                              <!-- <th> Payment Status </th> -->
                              <th> Paid Amount </th>
                              <th> Pending Amount </th>
                              <th> Payment Mode </th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td> <i class="fa fa-inr"></i> <?php echo floatval($order->offline_order_total) ?></td>
                              <td> <i class="fa fa-inr"></i> <?php echo $order->offline_order_paid_amt ?></td>
                              <td> <i class="fa fa-inr"></i> <?php echo $order->offline_order_pending_amt ?></td>
                              <td><?php echo $order->ord_payment_mode_name ?></td>
                              <!-- <td>
                                 <?php
                                 switch ($order->offline_order_payment_status) {
                                    case 1:
                                       echo "Pending";
                                       break;
                                    case 2:
                                       echo "Paid";
                                       break;
                                    default:
                                       echo "-";
                                       break;
                                 }
                                 ?>
                              </td> -->
                           </tr>
                        </tbody>
                     </table>
                     <div class="row">
                        <div class="col-sm-offset-8  col-sm-4 invoice-block no_padding">
                           <div class="table-responsive">
                              <table class="table " style="    margin-bottom: 0px;">
                                 <tbody>
                                    <tr>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <strong>Sub Total:
                                          </strong>
                                       </td>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <i class="fa fa-inr"></i>
                                          <?php echo floatval($order->offline_order_subtotal) ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <strong> Shipping Charges:
                                          </strong>
                                       </td>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <i class="fa fa-inr"></i>
                                          <?php echo floatval($order->offline_order_shipping_charges) ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <strong>Total:
                                          </strong>
                                       </td>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <i class="fa fa-inr"></i>
                                          <?php echo floatval($order->offline_order_total) ?>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END CONTENT BODY -->
               </div>
               <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->

            <!-- BEGIN FOOTER -->
            <?php $this->load->view('common/footer') ?>
            <!-- END FOOTER -->
         </div>

         <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
         <!-- END CORE PLUGINS -->

         <!-- BEGIN THEME GLOBAL SCRIPTS -->
         <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
         <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
         <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
         <!-- END PAGE LEVEL SCRIPTS -->
         <!-- BEGIN THEME LAYOUT SCRIPTS -->
         <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url(); ?>public/js/form_validation/update-order-product-status.js"></script>
</body>

</html>