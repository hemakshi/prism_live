<div class="profile-usermenu">
    <ul class="nav">
        <li class="active">
            <a href="<?php echo site_url('interior-requests')?>">
                <i class="icon-home"></i> Requests 
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('interior-order')?>">
                <i class="icon-info"></i>Place Order
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('interior-account')?>">
                <i class="icon-settings"></i> Account 
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('interior-tracking')?>">
                <i class="icon-settings"></i> Tracking 
            </a>
        </li>
    </ul>
</div>