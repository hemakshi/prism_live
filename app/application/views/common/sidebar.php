<style type="text/css">
  .page-sidebar .page-sidebar-menu>li:second-child>a {
    border-top: none;
  }

  .nav-item:hover .badge-orange {
    background: #81A937;
  }


  @media (min-width: 992px) {
    .page-sidebar {
      position: fixed !important;
      margin-left: 0;
      top: 50px;
      text-shadow: 0 0 black;
    }
  }
</style>

<div class="page-sidebar-wrapper">
  <!-- BEGIN SIDEBAR -->
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse">
    <div class="scroller" style="height:100vh;padding-right:0;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
      <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding: 44px 0">
        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
        <li class="sidebar-toggler-wrapper hide">
          <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
          <div class="sidebar-toggler">
            <span></span>
          </div>
          <!-- END SIDEBAR TOGGLER BUTTON -->
        </li>
        <?php
        $res = $this->access_model->getMenu();

        $i = 1;
        foreach ($res->result() as $row) {

          $link = '';
          $res1 = $this->access_model->getsubMenu($row->mnu_id);

          if ($row->mnu_link == '') {

            $link = 'javascript:;';
            echo '<li class="nav-item">
                            <a href="' . $link . '" class="nav-link nav-toggle">
                                <i class="' . $row->mnu_icon . '""style="color: #f1f4f7;"></i>&nbsp;&nbsp;
                                <span class="title" >' . $row->mnu_name . '</span>
                                <span class="selected"></span>
                            </a>';

            if (!empty($res1)) {
              echo '<ul class="sub-menu">';
              foreach ($res1->result() as $rw) {
                $res2 = $this->access_model->getpages($rw->sbm_id, $rw->sbm_mnu_id);
                // $res2 = $this->access_model->getpages($rw->page_id,$rw->module_id);
                if ($rw->sbm_pagelink == '') {
                  $link1 = 'javascript:;';

                  echo '<li class="nav-item">
                      <a href="' . $link . '" class="nav-link nav-toggle" >
                                        <i class="icon-settings"></i> ' . $rw->sbm_name . '
                                        <span class="arrow"></span>
                                    </a>';
                  if (!empty($res2)) {
                    echo '<ul class="sub-menu">';
                    foreach ($res2->result() as $rw1) {

                      echo '<li class="nav-item"><a href="' . site_url("/" . $rw1->form_name) . '" class="nav-link">
                      <i class="fa fa-angle-double-right" aria-hidden="true"></i> ' . $rw1->form_title . '</a></li>';
                    }
                    echo '</ul>';
                  }
                } else {
                  $link = $rw->sbm_pagelink;

                  echo '<li class="nav-item">
                      <a  href="' . site_url($link) . '" class="nav-link" >
                          <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                          ' . $rw->sbm_name . '
                      </a>';
                }
                /*echo '<li><a href="'.site_url($rw->sbm_pagelink).'">'.$rw->sbm_name.'</a>';*/
                echo '</li>';
              }
              echo '</ul>';
            }
          } else {
            $link = $row->mnu_link;
            $check_active_tab = "";
            if ($this->uri->segment(1) ==  $link || $this->uri->segment(1) == "") {
              $check_active_tab = "active";
            } else {
              $check_active_tab = "";
            }

            echo '<li class="nav-item  ' . $check_active_tab . '">
                            <a href="' . site_url($link) . '" class="nav-link nav-toggle">
                                <i class="' . $row->mnu_icon . '"></i>&nbsp;&nbsp;
                                <span class="title">' . $row->mnu_name . '</span>
                                <span class="selected"></span>';
            echo '  </a>';
            echo '</li>';
          }
          $i++;
        }
        ?>
        </li>

      </ul>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
</div>