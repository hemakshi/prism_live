<style type="text/css">
    .pagination>li>a,
    .pagination>li>span {
        color: #81A937;
    }

    .glyphicon.spinning {
        animation: spin 1s infinite linear;
        -webkit-animation: spin2 1s infinite linear;
    }

    .status {
        color: #81A937 !important;
        font-weight: bold;
        padding: 12px 15px;
        text-align: center;
    }

    @media only screen and (max-width: 768px) {
        .status {
            display: none;
        }

    }

    .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
        z-index: 2;
        color: #fff;
        background-color: #67872c;
        border-color: #67872c;
        cursor: default;
    }

    .external {
        background: #364150 !important;
    }

    .page-header.navbar .top-menu .navbar-nav>li.dropdown .dropdown-toggle:hover,
    .page-header.navbar .top-menu .navbar-nav>li.dropdown.open .dropdown-toggle {
        background: #fff;
    }


    .icon-bell {
        color: #81A937 !important;
    }

    .fa-bell {
        display: none !important;
    }

    .open .icon-bell {
        display: none;
    }

    .open .fa-bell {
        display: inline-block !important;
        color: #81A937 !important;
    }

    .page-header.navbar .top-menu .navbar-nav>li.dropdown .dropdown-menu:before {
        right: 19px;
    }

    .page-header.navbar .top-menu .navbar-nav>li.dropdown .dropdown-menu:after {
        right: 18px;
    }

    .page-header.navbar .top-menu .navbar-nav>li.dropdown .dropdown-toggle>i {
        color: #81A937;
    }

    .external h3,
    .external a {
        color: #fff !important;
    }
    }

    .icon-bell {
        color: #81A937 !important;
    }





    .page-header.navbar .top-menu .navbar-nav>li.dropdown-extended .dropdown-menu {
        min-width: 160px;
        max-width: 290px;
        width: 290px;
        z-index: 9995;
    }

    @media (max-width: 767px) {
        .page-header.navbar .top-menu .navbar-nav>li.dropdown-notification .dropdown-menu {
            margin-right: -50px;
        }
    }

    ] @keyframes spin {
        from {
            transform: scale(1) rotate(0deg);
        }

        to {
            transform: scale(1) rotate(360deg);
        }
    }

    @-webkit-keyframes spin2 {
        from {
            -webkit-transform: rotate(0deg);
        }

        to {
            -webkit-transform: rotate(360deg);
        }
    }

    .dashboard-stat.dashboard-stat-v2 .visual {
        padding-top: 0px;
        margin-bottom: 40px;
    }

    .custom {
        display: none !important;
    }


    @media only screen and (max-width: 768px) {
        .page-header.navbar .page-logo .logo-default {
            display: block;
            margin: 0 auto;
            width: 70%;
        }

        .page-header.navbar .menu-toggler>span,
        .page-header.navbar .menu-toggler>span:before,
        .page-header.navbar .menu-toggler>span:after {
            background: #000 !important;
            width: 20px;
            height: 2px;
        }

        .custom {
            display: initial !important;
            float: left !important;
        }

        .default {
            display: none !important;
        }

        .navbar-fixed-top {
            position: fixed !important;
        }

        .page-content-wrapper {
            margin-top: 60px;
        }

        .page-content-white .page-content .page-bar {
            position: fixed;
            width: 105%;
            z-index: 2;
        }

        .page-content-white .page-content .page-bar~div {
            margin-top: 30px;
        }

        .page-sidebar-wrapper {
            position: fixed;
            width: 100%;
            z-index: 3;
        }

        .page-header.navbar {
            height: 60px;
            padding: 5px 0;
        }

        .page-header.navbar .top-menu .navbar-nav>li.dropdown-extended .dropdown-menu .dropdown-menu-list>li>a {
            border-bottom: 3px solid #EFF2F6 !important;
            color: #888;
        }

        .logo-default {
            width: 170px;
            margin: 0px !important;
        }

        .page-sidebar .page-sidebar-menu {
            padding-top: 60px !important;
        }

        .page-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
        }
    }

    @media only screen and (max-width: 480px) {
        .page-header.navbar .top-menu .navbar-nav>li.dropdown-user .dropdown-toggle>img {
            height: 40px !important;
            margin-top: -12px;
        }

        .page-header.navbar .top-menu {
            display: block;
            clear: none;
            float: none;
        }

        .page-header.navbar .top-menu .navbar-nav>li.dropdown-user .dropdown-toggle {
            background: none !important;
        }
    }

    span.error {
        font-weight: 400;
        color: red !important;
    }

    .page-header.navbar .top-menu .navbar-nav>li.dropdown-extended .dropdown-menu .dropdown-menu-list>li>a:hover {
        background: initial !important;
    }

    @media only screen and (max-width: 767px) {
        .page-header.navbar .top-menu .navbar-nav>li.dropdown .dropdown-menu:before {
            right: -125px !important;
        }

        .page-header.navbar .top-menu .navbar-nav>li.dropdown .dropdown-menu:after {
            right: -126px !important;
        }
    }

    @media only screen and (min-width:768px) {
        .page-header {
            box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.3);
        }

        .page-header.navbar .page-logo .logo-default {
            display: block;
            margin: 0 auto;
            background: #fff;
            padding: 0 8px 0 2px;
            /* box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.3);*/
        }

    }

    @media only screen and (max-width: 480px) {
        .dataTables_wrapper .dataTables_length {
            float: left !important;
        }

        .dataTables_wrapper .dataTables_filter {
            margin-top: 0 !important;
            float: right;
        }

        .dt-buttons {
            display: none !important;
        }

        .portlet.light .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
        }
    }

    .nav-item .active .fa,
    .nav-item .active span {
        color: #81A937 !important;
    }

    .bg-green {
        background: #81A937 !important;
    }

    .form-control.error {
        border: 1px solid #ff6161;
    }

    label.error {
        font-size: 12px;
        color: #ff6161;
        /*display: none!important;*/
    }
</style>
<div class="page-header navbar navbar-fixed-top" style="    border-bottom: 1px solid #1e4e7f; background-color: white;">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <a href="javascript:;" class="menu-toggler responsive-toggler custom" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo site_url('dashboard') ?>" style="width: 80%;">
                <img src="<?php echo base_url() . '/' . $this->home_model->getBsnData('logo'); ?>" alt="logo" class="logo-default img-responsive" /> </a>
            <div class="menu- sidebar-toggler">
                <span></span>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler default" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <span class="btn" style="font-weight: bold;">
                    <i class="fa fa-user"></i>
                    <?php echo $this->session->userdata('tsn_usr_username') ? $this->session->userdata('tsn_usr_username') : "Admin"; ?>
                </span>

                <a href="<?php echo site_url('logout') ?>" class="btn green pull-right"> Logout </a>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>