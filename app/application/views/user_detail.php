<?php
if ($this->session->userdata('tsn_usr_id') == '' || $this->session->userdata('tsn_usr_dpt_id') != ADMIN_DEPARTMENT) {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | <?php echo $user->usr_username; ?> Detail</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <link href="<?php echo base_url() ?>public/assets/xlsTableFilter/css/xlstablefilter.css" type="text/css">
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- <link rel="shortcut icon" href="favicon.ico" /> -->
  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }

    .table td:first-child {
      width: 300px !important;
    }
  </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>

            <li>
              <a href="<?php echo site_url('user_list') ?>">User List</a>
            </li>
          </ul>

        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <!-- <i class="icon-user font-orange"></i> -->
                  <span class="caption-subject bold uppercase">User</span>
                </div>
                <div class="btn-group" style="padding-left: 10px">
                  <a href="<?php echo site_url('user_edit/' . $user->usr_id) ?>" class="btn green">
                    <i class="fa fa-edit"></i>
                  </a>
                </div>

                <div class="tools"> </div>
              </div>
              <div class="portlet-body">
                <div class="detail">
                  <div class="row">
                    <!-- <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="border-right: 1px solid #e5e5e5">
                      <div class="mt-widget-1" style="  border: 0px ">
                        <div class="mt-img" style="margin: 10px 0 10px;">
                          <?php
                          if ($user->prs_img == '') {
                            $prs_image = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                          } else {
                            $prs_image = base_url() . PERSON_IMAGE_PATH . $user->prs_img;
                            // $cr_image='http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';
                          }
                          ?>
                          <img style="width:80px;height:80px" src="<?php echo $prs_image ?>"> </div>
                        <div class="mt-body">
                          <div class="person-info" style="">
                            <a href="" class="nav-link nav-toggle">
                              <h3 class="mt-username"></h3>
                            </a>
                          </div>

                        </div>
                      </div>
                    </div> -->
                    <div class="col-md-9">
                      <!-- <?php var_dump($user); ?> -->
                      <table class="table table-striped table-bordered table-hover table-checkable order-column">
                        <tr>
                          <td>Full Name</td>
                          <td><?php echo $user->usr_fullname; ?></td>
                          <td>User Name</td>
                          <td><?php echo $user->usr_username; ?></td>
                        </tr>
                        <tr>
                          <td>Mobile No</td>
                          <td><?php echo $user->usr_mobile; ?></td>
                          <td>Email</td>
                          <td><?php echo $user->usr_email; ?></td>
                        </tr>
                        <tr>
                          <td>Department</td>
                          <td><?php echo $user->dpt_name; ?></td>
                          <td>Password</td>
                          <td><?php echo openssl_decrypt(trim($user->usr_password), CIPHER, KEY); ?></td>
                        </tr>
                        <tr>
                          <td>Status</td>
                          <td>
                            <?php
                            switch ($user->usr_status) {
                              case 1:
                                echo "Active";
                                break;
                              case 0:
                                echo "Deactive";
                                break;
                              default:
                                echo "-";
                                break;
                            }
                            ?>
                          </td>

                        </tr>
                      </table>
                    </div>
                  </div>
                  <!-- <table class="table table-striped table-bordered table-hover table-checkable order-column">
                    <tr>
                      <td>Zone</td>
                      <td><?php echo $user->prs_zone; ?></td>
                      <td>Designation</td>
                      <td><?php echo $user->prs_designation; ?></td>
                      <td>reporting To</td>
                      <td><?php $data = $this->user_model->getPersonNameById($user->prs_reporting_to);
                          if (!empty($data)) {
                            echo $data->prs_name;
                          } else {
                            echo '--';
                          } ?></td>
                    </tr>
                    <tr>
                      <td>Address</td>
                      <td><?php echo $user->prs_address ?></td>
                    </tr>
                  </table> -->

                </div>

              </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>

      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('common/footer') ?>
  <!-- END FOOTER -->
  <!--[if lt IE 9]>
<script src="<?php echo base_url() ?>public/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
  <!-- BEGIN CORE PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="<?php echo base_url() ?>public/assets/xlsTableFilter/js/jquery.xlstablefilter.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <!-- END THEME LAYOUT SCRIPTS -->
  <script type="text/javascript">
    $(function() {
      $('#sample_1_length').after(' <div style="text-align:center;color:darkred;margin:3px;font-weight:bold" id="divRowsDisplay"></div');
      $("#sample_1").xlsTableFilter({
        rowsDisplay: "divRowsDisplay"
      });
    });

    $('.ui-dialog-buttonset > .ui-button').click(function() {
      $('#divRowsDisplay').css('display', 'block');
    });


    $(document).keypress(function(e) {
      if (e.which == 13) {
        $('.ui-dialog-buttonset > .ui-button').click();
      }
    });
  </script>

</body>

</html>