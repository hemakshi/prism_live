<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
   <![endif]-->
   <!--[if IE 9]> 
   <html lang="en" class="ie9 no-js">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en">
         <!--<![endif]-->
         <!-- BEGIN HEAD -->
         <head>
            <meta charset="utf-8" />
            <title>Order Invoice </title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1" name="viewport" />
            <meta content="" name="description" />
            <meta content="" name="author" />
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="<?php echo base_url()?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN PAGE LEVEL STYLES -->
            <link href="<?php echo base_url()?>public/assets/pages/css/invoice-2.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="<?php echo base_url()?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="<?php echo base_url()?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="<?php echo base_url().'/'.$this->home_model->getBsnData('logo'); ?>" />
         </head>
         <!-- END HEAD -->
         <style type="text/css">
            @media only screen and (max-width: 480px){
            .txtarea2{
            width: 100px!important;
            }
            .txtarea3{
            width: 180px!important;
            }
            .table_color{background: #1f1c1c!important;    color: white!important}    
            }
         </style>
         <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" onload="window.print();">
            <!-- BEGIN HEADER -->
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
               <!-- BEGIN SIDEBAR -->
               <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
               <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
               <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="invoice-content-2 bordered" style="     padding: 0px 0px;    border: 1px solid rgba(0, 0, 0, 0.5);padding: 5px">
            <div class="row invoice-head" style="margin-bottom: 20px;" >
               <div class="col-xs-12 text-left">
                  <img src="<?php echo base_url().'/'.$this->home_model->getBsnData('logo'); ?>">
                   <div class="">
                  <h3 class="bold uppercase" style="color:#7e8691 ;">H & V ORGANIC FARM FRESH LIMITED</h3>
                     <p>SCO 87, MANSA DEVI COMPLEX,<br/>
                       SECTOR 5,PANCHKULA - 134109, <br/>
                     HARYANA, INDIA  <br/>
                     <span class="bold">Contact No.</span> - +(91)-172-5064242 <br/>
                     <span class="bold">GST</span> - 02AADCH9851L1ZC</p>
               </div>
               <div class="text-center">
                  <h4 class="bold uppercase underline" style="text-decoration: underline;font-size: 27px;">Retail Invoice</h4>
               </div>
               </div>
               <div class="text-left"></div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <h4 class="" style="color:#7e8691 ;">Order No: <span class="bold"><?php echo $order->ord_reference_no ?></span></h4>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                 <p> <h4 class="" style="color: #7e8691;">Date <span class="bold"><?php echo date('d-M-Y',strtotime($order->ord_date)) ?></span></h4></p>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-top: 0px">
                  <div class="company-address" style="    text-align: left!important;">
                     <img class="txtarea3" style="width: 250px;padding-bottom: 0px"  src="<?php echo base_url()?>logo_image/nextasy_1.png"   alt="" /><br>
                     <span class="uppercase">Customer Name : <span class="bold"><?php echo $order->prs_name ?></span></span>
                    <br/><span class="uppercase">Contact No: <span class="bold"><?php echo $order->prs_mob ?></span></span>
                     <br/><div style="max-width: 300px;"><?php echo $order->pad_location_id_name?> <p><strong> <?php echo $order->pad_state_name?> <?php echo $order->pad_city?> </strong></p>
                                                            <?php echo  $order->pad_building  != '' ? '<p>Premises / Building / Complex:'.$order->pad_building.'</p>' : '';?>
                                                            <?php echo  $order->pad_wing  != '' ? '<p>Wing / Part / Floor:'.$order->pad_wing.'</p>' : '';?>
                                                            <?php echo  $order->pad_flat_no  != '' ? '<p>Flat / Room / House No.'.$order->pad_flat_no.'</p>' : '';?>
                                                            <?php echo  $order->pad_road  != '' ? '<p>Road:'.$order->pad_road.'</p>' : '';?>
                                                         <?php echo  $order->pad_area  != '' ? '<p>Area:'.$order->pad_area.'</p>' : '';?>
                                                           <p> <b><?php if($order->pad_pincode != '') { echo 'Pin no. : '.$order->pad_pincode; } ?> </b> </p></div> <br/> 
                  </div>
               </div>
              
            </div>
           <!--  <hr style="border-top: 1px solid #17899f;    margin: 5px 0 10px 0;"> -->
          <!--   <div class="row invoice-cust-add" style="margin-bottom: 0px;">
               <div class="col-lg-6 col-md-6 col-xs-6" style="text-align: left;">
                  <table class="table " style="    margin-bottom: 0px;">
                     <tbody>
                        <tr style="">
                           <td style="width: 35%;border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"> Bill To :  </td>
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"><strong> Flats And Mates Online LLP</strong> </td>
                        </tr>
                        <tr style="">
                           <td style="width: 35%;border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"> Address : </td>
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"><strong>15/B, MILI CHS, LT.DILIP GUPTE MARG, MAHIM (W), MUMBAI Mumbai City MH 400016 IN</strong> </td>
                        </tr>
                        <tr style="">
                           <td style="width: 35%;border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"> GSTIN :</td>
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"> <strong></strong> </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <div class="col-lg-1 col-md-1 col-xs-1" style="text-align: left;">
               </div>
               <div class="col-lg-5 col-md-5 col-xs-5" style="text-align: left;">
                  <table class="table " style="    margin-bottom: 0px;">
                     <tbody>
                        <tr style="">
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"> Invoice No. : </td>
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"><strong>INV10026</strong> </td>
                        </tr>
                        <tr style="">
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"> Invoice Date : </td>
                           <td style="border-top: 0px solid #e7ecf1;   padding-bottom: 5px;padding-top: 5px;"><strong>  18-Feb-16</strong> </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div> -->
            <!-- <hr style="border-top: 1px solid #17899f;margin: 0px 0 10px 0;"> -->
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <table class="table table-bordered table-hover">
                     <thead class="table_color " style="    background: #1f1c1c!important;    color: white!important;">
                        <tr>
                           <th> Sr.No. </th>
                           <th> Product Name </th>
                           <th style="width: 10px"> Qty </th>
                           <th style="width: 10px"> Price </th>
                             <th style="width: 10px"> Sub-Total </th>
                            <th style="width: 10px"> Discount </th>
                           <th style="width: 80px"> Total </th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i=1;  foreach ($product as $key)
                                  { ?>
                        <tr>
                           <td> <?php echo $i++; ?></td>
                           <td style="font-size: 11px;"><?php echo $key->prdname_concat?>  </td>
                           <td style="text-align: center;font-size: 11px;" ><?php echo $key->odp_quantity?></td>
                           <td style="text-align: center;font-size: 11px;" ><?php echo $key->prd_price?></td>
                           <td style="text-align: center;font-size: 11px;" ><?php echo $key->odp_amt?></td>
                           <td style="text-align: center;font-size: 11px;" ><?php if($key->odp_disc_type == '1') { echo  $key->odp_disc_amt. ' %'; } else { echo '<i class="fa fa-inr" aria-hidden="true"></i> '. $key->odp_disc_amt;  } ?></td>
                           <td style="text-align: right;font-size: 11px;" > <?php echo $key->odp_total_amt?> </td>
                        </tr>
                        <?php } ; ?>
                     </tbody>
                  </table>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-lg-8 col-md-8 col-xs-8">
                  <div class="">
                     <address>
                       <!--  <strong>Payment is due 14 days from invoice date.</strong>
                        <br/>Payment should be made by bank transfer to the following account:
                        <br/>Nextasy Technologies
                        <br/> Current Account
                        <br/> Bank Name : ICICI Bank
                        <br/>Account Number : 120905500290
                        <br/> IFSC code : ICIC0001209
                        <br/> Mahavir Nagar Branch -->
                     </address>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-xs-4 invoice-block" style="float: right;">
                  <table class="table " style="    margin-bottom: 0px;">
                     <tbody>
                        <tr>
                           <td style="border-top: 0px solid #e7ecf1;  padding: 0px;">Sub-Total: </td>
                           <td style="border-top: 0px solid #e7ecf1;  padding: 0px;text-align: right;"><strong class="currency"><i class="fa fa-inr"></i> <?php echo indian_number_format($order->ord_sub_total)?> </strong> </td>
                        </tr>
                        <tr>
                           <td style="border-top: 0px solid #e7ecf1;  padding: 0px;">Discount: </td>
                           <td style="border-top: 0px solid #e7ecf1;  padding: 0px;text-align: right;"><strong class="currency"><i class="fa fa-inr"></i> <?php echo indian_number_format($order->ord_sub_total-$order->ord_total_amt); ?>  </strong> </td>
                        </tr> 
                        <tr>
                        <td style="border-top: 0px solid #e7ecf1;  padding: 0px;"> Final Amount: </td>
                        <td style="border-top: 0px solid #e7ecf1;  padding: 0px;text-align: right;"> <strong class="currency"><i class="fa fa-inr"></i> <?php echo indian_number_format($order->ord_total_amt)?></strong> </td>
                        </tr>
                        <tr>
                           <td style="border-top: 0px solid #e7ecf1;  padding: 0px;">Paid By Cash: </td>
                           <td style="border-top: 0px solid #e7ecf1;  padding: 0px;text-align: right;"><strong class="currency"> <i class="fa fa-inr"></i>  <?php echo indian_number_format($order->ord_total_amt)?></strong> </td>
                        </tr>
                     </tbody>
                  </table>
                  <ul class="list-unstyled amounts">
                     <li>
                       <!--  <img  class="txtarea2" style="width: 200px;" src="<?php echo base_url()?>logo_image/paid.png" alt="paid"  />  -->
                     </li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="javascript:window.print();">Print</a>
               </div>
            </div>
<script src="<?php echo base_url()?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

            <script type="text/javascript">
               $(document).ready(function () {
    window.print();

});
 
 window.onafterprint = function(){
      //window.location.reload(true);
 }
            </script>