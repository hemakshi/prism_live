<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Products</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- <link rel="shortcut icon"  href="<?php echo base_url(); ?>public/assets/global/img/favicon.png"> -->

  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }
  </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>

            <li>
              <span>Products</span>
            </li>
          </ul>

        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <!-- <i class="icon-user font-orange"></i> -->
                  <span class="caption-subject bold uppercase">Products</span>
                </div>
                <div class="btn-group" style="padding-left: 10px">
                  <a href="<?php echo site_url('products/add') ?>" class="btn green">
                    Add New <i class="fa fa-plus"></i>
                  </a>
                </div>

                <div class="tools"> </div>
              </div>

              <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th> Name </th>
                      <th> Size </th>
                      <!-- <th > Menu </th> -->
                      <th> Category </th>
                      <th class="td-style"> Price </th>
                      <!-- <th  class="td-style" > Flavours </th>
                        <th> Quantity</th>
                     <th> Discount</th>
                      <th>Featured</th> -->
                      <th> Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $text = base_url();
                    $i = 1;
                    foreach ($products as $key) {
                      //$str= preg_replace('/\W\w+\s*(\W*)$/', '$1', $text).'product/'.$key->prd_slug;
                      // $str= 'http://handvorganic.com/'.'product/'.$key->prd_slug;
                    ?>
                      <tr class="odd gradeX">
                        <!-- <td style="text-align: center;">
              <input type="checkbox" id="chk_nf<?php echo $i ?>" name="chk_fn[]" value="<?php echo $key->prd_id ?>">
            
            </td> -->
                        <td><?php echo $i; ?></td>
                        <td><?php echo $key->prd_name ?></a></td>
                        <td><?php echo $key->prd_size ?></a></td>
                        <!--  <td><?php echo $key->menu ?></td> -->
                        <td><?php echo $key->prd_cat_id_name ?></td>
                        <td class="td-style"><?php echo $key->prd_price ?></td>
                        <!-- <td  class="td-style" ><?php echo $key->prd_flavours ?></td>
                          <td  class="td-style" ><?php echo $key->prd_quantity_name ?></td>
                       <td><?php echo $key->prd_discount == 0 ? '---' : $key->prd_discount . '%' ?></td>
                       <td><?php if ($key->featured_product == '1') { ?><i class="fa fa-check" aria-hidden="true"></i> 
            <?php } ?></td>-->
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                              <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li>
                                <a href="<?php echo site_url('products/edit/' . $key->prd_slug) ?>" title="Edit <?php echo $key->prd_name ?> Content ">
                                  <i style="   font-size: 20px;  color:#EF7F1A;" class="fa fa-pencil" aria-hidden="true"></i> Edit Content</a>
                              </li>
                              <!--  <li>
                              <a href="<?php echo site_url('product-image/edit/' . $key->prd_slug) ?>">
                               <i style="    font-size: 20px;    color:#EF7F1A;" class="fa fa-pencil" aria-hidden="true" ></i>  Edit Images </a>
                             </li> -->
                              <li>
                                <a onclick="deleteFun(<?php echo $key->prd_id; ?>)">
                                  <i style="font-size: 20px; color:#EF7F1A;" class="fa fa-trash" aria-hidden="true"></i> Delete Product</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                    <?php
                      $i++;
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>

      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('common/footer') ?>
  <!-- END FOOTER -->
  <!--[if lt IE 9]>
<script src="<?php echo base_url() ?>public/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
  <!-- BEGIN CORE PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>



  <!-- END THEME LAYOUT SCRIPTS -->
  <script type="text/javascript">
    function deleteFun(prd_id) {
      if (confirm('Are you sure?') == true) {
        $.ajax({
          url: '<?php echo base_url('product/deleteProductById'); ?>',
          method: "post",
          dataType: "json",
          data: {
            prd_id: prd_id
          },
          success: function(response) {
            if (response.success == true) {
              alert(response.message);
              location.reload();
            } else {
              alert(response.message);
            }
          }
        })
      }
    }
  </script>
</body>

</html>