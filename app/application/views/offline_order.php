<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}

$selectedUser = '';
$selectedUsername = '';
$session_offline_userid = $this->session->userdata('session_offline_userid');
if ($session_offline_userid != '') {
  $selectedUser = $session_offline_userid;
  $selectedUsername = $this->session->userdata('session_offline_username');
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Create Order</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- <link rel="shortcut icon"  href="<?php echo base_url(); ?>public/assets/global/img/favicon.png"> -->

  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }

    table {
      width: 100%;
      margin: 20px 0;
      border-collapse: collapse;
    }

    table,
    th,
    td {
      border: 1px solid #cdcdcd;
    }

    table th,
    table td {
      padding: 5px;
      text-align: left;
    }
  </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <a href="<?php echo site_url('offline_order_list') ?>">Offline Orders</a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <span>Create Order</span>
            </li>
          </ul>
        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <span class="caption-subject bold uppercase">Create Order</span>
                </div>
                <div class="tools"> </div>
              </div>
              <div class="portlet-body">
              </div>

              <form method="POST" id="add_offline_order_product">
                <div class="row">
                  <div class="col-md-5">
                    <select id="offline_username" name="offline_username" class="form-control select2" required>
                      <option value="">Select Username</option>
                      <?php echo $this->offline_model->getAllOfflineUser($selectedUser); ?>
                    </select><br>

                    <select id="prd_cat_id" name="prd_cat_id" class="form-control select2" required>
                      <option value="">Select Category</option>
                      <?php echo $this->home_model->getProductCategories(); ?>
                    </select><br>

                    <select id="product_name" name="product_name" class="form-control select2" required>
                      <option value="">Select Product</option>
                    </select><br>

                    <input type="text" class="form-control placeholder-no-fix" placeholder="Price" title="Price" required id="product_price" name="product_price" onchange="changeItemTotal()" /> <br>

                  </div>

                  <div class="col-md-5">
                    <span id="height_width" hidden>
                      <input type="text" class="form-control placeholder-no-fix" placeholder="Height" title="Height" required id="prd_height" name="prd_height" value="1" onchange="changeItemTotal()" /> <br>
                      <input type="text" class="form-control placeholder-no-fix" placeholder="Width" title="Width" required id="prd_width" name="prd_width" value="1" onchange="changeItemTotal()" /><br>
                    </span>

                    <input type="text" class="form-control placeholder-no-fix" placeholder="Quantity" title="Quantity" required id="item_qty" name="item_qty" value="1" onchange="changeItemTotal()" /> <br>

                    <input type="text" class="form-control placeholder-no-fix" readonly placeholder="Total" title="Total" required id="total" name="total" /> <br>

                    <textarea class="form-control placeholder-no-fix" rows="2" placeholder="Remarks" title="Remarks" id="remark_box" name="remark_box"></textarea><br>

                  </div>
                </div>
                <!-- <input type="button" class="add-row1 btn green add_to_cart" value="Add To Cart" id="add_item"> -->
                <button id="add_item" class="add-row1 btn green add_to_cart_offline"><i class="fa fa-cart-plus" aria-hidden="true"></i> Add To Cart</button>

              </form>

              <?php
              if (count($this->cart->contents()) > 0) {
              ?>
                <div class="text-center bold">
                  <h4>Cart (<?php echo count($this->cart->contents()); ?> Items) <?php echo $selectedUsername ? "- " . $selectedUsername : ''; ?></h4>
                </div>
                <div class="text-right">
                  <a href="<?php echo base_url() . "offline_checkout" ?>" class="btn blue place_offline_order">Go To Cart</a>
                </div>
                <div class="table-responsive">
                <table >
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Product Name</th>
                      <th>Price</th>
                      <th>Height</th>
                      <th>Width</th>
                      <th>Qty</th>
                      <th class="text-center">Subtotal</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $count = 1;
                    foreach ($this->cart->contents() as $item) {
                      // echo var_dump($item) . "<br>";
                    ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['price']; ?></td>
                        <td><?php echo $item['prd_height'] ? $item['prd_height'] : "-"; ?></td>
                        <td><?php echo $item['prd_width'] ? $item['prd_width'] : "-"; ?></td>
                        <td><?php echo $item['qty']; ?></td>
                        <td class="text-right"><i class="fa fa-rupee"></i><?php echo $item['prd_subtotal']; ?></td>
                      </tr>
                    <?php
                      $count++;
                    }
                    ?>
                    <tr>
                      <th colspan="6" class="text-right">Total Rs.</th>
                      <th class="text-right"><i class="fa fa-rupee"></i><?php echo sprintf("%.2f", $this->session->userdata('carttotal')); ?></th>
                    </tr>
                  </tbody>
                </table>
                
              <?php
              } else {
                echo "<h4><div class='text-center text-danger'>Cart is Empty!!</div></h4>";
              }
              ?>
              </div>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('common/footer') ?>
  <!-- END FOOTER -->
  <!-- BEGIN QUICK NAV -->
  <div class="quick-nav-overlay"></div>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <!-- <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
  <!-- <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script> -->
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <!-- END THEME LAYOUT SCRIPTS -->

  <!-- BEGIN CUSTOM SCRIPTS -->
  <script src="<?php echo base_url() ?>public/js/offline_cart.js"></script>
  <!-- END CUSTOM SCRIPTS -->

</body>

</html>