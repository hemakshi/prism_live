<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Prism Prints | User Login</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
	<link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?php echo base_url() ?>public/assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME LAYOUT STYLES -->
	<!-- END THEME LAYOUT STYLES -->
	<link rel="shortcut icon" href="<?php echo $this->home_model->getBsnData('logo_ico'); ?>" alt="">
</head>
<!-- END HEAD -->

<style type="text/css">
	.errormesssage {
		caret-color: black;
		color: #ff0022 !important;
		font-size: 12px;
		font-weight: 500;
	}
</style>

<body class=" login" style="background-color: white!important;">
	<!-- BEGIN LOGO -->
	<div class="logo tabsize" style="margin: 60px auto 0px;">
		<a href="">
			<img src="<?php echo $this->home_model->getBsnData('logo'); ?>" alt="" /> </a>
	</div>
	<!-- END LOGO -->

	<!-- BEGIN RESET PASSWORD -->
	<div class="content textsize3" style="background-color: #ef7e18">
		<!-- BEGIN RESET PASSWORD FORM -->
		<h3 class="form-title" style="margin-top: 0px;">Reset Password</h3>
		<form class="" id="user_reset_password_form" method="post">
			<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">
			<input type="hidden" name="fpt_id" id="fpt_id" value="<?php echo $fpt_id ?>">
			
			<?php if ($check == '1') {  ?>				
				<div class="form-group">
					<label>Password
						<span class="asterix-error"><em>* </em> </span>
					</label>
					<div>
						<input class="form-control" type="password" autocomplete="new-password" required name="user_password" id="user_password" />
						<div class="help-block"></div>
					</div>
				</div>

				<div class="form-group">
					<label>Confirm Password
						<span class="asterix-error"><em>* </em> </span>
					</label>
					<div>
						<input class="form-control" type="password" autocomplete="new-password" required name="user_confirm_pass" id="user_confirm_pass" />
						<div class="help-block"></div>
					</div>
				</div>

				<div class="form-actions">
					<button type="button" id="back-btn" class="btn red btn-outline forget_pass_button">Back </button>
					<button type="submit" id="reset_pwd_btn_submit" class="btn btn_save">Submit&nbsp;<i class="fa fa-check"></i></button>
					<button type="button" class="btn btn_processing" style="display: none;"><i class='fa fa-spinner'></i> Submitting in progress...</i></button>
				</div>
			<?php } else { ?>
				<br>
				<h3 class="text-center"> <span style="color: #000"> <?php echo $msg ?></span> </h3>
			<?php } ?>

		</form>
		<!-- END RESET PASSWORD FORM -->

	</div>
	<!-- END RESET PASSWORD -->

	<!-- BEGIN COPYRIGHT -->
	<div class="copyright" style="color: black"> &copy; 2020 PRISM PRINTS. ALL RIGHTS RESERVED.</div>
	<!-- END COPYRIGHT -->

	<script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<!-- <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> -->
	<script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?php echo base_url() ?>public/js/form_validation/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?php echo base_url() ?>public/js/form_validation/login.js"></script>
	<script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>public/assets/pages/scripts/login-4.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->

	<script>
		$(window).load(function() {
			$('.loader').fadeOut();
		});
	</script>

	<script>
		var base_url = '<?php echo base_url() ?>';
	</script>
</body>

</html>