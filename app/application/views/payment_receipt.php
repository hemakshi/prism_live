<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Payment Receipt</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- <link rel="shortcut icon"  href="<?php echo base_url(); ?>public/assets/global/img/favicon.png"> -->

  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }
  </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>

            <li>
              <span>Payment Receipts</span>
            </li>
          </ul>

        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <!-- <i class="icon-user font-orange"></i> -->
                  <span class="caption-subject bold uppercase">Payment Receipt Details</span>
                  <p class="text-danger small">*Note: If the status of the receipt set as approve,then the amount will be added to the wallet balance.</p>
                </div>
                <div class="tools"> </div>
              </div>

              <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                  <thead>
                    <tr>
                      <th style="display: none;"></th>
                      <th>Sr.No</th>
                      <th>Receipt No.</th>
                      <th>Pay By</th>
                      <th>Pay Type</th>
                      <th> Pay Mode </th>
                      <th class="td-style"> Amount </th>
                      <th> Receipt </th>
                      <th> Status </th>
                      <th> Remarks </th>
                      <th> Updated On</th>
                      <th> Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $text = base_url();
                    $i = 1;
                    foreach ($paymentReceiptData as $key) {
                    ?>
                      <tr class="odd gradeX">
                        <td style="display: none;"></td>
                        <td style="text-align: center;"> <?php echo $i; ?></td>
                        <td><?php echo $key->rcpt_number ?></td>
                        <td><?php echo $key->prs_email ?></td>
                        <td><?php echo $key->pay_type ?></td>
                        <td style="min-width: 120px;"><?php echo $key->pay_mode ?></td>
                        <td class="td-style"><?php echo $key->pay_amt ?></td>
                        <td><a class="text-info" target="_blank" rel="noopener noreferrer" href="<?php echo  WEBSITE_ROOT_PATH . PAYMENT_RECEIPT_PATH . $key->rcpt_img ?>"> <?php echo $key->rcpt_img; ?> </a></td>
                        <td><?php echo $key->receipt_status ? $key->receipt_status : "Fail" ?></td>
                        <td><?php echo $key->pay_remarks ? $key->pay_remarks : "-" ?></td>
                        <td><?php echo date('d-M-Y', strtotime($key->update_on)); ?></td>
                        <td class="text-center">
                          <!-- <a data-toggle="modal" title="Change Status" onclick="return show_modal(this)" data-order_id="<?php echo $key->ord_id ?>" data-ord_reference_no="<?php echo $key->ord_reference_no ?>" data-order_status="<?php echo $key->ord_status ?>" data-ord_prs_id="<?php echo $key->ord_prs_id ?>" class="btn btn-sm red btn-outline filter-cancel">
                            <i class="fa fa-edit" aria-hidden="true"></i>
                          </a> -->
                          <button data-toggle="modal" onclick="return show_modal(this)" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" data-payment_receipt_id="<?php echo $key->ID; ?>" data-rcpt_number="<?php echo $key->rcpt_number; ?>" data-receipt_status="<?php echo $key->status; ?>" data-pay_remarks="<?php echo $key->pay_remarks; ?>" data-prs_id="<?php echo $key->upload_by; ?>" data-prs_name="<?php echo $key->prs_name; ?>" aria-expanded="false">
                            Actions<i class="fa fa-angle-down" aria-hidden="true"></i>
                          </button>
                        </td>
                      </tr>
                    <?php
                      $i++;
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>

      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->

  <!-- START MODAL FOR CHANGE ORDER STATUS -->
  <div class="modal fade" id="Add" tabindex="-1" role="Add" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" id="modal-title"></h4>
        </div>
        <form role="form" id="form_change_payment_receipt_status" method="POST">
          <!-- HIDDEN FIELDS -->
          <input type="hidden" name="payment_receipt_id" id="payment_receipt_id">
          <input type="hidden" name="prs_id" id="prs_id">
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="form_control_1">Select Status<span style="color:red">*</span></label>
                <select class="form-control" name="receipt_status" id="receipt_status">
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="form_control_1">Remark</label>
                <textarea id="receipt_remark" name="receipt_remark" class="form-control"></textarea>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="submit" id="form_submit" class="btn blue">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- END MODAL FOR CHANGE ORDER STATUS -->

  <!-- BEGIN FOOTER -->
  <?php echo $this->load->view('common/footer'); ?>
  <!-- END FOOTER -->
  </div>

  <!-- BEGIN QUICK NAV -->
  <div class="quick-nav-overlay"></div>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>public/js/form_validation/update-payment-receipt-status.js"></script>
  <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>