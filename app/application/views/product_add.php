<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Products</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- select2 css files -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="<?php echo base_url() . '/' . $this->home_model->getBsnData('logo_ico'); ?>" alt="">
  <style type="text/css">
    #information1 {
      position: absolute;
      bottom: 14px;
      left: 99px;
      display: none;
    }

    .imageThumb {
      /*max-height: 75px;*/
      border: 2px solid;
      padding: 1px;
      cursor: pointer;
      width: 110px;
      height: 75px;
    }

    .pip {
      display: inline-block;
      margin: 10px 10px 0 0;
    }
  </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header') ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <a href="<?php echo site_url('products') ?>">Products</a>
                <i class="fa fa-circle"></i>
              </li>
              <li><span>Add</span></li>
            </ul>
          </div>
          <!-- END PAGE BAR -->
          <!-- BEGIN PAGE TITLE-->
          <!--  <h1 class="page-title"> Blank Page Layout
                            <small>blank page layout</small>
                          </h1> -->
          <!-- END PAGE TITLE-->
          <!-- END PAGE HEADER-->
          <div class="row">
            <div class="col-md-12 ">
              <!-- BEGIN SAMPLE FORM PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-orange">
                    <i class="icon-user font-orange"></i>
                    <span class="caption-subject bold uppercase">Add Product</span>
                  </div>
                </div>

                <div class="portlet-body form">
                  <div id="error" style="text-align: center;">
                  </div>
                  <form id="add_product" method="post" class="horizontal-form">

                    <div class="row">
                      <!-- <div class="col-md-6">
                                    <div class="form-group"> 

                                      <label for="prd_menu" class="control-label">Menu<span  style="color:red">*</span></label>
                                      <select required="" id="prd_menu" name="prd_menu" onchange="getBrands(this.value),getCategories(this.value)" class="form-control select2"  >
                                     <option value="">Select Menu</option>
                                    <?php
                                    foreach ($menu as $key) {
                                      echo '<option value="' . $key->gnp_value . '" >' . $key->gnp_name . '</option>';
                                    }
                                    ?>
                                        </select>
                                         </div>
                                </div> -->
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="prd_name" class="control-label">Name<span style="color:red">*</span></label>
                          <input type="text" class="form-control" id="prd_name" name="prd_name" required="">
                          <span class="help-block"></span>
                        </div>
                      </div>
                      <!--  
                                <div class="col-md-6"  id="brand_div" style="display: none">
                                    <div class="form-group"> 

                                      <label for="prd_brand" class="control-label">Brand<span  style="color:red">*</span></label>
                                      <select required="" id="prd_brand" name="prd_brand"  class="form-control select2"  >
                                     
                                        </select>
                                         </div>
                                </div> -->
                      <!--/span-->
                      <div class="col-md-3">
                        <div class="form-group">

                          <label for="prd_cat_id" class="control-label">Category<span style="color:red">*</span></label>
                          <select required="" id="prd_cat_id" name="prd_cat_id" class="form-control select1">
                            <option value="">Select Category</option>
                            <?php echo $this->home_model->getProductCategories(); ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="prd_price" class="control-label">Price<span style="color:red">*</span></label>
                          <input type="text" required="" class="form-control Stylednumber" id="prd_price" name="prd_price">
                          <span class="help-block"></span>
                        </div>
                      </div>
                      <!--/span-->
                      <!--   <div class="col-md-6">
                                    <div class="form-group"> 

                                      <label for="prd_flv_id" class="control-label">Flavours<span  style="color:red">*</span></label>
                                      <select required="" id="prd_flv_id" name="prd_flv_id"  class="form-control select2-multiple" multiple  >
                                      <option value="">Select Category</option>
                                      <?php echo $this->home_model->getProductFlavours(); ?>
                                        </select>
                                         </div>
                                </div> -->
                      <!--   <div class="col-md-6">
                                    <div class="form-group"> 

                                      <label for="prd_availability" class="control-label">Availability<span  style="color:red">*</span></label>
                                      <select required="" id="prd_availability" name="prd_availability"  class="form-control select2"  >
                                      <option value="">Select Category</option>
                                      <?php echo $this->home_model->get_dropdown('availability'); ?>
                                        </select>
                                         </div>
                                </div> -->

                      <!--/span-->
                      <!--  <div class="col-md-6">
                           <div class="form-group"> 

                             <label for="prd_quantity_unit" class="control-label">Quantity Unit<span  style="color:red">*</span></label>
                             <select required="" id="prd_quantity_unit" name="prd_quantity_unit"  class="form-control select2"  >
                             <option value="">Select Category</option>
                             <?php echo $this->home_model->get_dropdown('prd_quantity_unit'); ?>
                               </select>
                                </div>      
                         </div> -->
                      <!--   <div class="col-md-6">
                          <div class="form-group">  
                           <label for="prd_order" class="control-label">Order</label>
                           <input type="text"  class="form-control"  id="prd_order" name="prd_order"  >
                           <span class="help-block"></span>
                         </div>        
                       </div> -->
                      <!--/span-->

                      <!--   <div class="col-md-6">
                            <label for="prd_bar_code" class="control-label">Discount (In %)</label>
                           <input type="text"  class="form-control"  id="prd_discount"  value="" name="prd_discount"  >
                           <span class="help-block"></span>
                         </div> -->

                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="prd_size" class="control-label">Size<span style="color:red">*</span></label>
                          <input type="text" class="form-control" id="prd_size" name="prd_size" required>
                          <span class="help-block"></span>
                        </div>
                      </div>
                    </div>
                    <!--  <div class="row">
                         <div class="col-md-12">
                                      <div class="form-group">
                                       <label for="prd_short_desc" class="control-label">Short Desc<span style="color:red">*</span></label> -->
                    <!--  <textarea  class="form-control" id="prd_short_desc" name="prd_short_desc" required="" ></textarea>   <span class="help-block"></span> -->
                    <!--  <textarea required name="prd_short_desc" id="summernote_2" rows="10"></textarea> 
                                     </div>
                                   </div>
                         <div class="col-md-12">
                          <div class="form-group">
                           <label for="prd_descripion" class="control-label">Description<span style="color:red">*</span></label>
                           <textarea required name="prd_descripion" id="summernote_1" rows="10"></textarea> 
                         </div>
                       </div>
                     </div> -->
                    <!--  <div class="row">
                         <div class="col-md-12">
                           <div class="form-group files">
                               <label for="form_control_1">Images (Size: 5mb)<span style="color:red">*</span></label>
                               <input type="hidden" class="form-control" id="documents_count" name="documents_count"  value="0" />
                              <input type="hidden" class="form-control" id="total_documents_count" name="total_documents_count"  value="0" />
                               <input type="hidden" id="upload_button_value_docs" name="upload_button_value_docs" value="0">
                              <input type="hidden" class="form-control" id="timg_reference_no" name="timg_reference_no"  value="" />
                               <input  type="file" required="" id="files1" name="files1" multiple  accept="image/*" />
                               <br /> 
                               <div class="fileList"  id='documents_priview'></div>


                               <h id="doc_uploaded" style="color:green"></h> <br> 
                               <h id="doc_error_invalid_size" style="color:red"></h>
                               <button type="button"  onclick="return upload_images()" style="display:none;margin-top:5px;margin-bottom: 50px;"  class="btn btn-primary" id="upload_docs">Upload</button>
                               <img id="information1" src = "<?php echo base_url() ?>public/images/info.png">
                                /.modal-content -->
                    <!--  </div>
                       </div>
                     </div>  -->
                    <div class="form-actions ">

                      <button type="submit" class="btn blue" name="form_submit" id="form_submit">Save</button>
                      <button type="button" class="btn default" name="cancel_button" id="cancel_button" onclick="history.go(-1)">Cancel</button>

                    </div>


                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT BODY -->
      </div>
      <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('common/footer') ?>
    <!-- END FOOTER -->
  </div>

  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/dropify/js/dropify.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <!-- END THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

  <script src="<?php echo base_url() ?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>public/js/form_validation/add-product.js"></script>
  <!-- START IMAGES PRODUCT SCRIPT -->
  <!--   <script type="text/javascript">
                  $.fn.fileUploader = function (filesToUpload) {


                    this.closest(".files").change(function (evt) {



                      document.getElementById('upload_button_value_docs').value='0';

                      $('#upload_docs').css('display','block');
                      $("#information1").css('display','block');
                      document.getElementById('documents_count').value=evt.target.files.length-1;
                      document.getElementById('total_documents_count').value=evt.target.files.length-1;
                      for (var i = 0; i < evt.target.files.length; i++)
                      {


                          filesToUpload.push(evt.target.files[i]);
                      };
                      var output = [];
                      document.getElementById("documents_priview").innerHTML='';

                      for (var i = 0, f; f = evt.target.files[i]; i++) 
                      {
                          var removeLink = "<button type='button' class=\"btn btn-danger in-form remove-doc\" onclick=\"removeImage("+i+");\" ><i class='fa fa-times'></i></button>";
                          var sizeInMB = ( f.size / (1024*1024)).toFixed(2);

                          data= "<div   id=\"document" +i+ "\"    class=\"row\"><div  class=\"col-md-6\"><span class=\"pip\">" + "<img class=\"imageThumb\" src=\"" +  URL.createObjectURL(evt.target.files[i]) + "\" />" +"</span> - <strong>"+f.name+ "</strong>-"+ sizeInMB+" MB. &nbsp; &nbsp;</div><div class=\"col-md-3\">"+removeLink+"</div> </div><input type='hidden' id='document_input"+i+ "' value=\"1\" >  ";
                          var div = document.getElementById('documents_priview');
                          document.getElementById("documents_priview").innerHTML=div.innerHTML +data;


                      }


                  });
                };

                var filesToUpload = [];



                $("#files1").fileUploader(filesToUpload);
                $("#files2").fileUploader(filesToUpload);

                $("#uploadBtn").click(function (e) {
                    e.preventDefault();
                });


            </script> -->

  <script type="text/javascript">
    function removeImage(doc_id) {
      var d = confirm("Are you sure you want to remove this image.")
      if (d == 1) {

        $("#document" + doc_id).remove();

        document.getElementById('document_input' + doc_id).value = '0';
        document.getElementById('total_documents_count').value = document.getElementById('total_documents_count').value - 1;

        if (document.getElementById('total_documents_count').value < 0) {
          document.getElementById('upload_button_value_docs').value = '1';
          $('#upload_docs').css('display', 'none');
          $("#information1").css('display', 'none');
        }
      }
    }
  </script>
  <!-- END IMAGES PRODUCT SCRIPT -->
</body>

</html>