<?php
if ($this->session->userdata('tsn_usr_id') == '' || $this->session->userdata('tsn_usr_dpt_id') != ADMIN_DEPARTMENT) {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | User Edit</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- select2 css files -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="<?php echo base_url() . '/' . $this->home_model->getBsnData('logo_ico'); ?>" alt="">
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header') ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <a href="<?php echo site_url('offline_customers') ?>">Offline Customers</a>
                <i class="fa fa-circle"></i>
              </li>
              <li><span>Edit</span></li>
            </ul>
          </div>
          <!-- END PAGE BAR -->
          <!-- BEGIN PAGE TITLE-->
          <!--  <h1 class="page-title"> Blank Page Layout
                            <small>blank page layout</small>
                          </h1> -->
          <!-- END PAGE TITLE-->
          <!-- END PAGE HEADER-->
          <div class="row">
            <div class="col-md-12 ">
              <!-- BEGIN SAMPLE FORM PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-orange">
                    <i class="icon-user font-orange"></i>
                    <span class="caption-subject bold uppercase">Edit Customer - <?php echo $user_details->offline_user_username ?></span>
                  </div>
                </div>
                <div class="tabbable-custom ">

                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_5_1">
                      <div class="portlet-body form">
                        <div id="error" style="text-align: center;">
                        </div>
                        <form id="edit_offline_customer" method="POST" class="horizontal-form">

                          <div class="form-body">

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <input type="hidden" name="prs_slug" id="prs_slug" value="<?php echo $user_details->offline_user_id; ?>">
                                  <label for="prs_name" class="control-label">Full Name<span style="color:red">*</span></label>
                                  <input type="text" class="form-control" id="prs_name" name="prs_name" value="<?php echo $user_details->offline_user_fullname; ?>" required>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_user_name" class="control-label">User Name<span style="color:red">*</span></label>
                                  <input type="text" class="form-control" readonly id="prs_user_name" name="prs_user_name" value="<?php echo $user_details->offline_user_username ?>" required>
                                  <span class="help-block"></span>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_mob" class="control-label">Mobile<span style="color:red">*</span></label>
                                  <input type="text" class="form-control" required id="prs_mob" name="prs_mob" onblur="return check_validation('offline_user_mobile',this.value,this.id)" value="<?php echo $user_details->offline_user_mobile ?>">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_email" class="control-label"> Email</label>
                                  <input type="email" class="form-control" id="prs_email" name="prs_email" onblur="return check_validation('offline_user_email',this.value,this.id)" value="<?php echo $user_details->offline_user_email ?>">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <!--    <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Gender</label>
                                  <select class="form-control select1" id="prs_gender" name="prs_gender" >
                                    <option>Select Gender</option>
                                    <?php
                                    global $gnp_active_status;
                                    echo $this->home_model->getCombo("SELECT gnp_value as f1, gnp_name as f2 from gen_prm where gnp_group = 'gender' AND gnp_status=" . ACTIVE_STATUS . "", $user_details->offline_user_gender);
                                    ?>
                                  </select>
                                </div>
                              </div> -->
                              <!-- <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Birth Date</label>
                                  <input type="date" id="prs_dob" name="prs_dob" value="<?php echo $user_details->offline_user_dob; ?>" />
                                </div>
                              </div> -->
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_gst" class="control-label">GST No.</label>
                                  <input type="text" class="form-control" id="prs_gst" name="prs_gst" value="<?php echo $user_details->offline_user_gst ?>">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_status" class="control-label">Status</label>
                                  <div class="form-group">
                                    <span for="prs_status">Is Active</span>
                                    <input type="checkbox" id="prs_status" name="prs_status" <?php echo $user_details->offline_user_status == 1 ? 'checked' : ''; ?> style="height: 16px; width: 20px;">
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_address_line" class="control-label">Address Line<span style="color:red">*</span></label>
                                  <textarea id="prs_address_line" name="prs_address_line" required> <?php echo $user_details->offline_user_address_line; ?> </textarea>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_district" class="control-label">District<span style="color:red">*</span></label>
                                  <input type="text" class="form-control" id="prs_district" name="prs_district" value="<?php echo $user_details->offline_user_district; ?>" required>
                                </div>
                              </div>

                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_city" class="control-label">City<span style="color:red">*</span></label>
                                  <input type="text" class="form-control" id="prs_city" name="prs_city" value="<?php echo $user_details->offline_user_city; ?>" required>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_state" class="control-label">State<span style="color:red">*</span></label>
                                  <select class="form-control" id="prs_state" name="prs_state" required>
                                    <option value="" selected disabled>--- Select State ---</option>
                                    <?php echo getDropdownResult('state-dropdown', 'pad_state', 'pad_state', $user_details->offline_user_state); ?>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="prs_pincode" class="control-label">Pincode<span style="color:red">*</span></label>
                                  <input type="text" class="form-control" id="prs_pincode" name="prs_pincode" value="<?php echo $user_details->offline_user_pincode; ?>" required>
                                </div>
                              </div>
                            </div>

                            <div class="form-actions ">
                              <button type="button" class="btn default" name="cancel_button" id="cancel_button" onclick="history.go(-1)">Cancel</button>
                              <button type="submit" class="btn blue" name="form_submit" id="form_submit">Save</button>
                            </div>
                        </form>
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
      </div>
      <!-- END CONTAINER -->
      <!-- BEGIN FOOTER -->
      <?php $this->load->view('common/footer') ?>
      <!-- END FOOTER -->
    </div>

    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/dropify/js/dropify.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>public/js/form_validation/offline_customer_edit.js"></script>

    <script type="text/javascript">
      function check_validation(type, value, id) {
        var prs_slug = document.getElementById('prs_slug').value;
        data = {
            type: type,
            value: value,
            prs_slug: prs_slug
          },
          $.ajax({
            type: "POST",
            url: base_url + "offline_controller/checkValidationForOfflineUser",
            data: data,
            dataType: "json",
            success: function(response) {
              if (response.success == true) {
                $('#' + id).html('');
                $('#' + id).parent().find('.error').html('');
                $('#' + id).parent().append('<span class="error">Data already exists</span>');
              } else {
                $('#' + id).html('');
                $('#' + id).parent().find('.error').html('');
              }
            }
          });
      }
    </script>
</body>

</html>