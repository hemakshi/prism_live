<?php require_once(APPPATH . "/config/mail_template.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Prism Prints | Order Id#<?php echo $order->offline_order_reference_no ?></title>
    <style type="text/css">
        #invoice {
            padding: 30px;
        }

        .invoice {
            position: relative;
            background-color: #FFF;
            min-height: 680px;
            padding: 15px
        }

        .invoice header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 3px solid black
        }

        .invoice .company-details {
            text-align: right
        }

        .invoice .company-details .name {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .contacts {
            margin-bottom: 20px
        }

        .invoice .invoice-to {
            text-align: left
        }

        .invoice .invoice-to .to {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 15px;

        }

        .invoice .invoice-details {
            text-align: right
        }

        .invoice .invoice-details .invoice-id {
            margin-top: 0;
            color: #3989c6
        }

        .invoice main {
            padding-bottom: 50px
        }

        .invoice main .thanks {
            margin-top: -100px;
            font-size: 2em;
            margin-bottom: 50px
        }

        .invoice main .notices {
            padding-left: 6px;
            border-left: 6px solid #3989c6
        }

        .invoice main .notices .notice {
            font-size: 1.2em
        }

        .invoice table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px
        }

        .invoice table td,
        .invoice table th {
            padding: 15px;
            background: #eee;
            border-bottom: 1px solid #fff
        }

        .invoice table th {
            white-space: nowrap;
            font-weight: 400;
            font-size: 16px
        }

        .invoice table td h3 {
            margin: 0;
            font-weight: 400;
            color: #3989c6;
            font-size: 1.2em
        }

        .invoice table .qty,
        .invoice table .total,
        .invoice table .unit {
            text-align: left;
            font-size: 15px;
        }

        .invoice table .no {
            color: #fff;
            font-size: 1.6em;
            background: #3989c6
        }

        .invoice table .unit {
            background: #ddd
        }

        .invoice table .total {
            background: #3989c6;
            color: #fff
        }

        .invoice table tbody tr:last-child td {
            border: none
        }

        .invoice table tfoot td {
            background: 0 0;
            border-bottom: none;
            white-space: nowrap;
            text-align: right;
            padding: 10px 20px;
            font-size: 1.2em;
            border-top: 1px solid #aaa
        }

        .border-bottom-none {
            border-bottom: none !important;
        }

        .invoice table tfoot tr:first-child td {
            border-top: none
        }

        .invoice table tfoot tr:last-child td {
            color: #3989c6;
            font-size: 1.4em;
            border-top: 1px solid #3989c6
        }

        .invoice table tfoot tr td:first-child {
            border: none
        }

        .invoice footer {
            width: 100%;
            text-align: center;
            color: #777;
            border-top: 1px solid #aaa;
            padding: 8px 0
        }

        /* @media print {
            .invoice {
                font-size: 11px !important;
                overflow: hidden !important
            }

            .invoice footer {
                position: absolute;
                bottom: 10px;
                page-break-after: always
            }

            .invoice>div:last-child {
                page-break-before: always
            }
            .noprint {
                visibility: hidden;
            }
        } */
        @media print {

            html,
            body {
                border: 1px solid white;
                height: 99%;
                page-break-after: avoid;
                page-break-before: avoid;
            }

            .noprint {
                visibility: hidden;
            }
        }

        /* This will remove the headers and backgrounds from the printer settings. */
        @page {
            size: auto;
            margin: 0mm;
        }

        .table-head {
            border-top: 2px solid black;
            border-bottom: 2px solid black;
        }

        .bold {
            font-weight: bold;
        }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
    <div id="invoice1">
        <div class="toolbar hidden-print noprint">
            <br>
            <div class="text-right">
                <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
            </div>
            <hr>
        </div>
        <div id="invoice">

            <div class="invoice overflow-auto">
                <div style="min-width: 600px">
                    <h5 class="text-center">TAX INVOICE</h5>
                    <header>
                        <div class="row">
                            <div class="col">
                                <a target="_blank" href="<?php WEBSITE_ROOT_PATH ?>">
                                    <img src="<?php echo base_url(); ?>public/logo_image/logo.png" data-holder-rendered="true" />
                                </a>
                            </div>
                            <div class="col company-details">
                                <h2 class="name">
                                    <?php echo 'Prism Prints' ?>
                                </h2>
                                <div><?php echo COMPANY_ADDRESS ?></div>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row contacts">
                            <div class="col invoice-to">
                                <div class="text-gray-light">Buyer's Name and Address:</div>
                                <h2 class="to bold text-capitalize"><?php echo $order->offline_user_fullname; ?></h2>
                                <div class="address"><?php echo $order->offline_user_address_line . ","; ?></div>
                                <div class="address"><?php echo $order->offline_user_city ?>-<?php echo $order->offline_user_pincode . ","; ?></div>
                                <div class="address"><?php echo $order->offline_user_district . "," . $order->offline_user_state . "."; ?></div>
                                <div class="email bold"><?php echo "Mobile No. " . $order->offline_user_mobile ?></div>
                            </div>
                            <div class="col invoice-details">
                                <div class="date bold">Order Id# <?php echo $order->offline_order_reference_no ?></div>
                                <div class="date">Date: <?php echo date("d M, Y", strtotime($order->offline_order_created_on)) ?></div>
                            </div>
                        </div>

                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <tr class="table-head">
                                    <th class="text-left">No.</th>
                                    <th class="text-left">Name</th>
                                    <th class="text-left">Price</th>
                                    <th class="text-left">Qty</th>
                                    <th class="text-left">Height</th>
                                    <th class="text-left">Width</th>
                                    <th class="text-left">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($products as $product) {
                                ?>
                                    <tr class="table-head">
                                        <td class="unit bold"><?php echo $count ?> </td>
                                        <td class="unit bold"><?php echo $product->prduct_name ?> </td>
                                        <td class="unit">₹ <?php echo $product->oop_price ?></td>
                                        <td class="unit"><?php echo $product->oop_quantity ?></td>
                                        <td class="unit"><?php echo $product->oop_height ? $product->oop_height : '-' ?></td>
                                        <td class="unit"><?php echo $product->oop_width ? $product->oop_width : '-' ?></td>
                                        <td class="unit bold">₹ <?php echo $product->oop_total ?></td>
                                    </tr>
                                <?php
                                    $count++;
                                }
                                ?>
                            </tbody>
                        </table>

                        <header class="border-bottom-none">
                            <div class="row">
                                <div class="col">
                                    <p> <b>Bank Details</b><br>
                                        <?php echo BANK_NAME ?> <br>
                                        <?php echo BANK_BRANCH ?> <br>
                                        IFSC: <?php echo BANK_IFSC_CODE ?><br>
                                        A/C: <?php echo BANK_ACCOUNT_NUMBER ?>
                                    </p>
                                </div>
                                <div class="col company-details">
                                    <div class="date">Sub Total: ₹ <?php echo $order->offline_order_subtotal ?></div>
                                    <div class="date">Shipping Charges: ₹ <?php echo $order->offline_order_shipping_charges ?></div>
                                    <div class="date bold">Order Total: <span class="bold">₹ <?php echo $order->offline_order_total ?></span></div>
                                    <br>
                                    <div class="bold">Payment Detail</div>
                                    <div class="date">Paid Amount: ₹ <?php echo $order->offline_order_paid_amt ?></div>
                                    <div class="date">Pending Amount: ₹ <?php echo $order->offline_order_pending_amt ?></div>

                                    <br><br>
                                    <p class="bold"> <?php echo 'Prism Prints' ?> </p>
                                    <img src="<?php echo base_url(); ?>public/signature.png" data-holder-rendered="true" />
                                    <p> Authorized signatury</p>
                                </div>
                            </div>
                            <div class="date text-center">This is computer generated invoice. Printed on <?php echo date("d M, Y", strtotime(date('Y-m-d'))) ?></div>

                        </header>
                    </main>

                </div>
                <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                <div></div>
            </div>

        </div>
</body>
<script type="text/javascript">
    $('#printInvoice').click(function() {
        Popup($('.invoice')[0].outerHTML);

        function Popup(data) {
            window.print();
            return true;
        }
    });
</script>

</html>