<?php
if ($this->session->userdata('tsn_usr_id') == '') {
   $abc = base_url();
   echo '<script> ';
   echo 'window.location="' . $abc . '"';
   echo '</script>';
}
?>

<!DOCTYPE html>

<html lang="en">

<head>
   <meta charset="utf-8" />
   <title>PRISM PRINTS | Order Details</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <!-- BEGIN GLOBAL MANDATORY STYLES -->

   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
   <!-- END GLOBAL MANDATORY STYLES -->

   <!-- BEGIN THEME GLOBAL STYLES -->
   <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME GLOBAL STYLES -->
   <!-- BEGIN THEME LAYOUT STYLES -->
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
   <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
   <!-- END THEME LAYOUT STYLES -->
   <link rel="shortcut icon" href="favicon.ico" />
   <style type="text/css">
      .td-style {
         width: 10%;
         text-align: center;
      }

      .errormesssage {
         caret-color: black !important;
         color: #ff6161 !important;
         font-size: 12px;
         font-weight: 500;
      }
   </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('common/header') ?>
   <!-- END HEADER -->
   <!-- BEGIN HEADER & CONTENT DIVIDER -->
   <div class="clearfix"> </div>
   <!-- END HEADER & CONTENT DIVIDER -->
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
         <!-- BEGIN CONTENT BODY -->
         <div class="page-content">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
               <ul class="page-breadcrumb">
                  <li>
                     <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                     <i class="fa fa-circle"></i>
                  </li>
                  <li>
                     <a style="text-decoration: none" href="<?php echo site_url('orders') ?>"> <span>Orders</span></a>
                     <i class="fa fa-circle"></i>
                  </li>
                  <li>
                     <span><?php echo $order->ord_reference_no ?></span>
                  </li>
               </ul>
            </div>
            <div class=""></div>
            <div class="row">
               <div class="col-md-12 ">
                  <div class="portlet light bordered">

                     <div class="portlet-body no-padding-top">
                        <form class="form-horizontal form" role="form">
                           <div class="form-body no-padding-top">
                              <h5 class="form-section  no-margin-bottom bold">
                                 Shipping Details:
                              </h5>
                              <div class="row">
                                 <div class="col-md-12 detail table-responsive">
                                    <table class="table table table-bordered">
                                       <tbody>
                                          <tr>
                                             <td>Order No</td>
                                             <td><?php echo $order->ord_reference_no ?></td>
                                             <td>Order Date</td>
                                             <td><?php echo date('d-M-Y', strtotime($order->ord_date)) ?></td>
                                          </tr>
                                          <tr>
                                             <td>Payment Method</td>
                                             <td><?php echo $order->ord_payment_mode_name ?></td>
                                             <td>Order Total</td>
                                             <td> <i class="fa fa-inr"></i> <?php echo floatval($order->ord_total_amt) ?></td>
                                          </tr>
                                          <tr>
                                             <td>Customer Name</td>
                                             <td><?php echo $order->pad_name ?></td>
                                             <td>Shipping Address</td>
                                             <td>
                                                <div class="bold"><?php echo $order->pad_name ?></div>
                                                <div><?php echo $order->pad_address ?></div>
                                                <div><?php echo $order->pad_locality; ?></div>
                                                <div><?php echo $order->pad_city; ?>- <?php echo $order->pad_pincode; ?></div>
                                                <div><?php echo $order->state_name; ?></div>
                                                <?php
                                                if ($order->pad_landmark != '') {
                                                   echo '<div>Landmark: ' . $order->pad_landmark . '</div>';
                                                }
                                                ?>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>Customer Mob No: </td>
                                             <td><?php echo $order->pad_mobile ?></td>
                                             <td>Alternate Mob No: </td>
                                             <td><?php echo $order->pad_alt_phone ?></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>

                     <h5 class="form-section  no-margin-bottom bold">Products</h5>
                     <div>
                        <div class="portlet-body table-responsive">
                           <table class="table table table-bordered product  ">
                              <thead>
                                 <th> Product </th>
                                 <th> File </th>
                                 <th> Price </th>
                                 <th> Qty </th>
                                 <th> Height </th>
                                 <th> Width </th>
                                 <th> Charges </th>
                                 <th> Total </th>
                                 <th> Status </th>
                                 <th> Remark </th>
                                 <th class="text-center"> Action </th>
                                 <!--   <th> Discount </th>
                                 <th> Taxable Amount </th>
                                 <th> IGST </th> -->
                              </thead>
                              <tbody>
                                 <?php
                                 foreach ($product as $key) {
                                 ?>
                                    <tr>
                                       <!--   <td  id="product">
                                  <a title="Product Detail" >   <img height="70px" src="<?php echo base_url() . PRODUCT_SMALL_IMAGE_PATH . $key->img_path ?>" title="<?php echo  $key->prduct_name ?>" alt="<?php echo  $key->prduct_name ?>"></a>
                                 </td> -->

                                       <td id="product">
                                          <?php echo $key->prduct_name ?>
                                          <!-- <a title="Product Detail" href="<?php echo site_url('product/' . $key->prduct_slug) ?>"> </a> -->
                                       </td>
                                       <td><a class="text-info" rel="noopener noreferrer" download="<?php echo $key->odp_file; ?>" href="<?php echo  WEBSITE_ROOT_PATH . ORDER_DESIGN_FILE_PATH . $key->odp_file; ?>"> <?php echo $key->odp_file; ?> </a></td>
                                       <td>
                                          <?php echo floatval($key->odp_amt); ?>
                                       </td>
                                       <td id="product">
                                          <?php echo $key->odp_quantity ?>
                                       </td>

                                       <td>
                                          <?php
                                          if (!empty($key->odp_height) && $key->odp_height != 0)
                                             echo $key->odp_height;
                                          else
                                             echo "-";
                                          ?>
                                       </td>
                                       <td>
                                          <?php
                                          if (!empty($key->odp_width) && $key->odp_width != 0)
                                             echo $key->odp_width;
                                          else
                                             echo "-";
                                          ?>
                                       </td>
                                       <td class='text-right'>
                                          <?php
                                          if (!empty($key->ord_shipping_charges) && $key->ord_shipping_charges != 0)
                                             echo floatval($key->ord_shipping_charges);
                                          else
                                             echo "-";
                                          ?>
                                       </td>
                                       <td class="text-right">
                                          <?php echo floatval($key->odp_total_amt) ?>
                                       </td>
                                       <td>
                                          <?php echo $key->order_product_status ?>
                                       </td>
                                       <td>
                                          <?php echo $key->ops_remark ?>
                                       </td>
                                       <td class="text-center">
                                          <?php if ($key->ord_shipping_charges == 0) { ?>
                                             <a data-toggle="modal" title="Add Charges" onclick="return showAddProductShippingChargeModal(this)" data-order_id="<?php echo $key->odp_ord_id ?>" data-product_id="<?php echo $key->odp_prd_id ?>" data-odp_id="<?php echo $key->odp_id ?>" data-name="<?php echo $key->prduct_name ?>" data-product_shipping_charges="<?php echo $key->ord_shipping_charges ?>" class="btn btn-danger filter-cancel">
                                                <i class="fa fa-money" aria-hidden="true"></i>
                                             </a>
                                          <?php } ?>
                                          <a data-toggle="modal" title="Change Status" onclick="return showChangeProductStatusModal(this)" data-order_id="<?php echo $key->odp_ord_id ?>" data-product_id="<?php echo $key->odp_prd_id ?>" data-status="<?php echo $key->odp_status ?>" data-odp_id="<?php echo $key->odp_id ?>" data-name="<?php echo $key->prduct_name ?>" data-remark="<?php echo $key->ops_remark; ?>" class="btn btn-danger filter-cancel">
                                             <i class="fa fa-edit" aria-hidden="true"></i>
                                          </a>
                                          <a title="Print invoice" target="_blank" href="<?php echo site_url('invoice/' . $key->odp_ord_id . '/' . $key->odp_prd_id) ?>" class="btn btn-danger filter-cancel" id="action-btn">
                                             <i class="fa fa-print" aria-hidden="true"></i>
                                          </a>
                                       </td>
                                    </tr>
                                 <?php
                                 }
                                 ?>

                              </tbody>
                           </table>

                        </div>
                     </div>
                     <h5 class="form-section  no-margin-bottom bold">Payment Detail</h5>

                     <table class="table table table-bordered product ">
                        <thead>
                           <tr>
                              <th> Payment Mode </th>
                              <th> Payment Amount </th>
                              <th> Payment Status </th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td id="product"><?php echo $order->ord_payment_mode_name ?></td>
                              <td id="product"><?php echo floatval($order->ord_total_amt) ?></td>
                              <td id="product">
                                 <?php
                                 switch ($order->ord_payment_status) {
                                    case 1:
                                       echo "Pending";
                                       break;
                                    case 2:
                                       echo "Paid";
                                       break;
                                    default:
                                       echo "-";
                                       break;
                                 }
                                 ?>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="row">
                        <div class="col-sm-offset-8  col-sm-4 invoice-block no_padding">
                           <div class="table-responsive">
                              <table class="table " style="    margin-bottom: 0px;">
                                 <tbody>
                                    <tr>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <strong>Sub Total:
                                          </strong>
                                       </td>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <i class="fa fa-inr">
                                          </i>
                                          <?php echo floatval($order->ord_sub_total) ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <strong> Shipping Charges:
                                          </strong>
                                       </td>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <i class="fa fa-inr"></i>
                                          <?php echo floatval($order->ord_shipping_charges) ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <strong>Total:
                                          </strong>
                                       </td>
                                       <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                          <i class="fa fa-inr">
                                          </i>
                                          <?php echo floatval($order->ord_total_amt) ?>
                                       </td>
                                    </tr>
                                    <!-- <tr>
                                   <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                      <strong> Due Amount: 
                                      </strong>
                                   </td>
                                   <td style="border-top: 0px solid #e7ecf1;  padding: 3px;">
                                      <i class="fa fa-inr">
                                      </i> 
                                    <?php echo $order->ord_total_amt ?>
                                   </td>
                                </tr> -->
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END CONTENT BODY -->
               </div>
               <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->

            <!-- START MODAL FOR UPDATE PRODUCT STATUS -->
            <div class="modal" id="ChangeProductStatus" tabindex="-1" role="dialog" aria-hidden="true">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="modal-title"></h4>
                     </div>
                     <form role="form" id="formChangeProductStatus" method="POST">
                        <input type="hidden" name="product_id" id="product_id">
                        <input type="hidden" name="odp_id" id="odp_id">
                        <input type="hidden" name="order_id" id="order_id">
                        <div class="modal-body">
                           <div class="row">
                              <div class="form-group col-md-6">
                                 <label for="form_control_1">Select Status<span style="color:red">*</span></label>
                                 <select class="form-control" name="odp_status" id="odp_status">
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label for="form_control_1">Remark</label>
                                 <textarea id="ops_remark" name="ops_remark" class="form-control"></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                           <button type="submit" id="form_submit" class="btn blue">Save changes</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <!-- END MODAL FOR UPDATE PRODUCT STATUS -->

            <!-- START MODAL FOR ADD SHIPPING CHARGES -->
            <div class="modal" id="AddProductShippingCharge" tabindex="-1" role="dialog" aria-hidden="true">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="app-shipping-charge-modal-title"></h4>
                     </div>
                     <form role="form" id="formAddProductShippingCharge" method="POST">
                        <input type="hidden" name="odp_id" id="odp_id">
                        <input type="hidden" name="order_id" id="order_id">
                        <!-- <input type="hidden" name="product_id" id="product_id"> -->
                        <div class="modal-body">
                           <div class="form-group">
                              <label for="form_control_1">Enter Shipping Charges<span style="color:red">*</span></label>
                              <input class="form-control" name="product_shipping_charges" id="product_shipping_charges" placeholder="Shipping Charges" required />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                           <button type="submit" id="form_submit" class="btn blue">Save changes</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <!-- START MODAL FOR ADD SHIPPING CHARGES -->

            <!-- BEGIN FOOTER -->
            <?php $this->load->view('common/footer') ?>
            <!-- END FOOTER -->
         </div>

         <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
         <!-- END CORE PLUGINS -->

         <!-- BEGIN THEME GLOBAL SCRIPTS -->
         <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
         <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
         <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
         <!-- END PAGE LEVEL SCRIPTS -->
         <!-- BEGIN THEME LAYOUT SCRIPTS -->
         <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
         <script src="<?php echo base_url(); ?>public/js/form_validation/update-order-product-status.js"></script>
</body>

</html>