<?php
   if($this->session->userdata('tsn_usr_id')==''){
      $abc = base_url();
        echo '<script> ';
          echo 'window.location="'.$abc.'"';
        echo '</script>';
    }   
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>TSN |  Product Flavours</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
             <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
           <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url()?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  
  <style type="text/css">
  #message

  {
    color:red;
  }
 
  .help-block
  {
   margin-bottom: 0;
   text-align: left;
   font-size: 12px;
   line-height: 15px;
   display: block;
   color: #c40000;
 }
 </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header')?>  
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar')?>  
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard')?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <span>Flavours Images</span>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12 ">
              <div class="portlet light bordered">
               <div class="portlet-title">
          <span class="caption-subject bold uppercase font-orange">Flavours  Images&nbsp; <a    id="addnewclick"  class="btn btn-sm red btn-outline filter-cancel">Add Flavour Images</a></span>
          <div class="tools">


          </div>
        </div>

               <div class="portlet-body form" >
                 <!-- BEGIN FORM-->
                 <div class="form" id="addnewform" style="display:none">
                   <form  id="add_param" method="post" action="" class="horizontal-form">
                     <!-- HIDDEN FIELDS -->
                     <input type="hidden" id="pfi_id" name="pfi_id" value="">
                     <input type="hidden" id="pfi_old_img" name="pfi_old_img" value="">
                     <input type="hidden" id="pfi_img_path" name="pfi_icon" value="<?php echo base_url().PRODUCT_BIG_IMAGE_PATH?>">
                     <input type="hidden" id="pfi_default_img" name="pfi_default_img" value="<?php echo base_url().DEFAULT_IMG_PATH?>">
                     <div class="form-body" style="padding: 0px;">
                       <div class="row">
                        <div class="col-md-3" id="prd-drop">
                         <div class="form-group"> 
					 <select required="" id="pfi_prd_id" name="pfi_prd_id" onchange="loadFlavours(this.value)" class="form-control select2"  >
                      <option value="">Select Product</option>
                      <?php 
                      foreach ($products as $key) {
						echo '<option value="'.$key->prd_id.'">'.$key->prd_name.'</option>';
						} 
						 ?>
                        </select>

                         </div>
                      </div>
                      <div class="col-md-3" id="prd-input">
                           <div class="form-group">
                    <input type="text"  class="form-control" disabled id="product" name="product" >
                          </div>
                        </div>
                         <div class="col-md-3" id="flavour-drop">
                           <div class="form-group">
 					<select required="" id="pfi_flv_id" name="pfi_flv_id"  class="form-control select2"  >
                      <option value="">Select Flavour</option>
                      </select>
                          </div>
                        </div>
                         <div class="col-md-3" id="flavour-input">
                           <div class="form-group">
                    <input type="text"  class="form-control" disabled id="flavour" name="flavour" >
                          </div>
                        </div>
                          <div class="col-md-3">
                         <div class="fileinput fileinput-new" data-provides="fileinput">

                           <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img id="pfi_img" src="<?php echo base_url().DEFAULT_IMG_PATH?>" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                            <div>
                             <span class="btn default btn-file">
                               <span class="fileinput-new"> Select Image </span>
                               <span class="fileinput-exists"> Change </span>
                               <input  type="file" id="file" name="file" required=""></span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                               <h6 id="message"></h6>
                             </div>
                             <h6 id="message"></h6>
                           </div>

                         </div>

                      <div class="col-md-3">
                      <div class="form-group">

                        <button type="submit"  class="btn blue" name="form_submit" id="form_submit" >
                         Save</button>
                         <button type="button" onclick="addnewclickCancel()" class="btn" name="form_submit_cancel" id="form_submit_cancel" >
                           Cancel</button>

                         </div>
                       </div>
                     </div>

                   </div>
                 </form>
                 <!-- END FORM-->
               </div>
             </div>

             <div class="portlet-body">
              <table class="table  table-bordered table-hover pagination_table dataTable no-footer" id="sample_1">
                <thead>
                  <tr>
                    <th style="display: none;">  </th>

                    <th>Product</th>
                    <th>Flavour</th>
                   <th>Image</th>
                    <th>Action</th> 


                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  foreach ($product_flavours as $key ) 
                  {
                    ?>
                    <tr >
                     <td style="display: none"></td>

                     <td><?php echo $key->product_name;?></td>
                     <td><?php echo $key->flavour_name;?></td>
                     <td><a  data-toggle="modal" id="info" href="#" data-flavour="<?php echo $key->flavour_name;?>" data-product="<?php echo $key->product_name;?>" onclick="showModel(this,'<?php echo  base_url().PRODUCT_BIG_IMAGE_PATH.$key->pfi_img?>')"><img width="150px" height="100px"  src="<?php echo base_url().PRODUCT_BIG_IMAGE_PATH.$key->pfi_img?>"></a></td>

                     <td><a  class="edit" title="Edit <?php echo $key->product_name;?>" id="updatenewclick" onclick="edit(<?php echo $key->pfi_id;?>)"><i  class="fa fa-pencil" aria-hidden="true" ></i></a> </tr>
                    <?php 
                    $i++;  
                  }
                  ?>


                </tbody>
              </table>
            </div>
            <div class="modal fade" id="Add" tabindex="-1" role="Add" aria-hidden="true">
             <div class="modal-dialog">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <strong> <h4 align="center" class="modal-title" id="modal-title"></h4></strong> 
                 </div>
                 <form role="form" id="add_bank_details" style="text-align: center" >
                   <img  height="400px" width="400px" src="" id="img">

                   <div class="modal-footer">
                     <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                   </div>
                 </form>
               </div>

             </div>

           </div>


           
         </div>
       </div>
     </div>
   </div>
   <!-- END CONTENT BODY -->
 </div>
 <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php $this->load->view('common/footer')?> 
<!-- END FOOTER -->
</div>


<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script src="<?php echo base_url()?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>      
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
   <script src="<?php echo base_url()?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url()?>public/assets/pages/scripts/components-select2.js" type="text/javascript"></script>
       <script src="<?php echo base_url();?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url()?>public/assets/pages/scripts/table-datatables-rowreorder.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/form_validation/editable-product-flavour.js"></script>  
<script src="<?php echo base_url();?>public/js/form_validation/add-product-flavour.js"></script> 

<script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script type="text/javascript">
function showModel(current_tag,img_path,flv_name)
{
 current_tag.setAttribute("href", "#Add");
 document.getElementById('img').setAttribute('src',img_path);
 document.getElementById('modal-title').innerHTML=$('#info').data("product") + '('+ $('#info').data("flavour")+ ')';
}

</script>
</body>

</html>
