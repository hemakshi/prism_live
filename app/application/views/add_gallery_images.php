<?php
if($this->session->userdata('hnv_prs_id')==''){
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="'.$abc.'"';
  echo '</script>';
}   
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>H & V | Gallery</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url()?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url()?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- select2 css files -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url()?>public/assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="<?php echo base_url().'/'.$this->home_model->getBsnData('logo_ico'); ?>" alt="" >




        <style type="text/css">
              #message{
                position:absolute;
                top:10%;/*55px;*/
                color:red;
                left:50%;
                }
       </style>

  <!--start styling for multiple images -->
  <style type="text/css">
    input[type="file"] {
      display: block;
    }
    .imageThumb {
      /*max-height: 75px;*/
      border: 2px solid;
      padding: 1px;
      cursor: pointer;
      width: 110px;
      height: 75px;
    }
    .pip {
      display: inline-block;
      margin: 10px 10px 0 0;
    }
    .remove {
      display: block;
      background: #444;
      border: 1px solid black;
      color: white;
      text-align: center;
      cursor: pointer;
    }
    .remove:hover {
      background: white;
      color: black;
    }
    .uploaded {
      display: block;
      background:#0BB7A5;;
      border: 1px solid black;
      color: white;
      text-align: center;
      cursor: pointer;
      font-size: 1.05em;
      font-weight: 500;
    }
    .listing-row:hover {
      box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.12);
    }

    #information{
      position: absolute;
      left: -140px;
      bottom: -30px;

      display: none;
    }

    #information1{
      position: absolute;
      left: 66px;
      bottom: -30px;

      display: none;
    }
  </style>






</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header')?> 
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar')?>  
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard')?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
               <a href="<?php echo site_url('gallery-images-list')?>">Gallery</a>
               <i class="fa fa-circle"></i>
             </li>
             <li><span>Gallery Images Add</span></li>
           </ul>
         </div>
         <!-- END PAGE BAR -->
         <!-- BEGIN PAGE TITLE-->
                       <!--  <h1 class="page-title"> Blank Page Layout
                            <small>blank page layout</small>
                          </h1> -->
                          <!-- END PAGE TITLE-->
                          <!-- END PAGE HEADER-->                        

                          
                          <div class="row">
                            <div class="col-md-12 ">
                              <!-- BEGIN SAMPLE FORM PORTLET-->
                              <div class="portlet light bordered">
                                <div class="portlet-title">
                                  <div class="caption font-orange">
                                    <i class="icon-user font-orange"></i>
                                    <span class="caption-subject bold uppercase">Add Gallery Images</span>
                                  </div>
                                </div>
                                      <div class="portlet-body form">
                                        <div id="error" style="text-align: center;">
                                        </div>
                                        <form id="add_gallery_images" class="horizontal-form">
                                         <div class="form-body">
                                          <div class="row">
                                           
                                                                    <div class="col-md-11">
                                                                            <input type="hidden" class="form-control" id="documents_count" name="documents_count"  value="0" />
                                                                            <input type="hidden" id="upload_button_value_img" name="upload_button_value_img" value="1">
                                                                            <input type="hidden" id="submit_button_value_docs" name="submit_button_value_docs" value="1">
                                                                            <div class="form-group files" style="position: relative;">
                                                                              <label>Gallery Images</label>                                              
                                                                          
                                                                                <input required="" type="file" id="files1" name="files1" multiple />
                                                                                   
                                                                            
                                                                               <!-- <input required="" type="file" id="files_documents" name="files1" multiple /> -->
                                                                              
                                                                               <br> <br> 
                                                                               <div class="fileList"  id='documents_priview'></div>

                                                                             <h id="doc_uploaded" style="color:green"></h> <br> 
                                                                             <h id="doc_error_invalid_size" style="color:red"></h>
                                                                             <!-- <button type="button"  style="display:none;margin-top:5px;margin-bottom: 50px;" onclick="return insertImages()"  class="btn btn-primary" id="upload_docsad">Upload</button> -->
                                                                              <img id="information1" src = "<?php echo base_url()?>public/images/info.png">
                                                                           </div> 
                                            </div>
                                          </div>
                                          
                                        </div>
                                     <div class="form-actions ">

                                      <button type="submit" class="btn blue" name="submit-button" id="upload_docs">Save</button>
                                      <button type="button" class="btn default" name="cancel_button" id="cancel_button" onclick="history.go(-1)">Cancel</button>                                                            

                                    </div>


                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>                         

                          
                  </div>
                </div>
              </div>
              <!-- BEGIN FOOTER -->
        <?php $this->load->view('common/footer')?> 
            </div>
            <!-- END CONTENT BODY -->

      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
      <!-- END CORE PLUGINS -->
      <!-- BEGIN PAGE LEVEL PLUGINS -->
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/dropify/js/dropify.min.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL PLUGINS -->
      <!-- BEGIN THEME GLOBAL SCRIPTS -->
      <script src="<?php echo base_url()?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
      <!-- END THEME GLOBAL SCRIPTS -->
      <!-- BEGIN THEME LAYOUT SCRIPTS -->
      <script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
      <!-- END THEME LAYOUT SCRIPTS -->
      <!--  Date Picker -->
      <!-- BEGIN PAGE LEVEL PLUGINS -->

      <script src="<?php echo base_url()?>public/assets/global/plugins/moment.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>

      <script src="<?php echo base_url()?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

      <!-- END PAGE LEVEL PLUGINS -->
      <!-- End Date Picker -->
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url()?>public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/js/form_validation/add-event.js"></script> 

       <script src="<?php echo base_url()?>public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url()?>public/js/form_gallery_images_insert.js" ></script>



<!-- Date range -->

<script type="text/javascript">
        $.fn.fileUploader = function (filesToUpload) {
           

            this.closest(".files").change(function (evt) {

          

             document.getElementById('upload_button_value_img').value='0';
             // $('#upload_docs').css('display','block');
              // $("#information1").css('display','block');
             document.getElementById('documents_count').value=+document.getElementById('documents_count').value + +evt.target.files.length;
             for (var i = 0; i < evt.target.files.length; i++)
             {
                filesToUpload.push(evt.target.files[i]);
            };
            var output = [];
            document.getElementById("documents_priview").innerHTML='';
           for (var i = 0, f; f = evt.target.files[i]; i++) 
           {
          var sizeInMB = ( f.size / (1024*1024)).toFixed(2);
          // alert(sizeInMB + 'MB');
                    // <span class="label label-danger">NOTE!</span>
                    
                    // var removeLink = "<a class=\"removeFile label label-danger\" href=\"#\" data-fileid=\"" + i + "\">Remove</a>";
                    var removeLink = "<button type='button' class=\"btn btn-danger in-form\" onclick=\"removeDocument("+i+");\" ><i class='fa fa-close'></i></button>";

                    
                    data= "<div id=\"document" +i+ "\"  class=\"row\"><div  class=\"col-md-6\"><span class=\"pip\">" + "<img class=\"imageThumb\" src=\"" +  URL.createObjectURL(evt.target.files[i]) + "\" />" +"</span> - <strong>"+f.name+ "</strong>-"+ sizeInMB+" MB. &nbsp; &nbsp;</div><div class=\"col-md-3\"><input type='text' id='gal_title"+i+"' name='gal_title"+i+"' class='form-control' placeholder='Image Title(Optional)' ></div><div class=\"col-md-3\">"+removeLink+"</div> </div><input type='hidden' id='document_input"+i+ "' value=\"1\" ></div>";
                          var div = document.getElementById('documents_priview');
                       // document.getElementById("documents_priview").innerHTML=div.innerHTML +data;
                       document.getElementById("documents_priview").innerHTML=div.innerHTML +data;
                      

                }

             });
        };

        var filesToUpload = [];

        $(document).on("click",".removeFile", function(e){
         document.getElementById('documents_count').value=document.getElementById('documents_count').value - 1;
         e.preventDefault();
         var fileName = $(this).parent().children("strong").text();
             // loop through the files array and check if the name of that file matches FileName
            // and get the index of the match
            for(i = 0; i < filesToUpload.length; ++ i){
                if(filesToUpload[i].name == fileName){
                    //console.log("match at: " + i);
                    // remove the one element at the index where we get a match
                    filesToUpload.splice(i, 1);
                }   
            }
            //console.log(filesToUpload);
            // remove the <li> element of the removed file from the page DOM
            $(this).parent().remove();
        });


        $("#files1").fileUploader(filesToUpload);
        $("#files2").fileUploader(filesToUpload);

        $("#uploadBtn").click(function (e) {
            e.preventDefault();
        });

    </script>


<script type="text/javascript">
  function removeDocument(doc_id) 
  {

    $("#document"+doc_id).remove();

    document.getElementById('document_input'+doc_id).value='0';
     // document.getElementById('documents_count').value=document.getElementById('documents_count').value - 1;

  }
</script>



<!-- End Date Range -->
</body>

</html>