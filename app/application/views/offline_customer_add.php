<?php
if ($this->session->userdata('tsn_usr_id') == '' || $this->session->userdata('tsn_usr_dpt_id') != ADMIN_DEPARTMENT) {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Add Offline Customer</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- select2 css files -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="<?php echo base_url() . '/' . $this->home_model->getBsnData('logo_ico'); ?>" alt="">
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header') ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <a href="<?php echo site_url('offline_customers') ?>">Offline Customers</a>
                <i class="fa fa-circle"></i>
              </li>
              <li><span>Add</span></li>
            </ul>
          </div>
          <!-- END PAGE BAR -->

          <!-- END PAGE HEADER-->
          <div class="row">
            <div class="col-md-12 ">
              <!-- BEGIN SAMPLE FORM PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-orange">
                    <i class="icon-user font-orange"></i>
                    <span class="caption-subject bold uppercase">Add Offline Customer</span>
                  </div>
                </div>

                <div class="portlet-body form">
                  <div id="error" style="text-align: center;">
                  </div>
                  <form id="add_offline_user" method="POST" role="form" class="horizontal-form">
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_name" class="control-label">Full Name<span style="color:red">*</span></label>
                            <input type="text" class="form-control" id="customer_name" name="customer_name" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_username" class="control-label">User Name<span style="color:red">*</span></label>
                            <input type="text" id="customer_username" name="customer_username" class="form-control" onblur="return isAlphaNumeric(this.value),check_validation('offline_user_username',this.value,this.id)" pattern=".{6,}" minlength="6" required>
                            <span class="help-block"></span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_mobile" class="control-label">Mobile<span style="color:red">*</span></label>
                            <input type="text" id="customer_mobile" name="customer_mobile" class="form-control" onblur="return check_validation('offline_user_mobile',this.value,this.id)" onkeypress='return validateMOBILE(event);' required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_email" class="control-label"> Email<span style="color:red">*</span></label>
                            <input type="text" id="customer_email" name="customer_email" class="form-control" onblur="return check_validation('offline_user_email',this.value,this.id)" required>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <!--   <div class="form-group">
                            <label class="control-label">Gender</label>
                            <select class="form-control select1" id="customer_gender" name="customer_gender" required="">
                              <option>Select Gender</option>
                              <?php
                              global $gnp_active_status;
                              echo $this->home_model->getCombo("SELECT gnp_value as f1, gnp_name as f2 from gen_prm where gnp_group = 'gender' AND gnp_status=" . ACTIVE_STATUS . "");
                              ?>
                            </select>
                          </div> -->
                        </div>
                        <!--  <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Birth Date</label>
                            <input type="date" id="customer_dob" name="customer_dob" />
                          </div>
                        </div> -->
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_address_line" class="control-label">Address Line<span style="color:red">*</span></label>
                            <textarea id="customer_address_line" name="customer_address_line" required></textarea>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_district" class="control-label">District<span style="color:red">*</span></label>
                            <input type="text" class="form-control" id="customer_district" name="customer_district" required>
                          </div>
                        </div>

                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_city" class="control-label">City<span style="color:red">*</span></label>
                            <input type="text" class="form-control" id="customer_city" name="customer_city" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_state" class="control-label">State<span style="color:red">*</span></label>
                            <select class="form-control" id="customer_state" name="customer_state" required>
                              <option value="" selected disabled>--- Select State ---</option>
                              <?php echo getDropdownResult('state-dropdown', 'pad_state', 'pad_state'); ?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_pincode" class="control-label">Pincode<span style="color:red">*</span></label>
                            <input type="text" class="form-control" id="customer_pincode" name="customer_pincode" required>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="customer_gst" class="control-label">GST No.</label>
                            <input type="text" class="form-control" id="customer_gst" name="customer_gst">
                          </div>
                        </div>
                      </div>

                      <div class="form-actions">
                        <button type="submit" class="btn blue" name="form_submit" id="form_submit">Add</button>
                        <button type="button" class="btn default" name="cancel_button" id="cancel_button" onclick="history.go(-1)">Cancel</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT BODY -->
      </div>
      <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('common/footer') ?>
    <!-- END FOOTER -->
  </div>

  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/dropify/js/dropify.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
  <!-- END THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>public/js/form_validation/offline_customer_add.js"></script>

  <script type="text/javascript">
    function isAlphaNumeric(str) {
      var code, i, len;
      for (i = 0, len = str.length; i < len; i++) {
        code = str.charCodeAt(i);
        if (!(code > 47 && code < 58)) { // lower alpha (a-z)
          $('.error').html('');
          // /console.log('inside if condition');
          return true;
        }
        $('.error').html('');
      }
      $('.error').html('');
      $('#prs_user_name').addClass('error');
      $('#prs_user_name').val('');
      $('.aplhaNum_error').append('<label class="error">Username cannot contain only numbers</span>');
      //console.log('else condition');
      return false;
    };

    function validateMOBILE(event) {
      var key = window.event ? event.keyCode : event.which;
      if (event.keyCode == 8 || event.keyCode == 46 ||
        event.keyCode == 37 || event.keyCode == 39) {
        return true;
      } else if (key < 48 || key > 57) {
        return false;
      } else return true;
    };

    function check_validation(type, value, id) {
      data = {
          type: type,
          value: value
        },
        $.ajax({
          type: "POST",
          url: base_url + "offline_controller/checkValidationForOfflineUser",
          data: data,
          dataType: "json",
          success: function(response) {
            if (response.success == true) {
              $('#' + id).html('');
              $('#' + id).parent().find('.error').html('');
              $('#' + id).parent().append('<span class="error">Data already exists</span>');
            } else {
              $('#' + id).html('');
              $('#' + id).parent().find('.error').html('');
            }
          }
        });
    }
  </script>
</body>

</html>