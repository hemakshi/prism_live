<?php
if ($this->session->userdata('tsn_usr_id') == '' || $this->session->userdata('tsn_usr_dpt_id') != ADMIN_DEPARTMENT) {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | <?php echo $user->offline_user_username; ?> Detail</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <link href="<?php echo base_url() ?>public/assets/xlsTableFilter/css/xlstablefilter.css" type="text/css">
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- <link rel="shortcut icon" href="favicon.ico" /> -->
  <style type="text/css">
    .td-style {
      width: 10%;
      text-align: center;
    }

    .table td:first-child {
      width: 300px !important;
    }
  </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('common/header') ?>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <a href="<?php echo site_url('offline_customers') ?>">Offline Customers</a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <span><?php echo $user->offline_user_username; ?></span>
            </li>
          </ul>

        </div>
        <!-- END PAGE BAR -->

        <!-- END PAGE HEADER-->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-orange">
                  <span class="caption-subject bold uppercase">Customer Detail</span>
                </div>
                <div class="btn-group" style="padding-left: 10px">
                  <a href="<?php echo site_url('customer_edit/' . $user->offline_user_id) ?>" title="Edit" class="btn green">
                    <i class="fa fa-edit"></i>
                  </a>
                </div>

                <div class="tools"> </div>
              </div>
              <div class="portlet-body ">
                <div class="detail">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 table-responsive">
                      <table class="table table-striped table-bordered table-hover  order-column text-left">
                        <tr>
                          <td>Full Name</td>
                          <td><?php echo $user->offline_user_fullname; ?></td>
                          <td>User Name</td>
                          <td><?php echo $user->offline_user_username; ?></td>
                        </tr>
                        <tr>
                          <td>Mobile No</td>
                          <td><?php echo $user->offline_user_mobile; ?></td>
                          <td>Email</td>
                          <td><?php echo $user->offline_user_email; ?></td>
                        </tr>
                        <!--<tr>-->
                        <!--  <td>Gender</td>-->
                        <!--  <td><?php echo $user->gender; ?></td>-->
                        <!--  <td>Birth Date</td>-->
                        <!--  <td><?php echo date('d-M-Y', strtotime($user->offline_user_dob)); ?></td>-->
                        <!--</tr>-->
                        <tr>
                          <td>GST No.</td>
                          <td><?php echo $user->offline_user_gst ? $user->offline_user_gst : "-"; ?></td>
                          <td>Status</td>
                          <td>
                            <?php
                            switch ($user->offline_user_status) {
                              case 1:
                                echo "Active";
                                break;
                              case 0:
                                echo "Deactive";
                                break;
                              default:
                                echo "-";
                                break;
                            }
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td> Address </td>
                          <td>
                            <?php echo $user->offline_user_address_line . ",<br>"; ?>
                            <?php echo $user->offline_user_city . "-" . $user->offline_user_pincode . ",<br>"; ?>
                            <?php echo $user->offline_user_district . ",<br>"; ?>
                            <?php echo $user->state_name; ?>
                          </td>
                          <td>Created On</td>
                          <td><?php echo date('d-m-Y', strtotime($user->offline_user_created_on)); ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>

                </div>

              </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>

      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('common/footer') ?>
  <!-- END FOOTER -->

  <!-- BEGIN CORE PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="<?php echo base_url() ?>public/assets/xlsTableFilter/js/jquery.xlstablefilter.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <!-- END THEME LAYOUT SCRIPTS -->
  <script type="text/javascript">
    $(function() {
      $('#sample_1_length').after(' <div style="text-align:center;color:darkred;margin:3px;font-weight:bold" id="divRowsDisplay"></div');
      $("#sample_1").xlsTableFilter({
        rowsDisplay: "divRowsDisplay"
      });
    });

    $('.ui-dialog-buttonset > .ui-button').click(function() {
      $('#divRowsDisplay').css('display', 'block');
    });


    $(document).keypress(function(e) {
      if (e.which == 13) {
        $('.ui-dialog-buttonset > .ui-button').click();
      }
    });
  </script>

</body>

</html>