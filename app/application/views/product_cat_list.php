<?php
if ($this->session->userdata('tsn_usr_id') == '') {
  $abc = base_url();
  echo '<script> ';
  echo 'window.location="' . $abc . '"';
  echo '</script>';
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Prism Prints | Product Category</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="<?php echo base_url() ?>public/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />

  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="<?php echo $this->home_model->getBsnData('logo_ico'); ?>" alt="">
  <style type="text/css">
    .portlet.light .dataTables_wrapper .dt-buttons {
      margin-top: -64px;
      display: none;
    }

    .dataTables_wrapper .dataTables_length,
    .dataTables_wrapper .dataTables_filter,
    .dataTables_wrapper .dataTables_info,
    .dataTables_wrapper .dataTables_processing,
    .dataTables_wrapper .dataTables_paginate {
      color: #333;
      display: none;
    }
  </style>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
  <div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('common/header') ?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('common/sidebar') ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <!-- BEGIN PAGE BAR -->
          <div class="page-bar">
            <ul class="page-breadcrumb">
              <li>
                <a href="<?php echo site_url('dashboard') ?>">Dashboard</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <span>Product Categories</span>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12 ">
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-orange">
                    <!-- <i class="icon-user font-orange"></i> -->
                    <span class="caption-subject bold uppercase">Product Categories</span>
                  </div>
                  <div class="btn-group" style="padding-left: 10px">
                    <button id="addnewclick" class="btn green">
                      Add New <i class="fa fa-plus"></i>
                    </button>
                  </div>
                  <div class="tools"> </div>
                </div>

                <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <div class="form" id="addnewform" style="display:none">
                    <form id="add_param" method="post" action="" class="horizontal-form">
                      <input type="hidden" id="cat_id" name="cat_id" value="">
                      <div class="form-body" style="padding: 0px;">
                        <div class="row">
                          <!-- <div class="col-md-3" id="prd-drop">
                         <div class="form-group"> 
           <select required="" id="cat_menu" name="cat_menu"  class="form-control select2"  >
                      <option value="">Select Menu</option>
                      <?php
                      foreach ($menu as $key) {
                        echo '<option value="' . $key->gnp_value . '">' . $key->gnp_name . '</option>';
                      }
                      ?>
                        </select>

                         </div>
                      </div> -->

                          <div class="col-md-3">
                            <div class="form-group">

                              <input class="form-control" placeholder="Category Name" id="cat_name" name="cat_name" type="text" required>

                            </div>
                          </div>

                          <div class="col-md-2">
                            <div class="form-group">

                              <input class="form-control" placeholder="Category Order" id="cat_order" name="cat_order" type="text" required>

                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">

                              <button type="submit" class="btn green" name="form_submit" id="form_submit">
                                Save</button>
                              <button type="button" onclick="addnewclickCancel()" class="btn" name="form_submit" id="form_submit">
                                Cancel</button>

                            </div>
                          </div>
                        </div>

                      </div>
                    </form>
                    <!-- END FORM-->
                  </div>
                </div>

                <div class="portlet-body">
                  <table class="table  table-bordered table-hover pagination_table dataTable no-footer" id="sample_1">
                    <thead>
                      <tr>
                        <th style="display: none;"> </th>
                        <th style="width: 50px;">Sr No.</th>
                        <th>Name</th>
                        <!-- <th>Menu</th> -->
                        <th>Order</th>
                        <th>Action</th>


                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      foreach ($category as $key) {
                      ?>
                        <tr>
                          <td style="display: none"></td>
                          <td style="width: 50px;"><?php echo $i; ?></td>
                          <td><?php echo $key->cat_name; ?></td>
                          <!--   <td><?php echo $key->menu; ?></td> -->
                          <td><?php echo $key->cat_order; ?></td>
                          <td>
                          
                            <div class="btn-group">
                              <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                <i class="fa fa-angle-down"></i>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li>
                                  <a href="#" title="Edit <?php echo $key->cat_name; ?> Content " id="updatenewclick" onclick="edit(<?php echo $key->cat_id; ?>)">

                                    <i style="   font-size: 20px;  color:#EF7F1A;" class="fa fa-pencil" aria-hidden="true"></i> Edit Content</a>
                                </li>
                               
                                <li>
                                  <a onclick="deleteCategory(<?php echo $key->cat_id; ?>)">
                                    <i style="font-size: 20px; color:#EF7F1A;" class="fa fa-trash" aria-hidden="true"></i> Delete Category
                                  </a>
                                </li>

                              </ul>
                            </div>
                          </td>
                        </tr>
                      <?php
                        $i++;
                      }
                      ?>


                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CONTENT BODY -->
      </div>
      <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('common/footer') ?>
    <!-- END FOOTER -->
  </div>


  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/datatable.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo base_url() ?>public/assets/pages/scripts/table-datatables-rowreorder.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url() ?>public/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>public/js/form_validation/editable-product-category.js"></script>
  <script src="<?php echo base_url(); ?>public/js/form_validation/add-product-category.js"></script>

  <script type="text/javascript">
    function deleteCategory(cat_id) {
      if (confirm('Are you sure?') == true) {
        $.ajax({
          url: '<?php echo base_url('product/deleteProductCategory'); ?>',
          method: "post",
          dataType: "json",
          data: {
            cat_id: cat_id
          },
          success: function(response) {
            if (response.success == true) {
              alert(response.message);
              location.reload();
            } else {
              alert(response.message);
            }
          }
        })
      }
    }
  </script>

  <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>