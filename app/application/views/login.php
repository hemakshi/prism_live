<?php
if ($this->session->userdata('tsn_usr_id') != '') {
    echo '<script> ';
    echo 'window.location="' . base_url() . "dashboard" . '"';
    echo '</script>';
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Prism Prints | User Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo base_url() ?>public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo base_url() ?>public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url() ?>public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url() ?>public/assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="<?php echo $this->home_model->getBsnData('logo_ico'); ?>" alt="">
</head>


<!-- END HEAD -->
<style type="text/css">
    .loader {
        display: table;
        position: absolute;
        top: 0;
        background: #fff;
        height: 100%;
        width: 100%;
        z-index: 9999;
    }

    .loader .img-wrap {
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }

    .login .content h3,
    .login .content h4 {
        color: #fff !important;
    }

    #submit-button:hover {
        border-color: #F29846;
    }

    @media only screen and (max-width:768px) {
        .loader img {
            max-width: 280px;
        }
    }

    @media only screen and (max-width: 480px) {
        .textsize {
            font-size: 15px !important;
        }

        .textsize2 {
            margin: 2px 0 !important;
        }

        .textsize3 {
            width: 300px !important;
        }
    }

    @media only screen and (max-width: 670px) {
        .tabsize {
            margin: 20px auto 0px !important;
        }

    }

    .glyphicon.spinning {
        animation: spin 1s infinite linear;
        -webkit-animation: spin2 1s infinite linear;
    }

    .form-control.error {
        border: 1px solid #ff6161;
        ;
    }

    label.error {
        display: none !important;
    }
</style>

<body class=" login" style="background-color: white!important;">
    <div class="loader">
        <div class="img-wrap">
            <img src="<?php echo $this->home_model->getBsnData('logo'); ?>" alt="" />
        </div>
    </div>
    <!-- BEGIN LOGO -->
    <div class="logo tabsize" style="margin: 60px auto 0px;">
        <a href="">
            <img src="<?php echo $this->home_model->getBsnData('logo'); ?>" alt="" /> </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div id="invalid_user_msg" style="text-align: center;display: none">
        <h style="color:#FF0000;"><?php echo INVALID_USER ?></h>
    </div>
    <div class="content textsize3" style="background-color: #ef7e18">
        <!-- BEGIN LOGIN FORM -->

        <form class="login-form" action="" autocomplete="off" method="post" id="login_form">
            <h3 class="form-title" style="    margin-top: 0px;">Login</h3>
            <input type="hidden" name="ref" id="ref" value="<?php echo $ref ?>">
            <div class="form-group">
                <!-- <input type="hidden" id="ref" value="<?php echo $ref ?>"> -->

                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="new-password" placeholder="Username" required name="usr_username" id="usr_username" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="new-password" placeholder="Password" required name="usr_password" id="usr_password" />
                </div>
            </div>
            <div class="form-actions">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="rememberme" id="rememberme" value="1" /> Remember me
                    <span></span>
                </label>
                <button type="submit" class="btn green pull-right login-button" id="form_submit"> Login </button>

            </div>

            <div class="forget-password textsize2">
                <h4><a href="javascript:;" id="forget-password">Forgot password </a></h4>
            </div>

        </form>
        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" action="" id="admin_forget_pass_form" method="post">
            <h3>Forgot Password </h3>
            <p class="light-blue-color block" id="error"></p>
            <div style="color: #fff;">We will send you a link to reset your password.</div>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" id="email" />
                </div>
            </div>
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn red btn-outline">Back </button>
                <button type="submit" class="btn green pull-right" id="pw-submit"> Submit </button>
                <button class="btn btn-danger" style="display:none" id="pw_processing"><span class="glyphicon glyphicon-refresh spinning"></span> Processing</button>
            </div>
        </form>
        <!-- END FORGOT PASSWORD FORM -->
        <!-- BEGIN REGISTRATION FORM -->

        <!-- END REGISTRATION FORM -->
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright" style="color: black"> &copy; 2020 PRISM PRINTS. ALL RIGHTS RESERVED.</div>

    <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <!-- <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url() ?>public/js/form_validation/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url() ?>public/js/form_validation/login.js"></script>
    <script src="<?php echo base_url() ?>public/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>public/assets/pages/scripts/login-4.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <!-- END THEME LAYOUT SCRIPTS -->

    <script>
        $(window).load(function() {
            $('.loader').fadeOut();
        });
    </script>

    <script>
        var base_url = '<?php echo base_url() ?>';
    </script>
</body>

</html>