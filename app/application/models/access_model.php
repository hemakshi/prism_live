<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Eye View Design CMS module Ajax Model
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   EVD CMS
 * @author    Frederico Carvalho
 * @copyright 2008 Mentes 100Limites
 * @version   0.1
 */

class access_model extends CI_Model
{
    /**
     * Instanciar o CI
     */
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getMenu()
    {
        $sql = "SELECT DISTINCT menu_master.mnu_name,mnu_icon,menu_master.mnu_id,menu_master.mnu_link FROM `menu_master` LEFT JOIN `menu_transaction` ON menu_master.mnu_id=menu_transaction.mtr_mnu_id WHERE menu_transaction.mtr_dpt_id ='" . $this->session->userdata('tsn_usr_dpt_id') . "' AND menu_master.mnu_Status='Y' AND menu_transaction.mtr_Status='Y' ORDER BY menu_master.mnu_order";
        $result = $this->db->query($sql);
        return $result;
    }

    public function getsubmenu($mnu_id)
    {
        $sql = "SELECT * FROM `sub_menu_master` WHERE sbm_Status='Y' AND sbm_group='submenu' AND sbm_mnu_id=" . $mnu_id . " AND sbm_parent_id=" . $mnu_id . " AND sbm_id IN (select fma_sbm_id FROM `form_access` WHERE fma_Status='Y' AND fma_access='1' AND fma_usr_id='" . $this->session->userdata('tsn_usr_id') . "' AND fma_mnu_id='" . $mnu_id . "') ORDER BY sbm_order";
        $result = $this->db->query($sql);
        return $result;
    }

    function getpages($pageid, $moduleid)
    {
        $sql = "SELECT sbm_mnu_id,sbm_pagelink AS form_name,sbm_name AS form_title,SUBSTRING_INDEX(sbm_name,'(',-1) AS pgname FROM `sub_menu_master` WHERE sbm_Status='Y' AND sbm_group='submenu' AND sbm_mnu_id='" . $moduleid . "' AND sbm_id IN (SELECT `fma_sbm_id` FROM form_access WHERE fma_Status='Y' AND fma_access='1' AND fma_usr_id='" . $this->session->userdata('tsn_usr_id') . "' AND sbm_parent_id='" . $pageid . "' AND fma_mnu_id='" . $moduleid . "') ORDER BY SUBSTRING_INDEX(sbm_name,'(',-1)";
        $result = $this->db->query($sql);
        return $result;
    }
}