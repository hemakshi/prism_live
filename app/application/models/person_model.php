<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class person_model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }


  public function getUserdataForLogin($usr_username, $usr_password)
  {
    $sql1 = "SELECT *,
    (SELECT department.dpt_name FROM `department` WHERE department.dpt_id = user.usr_dpt_id) dpt_name
    FROM `user` WHERE  usr_username ='" . $usr_username . "' and usr_password='" . $usr_password . "' and  usr_status='" . ACTIVE_STATUS . "'  ";
    $query1 = $this->db->query($sql1);
    return $query1->row();
  }

  public function getUserdataByEmail($userEmail)
  {
    $sql1 = "SELECT * FROM user WHERE usr_email='" . $userEmail . "' AND  usr_status=" . ACTIVE_STATUS . " ";
    $query1 = $this->db->query($sql1);
    return $query1->row();
  }
  public function sendMailToUserForPasswordReset($user_data)
  {
    $resetPwdData = array();
    $resetPwdData['user_data'] = $user_data;
    $resetPwdData['email']  = $user_data->usr_email;
    $resetPwdData['link']   = $user_data->link;

    $this->load->model('communication_model'); // load model
    $mailData = $this->communication_model->getMailMsg('admin_reset_password', '', $resetPwdData);    

    $isMailSend = $this->home_model->sendMail($user_data->prs_email, $mailData['subject'], $mailData['body']);
    return $isMailSend == true ? $isMailSend : false;
  }
  public function getForgotpswdTransaction($fpt_prs_id, $fpt_code)
  {
    $sql = "SELECT * FROM `forgot_password_transaction` WHERE  fpt_prs_id= '" . $fpt_prs_id . "' AND fpt_is_admin_user = 1 and fpt_code = '" . $fpt_code . "' and NOW() <= DATE_ADD(fpt_crtd_dt, INTERVAL " . FPT_LINK_VALIDITY_TIME . ") ORDER BY fpt_id DESC LIMIT 1";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }
}
