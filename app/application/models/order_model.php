<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class order_model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  public function getAllOrders($status = false)
  {
    $incompleteOrder = 0;
    $sql = "SELECT `ord_id`, `ord_reference_no`,prs_name,prs_mob,prs_email, `ord_prs_id`, `ord_delivery_adddress`, `ord_payment_mode`, `ord_total_amt`, `ord_date`, `ord_status`, `ord_remark`, `ord_is_success` ,ord_crtd_dt, `ord_shipping_charges`,
	(select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order.ord_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
	(select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order.ord_status and gen_prm.gnp_group='order_status') ord_status_name, `pad_address`, `pad_city`, `pad_pincode`
	from  person_order left join  person_addresses on person_addresses.pad_id=person_order.ord_delivery_adddress left join person on person.prs_id=person_order.ord_prs_id";
    if ($status) {
      $sql .= " WHERE ord_status=" . $status . " AND ord_is_success != " . $incompleteOrder . " ";
    }
    if (!$status) {
      $sql .= " WHERE ord_is_success != " . $incompleteOrder . "";
    }
    $sql .= " ORDER BY ord_crtd_dt DESC";

    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getUserOrderByRefrence($ord_reference_no)
  {
    $sql = "SELECT `ord_id`, `ord_reference_no`,prs_name,prs_mob,prs_email, `ord_prs_id`, `ord_delivery_adddress`, `ord_payment_mode`,`ord_payment_status`, `ord_total_amt`, `ord_date`, `ord_status`,prs_mob ,ord_sub_total,ord_discount,ord_crtd_dt, `ord_shipping_charges`,
   (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order.ord_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
   (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order.ord_status and gen_prm.gnp_group='order_status') ord_status_name,`pad_address_type`, `pad_state`, `pad_name`, `pad_mobile`, `pad_alt_phone`, `pad_locality`, `pad_address`, `pad_landmark`, `pad_pincode`, `pad_city`,
   (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_addresses.pad_address_type and gen_prm.gnp_group='address_type') pad_address_type_name, (select state_master.stm_name from  state_master where state_master. stm_id=person_addresses.pad_state) state_name
   from  person_order left join  person_addresses on person_addresses.pad_id=person_order.ord_delivery_adddress left join person on person.prs_id=person_order.ord_prs_id  where ord_reference_no='" . $ord_reference_no . "' and ord_status!= '" . ORDER_FAIL . "'";
    $query = $this->db->query($sql);
    return $query->row() ? $query->row() : false;
  }
  public function getOrderDataById($ord_id)
  {
    $sql = "SELECT `ord_id`, `ord_reference_no`,prs_name,prs_mob,prs_email, `ord_prs_id`, `ord_delivery_adddress`, `ord_payment_mode`, `ord_total_amt`, `ord_date`, `ord_status`, `ord_remark`, prs_mob ,ord_sub_total,ord_discount,ord_crtd_dt,
   (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order.ord_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
   (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order.ord_status and gen_prm.gnp_group='order_status') ord_status_name,`pad_address_type`, `pad_state`, `pad_name`, `pad_mobile`, `pad_alt_phone`, `pad_locality`, `pad_address`, `pad_landmark`, `pad_pincode`, `pad_city`,
   (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_addresses.pad_address_type and gen_prm.gnp_group='address_type') pad_address_type_name, (select state_master.stm_name from  state_master where state_master. stm_id=person_addresses.pad_state) state_name
   from  person_order left join  person_addresses on person_addresses.pad_id=person_order.ord_delivery_adddress left join person on person.prs_id=person_order.ord_prs_id  where ord_id='" . $ord_id . "'";
    $query = $this->db->query($sql);
    return $query->row() ? $query->row() : false;
  }

  public function insertOrder($ord_reference_no, $payment_type, $prs_id)
  {
    $this->load->library('cart');
    $this->db->trans_start();

    $order_data =
      array(
        'ord_prs_id' => $prs_id,
        'ord_delivery_adddress' => '',
        'ord_reference_no' => $ord_reference_no,
        'ord_source' => 1,
        'ord_payment_mode' => $payment_type,
        'ord_sub_total' => $this->cart->beforetotal(),
        'ord_total_amt' => $this->cart->total(),
        'ord_discount' => '',
        'ord_status' => ORDER_PLACED,
        'ord_date' => date('Y-m-d'),
        'ord_crtd_dt' => date('Y-m-d  H:i:s'),
        'ord_crtd_by' => $this->session->userdata('hnv_web_prs_id'),
        'ord_updt_by' => $this->session->userdata('hnv_web_prs_id')
      );
    $this->db->insert('person_order', $order_data);
    $ord_id = $this->db->insert_id();
    // $order_status_data =
    //   array(
    //     'ods_ord_id' => $ord_id,
    //     'ods_status' => ORDER_PLACED,
    //     'ods_date' => date('Y-m-d'),
    //     'ods_crtd_dt' => date('Y-m-d  H:i:s'),
    //     'ods_crtd_by' => $this->session->userdata('hnv_web_prs_id')

    //   );

    // $this->db->insert('order_status', $order_status_data);
    foreach ($this->cart->contents() as $items) {
      $order_product_data =
        array(
          'odp_ord_id' => $ord_id,
          'odp_prd_id' =>  $items["id"],
          'odp_quantity' => $items["qty"],
          'odp_amt' => $items["subtotal"],
          'odp_disc_type' =>  $items["disc_type"],
          'odp_disc_amt' => $items["disc"],
          'odp_total_amt' => $items["disc_amnt"],
          'odp_status' => ACTIVE_STATUS,
          'odp_date' => date('Y-m-d'),
          'odp_crtd_dt' => date('Y-m-d  H:i:s'),
          'odp_crtd_by' => $this->session->userdata('hnv_web_prs_id')

        );

      $this->db->insert('person_order_products', $order_product_data);
    }
    $this->db->trans_complete();
    return $ord_id;
  }
  public function updateProductOrderStatus($value = '')
  {
    try {
      $data1 = array(
        'odp_status' => $this->input->post('odp_status'),
        'odp_updt_by' => $this->session->userdata('tsn_usr_id')
      );
      $this->db->where('odp_id', $this->input->post('odp_id'));
      $this->db->update('person_order_products', $data1);
      return true;
    } catch (Exception $e) {
      log_message('NEXLOG', 'error occured while updating product order status >> order_model/updateProductOrderStatus ::ERROR-' . $e);
      return false;
    }
  }
  public function updateOrderShippingCharges($orderData)
  {
    try {
      $ord_shipping_charges = $this->input->post('ord_shipping_charges');
      $ord_total_amt = floatval($orderData['ord_sub_total'] + $ord_shipping_charges);
      $postOrderData = array(
        'ord_shipping_charges' => $ord_shipping_charges,
        'ord_total_amt' => $ord_total_amt,
        'ord_updt_by' => $this->session->userdata('tsn_usr_id'),
        'ord_updt_dt' => date('Y-m-d H:i:s')
      );
      $this->db->where('ord_id', $orderData['ord_id']);
      $this->db->update('person_order', $postOrderData);
      return true;
    } catch (Exception $e) {
      log_message('NEXLOG', 'error occured while updating product order status >> order_model/updateOrderShippingCharges ::ERROR-' . $e);
      return false;
    }
  }
  public function insertProductOrderStatus($value = '')
  {
    try {
      $data1 = array(
        'ops_prd_id' => $this->input->post('product_id'),
        'ops_ord_id' => $this->input->post('order_id'),
        'ops_status' => $this->input->post('odp_status'),
        'ops_remark' => $this->input->post('ops_remark'),
        'ops_crtd_dt' => date('Y-m-d H:i:s'),
        'ops_crtd_by' => $this->session->userdata('tsn_usr_id'),
        'ops_updt_by' => $this->session->userdata('tsn_usr_id')
      );
      $this->db->insert('person_order_product_status', $data1);
      return $this->db->insert_id();
    } catch (Exception $e) {
      log_message('NEXLOG', 'error occured while inserting data in product order status >> order_model/insertProductOrderStatus ::ERROR-' . $e);
      return false;
    }
  }

  public function getOrderStatus()
  {
    $status = $this->input->post('status');
    $sql = "SELECT * FROM `gen_prm` WHERE `gnp_group`='order_status' and `gnp_status`= " . ACTIVE_STATUS . "";
    $query = $this->db->query($sql);
    $str = '';
    $str = '';
    foreach ($query->result() as $key) {
      $selected = '';
      $disabled = '';
      if ($status) {
        if ($status == $key->gnp_value) {
          $selected = 'selected="selected"';
        }
        if ($key->gnp_value < $status) {
          $disabled = 'disabled';
        }
      }

      $str .= '<option value="' . $key->gnp_value . '" ' . $selected . '>' . $key->gnp_name . '</option>';
    }
    return $str;
  }

  public function updateOrderStatus($value = '')
  {
    try {
      $data1 = array(
        'ord_status'  => $this->input->post('ord_status'),
        'ord_remark'  => $this->input->post('ops_remark'),
        'ord_updt_by' => $this->session->userdata('tsn_usr_id'),
        'ord_updt_dt' => date('Y-m-d H:i:s')
      );
      $this->db->where('ord_id', $this->input->post('order_id'));
      $this->db->update('person_order', $data1);
      return true;
    } catch (Exception $e) {
      log_message('NEXLOG', 'error occured while updating order status >> order_model/updateOrderStatus ::ERROR-' . $e);
      return false;
    }
  }
}
