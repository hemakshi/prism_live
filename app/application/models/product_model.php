<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function getProductCategories($menu = false)
  {
    $sql = "SELECT *,  (select gen_prm.gnp_name from  gen_prm where gen_prm.gnp_value=product_category.cat_menu and gen_prm.gnp_group='menu') menu FROM `product_category` where cat_status=" . ACTIVE_STATUS . "";
    if ($menu) {
      $sql .= " and cat_menu=" . $menu . " ";
    }
    $sql .= " order by cat_order";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function insertProductCategory($value = '')
  {
    $cat_slug = url_title(word_limiter($this->input->post('cat_name'), SLUG_WORD_LIMIT), 'dash', TRUE);
    $data1 = array(
    //   'cat_id' => $this->input->post('cat_id'),
      'cat_slug' => $cat_slug,
      // 'cat_menu' => $this->input->post('cat_menu'),
      'cat_menu' => ACTIVE_STATUS,
      'cat_name' => $this->input->post('cat_name'),
      'cat_order' => $this->input->post('cat_order'),
      'cat_status' => ACTIVE_STATUS,
      'cat_crtd_by' => $this->session->userdata('tsn_usr_id'),
      'cat_updt_by' => $this->session->userdata('tsn_usr_id'),
      'cat_updt_date' => date('Y-m-d H:i:s'),
      'cat_updt_date' => date('Y-m-d H:i:s')
    );
    return $this->home_model->insert('product_category', $data1);
  }
  public function updateProductCategory($value = '')
  {
    $data1 = array(

      'cat_name' => $this->input->post('cat_name'),
      // 'cat_menu' => $this->input->post('cat_menu'),
      'cat_menu' => ACTIVE_STATUS,
      'cat_order' => $this->input->post('cat_order'),
      'cat_updt_by' => $this->session->userdata('tsn_usr_id'),

    );
    $this->home_model->update('cat_id', $this->input->post('cat_id'), $data1, 'product_category');
    return true;
  }

  public function deleteProductCategory()
  {
    $this->db->where('cat_id', $this->input->post('cat_id'));
    $this->db->delete('product_category');
    if ($this->db->affected_rows()) {
      $this->db->where('prd_cat_id', $this->input->post('cat_id'));
      $this->db->delete('products');
      return true;
    } else {
      return false;
    }
  }

  public function getProducts($value = '')
  {
    $sql = "SELECT *,
 (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name
 -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_quantity_unit and gen_prm.gnp_group='prd_quantity_unit') prd_quantity_name,
 -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_menu and gen_prm.gnp_group='menu') menu,
 -- (SELECT GROUP_CONCAT(flv_name) FROM product_flavours where flv_id in (products.prd_flv_id)) prd_flavours,

   -- (select fp_status from  featured_products where featured_products.fp_prd_id=products.prd_id) featured_product 
   FROM `products` where prd_status=" . ACTIVE_STATUS . " order by prd_order";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }
  public function insertProduct()
  {

    $prd_slug = url_title(word_limiter($this->input->post('prd_name'), SLUG_WORD_LIMIT), 'dash', TRUE);

    $data = array(
      'prd_slug' => $prd_slug,
      'prd_name' => $this->input->post('prd_name'),
      'prd_size' => $this->input->post('prd_size'),
      // 'prd_short_desc' => $this->input->post('prd_short_desc'),
      // 'prd_availability' =>$this->input->post('prd_availability'),
      //'prd_menu' =>$this->input->post('prd_menu'),
      'prd_menu' => ACTIVE_STATUS,
      // 'prd_brand' =>$this->input->post('prd_brand'),
      'prd_cat_id' => $this->input->post('prd_cat_id'),
      'prd_price' => str_replace(',', '', $this->input->post('prd_price')),
      // 'prd_flv_id' => @join(',', $this->input->post('prd_flv_id')),
      // 'prd_quantity_unit' =>$this->input->post('prd_quantity_unit'),
      'prd_order' => $this->input->post('prd_order'),
      // 'prd_descripion' => $this->input->post('prd_descripion'),
      'prd_status' => ACTIVE_STATUS,
      // 'prd_discount' =>$this->input->post('prd_discount'),
      'prd_datetime' => date('Y-m-d H:i:s'),
      'prd_crtd_by' => $this->session->userdata('tsn_usr_id'),
      'prd_updt_by' => $this->session->userdata('tsn_usr_id'),
      'prd_crtd_date' => date('Y-m-d H:i:s'),
      'prd_updt_date' => date('Y-m-d H:i:s')

    );

    $this->db->insert('products', $data);

    return $this->db->insert_id();
  }
  public function getProductDataBySlug($prd_slug)
  {
    $sql = "SELECT *, (select product_category.cat_name from product_category where product_category.cat_id=products.prd_cat_id) prd_cat_id_name
 -- (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=products.prd_quantity_unit and gen_prm.gnp_group='prd_quantity_unit') prd_quantity_name
  FROM `products` where prd_slug='" . $prd_slug . "'";
    $query = $this->db->query($sql);
    return $query->row();
  }
  
  public function getProductById($prd_id)
  {
    $sql = "SELECT * FROM `products` where prd_id='" . $prd_id . "'";
    $query = $this->db->query($sql);
    return $query->row();
  }
  
  public function updateProduct()
  {
    $data = array(

      'prd_name' => $this->input->post('prd_name'),
      'prd_size' => $this->input->post('prd_size'),
      // 'prd_short_desc' => $this->input->post('prd_short_desc'),
      // 'prd_flv_id' =>@join(',', $this->input->post('prd_flv_id')),
      'prd_cat_id' => $this->input->post('prd_cat_id'),
      'prd_price' => str_replace(',', '', $this->input->post('prd_price')),
      // 'prd_availability' => $this->input->post('prd_availability'),
      'prd_menu' => $this->input->post('prd_menu'),
      // 'prd_brand' =>$this->input->post('prd_brand'),
      // 'prd_quantity_unit' =>$this->input->post('prd_quantity_unit'),
      'prd_order' => $this->input->post('prd_order'),
      // 'prd_descripion' => $this->input->post('prd_descripion'),
      // 'prd_discount' =>$this->input->post('prd_discount'),
      // 'prd_flv_id' => @join(',', $this->input->post('prd_flv_id')),
      'prd_updt_by' => $this->session->userdata('tsn_usr_id'),

    );
    $this->db->where('prd_id', $this->input->post('prd_id'));
    $this->db->update('products', $data);
    return true;
  }

  public function deleteData($tablename, $where)
  {
    $query = $this->db->delete($tablename, $where);
    return $query;
  }

  public function deleteuser($id)
  {
    $this->load->db();
    $this->db->where('prd_id', $id);
    $this->db->delete(products);
    return true;
  }

  // public function getProductImagesBySlug($prd_slug)
  // {
  //  $sql="SELECT`img_id`, `img_type`, `img_type_id`, `img_title`, `img_path`, `img_name`, `img_alternate_text` from  images where img_type=".PRODUCT_IMG_TYPE." and img_type_id=(select prd_id from products where prd_slug='".$prd_slug."')";
  //  $query=$this->db->query($sql);
  //  return $query->result();
  // }
  public function deleteProductImage($value = '')
  {
    $sql = "delete  from  images  where  `img_id`=" . $this->input->post('img_id') . "";
    $this->db->query($sql);
    return true;
  }
  public function insertMakeFeatured()
  {
    $this->db->trans_start();
    $count = $this->input->post('count');
    $prd_id = $this->input->post('prd_id');
    $prd_id_del = implode(",", $this->input->post('prd_id'));
    $makeFeatures = '';

    $sql = "delete from  featured_products   ";
    $query = $this->db->query($sql);

    for ($i = 0; $i < $count; $i++) {

      $data1 = array(
        'fp_prd_id' => $prd_id[$i],
        'fp_status' => 1,
        'fp_crtd_dt' => date('Y-m-d H:i:s'),
        'fp_crtd_by' => $this->session->userdata('tsn_usr_id'),
      );
      $this->home_model->insert('featured_products', $data1);
    }

    $this->db->trans_complete();
    return true;
  }
  public function removeMakeFeatured()
  {
    $this->db->trans_start();
    $count = $this->input->post('count');
    $prd_id = implode(",", $this->input->post('prd_id'));


    $sql = "delete from featured_products where fp_prd_id IN (" . $prd_id . ") ";
    $query = $this->db->query($sql);

    $this->db->trans_complete();
    return true;
  }
  // public function getOrderProductsById($ord_id)
  // {
  //    $sql="SELECT `odp_id`, `odp_ord_id`,odp_quantity,odp_amt `odp_prd_id`,CONCAT((select products.prd_name from products where products.prd_id=person_order_products.odp_prd_id),' - ',(select products.prd_quantity from products where products.prd_id=person_order_products.odp_prd_id),' ',(select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value in (select products.prd_quantity_unit from products where products.prd_id=person_order_products.odp_prd_id)  and gen_prm.gnp_group='prd_quantity_unit')) as prdname_concat,
  //    (select products.prd_name from products where products.prd_id=person_order_products.odp_prd_id) prduct_name,
  //    (select products.prd_slug from products where products.prd_id=person_order_products.odp_prd_id) prduct_slug,
  //    (select products.prd_price from products where products.prd_id=person_order_products.odp_prd_id) prd_price,
  //    (select  REPLACE(img_path, 'products/big/', 'products/small/')  from images where   img_type=".PRODUCT_IMG_TYPE." and img_type_id=person_order_products.odp_prd_id  limit 1) as product_img, `odp_quantity`, `odp_amt`,odp_disc_type,odp_disc_amt,odp_total_amt,
  // (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order_products.odp_disc_type and gen_prm.gnp_group='".DISC_TYPE."') disc_type
  //  from person_order_products where odp_ord_id='".$ord_id."' and odp_status=".ACTIVE_STATUS."";
  //    $query=$this->db->query($sql);
  //    $result=$query->result();
  //    return $result;
  // }
  public function getOrderProductsById($ord_id)
  {
    $sql = "SELECT `odp_id`, `odp_ord_id`, `odp_prd_id`, `odp_flavour`, `odp_quantity`,`odp_height`,`odp_width`, `odp_amt`, `odp_disc_type`, `odp_disc_amt`, `odp_total_amt`, `odp_status`, `odp_date`, `odp_file`, `ord_shipping_charges`,
     (select products.prd_name from products where products.prd_id=person_order_products.odp_prd_id) prduct_name,
      (select products.prd_slug from products where products.prd_id=person_order_products.odp_prd_id) prduct_slug,
      (select gen_prm.gnp_name from  gen_prm where gen_prm.gnp_value=person_order_products.odp_status and gen_prm.gnp_group='order_prd_status') order_product_status,
      (SELECT ops_status from person_order_product_status WHERE odp_ord_id = person_order_products.odp_ord_id AND  ops_prd_id = person_order_products.odp_prd_id ORDER BY person_order_product_status.ops_id DESC LIMIT 1) ops_status,
      (SELECT ops_remark from person_order_product_status WHERE odp_ord_id = person_order_products.odp_ord_id AND  ops_prd_id = person_order_products.odp_prd_id ORDER BY person_order_product_status.ops_id DESC LIMIT 1) ops_remark
     FROM person_order_products  where odp_ord_id = " . $ord_id . "";
    $query = $this->db->query($sql);
    $result = $query->result();
    return $result;
  }

  public function getProdStatus()
  {
    $status = $this->input->post('status');
    $sql = "SELECT * FROM `gen_prm` WHERE `gnp_group`='order_prd_status' and `gnp_status`= " . ACTIVE_STATUS . "";
    $query = $this->db->query($sql);
    $str = '';
    $str = '';
    foreach ($query->result() as $key) {

      $selected = '';
      $disabled = '';
      if ($status) {
        if ($status == $key->gnp_value) {
          $selected = 'selected="selected"';
        }
        if ($key->gnp_value < $status) {
          $disabled = 'disabled';
        }
      }

      $str .= '<option value="' . $key->gnp_value . '"' . $selected . '>' . $key->gnp_name . '</option>';
    }
    return $str;
  }

  public function getAllFlavour()
  {
    $sql = "SELECT   `flv_id`, `flv_name`, `flv_order` FROM `product_flavours` order by flv_order";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function insertFlavour($value = '')
  {
    $data = array(
      'flv_slug' =>  url_title(word_limiter($this->input->post('flv_name'), SLUG_WORD_LIMIT), 'dash', TRUE),
      'flv_name' => $this->input->post('flv_name'),
      'flv_order' => $this->input->post('flv_order'),
      'flv_crtd_by' => $this->session->userdata('tsn_usr_id'),
      'flv_crtd_dt' => date('Y-m-d H:i:s'),
    );
    return $this->home_model->insert('product_flavours', $data);
  }

  public function updateFlavour($value = '')
  {
    $data = array(

      'flv_name' => $this->input->post('flv_name'),
      'flv_order' => $this->input->post('flv_order'),
      'flv_updt_by' => $this->session->userdata('tsn_usr_id'),

    );
    $this->home_model->update('flv_id', $this->input->post('flv_id'), $data, 'product_flavours');
    return true;
  }
  public function deletePortfolio($value = '')
  {
    try {
      $sql = 'DELETE FROM `product_flavours` WHERE flv_id=' . $this->input->post('flv_id') . '';
      $this->db->query($sql);
      return true;
    } catch (Exception $e) {
      log_message('PROJECTLOG', 'error occured while deleting portfolio>> portfolio_model/deletePortfolio ::ERROR-' . $e);
      return false;
    }
  }
  public function getProductFlavours()
  {
    $sql = "SELECT  *,(select prd_name from products where products.prd_id = product_flavor_images.pfi_prd_id) product_name,(select flv_name from product_flavours where product_flavours.flv_id = product_flavor_images.pfi_flv_id) flavour_name FROM `product_flavor_images` order by pfi_crtd_dt desc";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getProductFromFlavourImg()
  {
    $sql = "SELECT  prd_id,prd_name FROM `products` order by prd_crtd_date desc";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getPrdFlavours($flavours, $pfi_prd_id)
  {
    $sql = "SELECT  flv_id, flv_name FROM `product_flavours` where flv_id IN (" . $flavours . ") and flv_id not IN (SELECT pfi_flv_id  from product_flavor_images where pfi_prd_id = " . $pfi_prd_id . ") ";
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getAssignedPrdFlavours($pfi_prd_id)
  {
    $sql = "SELECT  prd_flv_id FROM `products` where  prd_id = " . $pfi_prd_id . "";
    $query = $this->db->query($sql);
    return $query->row();
  }
  public function insertPrdFlavourImg($value = '')
  {
    if (isset($_FILES["file"]["type"])) {
      $validextensions = array("jpeg", "JPEG", "jpg", "png", "JPG", "PNG");
      $temporary = explode(".", $_FILES["file"]["name"]);
      $file_extension = end($temporary);

      if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg")
          || ($_FILES["file"]["type"] == "image/JPG")
          || ($_FILES["file"]["type"] == "image/PNG")
          || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < IMAGE_SIZE) //Approx. 5mb files can be uploaded.
        && in_array($file_extension, $validextensions)
      ) {
        if ($_FILES["file"]["error"] > 0) {
          echo "size";
          return 'size';
          // echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
          if (file_exists(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"])) {
            $_FILES["file"]["name"] = getFileName($_FILES["file"]["name"]);
            if (file_exists(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"])) {
              $_FILES["file"]["name"] = getFileName($_FILES["file"]["name"]);
            } else {
              $_FILES["file"]["name"] = $_FILES["file"]["name"];
            }
          } else {
            $_FILES["file"]["name"] = $_FILES["file"]["name"];
          }

          $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable

          move_uploaded_file($sourcePath, PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]); // Moving Uploaded file


          copy(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"], PRODUCT_SMALL_IMAGE_PATH . $_FILES["file"]["name"]);
          copy(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"], PRODUCT_LARGE_IMAGE_PATH . $_FILES["file"]["name"]);
          $data = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]);
          $width = $data[0];
          $height = $data[1];
          $image_height = (PRODUCT_WIDTH_BIG * $height) / $width;
          //CONTROLLING IMAGE WIDTH Large

          $config['image_library'] = 'gd2';
          $config['source_image'] = PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]; //get original image
          //$config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;

          $config['width'] = PRODUCT_WIDTH_BIG;
          $config['height'] = $image_height;
          $this->image_lib->initialize($config);
          if (!$this->image_lib->resize()) {
            $this->handle_error($this->image_lib->display_errors());
          }
          //END OF CONTROLLING IMAGE WIDTH CODE

          //CONTROLLING IMAGE WIDTH SMall
          $data_small_img = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]);
          $width_small = $data_small_img[0];
          $height_small = $data_small_img[1];

          $image_height_small = (PRODUCT_WIDTH_SMALL * $height) / $width;
          $config['image_library'] = 'gd2';
          $config['source_image'] = PRODUCT_SMALL_IMAGE_PATH . $_FILES["file"]["name"]; //get original image
          //$config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;

          $config['width'] = PRODUCT_WIDTH_SMALL;
          $config['height'] = $image_height_small;
          $this->image_lib->initialize($config);
          if (!$this->image_lib->resize()) {
            $this->handle_error($this->image_lib->display_errors());
          }

          //CONTROLLING IMAGE WIDTH LARGE
          $data_large_img = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]);
          $width_large = $data_large_img[0];
          $height_large = $data_large_img[1];

          $image_height_large = (PRODUCT_WIDTH_LARGE * $height) / $width;
          $config['image_library'] = 'gd2';
          $config['source_image'] = PRODUCT_LARGE_IMAGE_PATH . $_FILES["file"]["name"]; //get original image
          //$config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;

          $config['width'] = PRODUCT_WIDTH_LARGE;
          $config['height'] = $image_height_large;
          $this->image_lib->initialize($config);
          if (!$this->image_lib->resize()) {
            $this->handle_error($this->image_lib->display_errors());
          }

          $data = array(
            'pfi_img' =>  $_FILES["file"]["name"],
            'pfi_prd_id' => $this->input->post('pfi_prd_id'),
            'pfi_flv_id' => $this->input->post('pfi_flv_id'),
            'pfi_crtd_by' => $this->session->userdata('tsn_usr_id'),
            'pfi_crtd_dt' => date('Y-m-d H:i:s'),
          );

          $this->db->insert('product_flavor_images', $data);

          return 'true';
        }
      } else {

        return 'error';
      }
    } else {
      return 'select_img';
    }
  }

  public function updatePrdFlavourImg($value = '')
  {
    if (isset($_FILES["file"]["type"])) {
      $validextensions = array("jpeg", "JPEG", "jpg", "png", "JPG", "PNG");
      $temporary = explode(".", $_FILES["file"]["name"]);
      $file_extension = end($temporary);

      if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg")
          || ($_FILES["file"]["type"] == "image/JPG")
          || ($_FILES["file"]["type"] == "image/PNG")
          || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < IMAGE_SIZE) //Approx. 5mb files can be uploaded.
        && in_array($file_extension, $validextensions)
      ) {
        if ($_FILES["file"]["error"] > 0) {
          return 'size';
          // echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
          if (file_exists(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"])) {
            $_FILES["file"]["name"] = getFileName($_FILES["file"]["name"]);
            if (file_exists(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"])) {
              $_FILES["file"]["name"] = getFileName($_FILES["file"]["name"]);
            } else {
              $_FILES["file"]["name"] = $_FILES["file"]["name"];
            }
          } else {
            $_FILES["file"]["name"] = $_FILES["file"]["name"];
          }

          $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable

          move_uploaded_file($sourcePath, PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]); // Moving Uploaded file

          copy(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"], PRODUCT_SMALL_IMAGE_PATH . $_FILES["file"]["name"]);
          copy(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"], PRODUCT_LARGE_IMAGE_PATH . $_FILES["file"]["name"]);
          $data = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]);
          $width = $data[0];
          $height = $data[1];
          $image_height = (PRODUCT_WIDTH_BIG * $height) / $width;
          //CONTROLLING IMAGE WIDTH Large

          $config['image_library'] = 'gd2';
          $config['source_image'] = PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]; //get original image
          //$config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;

          $config['width'] = PRODUCT_WIDTH_BIG;
          $config['height'] = $image_height;
          $this->image_lib->initialize($config);
          if (!$this->image_lib->resize()) {
            $this->handle_error($this->image_lib->display_errors());
          }
          //END OF CONTROLLING IMAGE WIDTH CODE

          //CONTROLLING IMAGE WIDTH SMall
          $data_small_img = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]);
          $width_small = $data_small_img[0];
          $height_small = $data_small_img[1];

          $image_height_small = (PRODUCT_WIDTH_SMALL * $height) / $width;
          $config['image_library'] = 'gd2';
          $config['source_image'] = PRODUCT_SMALL_IMAGE_PATH . $_FILES["file"]["name"]; //get original image
          //$config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;

          $config['width'] = PRODUCT_WIDTH_SMALL;
          $config['height'] = $image_height_small;
          $this->image_lib->initialize($config);
          if (!$this->image_lib->resize()) {
            $this->handle_error($this->image_lib->display_errors());
          }

          //CONTROLLING IMAGE WIDTH LARGE
          $data_large_img = getimagesize(PRODUCT_BIG_IMAGE_PATH . $_FILES["file"]["name"]);
          $width_large = $data_large_img[0];
          $height_large = $data_large_img[1];

          $image_height_large = (PRODUCT_WIDTH_LARGE * $height) / $width;
          $config['image_library'] = 'gd2';
          $config['source_image'] = PRODUCT_LARGE_IMAGE_PATH . $_FILES["file"]["name"]; //get original image
          //$config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;

          $config['width'] = PRODUCT_WIDTH_LARGE;
          $config['height'] = $image_height_large;
          $this->image_lib->initialize($config);
          if (!$this->image_lib->resize()) {
            $this->handle_error($this->image_lib->display_errors());
          }
          $data = array(
            'pfi_img' =>  $_FILES["file"]["name"],
            'pfi_updt_by' => $this->session->userdata('tsn_usr_id')
          );
          $this->db->where('pfi_id', $this->input->post('pfi_id'));
          $this->db->update('product_flavor_images', $data);
          unlink(PRODUCT_BIG_IMAGE_PATH . $this->input->post('pfi_old_img'));
          unlink(PRODUCT_SMALL_IMAGE_PATH . $this->input->post('pfi_old_img'));
          unlink(PRODUCT_LARGE_IMAGE_PATH . $this->input->post('pfi_old_img'));
          return 'true';
        }
      } else {
        return 'error';
      }
    } else {

      return 'select_img';
    }
  }

  public function getProductDataById($ord_id, $prd_id)
  {
    $sql = "SELECT *,    
    (SELECT prd_name from products where prd_id=odp_prd_id) prd_name,
    (select gen_prm.gnp_name from gen_prm where gen_prm. gnp_value=person_order_products.odp_status and gen_prm.gnp_group='order_prd_status') order_product_status_name,  
    (SELECT ops_status from person_order_product_status WHERE odp_ord_id = person_order_products.odp_ord_id AND ops_prd_id = person_order_products.odp_prd_id ORDER BY person_order_product_status.ops_id DESC LIMIT 1) order_product_status,
    (SELECT ops_remark from person_order_product_status WHERE odp_ord_id = person_order_products.odp_ord_id AND ops_prd_id = person_order_products.odp_prd_id ORDER BY person_order_product_status.ops_id DESC LIMIT 1) ops_remark
     FROM `person_order_products` where odp_ord_id = " . $ord_id . " and odp_prd_id=" . $prd_id . "";
    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }

  public function getSingleProductById($orderProductId)
  {
    $sql = "SELECT *,    
    (SELECT prd_name from products where prd_id=odp_prd_id) prd_name,    
    (select gen_prm.gnp_name from  gen_prm where gen_prm. gnp_value=person_order_products.odp_status and gen_prm.gnp_group='order_prd_status') order_product_status
    FROM `person_order_products` where odp_id = '" . $orderProductId . "'";

    $query = $this->db->query($sql);
    $row = $query->row();
    return $row;
  }

  public function getTotalShippingChargesByOrderId($ord_id)
  {
    $sql = "SELECT SUM(ord_shipping_charges) AS totalShippingCharges FROM `person_order_products` where odp_ord_id = '" . $ord_id . "'";
    $query = $this->db->query($sql);
    return $query->row() ? $query->row() : 0;
  }

  public function updateSingleProductShippingCharges($orderData, $orderProductData)
  {
    try {
      $product_shipping_charges = $this->input->post('product_shipping_charges');
      $odp_total_amt = floatval($orderProductData['odp_subtotal'] + $product_shipping_charges);
      $postOrderData = array(
        'ord_shipping_charges' => $product_shipping_charges,
        'odp_total_amt' => $odp_total_amt,
        'odp_updt_by' => $this->session->userdata('tsn_usr_id'),
        'ord_updt_dt' => date('Y-m-d H:i:s')
      );
      $this->db->where('odp_id', $orderProductData['odp_id']);
      $this->db->update('person_order_products', $postOrderData);

      /* Update Whole order's shipping charges- Start */
      $getTotalShippingChargesResult = $this->getTotalShippingChargesByOrderId($orderData['ord_id']);
      $totalShippingCharges = $getTotalShippingChargesResult->totalShippingCharges;

      $ord_total_amt = floatval($orderData['ord_sub_total'] + $totalShippingCharges);
      $postOrderData = array(
        'ord_shipping_charges' => $totalShippingCharges,
        'ord_total_amt' => $ord_total_amt,
        'ord_updt_by' => $this->session->userdata('tsn_usr_id'),
        'ord_updt_dt' => date('Y-m-d H:i:s')
      );
      $this->db->where('ord_id', $orderData['ord_id']);
      $this->db->update('person_order', $postOrderData);
      /* Update Whole order's shipping charges- End */

      return true;
    } catch (Exception $e) {
      log_message('NEXLOG', 'error occured while updating product order status >> product_model/updateSingleProductShippingCharges ::ERROR-' . $e);
      return false;
    }
  }
}
