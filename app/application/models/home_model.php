<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Eye View Design CMS module Ajax Model
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   EVD CMS
 * @author    Frederico Carvalho
 * @copyright 2008 Mentes 100Limites
 * @version   0.1
 */

class home_model extends CI_Model
{
	/**
	 * Instanciar o CI
	 */
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getCombo($sql, $value = false)
	{
		$query = $this->db->query($sql);
		$str = '';
		foreach ($query->result() as $row) {
			$selected = "";
			if ($value) {
				if ($value == $row->f1) {
					$selected = " selected='selected'";
				}
			}
			$str .= "<option value='" . $row->f1 . "'" . $selected . ">" . $row->f2 . "</option>";
		}
		return $str;
	}

	function getData($sql)
	{
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	function getadhocCombo($sql, $value = false)
	{
		$query = $this->db->query($sql);
		$str = '';
		foreach ($query->result() as $row) {
			$selected = "";
			if ($value) {
				if ($value == $row->f1) {
					$selected = " selected='selected'";
				}
			}
			$str .= "<option data-productid='" . $row->f1 . "' data-productname='" . $row->concat_string . "' data-productprice='" . $row->f3 . "' " . $selected . ">" . $row->concat_string . "</option>";
		}

		return $str;
	}

	function sendemail($email, $subject, $message)
	{
		//echo $message;
		$this->load->library('email');
		$this->email->from('studentinfo@rajasthani.org.in', 'Studentinfo-Rajasthani');
		$this->email->subject($subject);
		$this->email->to($email);
		$this->email->message($message);
		$this->email->send();
		return true;
	}

	function update($field, $id, $array, $table)
	{
		//echo $id;
		$this->db->where($field, $id);
		if ($this->db->update($table, $array) == TRUE) {
			return true;
		} else {
			return false;
		}
	}
	function insert($table, $array)
	{
		$this->db->insert($table, $array);
		$id = $this->db->insert_id();
		return $id;
	}

	public function viewallperson()
	{
		$sql = "select * from usr_people as usr_ppl left join usr_profession as usr_prof on usr_ppl.PPL_profession = usr_prof.pfs_id";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}
	public function getDataGpmData($group_name)
	{
		$sql = "select * from gen_prm where gnp_group='" . $group_name . "' and gnp_status=" . ACTIVE_STATUS . " order by gnp_order";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}

	public function getAllWalletHistory()
	{
		//  $sql="SELECT person_order.ord_date, payment_rcpt.upload_date, payment_rcpt.pay_type, payment_rcpt.pay_mode, payment_rcpt.pay_amt, person_order.ord_total_amt FROM person_order FULL OUTER JOIN payment_rcpt ON person_order.ord_prs_id = payment_rcpt.User_ID  ";
		// 		$sql="select ord_date as date, ord_total_amt, ord_prs_id as UserID from person_order 
		//  UNION ALL
		//  select upload_date as date, pay_type, pay_mode, pay_amt, User_ID as UserID from payment_rcpt order by date";
		// $sql = "SELECT `id`, `prs_id`, `wallet_id`, `odp_ord_id`, `type`, `transaction_mode`, `amount`, `closing_balance`, `description`, `created_on`, prs_email, prs_name
		// FROM `wallet_history` LEFT JOIN `person` ON wallet_history.prs_id = person.prs_id ORDER BY wallet_history.created_on DESC";
		$sql = "SELECT `id`, wallet_history.prs_id, `wallet_id`, `odp_ord_id`, `type`, `transaction_mode`, `amount`, `closing_balance`, `description`, `created_on`, person.prs_id AS personId, `prs_email`, `prs_name`
		FROM `wallet_history` LEFT JOIN `person` ON wallet_history.prs_id = person.prs_id ORDER BY wallet_history.created_on DESC";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}

	public function getReceipt()
	{
		$sql = "SELECT `ID`, `rcpt_number`, `pay_type`,`pay_mode`, `pay_amt`, `bank_name`, `rcpt_img`, `status`, `pay_remarks`, `upload_by`, `upload_date`, `update_on`, `prs_id`, `prs_email`, `prs_name`,
		(select gen_prm.gnp_name FROM `gen_prm` WHERE gen_prm.gnp_value=payment_rcpt.status AND gen_prm.gnp_group='payment_receipt_status') receipt_status
		FROM `payment_rcpt` LEFT JOIN `person` ON payment_rcpt.upload_by = person.prs_id ORDER BY payment_rcpt.update_on DESC";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}
	public function getReceiptById($receiptId)
	{
		$sql = "SELECT `ID`, `rcpt_number`, `pay_type`,`pay_mode`, `pay_amt`, `bank_name`, `rcpt_img`, `status` AS receipt_status,  `pay_remarks`, `upload_by`, `upload_date`, `update_on`, `prs_id`, `prs_email`, `prs_name`, `prs_mob`, `prs_whatsapp`
		FROM `payment_rcpt` LEFT JOIN `person` ON payment_rcpt.upload_by = person.prs_id WHERE payment_rcpt.ID = '" . $receiptId . "'";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}
	public function getAllPaytmTransactions()
	{
		$sql = "SELECT * FROM `paytm_transactions` LEFT JOIN `person` ON paytm_transactions.pyt_crtd_by = person.prs_id ORDER BY paytm_transactions.pyt_updt_dt DESC";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}

	public function getUserDetails()
	{
		$sql = "SELECT *,
		(SELECT stm_name from state_master where stm_id=prs_state) state_name
		FROM `person` LEFT JOIN `wallet` ON person.prs_id = wallet.prs_id";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}

	public function getStateDropdown()
	{
		$sqlQuery = "SELECT stm_id as f1,stm_name as f2 FROM state_master where stm_status='" . ACTIVE_STATUS . "' ORDER by f2 ASC";
		$query = $this->db->query($sqlQuery);
		$result = $query->result();
		return $result;
	}

	public function get_dropdown($gpm_group, $value = false)
	{

		$select = " gnp_value as f1,gnp_name as f2";
		$this->db->select($select);
		$this->db->where('gnp_group', $gpm_group);
		$this->db->where('gnp_status', ACTIVE_STATUS);
		$this->db->order_by('gnp_order', 'ASC');
		$query = $this->db->get('gen_prm');

		$str = '';
		foreach ($query->result() as $row) {
			//print_r($row);
			$selected = '';
			if ($value) {
				if ($value == $row->f1) {
					$selected = 'selected="selected"';
				}
			}

			$str .= '<option value="' . $row->f1 . '"' . $selected . '>' . $row->f2 . '</option>';
		}
		return $str;
	}
	public function get_dropdown1($gpm_group, $value = false)
	{

		$select = " gnp_value as f1,gnp_name as f2";
		$this->db->select($select);
		$this->db->where('gnp_group', $gpm_group);
		$this->db->where('gnp_status', ACTIVE);
		$this->db->order_by('gnp_order', 'ASC');
		$query = $this->db->get('gen_prm');

		$str = '';
		foreach ($query->result() as $row) {

			$str .= "<option value='" . $row->f1 . "'>" . $row->f2 . "</option>";
		}
		return $str;
	}

	public function getProductCategories($value = false)
	{
		$sql1 = "SELECT cat_id AS f1,cat_name AS f2 FROM `product_category` WHERE cat_status=" . ACTIVE_STATUS . " order by cat_order";
		$query = $this->db->query($sql1);
		$selected = '';
		$str = '';
		foreach ($query->result() as $row) {
			$selected = '';
			if ($value) {
				if ($value == $row->f1) {
					$selected = 'selected="selected"';
				}
			}
			$str .= "<option value='" . $row->f1 . "' " . $selected . ">" . $row->f2 . "</option>";
		}
		return $str;
	}
	public function getProductFlavours($value = false)
	{
		$sql = "SELECT   `flv_id`, `flv_name`, `flv_order` FROM `product_flavours` order by flv_order";
		$query = $this->db->query($sql);
		if ($value) {
			$selected_array = explode(',', $value);
		}
		$selected = '';
		$str = '';
		foreach ($query->result() as $row) {
			$selected = '';
			if ($value) {
				if (in_array($row->flv_id, $selected_array)) {
					$selected = 'selected="selected"';
				}
			}

			$str .= "<option value='" . $row->flv_id . "' " . $selected . ">" . $row->flv_name . "</option>";
		}
		return $str;
	}


	public function validateLocation($location)
	{
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($location) . '&sensor=false');
		$geo = json_decode($geo, true);
		if ($geo['status'] == 'OK') {
			$origLat = $geo['results'][0]['geometry']['location']['lat'];
			$origLon = $geo['results'][0]['geometry']['location']['lng'];

			$loc_id = $this->location($location, $origLat, $origLon);

			return $loc_id;
		} else {
			return '0';
		}
	}

	public function location($location, $origLat, $origLon)
	{
		$sql = "select  loc_id from location where  loc_lat='" . $origLat . "' and loc_long='" . $origLon . "' ";
		$query = $this->db->query($sql);
		$row = $query->row();

		if (!empty($row)) {
			return $row->loc_id;
		} else {
			$data1 = array(
				'loc_name' => $location,
				'loc_long' => $origLon,
				'loc_lat' => $origLat,
				'loc_crtd_dt' => date('Y-m-d H:i:s'),

			);
			$this->db->insert('location', $data1);
			return $this->db->insert_id();
		}
	}

	public function dateinmysql($datefromuser = '')
	{
		if (empty($datefromuser)) {
			return '0000-00-00';
		} else {
			$date = str_replace('/', '-', $datefromuser);
			return date('Y-m-d', strtotime($date));
		}
	}
	// public function dateinmysql1($datefromuser='')
	// {
	// 	if (empty($datefromuser)) {
	// 		return '0000-00-00';
	// 	}
	// 	else{
	// 		// $date = str_replace('/', '-', $datefromuser);
	// 		return date('Y-m-d', strtotime($date));
	// 	}

	// } 
	function generateRandomString($length)
	{
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
		$randomNo = '';
		for ($i = 0; $i < $length; $i++) {
			$randomNo .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomNo;
	}
	public function getCheckboxEdit($name, $gpm_group, $prp_id)
	{
		$sql = 'select gen_prm.gnp_name,gnp_value from gen_prm where gnp_value in (select pce_equipment_id from photographer_camera_equipments where  prt_property_id=' . $prp_id . ') and gen_prm.gpm_group="' . $gpm_group . '"';
		$query = $this->db->query($sql);

		$elements = array();
		foreach ($query->result() as $key) {
			// $string .= $key->LOC_id.',';
			$elements[] = $key->gnp_value;
		}
		$stringcat = implode(',', $elements);

		$group_array = explode(',', $stringcat);


		$select = "gnp_value as f1,gpm_id as id,gnp_name as f2";
		$this->db->select($select);
		$this->db->where('gpm_group', $gpm_group);
		$this->db->where('gpm_status', 1);
		$this->db->order_by('gpp_order', 'ASC');
		$query = $this->db->get('gen_prm');
		$str = '';
		foreach ($query->result() as $row) {
			if (in_array($row->f1, $group_array)) {


				$checked = 'checked';
			} else {
				$checked = '';
			}

			$str .= '<div style="margin: 4px 0" class="col-md-3 col-xs-6">
			<label>
				<input type="checkbox"  ' . $checked . ' name="' . $name . '[]" value=' . $row->f1 . ' id="' . $row->id . '"><span class="fa fa-check"></span>' . $row->f2 . '      
			</label>
		</div>';
		}
		return $str;
	}
	function generateRandomOtp($length)
	{
		$characters = '123456789';
		$randomNo = '';
		for ($i = 0; $i < $length; $i++) {
			$randomNo .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomNo;
	}
	public function getFileName($name)
	{

		$splitTimeStamp = explode(".", $name);
		$name = $splitTimeStamp[0];
		$ext = '.' . $splitTimeStamp[1];
		return  $name . '-' . $this->home_model->generateRandomStringNum(4) . $ext;
	}

	public function convertSmallFileName($name)
	{

		$splitTimeStamp = explode(".", $name);
		$name = $splitTimeStamp[0];
		$ext = '.' . $splitTimeStamp[1];
		return  $name . '-' . 'small' . $ext;
	}
	function generateRandomStringNum($length)
	{
		$randomNumber = '';
		for ($i = 0; $i < $length; $i++) {
			$randomNumber .= mt_rand(0, 9);
		}
		return $randomNumber;
	}
	public function getDropdownForBrand($type, $value = false)
	{

		$sql = "SELECT   brd_id as f1, brd_name as f2,brd_order from brands where  brd_status='" . ACTIVE_STATUS . "'  and brd_type='" . $type . "'
	union
	SELECT brd_id as f1, brd_name as f2,brd_order from brands where  brd_status='" . ACTIVE_STATUS . "' and brd_id='" . OTHER_BRAND . "'  order by brd_order";
		$query = $this->db->query($sql);


		$str = '';
		foreach ($query->result() as $row) {
			$selected = '';
			if ($value) {
				if ($value == $row->f1) {
					$selected = 'selected="selected"';
				}
			}

			$str .= '<option value="' . $row->f1 . '"' . $selected . '>' . $row->f2 . '</option>';
		}
		return $str;
	}
	public function getDropdownForCategory($pgr_id, $value = false)
	{

		$sql = "SELECT   pct_cat_id as f1, (select cat_name from category where category.cat_id=photographer_category.pct_cat_id) as f2 from photographer_category where  pct_pgr_id='" . $pgr_id . "'";

		$query = $this->db->query($sql);


		$str = '';
		foreach ($query->result() as $row) {
			$selected = '';
			if ($value) {
				if ($value == $row->f1) {
					$selected = 'selected="selected"';
				}
			}

			$str .= '<option value="' . $row->f1 . '"' . $selected . '>' . $row->f2 . '</option>';
		}
		return $str;
	}
	public function getDropdownForSubCategory($pgr_id, $sbc_cat_id, $value = false)
	{

		$sql = "select    sbc_id as f1, sbc_name as f2 from sub_categories where  sbc_cat_id=" . $sbc_cat_id . " and  sbc_status='" . ACTIVE_STATUS . "' order by     sbc_order";

		$query = $this->db->query($sql);


		$str = '';
		foreach ($query->result() as $row) {
			$selected = '';
			if ($value) {
				if ($value == $row->f1) {
					$selected = 'selected="selected"';
				}
			}

			$str .= '<option value="' . $row->f1 . '"' . $selected . '>' . $row->f2 . '</option>';
		}
		return $str;
	}
	public function getDropdownForModel($type, $value = false)
	{

		$sql = "SELECT mdl_id as f1, mdl_name as f2 from models where  mdl_status='" . ACTIVE_STATUS . "' and mdl_type='" . $type . "' order by mdl_order asc";
		$query = $this->db->query($sql);


		$str = '';
		foreach ($query->result() as $row) {
			$selected = '';
			if ($value) {
				if ($value == $row->f1) {
					$selected = 'selected="selected"';
				}
			}

			$str .= '<option value="' . $row->f1 . '"' . $selected . '>' . $row->f2 . '</option>';
		}
		return $str;
	}

	public function getCheckbox($name, $gpm_group, $checked_values = false)
	{
		$checked_data = explode(',', $checked_values);

		$select = "cat_id as f1,cat_name as f2";
		$this->db->select($select);
		$this->db->where('cat_status', ACTIVE);
		$this->db->order_by('cat_order', 'ASC');
		$query = $this->db->get('category');
		$str = '';
		foreach ($query->result() as $row) {
			$checked = '';

			if ($checked_values) {

				if (in_array($row->f1, $checked_data)) {


					$checked = 'checked';
				} else {
					$checked = '';
				}
			}
			$str .= '<div style="margin: 4px 0" class="col-md-6 col-xs-12"> <label class="mt-checkbox">
		<input type="checkbox"  ' . $checked . ' name="' . $name . '[]" value=' . $row->f1 . ' id="' . $row->f1 . '"><span></span>' . $row->f2 . '      
	</label></div>
	';
		}
		return $str;
	}
	public function getEquipmentsCheckbox($name, $equipment_array)
	{
		if (!empty($equipment_array)) {
			$elements = array();
			foreach ($equipment_array as $key) {
				$elements[] = $key->pge_equipment_id;
			}
			$stringcat = implode(',', $elements);
			$equipment_array_data = explode(',', $stringcat);
		}
		$select = "cme_id as f1,cme_name as f2";
		$this->db->select($select);
		$this->db->where('cme_status', ACTIVE);
		$this->db->order_by('cme_order', 'ASC');
		$query = $this->db->get('camera_equipments');
		$str = '';
		foreach ($query->result() as $row) {
			$checked = '';

			if (!empty($equipment_array)) {

				if (in_array($row->f1, $equipment_array_data)) {


					$checked = 'checked';
				} else {
					$checked = '';
				}
			}

			$str .= '<div style="margin: 4px 0" class="col-md-6 col-xs-12">  <label class="mt-checkbox">
		<input type="checkbox" onchange="return checkOther(' . $row->f1 . ')"  ' . $checked . ' name="' . $name . '[]" value=' . $row->f1 . ' id="inlineCheckbox' . $row->f1 . '">
		' . $row->f2 . '
		<span></span>      
	</label></div>
	';
		}
		return $str;
	}
	public function getBsnData($bpm_name)
	{
		$sql = "SELECT 	bpm_value from  bsn_prm where  bpm_name='" . $bpm_name . "'";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row->bpm_value;
	}

	function moneyFormatIndia($num)
	{
		$explrestunits = "";
		if (strlen($num) > 3) {
			$lastthree = substr($num, strlen($num) - 3, strlen($num));
			$restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);
			for ($i = 0; $i < sizeof($expunit); $i++) {
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0) {
					$explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				} else {
					$explrestunits .= $expunit[$i] . ",";
				}
			}
			$thecash = $explrestunits . $lastthree;
		} else {
			$thecash = $num;
		}
		return $thecash; // writes the final format where $currency is the currency symbol.
	}
	public function getTempImg($type, $timg_reference_no)
	{
		$sql = "SELECT * FROM `temp_images` WHERE timg_reference_no='" . $timg_reference_no . "' and 	timg_type='" . $type . "' ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	public function getTempDocuments($type, $tatc_reference_no)
	{
		$sql = "SELECT * FROM `temp_attachments` WHERE tatc_reference_no='" . $tatc_reference_no . "' and tatc_type='" . $type . "' ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	public function insertImages($images, $img_type_id, $timg_reference_no, $title)
	{
		foreach ($images as $key) {
			$data = array(
				'img_type' => $key->timg_type,
				'img_path' => $key->timg_path,
				'img_type_id' => $img_type_id,
				'img_name' => $key->timg_name,
				'img_title' => $title,
				'img_alternate_text' => $title,
				'img_crtd_date' => date('Y-m-d H:i:s'),
				'img_crtd_by' => $this->session->userdata('prs_id')


			);
			$this->db->insert('images', $data);
		}
		$sql = "delete  from temp_images where  `timg_reference_no`='" . $timg_reference_no . "'";
		$this->db->query($sql);
		return true;
	}
	public function insertDocument($attchments, $atc_type_id, $tatc_reference_no)
	{
		foreach ($attchments as $key) {
			$data = array(
				'atc_type' => $key->tatc_type,
				'atc_path' => $key->tatc_path,
				'atc_type_id' => $atc_type_id,
				'atc_name' => $key->tatc_name,
				'atc_crtd_dt' => date('Y-m-d H:i:s'),
				'atc_crtd_by' => $this->session->userdata('hnv_prs_id')


			);
			$this->db->insert('attachments', $data);
		}
		$sql = "delete  from temp_attachments where  `tatc_reference_no`='" . $tatc_reference_no . "'";
		$this->db->query($sql);
		return true;
	}
	public function datetimefordisplay($datefromuser)
	{
		if ($datefromuser == "0000-00-00 00:00:00") {
			return $datevalue = "";
		} elseif ($datefromuser == "") {
			return $datevalue = "";
		} else {

			$date = str_replace('-', '/', $datefromuser);
			$datevalue = date('d-M-Y h:i:A', strtotime($date));

			return $datevalue;
		}
	}
	public function validateReferenceNo($ord_reference_no)
	{
		$sql = "SELECT ord_id  FROM `person_order` where   ord_reference_no='" . $ord_reference_no . "'";
		$query = $this->db->query($sql);
		$row = $query->result();
		if (!empty($row)) {
			return false;
		} else {
			return true;
		}
	}

	/* WALLET- START */
	public function getWalletDataByPersonId($personId)
	{
		$sql = "SELECT wallet.id, wallet.prs_id, wallet.amount, wallet.status, wallet.active, wallet.created_on, wallet.updated_on, person.prs_id as person_id, `prs_email`, `prs_name`
		FROM `wallet` LEFT JOIN `person` ON wallet.prs_id = person.prs_id WHERE wallet.prs_id = '" . $personId . "'";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}
	/* WALLET- END */

	/* PERSON- START */
	public function getPersonDetailsById($personId)
	{
		$sql = "SELECT * FROM `person` WHERE person.prs_id = '" . $personId . "'";
		$q = $this->db->query($sql);
		$result = $q->row();
		return $result;
	}
	/* PERSON- END */

	/* CONTACT US DETAILS- START*/
	public function getContactUsDetails()
	{
		$sql = "SELECT * FROM `contact` ORDER BY CON_created_on DESC";
		$q = $this->db->query($sql);
		$result = $q->result();
		return $result;
	}
	/* CONTACT US DETAILS- END */

	/* Quotation Methods- Start */
	public function getAllQuotations()
	{
		$sql = "SELECT *,
		(SELECT `prs_email` FROM `person` WHERE `prs_id`= quotation_request.qr_prs_id) person_email
		FROM `quotation_request` ORDER BY `qr_updated_on` DESC";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	public function getQuotationDetailById($quotationId)
	{
		$sql = "SELECT *,
		(SELECT `prs_email` FROM `person` WHERE `prs_id`= quotation_request.qr_prs_id) person_email
		FROM `quotation_request` WHERE `qr_id`= '" . $quotationId . "' ORDER BY `qr_updated_on` DESC";
		$query = $this->db->query($sql);
		$result = $query->row();
		return $result;
	}
	/* Quotation Methods- End */

	/* Complaints Methods- Start */
	public function getAllCompaints()
	{
		$sql = "SELECT *,			
	  		(SELECT `cm_desc` FROM `complains_master` WHERE `cm_id`= complains.com_desc_id) com_desc_name,
	  		(SELECT `gnp_name` FROM `gen_prm` WHERE `gnp_value`= complains.com_status AND `gnp_status`=" . ACTIVE_STATUS . " AND `gnp_group`='complains_status') com_status_name
	  		FROM `complains`
			LEFT JOIN `person` ON complains.com_prs_id = person.prs_id
			ORDER BY `com_updated_on` DESC";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	/* Complaints Methods- End */

	/* Dashboard Methods- Start */
	public function totalNumberOfOrders()
	{
		$sql = "SELECT * FROM `person_order` WHERE `ord_is_success` = 1";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	public function totalNumberOfPaymentReceipt()
	{
		$sql = "SELECT * FROM `payment_rcpt`";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	public function totalNumberOfQuotations()
	{
		$sql = "SELECT * FROM `quotation_request`";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	public function totalNumberOfInquiry()
	{
		$sql = "SELECT * FROM `contact`";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	/* Dashboard Methods- End */
}
