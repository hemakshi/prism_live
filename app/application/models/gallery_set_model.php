<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class gallery_set_model extends CI_Model
{
	/**
	* Instanciar o CI
	*/
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

		public function getGalleryForList($resType,$dataOptn='')
		{
			$sqlQuery = "Select *,(DATE_FORMAT(gls_crdt_dt, '%d/%m/%Y')) gls_crdt_date
									from gallery_set where gls_status = '".ACTIVE_STATUS."'";
     $query = $this->db->query($sqlQuery);
     $result = $query->result();
			return $result;
		}

	public function gls_form_update()
	{

      $gls_id = $this->input->post('gls_id');
      $gls_old_img = $this->input->post('gls_old_img');
      $gallery_set = array();
      $gallery_set['gls_name']     = $this->input->post('gls_name');
      $gallery_set['gls_order_by'] = $this->input->post('gls_order_by');
      $gallery_set['gls_type'] = $this->input->post('gls_type');
      $gallery_set['gls_link'] = $this->input->post('gls_link');
      $gallery_set['gls_crdt_by'] = $this->session->userdata('hnv_prs_id') ? $this->session->userdata('hnv_prs_id') : 1;
      $gallery_set['gls_crdt_dt'] = date('Y-m-d H:i:s');
      $gallery_set['gls_updt_by'] = $this->session->userdata('hnv_prs_id') ? $this->session->userdata('hnv_prs_id') : 1;
      $gallery_set['gls_updt_dt'] = date('Y-m-d H:i:s');
      
      if(isset($_FILES['gls_image']['type']))
      {
         $gallery_set['gls_image']    = doc_upload(GALLERY_SET_IMAGE,GALLERY_SET_IMAGE_RESIZE,'gls_image');
      }
      if($gls_old_img != '')
      {
        UnlinkFile(GALLERY_SET_IMAGE.$gls_old_img);
        UnlinkFile(GALLERY_SET_IMAGE_RESIZE.$gls_old_img);
      }
      if($gls_id == '')
      {
         $gallery_setId = $this->home_model->insert('gallery_set', $gallery_set);
      }
      else
      {
        $gallery_setId =  $this->home_model->update('gls_id',$gls_id,$gallery_set,'gallery_set');;
      }
         return true;
    
	}
  public function deleteGlsImage()
 {
  try 
  {
    $sql='DELETE FROM `gallery_set` WHERE gls_id='.$this->input->post('gls_id').'';
    $this->db->query($sql);
    return true;    
  }
  catch (Exception $e)
  {
    log_message('PROJECTLOG','error occured while deleting portfolio>> portfolio_model/deletePortfolio ::ERROR-'.$e);
    return false;       
  }
}
}
