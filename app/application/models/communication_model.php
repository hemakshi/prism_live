<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class communication_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function communication($event_name, $receiver_data, $mail_data)
	{
		require_once(APPPATH . "/config/mail_template.php");
		log_message('error', '>> communication');
		$event_id =  $this->getEventID($event_name);
		if (!empty($event_id)) {
			$event_usr =  $this->getEventUsr($event_id->ent_id);
			foreach ($event_usr as $key) {
				if ($key->eus_type == EUS_TYPE_EMAIL) {

					$email_notifiy = $this->CheckSendNotify('email');
					if (!empty($email_notifiy)) {
						if ($key->eus_usr_type == EUS_USR_TYPE_ADMIN) {
							$email_id = ADMIN_EMAIL_VALUE;

							log_message('error', '>> communication admin email id = ' . $email_id);
						} else {
							$email_id = $receiver_data['email'];

							log_message('error', '>> communication user email id = ' . $email_id);
						}
						$sql = "SELECT tmp_name,tmp_sub,tmp_msg FROM `com_template` WHERE `tmp_id`=" . $key->eus_tmp_id . "";
						$query = $this->db->query($sql);
						$data =  $query->result();
						$rowtemp = $query->row();
						$msg = $this->getMailMsg($rowtemp->tmp_name, $receiver_data, $mail_data);

						$this->sendMail($receiver_data['email'], $msg['subject'], $msg['body']);
					}
				} else if ($key->eus_type == EUS_TYPE_SMS) {
					$sms_notifiy = $this->CheckSendNotify('sms');
					if (!empty($sms_notifiy)) {
						log_message('error', 'inside sms');
						if ($key->eus_usr_type == EUS_USR_TYPE_ADMIN) {
							$mobile_no = ADMIN_SMS_VALUE;
							log_message('error', '  admin mobile_no = ' . $mobile_no);
						} else {
							$mobile_no = $receiver_data['mobile'];
							log_message('error', ' user mobile_no = ' . $mobile_no);
						}
						$sql = "SELECT tmp_name,tmp_sub,tmp_msg FROM `com_template` WHERE `tmp_id`=" . $key->eus_tmp_id . "";
						$query = $this->db->query($sql);
						$data =  $query->result();
						$rowtemp = $query->row();
						if ($mobile_no != '') {
							$message = $this->getMsgData($rowtemp->tmp_name, $receiver_data, $mail_data);
							log_message('error', ' mobile_no :' . $mobile_no . 'message = ' . $message);
							$message = str_replace(' ', '%20', $message);
							$this->SendMessage($message, $mobile_no);
						}
					}
				}
			}
		}
	}

	function getEventID($event_name)
	{
		$sql = "SELECT ent_id FROM `com_events` WHERE `ent_name`='" . $event_name . "' and `ent_status`=" . ACTIVE_STATUS . "";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row;
	}

	function getEventUsr($ent_id)
	{
		$sql = "SELECT eus_id,eus_ent_id,eus_usr_type,eus_tmp_id,
		(Select com_template.tmp_name from com_template where com_template.tmp_id =com_events_usr.eus_tmp_id
	) template_name, eus_type FROM `com_events_usr` WHERE `eus_ent_id`=" . $ent_id . " and eus_status=" . ACTIVE_STATUS . " order by eus_order_by";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getAdminEmail()
	{
		$sql = "SELECT usr_email from user where usr_dpt_id='" . ADMIN_DEPARTMENT . "'";
		$query = $this->db->query($sql);
		$row = $query->row();

		return $row->prs_email;
	}
	function getMailMsg($name, $receiver_data, $mail_data)
	{
		require_once(APPPATH . "/config/mail_template.php");
		$subject = '';
		$body = '';
		$smsMsg = '';

		/* PAYMENT RECEIPT STATUS CHANGED- START */
		if ($name == 'payment_receipt_status_changed_user_mail' && $mail_data['receipt_status'] == PAYMENT_RECEIPT_STATUS_APPROVE) {
			$subject = CUSTOMER_RECEIPT_APPROVE_SUBJECT;
			$subject = str_replace('%RECEIPT_NUMBER%', $mail_data['rcpt_number'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $subject);

			$body = CUSTOMER_RECEIPT_APPROVE_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%RECEIPT_NUMBER%', $mail_data['rcpt_number'], $body);
			$body = str_replace('%REMARKS%', $mail_data["pay_remarks"] ? $mail_data["pay_remarks"] : '-', $body);
			$body = str_replace('%ADDED_BALANCE%', $mail_data["added_balance"], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);
			$body = str_replace('%TOTAL_BALANCE%', $mail_data['amount'], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);

			$smsMsg = CLIENT_RECEIPT_APPROVED_MSG;
			$smsMsg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $smsMsg);
			$smsMsg = str_replace('%RECEIPT_NUMBER%', $mail_data['rcpt_number'], $smsMsg);
			$smsMsg = str_replace('%ADDED_BALANCE%',  $mail_data["added_balance"], $smsMsg);
			$smsMsg = str_replace('%TOTAL_BALANCE%', $mail_data['amount'], $smsMsg);
		}
		if ($name == 'payment_receipt_status_changed_user_mail' && $mail_data['receipt_status'] == PAYMENT_RECEIPT_STATUS_REJECT) {
			$subject = CUSTOMER_RECEIPT_REJECT_SUBJECT;
			$subject = str_replace('%RECEIPT_NUMBER%', $mail_data['rcpt_number'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $subject);

			$body = CUSTOMER_RECEIPT_REJECT_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%RECEIPT_NUMBER%', $mail_data['rcpt_number'], $body);
			$body = str_replace('%REMARKS%', $mail_data['pay_remarks'], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);
		}
		/* PAYMENT RECEIPT STATUS CHANGED- END */

		/* ORDER STATUS CHANGED- START */
		if ($name == 'order_status_changed_user_mail' && $mail_data['ord_status'] == ORDER_STATUS_CANCELLED) {
			$subject = CUSTOMER_ORDER_CANCELLED_SUBJECT;
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $subject);

			$body = CUSTOMER_ORDER_CANCELLED_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%REMARKS%', $mail_data['ord_remark'], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);
		}
		if ($name == 'order_status_changed_user_mail' && $mail_data['ord_status'] == ORDER_STATUS_DISPATCHED) {
			$subject = CUSTOMER_ORDER_DISPATCHED_SUBJECT;
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $subject);

			$body = CUSTOMER_ORDER_DISPATCHED_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%REMARKS%', $mail_data['ord_remark'], $body);
			$body = str_replace('%AMOUNT%', $mail_data['ord_total_amt'], $body);
			$body = str_replace('%CUSTOMER_NAME%', $mail_data['pad_name'], $body);
			$body = str_replace('%ADDRESS%', $mail_data['pad_address'], $body);
			$body = str_replace('%LOCALITY%', $mail_data['pad_locality'], $body);
			$body = str_replace('%CITY%', $mail_data['pad_city'], $body);
			$body = str_replace('%PINCODE%', $mail_data['pad_pincode'], $body);
			$body = str_replace('%MOBILE_NUMBER%', $mail_data['pad_mobile'], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);

			$smsMsg = CLIENT_ORDER_OUT_FOR_DELIVERY_MSG;
			$smsMsg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $smsMsg);
			$smsMsg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $smsMsg);
			$smsMsg = str_replace('%ADMIN_REMARKS%', $mail_data['ord_remark'], $smsMsg);
			$smsMsg = str_replace('%DELIVERY_DATE%',  date("d-M-y", strtotime(date('Y-m-d'))), $smsMsg);
		}
		if ($name == 'order_status_changed_user_mail' && $mail_data['ord_status'] == ORDER_STATUS_DELIVERED) {
			$subject = CUSTOMER_ORDER_DELIVERED_SUBJECT;
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $subject);

			$body = CUSTOMER_ORDER_DELIVERED_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%DELIVERY_DATE%', date("M d, Y", strtotime(date('Y-m-d'))), $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);
		}
		/* ORDER STATUS CHANGED- END */

		/* PRODUCT STATUS CHANGED- START */
		if ($name == 'product_status_changed_user_mail' && $mail_data['order_product_status'] == PRODUCT_STATUS_DISPATCHED) {
			$subject = CUSTOMER_PRODUCT_OUT_FOR_DELIVERY_SUBJECT;
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$subject = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $subject);

			$body = CUSTOMER_PRODUCT_OUT_FOR_DELIVERY_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%REMARKS%', $mail_data['ops_remark'], $body);
			$body = str_replace('%AMOUNT%', $mail_data['odp_total_amt'], $body);
			$body = str_replace('%CUSTOMER_NAME%', $mail_data['pad_name'], $body);
			$body = str_replace('%ADDRESS%', $mail_data['pad_address'], $body);
			$body = str_replace('%LOCALITY%', $mail_data['pad_locality'], $body);
			$body = str_replace('%CITY%', $mail_data['pad_city'], $body);
			$body = str_replace('%PINCODE%', $mail_data['pad_pincode'], $body);
			$body = str_replace('%MOBILE_NUMBER%', $mail_data['pad_mobile'], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);

			$smsMsg = CLIENT_PRODUCT_OUT_FOR_DELIVERY_MSG;
			$smsMsg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $smsMsg);
			$smsMsg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $smsMsg);
			$smsMsg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $smsMsg);
			$smsMsg = str_replace('%ADMIN_REMARKS%', $mail_data['ops_remark'], $smsMsg);
			$smsMsg = str_replace('%DELIVERY_DATE%', date("d-M-y", strtotime(date('Y-m-d'))), $smsMsg);

			$msg = array("subject" => $subject, "body" => $body, 'smsMsg' => $smsMsg);
			return $msg;
		}
		if ($name == 'product_status_changed_user_mail' && $mail_data['order_product_status'] == PRODUCT_STATUS_DELIVERED) {
			$subject = CUSTOMER_PRODUCT_DELIVERED_SUBJECT;
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$subject = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $subject);

			$body = CUSTOMER_PRODUCT_DELIVERED_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%DELIVERY_DATE%', date("M d, Y", strtotime(date('Y-m-d'))), $body);
			$body = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $body);
		}
		if ($name == 'product_status_changed_user_mail') {
			$subject = CUSTOMER_PRODUCT_STATUS_CHANGE_SUBJECT;
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$subject = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $subject);
			$subject = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $subject);

			$body = CUSTOMER_PRODUCT_STATUS_CHANGE_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $body);
			$body = str_replace('%PRODUCT_STATUS%', $mail_data['order_product_status_name'], $body);
			$body = str_replace('%REMARKS%', $mail_data['ops_remark'], $body);
			$body = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $body);
		}

		if ($name == 'product_status_changed_admin_mail') {
			$subject = ADMIN_PRODUCT_STATUS_CHANGED_SUBJECT;
			$subject = str_replace('%STATUS_NAME%', $mail_data['order_product_status'], $subject);
			$subject = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $subject);
			$subject = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $subject);
			$body = ADMIN_PRODUCT_STATUS_CHANGED_BODY;
			$body = str_replace('%STATUS_NAME%', $mail_data['order_product_status'], $body);
			$body = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $body);
			$body = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $body);
			$body = str_replace('%ORDER_LINK%', base_url() . 'orders/' . $mail_data['ord_reference_no'], $body);
		}
		/* PRODUCT STATUS CHANGED- END */

		if ($name == 'admin_reset_password') {
			$subject = ADMIN_PASSWORD_RESET_SUBJECT;

			$body = ADMIN_PASSWORD_RESET_BODY;
			$body = str_replace('%WEBSITE_LOGO_PATH%', ADMIN_WEBSITE_ROOT_PATH . ADMIN_WEBSITE_LOGO_PATH, $body);
			$body = str_replace('%PASSWORD_RESET_LINK%', $mail_data['link'], $body);
			$body = str_replace('%WEBSITE_NAME%', PROJECT_NAME, $body);
		}

		$msg = array("subject" => $subject, "body" => $body, 'smsMsg' => $smsMsg);
		return $msg;
	}
	public function sendMail($email_id, $email_sub, $email_msg)
	{
		$email_notifiy = $this->CheckSendNotify('email');
		if (!empty($email_notifiy)) {
			if (SEND_MAIL_USING_SMTP) {

				log_message('error', '>> sendMail email id = ' . $email_id);
				log_message('error', '>> sendMail sub = ' . $email_sub);
				log_message('error', '>> sendMail message = ' . $email_msg);
				$list = '' . $email_id . '';
				$email_msg;

				$email_msg;

				$config = array(
					'protocol' => EMAIL_PROTOCOL,
					'smtp_host' => EMAIL_HOST,
					'smtp_port' => EMAIL_PORT,
					'smtp_user' => EMAIL_USERNAME_ADMIN,
					'smtp_pass' => EMAIL_PASSWORD,
					'mailtype'  => EMAIL_TYPE,
					'charset'   => EMAIL_CHARSET,
					'wordwrap' => EMAIL_WORDWRAP
				);

				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from(EMAIL_USERNAME_ADMIN, PROJECT_NAME);
				$this->email->subject($email_sub);
				$this->email->Bcc($list);
				$this->email->message($email_msg);

				// TEMP::
				if ($this->email->send()) {
					return true;
				} else {
					log_message('error', 'error occured in people insert communication_model/send_mail' . $this->email->print_debugger());
					show_error($this->email->print_debugger());
					return false;
				}
			} else {
				$to = $email_id;
				$subject = $email_sub;
				$message = $email_msg;

				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				// More headers
				$headers .= 'From: Prism Prints <' . EMAIL_USERNAME_ADMIN . '>' . "\r\n";

				if (mail($to, $subject, $message, $headers)) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}
	function getAdminMobile()
	{
		$sql = "SELECT prs_mob from person  where prs_dpt_id='" . ADMIN_DEPARTMENT . "'";
		$query = $this->db->query($sql);
		$row = $query->row();

		return $row->prs_mob;
	}
	function getMsgData($name, $receiver_data, $mail_data)
	{
		require_once(APPPATH . "/config/mail_template.php");
		$msg = '';
		// if ($name == 'product_status_changed_user_msg' && $mail_data['odp_status'] == PRODUCT_OUT_FOR_DELIVERY) {
		// 	$msg = CLIENT_PRODUCT_OUT_FOR_DELIVERY_MSG;
		// 	$msg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $msg);
		// 	$msg = str_replace('%DELIVERY_DATE%', date("M d, Y", strtotime(date('Y-m-d'))), $msg);
		// 	$msg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $msg);
		// 	$msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $msg);
		// }
		// if ($name == 'product_status_changed_admin_msg' && $mail_data['odp_status'] == PRODUCT_OUT_FOR_DELIVERY) {
		// 	$msg = ADMIN_PRODUCT_OUT_FOR_DELIVERY_MSG;
		// 	$msg = str_replace('%DELIVERY_DATE%', '30 Dec', $msg);
		// 	$msg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $msg);
		// 	$msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $msg);
		// }
		// if ($name == 'product_status_changed_user_msg' && $mail_data['odp_status'] == PRODUCT_DELIVERED) {
		// 	$msg = CLIENT_PRODUCT_DELIVERED_MSG;
		// 	$msg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $msg);
		// 	$msg = str_replace('%WEBSITE_NAME%', WEBSITE_NAME, $msg);
		// 	$msg = str_replace('%DELIVERY_DATE%', date("M d, Y", strtotime(date('Y-m-d'))), $msg);
		// 	$msg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $msg);
		// 	$msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $msg);
		// }
		// if ($name == 'product_status_changed_admin_msg' && $mail_data['odp_status'] == PRODUCT_DELIVERED) {
		// 	$msg = ADMIN_PRODUCT_DELIVERED_MSG;
		// 	$msg = str_replace('%PRODUCT_NAME%', $mail_data['prd_name'], $msg);
		// 	$msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $msg);
		// }

		if ($name == 'shipping_charges_deducted_user_msg') {
			$msg = CLIENT_SHIPPING_CHARGES_DEDUCTED_MSG;
			$msg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $msg);
			$msg = str_replace('%CHARGES_AMOUNT%', $mail_data['charges_amount'], $msg);
			$msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $msg);
			$msg = str_replace('%AVIALABLE_BALANCE%', $mail_data['avialable_amount'], $msg);
		}
		if ($name == 'offline_order_out_for_delivery_user_msg') {
			$msg = CLIENT_OFFLINE_ORDER_OUT_FOR_DELIVERY_MSG;
			$msg = str_replace('%RECEIVER_NAME%', $receiver_data['name'], $msg);
			$msg = str_replace('%ORDER_NUMBER%', $mail_data['ord_reference_no'], $msg);
			$msg = str_replace('%DELIVERY_DATE%',  date("d-M-y", strtotime(date('Y-m-d'))), $msg);
		}

		return $msg;
	}
	public function SendMessage($message, $mobile_no)
	{
		$sms_notifiy = $this->CheckSendNotify('sms');
		if (!empty($sms_notifiy)) {
			log_message('error', '>>mobile no = ' . $mobile_no);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,  "http://api.msg91.com/api/sendhttp.php?");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "?country=91&sender=MSGIND&route=4&mobiles=$mobile_no&authkey=243235A5ebDlzp5bc72911&message=$message");
			$buffer = curl_exec($ch);
			// print_r( $buffer);
		}
		return true;
	}
	function CheckSendNotify($bpm_name)
	{
		$sql = "SELECT * FROM `bsn_prm` WHERE `bpm_name`='" . $bpm_name . "' and `bpm_status`=" . ACTIVE_STATUS . "";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row;
	}
}
