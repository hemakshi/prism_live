<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class shop_model extends CI_Model 
{

  function __construct()
    {
      parent::__construct();
    }

 
 public function getAllBrands($status=false)
    {
     $sql="SELECT   `brd_id`, `brd_name`, `brd_order`, brd_status,  
      (select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=shop_brands.brd_status and gnp_group='status') as brd_status_name FROM `shop_brands`";
      if($status) {
        $sql.=" where brd_status=".$status." ";
      }
      $sql.="order by brd_order";
      $query = $this->db->query($sql);
     return $query->result();

   }
  public function insertBrand($value='')
   {
     $data = array(  
      'brd_slug' =>  url_title(word_limiter($this->input->post('brd_name'),SLUG_WORD_LIMIT), 'dash', TRUE),
                      'brd_name'=>$this->input->post('brd_name'),
                      'brd_order'=>$this->input->post('brd_order'),
                      'brd_status'=>$this->input->post('brd_status'),
                     'brd_crtd_by' =>$this->session->userdata('tsn_usr_id'),
                      'brd_crtd_dt'=>date('Y-m-d H:i:s'),
      );
   return $this->home_model->insert('shop_brands', $data);

   
   }
  
            public function updateBrand($value='')
            {
               $data = array(
     
       'brd_name' => $this->input->post('brd_name'),
       'brd_order' => $this->input->post('brd_order'),
        'brd_status'=>$this->input->post('brd_status'),
       'brd_updt_by' =>$this->session->userdata('tsn_usr_id'),
      
       );
 $this->home_model->update('brd_id', $this->input->post('brd_id'), $data, 'shop_brands');
 return true;
            }
           
}
