<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class user_model extends CI_Model
{
    /**
     * Instanciar o CI
     */
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function insert_user()
    {
        $pwd = openssl_encrypt($this->input->post('prs_password'), CIPHER, KEY);
        $data = array(
            'usr_dpt_id'      => $this->input->post('prs_department'),
            'usr_fullname'    => $this->input->post('prs_name'),
            'usr_username'    => $this->input->post('prs_user_name'),
            'usr_email'      => $this->input->post('prs_email'),
            'usr_mobile'      => $this->input->post('prs_mob'),
            'usr_password'    => $pwd,
            'usr_status'      => USER_ACTIVE_STATUS,
            'usr_created_by' => $this->session->userdata('tsn_usr_id') ? $this->session->userdata('tsn_usr_id') : 1,
            'usr_update_by'  => $this->session->userdata('tsn_usr_id') ? $this->session->userdata('tsn_usr_id') : 1,
            'usr_created_on' => date('Y-m-d H:i:s'),
            'usr_update_on'  => date('Y-m-d H:i:s')
        );
        $prs_id = $this->home_model->insert('user', $data);
        return $prs_id;
    }

    public function updateUserDetails()
    {
        log_message('error', ">> updateUserDetails");
        $data1 = array(
            'usr_fullname' => $this->input->post('prs_name'),
            'usr_mobile'   => $this->input->post('prs_mob'),
            'usr_email'   => $this->input->post('prs_email'),
            'usr_dpt_id'   => $this->input->post('prs_department'),
        );
        $result =  $this->home_model->update('usr_id', $this->input->post('prs_slug'), $data1, 'user');
        log_message('error', "<< updateUserDetails" . $result);
        return $result;
    }

    public function getUserData($prs_slug)
    {
        $sql = "SELECT *,(select location.loc_name from location where location.loc_id =person.prs_location) location_name from person where prs_slug ='" . $prs_slug . "' ";

        $query = $this->db->query($sql);
        $result = $query->row();
        log_message('nexlog', 'list  query = ' . $sql);
        return $result;
    }

    public function updateUserData($image, $loc_id)
    {
        //$pwd=openssl_encrypt($this->input->post('prs_password'),CIPHER,KEY);
        log_message('error', ">>Update User data - IMAGE:" . $image);
        $string = '';

        if (rtrim($image, ' ') != '') {
            log_message('error', ">>Update User data - 1");
            $data1 = array(
                'prs_name' => $this->input->post('prs_name'),
                'prs_mob' => $this->input->post('prs_mob'),
                'prs_email' => $this->input->post('prs_email'),
                'prs_dpt_id' => $this->input->post('prs_department'),
                'prs_location' => $loc_id,
                'prs_address' => $this->input->post('prs_address'),
                'prs_zone' => $this->input->post('prs_zone'),
                'prs_designation' => $this->input->post('prs_designation'),
                'prs_reporting_to' => $this->input->post('prs_reporting_to'),
                'prs_img' => $image
            );
            $string =  $this->home_model->update('prs_slug', $this->input->post('prs_slug'), $data1, 'person');
        } else {
            log_message('error', ">>Update User data - 2");
            $data1 = array(
                'prs_name' => $this->input->post('prs_name'),
                'prs_mob' => $this->input->post('prs_mob'),
                'prs_email' => $this->input->post('prs_email'),
                'prs_dpt_id' => $this->input->post('prs_department'),
                'prs_location' => $loc_id,
                'prs_address' => $this->input->post('prs_address'),
                'prs_zone' => $this->input->post('prs_zone'),
                'prs_designation' => $this->input->post('prs_designation'),
                'prs_reporting_to' => $this->input->post('prs_reporting_to')
            );
            $string =  $this->home_model->update('prs_slug', $this->input->post('prs_slug'), $data1, 'person');
        }

        log_message('error', "<<Update User data" . $string);
        return $string;
    }

    public function insertUserData($usr_prs_id)
    {


        $data = array(
            'usr_prs_id' => $usr_prs_id,
            'usr_status' => USER_ACTIVE_STATUS
        );

        $usr_id =  $this->home_model->insert('user', $data);
        return $usr_id;
    }
    public function ImageValidation($image_path, $image_name, $file_id)
    {
        log_message('error', '>> Validate image model');



        $validextensions = array("jpeg", "jpg", "png", "JPG", "PNG");
        $temporary = explode(".", $_FILES[$file_id]["name"]);
        $file_extension = end($temporary);
        $pic_name = $this->home_model->generateRandomString(10);
        $ext = $file_extension;
        $_FILES[$file_id]["name"] =  $pic_name . '.' . $ext;
        if ((($_FILES[$file_id]["type"] == "image/png") || ($_FILES[$file_id]["type"] == "image/jpg") || ($_FILES[$file_id]["type"] == "image/PNG") || ($_FILES[$file_id]["type"] == "image/jpeg")) && ($_FILES[$file_id]["size"] < 5120000) //Approx. 5MB files can be uploaded.
            && in_array($file_extension, $validextensions)
        ) {
            if ($_FILES[$file_id]["error"] > 0) {


                return "size";
            } else {
                if (file_exists($image_path . $_FILES[$file_id]["name"])) {
                    return 'error';
                } else {
                    $sourcePath = $_FILES[$file_id]['tmp_name']; // Storing source path of the file in a variable
                    $targetPath = $image_path . $_FILES[$file_id]['name']; // Target path where file is to be stored


                    $sourcePath1 = $_FILES[$file_id]['tmp_name'];
                    $targetPath1 = $image_path . $_FILES[$file_id]['name'];

                    move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file

                    //     if($image_name!='')
                    // {
                    //      unlink($image_path.$image_name);
                    //     // unlink( .$prs_img);
                    // }
                    // unlink( $person_path.$this->input->post('cus_old_pic'));


                    // if($resize != '')
                    // {
                    //               $config['image_library'] = 'gd2';
                    //                  $config['source_image'] =  .$_FILES['file']['name']; //get original image
                    //                //$config['create_thumb'] = TRUE;
                    //                  $config['maintain_ratio'] = TRUE;
                    //                  $config['width'] = prs_width;
                    //                  $config['height'] = prs_height;
                    //                 $this->image_lib->initialize($config);
                    //                  if (!$this->image_lib->resize()) {
                    //                      $this->handle_error($this->image_lib->display_errors());
                    //                  }
                    //                     copy($person_path.$_FILES['file']['name'],  $image_path.$_FILES['file']['name']);
                    // }




                    $prs_img = $_FILES[$file_id]["name"];

                    return $prs_img;
                }
            }
        } else {
            return "size";
        }
    }
    public function check_validation($type, $value, $prs_slug = '')
    {
        log_message('error', '>>User Model: check_validation - Person Slug : ' . $prs_slug);

        $sql = "SELECT COUNT(*) as count FROM `user` where usr_status='" . USER_ACTIVE_STATUS . "' and " . $type . "='" . $value . "' ";
        log_message('error', '>>User Model: check_validation - SQL1 : ' . $sql);

        if ($prs_slug != ' ') {
            $sql .= "  AND usr_id !='" . $prs_slug . "' ";
        }

        log_message('error', '>>User Model: check_validation - SQL2 : ' . $sql);

        $query = $this->db->query($sql);
        $result = $query->row();
        log_message('error', '<<User Model: check_validation2 - Result retrieved - Count: ' . $result->count);

        return $result->count;
    }
    public function getUserList()
    {
        $sql = "SELECT *,
        (SELECT department.dpt_id FROM `department` WHERE department.dpt_id = user.usr_dpt_id) dpt_id,
        (SELECT department.dpt_name FROM `department` WHERE department.dpt_id = user.usr_dpt_id) dpt_name
        FROM `user` ORDER BY `usr_update_on`";

        $query = $this->db->query($sql);
        $result = $query->result();
        log_message('error', 'list  query = ' . $sql);
        return $result;
    }
    public function getPersonNameById($prs_id)
    {
        $sql = "SELECT * from person where prs_id ='" . $prs_id . "' ";
        $query = $this->db->query($sql);
        $row = $query->row();
        //log_message('error', 'list  query = '.$sql);
        return $row;
    }
    public function getMultipleUsersInfo($prs_id)
    {

        $sql = "SELECT prs_email,prs_id,prs_name,prs_slug from person where prs_id in (" . $prs_id . ")
      ";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getUserDataBySlug($prs_slug)
    {
        $sql = "SELECT *,
            (SELECT department.dpt_id FROM `department` WHERE department.dpt_id = user.usr_dpt_id) dpt_id,
            (SELECT department.dpt_name FROM `department` WHERE department.dpt_id = user.usr_dpt_id) dpt_name                           
         from user where usr_id ='" . $prs_slug . "' ";

        $query = $this->db->query($sql);
        $result = $query->row();
        //log_message('nexlog', 'user data  query = '.$sql);
        return $result;
    }
    public function check_password($password, $prs_slug)
    {
        log_message('error', '>>User Model: check_password - Person Slug : ' . $prs_slug);
        $encrypted_password = openssl_encrypt($password, CIPHER, KEY);
        $sql1 = "SELECT count(*) count FROM person where prs_status='" . USER_ACTIVE_STATUS . "' and prs_password = '" . $encrypted_password . "' AND prs_slug ='" . $prs_slug . "' ";
        log_message('error', '>>User Model: check_password - SQL2 : ' . $sql1);

        $query = $this->db->query($sql1);
        $result = $query->row();

        return $result->count;
    }

    public function checkPersonPassword($data)
    {
        $encrypted_password = openssl_encrypt($data['old_password'], CIPHER, KEY);
        $sqlQuery = "SELECT COUNT(*) as count,usr_id FROM `user` WHERE usr_status='" . ACTIVE_STATUS . "' AND `usr_password`='" . $encrypted_password . "' ";
        if ($data['usr_id'] != '') {
            $sqlQuery .= " AND usr_id ='" . $data['usr_id'] . "'";
        }
        log_message('error', ' sqlQuery : ' . $sqlQuery);
        $query = $this->db->query($sqlQuery);
        $user_data = $query->row();
        log_message('error', ' row : ' . json_encode($user_data));
        log_message('error', 'user_model::checkPersonPassword');
        return $user_data;
    }

    public function changePasssword()
    {
        $encrypted_password = openssl_encrypt($this->input->post('password'), CIPHER, KEY);
        $data = array(
            'prs_password' => $encrypted_password,
            'prs_updt_by' => $this->session->userdata('hnv_prs_id')
        );
        log_message('nexlog', '>>changePasssword');
        $string =  $this->home_model->update('prs_slug', $this->input->post('prs_slug'), $data, 'person');
        log_message('nexlog', '<<changePasssword');
        return true;
    }

    public function changeUserPassword()
    {
        $encrypted_password = openssl_encrypt($this->input->post('password'), CIPHER, KEY);
        $data = array(
            'usr_password'   => $encrypted_password,
            'usr_update_by' => $this->session->userdata('tsn_usr_id'),
            'usr_update_on' => date('Y-m-d H:i:s')
        );
        log_message('nexlog', '>>changeUserPassword');
        $this->home_model->update('usr_id', $this->input->post('user_id'), $data, 'user');
        return true;
    }
    public function getPersonDetailsByMobNo($prs_mob)
    {
        $sql = "SELECT * from person where prs_mob ='" . $prs_mob . "' ";
        $query = $this->db->query($sql);
        $row = $query->row();
        //log_message('error', 'list  query = '.$sql);
        return $row;
    }
}
