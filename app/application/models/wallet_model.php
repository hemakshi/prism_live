<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class wallet_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /* START- PAYMENT RECEIPT */
    public function getPaymentReceiptStatus()
    {
        try {
            $status = $this->input->post('status');
            $sql = "SELECT * FROM `gen_prm` WHERE `gnp_group`='payment_receipt_status' and `gnp_status`= " . ACTIVE_STATUS . "";
            $query = $this->db->query($sql);
            $str = '';
            $str = '';
            foreach ($query->result() as $key) {
                $selected = '';
                $disabled = '';
                if ($status) {
                    if ($status == $key->gnp_value) {
                        $selected = 'selected="selected"';
                    }
                    if ($key->gnp_value < $status) {
                        $disabled = 'disabled';
                    }
                }
                $str .= '<option value="' . $key->gnp_value . '"' . $selected . '>' . $key->gnp_name . '</option>';
            }
            return $str;
        } catch (Exception $e) {
            log_message('NEXLOG', 'error occured while getting payment receipt status >> wallet_model/getPaymentReceiptStatus ::ERROR-' . $e);
            return false;
        }
    }

    public function updatePaymentReceiptStatus($value = '')
    {
        try {
            $data = array(
                'status'  => $this->input->post('receipt_status'),
                'pay_remarks' => $this->input->post('receipt_remark'),
                'updated_by' => $this->session->userdata('tsn_usr_id'),
                'update_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('ID', $this->input->post('payment_receipt_id'));
            $this->db->update('payment_rcpt', $data);
            return true;
        } catch (Exception $e) {
            log_message('NEXLOG', 'error occured while updating payment receipt status >> wallet_model/updatePaymentReceiptStatus ::ERROR-' . $e);
            return false;
        }
    }

    /* END- PAYMENT RECEIPT */

    public function addWalletBalance($singleReceiptData, $walletData)
    {
        try {
            // $deductAmount = round(floatval($singleReceiptData['pay_amt'] * 3 / 100), 2);
            // $amountAfterDeduct = round($singleReceiptData['pay_amt'] - $deductAmount, 2);
            // $totalAmount = round(floatval($walletData['amount'] + $amountAfterDeduct), 2);            
            $addAmount = round($singleReceiptData['pay_amt'], 2);
            $totalAmount = round(floatval($walletData['amount'] + $addAmount), 2);
            $data = array(
                'amount'  => $totalAmount,
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('prs_id', $singleReceiptData['upload_by']);
            $isBalanceAdded = $this->db->update('wallet', $data);
            $transactionMode = 0;
            switch ($singleReceiptData['pay_type']) {
                case 'Bank':
                    $transactionMode = TRANSACTION_MODE_BANK;
                    break;
                case 'Cash':
                    $transactionMode = TRANSACTION_MODE_CASH;
                    break;
                case 'Pay From Balance':
                    $transactionMode = TRANSACTION_MODE_PAY_FROM_BALANCE;
                    break;
                case 'Wallet':
                    $transactionMode = TRANSACTION_MODE_WALLET;
                    break;
                default:
                    $transactionMode = 0;
                    break;
            }
            if ($isBalanceAdded) {
                $walletHistoryData = array(
                    'prs_id' => $singleReceiptData['upload_by'],
                    'wallet_id' => $walletData['id'],
                    'type' => TRANSACTION_TYPE_CREDIT,
                    'transaction_mode' =>  $transactionMode,
                    'amount' => $addAmount,
                    'closing_balance' => $totalAmount,
                    'description' => "Balance Added of Receipt No." . $singleReceiptData['rcpt_number'],
                    'created_on' => date('Y-m-d H:i:s')
                );

                $this->db->insert('wallet_history', $walletHistoryData);
            }
            return true;
        } catch (Exception $e) {
            log_message('NEXLOG', 'error occured while adding wallet balance >> wallet_model/addWalletBalance ::ERROR-' . $e);
            return false;
        }
    }

    public function deductChargesFromWalletBalance($shippingCharges, $walletData, $orderData)
    {
        try {
            $deductAmount = round(floatval($shippingCharges), 2);
            $totalAmount = round(floatval($walletData['amount'] - $deductAmount), 2);
            $data = array(
                'amount'  => $totalAmount,
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('prs_id', $walletData['prs_id']);
            $isBalanceAdded = $this->db->update('wallet', $data);

            if ($isBalanceAdded) {
                $walletHistoryData = array(
                    'prs_id' => $walletData['prs_id'],
                    'wallet_id' => $walletData['id'],
                    'type' => TRANSACTION_TYPE_DEBIT,
                    'transaction_mode' =>  TRANSACTION_MODE_PAY_FROM_BALANCE,
                    'amount' => $deductAmount,
                    'closing_balance' => $totalAmount,
                    'description' => "Shipping Charges of Order#" . $orderData['ord_reference_no'],
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->db->insert('wallet_history', $walletHistoryData);
            }
            return true;
        } catch (Exception $e) {
            log_message('NEXLOG', 'error occured while deduct shippinig charges from wallet >> wallet_model/deductChargesFromWalletBalance ::ERROR-' . $e);
            return false;
        }
    }

    public function refundWalletBalanceOfCancelOrder($orderData, $walletData)
    {
        try {
            $refundAmount = $orderData['ord_total_amt'];
            $totalAmount = round(floatval($walletData['amount'] + $refundAmount), 2);
            $data = array(
                'amount'  => $totalAmount,
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('prs_id', $orderData['ord_prs_id']);
            $isBalanceAdded = $this->db->update('wallet', $data);
            if ($isBalanceAdded) {
                $walletHistoryData = array(
                    'prs_id' => $orderData['ord_prs_id'],
                    'wallet_id' => $walletData['id'],
                    'type' => TRANSACTION_TYPE_CREDIT,
                    'transaction_mode' =>  TRANSACTION_MODE_PAY_FROM_BALANCE,
                    'amount' => $refundAmount,
                    'closing_balance' => $totalAmount,
                    'description' => "Refund Balance of Order#" . $orderData['ord_reference_no'],
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->db->insert('wallet_history', $walletHistoryData);
            }
            return true;
        } catch (Exception $e) {
            log_message('NEXLOG', 'error occured while adding wallet balance >> wallet_model/refundWalletBalanceOfCancelOrder ::ERROR-' . $e);
            return false;
        }
    }
}
