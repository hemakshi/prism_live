<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Eye View Design CMS module Ajax Model
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   EVD CMS
 * @author    Frederico Carvalho
 * @copyright 2008 Mentes 100Limites
 * @version   0.1
 */

class offline_model extends CI_Model
{
	/**
	 * Instanciar o CI
	 */
	function __construct()
	{
		parent::__construct();
	}

	public function getProductsByCategoryId($postData)
	{
		$response = array();

		// Select record
		$this->db->select('prd_id,prd_name');
		$this->db->where('prd_cat_id', $postData['prd_cat_id']);
		$this->db->where('prd_status', ACTIVE_STATUS);
		$q = $this->db->get('products');

		$response = $q->result_array();
		return $response;
	}

	public function getProductById($postData)
	{
		$this->db->select('prd_id,prd_name,prd_price');
		$this->db->where('prd_id', $postData['prd_id']);
		$this->db->where('prd_status', ACTIVE_STATUS);
		$q = $this->db->get('products');

		$response = $q->result();
		return $response;
	}

	function generateOfflineOrderId($offlineUserId)
	{
		$isUnique = false;
		$order_reference_no = '';

		while ($isUnique != true) {
			$orderId = mt_rand(0, 1000000) . "-" . mt_rand(0, 1000000);
			$OrdReferenceId = $offlineUserId . "-" . $orderId;
			$this->db->where('offline_order_reference_no', $OrdReferenceId);
			$q = $this->db->get('offline_order');

			if ($q->num_rows() > 0) {
				continue;
			} else {
				$order_reference_no = $orderId;
				break;
			}
		}
		return $order_reference_no;
	}

	public function insertOrderItem($postData)
	{
		$data = array(
			'name'     => $this->input->post('name'),
			'meaning'  => $this->input->post('meaning'),
			'gender'   => $this->input->post('gender'),
			'religion' => $this->input->post('religion')
		);
		$this->db->insert('offline_order_product', $data);
		return $this->db->insert_id();
	}

	public function createOfflineOrder($ord_reference_no, $postData)
	{
		$admin_id = $this->session->userdata('tsn_usr_id');
		$offlineUserId = $this->session->userdata('session_offline_userid');
		$cart_total = sprintf("%.2f", $this->session->userdata('carttotal'));
		$shipping_charges = sprintf("%.2f", $this->session->userdata('shipping_charge'));
		$order_total = sprintf("%.2f", $this->session->userdata('grandtotal'));
		$paymentStatus = $postData['pending_amount'] == 0 ? ORDER_PAYMENT_STATUS_PAID : ORDER_PAYMENT_STATUS_PENDING;

		$data = array(
			'offline_order_reference_no'   	 => $ord_reference_no,
			'offline_order_user_id'        	 => $offlineUserId,
			'offline_order_subtotal'       	 => $cart_total,
			'offline_order_discount'       	 => 0,
			'offline_order_shipping_charges' => $shipping_charges,
			'offline_order_total'          	 => $order_total,
			'offline_order_payment_status' 	 => $paymentStatus,
			'offline_order_payment_mode'   	 => OFFLINE_ORD_PAYMENT_MODE_CASH,
			'offline_order_paid_amt'       	 => $postData['pay_amount'],
			'offline_order_pending_amt'    	 => $postData['pending_amount'],
			'offline_order_status' 		   	 => OFFLINE_ORD_STATUS_PLACED,
			'offline_order_is_success' 	   	 => OFFLINE_ORD_IS_SUCCESS_PROCESSING,
			'offline_order_created_by'     	 => $admin_id,
			'offline_order_updated_by'     	 => $admin_id,
			'offline_order_created_on'     	 => date('Y-m-d H:i:s'),
			'offline_order_updated_on'     	 => date('Y-m-d H:i:s')
		);
		$this->db->insert('offline_order', $data);
		return $this->db->insert_id();
	}

	public function getOfflineOrderDetailsById($ord_id)
	{
		$this->db->where('offline_order_id', $ord_id);
		$q = $this->db->get('offline_order');

		$response = $q->row();
		return json_encode($response);
	}
	public function getOfflineOrderFullDetailsById($ord_id)
	{
		$sql = "SELECT *,
   			(select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=offline_order.offline_order_status and gen_prm.gnp_group='order_status') ord_status_name,
   			(select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=offline_order.offline_order_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name
   			from offline_order left join offline_users on offline_users.offline_user_id=offline_order.offline_order_user_id  where offline_order_id='" . $ord_id . "'";
		$query = $this->db->query($sql);
		return $query->row() ? $query->row() : false;
	}

	public function insertOfflineOrderProducts($ord_id, $filesArray)
	{
		$ord_sts_id = 0;
		if (!empty($ord_id)) {
			$orderCreaterId = $this->session->userdata('tsn_usr_id');

			foreach ($this->cart->contents() as $items) {
				$rowid = $items['rowid'];

				$offline_ord_prd_data = array(
					'oop_order_id' => $ord_id,
					'oop_product_id' => $items['id'],
					'oop_designfile' => $filesArray ? $filesArray[$rowid] : '',
					'oop_quantity' => $items['qty'],
					'oop_height' => $items['prd_height'] == '' ? 0 : $items['prd_height'],
					'oop_width' => $items['prd_width']  == '' ? 0 : $items['prd_width'],
					'oop_price' => $items['price'],
					'oop_total' => $items['prd_subtotal'],
					'oop_status' => OFFLINE_ORD_PRD_STATUS_PLACED,
					'oop_remarks' => $items['remarks'],
					'oop_created_by' => $orderCreaterId,
					'oop_updated_by' => $orderCreaterId,
					'oop_created_on' => date('Y-m-d H:i:s'),
					'oop_updated_on' => date('Y-m-d H:i:s'),
				);
				$ord_sts_id = $this->db->insert('offline_order_product', $offline_ord_prd_data);

				if ($ord_sts_id) {
					$updateOrderStatus = "UPDATE `offline_order` SET `offline_order_is_success` = " . OFFLINE_ORD_IS_SUCCESS_SUCCESS . ", `offline_order_updated_on`= '" . date('Y-m-d H:i:s') . "' WHERE `offline_order_id` =" . $ord_id;
					$updateOrderStatusResult = $this->db->query($updateOrderStatus);

					// Clear Cart Data
					$this->load->library('cart');
					$this->cart->destroy();

					// Clear Order Session Data
					$this->load->library('session');
					$this->session->unset_userdata('session_offline_userid');
					$this->session->unset_userdata('carttotal');
					$this->session->unset_userdata('shipping_charge');
					$this->session->unset_userdata('grandtotal');
				}
			}
		}
		return $ord_sts_id;
	}

	/* Order List- Start */
	public function getAllOrders($status = false)
	{
		$sql = "SELECT *,
			(select gen_prm.gnp_name from gen_prm where gen_prm. gnp_value= offline_order_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
			(select gen_prm.gnp_name from gen_prm where gen_prm. gnp_value= offline_order_status and gen_prm.gnp_group='order_status') ord_status_name
			from offline_order left join offline_users on offline_users.offline_user_id=offline_order.offline_order_user_id";
		if ($status) {
			$sql .= " WHERE offline_order_status=" . $status . " AND offline_order_is_success != " . OFFLINE_ORD_IS_SUCCESS_PROCESSING . " ";
		}
		if (!$status) {
			$sql .= " WHERE offline_order_is_success != " . OFFLINE_ORD_IS_SUCCESS_PROCESSING . "";
		}
		$sql .= " ORDER BY offline_order_updated_on DESC";

		$query = $this->db->query($sql);
		return $query->result();
	}
	public function updateOrderDetail($postData)
	{
		$postData['add_amount'] = $postData['add_amount'] != '' ? intval($postData['add_amount']) : 0;
		$postData['pending_amount'] = $postData['pending_amount'] != '' ? intval($postData['pending_amount']) : 0;

		$paidAmt = floatval($postData['paid_amount'] + $postData['add_amount']);
		$pendingAmt = floatval($postData['pending_amount'] - $postData['add_amount']);
		$paymentStatus = $pendingAmt == 0 ? ORDER_PAYMENT_STATUS_PAID : ORDER_PAYMENT_STATUS_PENDING;

		$orderData = array(
			'offline_order_paid_amt' => $paidAmt,
			'offline_order_pending_amt' => $pendingAmt,
			'offline_order_status' => $postData['ord_status'],
			'offline_order_remarks' => $postData['ops_remark'],
			'offline_order_payment_status' => $paymentStatus,
			'offline_order_updated_on' => date('Y-m-d H:i:s')
		);
		return $this->home_model->update('offline_order_id', $postData['offline_order_id'], $orderData, 'offline_order');
	}
	public function getOfflineOrderByRefrence($ord_reference_no)
	{
		$sql = "SELECT *,
			(SELECT stm_name from state_master where stm_id=offline_user_state) state_name,
   			(select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=offline_order.offline_order_payment_mode and gen_prm.gnp_group='payment_mode') ord_payment_mode_name,
   			(select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=offline_order.offline_order_status and gen_prm.gnp_group='order_status') ord_status
   			from offline_order left join offline_users on offline_users.offline_user_id=offline_order.offline_order_user_id  where offline_order_reference_no='" . $ord_reference_no . "' and offline_order_status!= " . ORDER_FAIL . "";
		$query = $this->db->query($sql);
		return $query->row() ? $query->row() : false;
	}
	public function getOfflineOrderProductsById($ord_id)
	{
		$sql = "SELECT *,
			(select products.prd_name from products where products.prd_id=offline_order_product.oop_product_id) prduct_name,
			(select products.prd_slug from products where products.prd_id=offline_order_product.oop_product_id) prduct_slug,
			(select gen_prm.gnp_name from  gen_prm where gen_prm.gnp_value=offline_order_product.oop_status and gen_prm.gnp_group='order_prd_status') order_product_status
			FROM `offline_order_product` where oop_order_id = " . $ord_id . "";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	/* Order List- End */

	/* Offline Customer- Start */
	public function getOfflineUserById($postData)
	{
		$this->db->select('offline_user_id,offline_user_username');
		$this->db->where('offline_user_id', $postData['offline_user_id']);
		$this->db->where('offline_user_status', ACTIVE_STATUS);
		$q = $this->db->get('offline_users');

		$response = $q->result();
		return $response;
	}

	public function getOfflineUserAllDetailsById($userId)
	{
		$sql = "SELECT *,
			(SELECT stm_name from state_master where stm_id=offline_user_state) state_name,
     		(select gen_prm.gnp_name from gen_prm where gen_prm.gnp_value=offline_users.offline_user_gender and gen_prm.gnp_group='gender') gender
     		FROM offline_users where offline_user_id = " . $userId . "";
		$q = $this->db->query($sql);

		$response = $q->row();
		return $response;
	}

	public function getAllOfflineUser($value = false)
	{
		$this->db->select('offline_user_id,offline_user_username', 'offline_user_fullname');
		$this->db->where('offline_user_status', ACTIVE_STATUS);
		$this->db->order_by('offline_user_username', 'asc');
		$q = $this->db->get('offline_users');
		$result = $q->result();

		$selected = '';
		$response = '';
		foreach ($result as $row) {
			$selected = '';
			if ($value) {
				if ($value == $row->offline_user_id) {
					$selected = 'selected="selected"';
				}
			}
			$response .= "<option value='" . $row->offline_user_id . "' " . $selected . " >" . $row->offline_user_username . "</option>";
		}
		return $response;
	}

	public function getOfflineCustomersList()
	{
		$sql = "SELECT * FROM `offline_users` ORDER BY `offline_user_updated_on` DESC";

		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	public function updateOfflineCustomerDetail()
	{
		log_message('error', ">> updateOfflineCustomerDetail");
		$data1 = array(
			'offline_user_fullname' => $this->input->post('prs_name'),
			'offline_user_mobile'   => $this->input->post('prs_mob'),
			'offline_user_email'   => $this->input->post('prs_email'),
			'offline_user_gst'   => $this->input->post('prs_gst'),
			'offline_user_address_line'   => $this->input->post('prs_address_line'),
			'offline_user_city'   => $this->input->post('prs_city'),
			'offline_user_pincode'   => $this->input->post('prs_pincode'),
			'offline_user_district'   => $this->input->post('prs_district'),
			'offline_user_state'   => $this->input->post('prs_state'),
			'offline_user_status'   => $this->input->post('prs_status'),
			'offline_user_updated_by'   => $this->session->userdata('tsn_usr_id'),
			'offline_user_updated_on'   => date('Y-m-d H:i:s'),
		);
		$result =  $this->home_model->update('offline_user_id', $this->input->post('prs_slug'), $data1, 'offline_users');
		log_message('error', "<< updateOfflineCustomerDetail" . $result);
		return $result;
	}

	public function insertOfflineCustomer($postData)
	{
		$data = array(
			'offline_user_fullname'     => $postData['customer_name'],
			'offline_user_username'    	=> $postData['customer_username'],
			'offline_user_email'    	=> $postData['customer_email'],
			'offline_user_mobile'      	=> $postData['customer_mobile'],
			'offline_user_gst'      	=> $postData['customer_gst'],
			'offline_user_address_line' => $postData['customer_address_line'],
			'offline_user_city'      	=> $postData['customer_city'],
			'offline_user_pincode'      => $postData['customer_pincode'],
			'offline_user_district'     => $postData['customer_district'],
			'offline_user_state'      	=> $postData['customer_state'],
			'offline_user_status'      	=> USER_ACTIVE_STATUS,
			'offline_user_created_by' 	=> $this->session->userdata('tsn_usr_id') ? $this->session->userdata('tsn_usr_id') : 1,
			'offline_user_updated_by'  	=> $this->session->userdata('tsn_usr_id') ? $this->session->userdata('tsn_usr_id') : 1,
			'offline_user_created_on' 	=> date('Y-m-d H:i:s'),
			'offline_user_updated_on'  	=> date('Y-m-d H:i:s')
		);
		$prs_id = $this->home_model->insert('offline_users', $data);
		return $prs_id;
	}
	/* Offline Customer- End */

	public function checkValidationForOfflineUser($type, $value, $prs_slug = '')
	{
		$sql = "SELECT COUNT(*) AS count FROM `offline_users` WHERE " . $type . "='" . $value . "' ";
		if ($prs_slug != ' ') {
			$sql .= "  AND offline_user_id !='" . $prs_slug . "' ";
		}

		$query = $this->db->query($sql);
		$result = $query->row();
		return $result->count;
	}
}
