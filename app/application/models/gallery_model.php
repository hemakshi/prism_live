<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class gallery_model extends CI_Model 
{
	/**
	* Instanciar o CI
	*/
	function __construct()
	{
        // Call the Model constructor
		parent::__construct();
	}
	public function getGalleryList()
	{
		$sql    = "SELECT * FROM gallery_images ORDER BY gal_id ASC";
		$query  = $this->db->query($sql);
		$result = $query->result();
		return $result; 
	}
	public function delete_Image($gal_id)
  	{    
  		$sql    = "SELECT * FROM gallery_images WHERE gal_id= '".$gal_id."'";
		$query  = $this->db->query($sql);
		$row = $query->row();		
		$path=GALLERY_IMAGES_PATH.$row->gal_name;//1500x
		unlink($path);
		$path_small=GALLERY_SMALL_IMAGES_PATH.$row->gal_small_img_name;//small img
		unlink($path_small);
		$path_og=PATH_TO_STORE_FILE_RESIZE.$row->gal_name;//Resize Image
		unlink($path_og);		

 		$sql= "delete from  gallery_images  where gal_id='".$gal_id."'";
	    $this->db->query($sql);
	    return true;
  	}
	
}