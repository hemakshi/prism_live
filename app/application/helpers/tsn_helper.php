<?php
    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}

 function getCategoryDropdown($value=false)
{
    $CI = &get_instance();
                $sql = "SELECT  cat_id as f1, cat_name as f2  FROM   category where cat_status='".ACTIVE_STATUS."' ";
                $query=$CI->db->query($sql);

                $str='';
                foreach ($query->result() as $key) {
                  
                    $selected='';
                    if($value)
                    {
                        if($value==$key->f1)
                        {
                            $selected='selected="selected"';
                        }
                    }

                    $str .='<option value="'.$key->f1.'"'.$selected.'  data-price="'.$key->ptp_price.'">'.$key->f2.'</option>';
                }
                return $str;
}
function getDropdown($gpm_group,$value=false)
    {

        $CI = &get_instance();
        $sql = "SELECT  gnp_value as f1,gnp_name as f2 FROM gen_prm where gnp_group ='".$gpm_group."' and gnp_status='".ACTIVE_STATUS."' order by gnp_order";
        $query=$CI->db->query($sql);

        $str='';
        foreach ($query->result() as $key) {

            $selected='';
            if($value)
            {
                if($value==$key->f1)
                {
                    $selected='selected="selected"';
                }
            }

            $str .='<option value="'.$key->f1.'"'.$selected.'>'.$key->f2.'</option>';
        }
        return $str;

    }

    function getCatDropdown($value=false)
    {

        $CI = &get_instance();
        $sql = "SELECT  cat_id as f1,cat_name as f2 FROM category where cat_status='".ACTIVE_STATUS."' order by cat_order";
        $query=$CI->db->query($sql);

        $str='';
        foreach ($query->result() as $key) {

            $selected='';
            if($value)
            {
                if($value==$key->f1)
                {
                    $selected='selected="selected"';
                }
            }

            $str .='<option value="'.$key->f1.'"'.$selected.'>'.$key->f2.'</option>';
        }
        return $str;

    }
    function getFileName($name)
    {
        $splitTimeStamp = explode(".",$name);
        $name = $splitTimeStamp[0];
        $ext = '.'.$splitTimeStamp[1];
        return   $name.'-'.generateRandomStringNum(4).$ext;
    }
    function generateRandomStringNum($length) {
        $characters = '0123456789';
        $randomNo = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNo .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomNo;
    }
    function doc_upload($doc_path, $resize_doc_path, $file_id, $type = '')
{
  $CI = & get_instance();
  $doc_name = '';
  log_message('error', ' upload_doc >> doc file_id  = ' . $file_id);
  if (isset($_FILES[$file_id]['name']))
  {
    log_message('error', 'doc_path  = ' . $doc_path . ' resize_doc_path = ' . $resize_doc_path . ' doc name : ' . $_FILES[$file_id]['name']);
    if (file_exists($doc_path . $_FILES[$file_id]['name']))
    {
      $doc = $CI->home_model->getFileName($_FILES[$file_id]['name']);
      if (file_exists($doc_path . $_FILES[$file_id]["name"]))
      {
        $doc_name = $CI->home_model->getFileName($_FILES[$file_id]['name']);
      }
      else
      {
        $doc_name = $_FILES[$file_id]["name"];
      }
    }
    else
    {
      $doc_name = $_FILES[$file_id]["name"];
    }

    $sourcePath = $_FILES[$file_id]['tmp_name']; // Storing source path of the file in a variable
    $targetPath = $doc_path . $doc_name; // Target path where file is to be stored
    log_message('error', '>> doc sourcePath  = ' . $sourcePath);
    move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file

    if ($type == '')
    {
      copy($doc_path . $_FILES[$file_id]["name"], $resize_doc_path . $doc_name);
      $data = getimagesize($resize_doc_path . $_FILES[$file_id]['name']);
      $width = $data[0];
      $height = $data[1];
      $doc_height = (DOC_SIZE * $height) / $width;

      // CONTROLLING IMAGE WIDTH Large

      $config['image_library'] = 'gd2';
      $config['source_image'] = $resize_doc_path .$doc_name; //get original image

      // $config['create_thumb'] = TRUE;

      $config['maintain_ratio'] = TRUE;
      $config['file_name'] = $doc_name;
      $config['width'] = DOC_SIZE;
      $config['height'] = $doc_height;
      $CI->image_lib->initialize($config);
      if (!$CI->image_lib->resize())
      {
        $CI->handle_error($CI->image_lib->display_errors());
      }

      // END OF CONTROLLING IMAGE WIDTH CODE

    }

    log_message('error', ' upload_doc >> doc name  = ' . $doc_name);
  }

  return $doc_name;
}

function UnlinkFile($file)
{
  if (file_exists($file))
  {
    $image_name = unlink($file);
  }
  else
  {
    $image_name = $file;
  }

  return $image_name;
}

function getDropdownResult($type, $gpm_group, $fieldName, $value = false)
{
  $CI = & get_instance();
  $str = '';
  switch ($type)
  {
   case 'state-dropdown':
  $result = $CI->home_model->getStateDropdown($gpm_group);
    foreach($result as $resultkey)
    {
      $selected = '';
      if ($value)
      {
        if ($value == $resultkey->f1)
        {
          $selected = 'selected="selected"';
        }
      }
      $str.= '<option value="' . $resultkey->f1 . '"' . $selected . '>' . $resultkey->f2 . '</option>';
    }
    break;

  default:
    $str = '';
    break;
  }
  return $str;
}
?>