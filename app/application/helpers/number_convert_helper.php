<script>
   // ******* START FOR INDIAN CURRENCY FORMAT ******
     String.prototype.replaceAll = function(search, replacement) {
	var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
	};

	$('input.Stylednumber').keyup(function() {
	  var input = $(this).val().replaceAll(',', '');
	  if (input.length < 1)
		$(this).val();
	  else {
		var val = parseFloat(input);
		var formatted = inrFormat(input);
		if (formatted.indexOf('.') > 0) {
		  var split = formatted.split('.');
		  formatted = split[0] + '.' + split[1].substring(0, 2);
		}
		$(this).val(formatted);
	  }
	});

	function inrFormat(val) {
	  var x = val;
	  x = x.toString();
	  var afterPoint = '';
	  if (x.indexOf('.') > 0)
		afterPoint = x.substring(x.indexOf('.'), x.length);
	  x = Math.floor(x);
	  x = x.toString();
	  var lastThree = x.substring(x.length - 3);
	  var otherNumbers = x.substring(0, x.length - 3);
	  if (otherNumbers != '')
		lastThree = ',' + lastThree;
	  var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
	  return res;
	}
</script>