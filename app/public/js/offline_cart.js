var STICKER = 'sticker';
var BROCHURE_MIX = 'brochure-mix';

function changeItemTotal() {
    var price = document.getElementById('product_price').value;
    var height = document.getElementById('prd_height').value;
    var width = document.getElementById('prd_width').value;
    var qty = document.getElementById('item_qty').value;
    var total = document.getElementById('total');

    var subtotal = price * height * width * qty;

    total.value = subtotal;
}

$(document).ready(function() {
    /* Get User Details By User Id */
    $("#offline_username").change(function() {
        var offlineUserId = $(this).val();

        var data = {
            'offline_user_id': offlineUserId
        };
        $.ajax({
            url: base_url + "offline_controller/getOfflineUserById",
            method: 'post',
            data: data,
            dataType: 'json',
            success: function(response) {
                if (response.success == true) {
                    window.location.reload();
                }
            }
        });
    });

    /* Get Product list by category Id*/
    $("#prd_cat_id").change(function() {
        var cat_id = $(this).val();
        var height_width = $('#height_width');

        if (cat_id == 4 || cat_id == 7) {
            height_width.show();
        } else {
            height_width.hide();
        }

        $.ajax({
            url: base_url + "offline_controller/getProductsByCategoryId",
            method: 'post',
            data: {
                prd_cat_id: cat_id
            },
            dataType: 'json',
            success: function(response) {
                // Remove options
                $('#product_name').find('option').not(':first').remove();

                // Add options
                $.each(response, function(index, data) {
                    $('#product_name').append('<option value="' + data['prd_id'] + '">' + data['prd_name'] + '</option>');
                });
            }
        });
    });

    /* Get Product detail by Id*/
    $("#product_name").change(function() {
        var product_id = $(this).val();
        $.ajax({
            url: base_url + "offline_controller/getProductById",
            method: 'post',
            data: {
                prd_id: product_id
            },
            dataType: 'json',
            success: function(response) {
                $.each(response, function(index, data) {
                    $('#product_price').val(data['prd_price']);
                    changeItemTotal();
                });
            }
        });
    });

    /* Design file validation */
    $('.file_input').change(function() {
        var filePath = $(this).val();
        var allowedExtensions = /(\.CDR)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('Please upload file having extensions .CDR only.');
            $(this).val('');
            return false;
        }
    });

    $('.add_to_cart_offline').click(function() {
        var offline_username = $("#offline_username").val();
        var prd_category = $("#prd_cat_id").val();
        var product_id = $("#product_name").val();
        var prd_price = parseFloat($("#product_price").val());
        var prd_height = parseFloat($("#prd_height").val());
        var prd_width = parseFloat($("#prd_width").val());
        var prd_qty = parseInt($("#item_qty").val());
        // var total = $("#total").val();
        var remarks = $("#remark_box").val();

        if (offline_username != '' && prd_category != '' && product_id != '' && prd_price != '' && prd_height != '' && prd_width != '' && prd_qty != '' && prd_qty != 0) {
            var prd_subtotal = 0;
            if (prd_category == 4 || prd_category == 7) { /* 4= STICKER, 7= BROCHURE_MIX */
                prd_subtotal = parseFloat(prd_price * prd_height * prd_width * prd_qty);
            } else {
                prd_subtotal = parseFloat(prd_price * prd_qty);
                prd_height = '';
                prd_width = '';
            }
            $.ajax({
                url: base_url + "offline_shoping_cart/addToCart",
                method: "POST",
                data: {
                    product_id: product_id,
                    product_name: product_id,
                    product_price: prd_price,
                    product_subtotal: prd_subtotal,
                    product_quantity: prd_qty,
                    product_height: prd_height,
                    product_width: prd_width,
                    product_category: prd_category,
                    offline_user_id: offline_username,
                    item_remark: remarks
                },
                success: function(res) {
                    setTimeout(function() { location.reload(); }, 400); // Refresh Product List Page                
                }
            });
        } else {
            alert("Please fill up required fields.");
        }
    });

    $(document).on('click', '#remove_cart_product', function() {
        var row_id = $(this).data("cart_row");
        if (confirm("Are you sure you want to remove this product?")) {
            data = {
                row_id: row_id
            };
            $.ajax({
                url: base_url + "offline_shoping_cart/removeItem",
                method: "POST",
                data: data,
                success: function(data) {
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '#clear_cart', function() {
        if (confirm("Are you sure.You want clear cart?")) {
            $.ajax({
                url: base_url + "offline_shoping_cart/clear",
                success: function(data) {
                    window.location.href = base_url + 'offline';
                }
            });
        } else {
            return false;
        }
    });

    /***  Cart Section Start ***/

    /* OnCheckoutClick Start*/
    // $('.place-order').click(function() {

    $('#offlineCheckoutForm').submit(function(e) {
        if (confirm("Are you sure you want to place order?")) {
            try {
                e.preventDefault();
                var offlineCheckoutForm = new FormData(this);
                $.ajax({
                    url: base_url + "offline_shoping_cart/onOfflineCheckout",
                    data: offlineCheckoutForm,
                    type: 'POST',
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    async: false,
                    beforeSend: function() {
                        $('#result').html('Processing...');
                    },
                    success: function(data) {
                        if (data.success == true) {
                            alert(data.message);
                            $('#result').html('Checkout success!');
                            window.location.href = base_url + "offline_order_list";
                        } else {
                            $('#result').html('Checkout failed!');
                            window.location.href = data.linkn;
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        } else {
            return false;
        }
    });

    /* OnCheckoutClick  End*/

    /* Update Order Total- Start*/
    $(function() {
        $('#shipping_charge').change(function() {
            var shippingCharge = $(this).val();
            // $("#shipping_charge").val();
            var formData = {
                'shipping_charge': shippingCharge
            };
            if (shippingCharge != '') {
                $.ajax({
                    type: "POST",
                    url: base_url + "offline_shoping_cart/findGrandTotal",
                    data: formData,
                    dataType: "JSON",
                    beforeSend: function() {
                        $('#result').html('Processing...');
                    },
                    success: function(response) {
                        location.reload();
                        $('#result').html('Order Total Updated!');
                    }
                });
            }
        });
    });

    /* Update Order Total- End*/

    /* Update Pay Amount- Start*/
    $(function() {
        $('#pay_amount').change(function() {
            var payAmount = parseFloat($(this).val());
            var pendingAmount = $('#pending_amount');
            var grandTotal = parseFloat($('#grand_total').val());

            if (payAmount > grandTotal) {
                alert("Pay amount should be equal or less than order total.");
                $(this).val('');
                $(this).focus();
            } else {
                pending_amt = parseFloat(grandTotal - payAmount);
                pendingAmount.val(parseFloat(pending_amt));
            }
        });
    });
    /* Update Pay Amount- End*/

    /* Update Cart Item */
    $(function() {
        $('.updatecart').change(function() {
            var rowid = $(this).data("rowid");
            var qty = $("#qty" + rowid).val();

            var formData = {
                'rowid': rowid,
                'qty': $('input[name=qty' + rowid + ']').val(),
                'price': $('input[name=price' + rowid + ']').val(),
                'height': $('input[name=height' + rowid + ']').val(),
                'width': $('input[name=width' + rowid + ']').val(),
                'category': $('input[name=category' + rowid + ']').val()
            };
            // alert(JSON.stringify(formData));

            $.ajax({
                type: "POST",
                url: base_url + "offline_shoping_cart/updateCart",
                data: formData,
                beforeSend: function() {
                    $('#result').html('Processing...');
                },
                success: function(data) {
                    console.log("data: " + data);
                    location.reload();
                    $('#result').html('Item Updated!');
                }
            });
        });
    });

    /* Update Item  Quantity */
    $(function() {
        $('.changeqty').click(function() {
            var rowid = $(this).data("rowid");
            var fired_button = $(this).val();
            var qty = $("qty" + rowid).val();

            var formData = {
                'rowid': rowid,
                'qty': $('input[name=qty' + rowid + ']').val(),
                'change': fired_button,
                'price': $('input[name=price' + rowid + ']').val(),
                'height': $('input[name=height' + rowid + ']').val(),
                'width': $('input[name=width' + rowid + ']').val(),
                'category': $('input[name=category' + rowid + ']').val()
            };
            // alert(JSON.stringify(formData));

            $.ajax({
                type: "POST",
                url: base_url + "offline_shoping_cart/updateCart",
                data: formData,
                beforeSend: function() {
                    $('#result').html('Processing...');
                },
                success: function(data) {
                    console.log("data: " + data);
                    // location.reload();
                    window.location.reload();
                    $('#result').html('Item Size Updated!');
                }
            });
        });
    });

    /***  Cart Section End ***/
});

function valueChanged(current_tag) {
    alert("current_tag: " + current_tag);
    this.updateCart(current_tag);
}


function updateCartItem(obj, rowid, category) {
    data = {
            cart_id: rowid,
            cart_qty: obj.value,
            cart_category: category
        },
        $.ajax({
            url: base_url + "offline_shoping_cart/updateItemQty",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                if (data == 1) {
                    $('#response').html(data);
                } else {
                    $('#response').html(0);
                }

                alert("Data: " + data);
                if (data.redirect) {
                    swal({
                        title: '¡Success!',
                        text: data.msg,
                        timer: 2000,
                        type: data.type,
                        showConfirmButton: false
                    }, function() {
                        window.location.href = data.redirect;
                    });
                } else {
                    swal('¡Error!', data.msg, data.type);
                }
            }
        });
}

function clearData() {
    $('#cart_type').val();
    $('#cart_id').val();
    $('#cart_qty').val();
    return true;
}

function updateCart(current_tag) {
    var cart_type = $('#cart_type').val();
    var cart_id = $('#cart_id').val();
    var cart_qty = '';
    switch (cart_type) {
        case 'qty':
            cart_qty = $(current_tag).data("qty");
            break;
        default:
            cart_flavour = '';
            cart_qty = '';
            break;
    }
    var cart_qty = cart_qty;
    data = {
            cart_type: cart_type,
            cart_id: cart_id,
            cart_qty: cart_qty
        },
        console.log(data);
    $.ajax({
        url: base_url + "shoping_cart/updateCart",
        method: "POST",
        data: data,
        dataType: "JSON",
        success: function(data) {
            $('.close_modal').click();
            location.reload();

        }

    });
}

function placeOrder(argument) {
    var prs_id = document.getElementById('prs_id').value;
    if (prs_id == '') {
        window.location.href = base_url + 'login';
    } else {
        window.location.href = base_url + 'user-address';
    }
}

function getQty(qty) {
    data = {
            qty: qty,
        },
        $.ajax({
            url: base_url + "product/getQuantity",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#change-qty').html('');
                $('#change-qty').html(data);
            }
        });
}