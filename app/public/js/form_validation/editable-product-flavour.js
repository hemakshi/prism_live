$(function(){
	$("#addnewclick").click(function() {
		document.getElementById("add_param").reset();
		 document.getElementById('pfi_img').setAttribute('src',document.getElementById('pfi_default_img').value);
		document.getElementById("pfi_id").value="";
		  $("#prd-drop").css("display", "block");
   $("#flavour-drop").css("display", "block");
    $("#prd-input").css("display", "none");
   $("#flavour-input").css("display", "none");
		$("#addnewform").toggle("slow");
	});
});
function addnewclickCancel(argument) {
	$("#addnewform").toggle("slow");
	
}

function edit(pfi_id)
 {

	$.ajax({
		type:"POST",
    	url:base_url+"product/editPrdFlv", 
		data :{ pfi_id:pfi_id},
		success: function(myvar){
			 window.mydata = myvar;
			var a=trim(myvar);
	
	var arr1 = new Array();
	arr1=a.split("##");
		
 $('head').append(' <link href="'+base_url+'assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
  loadScript(""+base_url+"public/assets/global/plugins/select2/js/select2.full.min.js");
loadScript(""+base_url+"public/assets/pages/scripts/components-select2.min.js");
	document.getElementById("pfi_id").value=arr1[0];
	document.getElementById("product").value=arr1[1];
	document.getElementById("flavour").value=arr1[2];
	document.getElementById('pfi_img').setAttribute('src',document.getElementById('pfi_img_path').value+arr1[3]);
	document.getElementById("pfi_old_img").value=arr1[3];
	$("#addnewform").toggle("slow");
   $("#prd-drop").css("display", "none");
   $("#flavour-drop").css("display", "none");
    $("#prd-input").css("display", "block");
   $("#flavour-input").css("display", "block");
	
		}
	});
}




function LTrim( value )
{
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
}

function RTrim( value )
{
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
}

function trim( value )
{
	return LTrim(RTrim(value));
}
function deleteClient(current_tag,pfi_id)
 {
	var d=confirm("ARE YOU SURE ?,You want to delete this Flavour.")
		if(d==1)
		{
		data ={
			pfi_id:pfi_id,
			flv_old_img:$(current_tag).attr('data-img')
			}
			$.ajax({
				type: "POST",
				url: base_url+"product/deletePortfolio", 
				data : data,
				dataType:"json",
				success: function(response){
					if(response.success==true)
					{
						alert(response.message);
						location.reload();
					}
					if(response.success==false)
					{
						alert(response.message);
					
					}

				}
			});
			return false;

		}
}

function loadScript(url)
{
    document.body.appendChild(document.createElement("script")).src = url;
}