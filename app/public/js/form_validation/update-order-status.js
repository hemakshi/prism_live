$(function() {
    $("#form_change_order_status").validate({
        submitHandler: function(form) {
            data = {
                ord_prs_id: $('#ord_prs_id').val(),
                order_id: $('#order_id').val(),
                ord_status: $('#ord_status').val(),
                ops_remark: $('#ops_remark').val()
            }
            $.ajax({
                method: "POST",
                url: base_url + "order/updateOrderStatus",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },

                success: function(response) {
                    if (response.success == true) {
                        alert(response.message);
                        location.reload();
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        }
    });
});

function showChangeOrderStatusModal(current_tag) {
    getProductStatus($(current_tag).data("order_status"));
    current_tag.setAttribute("href", "#changeOrderStatusModal");
    document.getElementById('order_id').value = $(current_tag).data("order_id");
    document.getElementById('ops_remark').value = $(current_tag).data("ord_remark");
    document.getElementById('ord_prs_id').value = $(current_tag).data("ord_prs_id");
    document.getElementById('order-status-modal-title').innerHTML = 'Change status of order#<b>' + $(current_tag).data("ord_reference_no") + '</b>';
}

function getProductStatus(status) {
    data = {
            status: status
        },
        $.ajax({
            url: base_url + "order/getOrderStatus",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#ord_status').html('');
                $('#ord_status').html(data);
            }

        });
}

/* Start- Add Shipping Charge for Single Product*/
function showAddOrdrShippingChargeModal(current_tag) {
    current_tag.setAttribute("href", "#AddShippingCharge");
    document.getElementById('ord_prs_id').value = $(current_tag).data("ord_prs_id");
    document.getElementById('order_id').value = $(current_tag).data("order_id");
    document.getElementById('ord_shipping_charges').value = $(current_tag).data("shipping_charge");
    document.getElementById('app-shipping-charge-modal-title').innerHTML = 'Add Shipping Charges of Order#<b>' + $(current_tag).data("ord_reference_no") + '</b>';
}

$(function() {
    $("#formAddShippingCharge").validate({
        rules: {
            ord_shipping_charges: {
                required: true,
                digits: true
            }
        },
        messages: {
            ord_shipping_charges: { required: "Please enter shipping charges.", digits: "Please enter numbers only." }
        },
        errorClass: "errormesssage",
        submitHandler: function(form) {
            data = {
                ord_prs_id: $('#ord_prs_id').val(),
                order_id: $('#order_id').val(),
                ord_shipping_charges: $('#ord_shipping_charges').val(),
            }
            $.ajax({
                type: "POST",
                url: base_url + "Order/updateOrderShippingCharges",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        alert(response.message);
                        location.reload();
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);

                    }
                }
            });
        }
    });
});
/* End- Add Shipping Charge for Single Product*/