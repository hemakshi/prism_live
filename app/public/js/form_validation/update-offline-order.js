$(function() {
    $("#form_update_order_detail").validate({
        submitHandler: function(form) {
            data = {
                offline_order_id: $('#order_id').val(),
                ord_prs_id: $('#ord_prs_id').val(),
                order_total: $('#order_total').val(),
                paid_amount: $('#paid_amount').val(),
                pending_amount: $('#pending_amount').val(),
                add_amount: $('#add_amount').val(),
                ord_status: $('#ord_status').val(),
                ops_remark: $('#ops_remark').val()
            }

            $.ajax({
                type: "POST",
                url: base_url + "offline_controller/updateOrderDetail",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },

                success: function(response) {
                    if (response.success == true) {
                        location.reload();
                        alert(response.message);
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        }
    });
});

function showChangeOrderDetailModal(current_tag) {
    getProductStatus($(current_tag).data("order_status"));
    getOrderDetail($(current_tag).data("order_id"));

    current_tag.setAttribute("href", "#showChangeOrderDetailModal");
    document.getElementById('order_id').value = $(current_tag).data("order_id");
    document.getElementById('ord_prs_id').value = $(current_tag).data("ord_prs_id");
    document.getElementById('order-status-modal-title').innerHTML = 'Order#<b>' + $(current_tag).data("ord_reference_no") + '</b>';
}

function getProductStatus(status) {
    data = {
            status: status
        },
        $.ajax({
            url: base_url + "order/getOrderStatus",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#ord_status').html('');
                $('#ord_status').html(data);
            }

        });
}

function getOrderDetail(order_id) {
    data = {
            order_id: order_id
        },
        $.ajax({
            url: base_url + "offline_controller/getOfflineOrderDetailsById",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(res) {
                if (res.success == true) {
                    $('#label_order_total').html(res.data.offline_order_total);
                    $('#label_paid_amount').html(res.data.offline_order_paid_amt);
                    $('#label_pending_amount').html(res.data.offline_order_pending_amt);
                    $('#add_amount').attr('max', res.data.offline_order_pending_amt);

                    $('#order_total').val(res.data.offline_order_total);
                    $('#paid_amount').val(res.data.offline_order_paid_amt);
                    $('#pending_amount').val(res.data.offline_order_pending_amt);
                } else {
                    alert(res.message);
                }
            }
        });
}

/* Start- Add Shipping Charge for Single Product*/
function showAddOrdrShippingChargeModal(current_tag) {
    current_tag.setAttribute("href", "#AddShippingCharge");
    document.getElementById('ord_prs_id').value = $(current_tag).data("ord_prs_id");
    document.getElementById('order_id').value = $(current_tag).data("order_id");
    document.getElementById('app-shipping-charge-modal-title').innerHTML = 'Add Shipping Charges of Order#<b>' + $(current_tag).data("ord_reference_no") + '</b>';
}

$(function() {
    $("#formAddShippingCharge").validate({
        rules: {
            ord_shipping_charges: {
                required: true,
                digits: true
            }
        },
        messages: {
            ord_shipping_charges: { required: "Please enter shipping charges.", digits: "Please enter numbers only." }
        },
        errorClass: "errormesssage",
        submitHandler: function(form) {
            data = {
                ord_prs_id: $('#ord_prs_id').val(),
                order_id: $('#order_id').val(),
                ord_shipping_charges: $('#ord_shipping_charges').val(),
                ops_remark: $('#ops_remark').val()
            }
            $.ajax({
                type: "POST",
                url: base_url + "Order/updateOrderShippingCharges",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        location.reload();
                        alert(response.message);
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);

                    }
                }
            });
        }
    });
});
/* End- Add Shipping Charge for Single Product*/