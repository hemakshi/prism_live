$(function() {
    // Setup form validation on the #register-form element
    $("#add_param").validate({
        submitHandler: function(form) {
            dataString = {
                cat_id: $('#cat_id').val(),
                cat_menu: $('#cat_menu').val(),
                cat_name: $('#cat_name').val(),
                cat_order: $('#cat_order').val(),

            };

            $.ajax({
                type: "POST",
                url: base_url + "product/insertProductCategory",
                data: dataString,
                dataType: "json",
                beforeSend: function() {
                    $('#form_submit').attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        alert(response.message);
                        location.reload();
                    } else {
                        $('#form_submit').removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        }
    });
});