function addnewclickCancel(argument) {
    $("#addnewform").toggle("slow");
    clearData();
}
$("#addnewclick").click(function() {

    $("#addnewform").toggle("slow");
    clearData();
});
$(document).ready(function() {
    getGallerySetList();
});
$('#gls_form').validate({
    submitHandler: function(form) {
        try {
            var formData = new FormData();
            //********* IMAGE UPLOAD ***********//
            var file = document.getElementById('gls_image');
            var filejquery = $('#gls_image');
            var count = file.files.length;
            formData.append('file_count', count);
            for (var i = 0; i < count; i++) {
                var allowedFiles = ["jpeg", "jpg", "png", "JPG", "PNG"];

                // var file_name = files1;
                if (count != '0') {
                    var fileName = file.files[0].name;
                    var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                    var size = parseFloat(file.files[0].size / 1024).toFixed(2);
                    if ($.inArray(fileNameExt, allowedFiles) == -1 || size > 5000) {
                        var data = fileName + " is Invalid ";
                        flag = false;

                        filejquery.css('border-color', 'red ');
                        filejquery.next().css('color', 'red ');
                        filejquery.next().html(data);
                        return false;
                    } else {
                        flag = true;
                        filejquery.css('border-color', '#ccc');
                        filejquery.next().css('color', 'none ');
                        filejquery.next().html('');
                        formData.append("gls_image", document.getElementById('gls_image').files[i]);
                    }

                } else {
                    flag = true;
                    filejquery.css('border-color', '#ccc');
                    filejquery.next().css('color', 'none ');
                    filejquery.next().html('');
                }
            }
            //********* IMAGE UPLOAD ***********//

            formData.append('gls_id', $('#gls_id').val());
            formData.append('gls_name', $('#gls_name').val());
            formData.append('gls_order_by', $('#gls_order_by').val());
            formData.append('gls_old_img', $('#gls_old_img').val());
            formData.append('gls_type', $('#gls_type').val());
            formData.append('gls_link', $('#gls_link').val());

            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: formData,
                url: base_url + 'gallery_set/gls_form',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Save');
                    $('#form_submit').attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        alert(response.message);
                        $("#form_submit").html('Save');
                        $('#form_submit').removeAttr('disabled', 'disabled');
                        getGallerySetList();
                        $("#addnewform").toggle("slow");
                    } else {
                        $("#form_submit").html('Save');
                        $('#form_submit').removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });

        } catch (e) {
            console.log(e);
        }

    }
});

function customDatatable(element, url, columns, buttons = true, serverSide = false, delay = 1000, optParam = {}) {
    var common_dataTableObj = {
        responsive: false,
        "order": [],
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered to 1 from _END_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "lengthMenu": [
            [100, 200, 400, -1],
            [100, 200, 400, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 100,
        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

    };
    var btn_obj = [];

    if (buttons) {
        btn_obj = [
            { extend: 'print', className: 'btn dark btn-outline' },
            { extend: 'copy', className: 'btn red btn-outline' },
            { extend: 'excel', className: 'btn yellow btn-outline ' },
            { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns' }
        ];
    }

    common_dataTableObj = $.extend({}, common_dataTableObj, {
        buttons: btn_obj
    });

    if (serverSide) {
        common_dataTableObj = $.extend({}, common_dataTableObj, {
            searchDelay: delay,
            "serverSide": true,
        });
    }

    var dataTableObj = $.extend({}, {
            'ajax': url,
            'columns': columns
        },
        common_dataTableObj
    );

    /*if(optParam)
    {
    dataTableObj = $.extend({}, dataTableObj, optParam);
    }*/

    $(element).dataTable(dataTableObj);
}

function getGallerySetList() {
    var imgSmallLink = $('#gls_img_small').val();
    var imgBigLink = $('#gls_img_big').val();
    var customDataTableElement = '#gallery_set_list';
    $(customDataTableElement).DataTable().destroy();
    var customDataTableUrl = base_url + 'gallery_set/getGalleryDataTableList';
    var customDataTableColumns = [
        { 'data': 'gls_name' },
        { 'data': 'gls_order_by' },
        {
            'data': 'gls_image',
            'render': function(data, type, row, meta) {
                return '<a  data-toggle="modal" href="#imageModal" data-image_name=' + row.gls_image + '  data-image_title="' + row.gls_name + '" onclick="showImage(this)"><img width="150px" height="100px" src="' + base_url + imgBigLink + row.gls_image + '" title="View Image"></a>';
            }
        },
        {
            'data': 'gls_id',
            'render': function(data, type, row, meta) {
                return '<a id="addnewclick"  class="red btn-outline filter-cancel" data-image_name="' + row.gls_name + '" data-image_src="' + row.gls_image + '" data-image_order_by="' + row.gls_order_by + '" data-image_id="' + row.gls_id + '" onclick="updateDataInFields(this)" title="Update Details"><i class="fa fa-edit btn btn-danger"></i></a>&nbsp;<a  title="Delete Image" "><i  class="fa fa-trash btn btn-danger" aria-hidden="true" data-image_name="' + row.gls_image + '" onclick="deleteGallerySet(' + row.gls_id + ',this)"></i></a>&nbsp;';
            }
        }
    ];

    customDatatable(customDataTableElement, customDataTableUrl, customDataTableColumns, true);
}

function showImage(thisElement) {
    var imageSrcName = $(thisElement).data('image_name');
    var img_title = $(thisElement).data('image_title');
    var imgBigLink = $('#gls_img_big').val();
    var image_src = base_url + imgBigLink + imageSrcName;
    $('#image_src_tg').attr('src', image_src);
    $('#modal-title').html(img_title);
}

function updateDataInFields(thisElement) {
    $("#addnewform").toggle("slow");

    var imageSrcName = $(thisElement).data('image_name');
    var image_file = $(thisElement).data('image_src');
    var image_order_by = $(thisElement).data('image_order_by');
    var image_id = $(thisElement).data('image_id');
    var imgBigLink = $('#gls_img_big').val();
    var image_src = base_url + imgBigLink + image_file;
    $('#gls_img_pic').attr('src', image_src);
    $('#gls_name').val(imageSrcName);
    $('#gls_order_by').val(image_order_by);
    $('#gls_id').val(image_id);
    $('#gls_old_img').val(image_id);
    document.getElementById("gls_image").required = false;
}

function deleteGallerySet(gls_id, thisElement) {
    var imageSrcName = $(thisElement).data('image_name');
    data = {
            gls_id: gls_id,
            imageSrcName: imageSrcName
        },
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: data,
            url: base_url + 'gallery_set/deleteGlsImage',
            success: function(response) {
                if (response.success == true) {
                    alert(response.message);
                    getGallerySetList();
                } else {
                    alert(response.message);
                }
            }
        });
}

function clearData() {
    document.getElementById('gls_img_pic').setAttribute('src', document.getElementById('gls_default_img').value);
    $('#gls_name').val('');
    $('#gls_order_by').val('');
    $('#gls_id').val('');
    $('#gls_old_img').val('');
}