  $(function() {
      $("#edit_offline_customer").validate({
          submitHandler: function(form) {
              var formData = new FormData();
              formData.append('prs_slug', $('#prs_slug').val());
              formData.append('prs_name', $('#prs_name').val());
              formData.append('prs_mob', $('#prs_mob').val());
              formData.append('prs_email', $('#prs_email').val());
              formData.append('prs_gender', $('#prs_gender').val());
              formData.append('prs_dob', $('#prs_dob').val());
              formData.append('prs_gst', $('#prs_gst').val());
              formData.append('prs_address_line', $('#prs_address_line').val());
              formData.append('prs_district', $('#prs_district').val());
              formData.append('prs_city', $('#prs_city').val());
              formData.append('prs_state', $('#prs_state').val());
              formData.append('prs_pincode', $('#prs_pincode').val());
              if ($("#prs_status").prop('checked') == true) {
                  formData.append('prs_status', "1");
              } else {
                  formData.append('prs_status', "0");
              }

              $.ajax({
                  type: "POST",
                  url: base_url + "offline_controller/updateOfflineCustomerDetail",
                  data: formData,
                  dataType: "json",

                  contentType: false, // The content type used when sending data to the server.
                  cache: false, // To unable request pages to be cached
                  processData: false,

                  beforeSend: function() {
                      $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                      $("#form_submit").attr('disabled', 'disabled');
                  },
                  success: function(response) {
                      if (response.success == true) {
                          alert(response.message);
                          window.location.href = response.linkn;
                      } else {
                          $("#form_submit").html('Save');
                          $('#form_submit').removeAttr('disabled', 'disabled');
                          alert(response.message);
                      }
                  }
              });
          }
      });
  });