 $(function() {
     $("#add_offline_user").validate({
         //  rules: {

         //  },
         //  messages: {

         //  },
         submitHandler: function(form) {
             var formData = new FormData();
             formData.append('customer_name', $('#customer_name').val());
             formData.append('customer_username', $('#customer_username').val());
             formData.append('customer_email', $('#customer_email').val());
             formData.append('customer_mobile', $('#customer_mobile').val());
             formData.append('customer_gender', $('#customer_gender').val());
             formData.append('customer_dob', $('#customer_dob').val());
             formData.append('customer_gst', $('#customer_gst').val());
             formData.append('customer_address_line', $('#customer_address_line').val());
             formData.append('customer_city', $('#customer_city').val());
             formData.append('customer_pincode', $('#customer_pincode').val());
             formData.append('customer_district', $('#customer_district').val());
             formData.append('customer_state', $('#customer_state').val());

             $.ajax({
                 type: "POST",
                 url: base_url + "offline_controller/insertOfflineCustomer",
                 data: formData,
                 dataType: "json",
                 contentType: false, // The content type used when sending data to the server.
                 cache: false, // To unable request pages to be cached
                 processData: false,

                 beforeSend: function() {
                     $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                     $("#form_submit").attr('disabled', 'disabled');
                 },
                 success: function(response) {
                     if (response.success == true) {
                         alert(response.message);
                         window.location.href = response.linkn;
                     } else {
                         alert(response.message);
                         $("#form_submit").html('Add');
                         $('#form_submit').removeAttr('disabled', 'disabled');
                         window.location.href = response.linkn;
                     }
                 }
             });
         }
     });
 });