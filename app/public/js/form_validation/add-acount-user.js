$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    }, );
$(function() {
    $("#admin_change_password_form").validate({
        rules: {
            inputPasswordOld: {
                required: true,
                remote: {
                    url: base_url + 'user/validatePersonPassword',
                    type: "post",
                    data: {
                        inputPasswordOld: function() {
                            return $('#inputPasswordOld').val();
                        },
                        user_id: function() {
                            return $('#user_id').val();
                        },
                    },
                },
            },
            inputPasswordNew: {
                required: true,
                regex: /^.{8,}$/,
            },
            inputPasswordConfirm: {
                required: true,
                equalTo: "#inputPasswordNew"
            },
        },
        messages: {
            inputPasswordOld: {
                remote: function() { return $.validator.format("Please provide valid old password"); },
                required: "Please provide a old password",
            },
            inputPasswordNew: {
                required: "Please provide a new password",
                regex: "Password must be at least 8 characters.",
            },
            inputPasswordConfirm: {
                required: "Please provide a confirm password",
                equalTo: "New Password & Confirm Password Did Not Match."
            }
        },

        submitHandler: function(form) {
            var old_password = document.getElementById('inputPasswordOld').value;
            var password = document.getElementById('inputPasswordNew').value;
            var user_id = document.getElementById('user_id').value;
            data = {
                old_password: old_password,
                password: password,
                user_id: user_id
            }
            $.ajax({
                type: "POST",
                url: base_url + "user/changeUserPassword",
                data: data,
                dataType: "json",
                success: function(response) {
                    if (response.success == true) {
                        alert(response.message);
                        // document.getElementById("processing_pwd").style.display = "inline-block";
                        window.location.href = response.linkn;
                        location.reload();
                    } else {
                        alert(response.message);
                        document.getElementById("processing_pwd").style.display = "none";
                    }
                }
            });
        }
    });
});