$(function() {
    $("#quotation_detail_form").validate({
        rules: {
            quotation_hsn: {
                required: true
            },
            quotation_tax: {
                required: true
            },
            quotation_price: {
                required: true
            },
            quotation_total_price: {
                required: true
            }
        },
        errorClass: "errormesssage",
        submitHandler: function(form) {
            formData = {
                qr_id: $('#quotation_id').val(),
                qr_remarks: $('#quotation_remarks').val(),
                qr_hsn: $('#quotation_hsn').val(),
                qr_tax: $('#quotation_tax').val(),
                qr_price: $('#quotation_price').val(),
                qr_total_price: $('#quotation_total_price').val(),
            }
            $.ajax({
                type: "POST",
                url: base_url + "home/updateQuotationDetails",
                data: formData,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        alert(response.message);
                        window.location.href = response.linkn;
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        }
    });
});