 $(function() {
     $("#add_product").validate({


         submitHandler: function(form) {


             // var upload_button_value_docs = document.getElementById('upload_button_value_docs').value;

             // if(upload_button_value_docs!=0)
             // {

             dataString = {
                 timg_reference_no: $('#timg_reference_no').val(),
                 prd_id: $('#prd_id').val(),
                 prd_slug: $('#prd_slug').val(),
                 prd_name: $('#prd_name').val(),
                 prd_size: $('#prd_size').val(),
                 // prd_short_desc: document.getElementById('summernote_2').value,
                 prd_cat_id: $('#prd_cat_id').val(),
                 prd_price: $('#prd_price').val(),
                 prd_flv_id: $('#prd_flv_id').val(),
                 //  prd_quantity_unit: $('#prd_quantity_unit').val(),
                 // prd_availability: $('#prd_availability').val(),
                 prd_order: $('#prd_order').val(),
                 // prd_discount: $('#prd_discount').val(),
                 // prd_descripion: document.getElementById('summernote_1').value,
                 prd_menu: $('#prd_menu').val(),
                 // prd_brand: $('#prd_brand').val()
             };
             $.ajax({
                 type: "POST",
                 url: base_url + "product/updateProductData",
                 data: dataString,
                 dataType: "json",
                 beforeSend: function() {
                     $("#form_submit").attr('disabled', 'disabled');
                 },
                 success: function(response) {
                     if (response.success == true) {
                         alert(response.message);
                         window.location.href = response.linkn;
                     } else {
                         $('#form_submit').removeAttr('disabled', 'disabled');
                         alert(response.message);
                     }

                 }
             });


             // }
             // else
             // {
             //   alert('Please click on upload button before submitting details to upload images.');
             // }

         }

     });

 });


 //  function upload_images(argument) {
 //   $('.remove-doc').attr('disabled','disabled');


 //   document.getElementById('upload_button_value_docs').value='1';

 //   var form_data = new FormData();
 //   var ins = document.getElementById('files1').files.length;

 //   var document_input = new Array();
 //   var documents_count = document.getElementById('documents_count').value; 
 //   var timg_reference_no = document.getElementById('timg_reference_no').value; 

 //   for(i=0;i<=documents_count;i++)
 //   {
 //     document_input.push($('#document_input'+i).val());

 //   } 
 //   for (var x = 0; x < ins; x++) 
 //   {
 //    form_data.append("files[]", document.getElementById('files1').files[x]);
 //  }




 //  form_data.append("timg_reference_no",timg_reference_no);
 //  form_data.append("document_input",document_input);
 //  $.ajax({
 //   url: base_url +"product/uploadProductImages",
 //            dataType:"json", // what to expect back from the server
 //            cache: false,
 //            contentType: false,
 //            processData: false,
 //            data: form_data,
 //            type: 'post',
 //            beforeSend: function(){
 //             $("#upload_docs").attr('disabled','disabled');

 //           },
 //           success: function(response)
 //           {
 //             if(response.success==true)
 //             {
 //              document.getElementById("timg_reference_no").setAttribute("value", response.timg_reference_no);

 //              document.getElementById("documents_priview").innerHTML='';
 //              $('#information1').css('display','none');
 //              $('#upload_docs').css('display','none');
 //              $("#upload_docs").removeAttr('disabled','disabled');

 //              var success_msg='Following images uploaded successfully.</br>'+response.message;
 //              document.getElementById("doc_uploaded").innerHTML=document.getElementById("doc_uploaded").innerHTML+success_msg;

 //            }
 //            else{
 //              document.getElementById("timg_reference_no").setAttribute("value", response.timg_reference_no);
 //              document.getElementById("documents_priview").innerHTML='';
 //              $('#information1').css('display','none');
 //              $('#upload_docs').css('display','none');
 //              $("#upload_docs").removeAttr('disabled','disabled');
 //              if(response.uploaded_documents!='')
 //              {
 //               var success_msg='Following images uploaded successfully.</br>'+response.uploaded_documents;
 //               document.getElementById("doc_uploaded").innerHTML=document.getElementById("doc_uploaded").innerHTML+success_msg;
 //             }
 //             var error_msg='Following images cannot be uploaded..Invalid size or type</br>'+response.message;
 //             document.getElementById("doc_error_invalid_size").innerHTML=document.getElementById("doc_error_invalid_size").innerHTML+error_msg;

 //           }
 //         }
 //       });



 // }


 // ******* START FOR INDIAN CURRENCY FORMAT ******
 String.prototype.replaceAll = function(search, replacement) {
     var target = this;
     return target.replace(new RegExp(search, 'g'), replacement);
 };

 $('input.Stylednumber').keyup(function() {
     var input = $(this).val().replaceAll(',', '');
     if (input.length < 1)
         $(this).val();
     else {
         var val = parseFloat(input);
         var formatted = inrFormat(input);
         if (formatted.indexOf('.') > 0) {
             var split = formatted.split('.');
             formatted = split[0] + '.' + split[1].substring(0, 2);
         }
         $(this).val(formatted);
     }
 });

 function inrFormat(val) {
     var x = val;
     x = x.toString();
     var afterPoint = '';
     if (x.indexOf('.') > 0)
         afterPoint = x.substring(x.indexOf('.'), x.length);
     x = Math.floor(x);
     x = x.toString();
     var lastThree = x.substring(x.length - 3);
     var otherNumbers = x.substring(0, x.length - 3);
     if (otherNumbers != '')
         lastThree = ',' + lastThree;
     var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
     return res;
 }
 // ******* END FOR INDIAN CURRENCY FORMAT ******

 function getBrands(menu) {
     if (menu == 2) {
         $.ajax({
             type: "POST",
             url: base_url + "shop/ajaxCallBrands",
             success: function(myvar) {
                 document.getElementById('prd_brand').value = '';
                 $('#prd_brand').html(myvar);
                 document.getElementById('brand_div').style.display = " block";
             }
         });
     } else {
         document.getElementById('brand_div').style.display = " none";
         document.getElementById('prd_brand').value = '';
     }


 }

 function getCategories(menu) {
     $.ajax({
         type: "POST",
         url: base_url + "product/ajaxCallCategories",
         data: { menu: menu },
         success: function(myvar) {
             document.getElementById('prd_cat_id').value = '';
             $('#prd_cat_id').html(myvar);
         }
     });

 }