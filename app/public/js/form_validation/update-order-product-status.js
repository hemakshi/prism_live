$(function() {
    $("#formChangeProductStatus").validate({
        submitHandler: function(form) {
            data = {
                odp_id: $('#odp_id').val(),
                order_id: $('#order_id').val(),
                product_id: $('#product_id').val(),
                odp_status: $('#odp_status').val(),
                ops_remark: $('#ops_remark').val()
            }
            $.ajax({
                type: "POST",
                url: base_url + "order/updateOrderProductStatus",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        location.reload();
                        alert(response.message);
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);

                    }
                }
            });
        }
    });
});

function showChangeProductStatusModal(current_tag) {
    getProductStatus($(current_tag).data("status"));
    current_tag.setAttribute("href", "#ChangeProductStatus");
    document.getElementById('product_id').value = $(current_tag).data("product_id");
    document.getElementById('odp_id').value = $(current_tag).data("odp_id");
    document.getElementById('order_id').value = $(current_tag).data("order_id");
    document.getElementById('ops_remark').innerHTML = $(current_tag).data("remark");
    document.getElementById('modal-title').innerHTML = 'Update product status of ' + $(current_tag).data("name");
}

function getProductStatus(status) {
    data = {
            status: status
        },
        $.ajax({
            url: base_url + "product/getProdStatus",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#odp_status').html('');
                $('#odp_status').html(data);
            }
        });
}

/* Start- Add Shipping Charge for Single Product*/
function showAddProductShippingChargeModal(current_tag) {
    current_tag.setAttribute("href", "#AddProductShippingCharge");
    document.getElementById('odp_id').value = $(current_tag).data("odp_id");
    document.getElementById('order_id').value = $(current_tag).data("order_id");
    document.getElementById('product_shipping_charges').value = $(current_tag).data("product_shipping_charges");
    document.getElementById('app-shipping-charge-modal-title').innerHTML = 'Add Shipping Charge of <b>' + $(current_tag).data("name") + '</b>';
}

$(function() {
    $("#formAddProductShippingCharge").validate({
        rules: {
            ord_shipping_charges: {
                required: true,
                digits: true
            }
        },
        messages: {
            ord_shipping_charges: { required: "Please enter shipping charges.", digits: "Please enter numbers only." }
        },
        errorClass: "errormesssage",
        submitHandler: function(form) {
            data = {
                odp_id: $('#odp_id').val(),
                order_id: $('#order_id').val(),
                product_shipping_charges: $('#product_shipping_charges').val(),
            }
            $.ajax({
                type: "POST",
                url: base_url + "product/updateSingleProductShippingCharges",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        location.reload();
                        alert(response.message);
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);

                    }
                }
            });
        }
    });
});
/* End- Add Shipping Charge for Single Product*/