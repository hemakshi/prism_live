$(function() {
    $("#form_update_complaint").validate({
        submitHandler: function(form) {
            data = {
                com_id: $('#com_id').val(),
                com_status: $('#com_status').val(),
                com_remarks: $('#com_remarks').val()
            }
            $.ajax({
                method: "POST",
                url: base_url + "home/updateCompliantDetails",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        location.reload();
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        }
    });
});

function showUpdateComplaintModal(current_tag) {
    current_tag.setAttribute("href", "#updateComplaintModal");
    document.getElementById('com_id').value = $(current_tag).data("com_id");
    $("#com_status").val($(current_tag).data("com_status")).change();
    $("#com_remarks").val($(current_tag).data("com_remarks"));
    $("#person_mobile").val($(current_tag).data("person_mobile"));
    document.getElementById('complaint-modal-title').innerHTML = 'Complaint for Order Id#<b>' + $(current_tag).data("com_ord_id") + '</b>';
}