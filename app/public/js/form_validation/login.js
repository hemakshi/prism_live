$(function() {


    // Setup form validation on the #register-form element
    $("#login_form").validate({


        submitHandler: function(form) {
            if (document.getElementById('rememberme').checked == true) {
                var rememberme = 1;

            } else {
                var rememberme = 0;

            }

            dataString = {
                usr_username: $('#usr_username').val(),
                usr_password: $('#usr_password').val(),
                ref: $('#ref').val(),
                rememberme: rememberme,
            };

            $.ajax({
                type: "POST",
                url: base_url + "login/loginUser",
                data: dataString,
                dataType: "json",
                beforeSend: function() {
                    $('#form_submit').attr('disabled', 'disabled');
                },

                success: function(response) {

                    if (response.success == true) {
                        window.location.href = response.linkn;
                    } else {
                        $('#form_submit').removeAttr('disabled', 'disabled');
                        $('#invalid_user_msg').css('display', 'block');

                    }

                }
            });



        }

    });

    $("#admin_forget_pass_form").validate({
        rules: {
            email: "required",
        },
        message: {
            email: "Email Id is required."
        },
        submitHandler: function(form) {
            document.getElementById("pw-submit").style.display = "none";
            document.getElementById("pw_processing").style.display = "block";
            var user_email = document.getElementById("email").value;
            data = {
                user_email: user_email
            }
            $.ajax({
                type: "POST",
                url: base_url + "login/forgotPasswordResetForUser",
                data: data,
                dataType: "json",

                success: function(response) {
                    if (response.success == true) {
                        $('#error').css('display', 'block');
                        document.getElementById("error").innerHTML = response.message;
                        document.getElementById("pw-submit").style.display = "block";
                        document.getElementById("pw_processing").style.display = "none";
                    } else {
                        document.getElementById("pw-submit").style.display = "block";
                        document.getElementById("pw_processing").style.display = "none";
                        $('#error').css('display', 'block');
                        document.getElementById("error").innerHTML = response.message;
                    }
                }
            });
            return false;
        }
    });

    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var check = false;
            return this.optional(element) || regexp.test(value);
        }
    );

    $("#user_reset_password_form").validate({
        errorClass: "errormesssage",
        rules: {
            user_password: {
                required: true,
                regex: /^.{8,}$/,
            },
            user_confirm_pass: {
                required: true,
                equalTo: "#user_password",
            },
        },
        messages: {
            user_password: {
                required: "Please Enter Password",
                regex: "Password must be at least 8 characters.",
            },
            user_confirm_pass: {
                required: "Please Enter Confirm Password",
                equalTo: "Password and confirm password must be same."
            }
        },
        submitHandler: function(form) {
            try {
                var user_id = document.getElementById('user_id').value;
                var user_password = document.getElementById('user_password').value;
                var fpt_id = document.getElementById('fpt_id').value;
                data = {
                    user_id: user_id,
                    user_password: user_password,
                    fpt_id: fpt_id
                }
                $.ajax({
                    type: "POST",
                    url: base_url + "login/user_reset_password_submit",
                    data: data,
                    dataType: "json",
                    beforeSend: function() {
                        $('.btn_save').css('display', 'none');
                        $('.btn_processing').css('display', 'inline-block');
                    },
                    success: function(response) {
                        if (response.success == true) {
                            $('.btn_save').css('display', 'inline-block');
                            $('.btn_processing').css('display', 'none');
                            alert(response.message);
                            window.location.href = response.linkn;
                        } else {
                            $('.btn_save').css('display', 'inline-block');
                            $('.btn_processing').css('display', 'none');
                            alert(response.message);
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }
    });
});