$(function() {

  
   // Setup form validation on the #register-form element
   $("#add_param").validate({

   submitHandler: function(form)
     {
      var flag=true;
      if($("#file").val() != '' )
      {
      var allowedFiles = ["jpeg", "jpg", "png","JPG","PNG"];

      var fileName = $("#file")[0].files[0].name;
      var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);

      var size = parseFloat($("#file")[0].files[0].size / 1024).toFixed(2);

      if ($.inArray(fileNameExt, allowedFiles) == -1 || size>5000) 
      {

       var data = "Invalid Size or type";
       document.getElementById("message").innerHTML=data;
       flag = false;

     }
   }

     if(flag)      
       insertData();
 }

 });

 });
function insertData(argument) {


  var formData = new FormData();
  formData.append('file', $('input[type=file]')[0].files[0]);

  formData.append("pfi_id",$('#pfi_id').val());
  formData.append("pfi_old_img",$('#pfi_old_img').val());
  formData.append("pfi_prd_id",$('#pfi_prd_id').val());
  formData.append("pfi_flv_id",$('#pfi_flv_id').val());
    $.ajax({
    type: "POST",
    url:base_url+ "product/insertPrdFlavourImg",
    data : formData,
    dataType:"json",
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,
                beforeSend: function(){
                  $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Save');
                 $('#form_submit').attr('disabled','disabled');
               },
               success: function(response)
               {
                if(response.success==true)
                {
                  alert(response.message); 
                  location.reload();
                }
                else{
                  $("#form_submit").html('submit');
                  $('#form_submit').removeAttr('disabled','disabled');
                  alert(response.message);
                }
              }
            });
  return false;

}
$('#file').on("change", function()
{
  document.getElementById("message").innerHTML='';
});

function loadFlavours(pfi_prd_id) 
{
   $.ajax({
    type:"POST",
       url:base_url+"product/ajaxCallFlavours", 
    data :{ pfi_prd_id:pfi_prd_id},
    success: function(myvar){
      document.getElementById('pfi_flv_id').value='';
  $('#pfi_flv_id').html(myvar);
     }
  });

 }








