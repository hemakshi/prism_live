$(function() {
    $("#form_change_payment_receipt_status").validate({
        submitHandler: function(form) {
            data = {
                prs_id: $('#prs_id').val(),
                payment_receipt_id: $('#payment_receipt_id').val(),
                receipt_status: $('#receipt_status').val(),
                receipt_remark: $('#receipt_remark').val()
            }
            $.ajax({
                type: "POST",
                url: base_url + "order/updatePaymentReceiptStatus",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                    $("#form_submit").attr('disabled', 'disabled');
                },

                success: function(response) {
                    if (response.success == true) {
                        location.reload();
                        alert(response.message);
                    } else {
                        $("#form_submit").html('Save');
                        $("#form_submit").removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        }
    });
});

function show_modal(current_tag) {
    getProductStatus($(current_tag).data("receipt_status"));
    current_tag.setAttribute("href", "#Add");
    document.getElementById('payment_receipt_id').value = $(current_tag).data("payment_receipt_id");
    document.getElementById('receipt_remark').value = $(current_tag).data("pay_remarks");
    document.getElementById('prs_id').value = $(current_tag).data("prs_id");
    document.getElementById('modal-title').innerHTML = 'Payment Receipt No. <b>' + $(current_tag).data("rcpt_number") + '</b>';
}

function getProductStatus(status) {
    data = {
            status: status
        },
        $.ajax({
            url: base_url + "order/getPaymentReceiptStatus",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#receipt_status').html('');
                $('#receipt_status').html(data);
            }
        });
}