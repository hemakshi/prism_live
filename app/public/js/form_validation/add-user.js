 $(function() {
     $("#add_user").validate({
         rules: {
             prs_password: "required",
             prs_cnfrm_password: "required",
             prs_cnfrm_password: {
                 required: true,
                 equalTo: "#prs_password"
             },
         },
         submitHandler: function(form) {
             //  var allowedFiles = ["jpeg", "jpg", "png", "JPG", "PNG"];
             var formData = new FormData();

             //  if ($("#file").val() != '') {
             //      var fileName = $("#file")[0].files[0].name;
             //      var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);

             //      var size = parseFloat($("#file")[0].files[0].size / 1024).toFixed(2);

             //      if ($.inArray(fileNameExt, allowedFiles) == -1 || size > 5000) {
             //          var data = "Invalid Size or type";
             //          document.getElementById("message1").innerHTML = data;
             //          return false;
             //      } else {
             //          formData.append('file', $('input[type=file]')[0].files[0]);
             //      }
             //  } else {
             //      formData.append('file', '');
             //  }
             formData.append('prs_name', $('#prs_name').val());
             formData.append('prs_user_name', $('#prs_user_name').val());
             formData.append('prs_mob', $('#prs_mob').val());
             formData.append('prs_email', $('#prs_email').val());
             formData.append('prs_department', $('#prs_department').val());
             formData.append('prs_password', $('#prs_password').val());

             $.ajax({
                 type: "POST",
                 url: base_url + "user/insertUser",
                 data: formData,
                 dataType: "json",
                 contentType: false, // The content type used when sending data to the server.
                 cache: false, // To unable request pages to be cached
                 processData: false,

                 beforeSend: function() {
                     $("#form_submit").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:18px"></i> Processing');
                     $("#form_submit").attr('disabled', 'disabled');
                 },
                 success: function(response) {
                     if (response.success == true) {
                         alert(response.message);
                         window.location.href = response.linkn;
                     } else {
                         alert(response.message);
                         $("#form_submit").html('Save');
                         $('#form_submit').removeAttr('disabled', 'disabled');
                         // window.location.href=response.linkn;
                     }
                 }
             });
         }
     });
 });


 function upload_images(argument) {
     $('.remove-doc').attr('disabled', 'disabled');
     document.getElementById('upload_button_value_docs').value = '1';

     var form_data = new FormData();
     var ins = document.getElementById('files1').files.length;
     var document_input = new Array();
     var documents_count = document.getElementById('documents_count').value;
     var timg_reference_no = document.getElementById('timg_reference_no').value;

     for (i = 0; i <= documents_count; i++) {
         document_input.push($('#document_input' + i).val());
     }
     for (var x = 0; x < ins; x++) {
         form_data.append("files[]", document.getElementById('files1').files[x]);
     }

     form_data.append("timg_reference_no", timg_reference_no);
     form_data.append("document_input", document_input);
     $.ajax({
         url: base_url + "product/uploadProductImages",
         dataType: "json", // what to expect back from the server
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         type: 'post',
         beforeSend: function() {
             $("#upload_docs").attr('disabled', 'disabled');
         },
         success: function(response) {
             if (response.success == true) {
                 document.getElementById("timg_reference_no").setAttribute("value", response.timg_reference_no);
                 document.getElementById("documents_priview").innerHTML = '';
                 $('#information1').css('display', 'none');
                 $('#upload_docs').css('display', 'none');
                 $("#upload_docs").removeAttr('disabled', 'disabled');

                 var success_msg = 'Following images uploaded successfully.</br>' + response.message;
                 document.getElementById("doc_uploaded").innerHTML = document.getElementById("doc_uploaded").innerHTML + success_msg;

             } else {
                 document.getElementById("timg_reference_no").setAttribute("value", response.timg_reference_no);
                 document.getElementById("documents_priview").innerHTML = '';
                 $('#information1').css('display', 'none');
                 $('#upload_docs').css('display', 'none');
                 $("#upload_docs").removeAttr('disabled', 'disabled');
                 if (response.uploaded_documents != '') {
                     var success_msg = 'Following images uploaded successfully.</br>' + response.uploaded_documents;
                     document.getElementById("doc_uploaded").innerHTML = document.getElementById("doc_uploaded").innerHTML + success_msg;
                 }
                 var error_msg = 'Following images cannot be uploaded..Invalid size or type</br>' + response.message;
                 document.getElementById("doc_error_invalid_size").innerHTML = document.getElementById("doc_error_invalid_size").innerHTML + error_msg;

             }
         }
     });
 }

 // ******* START FOR INDIAN CURRENCY FORMAT ******
 String.prototype.replaceAll = function(search, replacement) {
     var target = this;
     return target.replace(new RegExp(search, 'g'), replacement);
 };

 $('input.Stylednumber').keyup(function() {
     var input = $(this).val().replaceAll(',', '');
     if (input.length < 1)
         $(this).val();
     else {
         var val = parseFloat(input);
         var formatted = inrFormat(input);
         if (formatted.indexOf('.') > 0) {
             var split = formatted.split('.');
             formatted = split[0] + '.' + split[1].substring(0, 2);
         }
         $(this).val(formatted);
     }
 });

 function inrFormat(val) {
     var x = val;
     x = x.toString();
     var afterPoint = '';
     if (x.indexOf('.') > 0)
         afterPoint = x.substring(x.indexOf('.'), x.length);
     x = Math.floor(x);
     x = x.toString();
     var lastThree = x.substring(x.length - 3);
     var otherNumbers = x.substring(0, x.length - 3);
     if (otherNumbers != '')
         lastThree = ',' + lastThree;
     var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
     return res;
 }
 // ******* END FOR INDIAN CURRENCY FORMAT ******