if ($('#section-menu-btn').length > 0) {
    $('.section-top').on('click', '#section-menu-btn', function() {
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened').html('<i class="menu-icon fa fa-shopping-cart"></i> products');
            $('#section-menu-wrap').fadeOut(200);
            $('.section-menu-overlay').fadeOut(200).remove();
        } else {
            $(this).addClass('opened').width($(this).width()).text('Close');
            $('#section-menu-wrap').fadeIn(200);
            $('body').append('<div class="section-menu-overlay"></div>');
            $('.section-menu-overlay').fadeIn(200);

            $('body').on('click', '.section-menu-overlay', function() {
                $('#section-menu-btn').removeClass('opened').html('<i class="menu-icon fa fa-shopping-cart"></i> products');
                $('#section-menu-wrap').fadeOut(200);
                $('.section-menu-overlay').fadeOut(200).remove();
                return false;
            });
        }
        return false;
    });
}

var STICKER = 'sticker';
var BROCHURE_MIX = 'brochure-mix';


$(document).ready(function() {

    $('.add_cart').click(function() {

        var quantity = $(this).data("product_qty");
        if (quantity != '' && quantity > 0) {
            var prd_category = $(this).data("category");
            var prd_price = parseFloat($(this).data("price"));
            var prd_qty = parseInt($(this).data("product_qty"));
            var prd_height = parseFloat($(this).data("product_height"));
            var prd_width = parseFloat($(this).data("product_width"));

            var prd_subtotal = 0;
            if (prd_category == 'sticker' || prd_category == 'brochure-mix') {
                prd_subtotal = prd_price * prd_qty * prd_height * prd_width;
            } else {
                prd_subtotal = prd_price * prd_qty;
                prd_height = '';
                prd_width = '';
            }

            $.ajax({
                url: base_url + "shoping_cart/add",
                method: "POST",
                data: {
                    product_id: $(this).data("productid"),
                    product_name: $(this).data("productname"),
                    product_price: $(this).data("price"),
                    product_subtotal: prd_subtotal,
                    product_quantity: prd_qty,
                    product_height: prd_height,
                    product_width: prd_width,
                    product_category: $(this).data("category")
                },
                success: function(data) {
                    document.getElementById("notify").innerHTML = 'Added to cart';
                    var x = document.getElementById("notify");
                    x.className = "show";
                    setTimeout(function() { location.reload(); }, 700); // Refresh Product List Page
                    setTimeout(function() { x.className = x.className.replace("show", ""); }, 3000);
                    $("#added-cart").css("display", "inline-block");
                    // $(".add_cart").css("display", "none");
                    // $("#cart-count").text(data);
                }

            });
        } else {
            alert("Please Enter quantity");
        }
    });


    $(document).on('click', '#remove_cart_product', function() {

        var row_id = $(this).data("cart_row");
        if (confirm("Are you sure you want to remove this product?")) {
            data = {
                    row_id: row_id
                },
                console.log(data);
            $.ajax({
                url: base_url + "shoping_cart/remove",
                method: "POST",
                data: data,
                dataType: "JSON",
                success: function(data) {
                    // alert("Product Removed From Cart");
                    // document.getElementById('counthead').innerHTML=data;
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });


    $(document).on('click', '#clear_cart', function() {
        if (confirm("Are you sure.You want clear cart?")) {
            $.ajax({
                url: base_url + "shoping_cart/clear",
                success: function(data) {
                    window.location.href = base_url + 'home';
                }
            });
        } else {
            return false;
        }
    });

    /***  Cart Section Start ***/

    /* OnCheckoutClick Start*/
    $('form').submit(function(e) {
        try {
            e.preventDefault();
            var checkoutForm = new FormData(this);
            $.ajax({
                url: base_url + "shoping_cart/onCheckout",
                data: checkoutForm,
                type: 'POST',
                dataType: 'JSON',
                processData: false,
                contentType: false,
                async: false,
                beforeSend: function() {
                    $('#result').html('Processing...');
                },
                success: function(data) {
                    if (data.success == true) {
                        $('#result').html('Checkout success!');
                        $.ajax({
                            url: base_url + "order/address_select",
                            type: 'POST',
                            success: function() {
                                window.location.href = base_url + "address-select";
                            },
                        });
                    } else {
                        $('#result').html('Checkout failed!');
                        window.location.href = data.linkn;
                    }
                }
            });
            e.preventDefault();
        } catch (e) {
            console.log(e);
        }
    });
    /* OnCheckoutClick  End*/

    /* Update Cart Item */
    $(function() {
        $('.updatecart').change(function() {
            var rowid = $(this).data("rowid");
            var qty = $("#qty" + rowid).val();

            var formData = {
                'rowid': rowid,
                'qty': $('input[name=qty' + rowid + ']').val(),
                'price': $('input[name=price' + rowid + ']').val(),
                'height': $('input[name=height' + rowid + ']').val(),
                'width': $('input[name=width' + rowid + ']').val(),
                'category': $('input[name=category' + rowid + ']').val()
            };
            // alert(JSON.stringify(formData));

            $.ajax({
                type: "POST",
                url: base_url + "shoping_cart/updateCart",
                data: formData,
                beforeSend: function() {
                    $('#result').html('Processing...');
                },
                success: function(data) {
                    console.log("data: " + data);
                    location.reload();
                    $('#result').html('Item Updated!');
                }
            });
        });
    });

    /* Update Item  Quantity */
    $(function() {
        $('.changeqty').click(function() {
            var rowid = $(this).data("rowid");
            var fired_button = $(this).val();
            var qty = $("qty" + rowid).val();

            var formData = {
                'rowid': rowid,
                'qty': $('input[name=qty' + rowid + ']').val(),
                'change': fired_button,
                'price': $('input[name=price' + rowid + ']').val(),
                'height': $('input[name=height' + rowid + ']').val(),
                'width': $('input[name=width' + rowid + ']').val(),
                'category': $('input[name=category' + rowid + ']').val()
            };
            // alert(JSON.stringify(formData));

            $.ajax({
                type: "POST",
                url: base_url + "shoping_cart/updateCart",
                data: formData,
                beforeSend: function() {
                    $('#result').html('Processing...');
                },
                success: function(data) {
                    console.log("data: " + data);
                    // location.reload();
                    window.location.reload();
                    $('#result').html('Item Size Updated!');
                }
            });
        });
    });

    /***  Cart Section End ***/
});

function valueChanged(current_tag) {
    alert("current_tag: " + current_tag);
    this.updateCart(current_tag);
}

function designFileValidation(data) {
    let filePath = data.value;
    let allowedExtensions = /(\.cdr)$/i;
    if (!allowedExtensions.exec(filePath)) {
        data.value = '';
        alert('Please upload file having extensions .cdr only.');
        return false;
    }
}

function updateCartItem(obj, rowid, category) {
    data = {
            cart_id: rowid,
            cart_qty: obj.value,
            cart_category: category
        },
        $.ajax({
            url: base_url + "shoping_cart/updateItemQty",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                if (data == 1) {
                    $('#response').html(data);
                } else {
                    $('#response').html(0);
                }

                alert("Data: " + data);
                if (data.redirect) {
                    swal({
                        title: '¡Success!',
                        text: data.msg,
                        timer: 2000,
                        type: data.type,
                        showConfirmButton: false
                    }, function() {
                        window.location.href = data.redirect;
                    });
                } else {
                    swal('¡Error!', data.msg, data.type);
                }
            }
        });
}

// function updateCartModal(type, thisElement) {
//     var cart_id = $(thisElement).data('cart_row');
//     // var cart_qty = $(thisElement).data('cart_qty');
//     var cart_qty = $(thisElement).val();
//     var cart_prd_id = $(thisElement).data('cart_prd_id');
//     var title = '';
//     var type_title = '';
//     clearData();
//     switch (type) {
//         case 'qty':
//             $('#modal-title').html('Change Quantity');
//             type_title = "qty";
//             $('#change-qty').css('display', 'block');
//             // $('#change-flavour').css('display','none');
//             // getQty(cart_qty);
//             // alert("cart_qty: " + getQty(cart_qty));
//             break;
//         default:
//             type_title = '';
//             break;
//     }
//     title = 'Update ' + type_title;
//     $('.update-cart-title').html(title);
//     $('#cart_type').val(type);
//     $('#cart_id').val(cart_id);
//     // $('#cart_flavour').val(cart_flavour);
//     $('#cart_qty').val(cart_qty);
// }

function clearData() {
    $('#cart_type').val();
    $('#cart_id').val();
    //$('#cart_flavour').val();
    $('#cart_qty').val();
    return true;
}
// $('#cart_flavour,#cart_qty').change(function(){
//   updateCart();
// });

function updateCart(current_tag) {

    var cart_type = $('#cart_type').val();
    var cart_id = $('#cart_id').val();
    // var cart_flavour  = '';
    var cart_qty = '';
    // var cart_flavour_name = '';
    // var cart_flavour_name = '';
    switch (cart_type) {
        // case 'flavour':
        //               cart_flavour  = $(current_tag).data("id");
        //               cart_flavour_name  = $(current_tag).data("name");
        //               break;
        case 'qty':

            cart_qty = $(current_tag).data("qty");
            break;
        default:
            cart_flavour = '';
            cart_qty = '';
            break;
    }
    // var cart_flavour  = cart_flavour;
    var cart_qty = cart_qty;
    data = {
            cart_type: cart_type,
            cart_id: cart_id,
            // cart_flavour:cart_flavour,
            // cart_flavour_name:cart_flavour_name,
            cart_qty: cart_qty

        },
        console.log(data);
    $.ajax({
        url: base_url + "shoping_cart/updateCart",
        method: "POST",
        data: data,
        dataType: "JSON",
        success: function(data) {
            $('.close_modal').click();
            location.reload();

        }

    });
}

function placeOrder(argument) {
    var prs_id = document.getElementById('prs_id').value;
    if (prs_id == '') {
        window.location.href = base_url + 'login';
    } else {
        window.location.href = base_url + 'user-address';
    }
}

function flavourSelected(current_tag) {
    window.location.href = base_url + 'product-detail/' + $("#product_slug").val() + '/' + $(current_tag).data("slug");
    //  var product_flavour = $("input[name='flavours']:checked").val();
    // $( "#product_flavour_name" ).val(value );
    //   this.loadZoomImg(product_flavour);
    //  this.loadSmallImg(product_flavour);
}

function getProdFlavours(prd_id, flavour) {
    data = {
            prd_id: prd_id,
            // flavour: flavour
        },
        $.ajax({
            url: base_url + "product/getProdFlavoursById",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#change-flavour').html('');
                $('#change-flavour').html(data);
            }

        });
}

function getQty(qty) {
    data = {
            qty: qty,
        },
        $.ajax({
            url: base_url + "product/getQuantity",
            method: "POST",
            data: data,
            dataType: "JSON",
            success: function(data) {
                $('#change-qty').html('');
                $('#change-qty').html(data);
            }
        });
}