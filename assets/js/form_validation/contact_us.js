 $("#contact_form").validate({
     errorClass: "errormesssage",
     rules: {
         contact_email: {
             required: true,
             email: true,
         }
     },
     messages: {
         contact_email: {
             required: "Please Enter Email Id",
             email: "Please Enter Valid Email Id"
         }
     },
     submitHandler: function(form) {
         try {
             var contact_name = document.getElementById('contact_name').value;
             var contact_email = document.getElementById('contact_email').value;
             var contact_mob = document.getElementById('contact_mob').value;
             var contact_msg = document.getElementById('contact_msg').value;
             data = {
                 contact_name: contact_name,
                 contact_email: contact_email,
                 contact_mob: contact_mob,
                 contact_msg: contact_msg
             }

             $.ajax({
                 type: "POST",
                 url: base_url + "home/contact_us_form",
                 data: data,
                 dataType: "json",
                 beforeSend: function() {
                     $("#btn_save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Send Message');
                     $('#btn_save').attr('disabled', 'disabled');
                 },
                 success: function(response) {
                     if (response.success == true) {
                         alert(response.message);
                         window.location.href = response.linkn;
                     } else {
                         $("#btn_save").html('Send Message');
                         $('#btn_save').removeAttr('disabled', 'disabled');
                         alert(response.message);
                     }
                 }
             });
         } catch (e) {
             console.log(e);
         }
     }
 });