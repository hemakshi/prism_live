$(function()
{
    $("#person_address_form").validate(
    {
        errorClass: "errormesssage",
        submitHandler: function(form)
        {
            try
            {
                var data = $('#person_address_form').serialize();

          $.ajax(
                {
                    type: "POST",
                    url: base_url + "person/personAddressform",
                    data: data,
                    dataType: "json",
                    beforeSend: function()
                    {
                         $("#btn-save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Save');
                         $('#btn-save').attr('disabled','disabled');
                    },
                    success: function(response)
                    {
                        if (response.success == true)
                        {
                            location.reload();
                        }
                        else
                        {
                          $("#btn-save").html('Save');
                          $('#btn-save').removeAttr('disabled','disabled');
                            alert(response.message);
                        }
                    }
                });
            }
            catch (e)
            {
                console.log(e);
            }
        }
    });
});

function updateFormData(thisElement)
{
 $('.modal-title').text('Edit address');
  console.log( $(thisElement).data());
  var pad_id = $(thisElement).data('pad_id');
  var address_type = $(thisElement).data('address_type');
  var address_type_name = $(thisElement).data('address_type_name');
  var pad_name = $(thisElement).data('pad_name');
  var pad_mobile = $(thisElement).data('pad_mobile');
  var pad_address = $(thisElement).data('pad_address');
  var pad_locality = $(thisElement).data('pad_locality');
  var pad_mobile = $(thisElement).data('pad_mobile');
  var pad_name = $(thisElement).data('pad_name');
  var state = $(thisElement).data('state');
  var state_name = $(thisElement).data('state_name');
  var pad_city = $(thisElement).data('pad_city');
  var pad_pincode = $(thisElement).data('pad_pincode');
  var pad_landmark = $(thisElement).data('pad_landmark');
  var pad_alt_phone = $(thisElement).data('pad_alt_phone');

   clearData();
  //UpdateData
  $('#pad_id').val(pad_id);
  $('#pad_name').val(pad_name);
  $('#pad_mobile').val(pad_mobile);
  $('#pad_address').val(pad_address);
  $('#pad_locality').val(pad_locality);
  $('#pad_mobile').val(pad_mobile);
  $('#pad_name').val(pad_name);
  $('#pad_city').val(pad_city);
  $('#pad_pincode').val(pad_pincode);
  $('#pad_landmark').val(pad_landmark);
  $('#pad_alt_phone').val(pad_alt_phone);

  var address_type_option = '<option selected="selected" value="'+address_type+'">'+address_type_name+'</option>';
  var state_option = '<option selected="selected" value="'+state+'">'+state_name+'</option>';
 console.log($('#pad_address_type').append(address_type_option));
  $('#pad_state').append(state_option);


}
function clearData()
{
  $('#pad_id').val('');
  $('#pad_flat_no').val('');
  $('#pad_wing').val('');
  $('#pad_building').val('');
  $('#pad_road').val('');
  $('#pad_area').val('');
  $('#pad_location_id').val('');
  $('#pad_city').val('');
  $('#pad_pincode').val('');
  return true;
} 
function updateSelectedAddress(selectedElement,pad_id) {
    $('.address-row').removeClass("selected");
    $(selectedElement).addClass("selected");
    $('#selected_address').val(pad_id);
  }  
$('.address_select').click(function(){
  console.log($(this).data());
  var address_id = $(this).data('address_id');
  $('.address_select').val('Deliver to this address');
  $('.address_select').removeClass('custom-btn-inactive');
  $('.address_select').removeClass('custom-btn-active');
  $(this).val('Shipping to this address');
  $(this).addClass('custom-btn-active');
  $('#selected_address').val(address_id);
});
function selectAddress()
{
  // var ord_adress_id = $('.select').parent('.selected').find('.select').attr('data-selected_address');
  // console.log( ord_adress_id);
  var ord_adress_id = $('#selected_address').val();

  if(ord_adress_id == 0)
  {
    alert('Please Select an address');
    return false;
  }
  var data = {
    ord_adress_id:ord_adress_id
  }
  $.ajax(
    {
        type: "POST",
        url: base_url + "order/selectAddress",
        data: data,
        dataType: "json",
        beforeSend: function()
        {
            //$("#login_form").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
        },
        success: function(response)
        {
            if (response.success == true)
            {

                  window.location.href=response.linkn;
            }
            else
            {
                  
                alert(response.message);
            }
        }
    });
}