// $(document).ready(function() {
//     var $regexname = /^[0-9]+$/;
//     $('#TXN_AMOUNT').on('keypress keydown keyup', function() {
//         if (!$(this).val().match($regexname)) {
//             // there is a mismatch, hence show the error message
//             $('.emsg').removeClass('hidden');
//             $('.emsg').show();
//         } else {
//             // else, do not display message
//             $('.emsg').addClass('hidden');
//         }
//     });
// });

$(function() {
    $("#add_balance_form").validate({
        rules: {
            TXN_AMOUNT: {
                required: true,
                digits: true,
                min: 500
            }
        },
        messages: {
            TXN_AMOUNT: { required: "Please Enter Amount", digits: "Please enter numbers Only", min: "Please enter minimum 500 amount" }
        },
        errorClass: "errormesssage"
            // TXN_AMOUNT: {
            //     required: true,
            //     min: 13,
            //     // regex: /^[0-9]*$/,
            // },
            // messages: {
            //     TXN_AMOUNT: "Please Enter Amount"
            // },
            // submitHandler: function(form) {
            //     try {
            //         var usr_username = document.getElementById('usr_username').value;
            //         var usr_password = document.getElementById('usr_password').value;

        //         var ref = $('#ref').val();
        //         data = {
        //             usr_username: usr_username,
        //             usr_password: usr_password,
        //             rememberme: 1,
        //             ref: ref
        //         }

        //         $.ajax({
        //             type: "POST",
        //             url: base_url + "login/checklogin",
        //             data: data,
        //             dataType: "json",
        //             beforeSend: function() {
        //                 $("#login_btn_save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Login');
        //                 $('#login_btn_save').attr('disabled', 'disabled');
        //             },
        //             success: function(response) {
        //                 if (response.success == true) {
        //                     window.location.href = response.linkn;
        //                 } else {
        //                     $('#not-match-error').css('display', 'block');
        //                     $("#login_btn_save").html('Login');
        //                     $('#login_btn_save').removeAttr('disabled', 'disabled');

        //                 }
        //             }
        //         });
        //     } catch (e) {
        //         console.log(e);
        //     }
        // }
    });
});