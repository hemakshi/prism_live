$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    }, );

$("#reset_password_form").validate({
    errorClass: "errormesssage",
    rules: {
        prs_password: {
            required: true,
            regex: /^.{8,}$/,
        },
        prs_confirm_pass: {
            required: true,
            equalTo: "#prs_password",
        },
    },
    messages: {
        prs_password: {
            required: "Please Enter Password",
            regex: "Password must be at least 8 characters.",
        },
        prs_confirm_pass: {
            required: "Please Enter Confirm Password",
            equalTo: "Password and confirm password must be same."
        }
    },
    submitHandler: function(form) {
        try {
            var prs_id = document.getElementById('prs_id').value;
            var prs_password = document.getElementById('prs_password').value;
            data = {
                prs_id: prs_id,
                prs_password: prs_password
            }

            // $('.btn_save').css('display', 'none');
            // $('.btn_processing').css('display', 'inline-block');

            $.ajax({
                type: "POST",
                url: base_url + "login/reset_password",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    //  $("#login_form").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                    $('.btn_save').css('display', 'none');
                    $('.btn_processing').css('display', 'inline-block');
                },
                success: function(response) {
                    if (response.success == true) {
                        $('.btn_save').css('display', 'inline-block');
                        $('.btn_processing').css('display', 'none');
                        alert(response.message);
                        window.location.href = response.linkn;
                    } else {
                        $('.btn_save').css('display', 'inline-block');
                        $('.btn_processing').css('display', 'none');
                        alert(response.message);
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }
    }
});