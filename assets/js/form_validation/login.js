$(function() {
    $("#login_form").validate({
        errorClass: "errormesssage",
        rules: {
            usr_username: "required",
            usr_password: "required"
        },
        messages: {
            usr_username: "Please Enter Username",
            usr_password: "Please Enter a Password"
        },
        submitHandler: function(form) {
            try {
                var usr_username = document.getElementById('usr_username').value;
                var usr_password = document.getElementById('usr_password').value;
                if (document.getElementById('rememberme').checked == true) {
                    var rememberme = 1;
                } else {
                    var rememberme = 0;
                }

                var ref = $('#ref').val();
                data = {
                    usr_username: usr_username,
                    usr_password: usr_password,
                    rememberme: rememberme,
                    ref: ref
                }

                $.ajax({
                    type: "POST",
                    url: base_url + "login/checklogin",
                    data: data,
                    dataType: "json",
                    beforeSend: function() {
                        $("#login_btn_save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Login');
                        $('#login_btn_save').attr('disabled', 'disabled');
                    },
                    success: function(response) {
                        if (response.success == true) {
                            window.location.href = response.linkn;
                        } else {
                            $('#not-match-error').css('display', 'block');
                            $("#login_btn_save").html('Login');
                            $('#login_btn_save').removeAttr('disabled', 'disabled');

                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }
    });
});
$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    }, );
$("#register_form").validate({
    errorClass: "errormesssage",
    rules: {
        register_prs_password: {
            required: true,
            regex: /^.{8,}$/,
        },
        register_prs_cnfrm_password: {
            required: true,
            equalTo: "#register_prs_password",
        },
        register_prs_mob: {
            required: true,
            regex: /^\d{10}$/,
            remote: {
                url: base_url + 'login/personContactValidation/mobile',
                type: "post",
                data: {
                    value: function() {
                        return $('#register_prs_mob').val();
                    },
                },
            },
        },
        register_prs_email: {
            required: true,
            remote: {
                url: base_url + 'login/personContactValidation/email',
                type: "post",
                data: {
                    value: function() {
                        return $('#register_prs_email').val();
                    },
                },
            },
        },
        register_prs_whatsapp: {
            regex: /^\d{10}$/,
        }
    },
    messages: {
        register_prs_mob: {
            regex: "Mobile number must be 10 digits only.",
            remote: function() { return $.validator.format("{0} is already taken", $("#register_prs_mob").val()); },
        },
        register_prs_email: {
            remote: function() { return $.validator.format("{0} is already taken", $("#register_prs_email").val()); },
        },
        register_prs_password: {
            regex: "Password must be at least 8 characters.",
        },
        register_prs_whatsapp: {
            regex: "Whatsapp number must be 10 digits only."
        }
    },
    submitHandler: function(form) {
        try {
            var prs_name = document.getElementById('register_prs_name').value;
            var prs_email = document.getElementById('register_prs_email').value;
            var prs_mob = document.getElementById('register_prs_mob').value;

            var prs_password = document.getElementById('register_prs_password').value;
            var comp_name = document.getElementById('register_comp_name').value;
            var cont_name = document.getElementById('register_prs_cont_name').value;
            var prs_country = document.getElementById('register_prs_country').value;
            var prs_state = document.getElementById('register_prs_state').value;
            var prs_dist = document.getElementById('register_prs_dist').value;
            var prs_city = document.getElementById('register_prs_city').value;
            var prs_pincode = document.getElementById('register_prs_pincode').value;
            var prs_address = document.getElementById('register_prs_address').value;
            var prs_landmark = document.getElementById('register_prs_landmark').value;
            var prs_whatsapp = document.getElementById('register_prs_whatsapp').value;
            var prs_gst = document.getElementById('register_prs_gst').value;


            var ref = $('#ref').val();
            data = {
                prs_name: prs_name,
                prs_email: prs_email,
                prs_mob: prs_mob,

                prs_password: prs_password,
                comp_name: comp_name,
                cont_name: cont_name,
                prs_country: prs_country,
                prs_state: prs_state,
                prs_dist: prs_dist,
                prs_city: prs_city,
                prs_pincode: prs_pincode,
                prs_address: prs_address,
                prs_landmark: prs_landmark,
                prs_whatsapp: prs_whatsapp,
                prs_gst: prs_gst,
                ref: ref
            }

            $.ajax({
                type: "POST",
                url: base_url + "login/registerCustomer",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#register_btn_save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Register');
                    $('#register_btn_save').attr('disabled', 'disabled');
                },
                success: function(response) {
                    alert(JSON.stringify(response));
                    if (response.success == true) {
                        window.location.href = response.linkn;
                    } else {
                        $("#register_btn_save").html('Register');
                        $('#register_btn_save').removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }
    }
});
$('.forget_pass_button').click(function() {
    $('#forget_pass_form_div').css('display', 'block');
    $('.login-form').css('display', 'none');
});
$('#register,#login').click(function() {
    $('#forget_pass_form_div').css('display', 'none');
});
$('#login,#back-btn').click(function() {
    $('.login-form').css('display', 'block');
    $('#forget_pass_form_div').css('display', 'none');
});
$("#forget_pass_form").validate({
    errorClass: "errormesssage",

    submitHandler: function(form) {
        try {
            var prs_email = document.getElementById('fpwd_email').value;

            data = {
                prs_email: prs_email
            }
            $.ajax({
                type: "POST",
                url: base_url + "login/forgotPasswordReset",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#fpwd_btn_save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Submit');
                    $('#fpwd_btn_save').attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        $('.fpwd_btn_save').css('display', 'inline-block');
                        $('.fpwd_btn_processing').css('display', 'none');
                        alert(response.message);
                        location.reload();
                    } else {
                        $("#fpwd_btn_save").html('Submit');
                        $('#fpwd_btn_save').removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }
    }
});