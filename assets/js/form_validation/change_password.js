$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    }, );
$("#change_password_form").validate({
    errorClass: "errormesssage",
    rules: {
        old_password: {
            required: true,
            remote: {
                url: base_url + 'person/validatePersonPassword',
                type: "post",
                data: {
                    old_password: function() {
                        return $('#old_password').val();
                    },
                    prs_id: function() {
                        return $('#prs_id').val();
                    },
                },
            },
        },
        new_password: {
            required: true,
            regex: /^.{8,}$/,
        },
        prs_confirm_pass: {
            required: true,
            equalTo: "#new_password",
        },
    },
    messages: {
        old_password: {
            remote: function() { return $.validator.format("Invalid Old Password"); },
        },
        new_password: {
            regex: "Password must be at least 8 characters.",
        },
        prs_confirm_pass: {
            equalTo: "New Password & Confirm Password Did Not Match."
        },
    },
    submitHandler: function(form) {
        try {
            var prs_id = document.getElementById('prs_id').value;
            var new_password = document.getElementById('new_password').value;
            data = {
                prs_id: prs_id,
                prs_password: new_password
            }

            $('.btn_save').css('display', 'none');
            $('.btn_processing').css('display', 'inline-block');

            $.ajax({
                type: "POST",
                url: base_url + "person/change_password",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    //$("#login_form").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                },
                success: function(response) {
                    if (response.success == true) {
                        $('.btn_save').css('display', 'inline-block');
                        $('.btn_processing').css('display', 'none');
                        alert("Password Changed Successfully.");
                        location.reload();
                    } else {
                        $('.btn_save').css('display', 'inline-block');
                        $('.btn_processing').css('display', 'none');
                        alert("Something Went Wrong.");
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }
    }
});