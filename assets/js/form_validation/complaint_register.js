var isComplaintRegisterFormValid = false;

$(document).ready(function() {
    $("#complaint_register_form").validate({
        errorClass: "errormesssage",
        rules: {
            order_number: {
                required: true
            },
            complaint_description: {
                required: true
            },
            printed_file: {
                required: true
            }
        },
        submitHandler: function(form) {
            isComplaintRegisterFormValid = true;
        }
    });
});

$(document).ready(function() {
    $("#complaint_register_form").submit(function(e) {
        if (isComplaintRegisterFormValid == true) {
            try {
                var checkoutForm = new FormData(this);
                $.ajax({
                    url: base_url + "Home/registerComplaint",
                    data: checkoutForm,
                    type: 'POST',
                    dataType: 'JSON',
                    mimeType: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    async: true,
                    beforeSend: function() {
                        $("#btn_register").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:16px; line-height: 0.2; border-radius: 17px;"></i> Processing');
                        $("#btn_register").attr('disabled', 'disabled');
                    },
                    success: function(data) {
                        if (data.success == true) {
                            window.location.href = data.linkn;
                            alert(data.message);
                        } else {
                            $("#btn_register").html('Register');
                            $('#btn_register').removeAttr('disabled', 'disabled');
                            alert(data.message);
                            window.location.href = data.linkn;
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        } else {
            e.preventDefault();
        }
    });
});

function fileValidation() {
    var fileInput = document.getElementById('printed_file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf|\.mp4)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('Please upload file having extensions .jpeg/.jpg/.png/.pdf/.mp4 only.');
        fileInput.value = '';
        return false;
    }
}