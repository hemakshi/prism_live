var isQuotationRequestFormValid = false;

$(document).ready(function() {
    $("#quotation_request_form").validate({
        errorClass: "errormesssage",
        rules: {
            txt_product_name: {
                required: true
            },
            txt_pepar_gsm_size: {
                required: true
            },
            txt_quantity: {
                required: true
            },
            txt_remarks: {
                required: true
            }
        },
        submitHandler: function(form) {
            isQuotationRequestFormValid = true;
        }
    });
});

$(document).ready(function() {
    $("#quotation_request_form").submit(function(e) {
        if (isQuotationRequestFormValid == true) {
            try {
                var checkoutForm = new FormData(this);
                $.ajax({
                    url: base_url + "Home/sendQuotationRequest",
                    data: checkoutForm,
                    type: 'POST',
                    dataType: 'JSON',
                    mimeType: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    async: true,
                    beforeSend: function() {
                        $("#btn_send_quotation_request").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:16px; line-height: 0.2; border-radius: 17px;"></i> Processing');
                        $("#btn_send_quotation_request").attr('disabled', 'disabled');
                    },
                    success: function(data) {
                        if (data.success == true) {
                            window.location.href = data.linkn;
                            alert(data.message);
                        } else {
                            $("#btn_send_quotation_request").html('Send Quotation Request');
                            $('#btn_send_quotation_request').removeAttr('disabled', 'disabled');
                            alert(data.message);
                            window.location.href = data.linkn;
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        } else {
            e.preventDefault();
        }
    });
});

function fileValidation() {
    var fileInput = document.getElementById('file_related_image');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('Please upload file having extensions .jpeg/.jpg/.png/.pdf only.');
        fileInput.value = '';
        return false;
    }
}