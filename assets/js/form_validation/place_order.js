$('.place-order').click(function() {
    if (confirm("Are you sure you want to place order?")) {
        place_order();
    } else {
        return false;
    }
});

function place_order() {
    try {
        var ord_deliver_address = $('#ord_address').val();
        var ord_shipping_charges = $('#ord_shipping_charges').val();
        var ord_payment_mode = $('input[name="ord_payment_mode"]:checked').val();
        var ord_grand_total = parseFloat($('#ord_grand_total').val());
        var wallet_balance = parseFloat($('#wallet_balance').val());

        if (ord_grand_total >= wallet_balance) {
            alert("Insufficient Balance");
        } else {

            data = {
                ord_deliver_address: ord_deliver_address,
                ord_shipping_charges: ord_shipping_charges,
                ord_payment_mode: ord_payment_mode
            }
            $.ajax({
                type: "POST",
                url: base_url + "order/place_order",
                dataType: "json",
                async: true,
                data: data,
                beforeSend: function() {
                    $(".btn_save").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Pay Now');
                    $('.btn_save').attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        $(".btn_save").html('Pay Now');
                        $('.btn_save').removeAttr('disabled', 'disabled');
                        $.ajax({
                            url: base_url + "order/deleteIncompleteOrderOfCurrentUser",
                            type: 'GET',
                            success: function(data) {
                                data = JSON.parse(data);
                                if (data.success == true) {
                                    alert(data.message);
                                    window.location.href = data.linkn;
                                    location.reload();
                                } else {
                                    alert(data.message);
                                }
                            },
                        });
                    } else {
                        alert(response.message);
                        window.location.href = response.linkn;
                        location.reload();
                    }
                }
            });
        }
    } catch (e) {
        console.log(e);
    }
}