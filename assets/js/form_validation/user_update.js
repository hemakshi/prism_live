$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    }, );

var prs_id = $('#prs_id').val();
$("#person_update_form").validate({
    errorClass: "errormesssage",
    rules: {
        prs_name: "required",
        prs_mob: {
            required: true,
            regex: /^\d{10}$/,
            remote: {
                url: base_url + 'login/personContactValidation/mobile',
                type: "post",
                data: {
                    value: function() {
                        return $('#prs_mob').val();
                    },
                    prs_id: function() {
                        return $('#prs_id').val();
                    },
                },
            },
        },
        prs_email: {
            remote: {
                url: base_url + 'login/personContactValidation/email',
                type: "post",
                data: {
                    value: function() {
                        return $('#prs_email').val();
                    },
                    prs_id: function() {
                        return $('#prs_id').val();
                    },
                },
            },
        },
        prs_whatsapp: {
            required: true,
            regex: /^\d{10}$/
        },
        prs_address: {
            required: true,
        },
        prs_city: {
            required: true,
        },
        prs_pincode: {
            required: true,
        },
        prs_dist: {
            required: true,
        },
        prs_state: {
            required: true,
        },
        prs_country: {
            required: true,
        }
    },
    messages: {
        prs_mob: {
            regex: "Mobile number must be 10 digits only.",
            remote: function() { return $.validator.format("{0} is already taken", $("#prs_mob").val()); },
        },
        prs_email: {
            remote: function() { return $.validator.format("{0} is already taken", $("#prs_email").val()); },
        },
        prs_whatsapp: {
            regex: "Whatsapp number must be 10 digits only.",
        },
    },
    submitHandler: function(form) {
        try {
            var prs_id = document.getElementById('prs_id').value;
            var prs_name = document.getElementById('prs_name').value;
            var prs_mob = document.getElementById('prs_mob').value;
            var prs_old_mob = document.getElementById('prs_old_mob').value;
            // var prs_bio = document.getElementById('prs_bio').value;
            // var prs_dob = document.getElementById('prs_dob').value;
            // var prs_gender = document.getElementById('prs_gender').value;
            var ref = document.getElementById('ref').value;

            var prs_gst = document.getElementById('prs_gst').value;
            var prs_whatsapp = document.getElementById('prs_whatsapp').value;
            var prs_address = document.getElementById('prs_address').value;
            var prs_city = document.getElementById('prs_city').value;
            var prs_pincode = document.getElementById('prs_pincode').value;
            var prs_dist = document.getElementById('prs_dist').value;
            var prs_state = document.getElementById('prs_state').value;
            var prs_country = document.getElementById('prs_country').value;
            data = {
                prs_id: prs_id,
                prs_name: prs_name,
                prs_mob: prs_mob,
                prs_old_mob: prs_old_mob,
                ref: ref,
                prs_gst: prs_gst,
                prs_whatsapp: prs_whatsapp,
                prs_address: prs_address,
                prs_city: prs_city,
                prs_pincode: prs_pincode,
                prs_dist: prs_dist,
                prs_state: prs_state,
                prs_country: prs_country
            };

            $.ajax({
                type: "POST",
                url: base_url + "person/updatePerson",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $("#login_form").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
                },
                success: function(response) {
                    if (response.success == true) {
                        $("#btn-dave").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Save');
                        $('#btn-dave').attr('disabled', 'disabled');
                        if (response.linkn != '') {
                            window.location.href = response.linkn;
                        } else {
                            location.reload();
                        }
                    } else {
                        $("#btn-dave").html('Save');
                        $('#btn-dave').removeAttr('disabled', 'disabled');
                        alert(response.message);
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }
    }
});

function clearData() {
    $('#prs_id').val('');
    $('#prs_name').val('');
    $('#prs_username').val('');
    $('#prs_mob').val('');
    $('#prs_old_mob').val('');
    $('#prs_email').val('');
    return true;
}
$('.user_update').click(function() {
    getUserData();
});

function getUserData() {
    $.ajax({
        type: "POST",
        url: base_url + "person/getUserData",
        dataType: "json",
        async: false,
        beforeSend: function() {
            //$("#login_form").html('<i class="fa fa-spinner fa-spin" style="font-size:18px"></i> Connecting');
        },
        success: function(response) {
            console.log(response);
            $('#prs_id').val(response.prs_id);
            $('#prs_name').val(response.prs_name);
            $('#prs_username').val(response.prs_username);
            $('#prs_mob').val(response.prs_mob);
            $('#prs_old_mob').val(response.prs_mob);
            $('#prs_email').val(response.prs_email);
        }
    });
}