function goBack() {
    if (confirm("Do you really want to go back??")) {
        history.go(-1);
        return false;
    }

}
$(document).ready(function() {
    // sendOtp();
});

function sendOtp(resend_otp = '') {
    try {
        var prs_id = document.getElementById('prs_id').value;
        // var prs_mob = document.getElementById('prs_mob').value;
        var prs_email = document.getElementById('prs_email').value;
        var resend_otp_code = resend_otp;

        data = {
            prs_id: prs_id,
            prs_email: prs_email,
            resend_otp_code: resend_otp_code
        }
        $('.otp_resend').css('display', 'none');
        $('.btn_processing').css('display', 'inline-block');

        $.ajax({
            type: "POST",
            url: base_url + "login/sendOtp",
            data: data,
            dataType: "json",
            success: function(response) {
                if (response.success == true) {
                    $('.otp_message_html').css('display', 'block');
                    $('.otp_message_code').removeAttr('disabled');
                    $('#otp_id').val(response.otp_id);

                } else {
                    alert(response.message);
                }
                $('.otp_resend').css('display', 'inline-block');
                $('.btn_processing').css('display', 'none');
            }
        });
    } catch (e) {
        console.log(e);
    }
}
$("#otp_verification").validate({
    errorClass: "errormesssage",

    submitHandler: function(form) {
        try {
            var otp_id = document.getElementById('otp_id').value;
            var otp_code = document.getElementById('otp_code').value;

            data = {
                otp_id: otp_id,
                otp_code: otp_code,
            }

            $.ajax({
                type: "POST",
                url: base_url + "login/checkOtp",
                data: data,
                dataType: "json",
                beforeSend: function() {
                    $(".otp_btn_verify").html('<i class="fa fa-circle-o-notch fa-spin spinner"></i> Verify');
                    $('.otp_btn_verify').attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.success == true) {
                        window.location.href = response.linkn;
                    } else {
                        $(".otp_btn_verify").html('Verify');
                        $('.otp_btn_verify').removeAttr('disabled', 'disabled');
                        $('.otp_message_html').css('display', 'block');
                        $('.otp_message_code').removeAttr('disabled');
                        alert(response.message);
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }
    }
});

setTimeout(function() {
    $('.otp_resend').css('display', 'inline-block');
}, 5000);

$('.otp_resend').click(function() {
    var prs_email = $('#prs_email').val();
    var otp_resend_msg = 'OTP has been successfully resent to <strong>' + prs_email + '</strong>';
    $('.otp_message_html').html(otp_resend_msg);
    sendOtp(1);
});