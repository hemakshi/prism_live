appname = angular.module("products",[]);
appname.filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);
appname.controller("productsCtrl",function($scope,$http){
   var cat_id = parseInt($('#cat_id').val());
    var menu = parseInt($('#menu').val());
     $http({

        method:'post',
        dataType : 'json',
        data:{menu:menu},
        url:base_url+'product/getAllProducts',
       headers : {'Content-Type': 'application/json'} 
    }).then(function (res)
    {
       $scope.products = res.data;
        $scope.products = $scope.products.filter((e) => {
        return  e.prd_cat_id = parseInt(e.prd_cat_id);
      });
    $scope.includeCat(cat_id);
  });

  // START FILTER FOR EXPERIENCE
       $scope.catIncludes = [];
       $scope.includeCat = function(id) {
        var i = $.inArray(id, $scope.catIncludes);
           if (i > -1) {
               $scope.catIncludes.splice(i, 1);
           } else {
               $scope.catIncludes.push(id);
           }
       }
      $scope.catFilter = function(products) {
        if ($scope.catIncludes.length > 0) {
               if ($.inArray(products.prd_cat_id, $scope.catIncludes) < 0)
                   return;
           }
           return products;
    }
 $scope.round = function(price) {
  return Math.round(price);
 }
});






